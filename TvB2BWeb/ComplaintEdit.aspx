﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ComplaintEdit.aspx.cs" Inherits="ComplaintEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title></title>
  <script src="Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery.datepick/jquery-ui-i18n.js" type="text/javascript"></script>
  <script src="Scripts/jquery.json.js" type="text/javascript"></script>
  <script src="Scripts/json2.js" type="text/javascript"></script>
  <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
  <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>
  <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
  <script src="Scripts/stickyFloat.js" type="text/javascript"></script>
  <script src="Scripts/checkDate.js" type="text/javascript"></script>
  <script src="Scripts/linq.js" type="text/javascript"></script>
  <script src="Scripts/jquery.linq.js" type="text/javascript"></script>
  <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>
  <script src="Scripts/dmuploader.js" type="text/javascript"></script>

  <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="CSS/ui.slider.extras.css" rel="stylesheet" type="text/css" />
  <link href="CSS/main.css" rel="stylesheet" type="text/css" />
  <link href="CSS/ComplaintEdit.css?v=1" rel="Stylesheet" type="text/css" />

  <style type="text/css">
    
  </style>

  <script type="text/javascript">

    var twoLetterISOLanguageName = '<%= twoLetterISOLanguageName %>';
    var basePageRoot = '<%= Global.getBasePageRoot() %>';

    function uploadPictureClose(fileName) {
      $("#dialog-pictureUpload").dialog('close');

      var data = $("#ComplaintEditForm").data('data');
      var params = new Object();
      params.RecID = data.ComplaintRecord.RecID;
      params.FileName = fileName;

      $.ajax({
        type: "POST",
        data: JSON.stringify(params),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "ComplaintEdit.aspx/saveComplaintPicture",
        success: function (response) {
          if (response.hasOwnProperty('d') && response.d != null) {
            if (response.d.saved == true) {
              if (response.d.DisabledImageBtn) {
                $("#btnPicUpload").hide();
              } else {
                $("#btnPicUpload").show();
              }
              showGaleria(response.d.ImageList);
            }
            else {
              alert('Not saved');
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            alert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function onClickPictureUpload() {
      var data = $("#ComplaintEditForm").data('data');
      $("#pictureUpload").attr("src", basePageRoot + "ComplaintImageUpload.aspx?ResNo=" + data.ComplaintRecord.ResNo + "&RecID=" + data.ComplaintRecord.RecID);
      $("#dialog-pictureUpload").dialog({
        autoOpen: true,
        modal: true,
        width: 450,
        height: 400,
        resizable: true,
        autoResize: true
      });
    }

    function onSaveButtonClick() {
      var formParams = $("#ComplaintEditForm").data('params');
      var data = $("#ComplaintEditForm").data('data');
      var params = new Object();
      var complaintRec = new Object();
      complaintRec.RecID = formParams.RecID;
      complaintRec.ResNo = data.ComplaintRecord.ResNo;
      complaintRec.PasTrfID = data.ComplaintRecord.PasTrfID;
      complaintRec.GroupNo = data.ComplaintRecord.GroupNo;
      complaintRec.GuestName = $("#GuestName").val();
      complaintRec.Agency = data.ComplaintRecord.Agency;
      complaintRec.TrfFrom = $("#TrfFrom").val();
      complaintRec.TrfTo = $("#TrfTo").val();
      complaintRec.Status = $("#Status").val();
      complaintRec.Category = $("#CompCategory").val();
      complaintRec.ComplaintDate = $("#ComplaintDate").datepicker('getDate') != null ? $("#ComplaintDate").datepicker('getDate') : null;
      complaintRec.ComplaintSource = $("#Source").val();
      complaintRec.ComplaintExplain = $("#ComplaintExplain").val();
      complaintRec.Investigation = data.ComplaintRecord.Investigation;
      complaintRec.DepartmentAction = data.ComplaintRecord.DepartmentAction;
      complaintRec.ManagementAction = data.ComplaintRecord.ManagementAction;
      complaintRec.Active = data.ComplaintRecord.Active;
      complaintRec.DepartmentID = $("#DepartmentID").val();
      complaintRec.ReporterName = data.ComplaintRecord.ReporterName;
      complaintRec.Hotel = $("#Hotel").val();
      complaintRec.Type = $("#ComplaintType").val();


      params.Edit = formParams.Edit;
      params.ComplaintRec = complaintRec;
      $.ajax({
        type: "POST",
        data: JSON.stringify(params),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "ComplaintEdit.aspx/saveComplaintRecord",
        success: function (response) {
          if (response.hasOwnProperty('d') && response.d != null) {
            if (response.d == true) {
              window.close;
              self.parent.saveAndCloseComplaint();
            }
            else {
              alert('Not saved');
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            alert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function onCloseButtonClick() {
      window.close;
      self.parent.closeComplaint();
    }

    function viewImage(imgUrl) {
      $(".galleriaView").html('');
      $(".galleriaView").append('<img alt="" src="Cache/' + imgUrl + '" />');
    }

    function showGaleria(ImageList) {
      $(".galleria").html('');
      if (ImageList.length > 0) {
        $.each(ImageList, function (i) {
          $(".galleria").append('<img data-title="" data-description="" src="Cache/small_' + this + '" alt="" onclick="viewImage(\'' + this + '\')" />');
        });
      }
    }

    function onChangeType() {
      var params = new Object();
      params.CatType = $("#ComplaintType").val();
      $.ajax({
        type: "POST",
        data: JSON.stringify(params),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "ComplaintEdit.aspx/changeComplaintType",
        success: function (response) {
          if (response.hasOwnProperty('d') && response.d != null) {
            $("#ComplaintEditForm").data('CompCategory', response.d);
            $("#CompCategory").html('');
            if (response.d.length > 0) {
              $("#CompCategory").append('<option value="">-</option>')
              $.each(response.d, function (r) {
                $("#CompCategory").append('<option value="' + this.Code + '">' + this.Name + '</option>');
              });
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            alert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getFormData(recID, edit) {
      var params = new Object();
      params.RecID = recID == '0' ? null : parseInt(recID);
      params.Edit = edit == '1';
      $("#ComplaintEditForm").data('params', params);
      $.ajax({
        type: "POST",
        data: JSON.stringify(params),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "ComplaintEdit.aspx/getFormData",
        success: function (response) {
          if (response.hasOwnProperty('d') && response.d != null) {
            $("#ComplaintEditForm").data('data', response.d);
            if (response.d.ComplaintRecord.PasTrfID != null && response.d.ComplaintRecord.PasTrfID != '') {
              $("#PasTrfID").html(response.d.ComplaintRecord.PasTrfID);
            }
            if (response.d.ComplaintRecord.ResNo != null && response.d.ComplaintRecord.ResNo != '') {
              $("#ResNo").html(response.d.ComplaintRecord.ResNo);
            }
            if (response.d.ComplaintRecord.GroupNo != null && response.d.ComplaintRecord.GroupNo != '') {
              $("#GroupNo").html(response.d.ComplaintRecord.GroupNo);
            }
            $("#Hotel").html('');
            if (response.d.HotelList.length > 0) {
              $("#Hotel").append('<option value="">-</option>')
              $.each(response.d.HotelList, function (r) {
                $("#Hotel").append('<option value="' + this.Code + '">' + this.Name + '</option>');
              });
            }
            $("#Hotel").val(response.d.ComplaintRecord.Hotel)
            if (params.Edit) {
              $("#Hotel").prop("disabled", true);
            }
            $("#GuestName").html('');
            if (response.d.GuestList.length > 0) {
              $("#GuestName").append('<option value="">-</option>')
              $.each(response.d.GuestList, function (r) {
                $("#GuestName").append('<option value="' + this.Name + '">' + this.Name + '</option>');
              });
            }
            $("#GuestName").val(response.d.ComplaintRecord.GuestName);
            if (params.Edit) {
              $("#GuestName").prop("disabled", true);
            }
            $("#DepartmentID").html('');
            if (response.d.DepartmentList.length > 0) {
              $("#DepartmentID").append('<option value="">-</option>')
              $.each(response.d.DepartmentList, function (r) {
                $("#DepartmentID").append('<option value="' + this.Code + '">' + this.Name + '</option>');
              });
            }
            $("#DepartmentID").val(response.d.ComplaintRecord.DepartmentID);
            if (params.Edit) {
              $("#DepartmentID").prop("disabled", true);
            }
            $("#ComplaintEditForm").data('CompCategory', response.d.CategoryList);
            $("#CompCategory").html('');
            if (response.d.CategoryList.length > 0) {
              $("#CompCategory").append('<option value="">-</option>')
              $.each(response.d.CategoryList, function (r) {
                $("#CompCategory").append('<option value="' + this.Code + '">' + this.Name + '</option>');
              });
            }
            $("#CompCategory").val(response.d.ComplaintRecord.Category);
            if (params.Edit) {
              $("#CompCategory").prop("disabled", true);
            }
            $("#Source").html('');
            if (response.d.ReporterList.length > 0) {
              $("#Source").append('<option value="">-</option>')
              $.each(response.d.ReporterList, function (r) {
                $("#Source").append('<option value="' + this.Code + '">' + this.Name + '</option>');
              });
            }
            $("#Source").val(response.d.ComplaintRecord.ComplaintSource);
            if (params.Edit) {
              $("#Source").prop("disabled", true);
            }
            $("#ComplaintType").val(response.d.ComplaintRecord.Type);
            if (params.Edit) {
              $("#ComplaintType").prop("disabled", true);
            }
            $("#Status").html('');
            if (response.d.StatusList.length > 0) {
              $("#Status").append('<option value="">-</option>')
              $.each(response.d.StatusList, function (r) {
                $("#Status").append('<option value="' + this.Code + '">' + this.Name + '</option>');
              });
            }
            $("#Status").val(response.d.ComplaintRecord.Status);
            $("#Status").prop("disabled", true);
            $("#CompAmount").html(response.d.ComplaintRecord.CompAmount); //            
            $("#CompAmountCur").html(response.d.ComplaintRecord.CompAmountCur);
            $("#CompTo").val(response.d.ComplaintRecord.CompTo);
            $("#CompTo").prop("disabled", true);
            $("textarea#CompExplain").val(response.d.ComplaintRecord.CompExplain);
            $("#CompExplain").prop("disabled", true);
            var currentDateFormat = $.datepicker.regional[twoLetterISOLanguageName != 'en' ? twoLetterISOLanguageName : ''].dateFormat;
            if (response.d.ComplaintRecord.LastInformToGuest != null) {
              var lastInformToGuestDate = new Date(response.d.ComplaintRecord.LastInformToGuest.toString().match(/\d+/)[0] * 1);
              $("#LastInformToGuest").html(response.d.ComplaintRecord.LastInformToGuest.length > 0 ? $.datepicker.formatDate(currentDateFormat, lastInformToGuestDate) + ' ' + lastInformToGuestDate.toLocaleTimeString() : '');
            }
            $("#ReporterName").html(response.d.ComplaintRecord.ReporterName);

            if (response.d.ComplaintRecord.ComplaintDate != null) {
              var complaintDate = new Date(response.d.ComplaintRecord.ComplaintDate.toString().match(/\d+/)[0] * 1);
              $("#ComplaintDate").datepicker("setDate", complaintDate)
            }
            if (params.Edit) {
              $("#ComplaintDate").datepicker('disable');
            }
            $("#TrfFrom").html('');
            if (response.d.TrfListDep.length > 0) {
              $("#TrfFrom").append('<option value="">-</option>')
              $.each(response.d.TrfListDep, function (r) {
                $("#TrfFrom").append('<option value="' + this.Dep + '">' + this.DepName + '</option>');
              });
            }
            if (params.Edit) {
              $("#TrfFrom").prop("disabled", true);
            }
            $("#TrfFrom").val(response.d.ComplaintRecord.TrfFrom);
            $("#TrfTo").html('');
            if (response.d.TrfListArr.length > 0) {
              $("#TrfTo").append('<option value="">-</option>')
              $.each(response.d.TrfListArr, function (r) {
                $("#TrfTo").append('<option value="' + this.Arr + '">' + this.ArrName + '</option>');
              });
            }
            $("#TrfTo").val(response.d.ComplaintRecord.TrfTo);
            if (params.Edit) {
              $("#TrfTo").prop("disabled", true);
            }
            $("textarea#ComplaintExplain").val(response.d.ComplaintRecord.ComplaintExplain);
            if (params.Edit) {
              $("#ComplaintExplain").prop("disabled", true);
            }
            $("textarea#Investigation").val(response.d.ComplaintRecord.Investigation);
            $("#Investigation").prop("disabled", true);
            $("textarea#DepartmentAction").val(response.d.ComplaintRecord.DepartmentAction);
            $("#DepartmentAction").prop("disabled", true);
            $("textarea#ManagementAction").val(response.d.ComplaintRecord.ManagementAction);
            $("#ManagementAction").prop("disabled", true);
            if (response.d.ComplaintRecord.GuestRight != null) {
              $("#GuestRight").val(response.d.ComplaintRecord.GuestRight.toString());
            }
            $("#GuestRight").prop("disabled", true);
            if (response.d.ComplaintRecord.Active != null) {
              $("#Active").val(response.d.ComplaintRecord.Active.toString());
            }
            if (response.d.DisabledImageBtn) {
              $("#btnPicUpload").hide();
            } else {
              $("#btnPicUpload").show();
            }
            $("#Active").prop("disabled", true);
            if (params.Edit) {
              showGaleria(response.d.ImageList);
            } else {
              $("#attach").hide();
              $("#btnPicUpload").hide();
            }            
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            alert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    $(document).ready(function () {

      $.datepicker.setDefaults($.datepicker.regional[twoLetterISOLanguageName != 'en' ? twoLetterISOLanguageName : '']);

      $("#ComplaintEditForm").removeData('params');

      $.query = $.query.load(location.href);
      recID = $.query.get('RecID');
      edit = $.query.get('Edit');

      $("#ComplaintDate").datepicker({
        showOn: "button",
        buttonImage: "Images/Calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true
      });
      getFormData(recID, edit);

    });
  </script>

  <script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">
    <div class="rg-image-wrapper">
      {{if itemsCount > 1}}
					<div class="rg-image-nav">
            <a href="#" class="rg-image-nav-prev">Previous Image</a>
            <a href="#" class="rg-image-nav-next">Next Image</a>
          </div>
      {{/if}}
				<div class="rg-image"></div>
      <div class="rg-loading"></div>
      <div class="rg-caption-wrapper">
        <div class="rg-caption" style="display: none;">
          <p></p>
        </div>
      </div>
    </div>
  </script>
</head>
<body class="ui-helper-reset">
  <form id="ComplaintEditForm" runat="server">
    <div class="ui-helper-clearfix complaint">
      <div class="ui-helper-clearfix description">
        <div class="leftSide">
          <div class="ui-helper-clearfix transferID">
            <div class="leftSideLabel"><span>Transfer ID :</span></div>
            <div class="leftSideInput">
              <span id="PasTrfID"></span>
            </div>
          </div>
          <div class="ui-helper-clearfix groupNo">
            <div class="leftSideLabel"><span>Group No :</span></div>
            <div class="leftSideInput">
              <span id="GroupNo"></span>
            </div>
          </div>
          <div class="ui-helper-clearfix reservationNo">
            <div class="leftSideLabel"><span>Reservation No :</span></div>
            <div class="leftSideInput">
              <span id="ResNo"></span>
            </div>
          </div>
          <div class="ui-helper-clearfix guestName">
            <div class="leftSideLabel"><span>Guest Name :</span></div>
            <div class="leftSideInput">
              <select id="GuestName"></select>
            </div>
          </div>
          <div class="ui-helper-clearfix hotel">
            <div class="leftSideLabel"><span>Hotel :</span></div>
            <div class="leftSideInput">
              <select id="Hotel"></select>
            </div>
          </div>
          <div class="ui-helper-clearfix department">
            <div class="leftSideLabel"><span>Department :</span></div>
            <div class="leftSideInput">
              <select id="DepartmentID"></select>
            </div>
          </div>
          <div class="ui-helper-clearfix complaintType">
            <div class="leftSideLabel"><span>Type :</span></div>
            <div class="leftSideInput">
              <select id="ComplaintType" onchange="onChangeType()">
                <option value="">-</option>
                <option value="1">Complaint</option>
                <option value="0">Request</option>
                <option value="2">Other</option>
              </select>
            </div>
          </div>
          <div class="ui-helper-clearfix reqCompCode">
            <div class="leftSideLabel"><span>Request/Complaint Code :</span></div>
            <div class="leftSideInput">
              <select id="CompCategory"></select>
            </div>
          </div>
          <div class="ui-helper-clearfix source">
            <div class="leftSideLabel"><span>Source :</span></div>
            <div class="leftSideInput">
              <select id="Source"></select>
            </div>
          </div>
          <div class="ui-helper-clearfix status">
            <div class="leftSideLabel"><span>Status :</span></div>
            <div class="leftSideInput">
              <select id="Status"></select>
            </div>
          </div>
        </div>
        <div class="rightSide">
          <div class="ui-widget-header header">
            <span>Compansation</span>
          </div>
          <div class="ui-helper-clearfix compAmount">
            <div class="rightSideLabel"><span>Amount :</span></div>
            <div class="rightSideInput">
              <span id="CompAmount"></span>&nbsp;&nbsp;<span id="CompAmountCur"></span>
            </div>
          </div>
          <div class="ui-helper-clearfix compTo">
            <div class="rightSideLabel"><span>To :</span></div>
            <div class="rightSideInput">
              <select id="CompTo">
                <option value="">-</option>
                <option value="6">Supplier</option>
                <option value="0">Agency</option>
                <option value="1">Customer</option>
                <option value="2">Driver</option>
                <option value="3">Guide</option>
                <option value="4">Hotel</option>
                <option value="5">Operator</option>
              </select>
            </div>
          </div>
          <div class="ui-helper-clearfix compExplanation">
            <div class="rightSideLabel"><span>Explanation :</span></div>
            <div class="rightSideInput">
              <textarea id="CompExplain" rows="3" cols="35"></textarea>
            </div>
          </div>
          <div class="ui-helper-clearfix lastInformToGuest">
            <div class="rightSideLabel"><span>Last Inform To Guest :</span></div>
            <div class="rightSideInput">
              <span id="LastInformToGuest"></span>
            </div>
          </div>
          <div class="ui-helper-clearfix reporterName">
            <div class="rightSideLabel"><span>Reporter Name :</span></div>
            <div class="rightSideInput">
              <span id="ReporterName"></span>
            </div>
          </div>
          <div class="ui-helper-clearfix receivedDate">
            <div class="rightSideLabel"><span>Received Date :</span></div>
            <div class="rightSideInput">
              <input id="ComplaintDate" />
            </div>
          </div>
          <div class="ui-helper-clearfix transferFromTo">
            <div class="rightSideLabel"><span>Transfer From/To :</span></div>
            <div class="rightSideInput">
              <select id="TrfFrom"></select>&nbsp;&nbsp;->&nbsp;&nbsp;<select id="TrfTo"></select>
            </div>
          </div>
        </div>
      </div>
      <div class="ui-helper-clearfix operation">
        <div class="ui-helper-clearfix complaintExplain">
          <div class="operationLabel">
            <span>Explanation :</span>
          </div>
          <div class="operationInput">
            <textarea id="ComplaintExplain" rows="4" cols="90"></textarea>
          </div>
        </div>
        <div class="ui-helper-clearfix investigation">
          <div class="operationLabel">
            <span>Investigation :</span>
          </div>
          <div class="operationInput">
            <textarea id="Investigation" rows="4" cols="90"></textarea>
          </div>
        </div>
        <div class="ui-helper-clearfix departmentAction">
          <div class="operationLabel">
            <span>Department Action :</span>
          </div>
          <div class="operationInput">
            <textarea id="DepartmentAction" rows="4" cols="90"></textarea>
          </div>
        </div>
        <div class="ui-helper-clearfix managementAction">
          <div class="operationLabel">
            <span>Management Action :</span>
          </div>
          <div class="operationInput">
            <textarea id="ManagementAction" rows="4" cols="90"></textarea>
          </div>
        </div>
        <div class="ui-helper-clearfix rightAndActive">
          <div class="guestRightLabel">
            <span>Guest Right :</span>
          </div>
          <div class="guestRightInput">
            <select id="GuestRight">
              <option value="">-</option>
              <option value="false">UnFair</option>
              <option value="true">Right</option>
            </select>
          </div>
          <div class="activeLabel">
            <span>Active :</span>
          </div>
          <div class="activeInput">
            <select id="Active">
              <option value="">-</option>
              <option value="false">Closed</option>
              <option value="true">Pending</option>
            </select>
          </div>
        </div>
      </div>
      <div class="seperator"></div>
      <div class="buttons">
        <input id="btnPicUpload" type="button" value="Picture upload" onclick="onClickPictureUpload()" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="button" value="Save" onclick="onSaveButtonClick()" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="button" value="Close" onclick="onCloseButtonClick()" />
      </div>
      <div class="seperator"></div>
      <div class="attach">
        <div class="galleria">
        </div>        
        <div class="galleriaView">
        </div>
      </div>
    </div>
  </form>
  <div id="dialog-pictureUpload" title='Picture'
    style="display: none; text-align: center;">
    <iframe id="pictureUpload" height="300px" width="400px" frameborder="0"
      style="clear: both; text-align: left;"></iframe>
  </div>
</body>
</html>
