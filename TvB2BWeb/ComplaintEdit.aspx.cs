﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;

public partial class ComplaintEdit : BasePage
{
    public static string twoLetterISOLanguageName = "en";
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;

        List<string> lang = new List<string>() { "ar-DZ", "ar-DZ", "ar", "en-AU", "en-GB", "en-NZ", "fr-CH", "fr", "pt-BR", "pt", "sr-SR", "sr-CS", "sr", "zh-CN", "zh-HK", "zh-TW" };
        if (lang.Contains(UserData.Ci.Name))
            twoLetterISOLanguageName = UserData.Ci.Name;
        else
            twoLetterISOLanguageName = UserData.Ci.TwoLetterISOLanguageName != "en" ? UserData.Ci.TwoLetterISOLanguageName : "";
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData(int? RecID, bool? Edit)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        string errorMsg = string.Empty;

        ComplaintRecord complaint = new Complaints().getComplaint(UserData, RecID, ref errorMsg);
        ResDataRecord ResData = new ResDataRecord();
        if (complaint != null) {
            ResData = new ResTables().getReservationData(UserData, complaint.ResNo, null, ref errorMsg);
        } else {
            if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        }

        List<CompDepartment> departmentL = new Complaints().getComplaintDepartment(UserData, ref errorMsg);
        List<CompCategory> categoryL = new Complaints().getComplaintCategory(UserData, ref errorMsg);
        List<CompReporter> complaintReporterL = new Complaints().getComplaintReporter(UserData, ref errorMsg);
        List<CompStatus> complaintStatusL = new Complaints().getComplaintStatus(UserData, ref errorMsg);

        List<CodeName> hotelList = new List<CodeName>();
        var hotelGroup = from q in ResData.ResService
                         where q.ServiceType == "HOTEL"
                         group q by new { Hotel = q.Service } into k
                         select new { k.Key.Hotel };
        foreach (var row in hotelGroup) {
            HotelRecord hotel = new Hotels().getHotelDetail(UserData, row.Hotel, ref errorMsg);
            if (hotel != null)
                hotelList.Add(new CodeName() {
                    Code = hotel.RecID.ToString(),
                    Name = useLocalName ? hotel.LocalName : hotel.Name
                });
        }
        var transferListDep = from q in ResData.ResService
                              where q.ServiceType == "TRANSFER"
                              group q by new {
                                  Dep = q.DepLocation,
                                  DepName = useLocalName ? q.DepLocationNameL : q.DepLocationName,
                              } into k
                              select new {
                                  Dep = k.Key.Dep,
                                  DepName = k.Key.DepName,
                              };
        var transferListArr = from q in ResData.ResService
                              where q.ServiceType == "TRANSFER"
                              group q by new {
                                  Arr = q.ArrLocation,
                                  ArrName = useLocalName ? q.ArrLocationNameL : q.ArrLocationName
                              } into k
                              select new {
                                  Arr = k.Key.Arr,
                                  ArrName = k.Key.ArrName
                              };
        var departmentList = from q in departmentL
                             select new { Code = q.RecID, Name = useLocalName ? q.NameL : q.Name };
        var guestList = from q in ResData.ResCust
                        select new { CustNo = q.CustNo, Name = q.TitleStr + "." + q.Surname + " " + q.Name };
        var complaintReporterList = from q in complaintReporterL
                                    select new { Code = q.RecID, Name = useLocalName ? q.NameL : q.Name };
        var complaintStatusList = from q in complaintStatusL
                                  select new { Code = q.RecID, Name = useLocalName ? q.NameL : q.Name };
        var complaintCategoryList = from q in categoryL
                                    select new { Code = q.RecID, Name = useLocalName ? q.NameL : q.Name, CatType = q.CatType };
        List<string> imageList = new List<string>();

        #region Delete old images
        System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath("~/Cache"));
        System.IO.FileInfo[] deleteFiles = dir.GetFiles("*" + ResData.ResMain.ResNo + "_" + UserData.SID + "*.*");
        foreach (System.IO.FileInfo row in deleteFiles) {
            System.IO.File.Delete(row.FullName);
        }
        #endregion

        List<object> imgList = new Complaints().getImageList(UserData, RecID, ref errorMsg);
        if (imgList != null && imgList.Count > 0) {
            int seqNo = 0;
            foreach (object row in imgList) {
                seqNo++;
                string fileName = UICommon.WriteBinaryImageCache(row, "small_" + ResData.ResMain.ResNo + "_" + UserData.SID + "_compimg" + seqNo.ToString() + ".jpg", true, null, 65);
                fileName = UICommon.WriteBinaryImageCache(row, ResData.ResMain.ResNo + "_" + UserData.SID + "_compimg" + seqNo.ToString() + ".jpg", false, null, null);
                if (fileName != "")
                    imageList.Add(fileName);
            }
        }
        bool disabledImageBtn = imageList.Count >= 3;

        System.IO.FileInfo[] files = dir.GetFiles(ResData.ResMain.ResNo + "_" + UserData.SID + "*.*");
        if (complaint != null) {
            return new {
                NewRecord = false,
                ComplaintRecord = complaint,
                HotelList = hotelList,
                DepartmentList = departmentList,
                GuestList = guestList,
                CategoryList = complaintCategoryList,
                ReporterList = complaintReporterList,
                StatusList = complaintStatusList,
                TrfListDep = transferListDep,
                TrfListArr = transferListArr,
                ImageList = imageList,
                DisabledImageBtn = disabledImageBtn
            };
        } else {
            return new {
                NewRecord = true,
                ComplaintRecord = new ComplaintRecord() {
                    ResNo = ResData.ResMain.ResNo,
                    Agency = UserData.AgencyRec.RecID,
                    GroupNo = ResData.ResMain.GroupNo,
                    ReporterName = UserData.UserName,
                    Status = 1
                },
                HotelList = hotelList,
                DepartmentList = departmentList,
                GuestList = guestList,
                CategoryList = complaintCategoryList,
                ReporterList = complaintReporterList,
                StatusList = complaintStatusList,
                TrfListDep = transferListDep,
                TrfListArr = transferListArr,
                ImageList = imageList,
                DisabledImageBtn = disabledImageBtn
            };
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object saveComplaintRecord(bool? Edit, ComplaintRecord ComplaintRec)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        string errorMsg = string.Empty;

        ComplaintRecord complaint = new Complaints().getComplaint(UserData, ComplaintRec.RecID, ref errorMsg);
        ResDataRecord ResData = new ResDataRecord();
        if (complaint != null) {
            ResData = new ResTables().getReservationData(UserData, complaint.ResNo, null, ref errorMsg);
        } else {
            if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        }

        if (new Complaints().saveComplaint(UserData, Edit, ComplaintRec, ref errorMsg))
            return true;
        else
            return false;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object saveComplaintPicture(int? RecID, string FileName)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];

        if (string.IsNullOrEmpty(FileName))
            return false;

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        string errorMsg = string.Empty;

        ComplaintRecord complaint = new Complaints().getComplaint(UserData, RecID, ref errorMsg);
        ResDataRecord ResData = new ResDataRecord();
        if (complaint != null) {
            ResData = new ResTables().getReservationData(UserData, complaint.ResNo, null, ref errorMsg);
        } else {
            if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        }


        List<string> imageList = new List<string>();
        List<object> imgList = new Complaints().getImageList(UserData, RecID, ref errorMsg);
        if (imgList != null && imgList.Count > 0) {
            int seqNo = 0;
            foreach (object row in imgList) {
                seqNo++;
                string fileName = UICommon.WriteBinaryImageCache(row, "small_" + ResData.ResMain.ResNo + "_" + UserData.SID + "_compimg" + seqNo.ToString() + ".jpg", true, null, 65);
                fileName = UICommon.WriteBinaryImageCache(row, ResData.ResMain.ResNo + "_" + UserData.SID + "_compimg" + seqNo.ToString() + ".jpg", false, null, null);
                if (fileName != "")
                    imageList.Add(fileName);
            }
        }
        bool disabledImageBtn = imageList.Count >= 3;
        return new {
            saved = true,
            ImageList = imageList,
            DisabledImageBtn = disabledImageBtn
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object changeComplaintType(int? CatType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];

        string errorMsg = string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<CompCategory> categoryL = new Complaints().getComplaintCategory(UserData, ref errorMsg);
        var complaintCategoryList = from q in categoryL
                                    where !CatType.HasValue || q.CatType == CatType
                                    select new { Code = q.RecID, Name = useLocalName ? q.NameL : q.Name, CatType = q.CatType };

        return complaintCategoryList;
    }

}