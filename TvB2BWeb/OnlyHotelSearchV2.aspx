﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlyHotelSearchV2.aspx.cs" Inherits="OnlyHotelSearchV2"
    EnableEventValidation="false" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<%@ Register Src="Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="cache-control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "PackageSearch")%></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

    <script src="Scripts/jquery.dialogextend.js" type="text/javascript"></script>

    <script src="Scripts/jquery.printPage.js" type="text/javascript"></script>

    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="CSS/OnlyHotelSearchV2.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">

        var myWidth = 0, myHeight = 0;
        
        $(document).ajaxStart(function () {
            startWait();
        }).ajaxStop(function () {
            stopWait();            
        });

        function startWait() {
            $.blockUI({
                message: '<h3><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h3>',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .9,
                    color: '#fff',
                    'font-size': '15px',
                    'font-weight': 'bold'
                }
            });
        }


        function stopWait() {
            $.unblockUI();
        }

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function logout() {
            $('<div>' + '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>' + '</div>').dialog({
                autoOpen: true,
                position: {
                    my: 'center',
                    at: 'center'
                },
                modal: true,
                resizable: true,
                autoResize: true,
                bigframe: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                        $(this).dialog("close");
                        $(this).dialog("destroy");
                        window.location = 'Default.aspx';
                    }
                }
            });
            window.setTimeout(function () { window.location = "<%=VirtualPathUtility.ToAbsolute("~/Default.aspx")%>"; }, 10000);
        }

        function reSizeFilterFrame(h) {
            $('#filterFrame').height((h + 60) + 'px');
        }

        function reSizeResultFrame(h) {
            var height = parseInt(h);
            if (height > 0) {
                $('#resultFrame').height((height + 20) + 'px');
            } else {
                $('#resultFrame').height(600 + 'px');
            }
        }

        function showDialog(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    autoOpen: true,
                    position: {
                        my: 'center',
                        at: 'center'
                    },
                    modal: true,
                    resizable: true,
                    autoResize: false,
                    bigframe: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        var NS = document.all;

        function maximize() {
            var myWidth = screen.availWidth;
            var myHeight = screen.availHeight;
            if (location.href.indexOf('pic') == -1) {
                if (window.opera) { } else {
                    top.window.moveTo(0, 0);
                    if (document.all) { top.window.resizeTo(myWidth, myHeight); }
                    else
                        if (document.layers || document.getElementById) {
                            if ((top.window.outerHeight < myHeight) || (top.window.outerWidth < myWidth)) {
                                top.window.outerHeight = myWidth + 'px';
                                top.window.outerWidth = myHeight + 'px';
                            }
                        }
                }
            }
        }

        function searchPrice() {
            window.scrollTo(0, 0);
            $("#resultFrame").attr("src", "OnlyHotelSearchResultV2.aspx");
        }

        function viewReportAddButton(btnName, func) {
            $.extend($.ui.dialog.prototype, {
                'addbutton': function (buttonName, func) {
                    var buttons = this.element.dialog('option', 'buttons');
                    buttons[buttonName] = func;
                    this.element.dialog('option', 'buttons', buttons);
                }
            });
            $("#dialog-viewReport").dialog('addbutton', btnName, func);
        }

        function viewReportRemoveButton(btnName) {
            $.extend($.ui.dialog.prototype, {
                'removebutton': function (buttonName) {
                    var buttons = this.element.dialog('option', 'buttons');
                    delete buttons[buttonName];
                    this.element.dialog('option', 'buttons', buttons);
                }
            });
            $("#dialog-viewReport").dialog('removebutton', btnName);
        }

        function printOffer(url, width, height) {
            if ($.browser.msie) {
                var windowSizeArray = ["width=" + width + ",height=" + height + ",scrollbars=yes"];
                var windowName = "popUp";
                var windowSize = windowSizeArray[0];
                window.open(url, windowName, windowSize);
            }
            else if ($.browser.chrome || $.browser.safari) {
                $("#viewReport").width(width + '32px');
                $("#viewReport").height(height + '32px');
                id = 'viewReport';
                var iframe = document.frames ? document.frames[id] : document.getElementById(id);
                var ifWin = iframe.contentWindow || iframe;
                iframe.focus();
                ifWin.print();
                return false;
            }
            else {
                $("#viewReport").width(width + 32);
                $("#viewReport").height(height + 32);
                //$("#viewReport").contents().find(".mainPage").printPage($("#viewReport").contents().find('style'), width);
                document.getElementById('viewReport').focus();
                document.getElementById('viewReport').contentWindow.print();

            }
        }

        function createOffer() {
            window.scrollTo(0, 0);
            var htmlUrl = 'CreateOffer.aspx';
            var DocName = 'Offer';
            $("#viewReport").removeAttr("src");
            $("#viewReport").attr("src", htmlUrl);
            $("#dialog-viewReport").dialog(
            {
                autoOpen: true,
                modal: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnPrint") %>': function () {
                        var width = $("#viewReport").contents().find(".mainPage").width();
                        var height = $("#viewReport").contents().find(".mainPage").height();
                        var param1 = $("#viewReport").contents().find("#param1").val();
                        printOffer(htmlUrl + "?docName=" + DocName + "&print=1" + (param1 != undefined && param1 != '' ? "&param1=" + param1 : ""), width, height);
                    },
                    '<%= GetGlobalResourceObject("LibraryResource", "btnPDF") %>': function () {
                        //viewPDFReport
                        var width = $("#viewReport").contents().find(".mainPage").width();
                        var _html = $("#viewReport").contents().find("html").html();
                        var noPrint = $("#viewReport").contents().find("#noPrintDiv").html();
                        if (noPrint != null)
                            _html = _html.replace(noPrint, '&nbsp;');
                        _html = _html.replace(/noPrint/g, 'noPDF');
                        _html = _html.replace(/"/g, '|');
                        $.ajax({
                            type: "POST",
                            url: "OnlyHotelSearchV2.aspx/viewPDFReport",
                            data: '{"reportType":"' + DocName + '","_html":"' + _html + '","pageWidth":"' + width + '","urlBase":"' + htmlUrl + '"}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (msg) {
                                if (msg.d != null) {
                                    $("#viewReport").removeAttr("src");
                                    var _pdfViewPage = "ViewPDF.aspx"
                                    $("#viewReport").attr("src", _pdfViewPage + "?url=" + msg.d);
                                    viewReportRemoveButton('<%= GetGlobalResourceObject("LibraryResource", "btnPrint") %>');
                                    viewReportRemoveButton('<%= GetGlobalResourceObject("LibraryResource", "btnPDF") %>');
                                }
                            },
                            error: function (xhr, msg, e) {
                                if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                    showDialog(xhr.responseText);
                            },
                            statusCode: {
                                408: function () {
                                    logout();
                                }
                            }
                        });
                        return false;
                    },
                    '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>': function () {
                        $(this).dialog('close');
                        return true;
                    }
                }
            }).dialogExtend({
                "maximize": true,
                "icons": {
                    "maximize": "ui-icon-circle-plus",
                    "restore": "ui-icon-pause"
                }
            });
            $("#dialog-viewReport").dialogExtend("maximize");
        }

        function blockUIWait() {
            $("#dialog-CreateReservation").dialog('open');
        }

        function unBlockUIWait() {
            $("#dialog-CreateReservation").dialog('close');
        }

        function bookReservation(bookRoomList) {
            $.ajax({
                async: false,
                type: "POST",
                data: '{"BookList":"' + bookRoomList + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: "OnlyHotelSearchV2.aspx/getBookReservation",
                success: function (msg) {
                    unBlockUIWait();
                    if (msg.d != null && msg.d.resOK) {
                        window.scrollTo(0, 0);
                        $('html').css('overflow', 'hidden');
                        $("#MakeReservation").attr("src", $("#basePageUrl").val() + 'MakeReservation' + msg.d.version + '.aspx');
                        $("#dialog-MakeReservation").dialog(
                            {
                                autoOpen: true,
                                modal: true,
                                width: 990,
                                height: 700,
                                resizable: true,
                                autoResize: true,
                                bigframe: true,
                                close: function (event, ui) { $('html').css('overflow', 'auto'); }
                            }).dialogExtend({
                                "maximize": true,
                                "icons": {
                                    "maximize": "ui-icon-circle-plus",
                                    "restore": "ui-icon-pause"
                                }
                            });
                        $("#dialog-MakeReservation").dialogExtend("maximize");
                        return true;
                    }
                    else {
                        showDialog(msg.d != null ? msg.d.errMsg : 'unknown error.');
                        return false;
                    }

                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                    unBlockUIWait();
                    return false;
                },
                statusCode: {
                    408: function () {
                        unBlockUIWait();
                        logout();
                    }
                }
            });
        }

        function bookRooms(bookRoomList) {
            $.ajax({
                async: false,
                type: "POST",
                data: '{"BookList":"' + bookRoomList + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: "OnlyHotelSearchV2.aspx/getAdvertising",
                success: function (msg) {
                    if (msg.d != null && msg.d != '') {
                        window.scrollTo(0, 0);
                        $('html').css('overflow', 'hidden');
                        $("#Advertising").attr("src", msg.d);
                        $("#dialog-Advertising").dialog(
                            {
                                autoOpen: true,
                                modal: true,
                                width: 990,
                                height: 700,
                                resizable: true,
                                autoResize: true,
                                bigframe: true,
                                close: function (event, ui) {
                                    $('html').css('overflow', 'auto');
                                    blockUIWait();
                                    setTimeout(function () {
                                        bookReservation(bookRoomList);
                                    }, 1000);
                                },
                                buttons: {
                                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                                    $(this).dialog('close');
                                }
                                }
                            }).dialogExtend({
                                "maximize": true,
                                "icons": {
                                    "maximize": "ui-icon-circle-plus",
                                    "restore": "ui-icon-pause"
                                }
                            });
                        $("#dialog-Advertising").dialogExtend("maximize");
                        return true;
                    }
                    else {
                        blockUIWait();
                        setTimeout(function () { bookReservation(bookRoomList); }, 1000);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                    return false;
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function cancelMakeRes() {
            $("#MakeReservation").attr("src", "");
            $("#dialog-MakeReservation").dialog("close");
            $('html').css('overflow', 'auto');
        }

        function gotoResViewPage(ResNo) {
            $("#MakeReservation").attr("src", "");
            $("#dialog-MakeReservation").dialog("close");
            $('html').css('overflow', 'auto');
            window.location = 'ResView.aspx?ResNo=' + ResNo;
        }

        function paymentPage(ResNo) {
            $("#MakeReservation").attr("src", "");
            $("#dialog-MakeReservation").dialog("close");
            window.open('Payments/BeginPayment.aspx?ResNo=' + ResNo, '_blank');
        }

        function showBrochure(holPack, CheckIn, CheckOut, Market) {
            var obj = new Object();
            obj.holPack = holPack;
            obj.CheckIn = CheckIn;
            obj.CheckOut = CheckOut;
            obj.Market = Market;
            $.ajax({
                type: "POST",
                data: $.json.encode(obj),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: "OnlyHotelSearchV2.aspx/getBrochure",
                success: function (msg) {
                    if (msg.d != "") {
                        window.scrollTo(0, 0);
                        $('html').css('overflow', 'hidden');
                        $("#viewReport").removeAttr("src");
                        var _pdfViewPage = "ViewPDF.aspx"
                        $("#viewReport").attr("src", _pdfViewPage + "?url=" + msg.d);
                        var width = $('body').width() - 50;
                        $("#viewReport").width(width);
                        $("#dialog-viewReport").dialog(
                            {
                                autoOpen: true,
                                modal: true,
                                resizable: true,
                                autoResize: true,
                                bigframe: true,
                                buttons: {
                                    '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>': function () {
                                        $(this).dialog('close');
                                        return true;
                                    }
                                },
                                close: function (event, ui) { $('html').css('overflow', 'auto'); }
                            }).dialogExtend({
                                "maximize": true,
                                "icons": {
                                    "maximize": "ui-icon-circle-plus",
                                    "restore": "ui-icon-pause"
                                }
                            });
                            $("#dialog-viewReport").dialogExtend("maximize");
                            return true;
                        }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                    return false;
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
            }

            function reSearch(date) {
                var iframe = document.getElementById("filterFrame");
                iframe.contentWindow.reSearch(date);
            }

            function offers() {
                $.ajax({
                    async: false,
                    type: "POST",
                    data: '{}',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    url: "OnlyHotelSearchV2.aspx/getOffersUrl",
                    success: function (msg) {
                        if (msg.d != null && msg.d != '') {
                            $("#resultFrame").attr("src", "http://" + msg.d);
                            $("#resultFrame").height(800);
                        }
                    },
                    error: function (xhr, msg, e) {
                        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                            alert(xhr.responseText);
                        return false;
                    },
                    statusCode: {
                        408: function () {
                            logout();
                        }
                    }
                });
            }

            $(document).ready(function () {                
                maximize();
                $("#resultFrame").removeAttr("src");
                $("#filterFrame").removeAttr("src");
                $("#filterFrame").attr("src", "OnlyHotelSearchFilterV2.aspx");
                $("#dialog-CreateReservation").dialog({ autoOpen: false, autoResize: true });
                offers();
            });

            function pageReLoad() {
                window.location = window.location.href;
            }

    </script>

</head>
<body class="ui-helper-reset">
    <form id="PackageSearchForm" runat="server">
        <input id="b2bMenuCat" type="hidden" value="" />
        <div class="Page">
            <div>
                <tv1:Header ID="tvHeader" runat="server" />
                <tv1:MainMenu ID="tvMenu" runat="server" />
            </div>
            <div class="Content">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td valign="top" style="width: 25%;">
                            <iframe id="filterFrame" width="100%" frameborder="0"></iframe>
                        </td>
                        <td valign="top" style="width: 75%;">
                            <div id="divWorkAreaSearch">
                                <div id="divResult">
                                    <div id="divFilterResult" class="iframe">
                                        <iframe id="resultFrame" width="100%" frameborder="0" style="clear: both; min-height: 400px;"></iframe>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="Footer">
                <tv1:Footer ID="tvfooter" runat="server" />
            </div>
        </div>
        <div id="dialog-MakeReservation" title='<%= GetGlobalResourceObject("MakeReservation", "lblMakeReservation") %>'
            style="display: none; text-align: center;">
            <iframe id="MakeReservation" runat="server" height="100%" width="960px" frameborder="0"
                style="clear: both; text-align: left;"></iframe>
        </div>
        <div id="dialog-Advertising" title=''
            style="display: none; text-align: center;">
            <iframe id="Advertising" runat="server" height="100%" width="960px" frameborder="0"
                style="clear: both; text-align: left;"></iframe>
        </div>
        <%--Message, Confirm Area--%>
        <div id="dialog-message" title="" style="display: none;">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span><span id="messages">Message</span>
            </p>
        </div>
        <%--Message, Confirm Area--%>
        <div id="dialog-viewReport" title='' style="display: none; text-align: center;">
            <iframe id="viewReport" runat="server" height="100%" width="700px" frameborder="0"
                style="clear: both; text-align: left;"></iframe>
        </div>
        <div id="dialog-CreateReservation" title=''
            style="display: none; text-align: center;">
            <h3 style="color: #000;">
                <%= GetGlobalResourceObject("MakeReservation", "PleaseWaitCreateRes")%></h3>
        </div>
    </form>
</body>
</html>
