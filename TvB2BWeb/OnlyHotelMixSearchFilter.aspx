﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlyHotelMixSearchFilter.aspx.cs"
  Inherits="OnlyHotelMixSearchFilter" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    <%= GetGlobalResourceObject("PageTitle", "OnlyHotelSearchFilter") %></title>

  <script src="Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery.datepick/jquery-ui-i18n.js" type="text/javascript"></script>
  <script src="Scripts/jquery.json.js" type="text/javascript"></script>
  <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
  <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
  <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>
  <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>
  <script src="Scripts/Tv.PackageSearchFilterV2.js" type="text/javascript"></script>

  <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="CSS/OnlyHotelMixSearchFilter.css?v=2" rel="stylesheet" type="text/css" />

  <style type="text/css">
    .special_day { background-color: #00CC00 !important; color: #222222 !important; background-image: none !important; }
      .special_day a { background-color: #00CC00 !important; color: #222222 !important; background-image: none !important; }
  </style>

  <script language="javascript" type="text/javascript">

    var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
    var ComboSelect = '<%= GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
    var ComboAll = '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>';
    var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
    var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
    var btnClear = '<%= GetGlobalResourceObject("LibraryResource", "btnClear") %>';
    var addPleaseSelectDeparture = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectDeparture") %>';
    var lblDay = '<%= GetGlobalResourceObject("LibraryResource", "lblDay") %>';
    var addPleaseSelectPackage = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectPackage") %>';
    var addPleaseSelectCountry = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectCountry") %>';
    var addPleaseSelectArrival = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectArrival") %>';
    var lblMainCountries = '<%= GetGlobalResourceObject("OnlyHotelMix", "lblMainCountries") %>';
    var lblOtherCountries = '<%= GetGlobalResourceObject("OnlyHotelMix", "lblOtherCountries") %>';
    var lblTopRegion = '<%= GetGlobalResourceObject("OnlyHotelMix", "lblTopRegion") %>';
    var lblCity = '<%= GetGlobalResourceObject("OnlyHotelMix", "lblCity") %>';
    var lblSelectNationality = '<%= GetGlobalResourceObject("OnlyHotelMix", "lblSelectNationality") %>';
    var lblTotalPaxNumberGT = '<%= GetGlobalResourceObject("OnlyHotelMix", "lblTotalPaxNumberGT") %>';

    var dayColor = [];
    var countryList = [];

    if (typeof window.event != 'undefined')
      document.onkeydown = function () {
        if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
          return (event.keyCode != 8);
      }
    else
      document.onkeypress = function (e) {
        if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
          return (e.keyCode != 8);
      }

    function logout() {
      self.parent.logout();
    }

    var myWidth = 0, myHeight = 0;

    $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '</h1>';
    $(document)
        .ajaxStart($.blockUI)
        .ajaxStop($.unblockUI);

    function reSizeFrame() {
      self.parent.reSizeFilterFrame($('body').height());
    }

    function showDialogYesNo(msg) {
      $("#messages").html(msg);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        modal: true,
        buttons: [{
          width: 240,
          text: btnOK,
          click: function () {
            $(this).dialog('close');
            return true;
          }
        }, {
          text: btnCancel,
          click: function () {
            $(this).dialog('close');
            return false;
          }
        }]
      });
    }

    function showDialog(msg) {
      $("#messages").html(msg);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        width: 240,
        modal: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog('close');
          }
        }]
      });
    }

    function showMessage(msg, transfer, trfUrl) {
      $("#messages").html(msg);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        width: 240,
        modal: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog('close');
          }
        }, {
          text: btnCancel,
          click: function () {
            $(this).dialog('close');
          }
        }]
      });
    }

    function getInf(adult, whois) {
      var Adult = parseInt(adult);
      $("#fltInfant" + whois).html("");
      for (i = 0; i < Adult + 1; i++) {
        $("#fltInfant" + whois).append("<option value='" + i.toString() + "'>" + i.toString() + "</option>");
      }
    }

    function SetChild(childCount, whois) {
      if (childCount > 0) $("#divRoomInfoChd1" + whois).show(); else $("#divRoomInfoChd1" + whois).hide();
      if (childCount > 1) $("#divRoomInfoChd2" + whois).show(); else $("#divRoomInfoChd2" + whois).hide();
      if (childCount > 2) $("#divRoomInfoChd3" + whois).show(); else $("#divRoomInfoChd3" + whois).hide();
      if (childCount > 3) $("#divRoomInfoChd4" + whois).show(); else $("#divRoomInfoChd4" + whois).hide();
    }

    function getChild(child, whois) {
      var _divChild = $("#divRoomInfoChd" + whois);
      if (parseInt(child) == 0) {
        SetChild(parseInt(child), whois);
        _divChild.hide();
      }
      else {
        _divChild.show();
        SetChild(parseInt(child), whois);
      }
    }

    function onChildChange(whois) {
      getChild($("#fltChild" + whois).val(), whois);
      reSizeFrame();
    }

    function getRoomPaxs() {
      var roomCount = parseInt($("#fltRoomCount").val());
      var dataList = [];
      var result = '';
      for (i = 1; i < roomCount + 1; i++) {
        var adult = parseInt($("#fltAdult" + i.toString()).val());
        var child = parseInt($("#fltChild" + i.toString()).val());
        var chd1Age = parseInt($("#fltRoomInfoChd1" + i.toString()).val());
        var chd2Age = parseInt($("#fltRoomInfoChd2" + i.toString()).val());
        var chd3Age = parseInt($("#fltRoomInfoChd3" + i.toString()).val());
        var chd4Age = parseInt($("#fltRoomInfoChd4" + i.toString()).val());
        if (i > 1) {
          result += ',(';
        }
        else {
          result += '(';
        }
        result += '|Adult|:|' + adult.toString() + '|';
        result += ',|Child|:|' + child.toString() + '|';
        result += ',|Chd1Age|:|' + (child > 0 ? chd1Age : -1) + '|';
        result += ',|Chd2Age|:|' + (child > 1 ? chd2Age : -1) + '|';
        result += ',|Chd3Age|:|' + (child > 2 ? chd3Age : -1) + '|';
        result += ',|Chd4Age|:|' + (child > 3 ? chd4Age : -1) + '|';
        result += ')';
        var obj = new Object();
        obj.Adult = adult;
        obj.Child = child;
        obj.Chd1Age = (child > 0 ? chd1Age : null);
        obj.Chd2Age = (child > 1 ? chd2Age : null);
        obj.Chd3Age = (child > 2 ? chd3Age : null);
        obj.Chd4Age = (child > 3 ? chd4Age : null);
        dataList.push(obj);
      }
      var hfRoomInfo = $("#hfRoomInfo");
      $("#hfRoomInfo").data("roomInfo", dataList);
      hfRoomInfo.val(result);
      reSizeFrame();
    }

    function getArrival() {
      var params = new Object();
      params.Country = $("#hfCountry").val().split('|').length > 1 ? $("#hfCountry").val().split('|')[0].toString() : '';
      $.ajax({
        type: "POST",
        url: "OnlyHotelMixSearchFilter.aspx/getArrival",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#fltArrCity").html("");
          $("#fltArrCity").append("<option value='' >" + ComboAll + "</option>");
          var country = '';
          var first = true;
          $.each($.json.decode(msg.d), function (i) {
            if (this.Country != country) {
              if (!first) {
                $("#fltArrCity").append("</optgroup>");
              }
              $("#fltArrCity").append("<optgroup label='" + this.Country + "'>");
              first = false;
              country = this.Country;
            }
            $("#fltArrCity").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
          });
          if ($("#fltArrCity").html() != '') {
            $("#fltArrCity").append("</optgroup>");
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            alert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function changeArrCity() {
      $("#fltHolpack").html(''); $("#hfHolPack").val('');
      $("#txtResort").html(''); $("#hfResort").val('');
      $("#txtCategory").html(''); $("#hfCategory").val('');
      $("#txtHotel").html(''); $("#hfHotel").val('');
      $("#txtRoom").html(''); $("#hfRoom").val('');
      $("#txtBoard").html(''); $("#hfBoard").val('');
      $("#divListPopup").html('');

      getHolpack();
      setCheckIn();
    }

    function getHolpack() {
      var countryID = $("#hfCountry").val() != "" ? ($("#hfCountry").val().split('|').length > 0 ? $("#hfCountry").val().split('|')[0] : $("#hfCountry").val()) : "";
      var params = new Object();
      params.CountryId = countryID;
      params.DepCity = '';
      params.ArrCity = $("#fltArrCity").val();
      $.ajax({
        type: "POST",
        url: "OnlyHotelMixSearchFilter.aspx/getHolpack",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#fltHolpack").html("");
          $("#fltHolpack").append("<option value=';;' >" + ComboSelect + "</option>");
          if (msg.hasOwnProperty('d') && msg.d != '') {
            $.each($.json.decode(msg.d), function (i) {
              $("#fltHolpack").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            alert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function changeHolpack() {
      var holpackCode = $("#fltHolpack").val();
      $("#fltArrCity").val(holpackCode.split(';')[2]);

      $("#txtHotel").html(''); $("#hfHotel").val('');
      $("#txtRoom").html(''); $("#hfRoom").val('');
      $("#txtBoard").html(''); $("#hfBoard").val('');
      $("#hfHolPack").val(holpackCode.split(';')[0]);
      setCheckIn();
    }

    String.prototype.ltrim = function () {
      return this.replace(/^\s+/, "");
    }

    function autoCompletteLocChange(key) {
      var autoCList = $('.countryAutoComp');
      $.each(autoCList, function (i) {
        if (key != '') {
          if ($.browser.msie) {
            if (this.innerText.ltrim().toLowerCase().indexOf(key.toLowerCase()) > -1) {
              $(this).show();
            }
            else {
              $(this).hide();
            }
          }
          else {
            if (this.textContent.ltrim().toLowerCase().indexOf(key.toLowerCase()) > -1) {
              $(this).show();
            }
            else {
              $(this).hide();
            }
          }
        } else {
          $(this).show();
        }
      });
    }

    function autoCompletteChange(key) {
      var autoCList = $('div[name="listPopupRow"]');
      $.each(autoCList, function (i) {
        if (key != '') {
          if ($.browser.msie) {
            if (this.innerText.ltrim().toLowerCase().indexOf(key.toLowerCase()) > -1) {
              $(this).show();
            }
            else {
              $(this).hide();
            }
          }
          else {
            if (this.textContent.ltrim().toLowerCase().indexOf(key.toLowerCase()) > -1) {
              $(this).show();
            }
            else {
              $(this).hide();
            }
          }
        } else {
          $(this).show();
        }
      });
    }

    function selectCountry(_this, value) {
      var hfCountry = $("#hfCountry");
      var iCountry = $("#fltCountry");
      hfCountry.val(value);
      iCountry.val($(_this).html());
      $("#dialog-listPopup").dialog('close');
      swichPaximumFilter(hfCountry.val());
    }

    function selectRegion(_this, value) {
      var hfLocation = $("#hfPaxLocation");
      var iLocation = $("#fltPaxLocation");
      hfLocation.val(value);
      iLocation.val($(_this).html());
      $("#dialog-listPopup").dialog('close');
    }

    function listPopupFillData(sourceBtn, clear, _title) {
      var depCity = '';
      var arrCity = $("#fltArrCity").val();
      var resort = $("#hfResort").val();
      var category = $("#hfCategory").val();
      var hotel = $("#hfHotel").val();
      var room = $("#hfRoom").val();
      var board = $("#hfBoard").val();
      var holpack = $("#fltHolpack").val() != null || $("#fltHolpack").val() != undefined ? $("#fltHolpack").val().split(';')[0] : "";
      var countryID = $("#hfCountry").val() != "" ? ($("#hfCountry").val().split('|').length > 0 ? $("#hfCountry").val().split('|')[0] : $("#hfCountry").val()) : "";
      var params = new Object();
      var serviceUrl = '';
      switch (sourceBtn) {
        case 'country':
          serviceUrl = 'OnlyHotelMixSearchFilter.aspx/getCountry';
          params = new Object();
          $.ajax({
            type: "POST",
            data: $.json.encode(params),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            url: serviceUrl,
            success: function (msg) {
              $("#fltArrCity").html('');
              $("#txtCategory").html(''); $("#hfCategory").val('');
              $("#txtHotel").html(''); $("#hfHotel").val('');
              $("#txtRoom").html(''); $("#hfRoom").val('');
              $("#txtBoard").html(''); $("#hfBoard").val('');
              var _input = '';
              var _inputMain = '';
              var _inputOther = '';
              if (msg.hasOwnProperty('d') && msg.d != null) {
                var mydata = msg.d;
                _input += '<input type="text" id="countryAutoComplette"  onkeyup="autoCompletteLocChange(this.value);" ><br />';
                $.each(mydata, function (index, inputData) {
                  if (inputData.CountryType == '3') { // All
                    _inputMain += "<div class='countryAutoComp countryAutoComp1' onclick='selectCountry(this,\"" + inputData.Code + "|1\")'>" + inputData.Name + "</div>";
                    _inputOther += "<div class='countryAutoComp countryAutoComp2' onclick='selectCountry(this,\"" + inputData.Code + "|2\")'>" + inputData.Name + "</div>";
                  } else if (inputData.CountryType == '1') { //TourVisio
                    _inputMain += "<div class='countryAutoComp countryAutoComp1' onclick='selectCountry(this,\"" + inputData.Code + "|1\")'>" + inputData.Name + "</div>";
                  } else { // Other                                        
                    _inputOther += "<div class='countryAutoComp countryAutoComp2' onclick='selectCountry(this,\"" + inputData.Code + "|2\")'>" + inputData.Name + "</div>";
                  }
                });
                _input += '<div class="divListPopupRow mainCountries"><h4>' + lblMainCountries + '</h4></div>';
                _input += _inputMain;
                _input += '<div class="divListPopupRow otherCountries"><h4>' + lblOtherCountries + '</h4></div>';
                _input += _inputOther;
              }
              $("#divListPopup").html('');
              $("#divListPopup").html(_input);
              if (mydata != null) { $("#countryAutoComplette").focus(); }
              showListPopupRun(_title, sourceBtn);
            },
            error: function (xhr, msg, e) {
              if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                alert(xhr.responseText);
              }
            },
            statusCode: {
              408: function () {
                logout();
              }
            }
          });
          break;
        case 'paxlocation':
          serviceUrl = 'OnlyHotelMixSearchFilter.aspx/getPaxLocation';
          params = new Object();
          params.CountryId = countryID;
          $.ajax({
            type: "POST",
            data: $.json.encode(params),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            url: serviceUrl,
            success: function (msg) {
              $("#fltArrCity").html('');
              $("#txtCategory").html(''); $("#hfCategory").val('');
              $("#txtHotel").html(''); $("#hfHotel").val('');
              $("#txtRoom").html(''); $("#hfRoom").val('');
              $("#txtBoard").html(''); $("#hfBoard").val('');
              var _input = '';
              var _inputMain = '';
              var _inputOther = '';
              if (msg.hasOwnProperty('d') && msg.d != '') {
                var mydata = msg.d;
                _input += '<input type="text" id="countryAutoComplette"  onkeyup="autoCompletteLocChange(this.value);" ><br />';
                $.each(mydata, function (index, inputData) {
                  if (inputData.TopRegion == '1') { //TourVisio
                    _inputMain += "<div class='countryAutoComp countryAutoComp1' onclick='selectRegion(this,\"" + inputData.Code + "\")'>" + inputData.Name + "</div>";
                  } else {
                    _inputOther += "<div class='countryAutoComp countryAutoComp2' onclick='selectRegion(this,\"" + inputData.Code + "\")'>" + inputData.Name + "</div>";
                  }
                });
                _input += '<div class="divListPopupRow mainCountries"><h4>' + lblTopRegion + '</h4></div>';
                _input += _inputMain;
                _input += '<div class="divListPopupRow otherCountries"><h4>' + lblCity + '</h4></div>';
                _input += _inputOther;
              }
              $("#divListPopup").html('');
              $("#divListPopup").html(_input);
              if (mydata != null) { $("#arrCityAutoComplette").focus(); }
              showListPopupRun(_title, sourceBtn);
            },
            error: function (xhr, msg, e) {
              if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                alert(xhr.responseText);
              }
            },
            statusCode: {
              408: function () {
                logout();
              }
            }
          });
          break;
        case 'resort':
          serviceUrl = 'OnlyHotelMixSearchFilter.aspx/getResourtData';
          params = new Object();
          params.ArrCity = arrCity;
          params.CountryId = countryID;
          $.ajax({
            type: "POST",
            data: $.json.encode(params),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            url: serviceUrl,
            success: function (msg) {
              $("#txtCategory").html(''); $("#hfCategory").val('');
              $("#txtHotel").html(''); $("#hfHotel").val('');
              $("#txtRoom").html(''); $("#hfRoom").val('');
              $("#txtBoard").html(''); $("#hfBoard").val('');
              var inputDataArray = msg.d;
              var listChk = $("#hfResort");
              var _input = '';
              var mydata = $.json.decode(inputDataArray)
              $.each(mydata, function (index, inputData) {
                var checked = '';
                if (listChk.val().length > 0) {
                  var chkListStr = listChk.val();
                  var chkList = chkListStr.split('|');
                  if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.RecID.toString()) > 0) {
                    checked = 'checked=\'checked\'';
                  }
                  else {
                    checked = '';
                  }
                } else {
                  checked = '';
                }
                _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.RecID + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
              });
              $("#divListPopup").html('');
              $("#divListPopup").html(_input);
              showListPopupRun(_title, sourceBtn);
            },
            error: function (xhr, msg, e) {
              if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                alert(xhr.responseText);
              }
            },
            statusCode: {
              408: function () {
                logout();
              }
            }
          });
          break;
        case 'category':
          serviceUrl = 'OnlyHotelMixSearchFilter.aspx/getCategoryData';
          params = new Object();
          params.CountryId = countryID;
          params.ArrCity = arrCity;
          params.Resort = resort;
          $.ajax({
            type: "POST",
            data: $.json.encode(params),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            url: serviceUrl,
            success: function (msg) {
              $("#txtCategory").html(''); $("#hfCategory").val('');
              $("#txtHotel").html(''); $("#hfHotel").val('');
              $("#txtRoom").html(''); $("#hfRoom").val('');
              $("#txtBoard").html(''); $("#hfBoard").val('');
              var inputDataArray = msg.d;
              var listChk = $("#hfCategory");
              var _input = '';
              var mydata = $.json.decode(inputDataArray)
              $.each(mydata, function (index, inputData) {
                var checked = '';
                if (listChk.val().length > 0) {
                  var chkListStr = listChk.val();
                  var chkList = chkListStr.split('|');
                  if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                    checked = 'checked=\'checked\'';
                  else checked = '';
                } else checked = '';
                _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
              });
              $("#divListPopup").html('');
              $("#divListPopup").html(_input);
              showListPopupRun(_title, sourceBtn);
            },
            error: function (xhr, msg, e) {
              if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                alert(xhr.responseText);
            },
            statusCode: {
              408: function () {
                logout();
              }
            }
          });
          break;
        case 'hotel':
          params = new Object();
          if ($("#hfCountry").val().split('|')[1] == '2') {
            params.CountryId = countryID;
            params.Location = $("#hfPaxLocation").val();
            serviceUrl = 'OnlyHotelMixSearchFilter.aspx/getMixHotelData';
          }
          else {
            params.CountryId = countryID;
            params.ArrCity = arrCity;
            params.Resort = resort;
            params.Category = category;
            params.Holpack = holpack;
            serviceUrl = 'OnlyHotelMixSearchFilter.aspx/getHotelData';
          }
          $.ajax({
            type: "POST",
            data: $.json.encode(params),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            url: serviceUrl,
            success: function (msg) {
              if (clear == true) $("#hfHotel").val('');
              $("#txtRoom").html(''); $("#hfRoom").val('');
              $("#txtBoard").html(''); $("#hfBoard").val('');
              if (msg.hasOwnProperty('d') && msg.d != '') {
                var inputDataArray = msg.d;
                var listChk = $("#hfHotel");
                var _input = '';
                var mydata = $.json.decode(inputDataArray);
                if (mydata != null) {
                  _input += '<input type="text" id="hotelAutoComplette"  onkeyup="autoCompletteChange(this.value);" >';
                  $.each(mydata, function (index, inputData) {
                    var checked = '';
                    if (listChk.val().length > 0) {
                      var chkListStr = listChk.val();
                      var chkList = chkListStr.split('|');
                      if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                        checked = 'checked=\'checked\'';
                      else checked = '';
                    } else checked = '';

                    if (index % 2)
                      _input += '<div class="divListPopupRow" name="listPopupRow" id="divListPopupRow_' + index.toString() + '">';
                    else _input += '<div class="divListPopupRowAlter" name="listPopupRow" id="divListPopupRow_' + index.toString() + '">';
                    _input += '<div class="divListPopupleftDiv">';
                    _input += " <input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " hotelName=\"" + inputData.Name + "(" + inputData.Category + ")" + "\" />";
                    _input += '</div>';
                    _input += '<div class="divListPopuprightDiv">';
                    _input += '<div class=\"tranCss\">';
                    _input += " <label for='" + sourceBtn + "_" + index.toString() + "'><b>" + inputData.Name + "(" + inputData.Category + ")" + "</b></label>";
                    _input += "<br />" + inputData.LocationName;
                    _input += '</div>';
                    _input += '</div>';
                    _input += '</div>';
                  });
                }
                $("#divListPopup").html('');
                $("#divListPopup").html(_input);
                if (mydata != null) { $("#hotelAutoComplette").focus(); }
                showListPopupRun(_title, sourceBtn);
              }
            },
            error: function (xhr, msg, e) {
              if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                alert(xhr.responseText);
            },
            statusCode: {
              408: function () {
                logout();
              }
            }
          });
          break;
        case 'room':
          params = new Object();
          params.CountryId = countryID;
          params.DepCity = depCity;
          params.ArrCity = arrCity;
          params.Resort = resort;
          params.Hotel = hotel;
          params.RoomViewID = '';
          serviceUrl = 'OnlyHotelMixSearchFilter.aspx/getRoomData';
          $.ajax({
            type: "POST",
            data: $.json.encode(params),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            url: serviceUrl,
            success: function (msg) {
              $("#txtRoom").html(''); $("#hfRoom").val('');
              $("#txtBoard").html(''); $("#hfBoard").val('');
              var inputDataArray = msg.d;
              var listChk = $("#hfRoom");
              var _input = '';
              var mydata = $.json.decode(inputDataArray)
              $.each(mydata, function (index, inputData) {
                var checked = '';
                if (listChk.val().length > 0) {
                  var chkListStr = listChk.val();
                  var chkList = chkListStr.split('|');
                  if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                    checked = 'checked=\'checked\'';
                  else checked = '';
                } else checked = '';

                _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
              });
              $("#divListPopup").html('');
              $("#divListPopup").html(_input);
              showListPopupRun(_title, sourceBtn);
            },
            error: function (xhr, msg, e) {
              if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                alert(xhr.responseText);
            },
            statusCode: {
              408: function () {
                logout();
              }
            }
          });
          break;
        case 'board':
          params = new Object();
          params.CountryId = countryID;
          params.DepCity = depCity;
          params.ArrCity = arrCity;
          params.Resort = resort;
          params.Hotel = hotel;
          serviceUrl = 'OnlyHotelMixSearchFilter.aspx/getBoardData';
          $.ajax({
            type: "POST",
            data: $.json.encode(params),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            url: serviceUrl,
            success: function (msg) {
              $("#txtBoard").text(''); $("#hfBoard").val('');
              var inputDataArray = msg.d;
              var listChk = $("#hfBoard");
              var _input = '';
              var mydata = $.json.decode(inputDataArray)
              $.each(mydata, function (index, inputData) {
                var checked = '';
                if (listChk.val().length > 0) {
                  var chkListStr = listChk.val();
                  var chkList = chkListStr.split('|');
                  if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                    checked = 'checked=\'checked\'';
                  else checked = '';
                } else checked = '';

                _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
              });
              $("#divListPopup").html('');
              $("#divListPopup").html(_input);
              showListPopupRun(_title, sourceBtn);
            },
            error: function (xhr, msg, e) {
              if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                alert(xhr.responseText);
            },
            statusCode: {
              408: function () {
                logout();
              }
            }
          });
          break;
        default: break;
      }
    }

    function listPopupGetSelected(sourceBtn) {
      switch (sourceBtn) {
        case 'paxlocation':
          var hfLocation = $("#hfPaxLocation");
          var iLocation = $("#fltPaxLocation");
          hfLocation.val($("input[type='radio'][name='listPopupRow']:checked").val());
          iLocation.val($("label[for='" + $("input[type='radio'][name='listPopupRow']:checked")[0].id + "']").text().replace("&amp;", "&"));
          break;
        case 'country':
          var hfCountry = $("#hfCountry");
          var iCountry = $("#fltCountry");
          hfCountry.val($("input[type='radio'][name='listPopupRow']:checked").val());
          iCountry.val($("label[for='" + $("input[type='radio'][name='listPopupRow']:checked")[0].id + "']").text().replace("&amp;", "&"));
          swichPaximumFilter(hfCountry.val());
          break;
        case 'resort':
          var hfResort = $("#hfResort");
          var iResort = $("#txtResort");
          var listChk = '';
          var listTxt = '';
          iResort.text('');
          $(":checked", "#divListPopup").each(function () {
            if (listChk.length > 0) {
              listChk += '|';
              listTxt += '<br />';
            }
            listChk += $(this).val();
            listTxt += $(this).next().text();
          });
          hfResort.val(listChk);
          iResort.html(listTxt);
          break;
        case 'category':
          var hfCategory = $("#hfCategory");
          var iCategory = $("#txtCategory");
          var listChk = '';
          var listTxt = '';
          iCategory.text('');
          $(":checked", "#divListPopup").each(function () {
            if (listChk.length > 0) {
              listChk += '|';
              listTxt += '<br />';
            }
            listChk += $(this).val();
            listTxt += $(this).next().text();
          });

          hfCategory.val(listChk);
          iCategory.html(listTxt);
          break;
        case 'hotel':
          var hfHotel = $("#hfHotel");
          var iHotel = $("#txtHotel");
          var listChk = '';
          var listTxt = '';
          iHotel.text('');
          $(":checked", "#divListPopup").each(function () {
            if (listChk.length > 0) {
              listChk += '|';
              listTxt += '<br />';
            }
            listChk += $(this).val();
            listTxt += $(this).attr("hotelName");
          });

          hfHotel.val(listChk);
          iHotel.html(listTxt);
          break;
        case 'room':
          var hfRoom = $("#hfRoom");
          var iRoom = $("#txtRoom");
          var listChk = '';
          var listTxt = '';
          iRoom.text('');
          $(":checked", "#divListPopup").each(function () {
            if (listChk.length > 0) {
              listChk += '|';
              listTxt += '<br />';
            }
            listChk += $(this).val();
            listTxt += $(this).next().text();
          });

          hfRoom.val(listChk);
          iRoom.html(listTxt);
          break;
        case 'board':
          var hfBoard = $("#hfBoard");
          var iBoard = $("#txtBoard");
          var listChk = '';
          var listTxt = '';
          iBoard.text('');
          $(":checked", "#divListPopup").each(function () {
            if (listChk.length > 0) {
              listChk += '|';
              listTxt += '<br />';
            }
            listChk += $(this).val();
            listTxt += $(this).next().text();
          });
          hfBoard.val(listChk);
          iBoard.html(listTxt);
          break;
        default: break;
      }
      reSizeFrame();
    }

    $.extend($.ui.dialog.prototype, {
      'addbutton': function (buttonName, func) {
        var buttons = this.element.dialog('option', 'buttons');
        buttons.push({ text: buttonName, click: func });
        this.element.dialog('option', 'buttons', buttons);
      }
    });

    function showListPopupAddButton(btnName, func) {
      $("#dialog-listPopup").dialog('addbutton', btnName, func);
    }

    function showListPopupRun(_title, sourceBtn) {
      var buttons = [];

      var top = 2;
      switch (sourceBtn) {
        case 'country':
          top = 5;
          buttons = [{
            text: btnCancel,
            click: function () {
              $(this).dialog('close');
              return false;
            }
          }];
          break;
        case 'paxlocation':
          top = 54;
          buttons = [{
            text: btnCancel,
            click: function () {
              $(this).dialog('close');
              return false;
            }
          }];
          break;
        case 'hotel':
          top = 2;
          buttons = [{
            text: btnOK,
            click: function () {
              listPopupGetSelected(sourceBtn);
              $(this).dialog('close');
              return true;
            }
          }, {
            text: btnCancel,
            click: function () {
              $(this).dialog('close');
              return false;
            }
          }];
          break;
        default:
          top = 2;
          buttons = [{
            text: btnOK,
            click: function () {
              listPopupGetSelected(sourceBtn);
              $(this).dialog('close');
              return true;
            }
          }, {
            text: btnCancel,
            click: function () {
              $(this).dialog('close');
              return false;
            }
          }];
          break
      }
      $("#dialog").dialog("destroy");
      $("#dialog-listPopup").dialog({
        title: _title,
        modal: true,
        height: 400,
        width: 230,
        open: function (event, ui) {
          $(event.target).parent().css('position', 'fixed');
          $(event.target).parent().css('top', top + 'px');
          $(event.target).parent().css('left', '8px');
        },
        buttons: buttons
      });

      reSizeFrame();
    }

    function showListPopup(_title, sourceBtn) {
      $("#divListPopup").html('');

      listPopupFillData(sourceBtn, false, _title);

      if ($("#CustomRegID").val() != '0970301') {
        showListPopupAddButton(btnClear, function () {
          $("#divListPopup").html('');
          listPopupFillData(sourceBtn, true, _title);
          reSizeFrame();
          return false;
        });
      }
    }

    function getRoomInfo(setCrt) {
      var params = new Object();
      params.RoomCount = $("#fltRoomCount").val();
      params.IsPaximum = $("#hfCountry").val().split('|')[1] == '2';
      $.ajax({
        type: "POST",
        url: "OnlyHotelMixSearchFilter.aspx/getRoomInfo",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#fltRoomInfo").html("");
          $("#fltRoomInfo").html(msg.d);
          reSizeFrame();
          if (setCrt == true)
            setCriteria();
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function filterclear() {
      self.parent.pageLoad();
    }

    function flightDays(date) {
      var noWeekend = $.datepicker.noWeekends(date);
      if (dayColor != null && dayColor.length > 0) {
        for (i = 0; i < dayColor.length; i++) {
          var year = date.getFullYear();
          var month = date.getMonth();
          var day = date.getDate();
          if (month == (dayColor[i].Month - 1) && day == dayColor[i].Day && year == dayColor[i].Year) {
            return [true, 'special_day', dayColor[i].Text];
          }
        }
        return [true, ''];
      }
      return [true, ''];
    }

    function setCheckIn() {
      var data = new Object();
      data.ArrCity = $("#fltArrCity").val();
      data.B2BMenuCat = $("#b2bMenuCat").val();
      data.HolPack = $("#fltHolpack").val() != null || $("#fltHolpack").val() != undefined ? $("#fltHolpack").val().split(';')[0] : "";
      $.ajax({
        async: false,
        type: "POST",
        url: "OnlyHotelMixSearchFilter.aspx/getCheckIn",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != '') {
            var data = msg.d;
            dayColor = [];
            dayColor = data;
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
          $("#ppcCheckIn").removeAttr('disabled');
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function setCriteria() {
      getRoomPaxs();
      var data = new Object();
      data.CheckIn = $("#iCheckIn").datepicker('getDate').getTime();
      data.ExpandDate = $("#fltCheckInDay").val();
      data.NightFrom = $("#fltNight1").val();
      data.NightTo = $("#fltNight2").val();
      data.RoomCount = $("#fltRoomCount").val();
      data.roomsInfo = $("#hfRoomInfo").data("roomInfo"); //$("#hfRoomInfo").data();
      data.Board = $("#hfBoard").val();
      data.Room = $("#hfRoom").val();
      data.DepCity = null;
      data.ArrCity = $("#fltArrCity").val();
      data.Resort = $("#hfResort").val();
      data.Category = $("#hfCategory").val();
      data.Hotel = $("#hfHotel").val();
      data.CurControl = ($("#CurrencyConvert")[0].checked ? 'Y' : 'N');
      data.CurrentCur = $("#currentCur").val();
      data.ShowStopSaleHotels = ($("#showStopSale")[0].checked ? 'Y' : 'N');
      data.HolPack = $("#fltHolpack").val() != null || $("#fltHolpack").val() != undefined ? $("#fltHolpack").val().split(';')[0] : ""
      data.holPackCat = $("#fltHolPackCat").val();
      data.B2BMenuCatStr = $("#b2bMenuCat").val();
      var countryID = $("#hfCountry").val() != "" ? ($("#hfCountry").val().split('|').length > 0 ? $("#hfCountry").val().split('|')[0] : $("#hfCountry").val()) : "";
      data.CountryId = countryID;
      $.ajax({
        type: "POST",
        url: "OnlyHotelMixSearchFilter.aspx/SetCriterias",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function paximumSearch(params) {
      $.ajax({
        type: "POST",
        url: "OnlyHotelMixSearchFilter.aspx/SetMixCriterias",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          self.parent.searchMixPrice(msg.d);
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
      reSizeFrame();
    }

    function search() {
      if ($("#CustomRegID").val() == "0302501") {
        if ($("#fltHolpack").val() == "") {
          showDialog(addPleaseSelectPackage);
          return false;
        }
      }

      if ($("#hfCountry").val() == '') {
        showDialog(addPleaseSelectCountry);
        return false;
      }

      getRoomPaxs();

      if ($("#hfCountry").val().split('|')[1] == '2') {
        if ($("#fltPaxNationality").val() == '') {
          showDialog(lblSelectNationality);
          return false;
        }
        var roomInfo = $("#hfRoomInfo").data("roomInfo");
        var paxCount = 0;
        for (var i = 0; i < roomInfo.length; i++) {
          var adult = roomInfo[i].Adult;
          var child = roomInfo[i].Child;
          paxCount += (adult + child);
        }
        if (paxCount > 9) {
          showDialog(lblTotalPaxNumberGT);
          return false;
        }
        var params = new Object();
        params.CheckIn = $("#iCheckIn").datepicker('getDate') != null ? $("#iCheckIn").datepicker('getDate').getTime() : null;
        params.ExpandDate = $("#fltCheckInDay").val();
        params.NightFrom = $("#fltNight1").val();
        params.NightTo = $("#fltNight2").val();
        params.Location = $("#hfPaxLocation").val();
        params.RoomCount = $("#fltRoomCount").val();
        params.roomsInfo = $("#hfRoomInfo").data("roomInfo");//$("#hfRoomInfo").val();
        params.Hotel = $("#hfHotel").val();
        params.CurControl = ($("#CurrencyConvert")[0].checked ? 'Y' : 'N');
        params.CurrentCur = $("#paxCurrentCur").length > 0 ? $("#paxCurrentCur").val() : "";
        params.DefaultNationality = $("#fltPaxNationality").length > 0 ? $("#fltPaxNationality").val() : "TR";
        params.Available = $("#fltAvailable").attr("checked") == 'checked';
        if ($("#fltPaxLocation").val() == '') {
          showDialog(addPleaseSelectArrival);
          return false;
        }

        paximumSearch(params);
        return;
      }
      else {
        var countryID = $("#hfCountry").val() != "" ? ($("#hfCountry").val().split('|').length > 0 ? $("#hfCountry").val().split('|')[0] : $("#hfCountry").val()) : "";

        var params = new Object();
        params.CheckIn = $("#iCheckIn").datepicker('getDate') != null ? $("#iCheckIn").datepicker('getDate').getTime() : null;
        params.ExpandDate = $("#fltCheckInDay").val();
        params.NightFrom = $("#fltNight1").val();
        params.NightTo = $("#fltNight2").val();
        params.RoomCount = $("#fltRoomCount").val();
        params.roomsInfo = $("#hfRoomInfo").data("roomInfo");  //$("#hfRoomInfo").val();
        params.Board = $("#hfBoard").val();
        params.Room = $("#hfRoom").val();
        params.DepCity = null;
        params.ArrCity = $("#fltArrCity").val();
        params.Resort = $("#hfResort").val();
        params.Category = $("#hfCategory").val();
        params.Hotel = $("#hfHotel").val();
        params.CurControl = ($("#CurrencyConvert")[0].checked ? 'Y' : 'N');
        params.CurrentCur = $("#currentCur").val();
        params.ShowStopSaleHotels = ($("#showStopSale")[0].checked ? 'Y' : 'N');
        params.HolPack = $("#hfHolPack").val();
        params.holPackCat = $("#fltHolPackCat").val();
        params.B2BMenuCatStr = $("#b2bMenuCat").val();
        params.CountryId = countryID;

        $.ajax({
          type: "POST",
          url: "OnlyHotelMixSearchFilter.aspx/SetCriterias",
          data: $.json.encode(params),
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (msg) {
            self.parent.searchPrice();
          },
          error: function (xhr, msg, e) {
            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
              alert(xhr.responseText);
          },
          statusCode: {
            408: function () {
              logout();
            }
          }
        });
        reSizeFrame();
      }
    }

    function getStopSaleHotels(showStopSale) {
      if (showStopSale == true) {
        $("#showStopSale").attr("checked", "checked");
      }
    }

    function swichPaximumFilter(item) {
      var code = item.toString().split('|')[0];
      var owner = item.toString().split('|')[1];

      $("#divNight2").hide();

      var today = new Date();
      var tomorrow = new Date(today);
      var data = $("#SearchFilterForm").data("filterData");
      if (owner == "2") {
        dayColor = [];
        $("#arrCityDiv").hide();
        $("#arrCitySepDiv").hide();
        $("#holpackageDiv").hide();
        $("#resortDiv").hide();
        $("#categoryDiv").hide();
        $("#categorySepDiv").hide();
        $("#roomDiv").hide();
        $("#roomSepDiv").hide();
        $("#boardDiv").hide();
        $("#boardSepDiv").hide();
        $("#stopSaleHotelsDiv").hide();
        $("#divExpandDate").hide();

        $("#currencyDiv").hide();
        $("#paxCurrencyDiv").show();
        $("#paxNationality").show();

        $("#arrPaxLocationDiv").show();
        $("#arrPaxLocationSepDiv").show();

        //$("#paxAvailableHotel").show();

        if (data.ShowAvailableHotel) {
          $("#paxAvailableHotel").show();
        }
        else {
          $("#paxAvailableHotel").hide();
        }

        $("#iCheckIn").datepicker('setDate', new Date(today.getTime() + 86400000));
        $("#iCheckIn").datepicker("option", "minDate", today);

        $("#hfPaxLocation").val('');
        $("#fltPaxLocation").val('');
        $("#txtHotel").html(''); $("#hfHotel").val('');
        if (data != null) {
          $("#fltRoomCount").html(packageSearchFilterV2_fltRoomCount(data.fltMixRoomCount));
        }
        getPaxLocation();
      }
      else {
        $("#arrCityDiv").show();
        $("#arrCitySepDiv").show();
        if (data.ShowFilterPackage == false) {
          $("#holpackageDiv").hide();
        } else {
          $("#holpackageDiv").show();
        }
        $("#resortDiv").show();
        $("#categoryDiv").show();
        $("#categorySepDiv").show();
        $("#roomDiv").show();
        $("#roomSepDiv").show();
        $("#boardDiv").show();
        $("#boardSepDiv").show();
        $("#stopSaleHotelsDiv").show();
        $("#divExpandDate").show();

        $("#currencyDiv").show();
        $("#paxCurrencyDiv").hide();
        $("#paxNationality").hide();

        $("#arrPaxLocationDiv").hide();
        $("#arrPaxLocationSepDiv").hide();

        $("#paxAvailableHotel").hide();

        $("#iCheckIn").datepicker("option", "minDate", today);
        $("#iCheckIn").datepicker("setDate", today);

        if (data != null) {
          $("#fltRoomCount").html(packageSearchFilterV2_fltRoomCount(data.fltRoomCount));
        }

        getArrival();
        getHolpack();
      }
      getRoomInfo(true);
    }

    var accentMap = {
      "ç": "c",
      "ö": "o",
      "ı": "i",
      "ü": "u",
      "ğ": "g",
      "ş": "s",
      "Ç": "C",
      "Ö": "O",
      "İ": "i",
      "Ü": "u",
      "Ğ": "G",
      "Ş": "S"
    }

    var normalize = function (term) {
      var ret = "";
      for (var i = 0; i < term.length; i++) {
        ret += accentMap[term.charAt(i)] || term.charAt(i);
      }
      return ret;
    }

    $.ui.autocomplete.prototype._renderItem = function (ul, item) {
      item.label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
      return $("<li></li>")
              .data("item.autocomplete", item)
              .append("<a>" + item.label + "</a>")
              .appendTo(ul);
    };

    function getCountry() {
      $("#fltCountry").autocomplete({
        minLength: 3,
        source: function (request, response) {
          var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
          response($.grep(countryList, function (value) {
            value = value.label || value.value || value;
            return matcher.test(value) || matcher.test(normalize(value));
          }));
        },
        select: function (event, ui) {
          $("#hfCountry").val(ui.item.item);
          $("#fltCountry").val(ui.item.value);
          swichPaximumFilter(ui.item.item);
          return false;
        }
      });
    }

    function getPaxLocation() {
      var params = new Object();
      params.CountryId = $("#hfCountry").val();
      $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "OnlyHotelMixSearchFilter.aspx/getPaxLocationCache",
        data: $.json.encode(params),
        dataType: "json",
        success: function (data) {

        },
        error: function (result) {
          alert("Error");
        }
      });
    }

    function getFormData() {
      $.ajax({
        type: "POST",
        url: "OnlyHotelMixSearchFilter.aspx/getFormData",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var data = msg.d;
          if (data == null || data == undefined) {
            self.location = self.location;
            return false;
          }

          countryList = data.fltCountry;

          $("#SearchFilterForm").data("filterData", data);

          $("#CustomRegID").val(data.CustomRegID);

          if (data.ShowFilterPackage == false) {
            $("#holpackageDiv").hide();
          } else {
            $("#holpackageDiv").show();
          }

          if (data.CustomRegID == '0970301' && data.Market == 'SWEMAR') {
            $("#divNight2").hide();
          }
          $("#fltCheckInDay").html(packageSearchFilterV2_fltCheckInDay(data.fltCheckInDay, lblDay));
          $("#fltNight1").html(packageSearchFilterV2_fltNight(data.fltNight));
          $("#fltNight2").html(packageSearchFilterV2_fltNight(data.fltNight2));
          $("#fltRoomCount").html(packageSearchFilterV2_fltRoomCount(data.fltRoomCount));
          $("#divCurrency").html('');
          $("#divCurrency").html(data.MarketCur);

          $("#divPaxNationality").html(data.PaxNationality);

          $("#divPaxCurrency").html('');
          $("#divPaxCurrency").html(data.PaxCurrency);

          $("#hfDate").val('');
          $("#hfDate").val(data.CheckIn);

          var _date1 = new Date(Date(eval(data.CheckIn)).toString());
          $("#iCheckIn").datepicker('setDate', _date1);

          if (!data.ShowExpandDay) $("#divExpandDate").hide();
          if (!data.ShowSecondDay) $("#divNight2").hide();

          getCountry();
          //getArrival();
          //getHolpack();
          getRoomInfo(true);
          if (data.CustomRegID == '0970301' || data.CustomRegID == '1076601') {
            $("#divStopSaleHotels").hide();
          }
          else {
            getStopSaleHotels(data.ShowStopSaleHotels);
          }

          if (data.DisableRoomFilter) {
            $("#roomFilter").hide();
          } else {
            $("#roomFilter").show();
          }

          if (data.AvailableHotel) {
            $("#fltAvailable").attr("checked","checked");
          }
          else {
            $("#fltAvailable").removeAttr("checked");
          }                    
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function changedNight1() {
      $("#fltNight2").val($("#fltNight1").val());
    }

    function changedNight2() {
      var night1 = parseInt($("#fltNight1").val());
      var night2 = parseInt($("#fltNight2").val());
      if (night1 > night2)
        $("#fltNight2").val($("#fltNight1").val());
    }

    function reSearch(date) {
      $.ajax({
        type: "POST",
        url: "OnlyHotelMixSearchFilter.aspx/reSearch",
        data: '{"date":' + date + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            $("#hfDate").val('');
            $("#hfDate").val(msg.d.CheckIn.toString());
            var _date = new Date(parseInt(msg.d.CheckIn) + 86400000);
            $("#iCheckIn").val($.format.date(_date, msg.d.DateFormat));
            self.parent.searchPrice();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
      reSizeFrame();
    }

    function createOffer() {
      self.parent.createOfferV2();
    }

    $(document).ready(function () {
      $.datepicker.setDefaults($.datepicker.regional['<%= twoLetterISOLanguageName %>' != 'en' ? '<%= twoLetterISOLanguageName %>' : '']);

          $("#iCheckIn").datepicker({
            showOn: "button",
            buttonImage: "Images/Calendar.gif",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            onSelect: function (dateText, inst) {
              if (dateText != '') {
                var date2 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
                var iDate = $("#hfDate");
                if (date2) {
                  iDate.val(Date.parse(date2));
                }
                else {
                  iDate.val(Date.parse(new Date()));
                }
              }
            },
            minDate: new Date(),
            beforeShowDay: flightDays
          });

          $.query = $.query.load(location.href);
          var HolPackCat = $.query.get('HolPackCat');
          var PackType = $.query.get('PackType');
          if (HolPackCat == undefined && HolPackCat.length == 0) HolPackCat == '';
          if (PackType == undefined && PackType.length == 0) PackType == '';
          $("#fltHolPackCat").val(HolPackCat);
          var b2bMenuCat = '';
          b2bMenuCat = $.query.get('B2BMenuCat');
          if (b2bMenuCat == undefined && b2bMenuCat.length == 0) b2bMenuCat == '';
          $("#b2bMenuCat").val(b2bMenuCat);
          getFormData();
          $("#fltArrCity").on("click", function () {

          });
          $("#fltCountry").blur(function () {
            if ($("#fltCountry").val() == '') {
              $("#hfCountry").val('');
            }
          });
          $("#fltPaxLocation").blur(function () {
            if ($("#fltPaxLocation").val() == '') {
              $("#hfPaxLocation").val('');
            }
          });
        });
  </script>

</head>
<body class="ui-helper-clearfix">
  <form id="SearchFilterForm" runat="server">
    <div class="ui-helper-hidden">
      <input id="hfPaxLocation" type="hidden" />
      <input id="hfCountry" type="hidden" />
      <input id="hfDate" type="hidden" />
      <input id="hfResort" type="hidden" />
      <input id="hfCategory" type="hidden" />
      <input id="hfHotel" type="hidden" />
      <input id="hfRoomInfo" type="hidden" />
      <input id="hfRoom" type="hidden" />
      <input id="hfBoard" type="hidden" />
      <input id="hfHolPack" type="hidden" />
      <input id="fltHolPackCat" type="hidden" value="" />
      <input id="b2bMenuCat" type="hidden" value="" />
      <input id="CustomRegID" type="hidden" value="" />
    </div>
    <div style="width: 250px;">
      <div class="ui-helper-clearfix filterGroup">
        <strong>
          <%= GetGlobalResourceObject("PackageSearchFilter", "lblCountry") %></strong>
        <div class="ui-helper-clearfix countryGrp">
          <input id="fltCountry" type="text" readonly="readonly" onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleCountryList") %>  ', 'country');" />
          <img src="Images/button.png" alt="" onclick="showListPopup('Country List', 'country');" />
        </div>
      </div>
      <div id="arrPaxLocationDiv" class="ui-helper-clearfix ui-helper-hidden filterGroup">
        <strong>
          <%= GetGlobalResourceObject("PackageSearchFilter", "lblDestination") %></strong>
        <div class="ui-helper-clearfix paxLocationGrp">
          <input id="fltPaxLocation" type="text" readonly="readonly" onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleArrivalList") %>  ', 'paxlocation');" />
          <img src="Images/button.png" alt="" onclick="showListPopup('Arrival List', 'paxlocation');" />
        </div>
      </div>
      <div id="arrCityDiv" class="ui-helper-clearfix filterGroup">
        <strong>
          <%= GetGlobalResourceObject("PackageSearchFilter", "lblDestination") %></strong>
        <div class="ui-helper-clearfix arrCityGrp">
          <select id="fltArrCity" onchange="changeArrCity();">
          </select>
        </div>
      </div>
      <div id="holpackageDiv" class="ui-helper-clearfix filterGroup">
        <strong>
          <%= GetGlobalResourceObject("PackageSearchFilter", "lblHolpack")%></strong>
        <div class="ui-helper-clearfix holPackGrp">
          <select id="fltHolpack" onchange="changeHolpack();">
          </select>
        </div>
      </div>
      <div class="ui-helper-clearfix filterGroup">
        <div id="divCheckIn" class="checkInDiv">
          <strong>
            <%= GetGlobalResourceObject("PackageSearchFilter", "lblCheckIn") %></strong>
          <div class="ui-helper-clearfix checkInGrp">
            <input id="iCheckIn" type="text" />
          </div>
        </div>
        <div id="divExpandDate" class="expendDayDiv">
          <strong>
            <%= GetGlobalResourceObject("PackageSearchFilter", "lblExpandDate") %></strong>
          <div class="ui-helper-clearfix expandDayGrp">
            <select id="fltCheckInDay">
            </select>
          </div>
        </div>
      </div>
      <div class="ui-helper-clearfix filterGroup">
        <div id="divNight1" class="nightFirstDiv">
          <strong>
            <%= GetGlobalResourceObject("PackageSearchFilter", "lblNight1") %></strong>
          <div class="ui-helper-clearfix nightFirstGrp">
            <select id="fltNight1" onchange="changedNight1();">
            </select>
          </div>
        </div>
        <div id="divNight2" class="nightLastDiv">
          <strong>
            <%= GetGlobalResourceObject("PackageSearchFilter", "lblNight2") %></strong>
          <div class="ui-helper-clearfix nightLastGrp">
            <select id="fltNight2" onchange="changedNight2();">
            </select>
          </div>
        </div>
      </div>
      <div class="ui-helper-clearfix filterRoomGroup">
        <strong>
          <%= GetGlobalResourceObject("PackageSearchFilter", "lblRoomCount") %></strong>
        <div class="ui-helper-clearfix roomCountGrp">
          <select id="fltRoomCount" onchange="getRoomInfo(false);">
          </select>
        </div>
        <div id="divRoomInfo" class="ui-helper-clearfix roomGrp">
          <div id="fltRoomInfo" class="roomInfo">
          </div>
        </div>
      </div>
      <div id="resortDiv" class="ui-helper-clearfix filterMultiSelectGroup">
        <div class="filterMultiSelectHeader" onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleResortList") %>', 'resort');">
          <div class="filterMultiSelectLabel">
            <span><strong>
              <%= GetGlobalResourceObject("PackageSearchFilter", "lblResort") %></strong></span>
          </div>
          <div class="filterMultiSelectImg">
            <img alt="" title="" src="Images/search.png" />
          </div>
        </div>
        <div id="txtResort" class="multiField">
        </div>
      </div>
      <div id="categoryDiv" class="ui-helper-clearfix filterMultiSelectGroup">
        <div class="filterMultiSelectHeader" onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleCategoryList") %>', 'category');">
          <div class="filterMultiSelectLabel">
            <span><strong>
              <%= GetGlobalResourceObject("PackageSearchFilter", "lblCategory") %></strong></span>
          </div>
          <div class="filterMultiSelectImg">
            <img alt="" title="" src="Images/search.png" />
          </div>
        </div>
        <div id="txtCategory" class="multiField">
        </div>
      </div>
      <div id="hotelDiv" class="ui-helper-clearfix filterMultiSelectGroup">
        <div class="filterMultiSelectHeader" onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleHotelList") %>', 'hotel');">
          <div class="filterMultiSelectLabel">
            <span><strong>
              <%= GetGlobalResourceObject("PackageSearchFilter", "lblHotel") %></strong></span>
          </div>
          <div class="filterMultiSelectImg">
            <img alt="" title="" src="Images/search.png" />
          </div>
        </div>
        <div id="txtHotel" class="multiField">
        </div>
      </div>
      <div id="roomDiv" class="ui-helper-clearfix filterMultiSelectGroup">
        <div class="filterMultiSelectHeader" onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleRoomList") %>', 'room');">
          <div class="filterMultiSelectLabel">
            <span><strong>
              <%= GetGlobalResourceObject("PackageSearchFilter", "lblRoom") %></strong></span>
          </div>
          <div class="filterMultiSelectImg">
            <img alt="" title="" src="Images/search.png" />
          </div>
        </div>
        <div id="txtRoom" class="multiField">
        </div>
      </div>
      <div id="boardDiv" class="ui-helper-clearfix filterMultiSelectGroup">
        <div class="filterMultiSelectHeader" onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleBoardList") %>', 'board');">
          <div class="filterMultiSelectLabel">
            <span><strong>
              <%= GetGlobalResourceObject("PackageSearchFilter", "lblBoard") %></strong></span>
          </div>
          <div class="filterMultiSelectImg">
            <img alt="" title="" src="Images/search.png" />
          </div>
        </div>
        <div id="txtBoard" class="multiField">
        </div>
      </div>

      <table cellpadding="2" cellspacing="0" style="text-align: left; width: 250px;">

        <tr>
          <td style="height: 3px;">&nbsp;
          </td>
        </tr>
        <tr id="currencyDiv">
          <td>
            <div id="divCurrency" style="clear: both; width: 100%;">
            </div>
          </td>
        </tr>
        <tr id="paxCurrencyDiv" class="ui-helper-hidden">
          <td>
            <div id="divPaxCurrency" style="clear: both; width: 100%;">
            </div>
          </td>
        </tr>
        <tr>
          <td style="height: 3px;">&nbsp;
          </td>
        </tr>
        <tr id="paxNationality" class="ui-helper-hidden">
          <td>
            <div id="divPaxNationality" style="clear: both; width: 100%;">
            </div>
          </td>
        </tr>
        <tr id="stopSaleHotelsDiv" class="ui-helper-hidden">
          <td>
            <div id="divStopSaleHotels" style="clear: both; width: 100%;">
              <br />
              <input id="showStopSale" type="checkbox" /><label for="showStopSale"><%= GetGlobalResourceObject("PackageSearchFilter", "lblShowStopSaleHotels") %></label>
              <br />
            </div>
          </td>
        </tr>
        <tr id="paxAvailableHotel" class="ui-helper-hidden">
          <td>
            <div id="divAvailable" style="clear: both; width: 100%;">
              <br />
              <input id="fltAvailable" type="checkbox" /><label for="fltAvailable"><%= GetGlobalResourceObject("PackageSearchFilter", "lblShowOnlyAvailableHotels") %></label>
              <br />
            </div>
          </td>
        </tr>
        <tr>
          <td style="height: 5px;">&nbsp;
          </td>
        </tr>
        <tr>
          <td style="z-index: 1; text-align: center;">
            <input type="button" name="btnSearch" value='<%= GetGlobalResourceObject("LibraryResource", "btnSearch") %>'
              style="width: 100px;" onclick="search()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />&nbsp;
                    <input type="button" name="btnClear" value='<%= GetGlobalResourceObject("LibraryResource", "btnClear") %>'
                      style="width: 100px;" onclick="filterclear()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
          </td>
        </tr>
        <tr>
          <td style="height: 3px;">&nbsp;
          </td>
        </tr>
        <tr>
          <td id="divOffers" style="z-index: 1; text-align: center; display: none;">
            <input type="button" name="btnCreateOffer" value='<%= GetGlobalResourceObject("PackageSearchResultV2","btnCreateOffer") %>'
              style="width: 80%;" onclick="createOffer()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />&nbsp;                    
          </td>
        </tr>
      </table>
    </div>
    <div id="dialog-listPopup" class="listPopup ui-helper-hidden">
      <div id="divListPopup" class="listPopupResult">
      </div>
    </div>
    <div id="dialog-message" title="" style="display: none;">
      <span id="messages">Message</span>
    </div>
  </form>
</body>
</html>
