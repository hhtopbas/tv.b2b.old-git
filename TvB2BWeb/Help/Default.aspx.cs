﻿using System;
using System.Web;
using TvBo;

public partial class Help_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        if (!string.IsNullOrEmpty(Request.Params["ExternalUrl"])) {
            Response.Redirect(HttpUtility.UrlDecode(Request.Params["ExternalUrl"]));
        } else {
            Response.Redirect(UserData.Market + "/Default.aspx");
        }
    }
}
