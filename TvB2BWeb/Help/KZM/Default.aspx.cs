﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Help_FINMAR_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Url.ToString().ToLower().IndexOf("milanatour")>-1)
            Response.Redirect("MILHELP.pdf");
        else Response.Redirect("HELP.pdf");
    }
}
