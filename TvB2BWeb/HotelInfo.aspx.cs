﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SautinSoft;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Web;
using System.Text;
using System.Web.Services;
using TvTools;

namespace TvSearch
{
    public partial class HotelInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.Params["Culture"]))
            {
                CultureInfo Ci = new CultureInfo("en-US");
                Ci = new CultureInfo((string)Request.Params["Culture"]);                
                Thread.CurrentThread.CurrentCulture = Ci;
                Thread.CurrentThread.CurrentUICulture = Ci;
            }
        }

        [WebMethod(EnableSession = true)]
        public static string gerFormData(string Hotel, string Market, string Culture)
        {
            if (!string.IsNullOrEmpty(Culture) && !string.IsNullOrEmpty(Hotel))
            {
                CultureInfo Ci = new CultureInfo(Culture);
                Thread.CurrentThread.CurrentCulture = Ci;
                Thread.CurrentThread.CurrentUICulture = Ci;
                Thread.CurrentThread.CurrentCulture = Ci;
                Thread.CurrentThread.CurrentUICulture = Ci;
                string errorMsg = string.Empty;
                HotelProperty hotelProperty = new Hotels().getHotelProperty(Market, Hotel, ref errorMsg);                
                return Newtonsoft.Json.JsonConvert.SerializeObject(hotelProperty);
            }
            else return string.Empty;
        }

        [WebMethod(EnableSession = true)]
        public static string getHotelPicture(string Hotel, string Culture)
        {
            if (!string.IsNullOrEmpty(Culture) && !string.IsNullOrEmpty(Hotel))
            {
                CultureInfo Ci = new CultureInfo(Culture);
                Thread.CurrentThread.CurrentCulture = Ci;
                Thread.CurrentThread.CurrentUICulture = Ci;

                string errorMsg = string.Empty;

                List<HotelPictRecord> hotelPictureList = new Hotels().getHotelPict(Hotel, ref errorMsg);
                return Newtonsoft.Json.JsonConvert.SerializeObject(hotelPictureList);
            }
            else return string.Empty;
        }

        [WebMethod(EnableSession = true)]
        public static string getHotelPictures(string Hotel)
        {
            return null;
        }
    }
}