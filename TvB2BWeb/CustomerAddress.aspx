﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CustomerAddress.aspx.cs"
    Inherits="CustomerAddress" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "CustomerAddress")%></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <script src="Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>

    <script src="Scripts/linq.js" type="text/javascript"></script>

    <script src="Scripts/jquery.linq.js" type="text/javascript"></script>

    <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/confirm.css" rel="stylesheet" type="text/css" />
    <link href="CSS/ResCustInfoMem.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">

        if (typeof window.event != 'undefined')
            document.onkeydown = function() {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function(e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        $(function() {
            $("#Tabs").tabs({ remote: true, selected: 1 });
        });

        function logout() {
            self.parent.logout();
        }

        function exit(source) {
            var data = '';
            data += '<';
            data += '|CTitle|:|' + $("#CTitle").val() + '|';
            data += ',|CName|:|' + $("#CName").val() + '|';
            data += ',|CSurName|:|' + $("#CSurName").val() + '|';
            data += ',|MobTel|:|' + ($("#MobTel").val() != '' ? $("#MobTelCountryCode").text() + $("#MobTel").val() : $("#MobTel").val()) + '|';
            data += ',|ContactAddr|:|' + $("#ContactAddr").val() + '|';
            data += ',|InvoiceAddr|:|' + $("#InvoiceAddr").val() + '|';
            data += ',|AddrHome|:|' + $("#AddrHome").val() + '|';
            data += ',|AddrHomeCity|:|' + $("#AddrHomeCity").val() + '|';
            data += ',|AddrHomeCountry|:|' + $("#AddrHomeCountry").val() + '|';
            data += ',|AddrHomeZip|:|' + $("#AddrHomeZip").val() + '|';
            data += ',|AddrHomeTel|:|' + ($("#AddrHomeTel").val() != '' ? $("#AddrHomeTelCountryCode").text() + $("#AddrHomeTel").val() : $("#AddrHomeTel").val()) + '|';
            data += ',|AddrHomeFax|:|' + ($("#AddrHomeFax").val() != '' ? $("#AddrHomeFaxCountryCode").text() + $("#AddrHomeFax").val() : $("#AddrHomeFax").val()) + '|';
            data += ',|AddrHomeEmail|:|' + $("#AddrHomeEmail").val() + '|';
            data += ',|HomeTaxOffice|:|' + $("#HomeTaxOffice").val() + '|';
            data += ',|HomeTaxAccNo|:|' + $("#HomeTaxAccNo").val() + '|';
            data += ',|WorkFirmName|:|' + $("#WorkFirmName").val() + '|';
            data += ',|AddrWork|:|' + $("#AddrWork").val() + '|';
            data += ',|AddrWorkCity|:|' + $("#AddrWorkCity").val() + '|';
            data += ',|AddrWorkCountry|:|' + $("#AddrWorkCountry").val() + '|';
            data += ',|AddrWorkZip|:|' + $("#AddrWorkZip").val() + '|';
            data += ',|AddrWorkTel|:|' + ($("#AddrWorkTel").val() != '' ? $("#AddrWorkTelCountryCode").text() + $("#AddrWorkTel").val() : $("#AddrWorkTel").val()) + '|';
            data += ',|AddrWorkFax|:|' + ($("#AddrWorkFax").val() != '' ? $("#AddrWorkFaxCountryCode").text() + $("#AddrWorkFax").val() : $("#AddrWorkFax").val()) + '|';
            data += ',|AddrWorkEMail|:|' + $("#AddrWorkEMail").val() + '|';
            data += ',|WorkTaxOffice|:|' + $("#WorkTaxOffice").val() + '|';
            data += ',|WorkTaxAccNo|:|' + $("#WorkTaxAccNo").val() + '|';
            data += ',|Bank|:|' + ($("#Bank").length > 0 ? $("#Bank").val() : '') + '|';
            data += ',|BankAccNo|:|' + ($("#BankAccNo").length > 0 ? $("#BankAccNo").val() : '') + '|';
            data += ',|BankIBAN|:|' + ($("#BankIBAN").length > 0 ? $("#BankIBAN").val() : '') + '|';
            data += ',|CustNo|:|' + ($("#CustNo").length > 0 ? $("#CustNo").val() : '') + '|';
            data += ',|LCID|:|' + $("#hfLCID").val() + '|';
            data += '>';

            window.close;
            self.parent.returnCustInfoEdit(data.replace(/\\/g, '/'), source);            
        }

        function getPage(CustNo) {
            $.ajax({
                type: "POST",
                url: "CustomerAddress.aspx/getFormData",
                data: '{"_CustNo":' + CustNo + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    var formData = $.json.decode(msg.d);
                    //                    $("#Tabs").html('');
                    //                    $("#Tabs").html(msg.d);
                    //                    $("#divCustomers").html(formData.Customers.replace(/#/g, '"'));
                    $("#divContact").html('');
                    $("#divContact").html(formData.Contact.replace(/#/g, '"'));
                    $("#divHomeAddress").html('');
                    $("#divHomeAddress").html(formData.HomeAddr.replace(/#/g, '"'));
                    $("#divWorkAddress").html('');
                    $("#divWorkAddress").html(formData.WorkAddr.replace(/#/g, '"'));

                    var _phoneMask = $("#phone_Mask").val();
                    if (_phoneMask != '')
                        $(".phoneMask").mask(_phoneMask);

                    var _mobPhoneMask = $("#mob_phone_Mask").val();
                    if (_mobPhoneMask != '')
                        $(".mobPhoneMask").mask(_mobPhoneMask);

                    $("#divBankInfo").html('');
                    if (formData.Bank != '')
                        $("#divBankInfo").html(formData.Bank.replace(/#/g, '"'));
                    else {
                        $("#liBankInfo").hide();
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getPageContactInfo(CustNo) {
            $.ajax({
                type: "POST",
                url: "CustomerAddress.aspx/getFormDataContactInfo",
                data: '{"_CustNo":' + CustNo + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#divCustomers").html('');
                    $("#divCustomers").html(msg.d);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function homeCountryChanged() {
            var country = $("#AddrHomeCountry").val();
            var countryList = $("#countryList").val();

            var queryResult = $.Enumerable.From($.json.decode(countryList))
                               .Where(function(x) { return x.CountryName == country })
                               .Select("$.CountryPhoneCode")
                               .First();
            if (queryResult != '') {
                $("#MobTelCountryCode").text('+' + queryResult);
                $("#AddrHomeTelCountryCode").text('+' + queryResult);
                $("#AddrHomeFaxCountryCode").text('+' + queryResult);
            }
        }

        function workCountryChanged() {
            var country = $("#AddrWorkCountry").val();
            var countryList = $("#countryList").val();

            var queryResult = $.Enumerable.From($.json.decode(countryList))
                               .Where(function(x) { return x.CountryName == country })
                               .Select("$.CountryPhoneCode")
                               .First();
            if (queryResult != '') {
                $("#AddrWorkTelCountryCode").text('+' + queryResult);
                $("#AddrWorkFaxCountryCode").text('+' + queryResult);
            }
        }


        function pageLoad() {
            $.query = $.query.load(location.href);
            var CustNo = $.query.get('CustNo');
            getPageContactInfo(CustNo);
            getPage(CustNo);
        }
        
    </script>

    <style type="text/css">
        </style>
</head>
<body onload="pageLoad();">
    <form id="ResCustInfoMemForm" runat="server">
    <div id="divCustomersInfo">
        <div id="divCustomers">
        </div>
        <div id="divInfo">
            <div id="Tabs">
                <ul>
                    <li id="liContact"><a href="#divContact">
                        <%= GetGlobalResourceObject("LibraryResource", "lblContactPerson") %></a></li>
                    <li id="liHomeAddress"><a href="#divHomeAddress">
                        <%= GetGlobalResourceObject("LibraryResource", "lblHomeAddress")%></a></li>
                    <li id="liWorkAddress"><a href="#divWorkAddress">
                        <%= GetGlobalResourceObject("LibraryResource", "lblWorkAddress")%></a></li>
                    <li id="liBankInfo"><a href="#divBankInfo">
                        <%= GetGlobalResourceObject("LibraryResource", "lblBankInfo")%></a></li>
                </ul>
                <div id="divContact">
                </div>
                <div id="divHomeAddress">
                </div>
                <div id="divWorkAddress">
                </div>
                <div id="divBankInfo">
                </div>
            </div>
            <div class="divButtons">
                <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource","btnSave").ToString() %>'
                    onclick="exit('save');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                    style="width: 110px;" />
                &nbsp;&nbsp;
                <input id="btnCancel" type="button" value='<%= GetGlobalResourceObject("LibraryResource","btnCancel").ToString() %>'
                    onclick="exit('cancel');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                    style="width: 110px;" />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
