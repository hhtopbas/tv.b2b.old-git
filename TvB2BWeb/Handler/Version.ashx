﻿<%@ WebHandler Language="C#" Class="Version" %>

using System;
using System.Web;

public class Version : IHttpHandler {
    
    public void ProcessRequest(HttpContext context)
    {
        context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(TvBo.VersionControl.getVersion()));
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}