﻿/*
* jQuery history plugin
*
* Copyright (c) 2006 Taku Sano (Mikage Sawatari)
* Licensed under the MIT License:
* http://www.opensource.org/licenses/mit-license.php
*
* Modified by Lincoln Cooper to add Safari support and only call the callback once during initialization
* for msie when no initial hash supplied.
*/


jQuery.extend({
    historyCurrentHash: undefined,
    historyCallback: undefined,
    historyInit: function(callback) {
        jQuery.historyCallback = callback;
        var current_hash = location.hash;

        jQuery.historyCurrentHash = current_hash;
        if (jQuery.browser.msie) {
            // To stop the callback firing twice during initilization if no hash present
            if (jQuery.historyCurrentHash == '') {
                jQuery.historyCurrentHash = '#';
            }

            // add hidden iframe for IE
            $("body").prepend('<iframe id="jQuery_history" style="display: none;"></iframe>');
            var ihistory = $("#jQuery_history")[0];
            var iframe = ihistory.contentWindow.document;
            iframe.open();
            iframe.close();
            iframe.location.hash = current_hash;
        }
        else if ($.browser.safari) {
            // etablish back/forward stacks
            jQuery.historyBackStack = [];
            jQuery.historyBackStack.length = history.length;
            jQuery.historyForwardStack = [];

            jQuery.isFirst = true;
        }
        jQuery.historyCallback(current_hash.replace(/^#/, ''));
        setInterval(jQuery.historyCheck, 100);
    },
    historyAddHistory: function(hash) {
        // This makes the looping function do something
        jQuery.historyBackStack.push(hash);

        jQuery.historyForwardStack.length = 0; // clear forwardStack (true click occured)
        this.isFirst = true;
    },
    historyCheck: function() {
        if (jQuery.browser.msie) {
            // On IE, check for location.hash of iframe
            var ihistory = $("#jQuery_history")[0];
            var iframe = ihistory.contentDocument || ihistory.contentWindow.document;
            var current_hash = iframe.location.hash;
            if (current_hash != jQuery.historyCurrentHash) {

                location.hash = current_hash;
                jQuery.historyCurrentHash = current_hash;
                jQuery.historyCallback(current_hash.replace(/^#/, ''));

            }
        } else if ($.browser.safari) {
            if (!jQuery.dontCheck) {
                var historyDelta = history.length - jQuery.historyBackStack.length;

                if (historyDelta) { // back or forward button has been pushed
                    jQuery.isFirst = false;
                    if (historyDelta < 0) { // back button has been pushed
                        // move items to forward stack
                        for (var i = 0; i < Math.abs(historyDelta); i++) jQuery.historyForwardStack.unshift(jQuery.historyBackStack.pop());
                    } else { // forward button has been pushed
                        // move items to back stack
                        for (var i = 0; i < historyDelta; i++) jQuery.historyBackStack.push(jQuery.historyForwardStack.shift());
                    }
                    var cachedHash = jQuery.historyBackStack[jQuery.historyBackStack.length - 1];
                    if (cachedHash != undefined) {
                        jQuery.historyCurrentHash = location.hash;
                        jQuery.historyCallback(cachedHash);
                    }
                } else if (jQuery.historyBackStack[jQuery.historyBackStack.length - 1] == undefined && !jQuery.isFirst) {
                    // back button has been pushed to beginning and URL already pointed to hash (e.g. a bookmark)
                    // document.URL doesn't change in Safari
                    if (document.URL.indexOf('#') >= 0) {
                        jQuery.historyCallback(document.URL.split('#')[1]);
                    } else {
                        var current_hash = location.hash;
                        jQuery.historyCallback('');
                    }
                    jQuery.isFirst = true;
                }
            }
        } else {
            // otherwise, check for location.hash
            var current_hash = location.hash;
            if (current_hash != jQuery.historyCurrentHash) {
                jQuery.historyCurrentHash = current_hash;
                jQuery.historyCallback(current_hash.replace(/^#/, ''));
            }
        }
    },
    historyLoad: function(hash) {
        var newhash;

        if (jQuery.browser.safari) {
            newhash = hash;
        }
        else {
            newhash = '#' + hash;
            location.hash = newhash;
        }
        jQuery.historyCurrentHash = newhash;

        if (jQuery.browser.msie) {
            var ihistory = $("#jQuery_history")[0];
            var iframe = ihistory.contentWindow.document;
            iframe.open();
            iframe.close();
            iframe.location.hash = newhash;
            jQuery.historyCallback(hash);
        }
        else if (jQuery.browser.safari) {
            jQuery.dontCheck = true;
            // Manually keep track of the history values for Safari
            this.historyAddHistory(hash);

            // Wait a while before allowing checking so that Safari has time to update the "history" object
            // correctly (otherwise the check loop would detect a false change in hash).
            var fn = function() { jQuery.dontCheck = false; };
            window.setTimeout(fn, 200);
            jQuery.historyCallback(hash);
            // N.B. "location.hash=" must be the last line of code for Safari as execution stops afterwards.
            // By explicitly using the "location.hash" command (instead of using a variable set to "location.hash") the
            // URL in the browser and the "history" object are both updated correctly.
            location.hash = newhash;
        }
        else {
            jQuery.historyCallback(hash);
        }
    }
});

function decimalPlaces(n) {
    // Make sure it is a number and use the builtin number -> string.
    var s = "" + (+n);
    // Pull out the fraction and the exponent.
    var match = /(?:\.(\d+))?(?:[eE]([+\-]?\d+))?$/.exec(s);
    // NaN or Infinity or integer.
    // We arbitrarily decide that Infinity is integral.
    if (!match) {
        match = /(?:\,(\d+))?(?:[eE]([+\-]?\d+))?$/.exec(s);
        if (!match) {
            return 0;
        }
    }
    // Count the number of digits in the fraction and subtract the
    // exponent to simulate moving the decimal point left by exponent places.
    // 1.234e+2 has 1 fraction digit and '234'.length -  2 == 1
    // 1.234e-2 has 5 fraction digit and '234'.length - -2 == 5
    return Math.max(
        0,  // lower limit.
        (match[1] == '0' ? 0 : (match[1] || '').length)  // fraction length
        - (match[2] || 0));  // exponent
}

function lzw_encode(s) {
    var dict = {};
    var data = (s + "").split("");
    var out = [];
    var currChar;
    var phrase = data[0];
    var code = 256;
    for (var i = 1; i < data.length; i++) {
        currChar = data[i];
        if (dict[phrase + currChar] != null) {
            phrase += currChar;
        }
        else {
            out.push(phrase.length > 1 ? dict[phrase] : phrase.charCodeAt(0)); dict[phrase + currChar] = code; code++;
            phrase = currChar;
        }
    }
    out.push(phrase.length > 1 ? dict[phrase] : phrase.charCodeAt(0));
    for (var i = 0; i < out.length; i++) {
        out[i] = String.fromCharCode(out[i]);
    }
    return out.join("");
}

function lzw_decode(s) {
    var dict = {};
    var data = (s + "").split("");
    var currChar = data[0];
    var oldPhrase = currChar;
    var out = [currChar];
    var code = 256;
    var phrase;
    for (var i = 1; i < data.length; i++) {
        var currCode = data[i].charCodeAt(0);
        if (currCode < 256) {
            phrase = data[i];
        }
        else {
            phrase = dict[currCode] ? dict[currCode] : (oldPhrase + currChar);
        }
        out.push(phrase);
        currChar = phrase.charAt(0);
        dict[code] = oldPhrase + currChar;
        code++;
        oldPhrase = phrase;
    }
    return out.join("");
}

function isNumericChar(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 8) return true;
    if (charCode >= 48 && charCode <= 57)
        return true;
    else return false;
}

function isPhoneChar(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 8 || charCode == 43 || charCode == 95) return true;
    if ((charCode >= 48 && charCode <= 57) || charCode == 32)
        return true;
    else return false;
}

function isNameSurnameChar(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 8 || charCode == 95) return true;
    if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode == 32 || charCode == 45)
        return true;
    else return false;
}

function isNameSurnameCharDetur(evt) {

    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 8 || charCode == 95) return true;
    if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 254) || charCode == 32 ||
        (charCode >= 228 && charCode <= 230) || (charCode >= 196 && charCode <= 198) ||
        charCode == 246 || charCode == 248 || charCode == 252 || charCode == 214 || charCode == 216 || charCode == 220)
        return true;
    else return false;
}

function isNameSurnameString(id) {
    var value = $("#" + id).val();
    var tmpVal = '';
    if (value.length > 0) {
        for (var i = 0; i < value.length; i++) {
            var charCode = value.split('')[i];
            if (value.charCodeAt(i) == 8 || value.charCodeAt(i) == 95) return true;
            if ((value.charCodeAt(i) >= 65 && value.charCodeAt(i) <= 90) || (value.charCodeAt(i) >= 97 && value.charCodeAt(i) <= 122) || value.charCodeAt(i) == 32 || charCode == 45)
                tmpVal += charCode;
        }
        $("#" + id).val(tmpVal);
    }
    else $("#" + id).val(value);
}

function isNonUniCodeChar(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 8 || charCode == 95) return true;
    if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode == 32 || (charCode >= 48 && charCode <= 57) || charCode == 45)
        return true;
    else return false;
}

function dateValue(_format, value) {
    var _day = 0;
    var _month = 0;
    var _year = 0;
    if (!isDate(value, _format)) return null;
    var dmy = [];
    var seperator = '/';
    if (_format.toString().indexOf('/') > 0) seperator = '/';
    else if (_format.toString().indexOf('-') > 0) seperator = '-';
    else if (_format.toString().indexOf(' ') > 0) seperator = ' ';
    else if (_format.toString().indexOf('.') > 0) seperator = '.';
    else if (_format.toString().indexOf(',') > 0) seperator = ',';
    dmy = _format.toString().split(seperator);
    var dmyValue = [];
    dmyValue = value.toString().split(seperator);
    for (i = 0; i < dmy.length; i++) {
        switch (dmy[i]) {
            case 'd', 'dd': _day = parseInt(dmyValue[i]); break;
            case 'M', 'MM': _month = dmyValue[i].substring(0, 1) == '0' ? dmyValue[i].substring(1, 2) : dmyValue[i]; break;
            case 'yy', 'yyyy': _year = parseInt(dmyValue[i]); break;
        }
    }
    return new Date(_year, _month - 1, _day);
}

function Age(_bDay, _tDay) {
    var bday = _bDay.getDate();
    var bmo = (_bDay.getMonth());
    var byr = (_bDay.getFullYear());
    var age;
    var tday = _tDay.getDate();
    var tmo = (_tDay.getMonth());
    var tyr = (_tDay.getFullYear());

    age = tyr - byr;
    if (tmo < bmo || (tmo == bmo && tday < bday))
        age -= 1;
    return age;

}

function toJSONDate(date) {
    if (date == null) return null;
    else return '\/Date(' + date.getTime() + ')\/';
}

function convertJsonDateToDate(date) {
    if (date == null) return null;
    var returnDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0);
    return "\/Date(" + returnDate.getTime() + ")\/";
}

function ConvertToJulian(date) {
    //var now_utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
    var _date = new Date(parseInt(parseFloat(date.getTime() - (date.getTimezoneOffset() * 60 * 1000))));    
    return parseInt(parseFloat(DateToJulian(_date)));
    
}

function ConvertFromJulian(jDay) {
    return JulianToDate(jDay);
}

//gregorian-julian calendar conversion routines.  do not use with
//gregorian dates under 1500AD.  cerain events occurred which interrupted
//the calendar.
//algorithm from http://en.wikipedia.org/wiki/Julian_day

/*


Author: Jim Michaels <jmichae3@yahoo.com>
Abstract: function library for conversion and time extraction from Julian and 
Gregorian Dates and Javascript Date() objects.
Create Date:  , 2008
Current Date: Oct 24, 2009
Version 2.2


If you want to do Gregorian date differences based on Julian date differences, 
make sure you add 4713 to the year component. e.g. 
var y=GregorianY(jdiff)+4713;
everything else is normal.



Copyright 2008,2009 Jim Michaels

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
function fmod(a, b) {
    //return a/b - Math.floor(a/b);
    return a % b;
}
function fdiv(a, b) {
    return Math.floor(Math.floor(a) / Math.floor(b));
    //return a/b;
}

// epoch is julian(-4713,11,24,12,0,0); hours are 0..23, months are 1..12, days are 1..31, years start with -4713.
function GregorianToJulian(Y, Mo, D, H, Mi, S, ms) {
    var a = Math.floor((14 - Mo) / 12);
    var y = Y + 4800 - a;
    var m = Mo + (12 * a) - 3;
    return D + Math.floor((153 * m + 2) / 5) + (365 * y) + Math.floor(y / 4) - Math.floor(y / 100) + Math.floor(y / 400) - 32045 + ((H - 12) / 24) + (Mi / 1440) + (S / 86400) + (ms / 86400000);
}
function JulianFromDate(date) {
    var a = Math.floor((14 - (date.getMonth() + 1)) / 12);
    var y = date.getFullYear() + 4800 - a;
    var m = (date.getMonth() + 1) + (12 * a) - 3;
    return date.getDate() + Math.floor((153 * m + 2) / 5) + (365 * y) + Math.floor(y / 4) - Math.floor(y / 100) + Math.floor(y / 400) - 32045 + ((date.getHours() - 12) / 24) + (date.getMinutes() / 1440) + (date.getSeconds() / 86400) + (date.getMilliseconds() / 86400000);
}
function DateToJulian(date) {
    var a = Math.floor((14 - (date.getMonth() + 1)) / 12);
    var y = date.getFullYear() + 4800 - a;
    var m = (date.getMonth() + 1) + (12 * a) - 3;
    return date.getDate() + Math.floor((153 * m + 2) / 5) + (365 * y) + Math.floor(y / 4) - Math.floor(y / 100) + Math.floor(y / 400) - 32045 + ((date.getHours() - 12) / 24) + (date.getMinutes() / 1440) + (date.getSeconds() / 86400) + (date.getMilliseconds() / 86400000);
}
function DateToJulianNew(d) {
    var x = Math.floor((14 - d.getMonth()) / 12);
    var y = d.getFullYear() + 4800 - x;
    var z = d.getMonth() - 3 + 12 * x;
    var n = d.getDate() + Math.floor(((153 * z) + 2) / 5) + (365 * y) + Math.floor(y / 4) + Math.floor(y / 400) - Math.floor(y / 100) - 32045;
    return n;
}

function GregorianToDTime(Y, Mo, D, H, Mi, S, ms) {
    var a = Math.floor((14 - Mo) / 12);
    var y = Y + 4800 - a;
    var m = Mo + (12 * a) - 3;
    var JDN = D + Math.floor((153 * m + 2) / 5) + (365 * y) + Math.floor(y / 4) - Math.floor(y / 100) + Math.floor(y / 400) - 32045;
    return (JDN * 1000 * 60 * 60 * 24) + (H * 1000 * 60 * 60) + (Mi * 1000 * 60) + (S * 1000) + ms;
}
function DTimeFromDate(date) {
    var a = Math.floor((14 - (date.getMonth() + 1)) / 12);
    var y = date.getFullYear() + 4800 - a;
    var m = (date.getMonth() + 1) + (12 * a) - 3;
    var JDN = date.getDate() + Math.floor((153 * m + 2) / 5) + (365 * y) + Math.floor(y / 4) - Math.floor(y / 100) + Math.floor(y / 400) - 32045;
    return (JDN * 1000 * 60 * 60 * 24) + (date.getHours() * 1000 * 60 * 60) + (date.getMinutes() * 1000 * 60) + (date.getSeconds() * 1000) + date.getMilliseconds();
}
function DateToDTime(date) {
    var a = Math.floor((14 - (date.getMonth() + 1)) / 12);
    var y = date.getFullYear() + 4800 - a;
    var m = (date.getMonth() + 1) + (12 * a) - 3;
    var JDN = date.getDate() + Math.floor((153 * m + 2) / 5) + (365 * y) + Math.floor(y / 4) - Math.floor(y / 100) + Math.floor(y / 400) - 32045;
    return (JDN * 1000 * 60 * 60 * 24) + (date.getHours() * 1000 * 60 * 60) + (date.getMinutes() * 1000 * 60) + (date.getSeconds() * 1000) + date.getMilliseconds();
}

function to2DigitString(n) {
    var s = n.toString();
    if (s.length == 1) { s = '0' + s; }
    return s;
}

function DTimeToDate(dtime) {
    var J = Math.floor(dtime / (1000 * 60 * 60 * 24)); //shifts epoch 1/2 day
    var j = J + 32044; //shifts epoch back to astronomical year -4800
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a; //integer number of full years elapsed since March 1, 4801 BC at 00:00 UTC
    var m = fdiv((da * 5 + 308), 153) - 2; //integer number of full months elapsed since the last March 1 at 00:00 UTC
    var d = da - fdiv((m + 4) * 153, 5) + 122; //number of days elapsed since day 1 of the month at 00:00 UTC, including fractions of one day
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var t = dtime % (1000 * 60 * 60 * 24);
    var MS = fmod(t, 1000);
    t = fdiv(t, 1000);
    var SS = fmod(t, 60);
    t = fdiv(t, 60);
    var MM = fmod(t, 60);
    t = fdiv(t, 60);
    var HH = fmod(t, 24);
    t = fdiv(t, 24);

    var date = new Date(Y, Mo - 1, D, HH, MM, SS, MS);

    return date;
}
function JulianToDate(JDN) {
    var J = JDN + 0.5; //shifts epoch 1/2 day
    var j = J + 32044; //shifts epoch back to astronomical year -4800
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a; //integer number of full years elapsed since March 1, 4801 BC at 00:00 UTC
    var m = fdiv((da * 5 + 308), 153) - 2; //integer number of full months elapsed since the last March 1 at 00:00 UTC
    var d = da - fdiv((m + 4) * 153, 5) + 122; //number of days elapsed since day 1 of the month at 00:00 UTC, including fractions of one day
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var t = J - Math.floor(J);
    var HH = fmod(24 * t + 12, 24);
    t = fdiv(24 * t + 12, 24);
    var MM = fmod(60 * t, 60);
    t = fdiv(60 * t, 60);
    var SS = fmod(60 * t, 60);
    t = fdiv(60 * t, 60);
    var MS = fmod(1000 * t, 1000);
    t = fdiv(1000 * t, 1000);

    var date = new Date(Y, Mo - 1, D, HH, MM, SS, MS);

    return date;
}
function DateFromJulian(JDN) {
    var J = JDN + 0.5;
    var j = J + 32044;
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a;
    var m = fdiv((da * 5 + 308), 153) - 2;
    var d = da - fdiv((m + 4) * 153, 5) + 122;
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var t = J - Math.floor(J);
    var HH = fmod(24 * t + 12, 24);
    t = fdiv(24 * t + 12, 24);
    var MM = fmod(60 * t, 60);
    t = fdiv(60 * t, 60);
    var SS = fmod(60 * t, 60);
    t = fdiv(60 * t, 60);
    var MS = fmod(1000 * t, 1000);
    t = fdiv(1000 * t, 1000);

    var date = new Date(Y, Mo - 1, D, HH, MM, SS, MS);

    return date;
}
function DateFromDTime(dtime) {
    var J = Math.floor(dtime / (1000 * 60 * 60 * 24)); //shifts epoch 1/2 day
    var j = J + 32044; //shifts epoch back to astronomical year -4800
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a; //integer number of full years elapsed since March 1, 4801 BC at 00:00 UTC
    var m = fdiv((da * 5 + 308), 153) - 2; //integer number of full months elapsed since the last March 1 at 00:00 UTC
    var d = da - fdiv((m + 4) * 153, 5) + 122; //number of days elapsed since day 1 of the month at 00:00 UTC, including fractions of one day
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var t = dtime % (1000 * 60 * 60 * 24);
    var MS = fmod(t, 1000);
    t = fdiv(t, 1000);
    var SS = fmod(t, 60);
    t = fdiv(t, 60);
    var MM = fmod(t, 60);
    t = fdiv(t, 60);
    var HH = fmod(t, 24);
    t = fdiv(t, 24);

    var date = new Date(Y, Mo - 1, D, HH, MM, SS, MS);

    return date;
}

function JulianToGregorianY(JDN) {
    var J = JDN + 0.5;
    var j = J + 32044;
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a;
    var m = fdiv((da * 5 + 308), 153) - 2;
    var d = da - fdiv((m + 4) * 153, 5) + 122;
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var H = (d - Math.floor(d)) * 24;
    var Mi = (H - Math.floor(H)) * 60;
    var S = (Mi - Math.floor(Mi)) * 60;
    var Ms = (S - Math.floor(S)) * 1000;
    return Y;
}
function DTimeToGregorianY(dtime) {
    var J = Math.floor(dtime / (1000 * 60 * 60 * 24)); //shifts epoch 1/2 day
    var j = J + 32044; //shifts epoch back to astronomical year -4800
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a; //integer number of full years elapsed since March 1, 4801 BC at 00:00 UTC
    var m = fdiv((da * 5 + 308), 153) - 2; //integer number of full months elapsed since the last March 1 at 00:00 UTC
    var d = da - fdiv((m + 4) * 153, 5) + 122; //number of days elapsed since day 1 of the month at 00:00 UTC, including fractions of one day
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var t = dtime % (1000 * 60 * 60 * 24);
    var MS = fmod(t, 1000);
    t = fdiv(t, 1000);
    var SS = fmod(t, 60);
    t = fdiv(t, 60);
    var MM = fmod(t, 60);
    t = fdiv(t, 60);
    var HH = fmod(t, 24);
    t = fdiv(t, 24);
    return Y;
}

function JulianToGregorianMo(JDN) {
    var J = JDN + 0.5;
    var j = J + 32044;
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a;
    var m = fdiv((da * 5 + 308), 153) - 2;
    var d = da - fdiv((m + 4) * 153, 5) + 122;
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var H = (d - Math.floor(d)) * 24;
    var Mi = (H - Math.floor(H)) * 60;
    var S = (Mi - Math.floor(Mi)) * 60;
    var Ms = (S - Math.floor(S)) * 1000;
    return Mo;
}
function DTimeToGregorianMo(dtime) {
    var J = Math.floor(dtime / (1000 * 60 * 60 * 24)); //shifts epoch 1/2 day
    var j = J + 32044; //shifts epoch back to astronomical year -4800
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a; //integer number of full years elapsed since March 1, 4801 BC at 00:00 UTC
    var m = fdiv((da * 5 + 308), 153) - 2; //integer number of full months elapsed since the last March 1 at 00:00 UTC
    var d = da - fdiv((m + 4) * 153, 5) + 122; //number of days elapsed since day 1 of the month at 00:00 UTC, including fractions of one day
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var t = dtime % (1000 * 60 * 60 * 24);
    var MS = fmod(t, 1000);
    t = fdiv(t, 1000);
    var SS = fmod(t, 60);
    t = fdiv(t, 60);
    var MM = fmod(t, 60);
    t = fdiv(t, 60);
    var HH = fmod(t, 24);
    t = fdiv(t, 24);
    return Mo;
}

function JulianToGregorianD(JDN) {
    var J = JDN + 0.5;
    var j = J + 32044;
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a;
    var m = fdiv((da * 5 + 308), 153) - 2;
    var d = da - fdiv((m + 4) * 153, 5) + 122;
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var H = (d - Math.floor(d)) * 24;
    var Mi = (H - Math.floor(H)) * 60;
    var S = (Mi - Math.floor(Mi)) * 60;
    var Ms = (S - Math.floor(S)) * 1000;
    return Math.floor(D);
}
function DTimeToGregorianD(dtime) {
    var J = Math.floor(dtime / (1000 * 60 * 60 * 24)); //shifts epoch 1/2 day
    var j = J + 32044; //shifts epoch back to astronomical year -4800
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a; //integer number of full years elapsed since March 1, 4801 BC at 00:00 UTC
    var m = fdiv((da * 5 + 308), 153) - 2; //integer number of full months elapsed since the last March 1 at 00:00 UTC
    var d = da - fdiv((m + 4) * 153, 5) + 122; //number of days elapsed since day 1 of the month at 00:00 UTC, including fractions of one day
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var t = dtime % (1000 * 60 * 60 * 24);
    var MS = fmod(t, 1000);
    t = fdiv(t, 1000);
    var SS = fmod(t, 60);
    t = fdiv(t, 60);
    var MM = fmod(t, 60);
    t = fdiv(t, 60);
    var HH = fmod(t, 24);
    t = fdiv(t, 24);
    return Math.floor(D);
}

function JulianToGregorianH(JDN) {
    var J = JDN + 0.5;
    var j = J + 32044;
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a;
    var m = fdiv((da * 5 + 308), 153) - 2;
    var d = da - fdiv((m + 4) * 153, 5) + 122;
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var H = (d - Math.floor(d)) * 24;
    var Mi = (H - Math.floor(H)) * 60;
    var S = (Mi - Math.floor(Mi)) * 60;
    var Ms = (S - Math.floor(S)) * 1000;
    return Math.floor(H);
}
function DTimeToGregorianH(dtime) {
    var J = Math.floor(dtime / (1000 * 60 * 60 * 24)); //shifts epoch 1/2 day
    var j = J + 32044; //shifts epoch back to astronomical year -4800
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a; //integer number of full years elapsed since March 1, 4801 BC at 00:00 UTC
    var m = fdiv((da * 5 + 308), 153) - 2; //integer number of full months elapsed since the last March 1 at 00:00 UTC
    var d = da - fdiv((m + 4) * 153, 5) + 122; //number of days elapsed since day 1 of the month at 00:00 UTC, including fractions of one day
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var t = dtime % (1000 * 60 * 60 * 24);
    var MS = fmod(t, 1000);
    t = fdiv(t, 1000);
    var SS = fmod(t, 60);
    t = fdiv(t, 60);
    var MM = fmod(t, 60);
    t = fdiv(t, 60);
    var HH = fmod(t, 24);
    t = fdiv(t, 24);
    return Math.floor(HH);
}

function JulianToGregorianMi(JDN) {
    var J = JDN + 0.5;
    var j = J + 32044;
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a;
    var m = fdiv((da * 5 + 308), 153) - 2;
    var d = da - fdiv((m + 4) * 153, 5) + 122;
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var H = (d - Math.floor(d)) * 24;
    var Mi = (H - Math.floor(H)) * 60;
    var S = (Mi - Math.floor(Mi)) * 60;
    var Ms = (S - Math.floor(S)) * 1000;
    return Math.floor(Mi);
}
function DTimeToGregorianMi(dtime) {
    var J = Math.floor(dtime / (1000 * 60 * 60 * 24)); //shifts epoch 1/2 day
    var j = J + 32044; //shifts epoch back to astronomical year -4800
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a; //integer number of full years elapsed since March 1, 4801 BC at 00:00 UTC
    var m = fdiv((da * 5 + 308), 153) - 2; //integer number of full months elapsed since the last March 1 at 00:00 UTC
    var d = da - fdiv((m + 4) * 153, 5) + 122; //number of days elapsed since day 1 of the month at 00:00 UTC, including fractions of one day
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var t = dtime % (1000 * 60 * 60 * 24);
    var MS = fmod(t, 1000);
    t = fdiv(t, 1000);
    var SS = fmod(t, 60);
    t = fdiv(t, 60);
    var MM = fmod(t, 60);
    t = fdiv(t, 60);
    var HH = fmod(t, 24);
    t = fdiv(t, 24);
    return Math.floor(MM);
}

function JulianToGregorianS(JDN) {
    var J = JDN + 0.5;
    var j = J + 32044;
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a;
    var m = fdiv((da * 5 + 308), 153) - 2;
    var d = da - fdiv((m + 4) * 153, 5) + 122;
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var H = (d - Math.floor(d)) * 24;
    var Mi = (H - Math.floor(H)) * 60;
    var S = (Mi - Math.floor(Mi)) * 60;
    var Ms = (S - Math.floor(S)) * 1000;
    return Math.floor(S);
}
function DTimeToGregorianS(dtime) {
    var J = Math.floor(dtime / (1000 * 60 * 60 * 24)); //shifts epoch 1/2 day
    var j = J + 32044; //shifts epoch back to astronomical year -4800
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a; //integer number of full years elapsed since March 1, 4801 BC at 00:00 UTC
    var m = fdiv((da * 5 + 308), 153) - 2; //integer number of full months elapsed since the last March 1 at 00:00 UTC
    var d = da - fdiv((m + 4) * 153, 5) + 122; //number of days elapsed since day 1 of the month at 00:00 UTC, including fractions of one day
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var t = dtime % (1000 * 60 * 60 * 24);
    var MS = fmod(t, 1000);
    t = fdiv(t, 1000);
    var SS = fmod(t, 60);
    t = fdiv(t, 60);
    var MM = fmod(t, 60);
    t = fdiv(t, 60);
    var HH = fmod(t, 24);
    t = fdiv(t, 24);
    return Math.floor(SS);
}

function JulianToGregorianMs(JDN) {
    var J = JDN + 0.5;
    var j = J + 32044;
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a;
    var m = fdiv((da * 5 + 308), 153) - 2;
    var d = da - fdiv((m + 4) * 153, 5) + 122;
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var H = (d - Math.floor(d)) * 24;
    var Mi = (H - Math.floor(H)) * 60;
    var S = (Mi - Math.floor(Mi)) * 60;
    var Ms = (S - Math.floor(S)) * 1000;
    return Math.floor(Ms);
}
function DTimeToGregorianMs(dtime) {
    var J = Math.floor(dtime / (1000 * 60 * 60 * 24)); //shifts epoch 1/2 day
    var j = J + 32044; //shifts epoch back to astronomical year -4800
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a; //integer number of full years elapsed since March 1, 4801 BC at 00:00 UTC
    var m = fdiv((da * 5 + 308), 153) - 2; //integer number of full months elapsed since the last March 1 at 00:00 UTC
    var d = da - fdiv((m + 4) * 153, 5) + 122; //number of days elapsed since day 1 of the month at 00:00 UTC, including fractions of one day
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var t = dtime % (1000 * 60 * 60 * 24);
    var MS = fmod(t, 1000);
    t = fdiv(t, 1000);
    var SS = fmod(t, 60);
    t = fdiv(t, 60);
    var MM = fmod(t, 60);
    t = fdiv(t, 60);
    var HH = fmod(t, 24);
    t = fdiv(t, 24);
    return Math.floor(MS);
}

function JulianToGregorianFracSec(JDN) {
    var J = JDN + 0.5;
    var j = J + 32044;
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a;
    var m = fdiv((da * 5 + 308), 153) - 2;
    var d = da - fdiv((m + 4) * 153, 5) + 122;
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var H = (d - Math.floor(d)) * 24;
    var Mi = (H - Math.floor(H)) * 60;
    var S = (Mi - Math.floor(Mi)) * 60;
    var fracSec = S - Math.floor(S);
    return fracSec;
}
function DTimeToGregorianFracSec(dtime) {
    var J = Math.floor(dtime / (1000 * 60 * 60 * 24)); //shifts epoch 1/2 day
    var j = J + 32044; //shifts epoch back to astronomical year -4800
    var g = fdiv(j, 146097);
    var dg = fmod(j, 146097);
    var c = fdiv((fdiv(dg, 36524) + 1) * 3, 4);
    var dc = dg - (c * 36524);
    var b = fdiv(dc, 1461);
    var db = fmod(dc, 1461);
    var a = fdiv((fdiv(db, 365) + 1) * 3, 4);
    var da = db - (a * 365);
    var y = (g * 400) + (c * 100) + (b * 4) + a; //integer number of full years elapsed since March 1, 4801 BC at 00:00 UTC
    var m = fdiv((da * 5 + 308), 153) - 2; //integer number of full months elapsed since the last March 1 at 00:00 UTC
    var d = da - fdiv((m + 4) * 153, 5) + 122; //number of days elapsed since day 1 of the month at 00:00 UTC, including fractions of one day
    var Y = y - 4800 + fdiv((m + 2), 12);
    var Mo = fmod((m + 2), 12) + 1;
    var D = d + 1;
    var t = dtime % (1000 * 60 * 60 * 24);
    var MS = fmod(t, 1000);
    t = fdiv(t, 1000);
    var SS = fmod(t, 60);
    t = fdiv(t, 60);
    var MM = fmod(t, 60);
    t = fdiv(t, 60);
    var HH = fmod(t, 24);
    t = fdiv(t, 24);
    return SS + (MS / 1000);
}
function Julian1Day() { //multipler or divisor for 1 day
    return 1;
}
function DTime1Day() { //multipler or divisor for 1 day
    return 1000 * 60 * 60 * 24;
}
function GregorianDiffWithEpoch(yr1, mo1, dy1, hr1, mi1, sec1, ms1, yr2, mo2, dy2, hr2, mi2, sec2, ms2) {
    var dt1 = GregorianToDTime(yr1, mo1, dy1, hr1, mi1, sec1, ms1);
    var dt2 = GregorianToDTime(yr2, mo2, dy2, hr2, mi2, sec2, ms2);
    var epoch = GregorianToDTime(0, 1, 1, 12, 0, 0, 0);
    return dt1 - dt2 + epoch;
}
function GregorianDiffWithoutEpoch(yr1, mo1, dy1, hr1, mi1, sec1, ms1, yr2, mo2, dy2, hr2, mi2, sec2, ms2) {
    var dt1 = GregorianToDTime(yr1, mo1, dy1, hr1, mi1, sec1, ms1);
    var dt2 = GregorianToDTime(yr2, mo2, dy2, hr2, mi2, sec2, ms2);
    return dt1 - dt2;
}

function zeropad(n, numdigits) {
    var s = "";
    var i;
    if (numdigits - n.toString().length >= 1) {
        for (i = 1; i <= numdigits - n.toString().length; i++) {
            s += '0';
        }
        return s + n.toString();
    } else {
        return n.toString();
    }
}


// ===================================================================
// Author: Matt Kruse <matt@mattkruse.com>
// WWW: http://www.mattkruse.com/
//
// NOTICE: You may use this code for any purpose, commercial or
// private, without any further permission from the author. You may
// remove this notice from your final code if you wish, however it is
// appreciated by the author if at least my web site address is kept.
//
// You may *NOT* re-distribute this code in any way except through its
// use. That means, you can include it in your product, or your web
// site, or any other form where the code is actually being used. You
// may not put the plain javascript up on your site for download or
// include it in your javascript libraries for download. 
// If you wish to share this code with others, please just point them
// to the URL instead.
// Please DO NOT link directly to my .js files from your site. Copy
// the files to your server and use them there. Thank you.
// ===================================================================

// HISTORY
// ------------------------------------------------------------------
// May 17, 2003: Fixed bug in parseDate() for dates <1970
// March 11, 2003: Added parseDate() function
// March 11, 2003: Added "NNN" formatting option. Doesn't match up
//                 perfectly with SimpleDateFormat formats, but 
//                 backwards-compatability was required.

// ------------------------------------------------------------------
// These functions use the same 'format' strings as the 
// java.text.SimpleDateFormat class, with minor exceptions.
// The format string consists of the following abbreviations:
// 
// Field        | Full Form          | Short Form
// -------------+--------------------+-----------------------
// Year         | yyyy (4 digits)    | yy (2 digits), y (2 or 4 digits)
// Month        | MMM (name or abbr.)| MM (2 digits), M (1 or 2 digits)
//              | NNN (abbr.)        |
// Day of Month | dd (2 digits)      | d (1 or 2 digits)
// Day of Week  | EE (name)          | E (abbr)
// Hour (1-12)  | hh (2 digits)      | h (1 or 2 digits)
// Hour (0-23)  | HH (2 digits)      | H (1 or 2 digits)
// Hour (0-11)  | KK (2 digits)      | K (1 or 2 digits)
// Hour (1-24)  | kk (2 digits)      | k (1 or 2 digits)
// Minute       | mm (2 digits)      | m (1 or 2 digits)
// Second       | ss (2 digits)      | s (1 or 2 digits)
// AM/PM        | a                  |
//
// NOTE THE DIFFERENCE BETWEEN MM and mm! Month=MM, not mm!
// Examples:
//  "MMM d, y" matches: January 01, 2000
//                      Dec 1, 1900
//                      Nov 20, 00
//  "M/d/yy"   matches: 01/20/00
//                      9/2/00
//  "MMM dd, yyyy hh:mm:ssa" matches: "January 01, 2000 12:30:45AM"
// ------------------------------------------------------------------

var MONTH_NAMES = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
var DAY_NAMES = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
function LZ(x) { return (x < 0 || x > 9 ? "" : "0") + x }

// ------------------------------------------------------------------
// isDate ( date_string, format_string )
// Returns true if date string matches format of format string and
// is a valid date. Else returns false.
// It is recommended that you trim whitespace around the value before
// passing it to this function, as whitespace is NOT ignored!
// ------------------------------------------------------------------
function isDate(val, format) {
    var date = getDateFromFormat(val, format);
    if (date == 0) { return false; }
    return true;
}

// -------------------------------------------------------------------
// compareDates(date1,date1format,date2,date2format)
//   Compare two date strings to see which is greater.
//   Returns:
//   1 if date1 is greater than date2
//   0 if date2 is greater than date1 of if they are the same
//  -1 if either of the dates is in an invalid format
// -------------------------------------------------------------------
function compareDates(date1, dateformat1, date2, dateformat2) {
    var d1 = getDateFromFormat(date1, dateformat1);
    var d2 = getDateFromFormat(date2, dateformat2);
    if (d1 == 0 || d2 == 0) {
        return -1;
    }
    else if (d1 > d2) {
        return 1;
    }
    return 0;
}

// ------------------------------------------------------------------
// formatDate (date_object, format)
// Returns a date in the output format specified.
// The format string uses the same abbreviations as in getDateFromFormat()
// ------------------------------------------------------------------
function formatDate(date, format) {
    format = format + "";
    var result = "";
    var i_format = 0;
    var c = "";
    var token = "";
    var y = date.getYear() + "";
    var M = date.getMonth() + 1;
    var d = date.getDate();
    var E = date.getDay();
    var H = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();
    var yyyy, yy, MMM, MM, dd, hh, h, mm, ss, ampm, HH, H, KK, K, kk, k;
    // Convert real date parts into formatted versions
    var value = new Object();
    if (y.length < 4) { y = "" + (y - 0 + 1900); }
    value["y"] = "" + y;
    value["yyyy"] = y;
    value["yy"] = y.substring(2, 4);
    value["M"] = M;
    value["MM"] = LZ(M);
    value["MMM"] = MONTH_NAMES[M - 1];
    value["NNN"] = MONTH_NAMES[M + 11];
    value["d"] = d;
    value["dd"] = LZ(d);
    value["E"] = DAY_NAMES[E + 7];
    value["EE"] = DAY_NAMES[E];
    value["H"] = H;
    value["HH"] = LZ(H);
    if (H == 0) { value["h"] = 12; }
    else if (H > 12) { value["h"] = H - 12; }
    else { value["h"] = H; }
    value["hh"] = LZ(value["h"]);
    if (H > 11) { value["K"] = H - 12; } else { value["K"] = H; }
    value["k"] = H + 1;
    value["KK"] = LZ(value["K"]);
    value["kk"] = LZ(value["k"]);
    if (H > 11) { value["a"] = "PM"; }
    else { value["a"] = "AM"; }
    value["m"] = m;
    value["mm"] = LZ(m);
    value["s"] = s;
    value["ss"] = LZ(s);
    while (i_format < format.length) {
        c = format.charAt(i_format);
        token = "";
        while ((format.charAt(i_format) == c) && (i_format < format.length)) {
            token += format.charAt(i_format++);
        }
        if (value[token] != null) { result = result + value[token]; }
        else { result = result + token; }
    }
    return result;
}

// ------------------------------------------------------------------
// Utility functions for parsing in getDateFromFormat()
// ------------------------------------------------------------------
function _isInteger(val) {
    var digits = "1234567890";
    for (var i = 0; i < val.length; i++) {
        if (digits.indexOf(val.charAt(i)) == -1) { return false; }
    }
    return true;
}
function _getInt(str, i, minlength, maxlength) {
    for (var x = maxlength; x >= minlength; x--) {
        var token = str.substring(i, i + x);
        if (token.length < minlength) { return null; }
        if (_isInteger(token)) { return token; }
    }
    return null;
}

// ------------------------------------------------------------------
// getDateFromFormat( date_string , format_string )
//
// This function takes a date string and a format string. It matches
// If the date string matches the format string, it returns the 
// getTime() of the date. If it does not match, it returns 0.
// ------------------------------------------------------------------
function getDateFromFormat(val, format) {
    val = val + "";
    format = format + "";
    var i_val = 0;
    var i_format = 0;
    var c = "";
    var token = "";
    var token2 = "";
    var x, y;
    var now = new Date();
    var year = now.getYear();
    var month = now.getMonth() + 1;
    var date = 1;
    var hh = now.getHours();
    var mm = now.getMinutes();
    var ss = now.getSeconds();
    var ampm = "";

    while (i_format < format.length) {
        // Get next token from format string
        c = format.charAt(i_format);
        token = "";
        while ((format.charAt(i_format) == c) && (i_format < format.length)) {
            token += format.charAt(i_format++);
        }
        // Extract contents of value based on format token
        if (token == "yyyy" || token == "yy" || token == "y") {
            if (token == "yyyy") { x = 4; y = 4; }
            if (token == "yy") { x = 2; y = 2; }
            if (token == "y") { x = 2; y = 4; }
            year = _getInt(val, i_val, x, y);
            if (year == null) { return 0; }
            i_val += year.length;
            if (year.length == 2) {
                if (year > 70) { year = 1900 + (year - 0); }
                else { year = 2000 + (year - 0); }
            }
        }
        else if (token == "MMM" || token == "NNN") {
            month = 0;
            for (var i = 0; i < MONTH_NAMES.length; i++) {
                var month_name = MONTH_NAMES[i];
                if (val.substring(i_val, i_val + month_name.length).toLowerCase() == month_name.toLowerCase()) {
                    if (token == "MMM" || (token == "NNN" && i > 11)) {
                        month = i + 1;
                        if (month > 12) { month -= 12; }
                        i_val += month_name.length;
                        break;
                    }
                }
            }
            if ((month < 1) || (month > 12)) { return 0; }
        }
        else if (token == "EE" || token == "E") {
            for (var i = 0; i < DAY_NAMES.length; i++) {
                var day_name = DAY_NAMES[i];
                if (val.substring(i_val, i_val + day_name.length).toLowerCase() == day_name.toLowerCase()) {
                    i_val += day_name.length;
                    break;
                }
            }
        }
        else if (token == "MM" || token == "M") {
            month = _getInt(val, i_val, token.length, 2);
            if (month == null || (month < 1) || (month > 12)) { return 0; }
            i_val += month.length;
        }
        else if (token == "dd" || token == "d") {
            date = _getInt(val, i_val, token.length, 2);
            if (date == null || (date < 1) || (date > 31)) { return 0; }
            i_val += date.length;
        }
        else if (token == "hh" || token == "h") {
            hh = _getInt(val, i_val, token.length, 2);
            if (hh == null || (hh < 1) || (hh > 12)) { return 0; }
            i_val += hh.length;
        }
        else if (token == "HH" || token == "H") {
            hh = _getInt(val, i_val, token.length, 2);
            if (hh == null || (hh < 0) || (hh > 23)) { return 0; }
            i_val += hh.length;
        }
        else if (token == "KK" || token == "K") {
            hh = _getInt(val, i_val, token.length, 2);
            if (hh == null || (hh < 0) || (hh > 11)) { return 0; }
            i_val += hh.length;
        }
        else if (token == "kk" || token == "k") {
            hh = _getInt(val, i_val, token.length, 2);
            if (hh == null || (hh < 1) || (hh > 24)) { return 0; }
            i_val += hh.length; hh--;
        }
        else if (token == "mm" || token == "m") {
            mm = _getInt(val, i_val, token.length, 2);
            if (mm == null || (mm < 0) || (mm > 59)) { return 0; }
            i_val += mm.length;
        }
        else if (token == "ss" || token == "s") {
            ss = _getInt(val, i_val, token.length, 2);
            if (ss == null || (ss < 0) || (ss > 59)) { return 0; }
            i_val += ss.length;
        }
        else if (token == "a") {
            if (val.substring(i_val, i_val + 2).toLowerCase() == "am") { ampm = "AM"; }
            else if (val.substring(i_val, i_val + 2).toLowerCase() == "pm") { ampm = "PM"; }
            else { return 0; }
            i_val += 2;
        }
        else {
            if (val.substring(i_val, i_val + token.length) != token) { return 0; }
            else { i_val += token.length; }
        }
    }
    // If there are any trailing characters left in the value, it doesn't match
    if (i_val != val.length) { return 0; }
    // Is date valid for month?
    if (month == 2) {
        // Check for leap year
        if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) { // leap year
            if (date > 29) { return 0; }
        }
        else { if (date > 28) { return 0; } }
    }
    if ((month == 4) || (month == 6) || (month == 9) || (month == 11)) {
        if (date > 30) { return 0; }
    }
    // Correct hours value
    if (hh < 12 && ampm == "PM") { hh = hh - 0 + 12; }
    else if (hh > 11 && ampm == "AM") { hh -= 12; }
    var newdate = new Date(year, month - 1, date, hh, mm, ss);
    return newdate.getTime();
}

// ------------------------------------------------------------------
// parseDate( date_string [, prefer_euro_format] )
//
// This function takes a date string and tries to match it to a
// number of possible date formats to get the value. It will try to
// match against the following international formats, in this order:
// y-M-d   MMM d, y   MMM d,y   y-MMM-d   d-MMM-y  MMM d
// M/d/y   M-d-y      M.d.y     MMM-d     M/d      M-d
// d/M/y   d-M-y      d.M.y     d-MMM     d/M      d-M
// A second argument may be passed to instruct the method to search
// for formats like d/M/y (european format) before M/d/y (American).
// Returns a Date object or null if no patterns match.
// ------------------------------------------------------------------
function parseDate(val) {
    var preferEuro = (arguments.length == 2) ? arguments[1] : false;
    generalFormats = new Array('y-M-d', 'MMM d, y', 'MMM d,y', 'y-MMM-d', 'd-MMM-y', 'MMM d');
    monthFirst = new Array('M/d/y', 'M-d-y', 'M.d.y', 'MMM-d', 'M/d', 'M-d');
    dateFirst = new Array('d/M/y', 'd-M-y', 'd.M.y', 'd-MMM', 'd/M', 'd-M');
    var checkList = new Array('generalFormats', preferEuro ? 'dateFirst' : 'monthFirst', preferEuro ? 'monthFirst' : 'dateFirst');
    var d = null;
    for (var i = 0; i < checkList.length; i++) {
        var l = window[checkList[i]];
        for (var j = 0; j < l.length; j++) {
            d = getDateFromFormat(val, l[j]);
            if (d != 0) { return new Date(d); }
        }
    }
    return null;
}