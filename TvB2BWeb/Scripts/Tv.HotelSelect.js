﻿function showHotelList(msg, btnOK, btnCancel) {
    $(function() {
        $("#messages").html(msg);
        $("#dialog").dialog('destroy');
        var _buttons = {};
        var ok = btnOK.toString();
        var cancel = btnCancel.toString();
        _buttons[ok] = function() {            
            $(this).dialog('close');
            //$("#dialog").dialog('destroy');
            return true;
        };
        _buttons[cancel] = function() {
            $(this).dialog('close');
            //$("#dialog").dialog('destroy');
            return true;
        };
        $("#dialog-HotelList").dialog({
            modal: true,
            height: 400,
            buttons: _buttons
        });
    });
}