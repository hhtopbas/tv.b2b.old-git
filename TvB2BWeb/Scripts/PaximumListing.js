﻿function paxList(pOption) {

    var instance = this;

    //Variables
    this.token = pOption.token;
    this.apiAddress = pOption.apiAddress;
    this.mediaAddress = pOption.mediaAddress;
    this.filters = { cities: [], stars: [], hotels: [], rooms: [], boards: [] };
    this.filterMap = { city: {}, star: {}, room: {}, board: {} }
    this.hotels = null;
    this.filteredHotels = null;
    this.debugMode = false;
    this.utils = new _utils();
    this.rowCountPerPage = pOption.rowCountPerPage;
    this.paginator = null
    this.localisation = null;

    //Init Events
    this.init = function (pDebugMode) {
        //Filter
        $('#cmbArrival').change(instance.setFilteredHotels);
        $('#cmbCategory').change(instance.setFilteredHotels);
        $('#cmbHotel').change(instance.setFilteredHotels);
        $('#cmbRoom').change(instance.setFilteredHotels);
        $('#cmbBoard').change(instance.setFilteredHotels);
        $('#chkSpecial').change(instance.setFilteredHotels)
        //Sort
        $('#cmbSort').change(instance.setFilteredHotels);

        if (pDebugMode != null && pDebugMode)
            this.debugMode = pDebugMode;

        return instance;
    };

    //Set Localisations
    this.setLocalisation = function (pData) {
        this.localisation = pData;
        return instance;
    }

    //Get Hotels
    this.getHotels = function (pCriteria) {
        instance.utils.interception.searchingStart();
        if (instance.debugMode) {
            $.ajax({
                type: "GET",
                url: "/hotels.json",
                dataType: "json",
                success: function (data) {
                    instance.utils.interception.searchingEnd();
                    instance.setHotels(data);
                },
                error: function (ex) {
                    console.log(ex);
                }
            });
        }
        else {
            $.ajax({
                url: instance.apiAddress + '/hotels',
                method: 'POST',
                type: 'POST',
                contentType: 'application/json; charset=UTF-8',
                data: JSON.stringify(pCriteria),
                success: function (data) {
                    instance.utils.interception.searchingEnd();
                    instance.setHotels(data.hotels);
                },
                error: function (ex) {
                    console.log(ex);
                },
                beforeSend: function (xhr, settings) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + instance.token);
                }
            });
        }

        return instance;
    };

    this.setHotels = function (pHotels) {
        if (pHotels != null && pHotels.length > 0) {
            instance.utils.interception.progressingStart();
            this.hotels = _.map(pHotels, function (m) {
                var bestOffer = _.take(m.offers)[0];
                m.offerId = bestOffer.id;
                m.price = bestOffer.price.amount;
                m.currency = bestOffer.price.currency;
                m.isSpecial = _.some(m.offers, ['isSpecial', true])
                return m;
            });
            this.setFilters().renderFilters();
            instance.utils.interception.progressingEnd();

            $('.paximum-listing').show();


            instance.renderHotels();
        }
        else {
            alert(instance.localisation.hotelNotFoundMessage);
            $('paximum-listing').hide();
        }
        return this;
    };

    this.setFilters = function () {
        var _cities = {}, _stars = {}, _rooms = {}, _boards = {};

        //Root
        $.each(this.hotels, function (index, hotel) {
            //City
            if (instance.filterMap.city[hotel.city.id] == null) {
                instance.filterMap.city[hotel.city.id] = [hotel.id];
                instance.filters.cities.push({ id: hotel.city.id, name: instance.utils.toTitleCase(hotel.city.name) });
            } else if (instance.filterMap.city[hotel.city.id].indexOf(hotel.id) == -1) {
                instance.filterMap.city[hotel.city.id].push(hotel.id);
            }
            //Stars
            if (instance.filterMap.star[hotel.stars] == null) {
                instance.filterMap.star[hotel.stars] = [hotel.id];
                instance.filters.stars.push({ id: hotel.stars, name: hotel.stars });
            } else if (instance.filterMap.star[hotel.stars].indexOf(hotel.id) == -1) {
                instance.filterMap.star[hotel.stars].push(hotel.id);
            }
            //Hotels
            instance.filters.hotels.push({ id: hotel.id, name: hotel.name });
            //Rooms & Boards
            $.each(hotel.offers, function (index, offer) {
                //Rooms
                $.each(offer.rooms, function (index, room) {
                    if (instance.filterMap.room[room.typeId] == null) {
                        instance.filterMap.room[room.typeId] = [hotel.id];
                        instance.filters.rooms.push({ id: room.typeId, name: instance.utils.toTitleCase(room.type) });
                    } else if (instance.filterMap.room[room.typeId].indexOf(hotel.id) == -1) {
                        instance.filterMap.room[room.typeId].push(hotel.id);
                    };
                });
                //Boards
                if (instance.filterMap.board[offer.boardId] == null) {
                    instance.filterMap.board[offer.boardId] = [hotel.id];
                    instance.filters.boards.push({ id: offer.boardId, name: instance.utils.toTitleCase(offer.board) });
                } else if (instance.filterMap.board[offer.boardId].indexOf(hotel.id) == -1) {
                    instance.filterMap.board[offer.boardId].push(hotel.id);
                }
            });
        });

        return instance;
    };

    this.renderFilters = function () {
        //Arrival
        $('#cmbArrival').empty().append($('<option/>', { value: '0', text: 'Hepsi' }));
        $.each(_.sortBy(this.filters.cities, 'name'), function (index, item) {
            $('#cmbArrival').append($('<option/>', { value: item.id, text: item.name + ' (' + instance.filterMap.city[item.id].length + ')' }));
        });
        //Category
        $('#cmbCategory').empty().append($('<option/>', { value: '0', text: 'Hepsi' }));
        $.each(_.sortBy(this.filters.stars, 'name'), function (index, item) {
            $('#cmbCategory').append($('<option/>', { value: item.id, text: item.name + ' Star (' + instance.filterMap.star[item.id].length + ')' }));
        });
        //Hotel
        $('#cmbHotel').empty().append($('<option/>', { value: '0', text: 'Hepsi' }));
        $.each(_.sortBy(this.filters.hotels, 'name'), function (index, item) {
            $('#cmbHotel').append($('<option/>', { value: item.id, text: item.name }));
        });
        //Room
        $('#cmbRoom').empty().append($('<option/>', { value: '0', text: 'Hepsi' }));
        $.each(_.sortBy(this.filters.rooms, 'name'), function (index, item) {
            $('#cmbRoom').append($('<option/>', { value: item.id, text: item.name + ' (' + instance.filterMap.room[item.id].length + ')' }));
        });
        //Board
        $('#cmbBoard').empty().append($('<option/>', { value: '-1', text: 'Hepsi' }));
        $.each(_.sortBy(this.filters.boards, 'name'), function (index, item) {
            $('#cmbBoard').append($('<option/>', { value: item.id, text: item.name + ' (' + instance.filterMap.board[item.id].length + ')' }));
        });

        return instance;
    };

    this.setFilteredHotels = function () {

        instance.utils.interception.filteringStart();

        instance.filteredHotels = _.cloneDeep(instance.hotels);

        instance.filteredHotels = _.map(instance.filteredHotels, function (m) {

            var isSpecial = false;
            m.offers = _.filter(m.offers, function (f) {
                //IsSpecial
                if (f.isSpecial) isSpecial = true;
                //Board
                var _board = false;
                if ($('#cmbBoard').val() == '-1')
                    _board = true;
                else
                    _board = (f.boardId == $('#cmbBoard').val());
                //Room
                var _room = false;
                if ($('#cmbRoom').val() == '0')
                    _room = true;
                else
                    _room = _.some(f.rooms, ['typeId', $('#cmbRoom').val()])

                return (_board && _room)
            });

            m.isSpecial = isSpecial;

            var bestOffer = m.offers[0];

            if (bestOffer != null) {
                m.offerId = bestOffer.id;
                m.price = bestOffer.price.amount;
                m.currency = bestOffer.price.currency;
                return m;
            }
            else return null;
        });

        instance.filteredHotels = _.filter(instance.filteredHotels, function (f) {
            //Null Check
            if (f == null) return false;
            //Arrival
            var _arrival = false;
            if ($('#cmbArrival').val() == '0')
                _arrival = true;
            else
                _arrival = (f.city.id == $('#cmbArrival').val());

            //Category
            var _category = false;
            if ($('#cmbCategory').val() == '0')
                _category = true;
            else
                _category = (f.stars == $('#cmbCategory').val());
            //Hotel
            var _hotel = false;
            if ($('#cmbHotel').val() == '0')
                _hotel = true;
            else
                _hotel = (f.id == $('#cmbHotel').val());
            //And Check
            return (_arrival && _category && _hotel);
        });


        instance.renderHotels();

        instance.utils.interception.filteringEnd();

        return instance;
    }

    this.renderHotels = function () {
        var _hotels = null;

        var sortCode = $('#cmbSort').val()

        if (instance.filteredHotels != null) {
            instance.filteredHotels = _.orderBy(instance.filteredHotels, ['price'], [sortCode]);
            _hotels = instance.filteredHotels;
        }
        else {
            instance.hotels = _.orderBy(instance.hotels, ['price'], [sortCode]);
            _hotels = instance.hotels;
        }

        $('#spnRecordCount').html(_hotels.length);

        if (_hotels.length == 0) {
            $('#response').empty();
            instance.paginator = null;
        } else {
            instance.paginator = $('#divPaginator').smartpaginator({
                totalrecords: _hotels.length,
                recordsperpage: 10,
                initval: 0,
                next: instance.localisation.next,
                prev: instance.localisation.previous,
                first: instance.localisation.first,
                last: instance.localisation.last,
                theme: 'green',
                onchange: instance.changePage
            });
            instance.changePage(1);
        }

        return instance;
    };

    this.changePage = function (page) {
        $('#response').empty();
        instance.renderPage(page);
        $('html, body').animate({
            scrollTop: $("body").offset().top
        }, 1);
        self.parent.reSizeResultFrame(document.body.offsetHeight + 25);
    };

    this.renderPage = function (pPage) {
        instance.utils.interception.listingStart();
        var _hotels = null;

        if (instance.filteredHotels != null)
            _hotels = instance.filteredHotels;
        else
            _hotels = instance.hotels;

        for (var i = ((pPage - 1) * instance.rowCountPerPage) ; i < (instance.rowCountPerPage * pPage) ; i++) {
            var hotel = _hotels[i];

            if (hotel != null) {

                $('<div/>', { 'class': 'hotel clearfix' }).append(
                    $('<div/>', { 'class': 'hotel-image no-image', 'html': (hotel.isSpecial ? '<span class="hotel-image-special-offer">' + instance.localisation.specialOffer + '</span>' : '') }).append(
                        (hotel.thumbnail != null && hotel.thumbnail != '') ? $('<img/>', { 'src': instance.mediaAddress + hotel.thumbnail }) : ''
                    )
                ).append(
                    $('<div/>', { 'class': 'hotel-info' }).append(
                        $('<div/>', { 'class': 'info-hotelname', 'html': hotel.name })
                    ).append(
                        $('<div/>', { 'class': 'info-hotelcategory' }).append(instance.renderStars(hotel.stars))
                    ).append(
                        $('<div/>', { 'class': 'info-location', 'html': hotel.city.name + ', ' + hotel.country.name })
                    ).append(
                        $('<div/>', { 'class': 'info-presentation', 'html': hotel.description })
                    )
                ).append(
                    $('<div/>', { 'class': 'hotel-booking' }).append(
                        $('<div/>', { 'class': 'booking-price' }).append(
                            $('<div/>', { 'class': 'price-amount', 'html': hotel.price.toFixed(2) })
                        ).append(
                            $('<div/>', { 'class': 'price-currency', 'html': hotel.currency })
                        )
                    ).append( 
                        $('<div/>', { 'class': 'booking-book' }).append(
                            $('<button/>', { 'href':'#','html': instance.localisation.reservation, 'data-hotel': hotel.id, 'data-offer': hotel.offerId }).on('click', instance.goReservation)
                            // button -> span 2016-05-09
                        )
                    )
                ).append(
                    $('<div/>', { 'class': 'offers' }).append(
                        $('<div/>', { 'class': 'offers-header', 'html': instance.localisation.offers })
                    ).append(
                        $('<div/>', { 'class': 'offers-body', 'id': 'offers-' + hotel.id })
                    )
                ).appendTo('#response');

                instance.asyncRenderOffer(hotel.id, hotel.offers);

            };

        };

        instance.utils.interception.listingEnd();

        return instance;
    };

    this.asyncRenderOffer = function (pHotelId, pOffers) {
        setTimeout(function () {
            instance.renderOffers(pHotelId, pOffers);
        }, 10);
    }

    this.renderOffers = function (pHotelId, pOffers) {

        var offerTable = $('<table/>', { 'class': 'offer-table' }).append(
                            $('<tr/>').append(
                                $('<th/>', { 'class': 'offer-room', 'html': instance.localisation.room })
                            ).append(
                                $('<th/>', { 'class': 'offer-board', 'html': instance.localisation.board })
                            ).append(
                                $('<th/>', { 'class': 'offer-status', 'html': instance.localisation.status })
                            ).append(
                                $('<th/>', { 'class': 'offer-cancellation', 'html': instance.localisation.conditions })
                            ).append(
                                $('<th/>', { 'class': 'offer-book', 'html': instance.localisation.reservation })
                            )
                        );

        $('#offers-' + pHotelId).append(offerTable);

        $.each(pOffers, function (index, offer) {
            offerTable.append(
                $('<tr/>').append(
                    $('<td/>', { 'class': 'offer-room', 'html': instance.joinOfferRooms(offer.rooms, offer.id) })
                ).append(
                    $('<td/>', { 'class': 'offer-board', 'html': instance.utils.toTitleCase(offer.board) })
                ).append(
                    $('<td/>', { 'class': 'offer-status', 'html': (offer.isAvailable ? instance.localisation.available : instance.localisation.notAvailable) })
                ).append(
                    $('<td/>', {
                        'class': 'offer-cancellation', 'html': function () {
                            return instance.renderContition(offer);
                        }
                    })
                ).append(
                    $('<td/>', { 'class': 'offer-book' }).append(
                        $('<button/>', { // button -> span
                            'html': offer.price.amount.toFixed(2) + ' ' + offer.price.currency,
                            'data-hotel': pHotelId, 'data-offer': offer.id,
                            'title': instance.localisation.reservation ,
                            'href': '#'
                            //'class': 'tooltip-' + offer.id
                        }).on('click', instance.goReservation)
                    )
                )
            );
            instance.utils.setTooltip(".tooltip-" + offer.id);
        });

        instance.utils.setScrolls('#offers-' + pHotelId);

    };

    this.joinOfferRooms = function (pRooms, pOfferId) {
        var result = '';
        $.each(pRooms, function (index, room) {
            if (result != '') result += '<br/>';
            result += '<span class="offer-room-names">' + instance.utils.toTitleCase(room.type); + '</span>';
            $.each(room.promotions, function (index, promotion) {
                result += ' <span class="icon icon-special-offer masterTooltip tooltip-' + pOfferId + '" title="' + promotion + '"></span>';
            });
        });
        return result;
    };

    this.renderStars = function (pCount) {
        var result = '';
        pCount = Math.floor(pCount) - 1;
        for (var i = 0; i < 5; i++) {
            result += '<span class="icon icon-star' + (i > pCount ? ' inactive' : '') + '"></span>'
        }
        return $(result);
    }

    this.goReservation = function () {
        var hotelId = $(this).data('hotel');
        var offerId = $(this).data('offer');
        var hotelDetail = _.find(instance.hotels, function (f) { return f.id = hotelId; });
        gotoReservation(hotelId, offerId, hotelDetail);
        //Go Reservation
    }

    this.checkCondition = function () {
        var btn = $(this);
        var _offerId = btn.data('offer');
        var prnt = btn.parent();

        btn.toggleClass('button-condition-check button-condition-checking');

        $.ajax({
            url: instance.apiAddress + '/offerdetails',
            method: 'POST',
            type: 'POST',
            contentType: 'application/json; charset=UTF-8',
            data: JSON.stringify({ offerId: _offerId }),
            success: function (data) {
                prnt.empty();
                var rid = instance.utils.getRandomId();
                prnt.append(instance.renderContition(data, rid))
                instance.utils.setTooltip("#" + rid);
            },
            error: function (ex) {
                btn.toggleClass('button-condition-checking button-condition-check');
                console.log(ex);
            },
            beforeSend: function (xhr, settings) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + instance.token);
            }
        });
    }

    this.renderContition = function (offer, rid) {
        if (offer.cancellationPolicies != null && offer.cancellationPolicies.length > 0) {
            var minDate = _.minBy(offer.cancellationPolicies, function (m) { return m.afterDate })
            var _date = instance.utils.toShortDateString(minDate.afterDate);
            return $('<span/>', {
                'id': (rid != null) ? rid : '',
                'class': 'condition tooltip-' + offer.id + ' ' + (new Date(minDate.afterDate) <= new Date() ? 'non-refundable' : 'refundable'),
                'html': _date,
                'title': instance.utils.formatString(instance.localisation.conditionTitle, minDate.fee.amount.toFixed(2), minDate.fee.currency, _date)
            });
        } else {
            return $('<a />', {
                'href': '#',
                'html': instance.localisation.checkStatus,
                'data-offer': offer.id,
                'class': 'check-bt button-condition-check'
            }).on('click', instance.checkCondition);
        }
    }

}

function _utils() {

    this.setTooltip = function (pSelector) {
        //Tooltip
        $(pSelector).hover(function () {
            // Hover over code
            var title = $(this).attr('title');
            $(this).data('tipText', title).removeAttr('title');
            $('<p class="tooltip"></p>')
            .text(title)
            .appendTo('body')
            .fadeIn('slow');
        }, function () {
            // Hover out code
            $(this).attr('title', $(this).data('tipText'));
            $('.tooltip').remove();
        }).mousemove(function (e) {
            var mousex = e.pageX + 20; //Get X coordinates
            var mousey = e.pageY + 10; //Get Y coordinates
            $('.tooltip')
            .css({ top: mousey, left: mousex })
        });
        return this;
    }

    this.setScrolls = function (pSelector) {
        $(pSelector).mCustomScrollbar({
            scrollButtons: { enable: true },
            theme: "rounded-dark",
            autoExpandScrollbar: true
        });
    }

    this.toTitleCase = function (str) {
        return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
    }

    this.formatString = function () {
        var s = arguments[0];
        for (var i = 0; i < arguments.length - 1; i++) {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            s = s.replace(reg, arguments[i + 1]);
        }
        return s;
    }

    this.toShortDateString = function (dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = day + "/" + month + "/" + year;

        return date;
    }

    this.getRandomId = function () {
        return (new Date()).getTime();
    }

    this.interception = {
        searchingStart: function () {
            console.log('Searching Start : ' + new Date())
        },
        searchingEnd: function () {
            console.log('Searching End : ' + new Date())
        },
        progressingStart: function () {
            console.log('Progressing Start : ' + new Date())
        },
        progressingEnd: function () {
            console.log('Progressing End : ' + new Date())
        },
        filteringStart: function () {
            console.log('Filtering Start : ' + new Date())
        },
        filteringEnd: function () {
            console.log('Filtering End : ' + new Date())
        },
        listingStart: function () {
            console.log('Listing Start : ' + new Date())
        },
        listingEnd: function () {
            console.log('Listing End : ' + new Date());
            self.parent.reSizeResultFrame(document.body.offsetHeight + 25);
        }
    }

}