﻿
var dateTimeConvert = {
	toCsharpticks: function (date) {
		if (date == undefined || date == null) return null;
		var epochTicks = 621355968000000000;
		var ticksPerMillisecond = 10000;
		return epochTicks + (date.getTime() * ticksPerMillisecond);
	},
	fromCsharpticks: function (csTicks) {
		if (csTicks == undefined || csTicks == null) return null;
		var epochTicks = 621355968000000000;
		var ticksPerMillisecond = 10000;
		return new Date(((csTicks - epochTicks) / ticksPerMillisecond));
	}
}