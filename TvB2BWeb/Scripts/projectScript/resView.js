﻿var documentReady = {
    init: function () {
        documentReady.dialogBoxInitialize();
        documentReady.maximize();
    },
    maximize: function () {
        var maxW = screen.availWidth;
        var maxH = screen.availHeight;
        if (location.href.indexOf('pic') == -1) {
            if (!window.opera) {
                top.window.moveTo(0, 0);
                if (document.all) {
                    top.window.resizeTo(maxW, maxH);
                }
                else {
                    if (document.layers || document.getElementById) {
                        if ((top.window.outerHeight < maxH) || (top.window.outerWidth < maxW)) {
                            top.window.outerHeight = maxH;
                            top.window.outerWidth = maxW;
                        }
                    }
                }
            }
        }
    },
    dialogBoxInitialize: function () {
        $("#dialog-cancelReservation").dialog({ autoOpen: false, modal: true, width: 650, height: 400, resizable: true, autoResize: true });
        $("#dialog-AddTourist").dialog({ autoOpen: false, modal: true, resizable: true, autoResize: true });
        $("#dialog-resServiceEdit").dialog({ autoOpen: false, modal: true, width: 675, height: 600, resizable: true, autoResize: true });
        $("#dialog-resServiceAdd").dialog({ autoOpen: false, modal: true, width: 675, height: 600, resizable: true, autoResize: true });
        $("#dialog-changeRoomAccom").dialog({ autoOpen: false, modal: true, width: 420, height: 390, resizable: true, autoResize: true });
        $("#dialog-resPayment").dialog({ autoOpen: false, modal: true, width: 1000, height: 600, resizable: true, autoResize: true });
        $("#dialog-changeDate").dialog({ autoOpen: false, modal: true, width: 500, height: 450, resizable: true, autoResize: true });
        $("#dialog-Ssrc").dialog({ autoOpen: false, modal: true, width: 640, height: 550, resizable: true, autoResize: true });
    }    
}