﻿(function($) {
    $.extend($.ui.dialog.prototype, {
        'addbutton': function(buttonName, func) {
            var buttons = this.element.dialog('option', 'buttons');
            buttons[buttonName] = func;
            this.element.dialog('option', 'buttons', buttons);
        }
    });

    // Allows simple button removal from a ui dialog
    $.extend($.ui.dialog.prototype, {
        'removebutton': function(buttonName) {
            var buttons = this.element.dialog('option', 'buttons');
            delete buttons[buttonName];
            this.element.dialog('option', 'buttons', buttons);
        }
    });
} (jQuery));