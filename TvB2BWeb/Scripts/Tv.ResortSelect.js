﻿$.fn.loadCheckBoxList = function(inputDataArray, checkedList) {
    var listChk = document.getElementById(checkedList);
    var _input = '';
    var mydata = $.json.decode(inputDataArray)
    $.each(mydata, function(index, inputData) {
        var checked = '';
        if (listChk.value.length > 0) {
            var chkListStr = listChk.value;
            var chkList = chkListStr.split('|');
            if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.RecID.toString()) > 0)
                checked = 'checked=\'checked\'';
            else checked = '';
        } else checked = '';

        _input += "<input type='checkbox' value='" + inputData.RecID + "' " + checked + " /><label>" + inputData.Name + "</label><br />";
    });
    this.html(_input);
    return this;

}

function getListComplette(listData, checkedList) {
    $("#tvIntList").html('');
    $("#tvIntList").loadCheckBoxList(listData, checkedList);
}

function listClick() {
    var listChk = '';
    $(":checked", "#dialog-ResortList").each(function() {
        if (listChk.length > 0) listChk += '|';
        listChk += $(this).val();
    });
    return listChk;
}

function showResortList(msg, btnOK, btnCancel, checkedList, textBox) {
    $(function() {
    //        $("#messages").html(msg);
        $("#dialog").dialog('destroy');
        var _buttons = {};
        var ok = btnOK.toString();
        var cancel = btnCancel.toString();
        _buttons[ok] = function() {
            var hfSelected = document.getElementById(checkedList);
            var _textBox = document.getElementById(textBox);
            hfSelected.value = listClick();
            _textBox.value = hfSelected.value.length > 0 ? hfSelected.value.split('|').length.toString() + ' Selected' : '';
//            __doPostBack('btnPostBack', '');
            $(this).dialog('close');            
            return true;
        };
        _buttons[cancel] = function() {
            $(this).dialog('close');
            //$("#dialog").dialog('destroy');
            return true;
        };
        $("#dialog-ResortList").dialog({
            modal: true,
            height: 400,
            buttons: _buttons
        });
    });
}

function ResortSelectClick(_country, _arrCity, _operator, _market, btnOK, btnCancel, checkedList, textBox) {
    var country = document.getElementById(_country).value;
    var arrCity = document.getElementById(_arrCity).value;
    var paraList = '{"Country":"' + country + '","ArrCity":"' + arrCity + '","Operator":"' + _operator + '","Market":"' + _market + '"}';
    var serviceUrl = 'Services/PriceSearch.asmx/getResourtData';
    $.ajax({
        type: "POST",
        data: paraList,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: serviceUrl,
        success: function(msg) {
            getListComplette(msg.d, checkedList);
            showResortList('Resort List', btnOK, btnCancel, checkedList, textBox);
        },
        error: function(xhr, msg, e) {
            alert(xhr.responseText);
        }
    });
}
