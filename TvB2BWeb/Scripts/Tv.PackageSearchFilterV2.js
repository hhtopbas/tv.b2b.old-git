﻿packageSearchFilterV2_fltCheckInDay = function(data, nameStr) {
    var html = '';
    $.each(data.maxDayList, function(i) {
        html += "<option value='" + this + "' " + (data.selectedDay == this ? "selected='selected'" : "") + ">" + '+' + this + ' ' + nameStr + "</option>";
    });
    return html;
}
packageSearchFilterV2_fltNight = function(data) {
    var html = '';
    $.each(data.maxDayList, function(i) {
        html += "<option value='" + this + "' " + (data.selectedDay == this ? "selected='selected'" : "") + ">" + this + "</option>";
    });
    return html;
}
packageSearchFilterV2_fltRoomCount = function(data) {
    var html = '';
    $.each(data.maxDayList, function(i) {
        html += "<option value='" + this + "' " + (data.selectedDay == this ? "selected='selected'" : "") + ">" + this + "</option>";
    });
    return html;
}
function packageSearchFilterV2_ArrCity(data, firstStr) {
    var html = '';
    if (firstStr != '')
        html += "<option value='' >" + firstStr + "</option>";
    var country = '';
    var first = true;
    if (data.hasOwnProperty('d') && data.d != null) {
        $.each(data.d, function(i) {
            if (this.Country != country) {
                if (!first) html += "</optgroup>";
                html += "<optgroup label='" + this.Country + "'>";
                first = false;
                country = this.Country;
            }
            html += "<option value='" + this.RecID + "'>" + this.Name + "</option>";
        });
        if (html != '') html += "</optgroup>";
    }
    return html;
}
