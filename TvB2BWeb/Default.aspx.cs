﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;

public partial class Default : System.Web.UI.Page
{ 
    protected override void InitializeCulture() 
    {        
        base.InitializeCulture();                

        String[] userLang = Request.UserLanguages;
        String[] CI = userLang[0].Split(';');
        string CultureStr = CI[0];
        if (Equals(CultureStr, "nb")) CultureStr = "nb-NO"; if (Equals(CultureStr, "sr-SC")) CultureStr = "en-US"; if (Equals(CultureStr, "sr-SC")) CultureStr = "en-US";
        CultureInfo[] allCulture = CultureInfo.GetCultures(CultureTypes.AllCultures);
        if (allCulture.Where(w => Equals(w.Name, CultureStr)).Count() < 1) {
            CultureStr = "en-US";
        }
        if (CultureStr.Length < 5)
        {
            CultureStr = (from culinf in CultureInfo.GetCultures(CultureTypes.AllCultures).AsEnumerable()
                          where culinf.Name == CultureStr
                          select new { CultureName = culinf.TextInfo.CultureName }).First().CultureName;
        }
        CultureInfo ci = new CultureInfo(CultureStr);
        Session["Culture"] = ci;
        if (Session["Culture"] != null)
        {
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo(((System.Globalization.CultureInfo)Session["Culture"]).Name, false);
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!IsPostBack)
        {                       
            if (!Session.IsNewSession)
            { 
                string errorMsg = string.Empty;
                new TvBo.Users().setLogoutUser(Session.SessionID, false, ref errorMsg);
            }
            if (!string.IsNullOrEmpty(Request.Params["B2BToB2CSource"]) && !string.IsNullOrEmpty(Request.Params["data"]))
            {
                Session["tempResData"] = (string)Request.Params["data"];
            }
            
        }
    }
}
