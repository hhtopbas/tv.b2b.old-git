﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using TvBo;
using TvTools;
using Winnovative;


namespace TvSearch
{
    public partial class PackageSearch : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User UserData = (User)Session["UserData"];
            if (UserData.Ci.Name.ToLower() == "lt-lt")
                UserData.Ci.DateTimeFormat.ShortDatePattern = "yyyy.MM.dd";
            Thread.CurrentThread.CurrentCulture = UserData.Ci;
            Thread.CurrentThread.CurrentUICulture = UserData.Ci;
            if (!IsPostBack)
            {
                string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\PriceSearchBasketV2." + HttpContext.Current.Session.SessionID;
                if (System.IO.File.Exists(path))
                    System.IO.File.Delete(path);
            }

            clientOfferVersion.Value = Conversion.getStrOrNull(System.Configuration.ConfigurationManager.AppSettings["ClientOfferVersion"]);
        }

        [WebMethod(EnableSession = true)]
        public static string UserHasAuth()
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
            return UserData.BlackList ? "N" : "Y";
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
        public static PackageSearchMakeRes getBookReservation(int? refNo)
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            object extAllotControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ExtAllotControl");
            bool ExtAllotCont = Conversion.getBoolOrNull(extAllotControl).HasValue ? Conversion.getBoolOrNull(extAllotControl).Value : false;
            object _checkAvailableFlightSeat = new TvBo.Common().getFormConfigValue("SearchPanel", "CheckAvailableFlightSeat");
            bool checkAvailableFlightSeat = Conversion.getBoolOrNull(_checkAvailableFlightSeat).HasValue ? Conversion.getBoolOrNull(_checkAvailableFlightSeat).Value : false;
            object _checkAvailableRoom = new TvBo.Common().getFormConfigValue("SearchPanel", "CheckAvailableRoom");
            bool checkAvailableRoom = Conversion.getBoolOrNull(_checkAvailableRoom).HasValue ? Conversion.getBoolOrNull(_checkAvailableRoom).Value : false;

            HttpContext.Current.Session["ResData"] = null;
            if (HttpContext.Current.Session["Criteria"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.SearchCriteria criteria = (TvBo.SearchCriteria)HttpContext.Current.Session["Criteria"];

            ResDataRecord ResData = new ResDataRecord();
            ResData.SelectBook = new List<SearchResult>();
            if (!refNo.HasValue)
            {
                List<SearchResult> bookSelected = readBasketData();
                if (bookSelected == null || bookSelected.Count < 1)
                    return new PackageSearchMakeRes { resOK = false, errMsg = "No", version = string.Empty };

                foreach (SearchResult row in bookSelected)
                {
                    SearchResult sr = new SearchResult();
                    SearchCriteriaRooms sc = new SearchCriteriaRooms();
                    sr = row;
                    sc = criteria.RoomsInfo.FirstOrDefault();
                    sr.Child1Age = sc.Chd1Age;
                    sr.Child2Age = sc.Chd2Age;
                    sr.Child3Age = sc.Chd3Age;
                    sr.Child4Age = sc.Chd4Age;
                    ResData.SelectBook.Add(sr);
                }
            }
            else
            {
                SearchResult sr = new SearchResult();
                SearchCriteriaRooms sc = new SearchCriteriaRooms();
                List<SearchResult> searchData = readSearchData();
                if (searchData == null) return new PackageSearchMakeRes { resOK = false, errMsg = "No", version = string.Empty };
                sr = searchData.Find(f => f.RefNo == refNo);
                sc = criteria.RoomsInfo.FirstOrDefault();
                sr.Child1Age = sc.Chd1Age;
                sr.Child2Age = sc.Chd2Age;
                sr.Child3Age = sc.Chd3Age;
                sr.Child4Age = sc.Chd4Age;
                ResData.SelectBook.Add(sr);
            }
            string errorMsg = string.Empty;

            Guid? logID = null;
            String useLog = System.Configuration.ConfigurationManager.AppSettings["useLog"];
            if (!string.IsNullOrEmpty(useLog) && string.Equals(useLog, "1"))
            {
                if (ResData.SelectBook.FirstOrDefault().LogID.HasValue)
                    logID = new WEBLog().saveWEBBookLog(ResData.SelectBook.FirstOrDefault().LogID.Value, DateTime.Now, null, ref errorMsg);
            }

            int totalPax = ResData.SelectBook.Sum(s => (s.HAdult + s.HChdAgeG2 + s.HChdAgeG3 + s.HChdAgeG4 + (s.ChdG1Age2.HasValue && s.ChdG1Age2.Value > (decimal)(199 / 100) ? s.HChdAgeG1 : 0)));
            int maxTotalPax = UserData.AgencyRec.MaxPaxCnt.HasValue ? UserData.AgencyRec.MaxPaxCnt.Value : (UserData.TvParams.TvParamReser.MaxPaxCnt.HasValue ? UserData.TvParams.TvParamReser.MaxPaxCnt.Value : 10);
            if (maxTotalPax < totalPax)
            {
                string msg = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "MaxPaxReservation").ToString(), maxTotalPax.ToString());
                return new PackageSearchMakeRes { resOK = false, errMsg = msg, version = string.Empty };
            }
            if (!checkAvailableFlightSeat && !ExtAllotCont)
                if (!(new UIReservation().bookFlightAllotControl(UserData, ResData.SelectBook, ResData.SelectBook.FirstOrDefault(), false, ref errorMsg)))
                    return new PackageSearchMakeRes { resOK = false, errMsg = errorMsg, version = string.Empty };

            if (!ExtAllotCont)
                if (!(new Hotels().AllotmentControlForMultipleRoom(UserData, ResData.SelectBook, "H", ref errorMsg)))
                    return new PackageSearchMakeRes { resOK = false, errMsg = errorMsg, version = string.Empty };

            ResData = new Reservation().getResData(UserData, ResData, criteria, SearchType.PackageSearch, false, ref errorMsg);
            if (!string.IsNullOrEmpty(errorMsg))
                return new PackageSearchMakeRes { resOK = false, errMsg = errorMsg, version = string.Empty };

            ResData.FirstData = new ResTables().getFirstResData(ResData);
            ResData.FirstData.ReservationType = SearchType.PackageSearch;

            string version = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "Version"));
            if (string.Equals(version, "V2"))
            {
                ResData = new ReservationV2().getResDataExtras(UserData, ResData, ref errorMsg);
            }
            if (ResData.ExtrasData == null)
            {
                ResData.ExtrasData = ResData;
            }

            bool CreateChildAge = false;
            if (UserData.WebService)
                CreateChildAge = true;
            else
            {
                object _createChildAge = new TvBo.Common().getFormConfigValue("MakeReservation", "CreateChildAge");
                CreateChildAge = _createChildAge != null ? (bool)_createChildAge : false;
            }

            if (!CreateChildAge)
            {
                foreach (ResCustRecord row in ResData.ResCust)
                {
                    row.Age = null;
                    row.Birtday = null;
                }
                foreach (ResCustRecord row in ResData.ExtrasData.ResCust)
                {
                    row.Age = null;
                    row.Birtday = null;
                }
            }

            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_SunFun) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka))
            {
                ResMainRecord resMain = ResData.ResMain;
                resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
            }

            ResData.LogID = logID;

            HttpContext.Current.Session["ResData"] = ResData;
            return new PackageSearchMakeRes { resOK = true, errMsg = string.Empty, version = version };
        }

        internal static List<SearchResult> readBasketData()
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci;
            Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\PriceSearchBasketV2." + HttpContext.Current.Session.SessionID;
            List<SearchResult> list = new List<SearchResult>();
            if (System.IO.File.Exists(path))
            {
                System.IO.StreamReader reader = new System.IO.StreamReader(path);

                try
                {
                    string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
                    list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SearchResult>>(uncompressed);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    reader.Close();
                }
            }
            return list;
        }

        internal static List<SearchResult> readSearchData()
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci;
            Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\PriceSearchV2." + HttpContext.Current.Session.SessionID;
            List<SearchResult> list = new List<SearchResult>();
            if (System.IO.File.Exists(path))
            {
                System.IO.StreamReader reader = new System.IO.StreamReader(path);

                try
                {
                    string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
                    list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SearchResult>>(uncompressed);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    reader.Close();
                }
            }
            return list;
        }

        [WebMethod(EnableSession = true)]
        public static string viewPDFReport(string reportType, string _html, string pageWidth, string urlBase, List<CodeName> param)
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci;
            Thread.CurrentThread.CurrentUICulture = UserData.Ci;
            _html = _html.Replace('|', '"');
            _html = _html.Replace("<div id=\"noPrintDiv\">", "<div id=\"noPrintDiv\" style=\"display: none; visibility: hidden;\">");
            _html = _html.Replace("<div class=\"noPrint\">", "<div style=\"display: none; visibility: hidden;\">");
            

            string errorMsg = string.Empty;
            string basePageUrl = WebRoot.BasePageRoot;
            string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
            string pdfFileName = reportType + "_" + UserData.AgencyID + "_" + UserData.UserID + "_" + Guid.NewGuid() + ".pdf";
            StringBuilder html = new StringBuilder(_html);

            int PageWidth = Conversion.getInt32OrNull(pageWidth).HasValue ? Conversion.getInt32OrNull(pageWidth).Value : 700;
            //initialize the PdfConvert object
            PdfConverter pdfConverter = new PdfConverter();
            
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.NoCompression;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.LeftMargin = 10;
            pdfConverter.PdfDocumentOptions.TopMargin = 10;
            // set the demo license key
            pdfConverter.LicenseKey = ConfigurationManager.AppSettings["Winnovative"];;

            // get the base url for string conversion which is the url from where the html code was retrieved
            // the base url is a hint for the converter to find the external CSS and images referenced by relative URLs
            string thisPageURL = HttpContext.Current.Request.Url.AbsoluteUri;
            string baseUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/";
            try
            {
                pdfConverter.SavePdfFromHtmlStringToFile(html.ToString(), siteFolderISS + "ACE\\" + pdfFileName, urlBase);
                string returnUrl = basePageUrl + "ACE/" + pdfFileName;
                return returnUrl;
            }
            catch(Exception ex)
            {
                errorMsg = ex.Message;
                return "";
            }
        }
    }

}

public class PackageSearchMakeRes
{
    public bool resOK { get; set; }
    public string errMsg { get; set; }
    public string version { get; set; }
}