﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BookOnlyTransfer.aspx.cs"
    Inherits="BookTicket" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "BookTicket")%></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>

    <%--<script src="Scripts/jquery.watermark.js" type="text/javascript"></script>--%>

    <script src="Scripts/checkDate.js" type="text/javascript"></script>

    <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/confirm.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="CSS/BookTicket.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        body { width: 950px; font-family: Tahoma; font-size: 10px; }
        #gridCust { width: 950px; border: solid 1px #666; font-family: Tahoma; font-size: 10px; }
    </style>

    <script type="text/javascript">

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        var reservationSaved = false;

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>'; $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        $(document).ready(function () {
            $("#dialog-resCustOtherInfo").dialog(
            {
                autoOpen: false,
                modal: true,
                width: 440,
                height: 440,
                resizable: true,
                autoResize: true
            });

            $("#dialog-resCustomerAddress").dialog(
            {
                autoOpen: false,
                modal: true,
                width: 595,
                height: 480,
                resizable: false,
                autoResize: true
            });
        });


        function showDialog(msg, transfer, trfUrl) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                            $(this).dialog('close');
                            if (transfer == true) {
                                var url = trfUrl;
                                $(location).attr('href', url);
                            }
                        }
                    }
                });
            });
        }

        function showDialogEndGotoPaymentPage(msg, ResNo, PaymentPageUrl, DirectPaymentPage) {
            if (DirectPaymentPage == true) {
                $(this).dialog('close');
                window.close;
                self.parent.paymentPage(ResNo, PaymentPageUrl);
            }
            else {
                $(function () {
                    $("#messages").html(msg);
                    $("#dialog").dialog("destroy");
                    $("#dialog-message").dialog({
                        maxWidth: 880,
                        maxHeight: 500,
                        modal: true,
                        resizable: true,
                        autoResize: true,
                        buttons: {
                            '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                                $(this).dialog('close');
                                window.close;
                                self.parent.gotoResViewPage(ResNo);
                            },
                            '<%= GetGlobalResourceObject("LibraryResource", "btnPaymentPage") %>': function () {
                                $(this).dialog('close');
                                window.close;
                                self.parent.paymentPage(ResNo, PaymentPageUrl);
                            }
                        }
                    });
                });
            }
        }

        function showDialogEndExit(msg, ResNo) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                            $(this).dialog('close');
                            window.close;
                            if (ResNo != '')
                                self.parent.gotoResViewPage(ResNo);
                            else self.parent.cancelMakeRes();
                        }
                    }
                });
            });
        }

        function SetAge(Id, cinDate, _formatDate) {
            try {
                var birthDate = dateValue(_formatDate, $("#iBirthDay" + Id).val());
                if (birthDate == null) {
                    $("#iBirthDay" + Id).val('');
                    $("#iAge" + Id).val('');
                    return;
                }
                var checkIN = dateValue(_formatDate, cinDate);
                if (checkIN == null) return;
                var _minBirthDate = new Date(1, 1, 1910);
                var _birthDate = birthDate;
                var _cinDate = checkIN;
                if (_birthDate < _minBirthDate) {
                    $("#iAge" + Id).val('');
                    birthDate = '';
                    showDialog('<%= GetGlobalResourceObject("LibraryResource","InvalidBirthDate").ToString() %>', false, '');
                    return;
                }
                var age = Age(_birthDate, _cinDate);  //parseInt((_cinDate - _birthDate) / (1000 * 24 * 60 * 60 * 365.25))
                $("#iAge" + Id).val(age);
            }
            catch (err) {
                $("#iAge" + Id).val('');
            }
        }

        function getCustomers() {
            var cust = '';
            var hfCustCount = parseInt($("#hfCustCount").val());
            var custCount = parseInt(hfCustCount);
            for (var i = 1; i <= custCount; i++) {
                var record = '';
                var SeqNo = $("#iSeqNo" + i).text();
                var ddlTitle = $("#iTitle" + i).length > 0 ? $("#iTitle" + i).val() : '';
                var txtSurname = $("#iSurname" + i).length > 0 ? $("#iSurname" + i).val() : '';
                var txtSurnameL = $("#iSurnameL" + i).length > 0 ? $("#iSurnameL" + i).val() : '';
                var txtName = $("#iName" + i).length > 0 ? $("#iName" + i).val() : '';
                var txtNameL = $("#iNameL" + i).length > 0 ? $("#iNameL" + i).val() : '';
                var txtBirtDay = $("#iBirthDay" + i).length > 0 ? $("#iBirthDay" + i).val() : '';
                var txtAge = $("#iAge" + i).length > 0 ? $("#iAge" + i).val() : '';
                var txtIDNo = $("#iIDNo" + i).length > 0 ? $("#iIDNo" + i).val() : '';
                var txtPassSerie = $("#iPassSerie" + i).length > 0 ? $("#iPassSerie" + i).val() : '';
                var txtPassNo = $("#iPassNo" + i).length > 0 ? $("#iPassNo" + i).val() : '';
                var txtPassIssueDate = $("#iPassIssueDate" + i).length > 0 ? $("#iPassIssueDate" + i).val() : '';
                var txtPassExpDate = $("#iPassExpDate" + i).length > 0 ? $("#iPassExpDate" + i).val() : '';
                var txtPassGiven = $("#iPassGiven" + i).length > 0 ? $("#iPassGiven" + i).val() : '';
                var countryCode = $("#iPhoneCountryCode" + i).length > 0 ? $("#iPhoneCountryCode" + i) : '';
                var txtPhone = /*countryCode + */($("#iPhone" + i).length > 0 ? $("#iPhone" + i).val() : '');
                var ddlNation = $("#iNation" + i).length > 0 ? $("#iNation" + i).val() : '';
                var ddlNationality = $("#iNationality" + i).length > 0 ? $("#iNationality" + i).val() : '';
                var txtPassport = $("#iHasPassport" + i).length > 0 ? $("#iHasPassport" + i)[0].checked : true;
                var txtLeader = $('input[name=Leader]:checked').val() == SeqNo;
                record = '<|SeqNo|:' + SeqNo +
                         ',|Title|:' + ddlTitle +
                         ',|Surname|:|' + txtSurname + '|' +
                         ',|Name|:|' + txtName + '|' +
                         ',|SurnameL|:|' + txtSurnameL + '|' +
                         ',|NameL|:|' + txtNameL + '|' +
                         ',|BirtDay|:|' + txtBirtDay + '|' +
                         ',|Age|:|' + txtAge + '|' +
                         ',|IDNo|:|' + txtIDNo + '|' +
                         ',|PassSerie|:|' + txtPassSerie + '|' +
                         ',|PassNo|:|' + txtPassNo + '|' +
                         ',|PassIssueDate|:|' + txtPassIssueDate + '|' +
                         ',|PassExpDate|:|' + txtPassExpDate + '|' +
                         ',|PassGiven|:|' + txtPassGiven + '|' +
                         ',|Phone|:|' + txtPhone + '|' +
                         ',|Nation|:' + ddlNation +
                         ',|Nationality|:' + ddlNationality +
                         ',|Passport|:' + txtPassport +
                         ',|Leader|:' + txtLeader + '>';
                if (cust.length > 0) cust += "@";
                cust += record;
            }
            return cust;
        }

        function setCustomers() {
            $.ajax({
                type: "POST",
                url: "BookTicket.aspx/setCustomers",
                data: '{"data":"' + getCustomers() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    return true;
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialogEndExit(xhr.responseText, '');
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function returnCustInfoEdit(data, source) {
            if (source == "save") {
                $.ajax({
                    type: "POST",
                    url: "BookTicket.aspx/setResCustInfo",
                    data: '{"data":"' + data + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (source == "save") {
                            getResCustDiv();

                        }
                        $("#dialog-resCustomerAddress").dialog("close");
                    },
                    error: function (xhr, msg, e) {
                        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                            showDialogEndExit(xhr.responseText, '');
                    },
                    statusCode: {
                        408: function () {
                            logout();
                        }
                    }
                });
            }
            else {
                $("#dialog-resCustomerAddress").dialog("close");
            }
        }

        function showCustAddress(CustNo) {
            var customers = getCustomers();
            $.ajax({
                type: "POST",
                url: "BookTicket.aspx/setResCustomers",
                data: '{"data":"' + customers + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var custInfoUrl = 'CustomerAddress.aspx?CustNo=';
                    $("#dialog-resCustomerAddress").dialog("open");
                    $("#resCustomerAddress").attr("src", custInfoUrl + CustNo);
                    return false;
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialogEndExit(xhr.responseText, '');
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getResCustDiv() {
            $.ajax({
                type: "POST",
                url: "BookTicket.aspx/createTicketCustStr",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#resCustGrid").html('');
                    $("#resCustGrid").html(msg.d);
                    $(".formatDate").mask($("#dateMask").val());
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialogEndExit(xhr.responseText, '');
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function returnCustDetailEdit(dataS, source) {
            if (source == "save") {
                var data = new Object();
                data.data = dataS;
                $.ajax({
                    type: "POST",
                    url: "BookTicket.aspx/setResCustomer",
                    data: $.json.encode(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (source == "save") {
                            getResCustDiv();
                        }
                        $("#dialog-resCustOtherInfo").dialog("close");
                    },
                    error: function (xhr, msg, e) {
                        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                            showDialogEndExit(xhr.responseText, '');
                    },
                    statusCode: {
                        408: function () {
                            logout();
                        }
                    }
                });
            }
            else {
                $("#dialog-resCustOtherInfo").dialog("close");
            }
        }

        function showResCustInfo(CustNo) {
            var customers = getCustomers();
            $.ajax({
                type: "POST",
                url: "BookTicket.aspx/setResCustomers",
                data: '{"data":"' + customers + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var custInfoUrl = 'CustomerDetail.aspx?CustNo=';

                    $("#dialog-resCustOtherInfo").dialog("open");
                    $("#resCustOtherInfo").attr("src", custInfoUrl + CustNo);
                    return false;
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialogEndExit(xhr.responseText, '');
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        $(document).ready(function () {
            $("#dialog-Ssrc").dialog(
            {
                autoOpen: false,
                modal: true,
                width: 640,
                height: 550,
                resizable: true,
                autoResize: true
            });
        });

        function searchSelectCust(CustNo) {
            $("#dialog-searchCust").dialog("close");
            var Custs = getCustomers();
            $.ajax({
                type: "POST",
                url: "BookTicket.aspx/copyCustomer",
                data: '{"CustNo":"' + CustNo + '","Custs":"' + Custs + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 'OK') {
                        getResCustDiv();
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialogEndExit(xhr.responseText, '');
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function searchCustListShow(_html) {
            $(function () {
                $("#searchCust").html('');
                $("#searchCust").html(_html);
                $("#dialog").dialog("destroy");
                $("#dialog-searchCust").dialog({
                    modal: true,
                    width: 650,
                    height: 450,
                    resizable: true,
                    autoResize: true
                });
            });
        }

        function searchCust(i) {
            var Surname = $("#iSurname" + i).val();
            var Name = $("#iName" + i).val();
            var BirthDate = $("#iBirthDay" + i).val();
            var SeqNo = $("#iSeqNo" + i).text();
            $.ajax({
                type: "POST",
                url: "BookTicket.aspx/searchCustomer",
                data: '{"refNo":"' + i + '","SeqNo":"' + SeqNo + '","Surname":"' + Surname + '","Name":"' + Name + '","BirthDate":"' + BirthDate + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '')
                        searchCustListShow(msg.d);
                    else
                        showDialogEndExit('<%= GetGlobalResourceObject("BookTicket", "lblNoResult").ToString() %>', '');
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialogEndExit(xhr.responseText, '');
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
    }

    function makeTransfer() {

    }

    function cbHandicapChanged() {
        if ($('#cbHandicap').attr('checked'))
            $("#btnSave").removeAttr("disabled");
        else $("#btnSave").attr("disabled", "true");
    }

    function btnSaveClick(pax) {
        if ($("#divWhereInvoice").css('display') == 'block') {
            if ($("#iInvoiceTo").val() == '' || $("#iInvoiceTo").val() == undefined) {
                showDialog('<%= GetGlobalResourceObject("LibraryResource", "WhereInvoiceTo") %>', false, '');
                    return;
                }
            }

            if (reservationSaved) {
                showDialog('<%= GetGlobalResourceObject("BookTicket", "lblAlreadyRes").ToString() %>', false, "");
                return;
            }
            else reservationSaved = true;
            //, string dosier, string agencyDiscount
            var customerCode = $("#iCustomerCode").length > 0 ? $("#iCustomerCode").val() : '';
            var dosier = $("#iDosier").length > 0 ? $("#iDosier").val() : '';
            var agencyDiscount = $("#iAgencyDiscount").length > 0 ? $("#iAgencyDiscount").val() : '';
            var _data = '{"data":"' + getCustomers() +
                        '","Note":"' + ($("#iNote").length > 0 ? $("#iNote").val().replace(/\"/g, "|") : '') +
                        '","customerCode":"' + customerCode +
                        '","dosier":"' + dosier +
                        '","agencyDiscount":"' + agencyDiscount +
                        '","invoiceTo":"' + $("#iInvoiceTo").val() +
                        '","saveOption":"' + $("#iSaveOption").attr('checked') +
                        '","optionTime":"' + ($("#withOption").length > 0 && $("#withOption").attr("checked") == 'checked' ? '1' : '0') + '"}';

            $.ajax({
                type: "POST",
                url: "BookTicket.aspx/saveReservation",
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var retVal = $.json.decode(msg.d)[0];
                    if (retVal.ControlOK == true) {
                        if (retVal.GotoPaymentPage == true)
                            showDialogEndGotoPaymentPage(retVal.Message, retVal.ResNo, retVal.PaymentPageUrl, retVal.DirectPaymentPage);
                        else showDialogEndExit(retVal.Message, retVal.ResNo);
                    }
                    else {
                        showDialog(retVal.Message, false, '');
                        reservationSaved = false;
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialogEndExit(xhr.responseText, '');
                    reservationSaved = false;
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function btnCancelClick() {
            window.close;
            self.parent.cancelMakeRes();
        }

        function showExtServiceDetail(idNo) {
            $("#extraservice" + idNo).hide();
            $("#extraserviceDetail" + idNo).show();
        }

        function hideExtServiceDetail(idNo) {
            $("#extraserviceDetail" + idNo).hide();
            $("#extraservice" + idNo).show();
        }

        function onClickOptionTime() {
            if ($("#withOption").length > 0) {
                if ($("#withOption").attr("checked") == "checked") {
                    $("#withOptionMsg").show();
                }
                else {
                    $("#withOptionMsg").hide();
                }
            }
        }

        $(document).ready(function () {
            $.query = $.query.load(location.href);
            //var bookNr = $.query.get('BookNr');
            $.ajax({
                type: "POST",
                url: "BookTicket.aspx/getFormData",
                data: {},
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        //                        var data = $.json.decode(msg.d);
                        //                        $("#dateMask").val(data.dateMask);
                        //                        $("#dateFormat").val(data.dateFormat);
                        //                        $("#maxPax").val(data.maxPax);
                    }
                    //                    getBookFlightInfo(bookNr);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText, false, '');
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        });

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <input id="dateMask" type="hidden" value="99/99/9999" />
        <input id="dateFormat" type="hidden" value="dd/MM/yyyy" />
        <input id="maxPax" type="hidden" value="99" />
        <div id="divbookTrsnafer">
            <fieldset>
                <legend>
                    <label>
                        »</label>
                    <strong>
                        <%= GetGlobalResourceObject("MakeReservation", "lblGridResCust")%></strong></legend>
                <div id="resCustGrid">
                </div>
            </fieldset>
            <br />
            <br />
            <fieldset>
                <legend>
                    <label>
                        »</label>
                    <span id="lblGridService">
                        <%=GetGlobalResourceObject("MakeReservation", "lblGridService")%></span></legend>
                <div id="gridResServiceDiv" style="padding-left: 2px; padding-right: 2px; text-align: left;">
                </div>
            </fieldset>
            <fieldset>
                <legend>
                    <label>
                        »</label>
                </legend>
                <div style="width: 100%; text-align: center;">
                    <br />
                    <table style="text-align: left;">
                        <tr>
                            <td align="right">
                                <strong>
                                    <%=GetGlobalResourceObject("Controls", "viewCountry")%>
                                    :</strong>
                            </td>
                            <td>
                                <select id="fltCountry" style="width: 250px;" onchange="fltCountryChanged();">
                                </select>&nbsp;*
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong>
                                    <%=GetGlobalResourceObject("Controls","viewTransferDate") %>
                                    :</strong>
                            </td>
                            <td>
                                <input type="text" id="txtTransferDate" style="width: 90px;" />&nbsp;*
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong>
                                    <%=GetGlobalResourceObject("Controls", "viewFrom")%>
                                    :</strong>
                            </td>
                            <td>
                                <select id="fltRegionFrom" style="width: 150px;" onchange="fltRegionFromChanged();">
                                    <option value=''>
                                        <%=GetGlobalResourceObject("LibraryResource", "ComboSelect")%></option>
                                    <option value='0'>
                                        <%=GetGlobalResourceObject("Controls", "viewFlight")%></option>
                                    <option value='1'>
                                        <%=GetGlobalResourceObject("Controls", "viewHotel")%></option>
                                    <option value='2'>
                                        <%=GetGlobalResourceObject("Controls", "maritial5")%></option>
                                </select>
                                <select id="fltTransferFrom" style="width: 350px;" onchange="fltTransferFromChanged();">
                                </select>&nbsp;*
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong>
                                    <%=GetGlobalResourceObject("Controls", "viewTo")%>
                                    :</strong>
                            </td>
                            <td>
                                <select id="fltRegionTo" style="width: 150px;" onchange="fltRegionToChanged();">
                                    <option value=''>
                                        <%=GetGlobalResourceObject("LibraryResource", "ComboSelect")%></option>
                                    <option value='0'>
                                        <%=GetGlobalResourceObject("Controls", "viewFlight")%></option>
                                    <option value='1'>
                                        <%=GetGlobalResourceObject("Controls", "viewHotel")%></option>
                                    <option value='2'>
                                        <%=GetGlobalResourceObject("Controls", "maritial5")%></option>
                                </select>
                                <select id="fltTransferTo" style="width: 350px;" onchange="fltTransferToChanged();">
                                </select>&nbsp;*
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong>
                                    <%=GetGlobalResourceObject("Controls", "viewTransfer")%>
                                    :</strong>
                            </td>
                            <td>
                                <asp:DropDownList ID="fltTransfer" runat="server" Width="300px" />
                                &nbsp;*
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong>
                                    <%=GetGlobalResourceObject("Controls", "viewPickupTime")%>
                                    :</strong>
                            </td>
                            <td>
                                <select id="fltPickupTimeHour" style="width: 40px;">
                                </select>
                                :
                                <select id="fltPickupTimeMinute" style="width: 40px;">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong>
                                    <%=GetGlobalResourceObject("Controls", "viewTransferNote")%>
                                    :</strong>
                            </td>
                            <td>
                                <input id="fltTransferNote" style="width: 500px;" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table>
                        <tr>
                            <td align="left">
                                <input type="button" id="btnPreCalc" value="Calc Price" style="width: 100px;" onclick="btnPreCalcClick()" />
                            </td>
                            <td width="10px">&nbsp;
                            </td>
                            <td>
                                <b>
                                    <strong>
                                        <%=GetGlobalResourceObject("Controls", "lblSalePrice")%>
                                    :</strong>
                                    <span id="txtTransferPrice"></span>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <input type="button" id="btnSaveService" value="Save Service" style="width: 100px;" onclick="btnSaveServiceClick()" />
                            </td>
                            <td width="10px">&nbsp;
                            </td>
                            <td align="left">
                                <input type="button" id="btnCancel" value="Cancel" style="width: 100px;" onclick="btnCancelClick()" />
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
        </div>
        <div id="dialog-searchCust" title='<%= GetGlobalResourceObject("MakeReservation", "searchCustLabel") %>'
            style="display: none;">
            <div id="searchCust">
            </div>
        </div>
        <div id="dialog-resCustomerAddress" title='<%= GetGlobalResourceObject("MakeReservation", "lblCustomerAddress") %>'
            style="display: none;">
            <iframe id="resCustomerAddress" runat="server" height="490px" width="570px" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-resCustOtherInfo" title='<%= GetGlobalResourceObject("MakeReservation", "lblCustomerOtherInfo") %>'
            style="display: none;">
            <iframe id="resCustOtherInfo" runat="server" height="400px" width="410px" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-message" title="" style="display: none;">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span><span id="messages">Message</span>
            </p>
        </div>
    </form>
</body>
</html>
