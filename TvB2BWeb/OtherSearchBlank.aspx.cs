﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;

public partial class OtherSearchBlank : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["SearchPLData"] = null;
            Session["Criteria"] = null;
            User UserData = (User)Session["UserData"];
            Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url);//"onlyHotelLongDistanceTripsBlank.aspx";

            if (!string.IsNullOrEmpty(Request.Params["B2BMenuCat"]) && !string.IsNullOrEmpty(Request.Params["PackType"]))
            {
                string b2bMenuCat = (string)Request.Params["B2BMenuCat"];
                string packType = (string)Request.Params["PackType"];
                if (string.Equals(packType, "H"))
                {
                    Response.Redirect("~/PackageSearch.aspx" + Request.Url.Query);
                }
                else
                    if (string.Equals(packType, "O"))
                    {
                        Response.Redirect("~/OnlyHotelSearch.aspx" + Request.Url.Query);
                    }
                    else
                        if (string.Equals(packType, "U"))
                        {
                            Response.Redirect("~/CruiseSearch.aspx" + Request.Url.Query);
                        }

            }
            else Response.Redirect("~/Default.aspx");
        }
    }
}
