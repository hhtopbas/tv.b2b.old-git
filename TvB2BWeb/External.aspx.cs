﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Web.Services;
using System.Text;
using TvTools;
using System.Web.Configuration;

public partial class ExternalLink : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        string url = Request.Url.Segments[Request.Url.Segments.Count() - 1].ToString();
        string queryStr = Request.Url.Query.ToString();
        Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url);  //Session["Menu"] = url + queryStr;
    }

    [WebMethod(EnableSession = true)]
    public static string getPageUrl(string LinkType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string extLink = Conversion.getStrOrNull(WebConfigurationManager.AppSettings["ExternalPage_" + LinkType]);
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Safiran)) {
            if (string.Equals(LinkType, "ATLAS") || string.Equals(LinkType, "BILETBANK") || string.Equals(LinkType, "WORLDHOTELS") || string.Equals(LinkType, "CREDITCARD")) {
                string qString = string.Empty;
                qString += "?agency=";
                qString += UserData.AgencyID;
                qString += "&user=";
                qString += UserData.UserID;
                extLink += qString;
            } else if (string.Equals(LinkType, "AXXA")) {
                string qString = string.Empty;
                qString += "?user=";
                qString += UserData.AgencyID + "/" + UserData.UserID;
                extLink += qString;
            } else if (string.Equals(LinkType, "AXXAKK")) {
                string qString = string.Empty;
                qString += "?agency=";
                qString += UserData.AgencyID;
                qString += "&user=";
                qString += UserData.UserID;
                extLink += qString;
            }
        } else if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro)) {
            //http: //resurse.fibula.ro/amadeus/?user={USERID}&session={SESSIONID}
            string qString = string.Empty;
            qString += "?user=";
            qString += UserData.UserID;
            qString += "&session=";
            qString += UserData.SID;
            extLink += qString;
        }
        return extLink;
    }
}
