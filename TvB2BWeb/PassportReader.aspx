﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PassportReader.aspx.cs" Inherits="PassportReader" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE10" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta content="MSHTML 5.00.3700.6699" name="GENERATOR" />

  <title>Default_Page</title>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
  <meta content="MSHTML 5.00.3700.6699" name="GENERATOR">  
  <script src="Scripts/jquery.min.js" type="text/javascript"></script>

  <style type="text/css">
    body { width: 420px; height: 420px; }
  </style>

  <script language="javascript" type="text/javascript">
    activexUsable = true;
    start = 0;

    var passportData = new Object();
    passportData.Field1 = '';
    passportData.Field2 = '';
    passportData.Field3 = '';

    function savePassportData() {
      window.close();
      self.parent.returnPassportScan(passportData, true);
    }
  </script>

</head>
<body onload="prInit();" onunload="prClose();">
  <div style="width: 100%;">
    preapi.ocx <font id="ocxver">- uncknown -</font>
    <p>
      <img id="init" name="init" src="Images\init.png" alt="" /><br />
      <object classid="clsid:D44C93E2-461F-4D53-AD54-96F503AC7CF3" height="288" id="GxImage1"
        style="height: 288px; left: 0px; top: 0px; width: 384px" width="384">
        <param name="_Version" value="117506304">
        <param name="_ExtentX" value="10160">
        <param name="_ExtentY" value="7620">
        <param name="_StockProps" value="1">
        <param name="BackColor" value="2304807">
        <param name="ImgSrcParam" value="available">
        <param name="AutoRefresh" value="-1">
        <param name="AutoSize" value="0">
        <param name="HasWindow" value="-1">
        <param name="StretchImage" value="0">
        <param name="SaveQuality" value="0">
        <param name="ViewMode" value="1">
        <param name="StretchMode" value="1">
        <param name="Zoom" value="1">
        <param name="HZoomPos" value="0">
        <param name="VZoomPos" value="0">
        <param name="WndLeft" value="0">
        <param name="WndTop" value="0">
        <param name="WndRight" value="0">
        <param name="WndBottom" value="0">
        <param name="Mirror" value="0">
        <param name="Rotation" value="0">
        <param name="AutoUpdateImage" value="-1">
        <param name="LineColor" value="0">
        <param name="LineBkColor" value="16777215">
        <param name="LineWidth" value="1">
        <param name="LineStyle" value="0">
        <param name="DrawStyle" value="0">
        <param name="DrawMode" value="2">
      </object>
      <object classid="clsid:1D89105E-8F52-400A-8B75-4FB2AD357AB6" id="PrApiOCX1" style="left: 0px; top: 0px">
        <param name="_Version" value="33619969">
        <param name="_ExtentX" value="847">
        <param name="_ExtentY" value="847">
        <param name="_StockProps" value="0">
        <param name="EventTypes" value="7">
        <param name="Freerunmode" value="4">
        <param name="PreviewLight" value="1">
        <param name="CaptureOnMotDet" value="0">
        <param name="RuntimeConfig" value="3">
      </object>
      <object classid="clsid:8A458222-EB58-4277-967F-27FAC179E21C" id="PrDocOCX1" style="left: 0px; top: 0px">
        <param name="_Version" value="33619969">
        <param name="_ExtentX" value="847">
        <param name="_ExtentY" value="847">
        <param name="_StockProps" value="0">
        <param name="RuntimeConfig" value="3">
      </object>
    </p>
    <p>
      <textarea id="MRZ" readonly="readOnly" cols="45" rows="3"></textarea>
    </p>
    <p>
      <input type="button" onclick="savePassportData();" value="Import passport data" />
      &nbsp;&nbsp;&nbsp;&nbsp;
      <button onclick="prStartCapture();" style="left: 10px; top: 382px" id="button1" name="button1">Capture</button>&nbsp;&nbsp;Process time: <font id="proctime">0</font> ms.      
    </p>

  </div>
  <script language="JavaScript" type="text/javascript">
    var MRZ = document.getElementById('MRZ');
    var init = document.getElementById('init');
    var ocxver = document.getElementById('ocxver');

    function AddToMRZ(str, fieldNumber) {						//print a line
      if (MRZ.value != "") MRZ.value += "\n";
      MRZ.value += str;
      switch (fieldNumber) {
        case 1:
          passportData.Field1 = str;
          break;
        case 2:
          passportData.Field2 = str;
          break;
        case 3:
          passportData.Field3 = str;
          break;
      }
      //alert(fieldNumber + ';' + str);
    }

    function prInit() {							//init
      MRZ.value = "";
      try {									//testing ActiveX and PR system
        if (isNaN(GxImage1.version) || isNaN(PrApiOCX1.version))
        { AddToMRZ("The Passport Reader is not installed correctly", 0); activexUsable = false; }
      } catch (e) { AddToMRZ("ActiveX is not enabled.", 0); activexUsable = false; }
      if (activexUsable) {
        a = "";
        temp = PrApiOCX1.version;			//print version
        ocxver.innerText = a.concat((temp >> 24) & 255, '.', (temp >> 16) & 255, '.', (temp >> 8) & 255, '.', temp & 255);
        if (temp > 0x02000900) {
          a = PrApiOCX1.GetErrorString();	//init test
          AddToMRZ(a, 0);
          if (a != "") activexUsable = false;
        }
      }
      if (activexUsable) {
        GxImage1.Connect(PrApiOCX1.GetImageSource(), PrApiOCX1.GetSourceHandle());
        GxImage1.ImgSrcParam = "infra";
        ret = PrApiOCX1.UseDeviceN(0, 2);	//use a device

        if (!ret) {
          alert("Cannot use device.");
          activexUsable = false;
        }
      }
      init.style.display = "none";
      if (activexUsable) {						//calibrate if needed
        if (!PrApiOCX1.IsCalibrated(0x01000000)) prCalibrate();
      }
    }

    function prClose() {							//close the device
      if (activexUsable) {
        PrApiOCX1.CloseDevice();
      }
    }

    function prCalibrate() {						//calibration process
      if (!activexUsable) return;
      PrApiOCX1.Freerunmode = 0;
      alert("Put the calibration image onto the window and press <OK>!");
      i = PrApiOCX1.Calibrate(0x01000000);
      if (i) {
        alert("Calibration done");
        PrApiOCX1.Freerunmode = 4;
      }
      else alert("Calibration failed");
    }

    function prStartCapture() {
      if (!activexUsable) return;
      tim = new Date;
      start = tim.getTime();
      li = new Array(1);
      li[0] = PrApiOCX1.GetLightMask(2, 0);
      PrApiOCX1.SetPagelightV(li.toString());
      PrApiOCX1.Capture();
    }

  </script>
  <script id="clientEventHandlersJS" language="javascript" type="text/javascript">


    function PrApiOCX1_PREvent(prevent, param1, param2) {
      if (prevent == 103) {						//clean up
        MRZ.value = "";
      }
      if (prevent == 101) {						//reading MRZ
        PrDocOCX1.Document = PrApiOCX1.GetMrz(0, 2, 0);
        AddToMRZ(PrDocOCX1.Field(1001), 1);
        AddToMRZ(PrDocOCX1.Field(1002), 2);
        AddToMRZ(PrDocOCX1.Field(1003), 3);
        tim = new Date;
        proctime.innerText = tim.getTime() - start;
      }
    }

    function PrApiOCX1_TestDocEvent(state) {	//displaying state of document detection
      motdet = ["No document detected.", "Document is moving."
        , "Take out the document!", "#", "Ready for reading."];
      status = motdet[state];
      if (state == 4) prStartCapture();
    }

    function PrApiOCX1_ButtonEvent(code, state) {	//starts process
      if (code == 65808 && state == 1) {
        status = "Button is pressed.";
        prStartCapture();
        status = "Take out the document!";
      }
    }

    function ConnectionEvent(connecting_devs) {	//connection
      alert(connecting_devs > 0 ? "Connected" : "Disconnected");
      activexUsable = true;
      if (connecting_devs < 0) prClose();
      else { prInit(); PrApiOCX1.FreerunMode = 4; }
    }


  </script>
  <script language="javascript" type="text/javascript" for="PrApiOCX1" event="PREvent(prevent,param1,param2)">

    PrApiOCX1_PREvent(prevent, param1, param2)

  </script>
  <script language="javascript" type="text/javascript" for="PrApiOCX1" event="TestDocEvent(state)">

    PrApiOCX1_TestDocEvent(state)

  </script>
  <script language="javascript" type="text/javascript" for="PrApiOCX1" event="ButtonEvent(code,state)">

    PrApiOCX1_ButtonEvent(code, state)

  </script>
  <script language="javascript" type="text/javascript" for="PrApiOCX1" event="ConnectionEvent(connecting_devs)">

    ConnectionEvent(connecting_devs)

  </script>
</body>
</html>
