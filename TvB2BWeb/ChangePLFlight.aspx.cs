﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Web.Services;
using System.Text;
using System.Threading;

public partial class ChangePLFlight : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        StringBuilder sb = new StringBuilder();

        List<ResServiceRecord> currentFlights = (from q in ResData.ResService
                                                 where string.Equals(q.ServiceType, "FLIGHT") && string.Equals(q.IncPack, "Y")
                                                 orderby q.BegDate, q.SeqNo
                                                 select q).ToList<ResServiceRecord>();
        if (currentFlights.Count < 2) return "";

        List<FlightInfosRecord> flightList = new Flights().spDepRetFlightInfo(UserData, ResData, ref errorMsg);
        List<FlightInfosRecord> flights = new List<FlightInfosRecord>();
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
        {

            var Query = from q in flightList
                        where q.DepSeat > 0
                        group q by new { FlightNo = q.DepFlightNo } into k
                        select new { FlightNo = k.Key.FlightNo };
            foreach (var q in Query)
            {
                foreach (FlightInfosRecord row in flightList)
                {
                    var retSeat = flightList.Where(w => w.DepFlightNo == q.FlightNo).Select(s => s);
                    bool retSeatEmpty = true;
                    if (retSeat.Count() > 0)
                        retSeatEmpty = retSeat.Sum(s => s.RetSeat) > 0;
                    if (q.FlightNo == row.DepFlightNo && retSeatEmpty)
                    {
                        flights.Add(row);                        
                        break;
                    }
                }
            }
        }
        else flights = flightList;

        List<FlightDayClassRecord> flightDayClass = new Flights().getFlightDayClass(UserData, flights, ResData.ResMain.PLOperator, currentFlights.FirstOrDefault().FlgClass, currentFlights.LastOrDefault().FlgClass, ref errorMsg);
        int i = 0;
        foreach (FlightInfosRecord row in flights.Where(w => w.RetSeat > 0).ToList<FlightInfosRecord>())
        {
            FlightDetailRecord depFlight = new Flights().getFlightDetail(UserData, UserData.Market, row.DepFlightNo, row.DepFlyDate.Value, ref errorMsg);
            FlightDetailRecord retFlight = new Flights().getFlightDetail(UserData, UserData.Market, row.RetFlightNo, row.RetFlyDate.Value, ref errorMsg);
            if (depFlight != null && retFlight != null)
            {
                ++i;
                sb.Append("<br />");
                sb.Append("<table class=\"flight ui-widget-content ui-corner-all ui-button-text\" width=\"100%\">");

                sb.Append("<tr>");
                sb.Append("<td colspan=\"2\" class=\"ui-widget ui-widget-header\" align=\"center\" style=\"height:30px;\">");
                sb.AppendFormat("<span class=\"lblFlightsHeader\">{0}</span>", "Departure Flight");
                sb.Append("</td>");
                sb.Append("<td style=\"width:4px;\">&nbsp;</td>");
                sb.Append("<td colspan=\"2\" class=\"ui-widget ui-widget-header\" align=\"center\" style=\"height:30px;\">");
                sb.AppendFormat("<span class=\"lblFlightsHeader\">{0}</span>", "Return Flight");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td colspan=\"2\"><span class=\"txtFlights\">{0}</span></td>",
                    depFlight.FlightNo + " (" + depFlight.FlyDate.Value.ToShortDateString() + " " + depFlight.DepAirport + " [" + (depFlight.DepTime.HasValue ? depFlight.DepTime.Value.ToShortTimeString() : "") + "] => " + depFlight.ArrAirport + " [" + (depFlight.ArrTime.HasValue ? depFlight.ArrTime.Value.ToShortTimeString() : "") + "])");
                sb.Append("<td style=\"width:4px;\">&nbsp;</td>");
                sb.AppendFormat("<td colspan=\"2\"><span class=\"txtFlights\">{0}</span></td>",
                    retFlight.FlightNo + " (" + retFlight.FlyDate.Value.ToShortDateString() + " " + retFlight.DepAirport + " [" + (retFlight.DepTime.HasValue ? retFlight.DepTime.Value.ToShortTimeString() : "") + "] => " + retFlight.ArrAirport + " [" + (retFlight.ArrTime.HasValue ? retFlight.ArrTime.Value.ToShortTimeString() : "") + "])");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\" style=\"width: 190px;\"><span class=\"lblAirline\">{0} : </span></td>", "AirLine");
                sb.AppendFormat("<td style=\"width: 235px;\"><span class=\"txtAirLine\">{0}</span></td>", depFlight.AirlineName);
                sb.Append("<td style=\"width:4px;\">&nbsp;</td>");
                sb.AppendFormat("<td align=\"right\" style=\"width: 190px;\"><span class=\"lblAirline\">{0} : </span></td>", "AirLine");
                sb.AppendFormat("<td style=\"width: 235px;\"><span class=\"txtAirLine\">{0}</span></td>", retFlight.AirlineName);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\" style=\"width: 190px;\"><span class=\"lblDepCityAirport\">{0} : </span></td>", "Departure (City / Airport)");
                sb.AppendFormat("<td><span class=\"txtDepCityAirPort\">{0}</span></td>", depFlight.DepCityName + " / " + depFlight.DepAirportName);
                sb.Append("<td style=\"width:4px;\">&nbsp;</td>");
                sb.AppendFormat("<td align=\"right\" style=\"width: 190px;\"><span class=\"lblDepCityAirport\">{0} : </span></td>", "Departure (City / Airport)");
                sb.AppendFormat("<td><span class=\"txtDepCityAirPort\">{0}</span></td>", retFlight.DepCityName + " / " + retFlight.DepAirportName);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\" style=\"width: 190px;\"><span class=\"lblArrCityAirport\">{0} : </span></td>", "Arrival (City / Airport)");
                sb.AppendFormat("<td><span class=\"txtArrCityAirport\">{0}</span></td>", depFlight.ArrCityName + " / " + depFlight.ArrAirportName);
                sb.Append("<td style=\"width:4px;\">&nbsp;</td>");
                sb.AppendFormat("<td align=\"right\" style=\"width: 190px;\"><span class=\"lblArrCityAirport\">{0} : </span></td>", "Arrival (City / Airport)");
                sb.AppendFormat("<td><span class=\"txtArrCityAirport\">{0}</span></td>", retFlight.ArrCityName + " / " + retFlight.ArrAirportName);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\" style=\"width: 190px;\"><span class=\"lblDepDateTime\">{0} : </span></td>", "Departure (Date / Time)");
                sb.AppendFormat("<td><span class=\"txtDepDateTime\">{0}</span></td>", depFlight.FlyDate.Value.ToShortDateString() + " / " + depFlight.DepTime.Value.ToShortTimeString());
                sb.Append("<td style=\"width:4px;\">&nbsp;</td>");
                sb.AppendFormat("<td align=\"right\" style=\"width: 190px;\"><span class=\"lblDepDateTime\">{0} : </span></td>", "Departure (Date / Time)");
                sb.AppendFormat("<td><span class=\"txtDepDateTime\">{0}</span></td>", retFlight.FlyDate.Value.ToShortDateString() + " / " + retFlight.DepTime.Value.ToShortTimeString());
                sb.Append("</tr>");

                string depFlightClass = string.Empty;
                var depClass = from q in flightDayClass
                               where q.FlightNo == row.DepFlightNo
                               group q by new { Code = q.SClass, Name = q.NameL } into k
                               select new { Code = k.Key.Code, Name = k.Key.Name };

                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                    depFlightClass += string.Format("<option value=\"{0}\">{1}</option>", currentFlights.FirstOrDefault().FlgClass, currentFlights.FirstOrDefault().FlgClass);
                else
                    foreach (var r1 in depClass)
                        depFlightClass += string.Format("<option value=\"{0}\" {2}>{1}</option>", r1.Code, r1.Name, Equals(currentFlights.FirstOrDefault().FlgClass, r1.Code) ? "selected=\"selected\"" : "");

                string retFlightClass = string.Empty;
                var retClass = from q in flightDayClass
                               where q.FlightNo == row.RetFlightNo
                               group q by new { Code = q.SClass, Name = q.NameL } into k
                               select new { Code = k.Key.Code, Name = k.Key.Name };
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                    retFlightClass += string.Format("<option value=\"{0}\">{1}</option>", currentFlights.LastOrDefault().FlgClass, currentFlights.LastOrDefault().FlgClass);
                else
                    foreach (var r1 in retClass)
                        retFlightClass += string.Format("<option value=\"{0}\" {2}>{1}</option>", r1.Code, r1.Name, Equals(currentFlights.LastOrDefault().FlgClass, r1.Code) ? "selected=\"selected\"" : "");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\" style=\"width: 190px;\"><span class=\"lblFlightClass\">{0} : </span></td>", "Flight Class");
                sb.AppendFormat("<td><select id=\"flightClassD_{0}\" class=\"ddlFlightClass\">{1}</select></td>", i.ToString(), depFlightClass);
                sb.Append("<td style=\"width:4px;\">&nbsp;</td>");
                sb.AppendFormat("<td align=\"right\" style=\"width: 190px;\"><span class=\"lblFlightClass\">{0} : </span></td>", "Flight Class");
                sb.AppendFormat("<td><select id=\"flightClassR_{0}\" class=\"ddlFlightClass\">{1}</select></td>", i.ToString(), retFlightClass);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td colspan=\"2\">");
                string depFlightNo = depFlight.FlightNo;
                string retFlightNo = retFlight.FlightNo;
                sb.AppendFormat("<input type=\"button\" value=\"{0}\" onclick=\"selectFlight('{1}','{2}', '{3}');\"", "Select", depFlightNo, retFlightNo, i.ToString());
                sb.Append("class=\"ui-button ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" />");
                sb.Append("</td>");
                sb.Append("<td>&nbsp;</td><td colspan=\"2\"></td></tr>");
                sb.Append("</table>");
            }
        }

        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string changeFlights(string DepFlightNo, string DepClass, string RetFlightNo, string RetClass)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        TvBo.ResDataRecord oldResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        try
        {
            List<ResServiceRecord> resService = ResData.ResService;

            List<ResServiceRecord> currentFlights = (from q in ResData.ResService
                                                     where string.Equals(q.ServiceType, "FLIGHT") && string.Equals(q.IncPack, "Y")
                                                     orderby q.BegDate, q.SeqNo
                                                     select q).ToList<ResServiceRecord>();
            ResServiceRecord depFlightService = resService.Find(f => f.RecID == currentFlights.FirstOrDefault().RecID);
            FlightDetailRecord depFlightDetail = new Flights().getFlightDetail(UserData, UserData.Market, DepFlightNo, depFlightService.BegDate.Value, ref errorMsg);
            string depServiceName = depFlightDetail.FlightNo + " (" + depFlightDetail.DepAirport + "->" + depFlightDetail.ArrAirport + "), " + DepClass + ", (" +
                                         (depFlightDetail.DepTime.HasValue ? depFlightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (depFlightDetail.ArrTime.HasValue ? depFlightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";

            ResServiceRecord retFlightService = resService.Find(f => f.RecID == currentFlights.LastOrDefault().RecID);
            FlightDetailRecord retFlightDetail = new Flights().getFlightDetail(UserData, UserData.Market, RetFlightNo, retFlightService.BegDate.Value, ref errorMsg);
            string retServiceName = retFlightDetail.FlightNo + " (" + retFlightDetail.DepAirport + "->" + retFlightDetail.ArrAirport + "), " + RetClass + ", (" +
                                        (retFlightDetail.DepTime.HasValue ? retFlightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (retFlightDetail.ArrTime.HasValue ? retFlightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";
            serviceDefaultStatusRecord DtStat = new serviceDefaultStatusRecord();
            depFlightService.Service = DepFlightNo;
            depFlightService.ServiceName = depServiceName;
            depFlightService.ServiceNameL = depServiceName;
            depFlightService.FlgClass = DepClass;
            depFlightService.Supplier = string.Empty;
            depFlightService.SupplierName = string.Empty;
            depFlightService.SupplierNameL = string.Empty;
            DtStat = new Reservation().getDefaultStatus("FLIGHT", DepFlightNo, ref errorMsg);
            depFlightService.StatConf = DtStat.ConfStat;

            retFlightService.Service = RetFlightNo;
            retFlightService.ServiceName = retServiceName;
            retFlightService.ServiceNameL = retServiceName;
            retFlightService.FlgClass = RetClass;
            retFlightService.Supplier = string.Empty;
            retFlightService.SupplierName = string.Empty;
            retFlightService.SupplierNameL = string.Empty;
            DtStat = new Reservation().getDefaultStatus("FLIGHT", RetFlightNo, ref errorMsg);
            retFlightService.StatConf = DtStat.ConfStat;

            currentFlights = (from q in ResData.ResService
                              where string.Equals(q.ServiceType, "FLIGHT") && string.Equals(q.IncPack, "Y")
                              orderby q.BegDate, q.SeqNo
                              select q).ToList<ResServiceRecord>();
            oldResData = new Reservation().extraServiceControl(UserData, oldResData, depFlightService, ref errorMsg);
            oldResData = new Reservation().extraServiceControl(UserData, oldResData, retFlightService, ref errorMsg);

            if (new TvBo.Reservation().reCalcResData(UserData, ref oldResData, ref errorMsg))
            {
                currentFlights = (from q in oldResData.ResService
                                  where string.Equals(q.ServiceType, "FLIGHT") && string.Equals(q.IncPack, "Y")
                                  orderby q.BegDate, q.SeqNo
                                  select q).ToList<ResServiceRecord>();
                resService = oldResData.ResService;
                foreach (ResServiceRecord row in currentFlights)
                {
                    ResServiceRecord _flightService = resService.Find(f => f.RecID == row.RecID);
                    SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, row.Supplier, ref errorMsg);
                    _flightService.SupplierName = supplierRec.Name;
                    _flightService.SupplierNameL = supplierRec.NameL;
                }
                var exSer = oldResData.ResServiceExt.Where(w => !w.Removed);
                foreach(var s in exSer)
                {
                    if(oldResData.ResConExt.Where(w=>w.Removed && w.ServiceID == s.RecID).Count() > 1)
                    {
                        Console.Write(s.ServiceTypeNameL);
                    }
                }

                HttpContext.Current.Session["ResData"] = oldResData;

                return "";
            }
            else return errorMsg;
        }
        catch (Exception Ex)
        {
            return Ex.Message;
        }
    }
}
