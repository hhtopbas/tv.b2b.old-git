﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Text;
using System.Web.Services;
using TvTools;

public partial class onlyHotelHolPackCatFilter : BasePage
{

    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    User UserData = (User)Session["UserData"];
    //    IsPostBackBefore(UserData);
    //    if (!IsPostBack)
    //        IsPostBackInside(sender, e, UserData);
    //    IsPosBackAfter(UserData);
    //}

    //void IsPostBackBefore(User UserData)
    //{
    //    CultureInfo ci = UserData.Ci;
    //    if (ci.Name.ToLower() == "lt-lt")
    //        ci.DateTimeFormat.ShortDatePattern = "yyyy.MM.dd";
    //    Thread.CurrentThread.CurrentCulture = ci;
    //    Thread.CurrentThread.CurrentUICulture = ci;
    //    //string datePatern = new TvBo.Common().getDateFormat(UserData.Ci);

    //    //ppcCheckIn.Culture = ci.Name + " " + ci.EnglishName;
    //    //ppcCheckIn.Format = datePatern.Replace('/', ' ');
    //    //ppcCheckIn.From.Date = DateTime.Today;
    //}

    //void IsPostBackInside(object sender, EventArgs e, User UserData)
    //{
    //    //string errorMsg = string.Empty;
    //    //ppcCheckIn.DateValue = DateTime.Today.Date;
    //}

    //void IsPosBackAfter(User UserData)
    //{
    //}
    public static string twoLetterISOLanguageName = "en";

    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        if (UserData.Ci.Name.ToLower() == "lt-lt")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "yyyy.MM.dd";
        else if (UserData.Ci.Name.ToLower() == "lv-lv")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<string> lang = new List<string>() { "ar-DZ", "ar-DZ", "ar", "en-AU", "en-GB", "en-NZ", "fr-CH", "fr", "pt-BR", "pt", "sr-SR", "sr-CS", "sr", "zh-CN", "zh-HK", "zh-TW" };
        if (lang.Contains(UserData.Ci.Name))
            twoLetterISOLanguageName = UserData.Ci.Name;
        else
            twoLetterISOLanguageName = UserData.Ci.TwoLetterISOLanguageName != "en" ? UserData.Ci.TwoLetterISOLanguageName : "";
    }


    private void WindowAlert(string Message, bool TransferPage, string PageUrl)
    {
        string jscript = "showDialog(" + UICommon.EncodeJsString(Message) + ");";

        if (ScriptManager.GetCurrent(this.Page) != null)
        {
            if (ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "showDialogPopup", jscript, true);
            else Page.ClientScript.RegisterStartupScript(this.GetType(), "showDialogPopup", jscript, true);
        }
        else Page.ClientScript.RegisterStartupScript(this.GetType(), "showDialogPopup", jscript, true);
    }

    [WebMethod(EnableSession = true)]
    public static string CreateCriteria()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;

        bool CurControl = false;
        object curControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowMarketCur");
        if (curControl != null) CurControl = (bool)curControl;

        bool ExtAllotCont = false;
        object extAllotControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ExtAllotControl");
        if (extAllotControl != null) ExtAllotCont = (bool)extAllotControl;

        bool ShowExpandDay = true;
        object showExpandDay = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowExpandDay");
        if (showExpandDay != null) showExpandDay = (bool)showExpandDay;

        bool ShowSecondDay = true;
        object showSecondDay = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowSecondDay");
        if (showSecondDay != null) ShowSecondDay = (bool)showSecondDay;

        SearchCriteria criteria = new SearchCriteria
        {
            SType = SearchType.PackageSearch,
            FromRecord = 1,
            ToRecord = 25,
            ShowAvailable = true,
            CurrentCur = UserData.SaleCur,
            ExpandDate = 0,
            NightFrom = 7,
            NightTo = 7,
            RoomCount = 1,
            RoomsInfo = new List<SearchCriteriaRooms>(),
            CheckIn = DateTime.Today,
            UseGroup = GroupSearch,
            CurControl = CurControl,
            ExtAllotControl = ExtAllotCont,
            ShowExpandDay = ShowExpandDay,
            ShowSecondDay = ShowSecondDay
        };
        HttpContext.Current.Session["Criteria"] = criteria;
        return "";
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData(Int16? HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        if (UserData.Ci.Name.ToLower() == "lt-lt")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "yyyy.MM.dd";
        else if (UserData.Ci.Name.ToLower() == "lv-lv")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        CreateCriteria();

        SearchCriteria criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];
        //criteria.HolPackCat
        Int16? defaultNight = Conversion.getInt16OrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "DefaultDays"));
        Int16 DefaultNight = defaultNight.HasValue ? defaultNight.Value : Convert.ToInt16(7);
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            switch (HolPackCat)
            {
                case 1: DefaultNight = 1; break;
                case 2: DefaultNight = 1; break;
                case 3: DefaultNight = 1; break;
                default: DefaultNight = 7; break;
            }
        //DefaultNight = (HolPackCat.HasValue && HolPackCat.Value == 1) ? Convert.ToInt16(1) : Convert.ToInt16(7);

        Int16? defaultNight2 = Conversion.getInt16OrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "DefaultDays2"));
        Int16 DefaultNight2 = defaultNight2.HasValue ? defaultNight2.Value : Convert.ToInt16(7);
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            switch (HolPackCat)
            {
                case 1: DefaultNight2 = 16; break;
                case 2: DefaultNight2 = 21; break;
                case 3: DefaultNight2 = 14; break;
                default: DefaultNight2 = 7; break;
            }

        string fltCheckInDay = string.Empty;
        for (int i = 0; i <= (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ? 60 : 7); i++)
            fltCheckInDay += string.Format("<option value='{0}'>{1}</option>", i, "+" + i + " Day");

        string fltNight = string.Empty;
        string fltNight2 = string.Empty;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
        {
            List<Int16> nights = new TvBo.Common().getPLNights(UserData.Market, SearchType.OnlyHotelSearch, ref errorMsg);
            if (nights == null || nights.Count < 1)
            {
                fltNight += string.Format("<option value='{0}' {1}>{2}</option>", "7", "selected='selected'", "7");
            }
            else
            {
                foreach (Int16 row in nights)
                    fltNight += string.Format("<option value='{0}' {1}>{2}</option>", row, row == DefaultNight ? "selected='selected'" : "", row);
            }
        }
        else
        {
            for (int i = 1; i < 22; i++)
                fltNight += string.Format("<option value='{0}' {1}>{2}</option>", i, i == DefaultNight ? "selected='selected'" : "", i);

            for (int i = 1; i < 22; i++)
                fltNight2 += string.Format("<option value='{0}' {1}>{2}</option>", i, i == DefaultNight2 ? "selected='selected'" : "", i);
        }

        string fltRoomCount = string.Empty;
        for (int i = 1; i <= (UserData.AgencyRec.MaxRoomCnt.HasValue ? UserData.AgencyRec.MaxRoomCnt.Value : UserData.MaxRoomCount); i++)
            fltRoomCount += string.Format("<option value='{0}'>{1}</option>", i, i);

        bool? currencyConvert = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "ShowMarketCur"));

        packageSearchFilterFormData data = new packageSearchFilterFormData();
        data.fltCheckInDay = fltCheckInDay;
        data.fltNight = fltNight;
        data.fltNight2 = fltNight2;
        data.fltRoomCount = fltRoomCount;
        data.CurrencyConvert = currencyConvert.HasValue ? currencyConvert.Value : true;
        List<ReadyCurrencyRecord> getCurrencyList = new TvBo.Common().getReadyCurrency(UserData.Market, UserData.SaleCur, ref errorMsg);
        string curList = string.Empty;
        curList += "<select id=\"currentCur\">";
        foreach (ReadyCurrencyRecord row in getCurrencyList)
        {
            curList += string.Format("<option value=\"{0}\" {2} >{1}</option>",
                                        row.Code,
                                        row.NameL,
                                        Equals(UserData.SaleCur, row.Code) ? "selected=\"selected\"" : "");
        }
        curList += "</select>";
        data.MarketCur = string.Format("<input id=\"CurrencyConvert\" type=\"checkbox\" {0} /><label for=\"CurrencyConvert\">{1}</label>&nbsp;{2}",
                data.CurrencyConvert ? "checked='checked'" : "",
                HttpContext.GetGlobalResourceObject("LibraryResource", "lblShowInPrice2"),
                curList);

        DateTime ppcCheckIn = DateTime.Today.Date;
        Int64 days = (ppcCheckIn - (new DateTime(1970, 1, 1))).Days - 1;
        bool? showFilterPackage = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ShowFilterPackage"));
        data.ShowFilterPackage = !showFilterPackage.HasValue || (showFilterPackage.HasValue && showFilterPackage.Value == true);
        data.CheckIn = (days * 86400000).ToString();
        data.ShowStopSaleHotels = true;
        data.CustomRegID = UserData.CustomRegID;
        data.ShowExpandDay = criteria.ShowExpandDay;
        data.ShowSecondDay = criteria.ShowSecondDay;
        data.DateFormat = new TvBo.Common().getDateFormat(UserData.Ci);
        data.ShowOfferBtn = string.Equals(Conversion.getStrOrNull(System.Configuration.ConfigurationManager.AppSettings["ClientOfferVersion"]), "V2");
        return Newtonsoft.Json.JsonConvert.SerializeObject(data);
    }

    public static string getSearchFilterData(Int16? HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["SearchPLDataOther" + HolPackCat.ToString()] == null)
        {
            bool GroupSearch = false;
            object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
            if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
            SearchPLData filterData = CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.OtherSearch, HolPackCat, string.Empty);
            HttpContext.Current.Session["SearchPLDataOther" + HolPackCat.ToString()] = filterData;
        }
        return "OK";
    }

    [WebMethod(EnableSession = true)]
    public static string getHolPackCat(Int16? HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        SearchPLData data = (SearchPLData)HttpContext.Current.Session["SearchPLDataOther" + HolPackCat.ToString()];
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<CodeName> holPackCat = new Search().getSearchWithOutCTHolPackCat(useLocalName, data, ref errorMsg);
        if (holPackCat.Count > 0)
            return Newtonsoft.Json.JsonConvert.SerializeObject(holPackCat);
        else return string.Empty;
    }

    [WebMethod(EnableSession = true)]
    public static string getArrival(string HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        HttpContext.Current.Session["SearchPLDataOther" + HolPackCat.ToString()] = null;

        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["SearchPLDataOther" + HolPackCat.ToString()] == null)
            getSearchFilterData(Conversion.getInt16OrNull(HolPackCat));
        if (HttpContext.Current.Session["SearchPLDataOther" + HolPackCat.ToString()] == null) return "";

        Int32? depCity = null;//Conversion.getInt32OrNull(DepCity);        
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        SearchPLData data = (SearchPLData)HttpContext.Current.Session["SearchPLDataOther" + HolPackCat.ToString()];
        List<plLocationData> arrCityData = new Search().getSearchArrCitys(useLocalName, data, depCity, ref errorMsg);
        if (HolPackCat == "1")
        {
            var retVal = from q in arrCityData
                         where q.HolPackCat == Conversion.getInt16OrNull(HolPackCat)
                         orderby q.ArrCountryName, q.ArrCityName
                         group q by new { country = q.ArrCountryName, RecID = q.ArrCity, Name = q.ArrCityName } into k
                         select new { Country = k.Key.country, RecID = k.Key.RecID, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(retVal);
        }
        else
        {
            var retVal = from q in arrCityData
                         where q.HolPackCat == Conversion.getInt16OrNull(HolPackCat)
                         orderby q.ArrCountryName, q.ArrCityName
                         group q by new { country = q.ArrCountryName, RecID = q.ArrCity, Name = q.ArrCityName } into k
                         select new { Country = k.Key.country, RecID = k.Key.RecID, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(retVal);
        }

    }

    [WebMethod(EnableSession = true)]
    public static string getHolpack(string ArrCity, string HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        Int32? depCity = null;
        Int32? arrCity = Conversion.getInt32OrNull(ArrCity);
        if (HttpContext.Current.Session["SearchPLDataOther" + HolPackCat.ToString()] == null) return "";
        SearchPLData data = (SearchPLData)HttpContext.Current.Session["SearchPLDataOther" + HolPackCat.ToString()];
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        HolPackCat =UserData.CustomRegID!=Common.crID_Novaturas_Lt &&  HolPackCat == "1" ? null : HolPackCat;

        List<CodeName> holpackData = new Search().getSearchHolPack(useLocalName, data, depCity, null, arrCity, Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        var retVal = from q in holpackData
                     orderby q.Name, q.Code
                     group q by new
                     {
                         Name = q.Name,
                         Code = q.Code + ';' + data.plHolPacks.Find(f => f.HolPack == q.Code).DepCity.ToString() + ';' + data.plHolPacks.Find(f => f.HolPack == q.Code).ArrCity.ToString()
                     } into k
                     select new { Code = k.Key.Code, Name = k.Key.Name };
        return Newtonsoft.Json.JsonConvert.SerializeObject(retVal);
    }

    [WebMethod(EnableSession = true)]
    public static string getResourtData(string ArrCity, string HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        TvBo.SearchPLData data = TvBo.CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.OnlyHotelSearch, Conversion.getInt16OrNull(HolPackCat), string.Empty);
        List<TvBo.LocationIDName> resortData = new TvBo.Search().getSearchResorts(useLocalName, data, null, Conversion.getInt32OrNull(ArrCity), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(resortData);
    }

    [WebMethod(EnableSession = true)]
    public static string getRoomInfo(string RoomCount)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        Int16 maxChildAge = UserData.TvParams.TvParamReser.MaxChdAgeB2Bsrc.HasValue ? Convert.ToInt16(Math.Floor(UserData.TvParams.TvParamReser.MaxChdAgeB2Bsrc.Value)) : (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ? Convert.ToInt16(18) : Convert.ToInt16(12));
        StringBuilder html = new StringBuilder();
        plMaxPaxCounts maxPaxData = new plMaxPaxCounts();
        object _paxData = new TvBo.Common().getFormConfigValue("SearchPanel", "PaxData");
        if (_paxData != null)
        {
            plMaxPaxCounts paxData = Newtonsoft.Json.JsonConvert.DeserializeObject<plMaxPaxCounts>(_paxData.ToString());
            maxPaxData = paxData;
        }
        if (HttpContext.Current.Session["SearchPLData"] != null && string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            //if (((SearchPLData)HttpContext.Current.Session["SearchPLData"]).PlMaxPaxCount != null)
                maxPaxData = ((SearchPLData)HttpContext.Current.Session["SearchPLData"]).PlMaxPaxCount;
        //TVB2B-4008
        if (string.Equals(UserData.CustomRegID, Common.crID_Rezeda))
            maxPaxData.maxAdult = 12;
        Int32 roomCount = Conversion.getInt32OrNull(RoomCount).HasValue ? Conversion.getInt32OrNull(RoomCount).Value : 1;
        Int32 j = 0;
        for (int i = 1; i < roomCount + 1; i++)
        {
            html.AppendFormat("<div style=\"width:250px; clear:both;\"><b>{0} {1}</b></div>",
                    HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblRoom"),
                    i.ToString());
            html.AppendFormat("<div id=\"divRoomInfo{0}\" style=\"width:250px; clear:both; border-top: solid 1px #000000; \">", i.ToString());
            html.AppendFormat("  <div id=\"divAdult{0}\" style=\"float: left; width:100px;\">", i.ToString());
            html.AppendFormat("  <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblAdult").ToString());
            html.AppendFormat("  <select id=\"fltAdult{0}\" style=\"width: 50px;\">", i.ToString(), i.ToString());
            for (j = 1; j <= maxPaxData.maxAdult; j++)
            {
                if (j == 2)
                    html.AppendFormat("     <option value='{0}' selected=\"selected\">{1}</option>", j.ToString(), j.ToString());
                else html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            }
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");

            html.AppendFormat("  <div id=\"divChild{0}\" style=\"float: left; width:50px;\">", i.ToString());
            html.AppendFormat("  <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChild").ToString());
            html.AppendFormat("  <select id=\"fltChild{0}\" onchange=\"onChildChange('{1}')\" style=\"width: 50px;\">", i.ToString(), i.ToString());
            for (j = 0; j < 5; j++)
                html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");
            html.AppendFormat("</div>");

            html.AppendFormat("<div id=\"divRoomInfoChd{0}\" style=\"width:250px; clear:both; display:none;\">", i.ToString());
            html.AppendFormat(" <div id=\"divRoomInfoChd1{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
            string childAgeStr = string.Empty;
            if (HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString().IndexOf('{') > 0)
                childAgeStr = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString(), "1");
            else childAgeStr = HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 1";

            html.AppendFormat("   <span>{0}</span><br />", childAgeStr);
            html.AppendFormat("   <select id=\"fltRoomInfoChd1{0}\" style=\"width: 100%;\">", i.ToString());
            for (j = 0; j <= maxChildAge; j++)
                html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");
            html.AppendFormat(" <div id=\"divRoomInfoChd2{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
            if (HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString().IndexOf('{') > 0)
                childAgeStr = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString(), "2");
            else childAgeStr = HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 2";
            html.AppendFormat("   <span>{0}</span><br />", childAgeStr);
            html.AppendFormat("   <select id=\"fltRoomInfoChd2{0}\" style=\"width: 100%;\">", i.ToString());
            for (j = 0; j <= maxChildAge; j++)
                html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");

            html.AppendFormat(" <div id=\"divRoomInfoChd3{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
            if (HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString().IndexOf('{') > 0)
                childAgeStr = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString(), "3");
            else childAgeStr = HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 3";
            html.AppendFormat("   <span>{0}</span><br />", childAgeStr);
            html.AppendFormat("   <select id=\"fltRoomInfoChd3{0}\" style=\"width: 100%;\">", i.ToString());
            for (j = 0; j <= maxChildAge; j++)
                html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");

            html.AppendFormat(" <div id=\"divRoomInfoChd4{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
            if (HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString().IndexOf('{') > 0)
                childAgeStr = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString(), "4");
            else childAgeStr = HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 4";
            html.AppendFormat("   <span>{0}</span><br />", childAgeStr);
            html.AppendFormat("   <select id=\"fltRoomInfoChd4{0}\" style=\"width: 100%;\">", i.ToString());
            for (j = 0; j <= maxChildAge; j++)
                html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");

            html.AppendFormat("</div>");
        }
        return html.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getCategoryData(string ArrCity, string Resort, string HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
        TvBo.SearchPLData data = TvBo.CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.OnlyHotelSearch, Conversion.getInt16OrNull(HolPackCat), string.Empty);
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<TvBo.CodeName> categoryData = new TvBo.Search().getSearchCategorys(useLocalName, data, null, Conversion.getInt32OrNull(ArrCity), Resort, Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(categoryData);
    }

    [WebMethod(EnableSession = true)]
    public static string getHotelData(string ArrCity, string Resort, string Category, string Holpack, string HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
        TvBo.SearchPLData data = TvBo.CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.OnlyHotelSearch, Conversion.getInt16OrNull(HolPackCat), string.Empty);
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<TvBo.CodeHotelName> hotelData = new TvBo.Search().getSearchHotels(useLocalName, data, null, null, Conversion.getInt32OrNull(ArrCity), Resort, Category, Holpack, Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(hotelData);
    }

    [WebMethod(EnableSession = true)]
    public static string getRoomData(string DepCity, string ArrCity, string Resort, string Hotel, string HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
        TvBo.SearchPLData data = TvBo.CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.OnlyHotelSearch, Conversion.getInt16OrNull(HolPackCat), string.Empty);
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<TvBo.CodeName> hotelData = new TvBo.Search().getSearchRooms(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity), Resort, null, Hotel, Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(hotelData);
    }

    [WebMethod(EnableSession = true)]
    public static string getBoardData(string DepCity, string ArrCity, string Resort, string Hotel, string HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
        TvBo.SearchPLData data = TvBo.CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.OnlyHotelSearch, Conversion.getInt16OrNull(HolPackCat), string.Empty);
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<TvBo.CodeName> hotelData = new TvBo.Search().getSearchBoards(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity), Resort, null, Hotel, Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(hotelData);
    }

    [WebMethod(EnableSession = true)]
    public static string SetCriterias(string CheckIn, string ExpandDate, string NightFrom, string NightTo,
            string RoomCount, string roomsInfo, string Board, string Room, string DepCity, string ArrCity,
            string Resort, string Category, string Hotel, string CurControl, string CurrentCur,
            string ShowStopSaleHotels, string HolPack, string holPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["Criteria"] == null) CreateCriteria();
        SearchCriteria criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];
        DateTime checkIn = new DateTime(1970, 1, 1);
        checkIn = checkIn.AddDays((Convert.ToInt64(CheckIn) / 86400000) + 1);
        criteria.CheckIn = checkIn.Date;
        criteria.ExpandDate = Convert.ToInt16(ExpandDate);
        criteria.NightFrom = Convert.ToInt16(NightFrom);
        criteria.NightTo = Convert.ToInt16(NightTo);
        criteria.RoomCount = Convert.ToInt16(RoomCount);
        string _room = "[" + roomsInfo.Replace("|", "\"").Replace(')', '}').Replace('(', '{') + "]";
        List<SearchCriteriaRooms> rooms = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SearchCriteriaRooms>>(_room);
        criteria.RoomsInfo.Clear();
        int i = 0;
        foreach (SearchCriteriaRooms row in rooms)
        {
            ++i;
            criteria.RoomsInfo.Add(new SearchCriteriaRooms
            {
                RoomNr = i,
                Adult = row.Adult,
                Child = row.Child,
                Chd1Age = row.Chd1Age >= 0 ? row.Chd1Age : null,
                Chd2Age = row.Chd2Age >= 0 ? row.Chd2Age : null,
                Chd3Age = row.Chd3Age >= 0 ? row.Chd3Age : null,
                Chd4Age = row.Chd4Age >= 0 ? row.Chd4Age : null
            });
        }
        criteria.Board = Conversion.getStrOrNull(Board);
        criteria.Room = Conversion.getStrOrNull(Room);
        criteria.DepCity = Conversion.getInt32OrNull(string.Empty);
        criteria.ArrCity = Conversion.getInt32OrNull(ArrCity);
        criteria.Resort = Conversion.getStrOrNull(Resort);
        criteria.Category = Conversion.getStrOrNull(Category);
        criteria.Hotel = Conversion.getStrOrNull(Hotel);
        criteria.Package = Conversion.getStrOrNull(HolPack);
        criteria.FromRecord = 1;
        criteria.ToRecord = 25;
        criteria.CurControl = Equals(CurControl, "Y");
        criteria.CurrentCur = CurrentCur;
        criteria.SType = SearchType.OnlyHotelSearch;
        criteria.ShowStopSaleHotels = Equals(ShowStopSaleHotels, "Y");
        criteria.HolPackCat = Conversion.getInt16OrNull(holPackCat);
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        criteria.UseGroup = groupSeacrh != null ? (bool)groupSeacrh : false;
        HttpContext.Current.Session["Criteria"] = criteria;
        return "OK";
    }

    [WebMethod(EnableSession = true)]
    public static string getTransportDays(string ArrCity, string HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        Int16? holPackCat = Conversion.getInt16OrNull(HolPackCat);
        if (!holPackCat.HasValue || holPackCat.Value < 1) return string.Empty;
        int? arrCity = Conversion.getInt32OrNull(ArrCity);
        string errorMsg = string.Empty;
        if (holPackCat.Value == 1)
        {
            List<TransportDays> transportDays = new Transports().getTransportDays(UserData.Market, UserData.Operator, arrCity, ref errorMsg);
            var flights = from q in transportDays
                          where !q.SerArea.HasValue || (q.SerArea.HasValue && q.SerArea.Value != 1)
                          group q by new { q.TransDate } into k
                          select new { k.Key.TransDate };
            string retVal = string.Empty;
            foreach (var row in flights)
            {
                var query1 = from q in transportDays
                             where q.TransDate == row.TransDate && (!q.SerArea.HasValue || (q.SerArea.HasValue && q.SerArea.Value != 1))
                             select q;
                string text = string.Empty;
                foreach (var row2 in query1)
                {
                    text += string.Format("{0} -> {1} \\n",
                                            row2.RouteFromName,
                                            row2.RouteToName);
                }
                if (retVal.Length > 0) retVal += ",";
                retVal += "{" + string.Format("\"Year\":\"{0}\",\"Month\":\"{1}\",\"Day\":\"{2}\",\"Text\":\"{3}\"", row.TransDate.Value.Year, row.TransDate.Value.Month, row.TransDate.Value.Day, text) + "}";
            }
            return retVal.Length > 0 ? "[" + retVal + "]" : string.Empty;
        }
        else
        {
            List<CalendarDays> calendarDays = new Search().getPLCheckin(UserData, holPackCat, arrCity, ref errorMsg);
            string retVal = string.Empty;
            foreach (CalendarDays row in calendarDays)
            {
                if (retVal.Length > 0) retVal += ",";
                retVal += "{" + string.Format("\"Year\":\"{0}\",\"Month\":\"{1}\",\"Day\":\"{2}\",\"Text\":\"{3}\"", row.Year.Value, row.Month.Value, row.Day, row.Text) + "}";
            }
            return retVal.Length > 0 ? "[" + retVal + "]" : string.Empty;
        }
    }

}