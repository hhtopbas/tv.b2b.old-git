﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ACEExport.aspx.cs" Inherits="ACEExport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <base href="<%= baseUrlStr%>" />
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    <%= GetGlobalResourceObject("PageTitle","ACEExport") %></title>

  <script src="Scripts/jquery.min.js" type="text/javascript"></script>

  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

  <script src="Scripts/jquery.json.js" type="text/javascript"></script>

  <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

  <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

  <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

  <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <style type="text/css">
    html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
    html { overflow: auto; }
    ol, ul { list-style: none; }
    blockquote, q { quotes: none; }
    :focus, a:focus, a:active { outline: 0; }
    input { outline: 0; }
    ins { text-decoration: none; }
    del { text-decoration: line-through; }
    table { border-collapse: collapse; border-spacing: 0; }
    body { line-height: 1; font-family: Arial; font-size: 8pt; width: 762px; }

    @media print {
      #noPrintDiv { display: none; visibility: hidden; }
    }
  </style>

  <script language="javascript" type="text/javascript">


    if (typeof window.event != 'undefined')
      document.onkeydown = function () {
        if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
          return (event.keyCode != 8);
      }
    else
      document.onkeypress = function (e) {
        if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
          return (e.keyCode != 8);
      }

    $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function getFormData() {
          $.ajax({
            type: "POST",
            url: "ACEExport.aspx/getFormData",
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
              if (msg.hasOwnProperty('d') && msg.d != '') {
                $("#aceExportDiv").html('');
                $("#aceExportDiv").html(msg.d);
              }
            },
            error: function (xhr, msg, e) {
              if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                alert(xhr.responseText);
            },
            statusCode: {
              408: function () {
                self.parent.logout();
              }
            }
          });
        }

        $(document).ready(
            function () {
              getFormData();
              self.parent.viewReportRemoveButton('<%= GetGlobalResourceObject("LibraryResource", "btnPrint") %>');
                self.parent.viewReportRemoveButton('<%= GetGlobalResourceObject("LibraryResource", "btnPDF") %>');
            });
  </script>

</head>
<body>
  <form id="AceExportForm" runat="server">
    <div id="aceExportDiv">
    </div>
  </form>
</body>
</html>
