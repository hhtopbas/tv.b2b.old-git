﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="onlyHotelHolPackCat.aspx.cs"
    Inherits="TvSearch.onlyHotelHolPackCat" EnableEventValidation="false" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<%@ Register Src="Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%=GetGlobalResourceObject("PageTitle", "onlyHotelHolPackCat")%></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="Scripts/jquery.dialogextend.js" type="text/javascript"></script>

    <script src="Scripts/jquery.printPage.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="CSS/PriceSearch.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function logout() {
            $('<div>' + '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>' + '</div>').dialog({
                autoOpen: true,
                position: {
                    my: 'center',
                    at: 'center'
                },
                modal: true,
                resizable: true,
                autoResize: true,
                bigframe: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                        $(this).dialog("close");
                        $(this).dialog("destroy");
                        window.location = 'Default.aspx';
                    }
                }
            });
            window.setTimeout(function () { window.location = "<%=VirtualPathUtility.ToAbsolute("~/Default.aspx")%>"; }, 10000);
        }

        var myWidth = 0, myHeight = 0;

        function reSizeFilterFrame(h) {
            $('#filterFrame').height((h + 60) + 'px');
        }

        function reSizeResultFrame(h) {
            var height = parseInt(h);
            if (height > 0) {
                $('#resultFrame').height(height + 'px');
            } else {
                $('#resultFrame').height(600 + 'px');
            }
        }

        function makeReservation(_url) {
            window.scrollTo(0, 0);
            $('html').css('overflow', 'hidden');
            $("#MakeReservation").attr("src", _url);
            $("#dialog").dialog("destroy");
            $("#dialog-MakeReservation").dialog(
                {
                    autoOpen: true,
                    modal: true,
                    minWidth: 1000,
                    minHeight: 700,
                    resizable: true,
                    autoResize: true,
                    bigframe: true,
                    close: function (event, ui) { $('html').css('overflow', 'auto'); }
                })
            .dialogExtend({
                "maximize": true,
                "icons": {
                    "maximize": "ui-icon-circle-plus",
                    "restore": "ui-icon-pause"
                }
            });
            $("#dialog-MakeReservation").dialogExtend("maximize");
        }

        function showDialog(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    autoOpen: true,
                    position: {
                        my: 'center',
                        at: 'center'
                    },
                    modal: true,
                    resizable: true,
                    autoResize: false,
                    bigframe: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                        $(this).dialog('close');
                    }
                    }
                });
            });
    }

    var NS = document.all;

    function maximize() {
        var myWidth = screen.availWidth;
        var myHeight = screen.availHeight;
        if (location.href.indexOf('pic') == -1) {
            if (window.opera) { } else {
                top.window.moveTo(0, 0);
                if (document.all) { top.window.resizeTo(myWidth, myHeight); }
                else
                    if (document.layers || document.getElementById) {
                        if ((top.window.outerHeight < myHeight) || (top.window.outerWidth < myWidth)) {
                            top.window.outerHeight = myWidth + 'px';
                            top.window.outerWidth = myHeight + 'px';
                        }
                    }
            }
        }
    }

    function searchPrice() {
        $("#resultFrame").attr("src", "PackageSearchResult.aspx");
    }

    $(document).ready(function () {
        $.query = $.query.load(location.href);
        var HolPackCat = $.query.get('HolPackCat');
        var PackType = $.query.get('PackType');
        if (HolPackCat == undefined && HolPackCat.length == 0) HolPackCat == '';
        if (PackType == undefined && PackType.length == 0) PackType == '';
        var queryString = '';
        if (HolPackCat != '') {
            if (queryString.length > 0) queryString += '&';
            queryString += 'HolPackCat=' + HolPackCat;
        }
        if (PackType != '') {
            if (queryString.length > 0) queryString += '&';
            queryString += 'PackType=' + PackType;
        }
        if (queryString.length > 0) {
            queryString = '?' + queryString;
        }
        $("#resultFrame").attr("src", "");
        $("#filterFrame").attr("src", "");
        $("#filterFrame").attr("src", "onlyHotelHolPackCatFilter.aspx" + queryString);
        maximize();
    });

    function printOffer(url, width, height) {
        if ($.browser.msie) {
            var windowSizeArray = ["width=" + width + ",height=" + height + ",scrollbars=yes"];
            var windowName = "popUp";
            var windowSize = windowSizeArray[0];
            window.open(url, windowName, windowSize);
        }
        else {
            $("#viewReport").width(width + 32);
            $("#viewReport").height(height + 32);
            $("#viewReport").contents().find(".mainPage").printPage($("#viewReport").contents().find('style'), width);
        }
    }

    function createOffer() {
        window.scrollTo(0, 0);
        var htmlUrl = 'CreateOffer.aspx';
        var DocName = 'Offer'
        $("#viewReport").removeAttr("src");
        $("#viewReport").attr("src", htmlUrl);
        $("#dialog-viewReport").dialog(
        {
            autoOpen: true,
            modal: true,
            buttons: {
                '<%= GetGlobalResourceObject("LibraryResource", "btnPrint") %>': function () {
                    var width = $("#viewReport").contents().find(".mainPage").width();
                    var height = $("#viewReport").contents().find(".mainPage").height();
                    var param1 = $("#viewReport").contents().find("#param1").val();
                    printOffer(htmlUrl + "?docName=" + DocName + "&print=1" + (param1 != undefined && param1 != '' ? "&param1=" + param1 : ""), width, height);
                },
                '<%= GetGlobalResourceObject("LibraryResource", "btnPDF") %>': function () {
                    //viewPDFReport
                    var width = $("#viewReport").contents().find(".mainPage").width();
                    var _html = $("#viewReport").contents().find("html").html();
                    var noPrint = $("#viewReport").contents().find("#noPrintDiv").html();
                    var _html = _html.replace(noPrint, '&nbsp;').replace(/"/g, '|');
                    $.ajax({
                        type: "POST",
                        url: "PackageSearch.aspx/viewPDFReport",
                        data: '{"reportType":"' + DocName + '","_html":"' + _html + '","pageWidth":"' + width + '","urlBase":"' + htmlUrl + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (msg.d != null) {
                                window.open(msg.d);
                            }
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                showDialog(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                    return false;
                },
                '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>': function () {
                    $(this).dialog('close');
                    return true;
                }
            }
        })
            .dialogExtend({
                "maximize": true,
                "icons": {
                    "maximize": "ui-icon-circle-plus",
                    "restore": "ui-icon-pause"
                }
            });
        $("#dialog-viewReport").dialogExtend("maximize");
    }

    function createHotelOffer(hotel, checkIn, night) {
        window.scrollTo(0, 0);
        var htmlUrl = '/ClientOffers/CreateClientHotelOffer.aspx?hotel=' + hotel + '&checkin=' + checkIn + '&night=' + night;

        $('html').css('overflow', 'hidden');

        var DocName = 'Offer';
        $("#viewReport").removeAttr("src");
        var url = window.location.href;
        url = url.toString().substring(0, url.toString().indexOf('PackageSearch.aspx'));
        $("#ClientOffer").attr("src", url + htmlUrl);
        $("#dialog-ClientOffer").dialog(
        {
            autoOpen: true,
            modal: true,
            buttons: {
                '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>': function () {
                        $(this).dialog('close');
                        return true;
                    }
                },
                close: function (event, ui) { $('html').css('overflow', 'auto'); }
            }).dialogExtend({
                "maximize": true,
                "icons": {
                    "maximize": "ui-icon-circle-plus",
                    "restore": "ui-icon-pause"
                }
            });
            $("#dialog-ClientOffer").dialogExtend("maximize");
        }

        function createOfferV2() {
            window.scrollTo(0, 0);
            var htmlUrl = 'ClientOffers/CreateClientOffer.aspx';
            $('html').css('overflow', 'hidden');
            var DocName = 'ClientOffer';
            $("#ClientOffer").removeAttr("src");
            var url = window.location.href;
            url = url.toString().substring(0, url.toString().indexOf('PackageSearchV2.aspx'));
            $("#ClientOffer").attr("src", url + htmlUrl);
            $("#dialog-ClientOffer").dialog(
            {
                autoOpen: true,
                modal: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>': function () {
                        $(this).dialog('close');
                        return true;
                    }
            },
            close: function (event, ui) { $('html').css('overflow', 'auto'); }
        }).dialogExtend({
            "maximize": true,
            "icons": {
                "maximize": "ui-icon-circle-plus",
                "restore": "ui-icon-pause"
            }
        });
            $("#dialog-ClientOffer").dialogExtend("maximize");
        }

        function bookReservation(bookRoomList) {
            $.ajax({
                type: "POST",
                data: '{"BookList":"' + bookRoomList + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: "OnlyHotelSearch.aspx/getBookReservation",
                success: function (msg) {
                    if (msg.d != null && msg.d.resOK == true) {
                        window.scrollTo(0, 0);
                        $('html').css('overflow', 'hidden');
                        $("#MakeReservation").attr("src", 'MakeReservation' + msg.d.version + '.aspx');
                        $("#dialog").dialog("destroy");
                        $("#dialog-MakeReservation").dialog(
                            {
                                autoOpen: true,
                                modal: true,
                                minWidth: 990,
                                minHeight: 700,
                                resizable: true,
                                autoResize: true,
                                close: function (event, ui) { $('html').css('overflow', 'auto'); }
                            })
                            .dialogExtend({
                                "maximize": true,
                                "icons": {
                                    "maximize": "ui-icon-circle-plus",
                                    "restore": "ui-icon-pause"
                                }
                            });
                        $("#dialog-MakeReservation").dialogExtend("maximize");
                        return true;
                    }
                    else {
                        showDialog(msg.d != null ? msg.d.errMsg : 'unknown error.');
                        return false;
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                    return false;
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });

        }

        function bookRooms(bookRoomList) {
            bookReservation(bookRoomList);
        }

        function cancelMakeRes() {
            $("#MakeReservation").attr("src", "");
            $("#dialog-MakeReservation").dialog("close");
        }

        function gotoResViewPage(ResNo) {
            $("#MakeReservation").attr("src", "");
            $("#dialog-MakeReservation").dialog("close");
            $('html').css('overflow', 'auto');
            window.location = 'ResView.aspx?ResNo=' + ResNo;
        }

        function showBrochure(holPack, CheckIn, CheckOut, Market) {            
            var obj = new Object();
            obj.holPack = holPack;
            obj.CheckIn = CheckIn;
            obj.CheckOut = CheckOut;
            obj.Market = Market;
            $.ajax({
                type: "POST",
                data: $.json.encode(obj),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: "onlyHotelHolPackCat.aspx/getBrochure",
                success: function (msg) {
                    if (msg.d != "") {
                        window.scrollTo(0, 0);
                        $('html').css('overflow', 'hidden');
                        $("#viewReport").removeAttr("src");
                        var _pdfViewPage = "ViewPDF.aspx"
                        $("#viewReport").attr("src", _pdfViewPage + "?url=" + msg.d);
                        var width = $('body').width() - 50;
                        $("#viewReport").width(width);
                        $("#dialog-viewReport").dialog(
                            {
                                autoOpen: true,
                                modal: true,
                                resizable: true,
                                autoResize: true,
                                bigframe: true,
                                buttons: {
                                    '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>': function () {
                                        $(this).dialog('close');
                                        return true;
                                    }
                                },
                                close: function (event, ui) { $('html').css('overflow', 'auto'); }
                            }).dialogExtend({
                                "maximize": true,
                                "icons": {
                                    "maximize": "ui-icon-circle-plus",
                                    "restore": "ui-icon-pause"
                                }
                            });
                            $("#dialog-viewReport").dialogExtend("maximize");
                            return true;
                        }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                    return false;
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
            }

            function paymentPage(ResNo) {
                $("#MakeReservation").attr("src", "");
                $("#dialog-MakeReservation").dialog("close");
                window.open('Payments/BeginPayment.aspx?ResNo=' + ResNo, '_blank');
            }
    </script>

</head>
<body>
    <form id="PackageSearchForm" runat="server">
        <div class="Page">
            <tv1:Header ID="tvHeader" runat="server" />
            <tv1:MainMenu ID="tvMenu" runat="server" />
            <br />
            <div class="Content">
                <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
                    <tr>
                        <td valign="top" style="width: 255px;">
                            <iframe id="filterFrame" runat="server" width="100%" frameborder="0"></iframe>
                        </td>
                        <td valign="top">
                            <div id="divWorkAreaSearch">
                                <div id="divResult">
                                    <div id="divFilterResult">
                                        <iframe id="resultFrame" runat="server" width="100%" frameborder="0" style="clear: both; min-height: 400px;"></iframe>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="Footer">
                <tv1:Footer ID="tvfooter" runat="server" />
            </div>
        </div>
        <div id="dialog-MakeReservation" title='<%= GetGlobalResourceObject("MakeReservation", "lblMakeReservation") %>'
            style="display: none; text-align: center;">
            <iframe id="MakeReservation" runat="server" height="100%" width="960px" frameborder="0"
                style="clear: both; text-align: left;"></iframe>
        </div>
        <%--Message, Confirm Area--%>
        <div id="dialog-message" title="" style="display: none;">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span><span id="messages">Message</span>
            </p>
        </div>
        <div id="waitMessage" style="display: none;">
            <img alt=" " title="" src="Images/Wait.gif" />
        </div>
        <%--Message, Confirm Area--%>
        <div id="dialog-viewReport" title='' style="display: none; text-align: center;">
            <iframe id="viewReport" runat="server" height="100%" width="700px" frameborder="0"
                style="clear: both; text-align: left;"></iframe>
        </div>

        <%--Offer Area--%>
        <asp:HiddenField ID="clientOfferVersion" runat="server" Value="" />
        <div id="dialog-ClientOffer" title='' class="ui-helper-hidden">
            <iframe id="ClientOffer" class="ui-helper-clearfix" style="width: 100%; height: 100%; text-align: left;" frameborder="0"></iframe>
        </div>
        <%--Offer Area--%>
    </form>
</body>
</html>
