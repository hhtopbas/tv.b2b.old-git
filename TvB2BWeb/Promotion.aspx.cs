﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Web.Services;
using System.Text;
using System.Threading;

public partial class Promotion : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod(EnableSession = true)]
    public static string getPromoList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];        
        List<PromoListRecord> PromoList = ResData.PromoList;
        PromoList = new Promos().getPromoListTemplate(UserData.Market, PromoList, ref errorMsg);
        if (PromoList == null || PromoList.Count < 1) return "";
        StringBuilder sb = new StringBuilder();

        sb.Append("<table>");
        sb.Append("<tr>");
        sb.AppendFormat("<td class=\"hSelect th\"><strong>{0}</strong></td>", 
            HttpContext.GetGlobalResourceObject("Promotion", "lblSelect"));
        sb.AppendFormat("<td class=\"hPromoType th\"><strong>{0}</strong></td>",
            HttpContext.GetGlobalResourceObject("Promotion", "lblPromoType"));
        sb.AppendFormat("<td class=\"hConfReq th\"><strong>{0}</strong></td>", 
            HttpContext.GetGlobalResourceObject("Promotion", "lblConfRq"));
        sb.AppendFormat("<td class=\"hMessage th\"><strong>{0}</strong></td>", 
            HttpContext.GetGlobalResourceObject("Promotion", "lblMessage"));
        sb.AppendFormat("<td class=\"hPassenger th\"><strong>{0}</strong></td>", 
            HttpContext.GetGlobalResourceObject("Promotion", "lblPassenger"));
        sb.AppendFormat("<td class=\"hAmount th\"><strong>{0}</strong></td>", 
            HttpContext.GetGlobalResourceObject("Promotion", "lblAmount"));
        sb.AppendFormat("<td class=\"hCur th\"><strong>{0}</strong></td>", 
            HttpContext.GetGlobalResourceObject("Promotion", "lblCur"));
        sb.Append("</tr>");
        int cnt = 0;
        foreach (PromoListRecord row in PromoList)
        {
            cnt++;
            ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            sb.Append("<tr>");
            sb.AppendFormat("<td class=\"select\"><input id=\"select{0}\" type=\"checkbox\" value=\"{1}\" name=\"promoid\" onclick=\"selectPromotion({2})\" custNo=\"{3}\" /></td>", 
                    cnt, row.RecID, cnt, row.CustNo);
            sb.AppendFormat("<td class=\"promoType\"><span>{0}</span></td>",
                    HttpContext.GetGlobalResourceObject("LibraryResource", "Promotion"+ row.PromoType.ToString()));
            sb.AppendFormat("<td class=\"confReq\">{0}</td>",
                    (row.ConfReq.HasValue ? row.ConfReq.Value : false) ? "<img title=\"\" src=\"Images/checked_16.gif\" />" : "&nbsp;");
            sb.AppendFormat("<td class=\"message\"><span>{0}</span></td>", row.PromoNameL != null ? row.PromoNameL : "&nbsp;");
            if (resCust != null)
                sb.AppendFormat("<td class=\"passenger\"><span>{0}</span></td>", resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name);
            else sb.AppendFormat("<td class=\"passenger\"><span>{0}</span></td>", "&nbsp");
            sb.AppendFormat("<td class=\"amount\">{0}</td>", row.Amount.HasValue ? row.Amount.Value.ToString("#,###.00") : "&nbsp;");
            sb.AppendFormat("<td class=\"cur\">{0}</td>", row.ResCur);
            sb.Append("</tr>");
        }
        sb.Append("</table>");
        string listPromoJson = Newtonsoft.Json.JsonConvert.SerializeObject(PromoList).Replace('"', '!');
        sb.AppendFormat("<input id=\"hfPromoList\" type=\"hidden\" value=\"{0}\" />", listPromoJson);
        return sb.ToString();
    }
}
