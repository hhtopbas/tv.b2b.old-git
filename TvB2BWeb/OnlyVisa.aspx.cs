﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using TvBo;
using TvTools;


public partial class OnlyVisa : BasePage
{
    public static string twoLetterISOLanguageName = "en";

    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<string> lang = new List<string>() { "ar-DZ", "ar-DZ", "ar", "en-AU", "en-GB", "en-NZ", "fr-CH", "fr", "pt-BR", "pt", "sr-SR", "sr-CS", "sr", "zh-CN", "zh-HK", "zh-TW" };
        if (lang.Contains(UserData.Ci.Name))
            twoLetterISOLanguageName = UserData.Ci.Name;
        else
            twoLetterISOLanguageName = UserData.Ci.TwoLetterISOLanguageName != "en" ? UserData.Ci.TwoLetterISOLanguageName : "";
        string _tmpPath = WebRoot.BasePageRoot + "Data/";
        tmplPath.Value = _tmpPath;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        List<Location> visaCountry = new Visas().getVisaLocations(UserData.Market, UserData.Market, ref errorMsg);

        return new
        {
            Adult = 2,
            Child = 0,
            Infant = 0,
            CheckIn = DateTimeExtensions.ToUnixTime(DateTime.Today) * 1000,
            CheckOut = DateTimeExtensions.ToUnixTime(DateTime.Today.AddMonths(1)) * 1000,
            VisaCountry = (from q in visaCountry
                           group q by new { RecID = q.RecID, Name = q.NameL } into k
                           select new { RecID = k.Key.RecID, Name = k.Key.Name })
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getServiceList(int? Country)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        List<VisaRecord> visaList = new Visas().getVisaCountry(UserData.Market, UserData.Market, Country, DateTime.Today, null, ref errorMsg);

        return new
        {
            ServiceList = from q in visaList                          
                          where !(q.RestSingleSale.HasValue && q.RestSingleSale.Value)
                          select new { Code = q.Code, Name = q.LocalName }
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object preReservation(int? Country, string Service, double? CheckIn, double? CheckOut, Int16? Adult, Int16? Child, Int16? Infant)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        try
        {
            ResDataRecord ResData = new ResDataRecord();
            ResData = new OnlyVisas().getOnlyVisaResData(UserData,
                                                         Country,
                                                         Service,
                                                         CheckIn.HasValue ? DateTimeExtensions.FromUnixTime(CheckIn.Value / 1000) : DateTime.Today,
                                                         CheckOut.HasValue ? DateTimeExtensions.FromUnixTime(CheckOut.Value / 1000) : DateTime.Today.AddDays(1),
                                                         Adult,
                                                         Child,
                                                         Infant,
                                                         ref errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
            {
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka))
                {
                    ResMainRecord resMain = ResData.ResMain;
                    resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
                }

                HttpContext.Current.Session["ResData"] = ResData;
                return new
                {
                    ResOK = true
                };
            }
            else
            {
                return new
                {
                    ResOK = false,
                    ErrorMsg = errorMsg
                };
            }
        }
        catch
        {
            return new
            {
                ResOK = false,
                ErrorMsg = errorMsg
            };
        }

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string setCustomers(List<resCustjSonData> Customers)
    {
        if (HttpContext.Current.Session["UserData"] == null || /*string.IsNullOrEmpty(data)*/ Customers == null)
        { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ResData"] == null)
        {
            return "NO";
        }

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();

        ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);

        try
        {
            resCustjSonData cust = new resCustjSonData();
            //data = data.Replace('|', '"').Replace('<', '{').Replace('>', '}');
            //string[] _data = data.Split('@');
            for (int i = 0; i < Customers.Count; i++)
            {
                cust = Customers[i];//Newtonsoft.Json.JsonConvert.DeserializeObject<resCustjSonData>(_data[i]);
                ResCustRecord _cust = ResData.ResCust.Find(f => f.SeqNo == cust.SeqNo);
                TitleRecord _title = ResData.Title.Find(f => f.TitleNo == cust.Title);
                _cust.Title = cust.Title;
                _cust.TitleStr = _title != null ? _title.Code : "";
                _cust.Surname = cust.Surname;
                _cust.Name = cust.Name;
                _cust.Birtday = Conversion.getDateTimeOrNull(cust.Birtday);
                _cust.Age = TvBo.Common.getAge(_cust.Birtday, UserData.TvParams.TvParamReser.AgeCalcType > 2 ? ResData.ResMain.EndDate : ResData.ResMain.BegDate);
                _cust.IDNo = cust.IDNo;
                _cust.PassSerie = cust.PassSerie;
                _cust.PassNo = cust.PassNo;
                _cust.PassExpDate = Conversion.getDateTimeOrNull(cust.PassExpDate);
                _cust.PassIssueDate = Conversion.getDateTimeOrNull(cust.PassIssueDate);
                _cust.PassGiven = cust.PassGiven;
                _cust.Phone = cust.Phone;
                _cust.Nation = cust.Nation;
                _cust.Nationality = cust.Nationality;
                _cust.HasPassport = cust.Passport;
                _cust.Leader = cust.Leader.HasValue ? (cust.Leader.Value ? "Y" : "N") : "N";
            }

            HttpContext.Current.Session["ResData"] = ResData;
            return "OK";
        }
        catch
        {
            return "NO";
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getCustomers()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        int? defaultNation = UserData.Country != null ? UserData.Country.Value : 1;
        Nationality nationality = new Locations().getDefaultNationality(UserData, defaultNation, ref errorMsg);

        List<Nationality> nationalityList = new Locations().getNationalityList(ref errorMsg);

        int cnt = 0;
        var resCust = from c in ResData.ResCust
                      select new
                      {
                          oddEven = cnt++ % 2 == 0,
                          SeqNo = c.SeqNo,
                          TitleList = from t in ResData.Title
                                      where t.Enable
                                      select new
                                      {
                                          t.TitleNo,
                                          t.Code,
                                          thisTitle = c.Title == t.TitleNo
                                      },
                          Surname = c.Surname,
                          Name = c.Name,
                          BirthDate = c.Birtday.HasValue ? c.Birtday.Value.ToShortDateString() : string.Empty,
                          Age = c.Age,
                          NationalityList = from n in nationalityList
                                            select new
                                            {
                                                n.Code3,
                                                n.Name,
                                                thisNationality = c.Nationality == n.Code3
                                            },
                          HasPassport = c.HasPassport.HasValue ? c.HasPassport.Value : false,
                          Leader = string.Equals(c.Leader, "Y"),
                          Phone = c.Phone
                      };

        string dateFormat = new TvBo.Common().getDateFormat(UserData.Ci).Replace('/', UserData.Ci.DateTimeFormat.DateSeparator[0]);
        string dateMaskA = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ');
        string[] dateMaskB = dateMaskA.Split(UserData.Ci.DateTimeFormat.DateSeparator[0]);
        string _dateMask = string.Empty;
        string _dateFormat = string.Empty;
        foreach (string row in dateMaskB)
            if (!string.IsNullOrEmpty(row.ToString()))
                switch (row[0].ToString().ToLower())
                {
                    case "d":
                        _dateMask += UserData.Ci.DateTimeFormat.DateSeparator[0] + "99";
                        _dateFormat += UserData.Ci.DateTimeFormat.DateSeparator[0] + "dd";
                        break;
                    case "m":
                        _dateMask += UserData.Ci.DateTimeFormat.DateSeparator[0] + "99";
                        _dateFormat += UserData.Ci.DateTimeFormat.DateSeparator[0] + "MM";
                        break;
                    case "y":
                        _dateMask += UserData.Ci.DateTimeFormat.DateSeparator[0] + "9999";
                        _dateFormat += UserData.Ci.DateTimeFormat.DateSeparator[0] + "yyyy";
                        break;
                    default:
                        break;
                }
        _dateMask = _dateMask.Remove(0, 1);
        var returnVal = new
        {
            lblTitle = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustTitle").ToString(),
            lblSurname = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustSurname").ToString(),
            lblName = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustName").ToString(),
            lblBirthDate = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustBirthDate").ToString(),
            lblAge = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustAge").ToString(),
            lblNationality = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustCitizenship").ToString(),
            lblPassport = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPass").ToString(),
            lblLeader = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustLeader").ToString(),
            lblPhone = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPhone").ToString(),
            CheckIn = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToString(dateFormat) : "",
            formatDate = dateFormat,
            dateMask = _dateMask,
            Customers = resCust
        };
        return returnVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getVisaService()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        useLocalName = false;

        foreach (TvBo.ResServiceRecord row in ResData.ResService)
        {
            string ServiceDesc = string.Empty;
            #region Description
            switch (row.ServiceType)
            {
                case "HOTEL":
                    ServiceDesc = (useLocalName ? row.RoomNameL : row.RoomName) + "," + row.Board + " , " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "FLIGHT":
                    FlightDayRecord flg = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                    ServiceDesc += flg != null && flg.TDepDate.HasValue ? flg.TDepDate.Value.ToShortDateString() : row.BegDate.Value.ToShortDateString();
                    break;
                case "TRANSPORT":
                    ServiceDesc += row.BegDate.Value.ToShortDateString();
                    break;
                case "TRANSFER":
                    TransferRecord trf = new Transfers().getTransfer(UserData.Market, row.Service, ref errorMsg);
                    if (trf != null && string.Equals(trf.Direction, "R"))
                        ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    else
                        ServiceDesc += row.BegDate.Value.ToShortDateString();
                    break;
                case "RENTING":
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "EXCURSION":
                    ServiceDesc += row.BegDate.Value.ToShortDateString();
                    break;
                case "INSURANCE":
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "VISA":
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "HANDFEE":
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                default:
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
            }
            #endregion
            row.Description = (useLocalName ? row.ServiceNameL : row.ServiceName) + (string.Equals(row.ServiceType, "HOTEL") ? (useLocalName ? row.RoomNameL : row.RoomName) + "," + row.Board : "");
        }

        object _showIncSrvPrice = new TvBo.Common().getFormConfigValue("MakeRes", "ShowIncSrvPrice");
        bool? ShowIncSrvPrice = Conversion.getBoolOrNull(_showIncSrvPrice);

        var resService = from s in ResData.ResService
                         where !string.Equals(s.ServiceType, "HOTEL")                           
                         select new
                         {
                             ServiceType = s.ServiceTypeName,
                             ServiceDesc = s.ServiceName + s.Description,
                             Compulsory = s.Compulsory.HasValue ? s.Compulsory.Value : false,
                             IncPack = string.Equals(s.IncPack, "Y"),
                             Unit = s.Unit,
                             Price = string.Equals(s.IncPack, "Y") || s.Compulsory == true ? HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString() : (s.SalePrice.HasValue ? s.SalePrice.Value.ToString("#,###.00") + " " + s.SaleCur : "")
                         };

        return new
        {
            lblServiceType = HttpContext.GetGlobalResourceObject("MakeReservation", "resSerServiceType").ToString(),
            lblServiceDesc = HttpContext.GetGlobalResourceObject("MakeReservation", "resSerServiceDesc").ToString(),
            lblCompulsory = HttpContext.GetGlobalResourceObject("MakeReservation", "resSerComp").ToString(),
            lblIncPack = HttpContext.GetGlobalResourceObject("MakeReservation", "resSerIncPack").ToString(),
            lblUnit = HttpContext.GetGlobalResourceObject("MakeReservation", "resSerUnit").ToString(),
            lblPrice = HttpContext.GetGlobalResourceObject("MakeReservation", "resSerPrice").ToString(),
            OtherService = resService
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object saveReservation(List<resCustjSonData> Customers, bool goAhead)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        setCustomers(Customers);

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        ResDataRecord ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);

        List<ResServiceRecord> resServiceList = new List<ResServiceRecord>();
        List<ResConRecord> resConList = new List<ResConRecord>();
        List<ResServiceExtRecord> resServiceExtList = new List<ResServiceExtRecord>();
        List<ResConExtRecord> resConExtList = new List<ResConExtRecord>();

        foreach (ResServiceRecord rowRs in ResData.ResService)
        {
            if ((!string.IsNullOrEmpty(rowRs.Accom) && !string.IsNullOrEmpty(rowRs.Board) && !string.IsNullOrEmpty(rowRs.Room) && string.Equals(rowRs.ServiceType, "HOTEL")) || !string.Equals(rowRs.ServiceType, "HOTEL"))
            {
                resServiceList.Add(rowRs);
                foreach (ResConRecord rowRc in ResData.ResCon.Where(w => w.ServiceID == rowRs.RecID))
                    resConList.Add(rowRc);

                foreach (ResServiceExtRecord rowRsE in ResData.ResServiceExt.Where(w => w.ServiceID == rowRs.RecID))
                {
                    resServiceExtList.Add(rowRsE);
                    foreach (ResConExtRecord rowRcE in ResData.ResConExt.Where(w => w.ServiceID == rowRsE.RecID))
                        resConExtList.Add(rowRcE);
                }
            }
        }

        ResData.ResService = resServiceList;
        ResData.ResCon = resConList;
        ResData.ResServiceExt = resServiceExtList;
        ResData.ResConExt = resConExtList;

        List<ReservastionSaveErrorRecord> returnData = new List<ReservastionSaveErrorRecord>();

        ResMainRecord resMain = ResData.ResMain;
        resMain.OptDate = null;

        bool saveAsDraft = true;
        bool? setOptionTime = null;

        #region Credit control
        Int16 CreditCont = new TvBo.Reservation().CreditControl(UserData.AgencyID, UserData.Market, ref errorMsg);

        if (CreditCont == 2)
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = HttpContext.GetGlobalResourceObject("LibraryResource", "NoCreditLimit").ToString()
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }
        #endregion

        #region No servis error
        if (ResData.ResService.Count < 1)
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = HttpContext.GetGlobalResourceObject("LibraryResource", "NoResServiceMsg").ToString()
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }
        #endregion

        #region Check flight time
        if (!(new Flights().CheckFlightTime(UserData, ResData.ResService, ref errorMsg)))
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = HttpContext.GetGlobalResourceObject("LibraryResource", "CheckInTimeOver").ToString()
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }
        #endregion

        List<CustControlErrorRecord> custControl = new Reservation().CustControl(UserData, ResData, ref errorMsg);
        if ((custControl == null || custControl.Count == 1) && (custControl.Where(w => w.ErrorCode == 0).Count() == 1 || custControl.Where(w => w.ErrorCode == 30).Count() == 1))
        {
            resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
            if (setOptionTime.HasValue && setOptionTime.Value == false)
                resMain.OptDate = null;

            returnData = new Reservation().SaveReservation(UserData, ref ResData, saveAsDraft);

            if (returnData == null || returnData.Count < 1)
            {
                returnData.Add(new ReservastionSaveErrorRecord
                {
                    ControlOK = false,
                    Message = HttpContext.GetGlobalResourceObject("LibraryResource", "ResNotSaved").ToString(),
                    OtherReturnValue = custControl.FirstOrDefault().ErrorCode.ToString()
                });
                return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
            }
            else
            {
                bool? saveResAfterResView = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "SaveResAfterResView"));
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                {
                    if (UserData.AgencyRec != null && UserData.AgencyRec.AceExport)
                    {
                        string AceFileName = string.Empty;
                        if (!(new Aces().SendAceTo(UserData, ResData, AppDomain.CurrentDomain.BaseDirectory + "ACE//", ref AceFileName, ref errorMsg)))
                        {
                        }
                    }

                    returnData.Clear();
                    ReservastionSaveErrorRecord retVal = new ReservastionSaveErrorRecord();
                    if (custControl.Where(w => w.ErrorCode == 30).Count() == 1)
                    {
                        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
                        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
                        string msg = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString(), ResData.ResMain.ResNo);
                        msg += "<br />";
                        msg += HttpContext.GetGlobalResourceObject("MakeReservation", "parentAuthorizationMsg").ToString();
                        msg += "<br />";
                        msg += string.Format("<a href=\"#\" onClick=\"window.open('{0}','mywindow')\" style=\"font-style:italic; font-weight:bold;\">{1}</a>",
                                             WebRoot.BasePageRoot + DocumentFolder + "/" + UserData.Market + "/authorization.pdf",
                                             HttpContext.GetGlobalResourceObject("MakeReservation", "parentAuthorizationLnk"));
                        retVal.ControlOK = true;
                        retVal.Message = msg;
                        retVal.OtherReturnValue = custControl != null && custControl.Count > 0 ? custControl.FirstOrDefault().ErrorCode.ToString() : string.Empty;
                        retVal.GotoReservation = !saveResAfterResView.HasValue || (saveResAfterResView.HasValue && saveResAfterResView.Value);
                        returnData.Add(retVal);
                    }
                    else
                        if (custControl.Where(w => w.ErrorCode == 32).Count() > 1 && goAhead != true)
                        {
                            retVal.ControlOK = false;
                            retVal.Message = custControl.Where(w => w.ErrorCode == 32).FirstOrDefault().ErrorMessage;
                            retVal.OtherReturnValue = custControl != null && custControl.Count > 0 ? custControl.FirstOrDefault().ErrorCode.ToString() : string.Empty;
                            retVal.GotoReservation = false;
                            returnData.Add(retVal);
                        }
                        else
                        {
                            List<resPayPlanRecord> resPayPlan = new ReservationMonitor().getPaymentPlan(ResData.ResMain.ResNo, ref errorMsg);
                            string paymentPlanStr = new TvBo.ReservationMonitor().getPaymentPlanHTML(ResData.ResMain.ResNo, UserData.Ci.LCID);
                            retVal.ControlOK = true;
                            if (resPayPlan != null && resPayPlan.Count > 0)
                            {
                                resPayPlanRecord firstPayment = resPayPlan.OrderBy(o => o.DueDate).FirstOrDefault();
                                if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                                    retVal.Message = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString() + "<br />" + paymentPlanStr,
                                                                            ResData.ResMain.ResNo);
                                else
                                {
                                    string paidMessage = string.Empty;
                                    if (firstPayment.Amount.HasValue)
                                        paidMessage = string.Format(HttpContext.GetGlobalResourceObject("MakeReservation", "lblAmount").ToString(),
                                                                               firstPayment.Amount.Value.ToString("#,###.00") + " " + firstPayment.Cur,
                                                                               firstPayment.DueDate.ToShortDateString());
                                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Orex))
                                        paidMessage = string.Empty;
                                    retVal.Message = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString() + "<br />{1}",
                                                                            ResData.ResMain.ResNo,
                                                                            paidMessage);
                                }
                            }
                            else
                                retVal.Message = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString(),
                                    ResData.ResMain.ResNo);

                            retVal.OtherReturnValue = custControl != null && custControl.Count > 0 ? custControl.FirstOrDefault().ErrorCode.ToString() : string.Empty;
                            //retVal.SendEmailB2C = getEmailUrl(UserData, ResData);
                            retVal.GotoPaymentPage = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur);
                            retVal.GotoReservation = !saveResAfterResView.HasValue || (saveResAfterResView.HasValue && saveResAfterResView.Value);
                            retVal.ResNo = ResData.ResMain.ResNo;
                            returnData.Add(retVal);
                        }

                    return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
                }
                else
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
                }
            }
        }
        else
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = new Reservation().getCustControlErrorMessage(custControl),
                OtherReturnValue = custControl != null && custControl.Count > 0 ? custControl.FirstOrDefault().ErrorCode.ToString() : string.Empty
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }
    }
}
