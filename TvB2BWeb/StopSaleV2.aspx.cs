﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Text;
using System.IO;
using TvTools;
using System.Web.Script.Services;
using System.Collections;

public partial class StopSaleV2 : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;        
        if (!IsPostBack)
            Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string getDateRegion()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string dateFormat = UserData.Ci.TwoLetterISOLanguageName;
        return dateFormat;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string GetFirstDataFlightSeatWithOpt()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        HttpContext.Current.Session["StopSaleFilter"] = null;
        StopSaleDefaultRecord data = new StopSaleDefaultRecord();
        string errorMsg = string.Empty;
        data.OptionType = 1;
        data.DepCity = new TvBo.StopSales().GetDepCity(UserData, ref errorMsg);
        data.FlgCls = new TvBo.StopSales().GetFlightClass(UserData, ref errorMsg); ;
        data.BegDate = null;
        data.EndDate = null;
        string datePatern = new TvBo.Common().getDateFormat(UserData.Ci);
        datePatern = datePatern.Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], '-');
        data.DateFormat = datePatern;
        return Newtonsoft.Json.JsonConvert.SerializeObject(data);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string GetFirstDataStandart()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        HttpContext.Current.Session["StopSaleFilter"] = null;
        StopSaleDefaultRecord data = new StopSaleDefaultRecord();
        string errorMsg = string.Empty;
        data.OptionType = 0;
        data.HotelList = new TvBo.StopSales().getStopSaleListHotels(UserData, ref errorMsg);
        data.BegDate = null;
        data.EndDate = null;
        string datePatern = new TvBo.Common().getDateFormat(UserData.Ci);
        datePatern = datePatern.Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], '-');
        data.DateFormat = datePatern;
        return Newtonsoft.Json.JsonConvert.SerializeObject(data);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string GetArrCityHotel(string begDate, string endDate, int? arrCityID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string datePatern = new TvBo.Common().getDateFormat(UserData.Ci);
        datePatern = datePatern.Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], '-');
        DateTime? BegDate = string.IsNullOrEmpty(begDate) ? null : Conversion.getDateTimeOrNull(begDate.Replace('-', UserData.Ci.DateTimeFormat.DateSeparator[0]));
        DateTime? EndDate = string.IsNullOrEmpty(endDate) ? null : Conversion.getDateTimeOrNull(endDate.Replace('-', UserData.Ci.DateTimeFormat.DateSeparator[0]));
        string errorMsg = string.Empty;
        List<CodeName> record = new TvBo.StopSales().getStopSaleArrCityHotels(UserData, BegDate, EndDate, arrCityID, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(record);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string GetArrCityByDepCity(int? depCityID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        List<StopSaleCityRecord> record = new TvBo.StopSales().GetArrCityByDep(UserData, depCityID, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(record);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string GetResult(string begDate, string endDate, string Hotel)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (begDate == "")
            begDate = DateTime.Today.Date.ToShortDateString();
        DateTime begin = DateTime.Parse(begDate, UserData.Ci.DateTimeFormat);
        if (endDate == "")
            endDate = begin.AddMonths(1).Date.ToShortDateString();
        DateTime end = DateTime.Parse(endDate, UserData.Ci.DateTimeFormat);
        string error = "";
        string datePatern = new TvBo.Common().getDateFormat(UserData.Ci);
        List<StopSaleResultRecord> result = new List<StopSaleResultRecord>();
        result = new TvBo.StopSales().getStopSaleStandartResult(UserData, begin, end, Hotel, null, null, ref error);
        CreateAndWriteFile(result);
        return ListToTable(result);
    }

    private static void CreateAndWriteFile(List<StopSaleResultRecord> result)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string yol = AppDomain.CurrentDomain.BaseDirectory + "Cache\\StopSale." + HttpContext.Current.Session.SessionID;
        if (File.Exists(yol))
            File.Delete(yol);
        FileStream f = new FileStream(yol, FileMode.OpenOrCreate, FileAccess.Write);
        StreamWriter writer = new StreamWriter(f);
        writer.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
        writer.Close();
        f.Close();
    }

    private static string ListToTable(List<StopSaleResultRecord> list)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();
        sb.Append("<table id=\"stopSaleTable\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"display\" style=\"margin-left: 0pt; width: 1000px;\">");
        sb.Append("<thead>");
        sb.Append("<tr>");
        sb.AppendFormat("<th class=\"sorting_asc\">{0}</th>" +
        "<th class=\"sorting room\">{1}</th>" +
        "<th class=\"sorting accom\">{2}</th>" +
        "<th class=\"sorting board\">{3}</th>" +
        "<th class=\"sorting begDate\">{4}</th>" +
        "<th class=\"sorting endDate\">{5}</th>" +
        "<th class=\"sorting pList\">{6}</th>" +
        "<th class=\"sorting appType\">{7}</th>" +
        "<th class=\"sorting loc\">{8}</th>" +
        "<th class=\"sorting allot\">{9}</th>",
            HttpContext.GetGlobalResourceObject("StopSale", "Hotel"),
            HttpContext.GetGlobalResourceObject("StopSale", "Room"),
            HttpContext.GetGlobalResourceObject("StopSale", "Accommodation"),
            HttpContext.GetGlobalResourceObject("StopSale", "Board"),
            HttpContext.GetGlobalResourceObject("StopSale", "BeginingDate"),
            HttpContext.GetGlobalResourceObject("StopSale", "EndingDate"),
            HttpContext.GetGlobalResourceObject("StopSale", "PriceList"),
            HttpContext.GetGlobalResourceObject("StopSale", "ApplicationType"),
            HttpContext.GetGlobalResourceObject("StopSale", "Location"),
            HttpContext.GetGlobalResourceObject("StopSale", "AllotmentType"));
        sb.Append("</tr>");
        sb.Append("</thead>");
        int cnt = 0;
        sb.Append("<tbody>");
        foreach (StopSaleResultRecord item in list)
        {
            if (Convert.ToInt32(cnt / 2.0) == (cnt / 2.0))
                sb.Append("<tr class=\"gradeU odd\">");
            else sb.Append("<tr class=\"gradeU even\">");
            sb.AppendFormat("<td NOWRAP>{0}</td>" +
            "<td NOWRAP>{1}</td>" +
            "<td class=\"centeraligned\">{2}</td>" +
            "<td class=\"centeraligned\">{3}</td>" +
            "<td class=\"rightaligned\">{4}</td>" +
            "<td class=\"rightaligned\">{5}</td>" +
            "<td class=\"centeraligned\">{6}</td>" +
            "<td >{7}</td>" +
            "<td class=\"centeraligned\">{8}</td>" +
            "<td class=\"centeraligned\">{9}</td>",
                Equals(item.Hotel, "_ALL") ? HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll") : item.Hotel,
                Equals(item.Room, "_ALL") ? HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll") : item.Room,
                Equals(item.Accom, "_ALL") ? HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll") : item.Accom,
                Equals(item.Board, "_ALL") ? HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll") : item.Board,
                item.BegDate.Value.Date.ToShortDateString(),
                item.EndDate.Value.Date.ToShortDateString(),
                Equals(item.PriceList, "_ALL") ? HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll") : item.PriceList,
                HttpContext.GetGlobalResourceObject("LibraryResource", "AppType_" + item.AppType),
                item.Location.HasValue ? new Locations().getLocationName(UserData, item.Location.Value, ref errorMsg) : HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"),
                item.AllotType.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "AllotType" + item.AllotType.ToString()) : HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            sb.Append("</tr>");
            cnt++;
        }
        sb.Append("</tbody>");
        sb.Append("</table>");
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getFlightSeatWithOptResult(string begDate, string endDate, string Hotel, int? depCity, int? arrCity, string flgClass)
    {
        int width = 0;
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        DateTime? BegDate = Conversion.getDateTimeOrNull(begDate.Replace('-', UserData.Ci.DateTimeFormat.DateSeparator[0]));
        DateTime? EndDate = Conversion.getDateTimeOrNull(endDate.Replace('-', UserData.Ci.DateTimeFormat.DateSeparator[0]));
        List<StopSaleFlightSeatOpt> flights = new StopSales().getStopSaleFlightSeatWithOptResult(UserData, BegDate, EndDate, depCity, arrCity, flgClass, ref errorMsg);
        var Nights = from N in flights
                     group N by new { Night = N.Night } into g
                     orderby g.Key.Night
                     select new { Night = g.Key.Night };
        StringBuilder sb = new StringBuilder();

        sb.Append(" <div style=\"width:100%;float:left;min-height:100px;\" class=\"dataTables_scroll\">");
        sb.Append("<table id=\"stopSaleTable\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" >");
        sb.Append("<thead>");//ui-state-default
        sb.Append(" <tr style=\"height:40px;\" class=\"ui-widget-header\">");
        sb.Append("     <th style=\"width:450px !important;\" class=\"headerTh\">" + HttpContext.GetGlobalResourceObject("StopSale", "SeatsInTheAircraft").ToString() + "</th>");
        width += 450;
        sb.Append("     <th style=\"width:50px !important;\" class=\"headerTh\" >");
        width += 50;
        sb.Append("         " + HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblCategory").ToString());
        sb.Append("     </th>");
        int Days = (EndDate.Value - BegDate.Value).Days;
        for (int i = 0; i < Days; i++)
        {
            sb.Append("     <th style=\"text-align:center;width:30px;\" class=\"headerTh\">");
            sb.Append("        " + BegDate.Value.AddDays(i).ToString("dd MMM"));//<image alt=\"\" src=\"getLabel90.aspx?Label=" + BegDate.AddDays(i).ToString("dd MMM") + "\" />");
            sb.Append("     </th>");
            width += 30;
        }
        sb.Append(" </tr>");
        sb.Append("</thead>");
        sb.Append("<tbody>");

        //Seats in the aircraft

        foreach (var nr in Nights)
        {
            sb.Append(" <tr style=\"height:20px;\">");
            sb.Append("     <td style=\"white-space:nowrap;padding-left:10px;\">" + nr.Night.ToString() + " " + HttpContext.GetGlobalResourceObject("StopSale", "NightDuration").ToString() + "</td>");
            sb.Append("     <td>&nbsp;</td>");
            for (int i = 0; i < Days; i++)
            {
                var Seat = from s in flights
                           where s.Night == nr.Night && s.FlyDate == (BegDate.HasValue ? BegDate.Value.AddDays(i) : DateTime.Today)
                           select new { Seat = s.Seat };
                string SeatStr = (Seat.Count() > 0 ? (Seat.FirstOrDefault().Seat > 10 ? "10+" : (Seat.FirstOrDefault().Seat > 0 ? Seat.FirstOrDefault().Seat.ToString() : "&nbsp;")) : "&nbsp;");
                if (SeatStr == "&nbsp;")
                    sb.Append("     <td class=\"RedCell\" style=\"text-align: center;\">");
                else
                    sb.Append("     <td class=\"GreenCell\" style=\"text-align: center;\">");
                sb.Append("         " + SeatStr);
                sb.Append("     </td>");
            }
            sb.Append(" </tr>");
        }
        sb.Append("</tbody>");
        sb.Append(" </table>");
        sb.Append(" </div>");
        sb.Append("<div id=\"StopSaleListMiddle\" class=\"ui-widget-header\" style=\"float:left;width:100%;height:40px;border:none !important;\"><div id=\"PageNumbersHeader\" style=\"width:1000px;float:left;\">  <div id=\"divPagingListCount\" class=\"headerTh\" style=\"padding:12px 0 0 10px ; float:left;height:28; \"><select id=\"slctPagingListCount\" onchange=\"changeSlctPagingListCount(this.value);\" style=\"width:60px;\"><option value=\"10\" selected=\"selected\">10</option><option value=\"25\">25</option><option value=\"50\" >50</option><option value=\"100\">100</option></select></div><div id=\"PagingListCountLabel\" class=\"headerTh\" style=\"padding:16px 0 0 2px ; float:left;height:28; \"></div></div></div>");

        sb.Append(" <div style=\"width:100%;float:left;min-height:100px;\" class=\"dataTables_scroll\">");

        //stopSaleListTableHeader start
        sb.Append("<table id=\"stopSaleListTable\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"margin:0; padding:0;border:none;\">");
        sb.Append("<thead>");
        sb.Append(" <tr style=\"height:40px;\" class=\"ui-widget-header\">");
        sb.Append("     <th  style=\"width:450px !important;\" class=\"headerTh\">" + HttpContext.GetGlobalResourceObject("StopSale", "Hotel").ToString() + "</th>");
        sb.Append("     <th style=\"width:50px !important;text-align:center;\" class=\"headerTh\">");
        sb.Append("         " + HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblCategory").ToString());
        sb.Append("     </th>");
        for (int i = 0; i < Days; i++)
        {
            sb.Append("     <th style=\"text-align:center;width:30px;\" class=\"headerTh\">");
            sb.Append("        " + BegDate.Value.AddDays(i).ToString("dd MMM"));//<image alt=\"\" src=\"getLabel90.aspx?Label=" + BegDate.AddDays(i).ToString("dd MMM") + "\" />");
            sb.Append("     </th>");
        }
        sb.Append(" </tr>");
        sb.Append("</thead>");
        sb.Append("<tbody>");
        //stopSaleListTableHeader end


        List<StopSaleResultRecord> stopSaleHotels = new StopSales().getStopSaleStandartResult(UserData, BegDate, EndDate, Hotel, depCity, arrCity, ref errorMsg);
        //otel


        var SSHotel = from HSS in stopSaleHotels
                      group HSS by new
                      {
                          Code = HSS.HotelCode,
                          Hotel = HSS.Hotel,
                          Room = HSS.Room,
                          Acoom = HSS.Accom,
                          Board = HSS.Board,
                          Cat = HSS.Category
                      } into g
                      select new
                      {
                          Code = g.Key.Code,
                          Hotel = g.Key.Hotel,
                          Room = g.Key.Room,
                          Accom = g.Key.Acoom,
                          Board = g.Key.Board,
                          Cat = g.Key.Cat
                      };

        foreach (var row in SSHotel)
        {
            sb.Append(" <tr style=\"height:20px;\">");
            sb.Append("     <td nowrap=\"nowrap\" style=\"padding-left:10px;\">" + row.Hotel.ToString() + "<br />");
            sb.Append("     " + (row.Room.ToString() != "_ALL" ? row.Room.ToString() + "," : "")
                           + (row.Accom.ToString() != "_ALL" ? row.Accom.ToString() + "," : "")
                           + (row.Board.ToString() != "_ALL" ? row.Board.ToString() : ""));
            sb.Append("    </td>");
            sb.Append("     <td class=\"cell\" style=\"text-align:center;\">" + row.Cat.ToString() + "</td>");

            var SSale = from H in stopSaleHotels
                        where H.HotelCode == row.Code &&
                                H.Room == row.Room &&
                                H.Accom == row.Accom &&
                                H.Board == row.Board
                        orderby H.BegDate, H.EndDate
                        select new { SBDate = H.BegDate, SEDate = H.EndDate };

            for (int i = 0; i < Days; i++)
            {
                string StopSaleStr = "&nbsp;";
                string stopStr = string.Empty;
                foreach (var row1 in SSale)
                {
                    if (BegDate.HasValue && row1.SBDate <= BegDate.Value.AddDays(i) && row1.SEDate >= BegDate.Value.AddDays(i))
                    {
                        stopStr = "S";
                        break;
                    }
                }

                if (stopStr != "")
                {
                    sb.Append("     <td class=\"RedCell\">");
                    sb.Append("         " + stopStr);
                    sb.Append("     </td>");
                }
                else
                {
                    sb.Append("     <td class=\"GreenCell\">");
                    sb.Append("         " + StopSaleStr);
                    sb.Append("     </td>");
                }
            }
            sb.Append(" </tr>");
        }
        sb.Append("</tbody>");
        sb.Append(" </table>");

        sb.Append(" </div>");
        sb.Append("<div id=\"StopSaleListFooter\" class=\"ui-widget-header\" style=\"float:left;width:100%;height:40px;border:none !important;\"><div id=\"PageNumbers\" style=\"width:1000px;float:left;\">  <div id=\"PageNumbersLabel\" class=\"headerTh\" style=\"padding:12px 0 0 10px ; float:left;height:28; \"></div><div id=\"PageNumbersContent\" style=\"padding:12px 0 0 10px ; float:right;height:28; \"><select id=\"slctPageNumbers\" onchange=\"changeSlctPageNumbers(this.value);\" style=\"width:60px;\"></select></div></div></div>");

        width += 10;
        StringBuilder sb2 = new StringBuilder();

        sb2.AppendFormat(" <div style=\"width:" + width.ToString() + "px;\">{0}</div>", sb);
        return sb2.ToString();
    }
}
