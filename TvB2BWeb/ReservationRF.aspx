﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReservationRF.aspx.cs" Inherits="ReservationRF" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="cache-control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  <title>Reservation Request Form</title>

  <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />
  <script src="Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery.json.js" type="text/javascript"></script>
  <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
  <script src="Scripts/jquery.dialogextend.js" type="text/javascript"></script>
  <script src="Scripts/json2.js" type="text/javascript"></script>
  <script src="Scripts/mustache.js" type="text/javascript"></script>
  <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>
  <script src="Scripts/checkDate.js" type="text/javascript"></script>
  <script src="Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
  <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

  <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="CSS/main.css" rel="stylesheet" type="text/css" />
  <link href="CSS/ReservationRF.css?v=7" rel="Stylesheet" type="text/css" />

  <style type="text/css">
    </style>

  <script language="javascript" type="text/javascript">

    var reservationSaved = false;

    var culture = '<%= twoLetterISOLanguageName %>';

    var pageRoot = '<%= pageRoot %>';

    var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
    var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
    var btnGotoResCard = '<%= GetGlobalResourceObject("LibraryResource", "btnGotoResCard") %>';
    var btnPaymentPage = '<%= GetGlobalResourceObject("LibraryResource", "btnPaymentPage") %>';
    var btnYes = '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>';
    var btnNo = '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>';

    var msgWhereInvoiceTo = '<%= GetGlobalResourceObject("LibraryResource", "WhereInvoiceTo") %>';
    var lblAlreadyRes = '<%= GetGlobalResourceObject("BookTicket", "lblAlreadyRes") %>';
    var msgEnterChildOrInfantBirtDay = '<%= GetGlobalResourceObject("LibraryResource", "EnterChildOrInfantBirtDay") %>';
    var msglblNoResult = '<%= GetGlobalResourceObject("BookTicket", "lblNoResult") %>';
    var msgviewOldResSalePrice = '<%= GetGlobalResourceObject("Controls", "viewOldResSalePrice") %>';
    var msgviewNewResSalePrice = '<%= GetGlobalResourceObject("Controls", "viewNewResSalePrice") %>';
    var msgcancelService = '<%= GetGlobalResourceObject("LibraryResource", "cancelService") %>';
    var ComboSelect = '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
    var ForAgency = '<%=GetGlobalResourceObject("LibraryResource", "ForAgency") %>';
    var ForPassenger = '<%=GetGlobalResourceObject("LibraryResource", "ForPassenger") %>';
    var msglblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
    var msgInvalidBirthDate = '<%= GetGlobalResourceObject("LibraryResource","InvalidBirthDate").ToString() %>';
    var cancelService = '<%= GetGlobalResourceObject("LibraryResource", "cancelService") %>';

    var NS = document.all;

    if (typeof window.event != 'undefined')
      document.onkeydown = function () {
        if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
          return (event.keyCode != 8);
      }
    else
      document.onkeypress = function (e) {
        if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
          return (e.keyCode != 8);
      }

    $(document).ajaxStart(function () {
      $.blockUI({
        message: '<h1>' + msglblPleaseWait + '</h1>'
      });
    }).ajaxStop(function () {
      $.unblockUI();
      return false;
    });

    function maximize() {
      var maxW = screen.availWidth;
      var maxH = screen.availHeight;
      if (location.href.indexOf('pic') == -1) {
        if (window.opera) { } else {
          top.window.moveTo(0, 0);
          if (document.all) { top.window.resizeTo(maxW, maxH); }
          else
            if (document.layers || document.getElementById) {
              if ((top.window.outerHeight < maxH) || (top.window.outerWidth < maxW)) {
                top.window.outerHeight = maxH; top.window.outerWidth = maxW;
              }
            }
        }
      }
    }

    function showDialogEndExit(msg, ResNo) {
      $("#messages").html('');
      $("#messages").html(msg);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        modal: true,
        resizable: true,
        autoResize: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog('close');
            window.close;
            if (ResNo != '') {
              window.location = 'ResView.aspx?ResNo=' + ResNo;
            }
            else {
              window.location.reload();
            }
          }
        }],
        open: function () {
          //$(this).parent().children().children("a.ui-dialog-titlebar-close").remove();
        },
        closeOnEscape: false
      });
    }

    function showDialogEndGotoPaymentPage(msg, ResNo) {
      $('<div>' + msg + '</div>').dialog({
        buttons: [{
          text: btnGotoResCard,
          click: function () {
            $(this).dialog('close');
            window.close;
            window.location = 'ResView.aspx?ResNo=' + ResNo;
          }
        }]
      });
    }

    function showMessage(msg) {
      $("#messages").html(msg);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        modal: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog('close');
          }
        }, {
          text: btnCancel,
          click: function () {
            $(this).dialog('close');
          }
        }]
      });
    }

    function setage(Id, cinDate, _formatDate) {
      try {
        var birthDate = dateValue(_formatDate, $("#iBirthDay" + Id).val());
        if (birthDate == null) {
          $("#iBirthDay" + Id).val('');
          $("#iAge" + Id).val('');
          return;
        }
        var checkIN = dateValue(_formatDate, cinDate);
        if (checkIN == null) return;
        var _minBirthDate = new Date(1, 1, 1910);
        var _birthDate = birthDate;
        var _cinDate = checkIN;
        if (_birthDate < _minBirthDate) {
          $("#iAge" + Id).val('');
          birthDate = '';
          showMsg(InvalidBirthDate);
          return;
        }
        var age = Age(_birthDate, _cinDate);
        $("#iAge" + Id).val(age);
      }
      catch (err) {
        $("#iAge" + Id).val('');
      }
    }

    function removeResService(recID) {
      $.ajax({
        type: "POST",
        url: "ReservationRF.aspx/removeService",
        data: '{"RecID":' + recID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d && msg.d == true) {
            showHotelServices();
            showOtherServices();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMsg(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function removeResServiceDialog(recID) {
      $("#messages").html(cancelService);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        maxWidth: 880,
        maxHeight: 500,
        modal: true,
        buttons: [{
          text: btnYes,
          click: function () {
            $(this).dialog('close');
            removeResService(recID);
            return true;
          }
        }, {
          text: btnNo,
          click: function () {
            $(this).dialog('close');
            return false;
          }
        }]
      });
    }

    function setCustomers() {
      var data = new Object();
      data.Customers = getCustomers();
      $.ajax({
        async: false,
        type: "POST",
        url: "ReservationRF.aspx/setCustomers",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          return true;
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showMsg(xhr.responseText);
          }
          return false;
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function editResService(serviceUrl) {
      setCustomers();
      $("#dialog-resServiceEdit").dialog("open");
      $("#resServiceEdit").attr("src", serviceUrl);
      return false;
    }

    function returnEditResServices(save) {
      if (save == true) {
        $("#dialog-resServiceEdit").dialog("close");
        showHotelServices();
        showOtherServices();
      }
      else {
        $("#dialog-resServiceEdit").dialog("close");
      }
    }

    function returnAddResServices(reLoad, msg) {
      $("#dialog-resServiceAdd").dialog("close");
      if (reLoad == true) {
        showHotelServices();
        showOtherServices();
        $(".saveReservationBtnDiv").show();
      }
    }

    function showAddResService(serviceUrl) {
      var params = new Object();
      params.Customers = getCustomers();
      if (params.Customers.length == 0) {
        showMessage("Please create customers.");
      }
      $.ajax({
        async: false,
        type: "POST",
        url: "ReservationRF.aspx/setCustomers",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var chkUsers = true;//checkUsers();
          if (chkUsers || msg.d == 'OK' || msg.d == 'NO') {
            $("#dialog-resServiceAdd").dialog("open");
            if (serviceUrl.toString().indexOf('?') > -1) {
              $("#resServiceAdd").attr("src", serviceUrl + '&recordType=temp' + '&defaultUnSelect=1');
            }
            else {
              $("#resServiceAdd").attr("src", serviceUrl + '?recordType=temp' + '&defaultUnSelect=1');
            }
          }
          else {
            showMessage(msgEnterChildOrInfantBirtDay);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showMessage(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function returnAddResServicesExt(save) {
      if (save == true) {
        $("#dialog-resServiceExtAdd").dialog("close");
      }
      else {
        $("#dialog-resServiceExtAdd").dialog("close");
      }
    }

    function showAddResServiceExt(serviceUrl) {
      var params = new Object();
      params.Customers = getCustomers();
      $.ajax({
        async: false,
        type: "POST",
        url: "ReservationRF.aspx/setCustomers",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var chkUsers = true; //checkUsers();
          if (chkUsers) {
            $("#dialog-resServiceExtAdd").dialog("open");
            $("#resServiceExtAdd").attr("src", serviceUrl + '?recordType=temp');
          } else {
            showMessage(msgEnterChildOrInfantBirtDay);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
          return false;
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function showPackageWizardButton(_this) {
      $("#btnWizard").show();
      return;
      if ($("#fltPackage").val() != '') {
        $("#btnWizard").show();
        var element = $(_this).find('option:selected');
        var depCity = element.attr("DepCity");
        var arrCity = element.attr("ArrCity");
        $("#fltDeparture").val(depCity);
        $("#fltArrival").val(arrCity);
      }
      else {
        $("#btnWizard").hide();
      }
    }

    function getHolpackList() {
      var params = new Object();
      var begDate = $("#fltBegDate").datepicker('getDate') != null ? new Date($("#fltBegDate").datepicker('getDate')) : new Date();
      begDate.setMinutes(begDate.getMinutes() - begDate.getTimezoneOffset());
      params.BegDate = begDate.getTime();
      params.Night = parseInt($("#fltNight").val());
      params.ResType = parseInt($("#fltResType").val());
      $.ajax({
        async: false,
        type: "POST",
        url: "ReservationRF.aspx/getHolpackList",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            $("#fltPackage").html('');
            if (msg.d != null) {
              $("#fltPackage").append('<option value="" depCity="" arrCity=""></option>');
              $.each(msg.d, function (i) {
                $("#fltPackage").append('<option value="' + this.Code + '" depCity="' + this.DepCity + '" arrCity="' + this.ArrCity + '">' + this.Name + '</option>');
              });
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function onChangeGroup(_this) {
      var element = $(_this).find('option:selected');
      var checkIn = element.attr("checkIn");
      var days = element.attr("days");
      if (checkIn != '' && checkIn != 'null') {
        var date1 = new Date(checkIn);
        $("#fltBegDate").datepicker("setDate", date1);
        $("#fltNight").val(days);
      }
      else {
        $("#fltBegDate").datepicker("setDate", new Date());
        $("#fltNight").val('7');
      }
    }

    function getOtherService(tmpHtml) {
      $.ajax({
        type: "POST",
        url: "ReservationRF.aspx/getOtherService",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var data = msg.d;
            var html = Mustache.render(tmpHtml, data);
            $("#otherServices").html(html);
            $("#otherServices").show();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function showOtherServices() {
      $.get($("#tmplPath").val() + 'RRFOtherServices.html?v=201602051525', function (html) {
        getOtherService(html);
      }).fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        showAlert("Request Failed: " + err);
      });
    }

    function getHotelService(tmpHtml) {
      $("#hotelServices").html('');
      $.ajax({
        type: "POST",
        url: "ReservationRF.aspx/getHotelService",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var data = msg.d;
            var html = Mustache.render(tmpHtml, data);
            $("#hotelServices").html(html);
            $("#hotelServices").show();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function showHotelServices() {
      $.get($("#tmplPath").val() + 'RRFHotelServices.html?v=201602171340', function (html) {
        getHotelService(html);
      }).fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        showAlert("Request Failed: " + err);
      });
    }

    function getCustomerList(tmpHtml) {
      $.ajax({
        type: "POST",
        url: "ReservationRF.aspx/getCustomers",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var data = msg.d;
            var html = Mustache.render(tmpHtml, data);
            $("#customers").html(html);
            $("#customers").show();
            var _dateMask = $("#dateMask").val();
            if ($(".formatDate").length > 0 && $("#dateMask").length > 0) {
              $(".formatDate").mask(_dateMask);
            }
            if ($("#scanPass").val() == '1') {
              $(".passportScan").show();
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function showCustomers() {
      $.get($("#tmplPath").val() + 'RRFCustomers.html?v=201511241130', function (html) {
        getCustomerList(html);
      }).fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        showAlert("Request Failed: " + err);
      });
    }

    function getCustomers() {
      var adult = $("#fltAdult").val() != '' ? parseInt($("#fltAdult").val()) : 0;
      var child = $("#fltChild").val() != '' ? parseInt($("#fltChild").val()) : 0;
      var infant = $("#fltInfant").val() != '' ? parseInt($("#fltInfant").val()) : 0;
      var custCount = adult + child + infant;
      var custList = [];
      for (var i = 1; i <= custCount; i++) {
        var cust = new Object();
        cust.SeqNo = $("#iSeqNo" + i).text();
        cust.Title = $("#iTitle" + i).length > 0 ? $("#iTitle" + i).val() : '';
        cust.Surname = $("#iSurname" + i).length > 0 ? $("#iSurname" + i).val() : '';
        cust.SurnameL = $("#iSurnameL" + i).length > 0 ? $("#iSurnameL" + i).val() : '';
        cust.Name = $("#iName" + i).length > 0 ? $("#iName" + i).val() : '';
        cust.NameL = $("#iNameL" + i).length > 0 ? $("#iNameL" + i).val() : '';
        cust.BirtDay = $("#iBirthDay" + i).length > 0 ? $("#iBirthDay" + i).val() : '';
        cust.Age = $("#iAge" + i).length > 0 ? $("#iAge" + i).val() : '';
        cust.IDNo = $("#iIDNo" + i).length > 0 ? $("#iIDNo" + i).val() : '';
        cust.PassSerie = $("#iPassSerie" + i).length > 0 ? $("#iPassSerie" + i).val() : '';
        cust.PassNo = $("#iPassNo" + i).length > 0 ? $("#iPassNo" + i).val() : '';
        cust.PassIssueDate = $("#iPassIssueDate" + i).length > 0 ? $("#iPassIssueDate" + i).val() : '';
        cust.PassExpDate = $("#iPassExpDate" + i).length > 0 ? $("#iPassExpDate" + i).val() : '';
        cust.PassGiven = $("#iPassGiven" + i).length > 0 ? $("#iPassGiven" + i).val() : '';
        cust.Phone = $("#iPhone" + i).length > 0 ? $("#iPhone" + i).val() : '';
        cust.Nation = $("#iNation" + i).length > 0 ? parseInt($("#iNation" + i).val()) : null;
        cust.Nationality = $("#iNationality" + i).length > 0 ? $("#iNationality" + i).val() : null;
        cust.Passport = $("#iHasPassport" + i).length > 0 ? $("#iHasPassport" + i)[0].checked : true;
        cust.Leader = $('input[name=Leader]:checked').val() == cust.SeqNo;
        custList.push(cust);
      }
      return custList;
    }

    function getVehilcleCatList() {
      var vehicleCatIdList = $("select[name=VehicleCatID]");
      var vehicleCatList = [];
      for (var i = 0; i < vehicleCatIdList.length; i++) {
        var ServiceID = $(vehicleCatIdList[i])[0].id.toString().split('_')[1];
        var VehicleCatID = $(vehicleCatIdList[i]).val();
        vehicleCatList.push({ ServiceID: ServiceID, VehicleCatID: VehicleCatID, VehicleUnit: $("#VehicleUnit_" + ServiceID).val() });
      };
      return vehicleCatList;
    }

    function saveReservation() {
      var params = new Object();
      params.Customers = getCustomers();
      params.goAhead = $("#goAhead").val() == '1' ? true : false;
      params.ResType = parseInt($("#fltResType").val());
      params.VehicleCatIDList = getVehilcleCatList();
      params.GroupNo = $("#fltGroupNo").val();
      if (reservationSaved) {
        showMessage(lblAlreadyRes);
        return;
      } else {
        reservationSaved = true;
      }

      $.ajax({
        type: "POST",
        url: "ReservationRF.aspx/saveReservation",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var retVal = $.json.decode(msg.d)[0];
            if (retVal.ControlOK == true) {
              if (retVal.GotoPaymentPage == true)
                showDialogEndGotoPaymentPage(retVal.Message, retVal.ResNo);
              else if (retVal.GotoReservation)
                showDialogEndExit(retVal.Message, retVal.ResNo);
              else showDialogEndExit(retVal.Message, '');
            }
            else {
              if (retVal.OtherReturnValue == '32') {
                showMessage(retVal.Message);
                $("#goAhead").val('1');
                reservationSaved = false;
              }
              else {
                showMessage(retVal.Message);
                reservationSaved = false;
                $("#goAhead").val('0');
              }
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        complette: function (e, xhr, settings) {
          if (e.status === 200) {

          } else if (e.status === 304) {

          } else if (e.status === 408) {
            logout();
          } else {

          }
        }
      });
    }

    function saveWizard() {
      $("#dialog-MakeReservation").dialog('close');
      showCustomers();
      showHotelServices();
      showOtherServices();
      $(".saveReservationBtnDiv").show();
    }

    function createHotelServiceWizard(html) {
      window.scrollTo(0, 0);
      $('html').css('overflow', 'hidden');
      $("#MakeReservation").attr("src", $("#basePageUrl").val() + 'ReservationRFWizard.aspx');
      $("#dialog-MakeReservation").dialog(
          {
            autoOpen: true,
            modal: true,
            width: 990,
            height: 700,
            resizable: true,
            autoResize: true,
            bigframe: true,
            close: function (event, ui) { $('html').css('overflow', 'auto'); }
          }).dialogExtend({
            "maximize": true,
            "icons": {
              "maximize": "ui-icon-circle-plus",
              "restore": "ui-icon-pause"
            }
          });
      $("#dialog-MakeReservation").dialogExtend("maximize");
    }

    function getHotels(hotelServices) {
      $.get($("#tmplPath").val() + 'RRFHotels.html?v=201510261325', function (html) {
        createHotelServiceWizard(html);
      }).fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        showAlert("Request Failed: " + err);
      });
    }

    function onClickBtnWizard() {
      var params = new Object();
      data = new Object();
      data.ResType = parseInt($("#fltResType").val());
      data.GroupNo = parseInt($("#fltGroupNo").val());
      data.CheckIn = $("#fltBegDate").datepicker('getDate') != null ? $("#fltBegDate").datepicker('getDate') : null;
      data.Night = parseInt($("#fltNight").val()) != NaN ? parseInt($("#fltNight").val()) : 1;
      data.Adult = parseInt($("#fltAdult").val()) != NaN ? parseInt($("#fltAdult").val()) : 1;
      data.Child = parseInt($("#fltChild").val()) != NaN ? parseInt($("#fltChild").val()) : 0;
      data.Infant = parseInt($("#fltInfant").val()) != NaN ? parseInt($("#fltInfant").val()) : 0;
      data.LeaderSurname = $("#fltSurname").val();
      data.LeaderName = $("#fltName").val();
      data.HolPack = $("#fltPackage").val();
      data.DepCity = parseInt($("#fltDeparture").val()) != NaN ? parseInt($("#fltDeparture").val()) : null;
      data.ArrCity = parseInt($("#fltArrival").val()) != NaN ? parseInt($("#fltArrival").val()) : null;
      data.ResNote = $("#fltReservationNote").val();
      data.Nationality = $("#fltNationalty").val();
      data.VisaSupplier = $("#fltVisaSupplier").val();
      data.EntrancePort = $("#fltEntrancePort").val();
      params.data = data;
      $.ajax({
        type: "POST",
        url: "ReservationRF.aspx/getPackageWizard",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            createHotelServiceWizard();
            $(".OtherServiceButtons").show();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        complette: function (e, xhr, settings) {
          if (e.status === 200) {

          } else if (e.status === 304) {

          } else if (e.status === 408) {
            logout();
          } else {

          }
        }
      });

    }

    function onClickAddCust() {
      var params = new Object();
      params.ResType = parseInt($("#fltResType").val());
      params.GroupNo = parseInt($("#fltGroupNo").val());
      params.CheckIn = $("#fltBegDate").datepicker('getDate') != null ? $("#fltBegDate").datepicker('getDate') : null;
      params.Night = parseInt($("#fltNight").val()) != NaN ? parseInt($("#fltNight").val()) : 1;
      params.Adult = parseInt($("#fltAdult").val()) != NaN ? parseInt($("#fltAdult").val()) : 1;
      params.Child = parseInt($("#fltChild").val()) != NaN ? parseInt($("#fltChild").val()) : 0;
      params.Infant = parseInt($("#fltInfant").val()) != NaN ? parseInt($("#fltInfant").val()) : 0;
      params.LeaderSurname = $("#fltSurname").val();
      params.LeaderName = $("#fltName").val();
      params.DepCity = parseInt($("#fltDeparture").val()) != NaN ? parseInt($("#fltDeparture").val()) : null;
      params.ArrCity = parseInt($("#fltArrival").val()) != NaN ? parseInt($("#fltArrival").val()) : null;
      $.ajax({
        type: "POST",
        url: "ReservationRF.aspx/createCustomers",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            showCustomers();
            $(".OtherServiceButtons").show();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        complette: function (e, xhr, settings) {
          if (e.status === 200) {

          } else if (e.status === 304) {

          } else if (e.status === 408) {
            logout();
          } else {

          }
        }
      });
    }

    function onChangeResType() {
      var value = $("#fltResType").val();
      if (value == "0") {
        $(".Package").hide();
        $("#fltPackage").val('');
        //$("#btnWizard").hide();
      }
      else {
        $(".Package").show();
        //$("#btnWizard").show();
      }
    }

    function getFormData() {
      $.ajax({
        type: "POST",
        url: "ReservationRF.aspx/getFormData",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            $("#fltResType").html('');
            if (msg.d.hasOwnProperty('ResTypeList') && msg.d.ResTypeList != null) {
              $.each(msg.d.ResTypeList, function (i) {
                $("#fltResType").append('<option value="' + this.Code + '">' + this.Name + '</option>');
              });
            }
            $("#fltGroupNo").html('');
            if (msg.d.hasOwnProperty('GroupList') && msg.d.GroupList != null) {
              $.each(msg.d.GroupList, function (i) {
                $("#fltGroupNo").append('<option value="' + this.GroupNo + '" checkIn="' + this.CheckInUnix + '" days="' + this.Days + '">' + this.FullName + '</option>');
              });
            }
            $("#fltNationalty").html('');
            if (msg.d.hasOwnProperty('Nationality') && msg.d.Nationality != null) {
              $.each(msg.d.Nationality, function (i) {
                $("#fltNationalty").append('<option value="' + this.Code3 + '" ' + (this.Code3 == msg.d.CurrentNational ? 'selected="selected"' : '') + '>' + this.Name + '</option>');
              });
            }
            $("#fltDeparture").html('');
            $("#fltArrival").html('');
            if (msg.d.hasOwnProperty('LocationList') && msg.d.LocationList.length > 0) {
              $("#fltDeparture").append('<option value="">' + ComboSelect + '</option>');
              $("#fltArrival").append('<option value="">' + ComboSelect + '</option>');
              $.each(msg.d.LocationList, function (i) {
                $("#fltDeparture").append('<option value="' + this.RecID + '">' + this.FullNameL + '</option>');
                $("#fltArrival").append('<option value="' + this.RecID + '">' + this.FullNameL + '</option>');
              });
            }
            $("#fltVisaSupplier").html('');
            if (msg.d.hasOwnProperty('SupplierList') && msg.d.SupplierList.length > 0) {
              $("#fltVisaSupplier").append('<option value="">' + ComboSelect + '</option>');
              $.each(msg.d.SupplierList, function (i) {
                $("#fltVisaSupplier").append('<option value="' + this.Code + '">' + this.NameL + '</option>');
              });
            }
            $("#fltEntrancePort").html('');
            if (msg.d.hasOwnProperty('EntrancePortList') && msg.d.EntrancePortList.length > 0) {
              $("#fltEntrancePort").append('<option value="">' + ComboSelect + '</option>');
              $.each(msg.d.EntrancePortList, function (i) {
                $("#fltEntrancePort").append('<option value="' + this.Code + '">' + this.NameL + '</option>');
              });
            }

            $(".OtherServiceButtons").html('');
            if (msg.d.hasOwnProperty('OtherServiceButtons') && msg.d.OtherServiceButtons.length > 0) {
              $.each(msg.d.OtherServiceButtons, function (i) {
                var btn = '<div onclick="showAddResService(\'' + this.controlUrl + '\');" class="OtherMenuBtn"><img alt="" src="' + this.imageUrl + '" width="32" height="32"><br />' + this.description + '</div>';
                $(".OtherServiceButtons").append(btn);
              });
            }

            getHolpackList();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function dialogInitialize() {
      //$("#dialog-Ssrc").dialog({ autoOpen: false, modal: true, width: 640, height: 550, resizable: true, autoResize: true });
      $("#dialog-resServiceEdit").dialog({ autoOpen: false, modal: true, width: 675, height: 600, resizable: true, autoResize: true });
      //$("#dialog-promoSelect").dialog({ autoOpen: false, modal: true, width: 820, height: 440, resizable: true, autoResize: true });
      //$("#dialog-resCustOtherInfo").dialog({ autoOpen: false, modal: true, width: 440, height: 440, resizable: true, autoResize: true });
      //$("#dialog-resCustomerAddress").dialog({ autoOpen: false, modal: true, width: 600, height: 535, resizable: false, autoResize: true });
      $("#dialog-resServiceAdd").dialog({ autoOpen: false, modal: true, width: 675, height: 600, resizable: true, autoResize: true });
      $("#dialog-resServiceExtAdd").dialog({ autoOpen: false, modal: true, width: 870, height: 525, resizable: true, autoResize: true });
      //$("#dialog-changeFlight").dialog({ autoOpen: false, modal: true, width: 900, height: 525, resizable: true, autoResize: true });
    }

    var selectedSeqNo = null;

    function returnPassportScan(passportData, ready) {
      $("#dialog-passportScan").dialog('close');
      if (ready != undefined && ready != null && ready == true) {
        var params = new Object();
        params.SeqNo = selectedSeqNo;
        params.PassportData = passportData;
        $.ajax({
          type: "POST",
          url: "ReservationRF.aspx/updateCustomerData",
          data: JSON.stringify(params),
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (msg) {
            if (msg.hasOwnProperty('d') && msg.d != null && msg.d == true) {
              showCustomers();
            }
          },
          error: function (xhr, msg, e) {
            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
              showMessage(xhr.responseText);
          },
          complette: function (e, xhr, settings) {
            if (e.status === 200) {

            } else if (e.status === 304) {

            } else if (e.status === 408) {
              logout();
            } else {

            }
          }
        });
      }
      selectedSeqNo = null;
    }

    function onclickPassportScanBtn(seqNo) {
      selectedSeqNo = seqNo;
      //window.open(pageRoot + 'PassportReader.aspx', '_blank');

      window.scrollTo(0, 0);
      $('html').css('overflow', 'hidden');
      $("#passportScan").attr("src", pageRoot + 'PassportReader.aspx');
      $("#dialog-passportScan").dialog(
          {
            autoOpen: true,
            modal: true,
            width: 540,
            height: 530,
            resizable: true,
            autoResize: true,
            bigframe: true,
            close: function (event, ui) {
              $('html').css('overflow', 'auto');
            }
          });

    }

    function checkPassScan() {
      if ($.browser.msie) {
        $("#scanPass").val("1");
      }
    }

    $(document).ready(function () {

      maximize();

      dialogInitialize();

      $.datepicker.setDefaults($.datepicker.regional[culture != 'en' ? culture : '']);

      $("#fltBegDate").datepicker({
        showOn: "button",
        buttonImage: "Images/Calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onSelect: function (dateText, inst) {
          if (dateText != '') {
            getHolpackList();
          }
        },
        minDate: new Date()
      });

      $("#fltNight").blur(function () {
        getHolpackList();
      });

      getFormData();

      //$("#fltGroupNo").change(function () {
      //    onChangeGroup(this);
      //});

      //$("#btnWizard").hide();

      $("#btnAddHotel").on("click", function () {

      });

      $("#btnEditHotel").on("click", function () {

      });

      //checkPassScan();
    });
  </script>

</head>
<body>
  <form id="form1" runat="server">
    <div class="Page">
      <div>
        <tv1:Header ID="tvHeader" runat="server" />
        <tv1:MainMenu ID="tvMenu" runat="server" />
      </div>
      <div class="ui-helper-hidden">
        <asp:HiddenField ID="tmplPath" runat="server" />
        <input id="goAhead" type="hidden" value="0" />
        <input id="scanPass" type="hidden" value="0" />
      </div>
      <div class="Content">
        <div class="ui-helper-clearfix RRFHeader">
          <div class="RRFHeaderLeft">
            <div class="ui-helper-clearfix ResTypeResNo">
              <div class="ResType">
                <div class="lblResType">Reservation Type : </div>
                <select id="fltResType" onchange="onChangeResType()">
                  <option value="0">&lt;Individual&gt;</option>
                  <option value="1">Umrah</option>
                  <option value="2">Hajj</option>
                </select>
              </div>
            </div>
            <div class="ui-helper-clearfix GroupNo">
              <div class="lblGroupNo">Group No : </div>
              <select id="fltGroupNo" onchange="onChangeGroup(this)">
              </select>
            </div>
            <div class="ui-helper-clearfix BegDateNight">
              <div class="BegDate">
                <div class="lblBegDate">Begin Date : </div>
                <input id="fltBegDate" type="text" />
              </div>
              <div class="Night">
                <div class="lblNight">Night : </div>
                <input id="fltNight" type="text" name="number" value="7" />
              </div>
            </div>
            <div class="ui-helper-clearfix Pax">
              <div class="Adult">
                <div class="lblAdult">Adult : </div>
                <input id="fltAdult" type="text" name="number" value="1" />
              </div>
              <div class="Child">
                <div class="lblChild">Child : </div>
                <input id="fltChild" type="text" name="number" value="0" />
              </div>
              <div class="Infant">
                <div class="lblInfant">Infant : </div>
                <input id="fltInfant" type="text" name="number" value="0" />
              </div>
              <div class="AddCust">
                <input id="btnAddCust" type="button" value="Create" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" onclick="onClickAddCust()" />
              </div>
            </div>
            <div class="ui-helper-clearfix Guest">
              <div class="lblGuest">Guest Surname/Name : </div>
              <input id="fltSurname" type="text" /><input id="fltName" type="text" />
            </div>
            <div class="ui-helper-clearfix Package">
              <div class="lblPackage">Package : </div>
              <select id="fltPackage" onchange="showPackageWizardButton(this);">
              </select>
            </div>
            <div class="ui-helper-clearfix DepArr">
              <div class="lblDepArr">Departure/Arrival : </div>
              <select id="fltDeparture"></select>
              <select id="fltArrival"></select>
            </div>
          </div>
          <div class="RRFHeaderRight">
            <div class="ui-helper-clearfix RegisterDate">
              <div class="lblRegisterDate">Register Date : </div>
              <span id="fltRegisterDate"></span>
            </div>
            <div class="ui-helper-clearfix ReservationStatus">
              <div class="lblReservationStatus">Reservation Status : </div>
              <span id="fltReservationStatus"></span>
            </div>
            <div class="ui-helper-clearfix Confirmation">
              <div class="lblConfirmation">Confirmation : </div>
              <span id="fltConfirmation"></span>
            </div>
            <div class="ui-helper-clearfix ReservationNote">
              <div class="lblReservationNote">Reservation Note : </div>
              <input id="fltReservationNote" />
            </div>
            <div class="ui-helper-clearfix Nationalty">
              <div class="lblNationalty">Nationalty : </div>
              <select id="fltNationalty">
              </select>
            </div>
            <div class="ui-helper-clearfix VisaSupplier">
              <div class="lblVisaSupplier">Visa Supplier : </div>
              <select id="fltVisaSupplier">
              </select>
            </div>
            <div class="ui-helper-clearfix EntrancePort">
              <div class="lblEntrancePort">Entrance Port : </div>
              <select id="fltEntrancePort">
              </select>
            </div>
          </div>
        </div>
        <div class="ui-helper-clearfix RRForm">
          <div id="hotelServices" class="ui-helper-clearfix HotelServices">
          </div>
          <div class="ui-helper-clearfix">
            <input type="button" id="btnWizard" onclick="onClickBtnWizard();" value="Accommodation Wizard" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />&nbsp;&nbsp;            
          </div>
          <div id="otherServices" class="ui-helper-clearfix OtherServices">
          </div>
          <div class="ui-helper-clearfix OtherServiceButtons ui-widget-header ui-helper-hidden">
          </div>
          <div id="customers" class="ui-helper-clearfix Customers ui-helper-hidden">
          </div>
          <div class="ui-helper-hidden saveReservationBtnDiv">
            <input type="button" value="Save reservation" onclick="saveReservation()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
          </div>
        </div>
      </div>
      <div class="Footer">
        <tv1:Footer ID="tvfooter" runat="server" />
      </div>
    </div>
  </form>
  <div id="dialog-message" title="" class="ui-helper-hidden">
    <span id="messages"></span>
  </div>
  <div id="dialog-MakeReservation" title='<%= GetGlobalResourceObject("MakeReservation", "lblMakeReservation") %>'
    style="display: none; text-align: center;">
    <iframe id="MakeReservation" height="100%" width="960px" frameborder="0" style="clear: both; text-align: left;"></iframe>
  </div>
  <div id="dialog-resServiceAdd" title='<%= GetGlobalResourceObject("MakeReservation", "lblService") %>'
    class="ui-helper-hidden">
    <iframe id="resServiceAdd" height="100%" width="100%" frameborder="0" style="clear: both;"></iframe>
  </div>
  <div id="dialog-resServiceExtAdd" title='<%= GetGlobalResourceObject("MakeReservation", "lblExtService") %>'
    style="display: none;">
    <iframe id="resServiceExtAdd" height="100%" width="830px" frameborder="0" style="clear: both;"></iframe>
  </div>
  <div id="dialog-resServiceEdit" title='<%= GetGlobalResourceObject("ResView", "lblEditService") %>'
    style="display: none;">
    <iframe id="resServiceEdit" height="100%" width="100%" frameborder="0"
      style="clear: both;"></iframe>
  </div>
  <div id="dialog-passportScan" title='Read Passport Data' style="display: none;">
    <iframe id="passportScan" height="100%" width="520" frameborder="0" style="clear: both;"></iframe>
  </div>
</body>
</html>
