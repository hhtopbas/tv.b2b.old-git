﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AgencyPaymentMonitor.aspx.cs"
    Inherits="AgencyPaymentMonitor" EnableEventValidation="false" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<%@ Register Src="~/Comment.ascx" TagName="ResComment" TagPrefix="resCom1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "AgencyPaymentMonitor")%></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

    <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <script src="Scripts/jquery.textTruncate.js" type="text/javascript"></script>

    <script src="Scripts/datatable/jquery.dataTables.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.dialogextend.js" type="text/javascript"></script>

    <%--<script src="Scripts/jquery.printElement.js" type="text/javascript"></script>--%>

    <script src="Scripts/jquery.printPage.js" type="text/javascript"></script>

    <script src="Scripts/jquery.browser.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/confirm.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="CSS/AgencyPayment.css" rel="stylesheet" type="text/css" />
    <link href="CSS/jquery.table.jui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/ColVis.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        if (typeof window.event != 'undefined')
            document.onkeydown = function() {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function(e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function logout() {
            $('<div>' + '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>' + '</div>').dialog({
                autoOpen: true,
                position: {
                    my: 'center',
                    at: 'center'
                },
                modal: true,
                resizable: true,
                autoResize: true,
                bigframe: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                        $(this).dialog("close");
                        $(this).dialog("destroy");
                        window.location = 'Default.aspx';
                    }
                }
            });
        }

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>'; $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function showDialogYesNo(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                            return true;
                        }
                        ,
                        '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>': function() {
                            $(this).dialog('close');
                            return false;
                        }
                    }
                });
            });
        }

        function showAlert(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function showDialog(msg, transfer, trfUrl) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                            if (transfer == true) {
                                var url = trfUrl;
                                $(location).attr('href', url);
                            }
                        }
                    }
                });
            });
        }

        function showMessage(msg, transfer, trfUrl) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                        }
                        ,
                        '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        var NS = document.all;

        function maximize() {
            var maxW = screen.availWidth;
            var maxH = screen.availHeight;
            if (location.href.indexOf('pic') == -1) {
                if (window.opera) { } else {
                    top.window.moveTo(0, 0);
                    if (document.all) { top.window.resizeTo(maxW, maxH); }
                    else
                        if (document.layers || document.getElementById) {
                        if ((top.window.outerHeight < maxH) || (top.window.outerWidth < maxW)) {
                            top.window.outerHeight = maxH; top.window.outerWidth = maxW;
                        }
                    }
                }
            }
        }

        function setDepCity(data) {
            $("#fltDepCity").html('');
            $("#fltDepCity").append("<option value=''>" + '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>' + "</option>");
            $.each(data.DepCityData, function(i) {
                $("#fltDepCity").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
            if (data.Filter.DepCity != null && data.Filter.DepCity != '') $("#fltDepCity").val(data.Filter.DepCity);
        }

        function setArrCity(data) {
            $("#fltArrCity").html('');
            $("#fltArrCity").append("<option value=''>" + '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>' + "</option>");
            $.each(data.ArrCityData, function(i) {
                $("#fltArrCity").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
            if (data.Filter.ArrCity != null && data.Filter.ArrCity != '') $("#fltArrCity").val(data.Filter.ArrCity);
        }

        function setAgencyOffice(data) {
            $("#fltAgencyOffice").html('');
            $("#fltAgencyOffice").append("<option value=''>" + '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>' + "</option>");
            $.each(data.AgencyOfficeData, function(i) {
                $("#fltAgencyOffice").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
            if (data.Filter.AgencyOffice != null && data.Filter.AgencyOffice != '') $("#fltAgencyOffice").val(data.Filter.AgencyOffice);
        }

        function setAgencyUser(data) {
            $("#fltAgencyUser").html('');
            $("#fltAgencyUser").append("<option value=''>" + '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>' + "</option>");
            $.each(data.AgencyUserData, function(i) {
                $("#fltAgencyUser").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
            if (!data.Filter.ShowAllRes)
                $("#fltAgencyUser").attr("disabled", true);
            else $("#fltAgencyUser").attr("disabled", false);

            if (data.Filter.AgencyUser != null && data.Filter.AgencyUser != '')
                $("#fltAgencyUser").val(data.Filter.AgencyUser);

        }

        function DateChgBegDate(_TextDay1, _PopCal) {
            if (_TextDay1.value == '') return
            var _TextDay2 = document.getElementById('<%= fltBeginDate2.ClientID %>');
            if ((!_TextDay1) || (!_PopCal)) return
            if (_TextDay2.value != '') {
                var _format = _TextDay1.getAttribute("Format");
                var _Day1 = _PopCal.getDate(_TextDay1.value, _format);
                var _Day2 = _PopCal.getDate(_TextDay2.value, _format);
                if (_Day1 > _Day2) {
                    showAlert('<%= GetGlobalResourceObject("LibraryResource", "LastDateSmall") %>');
                    _TextDay2.value = _TextDay1.value;
                }
            }
        }

        function DateChgBegDateTo(_TextDay2, _PopCal) {
            if (_TextDay2.value == '') return
            var _TextDay1 = document.getElementById('<%= fltBeginDate1.ClientID %>');
            if ((!_TextDay2) || (!_PopCal)) return
            if (_TextDay1.value != '') {
                var _format = _TextDay2.getAttribute("Format");
                var _Day2 = _PopCal.getDate(_TextDay2.value, _format);
                var _Day1 = _PopCal.getDate(_TextDay1.value, _format);
                if (_Day1 > _Day2) {
                    showAlert('<%= GetGlobalResourceObject("LibraryResource", "LastDateSmall") %>');
                    _TextDay2.value = _TextDay1.value;
                }
            }
        }

        function DateChgSaleDate(_TextDay1, _PopCal) {
            if (_TextDay1.value == '') return
            var _TextDay2 = document.getElementById('<%= fltResDate2.ClientID %>');
            if ((!_TextDay1) || (!_PopCal)) return
            if (_TextDay2.value != '') {
                var _format = _TextDay1.getAttribute("Format");
                var _Day1 = _PopCal.getDate(_TextDay1.value, _format);
                var _Day2 = _PopCal.getDate(_TextDay2.value, _format);
                if (_Day1 > _Day2) {
                    showAlert('<%= GetGlobalResourceObject("LibraryResource", "LastDateSmall") %>');
                    _TextDay2.value = _TextDay1.value;
                }
            }
        }

        function DateChgSaleDateTo(_TextDay2, _PopCal) {
            if (_TextDay2.value == '') return
            var _TextDay1 = document.getElementById('<%= fltResDate1.ClientID %>');
            if ((!_TextDay2) || (!_PopCal)) return
            if (_TextDay1.value != '') {
                var _format = _TextDay2.getAttribute("Format");
                var _Day2 = _PopCal.getDate(_TextDay2.value, _format);
                var _Day1 = _PopCal.getDate(_TextDay1.value, _format);
                if (_Day1 > _Day2) {
                    showAlert('<%= GetGlobalResourceObject("LibraryResource", "LastDateSmall") %>');
                    _TextDay2.value = _TextDay1.value;
                }
            }
        }

        function setOtherFields(data) {
            $('input[name=fltResStatus]').each(function(index) { this.checked = false; });
            if (data.Filter.ResStatus != null && data.Filter.ResStatus != '') {
                for (i = 0; i < data.Filter.ResStatus.split(';').length; i++) {
                    $("#fltResStatus" + data.Filter.ResStatus.split(';')[i]).attr("checked", "checked");
                }
            }
            $('input[name=fltConfirmStatus]').each(function(index) { this.checked = false; });
            if (data.Filter.ConfStatus != null && data.Filter.ConfStatus != '') {
                for (i = 0; i < data.Filter.ConfStatus.split(';').length; i++) {
                    $("#fltConfirmStatus" + data.Filter.ConfStatus.split(';')[i]).attr("checked", "checked");
                }
            }
            $('input[name=fltPaymentStatus]').each(function(index) { this.checked = false; });
            if (data.Filter.PayStatus != null && data.Filter.PayStatus != '') {
                for (i = 0; i < data.Filter.PayStatus.split(';').length; i++) {
                    $("#fltPaymentStatus" + data.Filter.PayStatus.split(';')[i]).attr("checked", "checked");
                }
            }

            $("#fltSurname").val(data.Filter.LeaderNameF != null ? data.Filter.LeaderNameF : "");
            $("#fltName").val(data.Filter.LeaderNameS != null ? data.Filter.LeaderNameS : "");
            var _date = '';
            if (data.Filter.BeginDate1 != null && data.Filter.BeginDate1 != '') {
                var _date = new Date(Date(eval(data.Filter.BeginDate1)).toString());
                $("#fltBeginDate1").val($.format.date(_date, data.Filter.DateFormat));
            }
            else $("#fltBeginDate1").val('');

            if (data.Filter.BeginDate2 != null && data.Filter.BeginDate2 != '') {
                var _date = new Date(Date(eval(data.Filter.BeginDate2)).toString());
                $("#fltBeginDate2").val($.format.date(_date, data.Filter.DateFormat));
            }
            else $("#fltBeginDate2").val('');

            if (data.Filter.ResDate1 != null && data.Filter.ResData1 != '') {
                var _date = new Date(Date(eval(data.Filter.ResDate1)).toString());
                $("#fltResDate1").val($.format.date(_date, data.Filter.DateFormat));
            }
            else $("#fltResDate1").val('');

            if (data.Filter.ResDate2 != null && data.Filter.ResData2 != '') {
                var _date = new Date(Date(eval(data.Filter.ResDate2)).toString());
                $("#fltResDate2").val($.format.date(_date, data.Filter.DateFormat));
            }
            else $("#fltResDate2").val('');

            btnFilterClick();
        }


        function btnFilterClick() {
            var _resStatus = '';
            var _confStatus = '';
            var _payStatus = '';

            var chkBox = $("input:checked");
            for (i = 0; i < chkBox.length; i++) {
                if (chkBox[i].name == 'fltResStatus') {
                    if (_resStatus.length > 0) _resStatus += ',';
                    _resStatus += chkBox[i].value;
                }
                if (chkBox[i].name == 'fltConfirmStatus') {
                    if (_confStatus.length > 0) _confStatus += ',';
                    _confStatus += chkBox[i].value;
                }
                if (chkBox[i].name == 'fltPaymentStatus') {
                    if (_payStatus.length > 0) _payStatus += ',';
                    _payStatus += chkBox[i].value;
                }
            }
            var _includeDueDate = $("#fltIncludeDueDate:checked").is(':checked') ? '1' : '0';
            var _filterData = '';
            _filterData += '<|DueDate|:|' + $("#fltDueDate").val() + '|';
            _filterData += ',|BegDate1|:|' + $("#fltBeginDate1").val() + '|';
            _filterData += ',|BegDate2|:|' + $("#fltBeginDate2").val() + '|';
            _filterData += ',|ResDate1|:|' + $("#fltResDate1").val() + '|';
            _filterData += ',|ResDate2|:|' + $("#fltResDate2").val() + '|';
            _filterData += ',|ArrCity|:|' + $("#fltArrCity").val() + '|';
            _filterData += ',|DepCity|:|' + $("#fltDepCity").val() + '|';
            _filterData += ',|AgencyUser|:|' + $("#fltAgencyUser").val() + '|';
            _filterData += ',|LeaderNameF|:|' + $("#fltName").val() + '|';
            _filterData += ',|LeaderNameS|:|' + $("#fltSurname").val() + '|';
            _filterData += ',|PayStatus|:|' + _payStatus + '|';
            _filterData += ',|ConfStatus|:|' + _confStatus + '|';
            _filterData += ',|ResStatus|:|' + _resStatus + '|';
            _filterData += ',|IncludeDueDate|:|' + _includeDueDate + '|';
            _filterData += ',|AgencyOffice|:|' + $("#fltAgencyOffice").val() + '|';
            _filterData += ',|DateFormat|:|' + $("#fltBeginDate1").attr("Format") + '|';
            _filterData += ',|PageNo|:|' + '1' + '|>';

            $.ajax({
                type: "POST",
                data: '{"data":"' + _filterData + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'AgencyPaymentMonitor.aspx/GetPaymentMonitorGrid',
                success: function(msg) {
                    showSummary(msg.d);

                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function showResNote(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function showPaymentPlanDiv(msg) {
            $(function() {
                $('#paymentPlan').html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-paymentPlan").dialog({
                    modal: true,

                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                            return false;

                        }
                    }
                });
            });
        }

        function showPaymentPlan(resNo, ci) {
            //paymentPlan dialog-resComment
            var paraList = '{"ResNo":"' + resNo + '", "lcID":' + ci + '}';
            $.ajax({
                type: "POST",
                data: paraList,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'Services/ResMonitorServices.asmx/getPaymentPlanData',
                success: function(msg) {
                    showPaymentPlanDiv(msg.d);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }
        function createFilterData(clear) {
            $.ajax({
                type: "POST",
                data: '{"clear":"' + clear + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'AgencyPaymentMonitor.aspx/CreateFilterData',
                success: function(msg) {
                    var data = $.json.decode(msg.d);
                    setDepCity(data);
                    setArrCity(data);
                    setAgencyOffice(data);
                    setAgencyUser(data);
                    setOtherFields(data);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function showResView(ResNo) {
            window.location = $("#basePageUrl").val() + "ResView.aspx?ResNo=" + ResNo;
        }

        $.extend($.ui.dialog.prototype, {
            'addbutton': function(buttonName, func) {
                var buttons = this.element.dialog('option', 'buttons');
                buttons[buttonName] = func;
                this.element.dialog('option', 'buttons', buttons);
            }
        });

        // Allows simple button removal from a ui dialog
        $.extend($.ui.dialog.prototype, {
            'removebutton': function(buttonName) {
                var buttons = this.element.dialog('option', 'buttons');
                delete buttons[buttonName];
                this.element.dialog('option', 'buttons', buttons);
            }
        });

        function viewReportAddButton(btnName, func) {
            $("#dialog-viewReport").dialog('addbutton', btnName, func);
        }

        function viewReportRemoveButton(btnName) {
            $("#dialog-viewReport").dialog('removebutton', btnName);
        }

        function viewReportPrint(url, width, height) {
            if ($.browser.msie) {
                var windowSizeArray = ["width=" + width + ",height=" + height + ",scrollbars=yes"];
                var windowName = "popUp";
                var windowSize = windowSizeArray[0];
                window.open(url, windowName, windowSize);
            }
            else {
                $("#viewReport").width(width + 32);
                $("#viewReport").height(height + 32);
                $("#viewReport").contents().find(".mainPage").printPage($("#viewReport").contents().find('style'), width);
            }
        }

        function ReportShow(param1, param2) {
            if (param1 == '' && (param2 != null || param2 != ''))
                showAlert(param2);
            else window.open(param1);
        }

        function htmlPageRender(resNo, htmlUrl, DocName, docNameResource) {
            $("#viewReport").removeAttr("src");
            $("#viewReport").attr("src", htmlUrl + "?ResNo=" + resNo + "&docName=" + docNameResource);
            $("#dialog-viewReport").dialog(
            {
                autoOpen: true,
                modal: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnPrint") %>': function() {
                        var width = $("#viewReport").contents().find(".mainPage").width();
                        var height = $("#viewReport").contents().find(".mainPage").height();
                        var param1 = $("#viewReport").contents().find("#param1").val();
                        viewReportPrint(htmlUrl + "?ResNo=" + resNo + "&docName=" + docNameResource + "&print=1" + (param1 != undefined && param1 != '' ? "&param1=" + param1 : ""), width, height);
                    },
                    '<%= GetGlobalResourceObject("LibraryResource", "btnPDF") %>': function() {
                        //viewPDFReport
                        var width = $("#viewReport").contents().find(".mainPage").width();
                        var _html = $("#viewReport").contents().find("html").html();
                        var noPrint = $("#viewReport").contents().find("#noPrintDiv").html();
                        var _html = _html.replace(noPrint, '&nbsp;').replace(/"/g, '|');
                        $.ajax({
                            async: false,
                            type: "POST",
                            url: "AgencyPaymentMonitor.aspx/viewPDFReport",
                            data: '{"reportType":"' + DocName + '","_html":"' + _html + '","pageWidth":"' + width + '","ResNo":"' + resNo + '","urlBase":"' + htmlUrl + '","pdfConvSerial":"' + $("#pdfConvSerial").val() + '"}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function(msg) {
                                if (msg.d != null) {
                                    ReportShow(msg.d, "");
                                }
                            },
                            error: function(xhr, msg, e) {
                                if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                    showAlert(xhr.responseText);
                            },
                            statusCode: {
                                408: function() {
                                    logout();
                                }
                            }
                        });
                        return false;
                    },
                    '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>': function() {
                        $(this).dialog('close');
                        return true;
                    }
                }
            })
            .dialogExtend({
                "maximize": true,
                "icons": {
                    "maximize": "ui-icon-circle-plus",
                    "restore": "ui-icon-pause"
                }
            });
            $("#dialog-viewReport").dialogExtend("maximize");
        }

        function showResInv(ResNo) {
            $.ajax({
                async: false,
                type: "POST",
                url: "AgencyPaymentMonitor.aspx/getInvoice",
                data: '{"ResNo":"' + ResNo + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != null && msg.d != '') {
                        var retVal = $.json.decode(msg.d);
                        htmlPageRender(ResNo, retVal.htmlUrl, retVal.DocName, retVal.docNameResource);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function pageLoad() {
            maximize();
            createFilterData(true);

        }
        function showSummary(_html) {
            var oLanguage = {
                "sProcessing": '<%= GetGlobalResourceObject("LibraryResource", "sProcessing") %>',
                "sLengthMenu": '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>',
                "sZeroRecords": '<%= GetGlobalResourceObject("LibraryResource", "sZeroRecords") %>',
                "sEmptyTable": '<%= GetGlobalResourceObject("LibraryResource", "sEmptyTable") %>',
                "sInfo": '<%= GetGlobalResourceObject("LibraryResource", "sInfo") %>',
                "sInfoEmpty": '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>',
                "sInfoEmpty": '<%= GetGlobalResourceObject("LibraryResource", "sInfoEmpty") %>',
                "sInfoFiltered": '<%= GetGlobalResourceObject("LibraryResource", "sInfoFiltered") %>',
                "sInfoPostFix": '<%= GetGlobalResourceObject("LibraryResource", "sInfoPostFix") %>',
                "sSearch": '<%= GetGlobalResourceObject("LibraryResource", "sSearch") %>',
                "sUrl": '<%= GetGlobalResourceObject("LibraryResource", "sUrl") %>',
                "oPaginate": {
                    "sFirst": '<%= GetGlobalResourceObject("LibraryResource", "sFirst") %>',
                    "sPrevious": '<%= GetGlobalResourceObject("LibraryResource", "sPrevious") %>',
                    "sNext": '<%= GetGlobalResourceObject("LibraryResource", "sNext") %>',
                    "sLast": '<%= GetGlobalResourceObject("LibraryResource", "sLast") %>'
                },
                "fnInfoCallback": null
            };

            var aoColumns = [{ "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "ResNo") %>', "sWidth": "60px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "DepCity") %>', "sWidth": "125px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "ArrCity") %>', "sWidth": "125px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "BegDate") %>', "sWidth": "60px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "EndDate") %>', "sWidth": "60px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "Nights") %>', "sWidth": "30px" },
                              { "sTitle": '<%= GetGlobalResourceObject("ResMonitor", "lblStatus") %>', "sWidth": "50px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "SaleDate") %>', "sWidth": "60px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "Leader") %>', "sWidth": "125px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "Adult") %>', "sWidth": "30px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "Child") %>', "sWidth": "30px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "Currency") %>', "sWidth": "30px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "SalePrice") %>', "sWidth": "30px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyCom") %>', "sWidth": "75px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "Discount") %>', "sWidth": "75px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "BrokerCom") %>', "sWidth": "75px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "AgencySupDis") %>', "sWidth": "75px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyComSup") %>', "sWidth": "75px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyEB") %>', "sWidth": "75px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "PassengerEB") %>', "sWidth": "75px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyPayable") %>', "sWidth": "75px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyPayableAccording") %>', "sWidth": "75px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyPayment") %>', "sWidth": "75px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "DueDateBalance") %>', "sWidth": "75px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "UserBonus") %>', "sWidth": "75px" },
                              { "sTitle": '<%= GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyBonus") %>', "sWidth": "75px"}];

            $.ajax({
                type: "POST",
                data: "{}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'AgencyPaymentMonitor.aspx/GetSummary',
                success: function(msg) {
                    $("#resMonitorGrid").html('');
                    $("#resMonitorGrid").html(_html);
                    $('#divSum').html('');
                    $('#divSum').html(msg.d);
                    var oTable = $('#resTable').dataTable({
                        "sScrollX": "1000px",
                        "sScrollXInner": "300%",
                        "bScrollCollapse": true,
                        "sPaginationType": "full_numbers",
                        "bFilter": false,
                        "bJQueryUI": true,
                        "oLanguage": oLanguage,
                        "aoColumns": aoColumns,
                        "aaSorting": [],
                        "fnDrawCallback": function(o) {
                            var resTable_ = $('#resTable').height() + 20;
                            $(".dataTables_scrollBody").height(resTable_ + 'px');
                            var resTablewrapper = $("#resTable_wrapper").height() + 4;
                            $(".Content").height(resTablewrapper + 'px');
                        },
                        "fnInitComplete": function(o) {
                            var resTable_ = $('#resTable').height() + 20;
                            $(".dataTables_scrollBody").height(resTable_ + 'px');
                            var resTablewrapper = $("#resTable_wrapper").height() + 4;
                            $(".Content").height(resTablewrapper + 'px');
                        }
                    });
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }
    </script>

</head>
<body onload="pageLoad();">
    <form id="ResMonitorForm" runat="server">        
    <div class="Page">
        <tv1:Header ID="tvHeader" runat="server" />
        <tv1:MainMenu ID="tvMenu" runat="server" />
        <div class="FilterArea">
            <div>
                <div class="LeftDiv border">
                    <div class="divResNo">
                        <span class="Caption">
                            <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblDueDate")%>
                            : </span>
                        <div style="width: 115px; float: left;">
                            <asp:TextBox ID="fltDueDate" runat="server" Width="90px" />
                            <rjs:PopCalendar ID="ppcDueDate" runat="server" Control="fltDueDate" />
                        </div>
                        <div style="width: 20px; float: left; margin-top: 4px; text-align: center;">
                        </div>
                        <div style="width: 125px; float: left;">
                            <input id="fltIncludeDueDate" type="checkbox" checked="checked" />
                            <label for="fltIncludeDueDate">
                                <%= GetGlobalResourceObject("AgencyPaymentMonitor", "fltIncludeDueDate")%></label>
                        </div>
                    </div>
                    <div class="divResBegDate">
                        <span class="Caption">
                            <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblBeginDate")%>
                            : </span>
                        <div style="width: 115px; float: left; white-space: nowrap;">
                            <asp:TextBox ID="fltBeginDate1" runat="server" Width="90px" />
                            <rjs:PopCalendar ID="ppcBegDate1" runat="server" Control="fltBeginDate1" ClientScriptOnDateChanged="DateChgBegDate" />
                        </div>
                        <div style="width: 20px; float: left; margin-top: 4px; text-align: center;">
                            <span style="font-size: 7pt; font-weight: bold; font-family: Tahoma;">&gt;&gt;</span>
                        </div>
                        <div style="width: 115px; float: left; white-space: nowrap;">
                            <asp:TextBox ID="fltBeginDate2" runat="server" Width="90px" />
                            <rjs:PopCalendar ID="ppcBegDate2" runat="server" Control="fltBeginDate2" ClientScriptOnDateChanged="DateChgBegDateTo" />
                        </div>
                    </div>
                    <div class="divResEndDate">
                        <span class="Caption">
                            <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblSaleDate")%>
                            : </span>
                        <div style="width: 115px; float: left; white-space: nowrap;">
                            <asp:TextBox ID="fltResDate1" runat="server" Width="90px" />
                            <rjs:PopCalendar ID="ppcSaleDate1" runat="server" Control="fltSaleDate1" ClientScriptOnDateChanged="DateChgSaleDate" />
                        </div>
                        <div style="width: 20px; float: left; margin-top: 4px; text-align: center;">
                            <span style="font-size: 7pt; font-weight: bold; font-family: Tahoma;">&gt;&gt;</span>
                        </div>
                        <div style="width: 115px; float: left; white-space: nowrap;">
                            <asp:TextBox ID="fltResDate2" runat="server" Width="90px" />
                            <rjs:PopCalendar ID="ppcSaleDate2" runat="server" Control="fltSaleDate2" ClientScriptOnDateChanged="DateChgSaleDateTo" />
                        </div>
                    </div>
                    <div class="divDepCity">
                        <span class="Caption">
                            <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblDepCity")%>
                            : </span>
                        <div style="width: 300px; float: left; text-align: left;">
                            <select id="fltDepCity" style="width: 95%;">
                            </select>
                        </div>
                    </div>
                    <div class="divArrCity">
                        <span class="Caption">
                            <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblArrCity")%>
                            : </span>
                        <div style="width: 300px; float: left; text-align: left;">
                            <select id="fltArrCity" style="width: 95%;">
                            </select>
                        </div>
                    </div>
                    <%--<div class="divSelect">
                        <div style="margin-left: 3px;">
                            <asp:CheckBox ID="cbShowDraft" runat="server" Text='<%= GetGlobalResourceObject("AgencyPaymentMonitor", "cbShowDraft")%>' />                           
                            <input id="rbOptionControl0" type="radio" name="rbOptionControl" value="0" /><label for="rbOptionControl0"><%= GetGlobalResourceObject("LibraryResource", "ReservationAll")%></label>&nbsp;&nbsp;
                            <input id="rbOptionControl1" type="radio" name="rbOptionControl" value="1" /><label for="rbOptionControl0"><%= GetGlobalResourceObject("LibraryResource", "ReservationOptional")%></label>
                        </div>
                    </div>--%>
                    <div id="divSum" align="center">
                    </div>
                </div>
                <div class="RightDiv border">
                    <div class="divAgencyOfficeAgencyUser">
                        <div class="divAgencyOffice">
                            <span class="Caption">
                                <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblAgencyOffice")%>
                                : </span>
                            <br />
                            <select id="fltAgencyOffice" style="width: 200px;">
                            </select>
                        </div>
                        <div class="divAgencyUser">
                            <span class="Caption">
                                <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblAgencyUser")%>
                                : </span>
                            <br />
                            <select id="fltAgencyUser" style="width: 200px;">
                            </select>
                        </div>
                    </div>
                    <div class="divLeader">
                        <div class="divSurname">
                            <span class="Caption">
                                <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblSurname")%>
                                : </span>
                            <br />
                            <asp:TextBox ID="fltSurname" runat="server" Width="200px" />
                        </div>
                        <div class="divName">
                            <span class="Caption">
                                <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblName")%>
                                : </span>
                            <br />
                            <asp:TextBox ID="fltName" runat="server" Width="200px" />
                        </div>
                    </div>
                    <div class="divStatus">
                        <table>
                            <tr>
                                <td>
                                    <span class="Caption">
                                        <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblResStatus")%>
                                        :</span>
                                </td>
                                <td>
                                    <span class="Caption">
                                        <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblContStatus")%>
                                        :</span>
                                </td>
                                <td>
                                    <span class="Caption">
                                        <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblPayStatus")%>
                                        :</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="fltResStatus0" name="fltResStatus" type="checkbox" value="0" /><label
                                        for="fltStatus0"><%= GetGlobalResourceObject("LibraryResource", "ResStatus0")%></label><br />
                                    <input id="fltResStatus1" name="fltResStatus" type="checkbox" value="1" /><label
                                        for="fltStatus1"><%= GetGlobalResourceObject("LibraryResource", "ResStatus1")%></label><br />
                                    <input id="fltResStatus2" name="fltResStatus" type="checkbox" value="2" /><label
                                        for="fltStatus2"><%= GetGlobalResourceObject("LibraryResource", "ResStatus2")%></label><br />
                                    <input id="fltResStatus3" name="fltResStatus" type="checkbox" value="3" /><label
                                        for="fltStatus3"><%= GetGlobalResourceObject("LibraryResource", "ResStatus3")%></label>
                                </td>
                                <td>
                                    <input id="fltConfirmStatus0" name="fltConfirmStatus" type="checkbox" value="0" /><label
                                        for="fltConfirmStatus0"><%= GetGlobalResourceObject("LibraryResource", "ConfStatus0")%></label><br />
                                    <input id="fltConfirmStatus1" name="fltConfirmStatus" type="checkbox" value="1" /><label
                                        for="fltConfirmStatus1"><%= GetGlobalResourceObject("LibraryResource", "ConfStatus1")%></label><br />
                                    <input id="fltConfirmStatus2" name="fltConfirmStatus" type="checkbox" value="2" /><label
                                        for="fltConfirmStatus2"><%= GetGlobalResourceObject("LibraryResource", "ConfStatus2")%></label><br />
                                    <input id="fltConfirmStatus3" name="fltConfirmStatus" type="checkbox" value="3" /><label
                                        for="fltConfirmStatus3"><%= GetGlobalResourceObject("LibraryResource", "ConfStatus3")%></label>
                                </td>
                                <td>
                                    <input id="fltPaymentStatus1" name="fltPaymentStatus" type="checkbox" value="1" /><label
                                        for="fltPaymentStatus1"><%= GetGlobalResourceObject("LibraryResource", "fltBalance1")%></label><br />
                                    <input id="fltPaymentStatus2" name="fltPaymentStatus" type="checkbox" value="2" /><label
                                        for="fltPaymentStatus2"><%= GetGlobalResourceObject("LibraryResource", "fltBalance2")%></label><br />
                                    <input id="fltPaymentStatus3" name="fltPaymentStatus" type="checkbox" value="3" /><label
                                        for="fltPaymentStatus3"><%= GetGlobalResourceObject("LibraryResource", "fltBalance3")%></label><br />
                                    <input id="fltPaymentStatus4" name="fltPaymentStatus" type="checkbox" value="4" /><label
                                        for="fltPaymentStatus4"><%= GetGlobalResourceObject("LibraryResource", "fltBalance4")%></label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="divFilterButton">
                <table>
                    <tr>
                        <td>
                            <input type="button" id="btnFilter" value='<%= GetGlobalResourceObject("AgencyPaymentMonitor", "btnFilter")%>'
                                style="width: 140px;" onclick="btnFilterClick();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                        </td>
                        <td>
                            <input type="button" id="btnfilterclear" value='<%= GetGlobalResourceObject("AgencyPaymentMonitor", "btnfilterclear")%>'
                                style="width: 140px;" onclick="createFilterData(true);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="Content">
            <div class="dataTables_scroll">
                <div id="resMonitorGrid" class="dataTables_scrollBody" style="width: 100%;">                    
                </div>
            </div>
        </div>
        <div class="Footer">
            <tv1:Footer ID="tvfooter" runat="server" />
        </div>
    </div>
    <div id="dialog-paymentPlan" title="Payment Plan" style="display: none;">
        <span id="paymentPlan"></span>
    </div>
    <div id="dialog-resComment" title="Reservation Comment" style="display: none;">
        <resCom1:ResComment ID="resComment" runat="server" />
    </div>
    <div id="dialog-message" title="" style="display: none;">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;">
            </span><span id="messages">
        Message
    </div>
    <div id="confirm">
        <div class="header">
            <span>
                <%= GetGlobalResourceObject("LibraryResource", "msgConfirm")%></span></div>
        <p class="message">
        </p>
        <div class="buttons">
            <div class="no simplemodal-close">
                <%= GetGlobalResourceObject("LibraryResource", "btnNo")%></div>
            <div class="yes">
                <%= GetGlobalResourceObject("LibraryResource", "btnYes")%></div>
        </div>
    </div>
    <div id="dialog-viewReport" title='' style="display: none;">
        <iframe id="viewReport" runat="server" height="100%" width="100%" frameborder="0"
            style="clear: both;"></iframe>
    </div>
    <div id="waitMessage" style="display: none;">
        <img alt="" title="" src="Images/Wait.gif" />
    </div>
    <asp:HiddenField ID="pdfConvSerial" runat="server" Value="RG92ZHVkdXF0cGRxanRkd3VqdXZqfX19fQ==" />
    </form>
</body>
</html>
