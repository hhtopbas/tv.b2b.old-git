﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Promotion.aspx.cs" Inherits="Promotion" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "Promotion") %></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

    <script src="Scripts/jquery.linq.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/Promotion.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        if (typeof window.event != 'undefined')
            document.onkeydown = function() {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function(e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function logout() {
            self.parent.logout();
        }

        function getPageData() {
            $.ajax({
                type: "POST",
                url: "Promotion.aspx/getPromoList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#promoGrid").html('');
                    $("#promoGrid").html(msg.d);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function exit(source) {
            var selectedPromo = '';
            if (source == 'save') {
                var selectPromo = $("input[name=promoid]:checked");
                $.each(selectPromo, function(i) {
                    if (selectedPromo.length > 0) selectedPromo += ';';
                    selectedPromo += this.value;
                });
                window.close;
                self.parent.returnSelectedPromotion(selectedPromo);
            }
            else {
                window.close;
                self.parent.returnSelectedPromotion(selectedPromo);
            }
        }

        function noValidWithPromo(rec, list, custNo) {
            //  with OpenQry(Format('Select RecID from #Promos Where Sel=1 and RecID<>%0:s and '+
            //    ' (Case When %2:d in (2,3) then 0 
            //            When %2:d in (0,1) then CustNo else 0 end=%1:d or CustNo is null)',
            //    [PromosQryRecID.Text,PromosQryCustNo.Value, PromosQryPromoType.Value]) ) do
            var retVal = true;

            var queryResult = $.Enumerable.From(list)
                               .Where(function(x) {
                                   return x.Sel &&
                                          x.RecID != rec.RecID &&
                                          (((x.PromoType == 2 || x.PromoType == 3) ? 0 : ((x.PromoType == 0 || x.PromoType == 1) ? x.CustNo : 0) == custNo) || x.CustNo == null)
                               });
            if (queryResult.Count() > 0) return false;
            else return true;
            //            $.each(list, function(i) {
            //                var validWithPromo = true;
            //                if (this.ValidWithPromo != null) validWithPromo = this.ValidWithPromo;
            //                var custCont = true;

            //                custCont = ((this.PromoType == 2 || this.PromoType == 3) ? 0 : ((this.PromoType == 0 || this.PromoType == 1) ? this.CustNo : 0) == custNo) || this.CustNo == null;

            //                if (this.Sel && !this.ValidWithPromo && this.RecID != rec.RecID && custCont) {
            //                    retVal = false;
            //                    return;
            //                }
            //            });
            //            return retVal;
        }


        function yesValidWithPromo(rec, list, custNo) {
            var retVal = true;
            var validWithPromo = true;
            if (rec.ValidWithPromo != null) validWithPromo = rec.ValidWithPromo;

            var queryResult = $.Enumerable.From(list)
                               .Where(function(x) { return x.Sel && x.RecID != rec.RecID && !validWithPromo });
            if (queryResult.Count() > 0) return false;
            else return true;

            //            $.each(list, function(i) {                               
            //                if (this.Sel && !validWithPromo && this.RecID != rec.RecID) {
            //                    retVal = false;
            //                    return;
            //                }
            //            });
            //            return retVal;
        }


        function selectPromotion(seqId) {

            var selectPromo = $("input[name=promoid]");
            var slctdPromo = '';
            $.each(selectPromo, function(i) {
                if (slctdPromo.length > 0) slctdPromo += ';';
                slctdPromo += this.value;
            });

            var selectedPromo = $("#select" + seqId);
            var listPromo = $.json.decode($('#hfPromoList').val().replace(/!/g, '"'));
            if (selectedPromo[0].checked) {
                $.each(listPromo, function(i) {
                    if (selectedPromo.val() == this.RecID) {
                        var validWithPromo = true;
                        if (this.ValidWithPromo != null) validWithPromo = this.ValidWithPromo;
                        var custNo = selectedPromo.attr("custNo") != '' ? parseInt(selectedPromo.attr("custNo")) : null

                        this.Sel = true;
                        this.Exist = true;

                        if (this.ValidWithPromo == false) {
                            if (!noValidWithPromo(this, listPromo, custNo)) {
                                selectedPromo[0].checked = false;
                                this.Sel = false;
                                this.Exist = false;
                                return;
                            }
                        }
                        else {
                            if (!yesValidWithPromo(this, listPromo, custNo)) {
                                selectedPromo[0].checked = false;
                                this.Sel = false;
                                this.Exist = false;
                                return;
                            }
                        }
                    }
                });
            }
            else {
                $.each(listPromo, function(i) {
                    if (selectedPromo.val() == this.RecID) {
                        selectedPromo[0].checked = false;
                        this.Sel = false;
                        this.Exist = false;
                    }
                });
            }
            $('#hfPromoList').val($.json.encode(listPromo).replace(/"/g, '!'));
        }

        function pageLoad() {
            getPageData();
        }

    </script>

</head>
<body onload="javascript:pageLoad();">
    <form id="PromotionForm" runat="server">
    <div id="promoGrid">
    </div>
    <div class="btnDiv">
        <input id="btnSave" type="button" value="OK" onclick="exit('save');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />&nbsp;&nbsp;
        <input id="btnCancel" type="button" value="Cancel" onclick="exit('cancel');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
    </div>
    </form>
</body>
</html>
