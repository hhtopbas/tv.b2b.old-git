﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReservationRFWizard.aspx.cs" Inherits="ReservationRFWizard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="cache-control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  <title>Reservation Request Form</title>

  <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />
  <script src="Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
  <script src="Scripts/jquery.dialogextend.js" type="text/javascript"></script>
  <script src="Scripts/json2.js" type="text/javascript"></script>
  <script src="Scripts/mustache.js" type="text/javascript"></script>
  <script src="Scripts/ui.multiselect.js" type="text/javascript"></script>
  <script src="Scripts/jquery.tmpl.1.1.1.js" type="text/javascript"></script>
  <script src="Scripts/jquery.maskedinput.js" type="text/javascript"></script>
  <script src="Scripts/TVTools.DateFunctions.js" type="text/javascript"></script>

  <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="CSS/main.css" rel="stylesheet" type="text/css" />
  <link href="CSS/ui.multiselect.css?v=1" rel="stylesheet" type="text/css" />
  <link href="CSS/ReservationRFWizard.css?v=7" rel="Stylesheet" type="text/css" />

  <style type="text/css">
    </style>

  <script language="javascript" type="text/javascript">

    var culture = '<%= twoLetterISOLanguageName %>';

    var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
    var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
    var btnClose = '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>';
    var btnGotoResCard = '<%= GetGlobalResourceObject("LibraryResource", "btnGotoResCard") %>';
    var btnPaymentPage = '<%= GetGlobalResourceObject("LibraryResource", "btnPaymentPage") %>';
    var btnYes = '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>';
    var btnNo = '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>';

    var msgWhereInvoiceTo = '<%= GetGlobalResourceObject("LibraryResource", "WhereInvoiceTo") %>';
    var msglblAlreadyRes = '<%= GetGlobalResourceObject("BookTicket", "lblAlreadyRes") %>';
    var msgEnterChildOrInfantBirtDay = '<%= GetGlobalResourceObject("LibraryResource", "EnterChildOrInfantBirtDay") %>';
    var msglblNoResult = '<%= GetGlobalResourceObject("BookTicket", "lblNoResult") %>';
    var msgviewOldResSalePrice = '<%= GetGlobalResourceObject("Controls", "viewOldResSalePrice") %>';
    var msgviewNewResSalePrice = '<%= GetGlobalResourceObject("Controls", "viewNewResSalePrice") %>';
    var msgcancelService = '<%= GetGlobalResourceObject("LibraryResource", "cancelService") %>';
    var ComboSelect = '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
    var ForAgency = '<%=GetGlobalResourceObject("LibraryResource", "ForAgency") %>';
    var ForPassenger = '<%=GetGlobalResourceObject("LibraryResource", "ForPassenger") %>';
    var msglblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
    var msgInvalidBirthDate = '<%= GetGlobalResourceObject("LibraryResource","InvalidBirthDate").ToString() %>';

    var NS = document.all;

    if (typeof window.event != 'undefined')
      document.onkeydown = function () {
        if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
          return (event.keyCode != 8);
      }
    else
      document.onkeypress = function (e) {
        if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
          return (e.keyCode != 8);
      }

    $(document).ajaxStart(function () {
      $.blockUI({
        message: '<h1>' + msglblPleaseWait + '</h1>'
      });
    }).ajaxStop(function () {
      $.unblockUI();
      return false;
    });

    function showMessage(msg) {
      $("#messages").html(msg);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        modal: true,
        buttons: [{
          text: btnClose,
          click: function () {
            $(this).dialog('close');
          }
        }]
      });
    }

    function saveWizard() {
      self.close();
      self.parent.saveWizard();
    }

    function saveChangeCustomers(recID) {
      var selectedCust = $("#customerList").multiselect('selectedValues');
      var params = new Object();
      params.RecID = parseInt(recID);
      params.SelectedCust = selectedCust;
      $.ajax({
        type: "POST",
        url: "ReservationRFWizard.aspx/setHotelRoomCustomers",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var data = msg.d;
            /*
setCust 
roomOKList 
roomOK 
            */
            if (data.roomOK) {
              $(".saveWizardBtn").show();
            }
            else {
              $(".saveWizardBtn").hide();
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function showHotelRoomCustomers(tmpHtml, RecID, Hotel, Room, Board, Accom) {
      $(".HotelServiceRoomCustomers").html('');
      var params = new Object();
      params.RecID = parseInt(RecID);
      params.Hotel = Hotel;
      params.Room = Room;
      params.Board = Board;
      params.Accom = Accom;
      $.ajax({
        type: "POST",
        url: "ReservationRFWizard.aspx/getHotelRoomCustomers",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var data = msg.d;
            var html = Mustache.render(tmpHtml, data);
            $(".HotelServiceRoomCustomers").html(html);            
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function selectHotelRoom(RecID, Hotel, Room, Board, Accom) {
      $.get($("#tmplPath").val() + 'RRFWizardHotelRoomCustomers.html?v=201602161735', function (html) {
        showHotelRoomCustomers(html, RecID, Hotel, Room, Board, Accom);
      }).fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        showAlert("Request Failed: " + err);
      });
    }

    function removeRoom(_this) {
      var parentDiv = $(_this).parent().parent();
      $(parentDiv).remove();
      $("#btnAddRoom").show();
    }

    function addRoom(hotelCode, recId) {
      if ($("#iRoom").val() == '' || $("#iAccom").val() == '' || $("#iBoard").val() == '') {
        showMessage("Please select room defination.");
        return false;
      }
      var params = new Object();
      params.Hotel = hotelCode;
      params.RecID = parseInt(recId);
      params.Room = $("#iRoom").val();
      params.Accom = $("#iAccom").val();
      params.Board = $("#iBoard").val();
      params.Unit = $("#iUnit").val();
      $.ajax({
        type: "POST",
        url: "ReservationRFWizard.aspx/addHotelRooms",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null && msg.d == true) {
            $(".HotelServiceRooms").html('');
            $(".HotelServiceRoomCustomers").html('');

            getFormData(hotelCode, recId);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function addHotelRoom(hotelCode, seqNo) {
      var addRow = '<div class="ui-helper-clearfix row">';
      addRow += '<div class="srvHotelRoomSeqNo"><img src="Images/ok.png" alt="" onclick="addRoom(\'' + hotelCode + '\',' + seqNo + ');"/></div>';
      addRow += '<div class="srvHotelRoomSelect"><img src="Images/cancel.png" alt="" onclick="removeRoom(this);"/></div>';
      addRow += '<div class="srvHotelRoomRoom"><select id="iRoom"></select></div>';
      addRow += '<div class="srvHotelRoomAccom"><select id="iAccom"></select></div>';
      addRow += '<div class="srvHotelRoomBoard"><select id="iBoard"></select></div>';
      addRow += '<div class="srvHotelRoomUnit"><input id="iUnit" /></div>';
      addRow += '</div>';
      $(".HotelServiceRooms").append(addRow);
      $("#btnAddRoom").hide();
      if ($(".HotelServiceRooms").data("Room") != null) {
        $("#iRoom").html('');
        $("#iRoom").append('<option value=""> -- </option>');
        $.each($(".HotelServiceRooms").data("Room"), function (i) {
          $("#iRoom").append('<option value="' + this.Code + '">' + this.Name + '</option>');
        });
      }
      if ($(".HotelServiceRooms").data("Accom") != null) {
        $("#iAccom").html('');
        $("#iAccom").append('<option value=""> -- </option>');
        $.each($(".HotelServiceRooms").data("Accom"), function (i) {
          $("#iAccom").append('<option value="' + this.Code + '">' + this.Name + '</option>');
        });
      }
      if ($(".HotelServiceRooms").data("Board") != null) {
        $("#iBoard").html('');
        $("#iBoard").append('<option value=""> -- </option>');
        $.each($(".HotelServiceRooms").data("Board"), function (i) {
          $("#iBoard").append('<option value="' + this.Code + '">' + this.Name + '</option>');
        });
      }
    }

    function showHotelRooms(tmpHtml, hotel, recID) {
      $(".HotelServiceRooms").removeData("Room");
      $(".HotelServiceRooms").removeData("Accom");
      $(".HotelServiceRooms").removeData("Board");
      var params = new Object();
      params.Hotel = hotel;
      params.RecID = parseInt(recID);
      $.ajax({
        type: "POST",
        url: "ReservationRFWizard.aspx/getHotelRooms",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var data = msg.d;
            var html = Mustache.render(tmpHtml, data.Rooms);
            $(".HotelServiceRooms").html(html);
            $(".HotelServiceRooms").data("Room", data.HotelRoomList);
            $(".HotelServiceRooms").data("Accom", data.HotelAccomList);
            $(".HotelServiceRooms").data("Board", data.HotelBoardList);
            $(".HotelServiceRoomCustomers").html('');
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function selectedHotelShowRoom(hotel, recID) {
      $.get($("#tmplPath").val() + 'RRFWizardHotelRooms.html?v=201602161735', function (html) {
        showHotelRooms(html, hotel, recID);
      }).fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        showAlert("Request Failed: " + err);
      });
    }

    function selectHotel(hotel, recID) {
      $("img[name='selected']").attr("src", "Images/nochecked_16.gif");
      var selected = $("#selected_" + recID);
      if (selected.attr("src").indexOf("Images/nochecked_16.gif") > -1) {
        selected.attr("src", selected.attr("src").replace("Images/nochecked_16.gif", "Images/checked_16.gif"));
        selectedHotelShowRoom(hotel, recID);
      }
      else {
        selected.attr("src", selected.attr("src").replace("Images/checked_16.gif", "Images/nochecked_16.gif"));
      }
    }

    function showBaseHotels(tmpHtml, hotel, recID) {
      $(".HotelServices").removeData('data');
      $.ajax({
        type: "POST",
        url: "ReservationRFWizard.aspx/getFormData",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var data = msg.d;
            $(".HotelServices").data('data', data);

            $("#iResBegDate").html(data.ResBegDate);
            $("#iResEndDate").html(data.ResEndDate);
            $("#iResNight").html(data.ResNight);
            $("#iResPax").html(data.ResPax);

            var html = Mustache.render(tmpHtml, data);
            $(".HotelServices").html(html);
            if (hotel != null && recID != null) {
              selectHotel(hotel, recID);
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function AddHotel() {
      var data = $(".HotelServices").data('data');
      var params = new Object();
      params.Hotel = $("#iHotel").val();
      params.BegDate = $("#iBegDate").val();
      params.EndDate = $("#iEndDate").val();
      params.Night = $("#iNight").val();
      params.DateTimeFormat = data.DateTimeFormat.replace(/m/g, 'M');
      if (params.Hotel == '' || params.BegDate == '' || params.EndDate == '' || params.Night == '') {
        showMessage("Please, fill all data.");
        return;
      }
      $.ajax({
        type: "POST",
        url: "ReservationRFWizard.aspx/AddHotel",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            if (msg.d.OK) {
              $("#HotelWizardAddHotel").show();
              $("#addHotelWizardRow").remove();
              getFormData(null, null);
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function CancelAddHotel() {
      $("#HotelWizardAddHotel").show();
      $("#addHotelWizardRow").remove();
    }

    function onClickHotelWizardAddHotel() {
      try {
        $("#iBegDate").unmask();
        $("#iEndDate").unmask();
        $("#iNight").unmask();
      } catch (e) {
      }
      $("#HotelWizardAddHotel").hide();
      var data = $(".HotelServices").data('data');
      var params = new Object();
      params.DateTimeFormat = data.DateTimeFormat.replace(/m/g, 'M');
      $.ajax({
        type: "POST",
        url: "ReservationRFWizard.aspx/wizardAddHotel",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var addRow = '<div id="addHotelWizardRow" class="ui-helper-clearfix row">';
            addRow += '<div class="srvAddHotelSeqNo"><img src="Images/ok.png" alt="" onclick="AddHotel()"/></div>';
            addRow += '<div class="srvAddHotelSelect"><img src="Images/cancel.png" alt="" onclick="CancelAddHotel(this)"/></div>';
            var hotels = '';
            $.each(msg.d.HotelList, function (i) {
              hotels += '<option value="' + this.Code + '">' + this.Name + '</option>';
            });
            addRow += '<div class="srvAddHotelDescription"><select id="iHotel">' + hotels + '</select></div>';
            addRow += '<div class="srvAddHotelUnit">1</div>';
            addRow += '<div class="srvAddHotelCheckIn"><input name="hotelNight" id="iBegDate" value="' + msg.d.BegDate + '" /></div>';
            addRow += '<div class="srvAddHotelCheckOut"><input name="hotelNight" id="iEndDate" value="" /></div>';
            addRow += '<div class="srvAddHotelNight"><input name="hotelNight" id="iNight" value="" /></div>';
            addRow += '</div>';
            $(".HotelServices").append(addRow);
            $("#iBegDate").mask(data.DateMaskFormat, { placeholder: data.DateTimeFormat });
            $("#iEndDate").mask(data.DateMaskFormat, { placeholder: data.DateTimeFormat });
            $('input[name=hotelNight]').on("blur", function () {
              var checkIn = $("#iBegDate").val();
              var checkOut = $("#iEndDate").val();
              var night = $("#iNight").val();
              if (checkIn != '' && checkOut != '') {
                var begDate = getDateFromFormat(checkIn, data.DateTimeFormat.replace(/m/g, 'M'));
                var endDate = getDateFromFormat(checkOut, data.DateTimeFormat.replace(/m/g, 'M'));
                $("#iNight").val(dateiff(begDate, endDate));
              } else {
                if (night != '' && checkIn != '') {
                  var begDate = getDateFromFormat(checkIn, data.DateTimeFormat.replace(/m/g, 'M'));
                  var endDate = null;
                  endDate = begDate.setDate(begDate.getDate() + parseInt(night));
                  $("#iEndDate").val(formatDate(new Date(endDate), data.DateTimeFormat.replace(/m/g, 'M')));
                } else if (night != '' && checkOut != '') {
                  var begDate = null;
                  var endDate = getDateFromFormat(checkOut, data.DateTimeFormat.replace(/m/g, 'M'));
                  begDate = endDate.setDate(endDate.getDate() + parseInt(night));
                  $("#iBegDate").val(formatDate(new Date(begDate), data.DateTimeFormat.replace(/m/g, 'M')));
                }
              }
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getFormData(hotel, recID) {
      $.get($("#tmplPath").val() + 'RRFWizardHotels.html?v=201602111630', function (html) {
        showBaseHotels(html, hotel, recID);
      }).fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        showAlert("Request Failed: " + err);
      });
    }

    $(document).ready(function () {
      $(".HotelServices").removeData('data');
      getFormData(null, null);
    });
  </script>

</head>
<body>
  <form id="form1" runat="server">
    <div class="RRForm">
      <div class="ui-helper-clearfix ResHeader">
        <div class="BegDate">
          <strong>Begin Date</strong><br />
          <span id="iResBegDate"></span>
        </div>
        <div class="EndDate">
          <strong>End Date</strong><br />
          <span id="iResEndDate"></span>
        </div>
        <div class="Night">
          <strong>Night</strong><br />
          <span id="iResNight"></span>
        </div>
        <div class="Pax">
          <strong>Total Pax</strong><br />
          <span id="iResPax"></span>
        </div>
      </div>
      <div class="ui-helper-clearfix HotelServices">
        test
      </div>
      <div class="ui-helper-clearfix HotelWizardButtons">
        <input id="HotelWizardAddHotel" type="button" value="Add Hotel" onclick="onClickHotelWizardAddHotel();" />
      </div>
      <div class="ui-helper-clearfix HotelServiceRooms">
      </div>            
      <div class="ui-helper-clearfix saveWizardBtn">
        <input id="btnSaveWizard" type="button" value="Save Wizard" onclick="saveWizard()" />
      </div>
    </div>
    <div class="HotelServiceRoomCustomers">
    </div>
    <div class="ui-helper-hidden">
      <asp:HiddenField ID="tmplPath" runat="server" />
    </div>
  </form>

  <div id="dialog-message" title="" class="ui-helper-hidden">
    <span id="messages"></span>
  </div>
</body>
</html>
