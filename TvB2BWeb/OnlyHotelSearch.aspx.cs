﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Text;
using System.Web.Services;
using TvTools;
using System.IO;
using BankIntegration;
using System.Web.Script.Services;
using Winnovative;
using System.Configuration;

namespace TvSearch
{
    public partial class OnlyHotelSearch : BasePage
    {       
        protected void Page_Load(object sender, EventArgs e)
        {
            User UserData = (User)Session["UserData"];            
            //Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url);  
            if (Session["SearchPLData"] == null)
            {
                bool GroupSearch = false;
                object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
                if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
                string b2bMenuCatStr = string.IsNullOrEmpty(Request.Params["B2bMenuCat"]) ? string.Empty : (string)Request.Params["B2bMenuCat"];
                SearchPLData filterData = CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.OnlyHotelSearch, null, b2bMenuCatStr);
                Session["SearchPLData"] = filterData;
            }

            IsPostBackBefore(UserData);
            if (!IsPostBack)
                IsPostBackInside(sender, e, UserData);
            IsPosBackAfter(UserData);
        }

        void IsPostBackBefore(User UserData)
        {
            CultureInfo ci = UserData.Ci;
            Thread.CurrentThread.CurrentCulture = ci;            
        }

        void IsPostBackInside(object sender, EventArgs e, User UserData)
        {            
        }

        void IsPosBackAfter(User UserData)
        {            
        }        
                                     
        [WebMethod]
        public static string UserHasAuth()
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            return UserData.BlackList ? "N" : "Y";            
        }        
      
        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
        public static PackageSearchMakeRes getBookReservation(string BookList)
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            object extAllotControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ExtAllotControl");
            bool ExtAllotCont = Conversion.getBoolOrNull(extAllotControl).HasValue ? Conversion.getBoolOrNull(extAllotControl).Value : false;
            
            object _checkAvailableRoom = new TvBo.Common().getFormConfigValue("SearchPanel", "CheckAvailableRoom");
            bool checkAvailableRoom = Conversion.getBoolOrNull(_checkAvailableRoom).HasValue ? Conversion.getBoolOrNull(_checkAvailableRoom).Value : false;

            string version = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "Version"));
            string onlyHotelVersion = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "OnlyHotelVersion"));

            HttpContext.Current.Session["ResData"] = null;            
            TvBo.SearchCriteria criteria = (TvBo.SearchCriteria)HttpContext.Current.Session["Criteria"];
            List<int> _bookList = new List<int>();
            for (int i = 0; i < BookList.Split(',').Length; i++)
                _bookList.Add(Convert.ToInt32(BookList.Split(',')[i]));
            ResDataRecord ResData = new ResDataRecord();
            ResData.SelectBook = new List<SearchResult>();
            MultiRoomResult searchResult = (MultiRoomResult)HttpContext.Current.Session["SearchResult"];
            var query = from q1 in searchResult.ResultSearch
                        join q2 in _bookList on q1.RefNo equals q2
                        select q1;
            foreach (int row in _bookList)
            {
                SearchResult sr = new SearchResult();
                SearchCriteriaRooms sc = new SearchCriteriaRooms();                
                sr = searchResult.ResultSearch.Find(f => f.RefNo == row);
                sc = criteria.RoomsInfo.Find(f => f.RoomNr == sr.RoomNr);
                sr.Child1Age = sc.Chd1Age;
                sr.Child2Age = sc.Chd2Age;
                sr.Child3Age = sc.Chd3Age;
                sr.Child4Age = sc.Chd4Age;         
                ResData.SelectBook.Add(sr);
            }
            string errorMsg = string.Empty;

            Guid? logID = null;
            String useLog = System.Configuration.ConfigurationManager.AppSettings["useLog"];
            if (!string.IsNullOrEmpty(useLog) && string.Equals(useLog, "1"))
            {
                if (ResData.SelectBook.FirstOrDefault().LogID.HasValue)
                    logID = new WEBLog().saveWEBBookLog(ResData.SelectBook.FirstOrDefault().LogID.Value, DateTime.Now, null, ref errorMsg);
            }

            int totalPax = ResData.SelectBook.Sum(s => (s.HAdult + s.HChdAgeG2 + s.HChdAgeG3 + s.HChdAgeG4 + (s.ChdG1Age2.HasValue && s.ChdG1Age2.Value > (decimal)(199 / 100) ? s.HChdAgeG1 : 0)));
            int maxTotalPax = UserData.AgencyRec.MaxPaxCnt.HasValue ? UserData.AgencyRec.MaxPaxCnt.Value : (UserData.TvParams.TvParamReser.MaxPaxCnt.HasValue ? UserData.TvParams.TvParamReser.MaxPaxCnt.Value : 10);
            if (maxTotalPax < totalPax)
            {
                string msg = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "MaxPaxReservation").ToString(), maxTotalPax.ToString());
                return new PackageSearchMakeRes { resOK = false, errMsg = msg, version = version }; 
            }
            if (!ExtAllotCont)
                if (!(new Hotels().AllotmentControlForMultipleRoom(UserData, ResData.SelectBook, "O", ref errorMsg)))
                    return new PackageSearchMakeRes { resOK = false, errMsg = errorMsg, version = version };
            if (string.Equals(onlyHotelVersion, "V2"))
                ResData = new Reservation().getResDataV2(UserData, ResData, criteria, ref errorMsg);
            else ResData = new Reservation().getResData(UserData, ResData, criteria, SearchType.OnlyHotelSearch, false, ref errorMsg);

            ResData.FirstData = new ResTables().getFirstResData(ResData);
            ResData.FirstData.ReservationType = SearchType.OnlyHotelSearch;

            if (!string.IsNullOrEmpty(errorMsg))
                return new PackageSearchMakeRes { resOK = false, errMsg = errorMsg, version = version }; 

            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            {
                ResMainRecord resMain = ResData.ResMain;
                resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
            }
            
            if (!string.Equals(onlyHotelVersion, "V2") && string.Equals(version, "V2"))
                ResData = new ReservationV2().getResDataExtras(UserData, ResData, ref errorMsg);
            
            if (ResData.ExtrasData == null)
                ResData.ExtrasData = ResData;
            
            ResData.LogID = logID;

            bool CreateChildAge = false;
            if (UserData.WebService)
                CreateChildAge = true;
            else
            {
                object _createChildAge = new TvBo.Common().getFormConfigValue("MakeReservation", "CreateChildAge");
                CreateChildAge = _createChildAge != null ? (bool)_createChildAge : false;
            }

            if (!CreateChildAge)
            {
                foreach (ResCustRecord row in ResData.ResCust)
                {
                    row.Age = null;
                    row.Birtday = null;
                }
                foreach (ResCustRecord row in ResData.ExtrasData.ResCust)
                {
                    row.Age = null;
                    row.Birtday = null;
                }
            }

            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_SunFun) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka))
            {
                ResMainRecord resMain = ResData.ResMain;
                resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
            }

            HttpContext.Current.Session["ResData"] = ResData;

            return new PackageSearchMakeRes { resOK = true, errMsg = string.Empty, version = version }; 
        }

        [WebMethod(EnableSession = true)]
        public static string viewPDFReport(string reportType, string _html, string pageWidth, string urlBase)
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci;
            Thread.CurrentThread.CurrentUICulture = UserData.Ci;
            _html = _html.Replace('|', '"');
            _html = _html.Replace("<div id=\"noPrintDiv\">", "<div id=\"noPrintDiv\" style=\"display: none; visibility: hidden;\">");
            string errorMsg = string.Empty;
            string basePageUrl = WebRoot.BasePageRoot;
            string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
            string pdfFileName = reportType + "_" + UserData.AgencyID + ".pdf";
            StringBuilder html = new StringBuilder(_html);
            int PageWidth = Conversion.getInt32OrNull(pageWidth).HasValue ? Conversion.getInt32OrNull(pageWidth).Value : 700;
            //initialize the PdfConvert object
            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.NoCompression;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;            
            pdfConverter.PdfDocumentOptions.LeftMargin = 10;
            pdfConverter.PdfDocumentOptions.TopMargin = 10;
            // set the demo license key
            pdfConverter.LicenseKey = ConfigurationManager.AppSettings["Winnovative"];;

            // get the base url for string conversion which is the url from where the html code was retrieved
            // the base url is a hint for the converter to find the external CSS and images referenced by relative URLs
            string thisPageURL = HttpContext.Current.Request.Url.AbsoluteUri;
            string baseUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/";
            try
            {
                pdfConverter.SavePdfFromHtmlStringToFile(html.ToString(), siteFolderISS + "ACE\\" + pdfFileName, urlBase);
                string returnUrl = basePageUrl + "ACE/" + pdfFileName;
                return returnUrl;
            }
            catch
            {
                return "";
            }
        }

        [WebMethod(EnableSession = true)]
        public static string getBrochure(string holPack, long? CheckIn, long? CheckOut, string Market)
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            DateTime begDate = CheckIn.HasValue ? new DateTime(CheckIn.Value) : DateTime.Today;
            DateTime endDate = CheckOut.HasValue ? new DateTime(CheckOut.Value) : DateTime.Today;

            string errorMsg = string.Empty;
            HolPackBrochureRecord holPackBrochure = new TvBo.Common().getHolPackBrochure(Market, holPack, begDate, endDate, ref errorMsg);
            if (holPackBrochure != null)
            {
                string fileName = holPack + "_" + CheckIn.ToString() + "_" + CheckOut.ToString() + ".PDF";
                if (File.Exists(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName))
                    File.Delete(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName);
                FileStream fileStream = new FileStream(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName, FileMode.Create, FileAccess.ReadWrite);
                fileStream.Write((byte[])holPackBrochure.FileImage, 0, ((byte[])holPackBrochure.FileImage).Length);
                fileStream.Close();

                return WebRoot.BasePageRoot + "/ACE/" + fileName;
            }
            else return "";

        }
        [WebMethod(EnableSession = true)]

        public static string getOffersUrl()
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            object _ppd = new TvBo.Common().getFormConfigValue("General", "b2cOffersUrl");
            List<BIPaymentPageDefination> ppd = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BIPaymentPageDefination>>(Conversion.getStrOrNull(_ppd));
            if (ppd != null)
            {
                BIPaymentPageDefination offersUrl = ppd.Find(w => w.Market == UserData.Market && string.Equals(w.Code, "PACK"));
                return offersUrl != null ? offersUrl.B2CUrl : string.Empty;
            }
            else
                return string.Empty;

        }
    }

}