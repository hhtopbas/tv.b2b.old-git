﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Threading;
using TvBo;
using System.Web.Services;
using System.Text;
using TvTools;

public partial class AgencyCommisionReport : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        string datePatern = new TvBo.Common().getDateFormat(UserData.Ci);

        ppcBegDate.Culture = ci.Name + " " + ci.EnglishName;
        ppcBegDate.Format = datePatern.Replace(ci.DateTimeFormat.DateSeparator[0], ' ');
        ppcEndDate.Culture = ci.Name + " " + ci.EnglishName;
        ppcEndDate.Format = datePatern.Replace(ci.DateTimeFormat.DateSeparator[0], ' ');
        if (!IsPostBack)
        {
            string errorMsg = string.Empty;

            Session["Menu"] = Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url);//"AgencyCommisionReport.aspx";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string GetAgencyList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = "";
        List<CodeName> agencyList = new TvBo.Common().getAgencyCommisionReportAgencyList(UserData.AgencyID, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(agencyList);
    }

    [WebMethod(EnableSession = true)]
    public static string GetResult(string begDate, string endDate, string agency, string RegisterDate, string dateFormat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];

        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        DateTime? begin = Conversion.convertDateTime(begDate, dateFormat);
        DateTime? end = Conversion.convertDateTime(endDate, dateFormat);
        bool registerDate = true;
        if (string.Equals(RegisterDate, "Y")) registerDate = false;
        string errorMsg = "";
        //string currency = string.Empty;
        string currency = UserData.SaleCur;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_FamilyTour))
            currency = "EUR";
        List<AgencyCommisionListRecord> list = new TvBo.Common().getAgencyCommisionList(UserData.AgencyID, agency, begin, end, registerDate, currency, ref errorMsg);
        if (list != null || list.Count > 0)
            return ListToTable(list);
        else
            return "";
    }

    private static string ListToTable(List<AgencyCommisionListRecord> list)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        bool showBrokerCom = UserData.AgencyType.HasValue && UserData.AgencyType.Value == 2 ? false : true;

        StringBuilder sb = new StringBuilder();
        sb.Append("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" id=\"resTable\" class=\"display\" style=\"margin-left: 0pt; width: 1000px;\">");
        #region Header
        sb.Append("<thead>");
        sb.Append("<tr>");
        sb.Append("<th class=\"sorting date\">" + HttpContext.GetGlobalResourceObject("Controls", "viewDate") + "</th>");
        sb.Append("<th class=\"sorting price\">" + HttpContext.GetGlobalResourceObject("AgencyCommisionReport", "lblAgency") + "</th>");
        sb.Append("<th class=\"sorting price\">" + HttpContext.GetGlobalResourceObject("AgencyCommisionReport", "lblCurrency") + "</th>");
        sb.Append("<th class=\"sorting price\">" + HttpContext.GetGlobalResourceObject("AgencyCommisionReport", "lblDebit") + "</th>");
        sb.Append("<th class=\"sorting price\">" + HttpContext.GetGlobalResourceObject("AgencyCommisionReport", "lblCommision") + "</th>");
        sb.Append("<th class=\"sorting price\">" + HttpContext.GetGlobalResourceObject("AgencyCommisionReport", "lblBrokerCommision") + "</th>");
        sb.Append("<th class=\"sorting price\">" + HttpContext.GetGlobalResourceObject("AgencyCommisionReport", "lblPayment") + "</th>");
        sb.Append("<th class=\"sorting price\">" + HttpContext.GetGlobalResourceObject("AgencyCommisionReport", "lblBalance") + "</th>");
        sb.Append("<th class=\"sorting\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Adult") + "</th>");
        sb.Append("<th class=\"sorting\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Child") + "</th>");
        sb.Append("<th class=\"sorting price\">" + HttpContext.GetGlobalResourceObject("AgencyCommisionReport", "lblPax") + "</th>");
        sb.Append("</tr>");
        sb.Append("</thead>");
        #endregion
        int cnt = 0;
        sb.Append("<tbody>");
        foreach (AgencyCommisionListRecord record in list)
        {
            if (Convert.ToInt32(cnt / 2.0) == (cnt / 2.0))
                sb.Append("<tr class=\"gradeU odd\">");
            else sb.Append("<tr  class=\"gradeU even\">");
            sb.Append("<td class=\"rightaligned\">" + record.SaleDate.Value.ToShortDateString() + "</td>");
            sb.Append("<td class=\"centeraligned\">" + record.AgencyCode + "</td>");
            sb.Append("<td class=\"centeraligned\">" + record.Cur + "</td>");
            sb.Append("<td class=\"rightaligned\">" + (record.Debit.HasValue && record.Debit.Value > 0 ? record.Debit.Value.ToString("#.00") : "0,00") + "</td>");
            sb.Append("<td class=\"rightaligned\">" + (record.Commission.HasValue && record.Commission.Value > 0 ? record.Commission.Value.ToString("#.00") : "0,00") + "</td>");
            if (showBrokerCom)
                sb.Append("<td class=\"rightaligned\">" + (record.BrokerCommission.HasValue && record.BrokerCommission.Value > 0 ? record.BrokerCommission.Value.ToString("#.00") : "0,00") + "</td>");
            else
                sb.Append("<td class=\"rightaligned\">" + "0,00" + "</td>");
            sb.Append("<td class=\"rightaligned\">" + (record.Payment.HasValue && record.Payment.Value > 0 ? record.Payment.Value.ToString("#.00") : "0,00") + "</td>");
            if (showBrokerCom)
                sb.Append("<td class=\"rightaligned\">" + (record.Balance.HasValue && record.Balance.Value > 0 ? record.Balance.Value.ToString("#.00") : "0,00") + "</td>");
            else
            {
                decimal balance = 0;
                if (record.Balance.HasValue)
                    balance = record.Balance.Value;
                if (record.BrokerCommission.HasValue)
                    balance += record.BrokerCommission.Value;
                sb.Append("<td class=\"rightaligned\">" + (balance > 0 ? balance.ToString("#.00") : "0,00") + "</td>");
            }
            sb.Append("<td class=\"centeraligned\">" + record.Adult + "</td>");
            sb.Append("<td class=\"centeraligned\">" + record.Child + "</td>");
            sb.Append("<td class=\"centeraligned\">" + (record.Pax.HasValue ? record.Pax.Value.ToString() : "0") + "</td>");
            sb.Append("</tr>");
            cnt++;

        }
        sb.Append("</tbody>");
        sb.Append("<tfoot>");
        sb.Append("<tr>");
        sb.Append("<th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th>");
        sb.Append("</tr>");
        sb.Append("</tfoot>");

        sb.Append("</table>");
        sb.Append("</div>");
        return sb.ToString();
    }

}
