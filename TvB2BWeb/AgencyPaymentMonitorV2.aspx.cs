﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using TvBo;
using TvTools;
using Winnovative;


public partial class AgencyPaymentMonitorV2 : BasePage
{
    public static string twoLetterISOLanguageName = "en";

    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url);
        List<string> lang = new List<string>() { "ar-DZ", "ar-DZ", "ar", "en-AU", "en-GB", "en-NZ", "fr-CH", "fr", "pt-BR", "pt", "sr-SR", "sr-CS", "sr", "zh-CN", "zh-HK", "zh-TW" };
        if (lang.Contains(UserData.Ci.Name))
            twoLetterISOLanguageName = UserData.Ci.Name;
        else
            twoLetterISOLanguageName = UserData.Ci.TwoLetterISOLanguageName != "en" ? UserData.Ci.TwoLetterISOLanguageName : "";
    }

    public static paymentMonitorFilterRecord createFilter()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;

        paymentMonitorFilterRecord filterDef = new paymentMonitorFilterRecord();
        if (HttpContext.Current.Session["FilterDefPayment"] != null)
            filterDef = (paymentMonitorFilterRecord)HttpContext.Current.Session["FilterDefPayment"];
        string datePatern = new TvBo.Common().getDateFormatOrg(UserData.Ci);
        filterDef.DateLang = UserData.Ci.Parent.ToString();
        filterDef.ResStatus = "0;1;3";
        filterDef.ConfStatus = "0;1;2;3";
        filterDef.PayStatus = "2;3";
        filterDef.DateFormat = datePatern;
        filterDef.ShowAllRes = UserData.ShowAllRes;
        filterDef.DueDate = DateTime.Today;
        return filterDef;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static object selectedRowUpdate(string ResNo, bool Selected)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        List<AgencyPaymentMonitorFields> resList = ReadFile();
        AgencyPaymentMonitorFields res = resList.Find(f => f.ResNo == ResNo);
        res.SelectedRow = Selected;
        CreateAndWriteFile(resList);

        return addSelectedRow(res.ResNo);

        /*
        string retVal = string.Empty;
        var query = from q in resList
                    group q by new { q.SaleCur } into key
                    select new { key.Key.SaleCur };
        decimal? totalSum = 0;
        foreach (var row in query)
        {
            decimal CurrTotal = (decimal)0;
            CurrTotal += resList.Where(w => w.SaleCur == row.SaleCur && w.SelectedRow).Sum(s => (s.Balance.HasValue ? s.Balance.Value : (decimal)0));
            retVal += string.Format("<div style=\"width: 150px; text-align: right;\">{0}</div>", CurrTotal.ToString("#,###.00") + " " + row.SaleCur);
            totalSum += CurrTotal;
        }
        return new
        {
            show = totalSum.HasValue && totalSum.Value > 0,
            innerHtml = retVal
        };
        */
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static paymentMonitorFilterData CreateFilterData(string clear)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        bool? _clear = Conversion.getBoolOrNull(clear);
        if ((_clear.HasValue ? _clear.Value : true))
            HttpContext.Current.Session["FilterDefPayment"] = null;

        List<paymentMonitorDefaultRecord> defaultData = new AgencyPaymentMonitors().getResMonDefaultData(UserData, ref errorMsg);
        paymentMonitorFilterData filterData = new paymentMonitorFilterData();
        //if (createFilter() != "OK") Newtonsoft.Json.JsonConvert.SerializeObject(filterData);        

        filterData.Filter = createFilter();
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            filterData.AgencyOfficeData = new AgencyPaymentMonitors().getAgencyOffices(defaultData).Where(w => w.Code == UserData.AgencyID).ToList<listString>();
        else
            filterData.AgencyOfficeData = new AgencyPaymentMonitors().getAgencyOffices(defaultData);
        filterData.AgencyUserData = new AgencyPaymentMonitors().getAgencyUser(defaultData);
        filterData.DepCityData = new AgencyPaymentMonitors().getDepCity(defaultData);
        filterData.ArrCityData = new AgencyPaymentMonitors().getArrCity(defaultData);

        if (filterData.AgencyUserData.Count > 0) {
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
                filterData.ShowAllRes = true;
            else
                filterData.Filter.AgencyUser = UserData.UserID;
            filterData.ShowAllRes = UserData.ShowAllRes;
            if (!filterData.ShowAllRes && filterData.AgencyUserData.Where(w => w.Code != UserData.UserID).Count() > 0)
                foreach (listString row in filterData.AgencyUserData.Where(w => w.Code != UserData.UserID).Select(s => s).ToList<listString>())
                    filterData.AgencyUserData.Remove(row);
        }
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr))
            filterData.ShowDraft = true;

        filterData.currentCultureNumber = UserData.Ci.NumberFormat;
        bool showPayment = (string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt) && UserData.MainAgency) && VersionControl.CheckWebVersion(UserData.WebVersion, 48, VersionControl.Equality.gt);
        filterData.showAgencyPayment = showPayment;
        return filterData;
    }

    public static paymentMonitorFilterRecord convertFilterRecord(paymentMonitorFilterJSonRecord _data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        paymentMonitorFilterRecord retVal = new paymentMonitorFilterRecord();
        retVal.AgencyOffice = _data.AgencyOffice;
        retVal.AgencyUser = _data.AgencyUser;
        
        retVal.ArrCity = Conversion.getInt32OrNull(_data.ArrCity);
        retVal.DueDate = Conversion.getDateTimeOrNull(_data.DueDate);
        retVal.BegDate1 = Conversion.getDateTimeOrNull(_data.BegDate1);
        retVal.BegDate2 = Conversion.getDateTimeOrNull(_data.BegDate2);
        retVal.ResDate1 = Conversion.getDateTimeOrNull(_data.ResDate1);
        retVal.ResDate2 = Conversion.getDateTimeOrNull(_data.ResDate2);
        retVal.ConfStatus = _data.ConfStatus;
        retVal.DateFormat = _data.DateFormat;
        retVal.DepCity = Conversion.getInt32OrNull(_data.DepCity);
        retVal.LeaderNameF = _data.LeaderNameF;
        retVal.LeaderNameS = _data.LeaderNameS;
        retVal.PageNo = Conversion.getInt32OrNull(_data.PageNo);
        retVal.PayStatus = _data.PayStatus;
        retVal.ResDate1 = Conversion.getDateTimeOrNull(_data.ResDate1);
        retVal.ResDate2 = Conversion.getDateTimeOrNull(_data.ResDate2);
        retVal.ResStatus = _data.ResStatus;
        retVal.IncludeDueDate = Equals(_data.IncludeDueDate, "1") ? true : false;
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string GetSummary(string data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string error = "";
        string _dataJson = data.Replace('<', '{').Replace('>', '}').Replace('|', '"');
        paymentMonitorFilterRecord _data = Newtonsoft.Json.JsonConvert.DeserializeObject<paymentMonitorFilterRecord>(_dataJson);
        List<AgencyPaymentMonitorFields> list = new AgencyPaymentMonitors().getAgencyPaymentMonitorData(UserData,
                                                        _data.AgencyUser, _data.AgencyOffice, _data.ArrCity,
                                                        _data.DepCity, _data.BegDate1, _data.BegDate2,
                                                        _data.ResDate1, _data.ResDate2, _data.DueDate,
                                                        _data.PayStatus, _data.ResStatus, _data.ConfStatus,
                                                        _data.IncludeDueDate, _data.LeaderNameS, _data.LeaderNameF,
                                                        _data.ResNo1, _data.ResNo2, ref error);

        CreateAndWriteFile(list);
        var sums = from s in list.AsEnumerable()
                   group s by new { Cur = s.SaleCur } into g
                   select new {
                       Cur = g.Key.Cur,
                       Payable = list.AsEnumerable().Where(w => w.SaleCur == g.Key.Cur.ToString()).Sum(s => s.AgencyPayable),
                       Payable2 = list.AsEnumerable().Where(w => w.SaleCur == g.Key.Cur.ToString()).Sum(s => s.AgencyPayable2),
                       Payment = list.AsEnumerable().Where(w => w.SaleCur == g.Key.Cur.ToString()).Sum(s => s.AgencyPayment),
                       Balance = list.AsEnumerable().Where(w => w.SaleCur == g.Key.Cur.ToString()).Sum(s => s.Balance)
                   };
        StringBuilder sb = new StringBuilder();
        sb.Append("<div id=\"divSummation\">");
        sb.Append("<div class=\"reservationHeaderRow\">");
        sb.Append("<div class=\"divSumCurHeader\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Cur") + "</div>");
        sb.Append("<div class=\"divSumAgencyPayableHeader\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyPayable") + "</div>");
        sb.Append("<div class=\"divSumAgencyPayableAccordingHeader\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyPayableAccording") + "</div>");
        sb.Append("<div class=\"divSumAgencyPaymentHeader\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyPayment") + "</div>");
        sb.Append("<div class=\"divSumDueDateBalanceHeader\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "DueDateBalance") + "</div>");
        sb.Append("</div>");
        int cnt = 0;
        foreach (var record in sums) {
            if (Convert.ToInt32(cnt / 2.0) == (cnt / 2.0))
                sb.Append("<div class=\"reservationRow\" style=\"background-color: #FFF;\">");
            else
                sb.Append("<div class=\"reservationRow\" style=\"background-color: #CCC;\">");
            sb.Append("<div class=\"divSumCur\"><span>" + record.Cur + "</span></div>");
            sb.Append("<div class=\"divSumAgencyPayable\"><span>" + (record.Payable.HasValue ? record.Payable.Value.ToString("#,###.00") : "") + "</span></div>");
            sb.Append("<div class=\"divSumAgencyPayableAccording\"><span>" + (record.Payable2.HasValue ? record.Payable2.Value.ToString("#,###.00") : "") + "</span></div>");
            sb.Append("<div class=\"divSumAgencyPayment\"><span>" + (record.Payment.HasValue ? record.Payment.Value.ToString("#,###.00") : "") + "</span></div>");
            sb.Append("<div class=\"divSumDueDateBalance\"><span>" + (record.Balance.HasValue ? record.Balance.Value.ToString("#,###.00") : "") + "</span></div>");
            sb.Append("</div>");
            cnt++;
        }
        sb.Append("</div>");
        return sb.ToString();
    }

    public static object getObject(object row, string fieldName)
    {
        System.Reflection.PropertyInfo[] oProps = null;
        oProps = row.GetType().GetProperties();
        List<System.Reflection.PropertyInfo> _pi = (from q in oProps
                                                    where q.Name == fieldName
                                                    select q).ToList<System.Reflection.PropertyInfo>();
        object value = null;
        if (_pi != null && _pi.Count() > 0) {
            System.Reflection.PropertyInfo pi = _pi[0];
            value = pi.GetValue(row, null) == null ? DBNull.Value : pi.GetValue(row, null);
        }
        return value;
    }

    [WebMethod(EnableSession = true)]
    public static string getGridHeaders()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        bool? showAgencyComView = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyComView"));
        bool showAgencyCom = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyCom")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyCom")).Value : true;
        bool showAgencyEB = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyEB")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyEB")).Value : true;
        bool showPassengerEB = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showPassengerEB")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showPassengerEB")).Value : true;

        if (!Users.checkCurrentUserOperator(TvBo.Common.crID_Anex)) {
            #region for Anex
            if (showAgencyComView.HasValue && (UserData.ShowAllRes || showAgencyComView.Value)) {
                showAgencyCom = true;
                showAgencyEB = true;
                showPassengerEB = true;
            }
            #endregion
        } else {
            if (showAgencyComView.HasValue && showAgencyComView.Value) {
                showAgencyCom = true;
                showAgencyEB = true;
                showPassengerEB = true;
            }
        }

        List<dataTableColumnsDef> aoColumns = new List<dataTableColumnsDef>();
        bool showPayment = (string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt) && UserData.MainAgency) && VersionControl.CheckWebVersion(UserData.WebVersion, 48, VersionControl.Equality.gt);
        if (showPayment)
            aoColumns.Add(new dataTableColumnsDef { IDNo = 0, ColName = "SelectRes", sTitle = "&nbsp;", sWidth = "35px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 1, ColName = "ResNo", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "ResNo").ToString(), sWidth = "100px", bSortable = true });
        if (string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt))
            aoColumns.Add(new dataTableColumnsDef { IDNo = 29, ColName = "OptionDate", sTitle = "Option Date", sWidth = "90px", bSortable = false, sType = "date" });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 2, ColName = "DepCity", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "lblDepCity").ToString(), sWidth = "150px", bSortable = true });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 3, ColName = "ArrCity", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "lblArrCity").ToString(), sWidth = "150px", bSortable = true });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 4, ColName = "BegDate", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "BegDate").ToString(), sWidth = "90px", bSortable = true, sType = "date" });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 5, ColName = "EndDate", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "EndDate").ToString(), sWidth = "90px", bSortable = true, sType = "date" });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 6, ColName = "Nights", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Nights").ToString(), sWidth = "30px", bSortable = true });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 7, ColName = "lblStatus", sTitle = HttpContext.GetGlobalResourceObject("ResMonitor", "lblStatus").ToString(), sWidth = "50px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 8, ColName = "SaleDate", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "SaleDate").ToString(), sWidth = "90px", bSortable = true, sType = "date" });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 9, ColName = "Leader", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Leader").ToString(), sWidth = "250px", bSortable = true });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 10, ColName = "Adult", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Adult").ToString(), sWidth = "30px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 11, ColName = "Child", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Child").ToString(), sWidth = "30px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 12, ColName = "Currency", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Currency").ToString(), sWidth = "30px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 13, ColName = "SalePrice", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "SalePrice").ToString(), sWidth = "90px", bSortable = false, sType = "numeric", sClass = "numericStyle" });
        if (showAgencyCom) {
            aoColumns.Add(new dataTableColumnsDef { IDNo = 14, ColName = "AgencyCom", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyCom").ToString(), sWidth = "90px", bSortable = false, sType = "numeric", sClass = "numericStyle" });
            aoColumns.Add(new dataTableColumnsDef { IDNo = 15, ColName = "DiscountFromCom", sTitle = HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyDiscountFromCommision").ToString(), sWidth = "90px", bSortable = false, sType = "numeric", sClass = "numericStyle" });
        }
        aoColumns.Add(new dataTableColumnsDef { IDNo = 16, ColName = "Discount", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Discount").ToString(), sWidth = "90px", bSortable = false, sType = "numeric", sClass = "numericStyle" });
        if (UserData.MainAgency)
            aoColumns.Add(new dataTableColumnsDef { IDNo = 17, ColName = "BrokerCom", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "BrokerCom").ToString(), sWidth = "90px", bSortable = false, sType = "numeric", sClass = "numericStyle" });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 18, ColName = "AgencySupDis", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencySupDis").ToString(), sWidth = "90px", bSortable = false, sType = "numeric", sClass = "numericStyle" });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 19, ColName = "AgencyComSup", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyComSup").ToString(), sWidth = "90px", bSortable = false, sType = "numeric", sClass = "numericStyle" });
        if (showAgencyEB)
            aoColumns.Add(new dataTableColumnsDef { IDNo = 20, ColName = "AgencyEB", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyEB").ToString(), sWidth = "90px", bSortable = false, sType = "numeric", sClass = "numericStyle" });
        if (showPassengerEB)
            aoColumns.Add(new dataTableColumnsDef { IDNo = 21, ColName = "PassengerEB", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "PassengerEB").ToString(), sWidth = "90px", bSortable = false, sType = "numeric", sClass = "numericStyle" });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 22, ColName = "AgencyPayable", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyPayable").ToString(), sWidth = "90px", bSortable = false, sType = "numeric", sClass = "numericStyle" });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 23, ColName = "AgencyPayableAccording", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyPayableAccording").ToString(), sWidth = "90px", bSortable = false, sType = "numeric", sClass = "numericStyle" });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 24, ColName = "AgencyPayment", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyPayment").ToString(), sWidth = "90px", bSortable = false, sType = "numeric", sClass = "numericStyle" });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 25, ColName = "DueDateBalance", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "DueDateBalance").ToString(), sWidth = "90px", bSortable = false, sType = "numeric", sClass = "numericStyle" });

        if (string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt))
            aoColumns.Add(new dataTableColumnsDef { IDNo = 28, ColName = "JournalRef", sTitle = "Journal Ref.", sWidth = "110px", bSortable = false });
            
        if (UserData.Bonus.BonusUserSeeOwnW)
            aoColumns.Add(new dataTableColumnsDef { IDNo = 26, ColName = "UserBonus", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "UsedUserBonus").ToString(), sWidth = "90px", bSortable = false, sType = "numeric", sClass = "numericStyle" });
        if ((UserData.Bonus.BonusAgencySeeOwnW && UserData.Bonus.BonusUserSeeAgencyW) || (UserData.Bonus.BonusAgencySeeOwnW && UserData.MasterAgency))
            aoColumns.Add(new dataTableColumnsDef { IDNo = 27, ColName = "AgencyBonus", sTitle = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "UsedAgencyBonus").ToString(), sWidth = "90px", bSortable = false, sType = "numeric", sClass = "numericStyle" });
        int ii = 0;
        foreach (dataTableColumnsDef row in aoColumns) {
            row.SeqNo = row.IDNo;
            ii++;
            row.IDNo = ii;
        }
        var retVal = from q in aoColumns.AsEnumerable()
                     select new { sTitle = q.sTitle, sWidth = q.sWidth, bSortable = q.bSortable, sType = q.sType, sClass = q.sClass, q.SeqNo };
        return Newtonsoft.Json.JsonConvert.SerializeObject(retVal);
    }

    private static void CreateAndWriteFile(List<AgencyPaymentMonitorFields> result)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\AgencyPaymentMonitor." + HttpContext.Current.Session.SessionID;
        if (File.Exists(path))
            File.Delete(path);
        FileStream f = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
        try {
            StreamWriter writer = new StreamWriter(f);
            string compress = TvTools.GZipCompres.Compress(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            writer.Write(compress);
            writer.Close();
        }
        catch (Exception) {
            throw;
        }
        finally {
            f.Close();
        }
    }

    private static List<AgencyPaymentMonitorFields> ReadFile()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string yol = AppDomain.CurrentDomain.BaseDirectory + "Cache\\AgencyPaymentMonitor." + HttpContext.Current.Session.SessionID;
        StreamReader reader = new StreamReader(yol);
        List<AgencyPaymentMonitorFields> list = new List<AgencyPaymentMonitorFields>();
        try {
            string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
            list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgencyPaymentMonitorFields>>(uncompressed);
        }
        catch (Exception) {
            throw;
        }
        finally {
            reader.Close();
        }
        return list;
    }

    [WebMethod(EnableSession = true)]
    public static object getMultiLangID(string ResNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        // if (HttpContext.Current.Session["ResData"] == null) return null;
        string errorMsg = string.Empty;

        TvBo.ResMainRecord resMain = new ResTables().getResMain(UserData, ResNo, null, ref errorMsg);
        if (resMain == null)
            return null;
        if (VersionControl.CheckWebVersion(UserData.WebVersion, 44, VersionControl.Equality.le))
            return null;

        List<int?> multiLangID = new TvReport.AllOperator().getDocumentLang(resMain.Market, 1, ref errorMsg);

        var query = from q in CultureInfo.GetCultures(CultureTypes.AllCultures)
                    join qMl in multiLangID on q.LCID.ToString() equals qMl.ToString()
                    select new { Id = q.LCID, Name = q.DisplayName };
        return query.Count() > 0 ? query : null;
    }

    [WebMethod(EnableSession = true)]
    public static string getInvoice(string ResNo, int? LangId)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        ResDataRecord ResData = new ResTables().getReservationData(UserData, ResNo, string.Empty, ref errorMsg);
        HttpContext.Current.Session["ResData"] = ResData;
        List<ResViewMenu> subMenuList = CacheObjects.getResViewMenuData();
        var invoiceM = subMenuList.Where(w => w.Code == "Invoice" || w.Code == "Proforma");
        bool invoiceMenu = false;
        if (invoiceM.Count() > 0)
            invoiceMenu = true;
        else
            invoiceMenu = false;

        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        string docFolder = string.Empty;
        if (invoiceM.FirstOrDefault().SubFolder)
            docFolder = basePageUrl + (invoiceM.FirstOrDefault().SubFolder ? (DocumentFolder + "/" + UserData.Market + "/") : "");
        else
            docFolder = "";
        string DocUrl = string.Empty;
        if (invoiceMenu)
            DocUrl = docFolder + invoiceM.FirstOrDefault().Url;
        string docFile = docFolder + DocUrl;
        string retValM = "\"NewWindow\":\"{0}\",\"JavaScriptName\":\"{1}\",\"Param1\":\"{2}\",\"Param2\":\"{3}\",\"ResultType\":\"{4}\"";
        string retVal = "";
        /*
        string retVal = string.Format(retValM,
                        DocUrl,
                        "Invoice",
                        new UICommon().GetSubMenuGlobalResourceObject(UserData, invoiceM.FirstOrDefault().ResourceName).ToString());
        */
        Int16 docOutputType = 0;
        string reportResult = new TvReports().getReportV2PDF(UserData, ResData, "Proforma", HttpContext.Current.Server.MapPath("~") + "\\ACE\\", LangId, docOutputType, null, null, docFolder, false, ref errorMsg);
        if (!string.IsNullOrEmpty(errorMsg))
            retVal = string.Format(retValM, "0", "deturReport", "", errorMsg, "");
        else {
            if (!string.IsNullOrEmpty(reportResult)) {
                retVal = string.Format(retValM, "0", "deturReport", WebRoot.BasePageRoot + "ACE/" + reportResult, "", "PDF");
            } else {
                retVal = string.Format(retValM, "0", "deturReport", "", errorMsg, "");
            }
        }

        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string viewPDFReport(string reportType, string _html, string pageWidth, string ResNo, string urlBase, string pdfConvSerial)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        _html = _html.Replace('|', '"');
        _html = _html.Replace("<div id=\"noPrintDiv\">", "<div id=\"noPrintDiv\" style=\"display: none; visibility: hidden;\">");
        string errorMsg = string.Empty;
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        string pdfFileName = reportType + "_" + ResNo + ".pdf";
        StringBuilder html = new StringBuilder(_html);
        int PageWidth = Conversion.getInt32OrNull(pageWidth).HasValue ? Conversion.getInt32OrNull(pageWidth).Value : 700;

        //initialize the PdfConvert object
        PdfConverter pdfConverter = new PdfConverter();

        pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
        pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.NoCompression;
        pdfConverter.PdfDocumentOptions.ShowHeader = false;
        pdfConverter.PdfDocumentOptions.ShowFooter = false;
        pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
        pdfConverter.PdfDocumentOptions.LeftMargin = 10;
        pdfConverter.PdfDocumentOptions.TopMargin = 10;
        // set the demo license key
        //pdfConverter.LicenseKey = pdfConvSerial; // "RG92ZHVkdXF0cGRxanRkd3VqdXZqfX19fQ==";
        pdfConverter.LicenseKey = ConfigurationManager.AppSettings["Winnovative"];// "RG92ZHVkdXF0cGRxanRkd3VqdXZqfX19fQ==";

        // get the base url for string conversion which is the url from where the html code was retrieved
        // the base url is a hint for the converter to find the external CSS and images referenced by relative URLs
        string thisPageURL = HttpContext.Current.Request.Url.AbsoluteUri;
        string baseUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/";

        try {
            pdfConverter.SavePdfFromHtmlStringToFile(html.ToString(), siteFolderISS + "ACE\\" + pdfFileName, urlBase);
            string returnUrl = basePageUrl + "ACE/" + pdfFileName;
            return returnUrl;
        }
        catch {
            return "";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string exportExcel()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        bool? showAgencyComView = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyComView"));
        bool showAgencyCom = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyCom")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyCom")).Value : true;
        bool showAgencyEB = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyEB")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyEB")).Value : true;
        bool showPassengerEB = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showPassengerEB")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showPassengerEB")).Value : true;

        if (!Users.checkCurrentUserOperator(TvBo.Common.crID_Anex)) {
            #region for Anex
            if (showAgencyComView.HasValue && (UserData.ShowAllRes || showAgencyComView.Value)) {
                showAgencyCom = true;
                showAgencyEB = true;
                showPassengerEB = true;
            }
            #endregion
        } else {
            if (showAgencyComView.HasValue && showAgencyComView.Value) {
                showAgencyCom = true;
                showAgencyEB = true;
                showPassengerEB = true;
            }
        }
        List<dataTableColumnsDef> aoColumns = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dataTableColumnsDef>>(getGridHeaders());
        StringBuilder sb = new StringBuilder();
        string header = string.Empty;
        foreach (dataTableColumnsDef row in aoColumns) {
            if (row.SeqNo != 0)
                if ((UserData.MainAgency && row.SeqNo == 17) || row.SeqNo != 17) {
                    if (header.Length > 0)
                        header += ";";
                    header += row.sTitle;
                }
        }
        if (UserData.MasterAgency) {
            header += ";" + HttpContext.GetGlobalResourceObject("ResView", "lblAgency").ToString();
        }
        sb.Append(header);
        List<AgencyPaymentMonitorFields> filteredRecords = ReadFile();
        var retval = from q in filteredRecords.AsQueryable()
                     select new {
                         ResNo = q.ResNo,
                         DepCityName = q.DepCityName,
                         ArrCityName = q.ArrCityName,
                         BegDate = q.BegDate.HasValue ? q.BegDate.Value.ToShortDateString() : "",
                         EndDate = q.EndDate.HasValue ? q.EndDate.Value.ToShortDateString() : "",
                         Days = q.Days.ToString(),
                         statusStr = string.Format("{0}, {1}",
                                                    HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + q.ResStat.ToString()).ToString(),
                                                    HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + q.ConfStat.ToString()).ToString()),
                         ResDate = q.ResDate.HasValue ? q.ResDate.Value.ToShortDateString() : "",
                         LeaderName = q.Leader,
                         Adult = q.Adult.ToString(),
                         Child = q.Child.ToString(),
                         SaleCur = q.SaleCur,
                         SalePrice = q.SalePrice,
                         AgencyCom = q.AgencyCom,
                         DiscountFromCom = q.DiscountFromCom,
                         Discount = q.Discount,
                         BrokerCom = q.BrokerCom,
                         AgencySupDis = q.AgencySupDis,
                         AgencyComSup = q.AgencyComSup,
                         EBAgency = q.EBAgency,
                         EBPas = q.EBPas,
                         AgencyPayable = q.AgencyPayable,
                         AgencyPayable2 = q.AgencyPayable2,
                         AgencyPayment = q.AgencyPayment,
                         Balance = q.Balance,
                         UserBonus = q.UserBonus,
                         AgencyBonus = q.AgencyBonus,
                         AgencyName = q.AgencyName
                     };

        foreach (var row in retval) {
            string rowStr = string.Empty;
            rowStr += "\n";
            rowStr += row.ResNo;
            rowStr += ";";
            rowStr += row.DepCityName;
            rowStr += ";";
            rowStr += row.ArrCityName;
            rowStr += ";";
            rowStr += row.BegDate;
            rowStr += ";";
            rowStr += row.EndDate;
            rowStr += ";";
            rowStr += row.Days.ToString();
            rowStr += ";";
            rowStr += row.statusStr;
            rowStr += ";";
            rowStr += row.ResDate;
            rowStr += ";";
            rowStr += row.LeaderName;
            rowStr += ";";
            rowStr += row.Adult.ToString();
            rowStr += ";";
            rowStr += row.Child.ToString();
            rowStr += ";";
            rowStr += row.SaleCur;
            rowStr += ";";
            rowStr += row.SalePrice.HasValue ? row.SalePrice.Value.ToString().Replace(".", ",") : "";
            if (showAgencyCom) {
                rowStr += ";";
                rowStr += row.AgencyCom.HasValue ? row.AgencyCom.Value.ToString().Replace(".", ",") : "";
                rowStr += ";";
                rowStr += row.DiscountFromCom.HasValue ? row.DiscountFromCom.Value.ToString().Replace(".", ",") : "";
            }
            rowStr += ";";
            rowStr += row.Discount.HasValue ? row.Discount.Value.ToString().Replace(".", ",") : "";
            if (UserData.MainAgency) {
                rowStr += ";";
                rowStr += row.BrokerCom.HasValue ? row.BrokerCom.Value.ToString().Replace(".", ",") : "";
            }
            rowStr += ";";
            rowStr += row.AgencySupDis.HasValue ? row.AgencySupDis.Value.ToString().Replace(".", ",") : "";
            rowStr += ";";
            rowStr += row.AgencyComSup.HasValue ? row.AgencyComSup.Value.ToString().Replace(".", ",") : "";
            if (showAgencyEB) {
                rowStr += ";";
                rowStr += row.EBAgency.HasValue ? row.EBAgency.Value.ToString().Replace(".", ",") : "";
            }
            if (showPassengerEB) {
                rowStr += ";";
                rowStr += row.EBPas.HasValue ? row.EBPas.Value.ToString().Replace(".", ",") : "";
            }
            rowStr += ";";
            rowStr += row.AgencyPayable.HasValue ? row.AgencyPayable.Value.ToString().Replace(".", ",") : "";
            rowStr += ";";
            rowStr += row.AgencyPayable2.HasValue ? row.AgencyPayable2.Value.ToString().Replace(".", ",") : "";
            rowStr += ";";
            rowStr += row.AgencyPayment.HasValue ? row.AgencyPayment.Value.ToString().Replace(".", ",") : "";
            rowStr += ";";
            rowStr += row.Balance.HasValue ? row.Balance.Value.ToString().Replace(".", ",") : "";
            if (UserData.Bonus.BonusUserSeeOwnW) {
                rowStr += ";";
                rowStr += row.UserBonus.HasValue ? row.UserBonus.Value.ToString().Replace(".", ",") : "";
            }
            if ((UserData.Bonus.BonusAgencySeeOwnW && UserData.Bonus.BonusUserSeeAgencyW) || (UserData.Bonus.BonusAgencySeeOwnW && UserData.MasterAgency)) {
                rowStr += ";";
                rowStr += row.AgencyBonus.HasValue ? row.AgencyBonus.Value.ToString().Replace(".", ",") : "";
            }
            if (UserData.MasterAgency) {
                rowStr += ";";
                rowStr += row.AgencyName;
            }
            sb.Append(rowStr);
        }
        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\PM_" + HttpContext.Current.Session.SessionID + ".CSV";
        if (File.Exists(path))
            File.Delete(path);
        FileStream f = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
        try {
            StreamWriter writer = new StreamWriter(f, Encoding.UTF8);
            writer.Write(sb.ToString());
            writer.Close();
        }
        catch (Exception) {
            throw;
        }
        finally {
            f.Close();
        }

        return WebRoot.BasePageRoot + "Cache/PM_" + HttpContext.Current.Session.SessionID + ".CSV";
    }

    [WebMethod(EnableSession = true)]
    public static string exportHtml()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        bool? showAgencyComView = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyComView"));
        bool showAgencyCom = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyCom")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyCom")).Value : true;
        bool showAgencyEB = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyEB")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyEB")).Value : true;
        bool showPassengerEB = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showPassengerEB")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showPassengerEB")).Value : true;

        if (!Users.checkCurrentUserOperator(TvBo.Common.crID_Anex)) {
            #region for Anex
            if (showAgencyComView.HasValue && (UserData.ShowAllRes || showAgencyComView.Value)) {
                showAgencyCom = true;
                showAgencyEB = true;
                showPassengerEB = true;
            }
            #endregion
        } else {
            if (showAgencyComView.HasValue && showAgencyComView.Value) {
                showAgencyCom = true;
                showAgencyEB = true;
                showPassengerEB = true;
            }
        }

        List<AgencyPaymentMonitorFields> filteredRecords = ReadFile();
        var list = from q in filteredRecords.AsQueryable()
                   select new {
                       ResNo = q.ResNo,
                       DepCityName = q.DepCityName,
                       ArrCityName = q.ArrCityName,
                       BegDate = q.BegDate.HasValue ? q.BegDate.Value.ToShortDateString() : "",
                       EndDate = q.EndDate.HasValue ? q.EndDate.Value.ToShortDateString() : "",
                       Days = q.Days.ToString(),
                       statusStr = string.Format("{0}, {1}",
                                                  HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + q.ResStat.ToString()).ToString(),
                                                  HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + q.ConfStat.ToString()).ToString()),
                       ResDate = q.ResDate.HasValue ? q.ResDate.Value.ToShortDateString() : "",
                       LeaderName = q.Leader,
                       Adult = q.Adult.ToString(),
                       Child = q.Child.ToString(),
                       SaleCur = q.SaleCur,
                       SalePrice = q.SalePrice,
                       AgencyCom = q.AgencyCom,
                       DiscountFromCom = q.DiscountFromCom,
                       Discount = q.Discount,
                       BrokerCom = q.BrokerCom,
                       AgencySupDis = q.AgencySupDis,
                       AgencyComSup = q.AgencyComSup,
                       EBAgency = q.EBAgency,
                       EBPas = q.EBPas,
                       AgencyPayable = q.AgencyPayable,
                       AgencyPayable2 = q.AgencyPayable2,
                       AgencyPayment = q.AgencyPayment,
                       Balance = q.Balance,
                       UserBonus = q.UserBonus,
                       AgencyBonus = q.AgencyBonus
                   };

        List<dataTableColumnsDef> aoColumns = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dataTableColumnsDef>>(getGridHeaders());
        StringBuilder sb = new StringBuilder();
        sb.Append("<table width=\"100%\" cellpadding=\"5\" cellspacing=\"5\">");
        sb.Append("<tr>");
        //foreach (dataTableColumnsDef row in aoColumns)
        //{
        //    sb.AppendFormat("<th>{0}</th>", row.sTitle);
        //}

        foreach (dataTableColumnsDef row in aoColumns) {
            if (row.SeqNo != 0)
                if ((UserData.MainAgency && row.SeqNo == 17) || row.SeqNo != 17) {
                    sb.AppendFormat("<th>{0}</th>", row.sTitle);
                }
        }

        sb.Append("</tr>");
        foreach (var row in list) {
            sb.Append("<tr>");

            sb.AppendFormat("<td>{0}</td>", row.ResNo);
            sb.AppendFormat("<td>{0}</td>", row.DepCityName);
            sb.AppendFormat("<td>{0}</td>", row.ArrCityName);
            sb.AppendFormat("<td>{0}</td>", row.BegDate);
            sb.AppendFormat("<td>{0}</td>", row.EndDate);
            sb.AppendFormat("<td>{0}</td>", row.Days.ToString());
            sb.AppendFormat("<td>{0}</td>", row.statusStr);
            sb.AppendFormat("<td>{0}</td>", row.ResDate);
            sb.AppendFormat("<td>{0}</td>", row.LeaderName);
            sb.AppendFormat("<td>{0}</td>", row.Adult.ToString());
            sb.AppendFormat("<td>{0}</td>", row.Child.ToString());
            sb.AppendFormat("<td>{0}</td>", row.SaleCur);
            sb.AppendFormat("<td>{0}</td>", row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") : "");
            if (showAgencyCom) {
                sb.AppendFormat("<td>{0}</td>", row.AgencyCom.HasValue ? row.AgencyCom.Value.ToString("#,###.00") : "");
                sb.AppendFormat("<td>{0}</td>", row.DiscountFromCom.HasValue ? row.DiscountFromCom.Value.ToString("#,###.00") : "");
            }
            sb.AppendFormat("<td>{0}</td>", row.Discount.HasValue ? row.Discount.Value.ToString("#,###.00") : "");
            if (UserData.MainAgency)
                sb.AppendFormat("<td>{0}</td>", row.BrokerCom.HasValue ? row.BrokerCom.Value.ToString("#,###.00") : "");
            sb.AppendFormat("<td>{0}</td>", row.AgencySupDis.HasValue ? row.AgencySupDis.Value.ToString("#,###.00") : "");
            sb.AppendFormat("<td>{0}</td>", row.AgencyComSup.HasValue ? row.AgencyComSup.Value.ToString("#,###.00") : "");
            if (showAgencyEB)
                sb.AppendFormat("<td>{0}</td>", row.EBAgency.HasValue ? row.EBAgency.Value.ToString("#,###.00") : "");
            if (showPassengerEB)
                sb.AppendFormat("<td>{0}</td>", row.EBPas.HasValue ? row.EBPas.Value.ToString("#,###.00") : "");
            sb.AppendFormat("<td>{0}</td>", row.AgencyPayable.HasValue ? row.AgencyPayable.Value.ToString("#,###.00") : "");
            sb.AppendFormat("<td>{0}</td>", row.AgencyPayable2.HasValue ? row.AgencyPayable2.Value.ToString("#,###.00") : "");
            sb.AppendFormat("<td>{0}</td>", row.AgencyPayment.HasValue ? row.AgencyPayment.Value.ToString("#,###.00") : "");
            sb.AppendFormat("<td>{0}</td>", row.Balance.HasValue ? row.Balance.Value.ToString("#,###.00") : "");
            if (UserData.Bonus.BonusUserSeeOwnW)
                sb.AppendFormat("<td>{0}</td>", row.UserBonus.HasValue ? row.UserBonus.Value.ToString("#,###.00") : "");
            if ((UserData.Bonus.BonusAgencySeeOwnW && UserData.Bonus.BonusUserSeeAgencyW) || (UserData.Bonus.BonusAgencySeeOwnW && UserData.MasterAgency))
                sb.AppendFormat("<td>{0}</td>", row.AgencyBonus.HasValue ? row.AgencyBonus.Value.ToString("#,###.00") : "");

            sb.Append("</tr>");
        }
        sb.Append("</table>");


        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    private static List<AgencyPayList> getSelectedList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        List<AgencyPayList> list = new List<AgencyPayList>();
        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\APMSelectList." + HttpContext.Current.Session.SessionID;
        if (File.Exists(path)) {
            StreamReader reader = new StreamReader(path);
            try {
                string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
                list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgencyPayList>>(uncompressed);
            }
            catch (Exception) {
                throw;
            }
            finally {
                reader.Close();
            }
        }
        return list;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static object addSelectedRow(string resNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\APMSelectList." + HttpContext.Current.Session.SessionID;

        List<AgencyPayList> currentSelectedList = getSelectedList();
        List<AgencyPaymentMonitorFields> currentResList = ReadFile();
        AgencyPaymentMonitorFields selectedRow = currentResList.Find(f => f.ResNo == resNo);
        if (selectedRow == null)
            return false;

        if (selectedRow.SelectedRow) {
            AgencyPayList currentRes = currentSelectedList.Find(f => f.ResNo == selectedRow.ResNo);
            if (currentRes == null) {
                currentSelectedList.Add(new AgencyPayList {
                    ResNo = selectedRow.ResNo,
                    PayAmount = selectedRow.Balance,
                    Currency = selectedRow.SaleCur
                });
            }
        } else {
            AgencyPayList currentRes = currentSelectedList.Find(f => f.ResNo == selectedRow.ResNo);

            if (currentRes != null) {
                currentSelectedList.Remove(currentRes);
            }
        }

        if (File.Exists(path))
            File.Delete(path);
        FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
        try {
            StreamWriter writer = new StreamWriter(file);
            string compress = TvTools.GZipCompres.Compress(Newtonsoft.Json.JsonConvert.SerializeObject(currentSelectedList));
            writer.Write(compress);
            writer.Close();
        }
        catch (Exception) {
            throw;
        }
        finally {
            file.Close();
        }

        if (currentSelectedList.Count > 0) {
            var query = from q in currentSelectedList
                        group q by new { q.Currency } into k
                        select new { k.Key.Currency };

            string total = string.Empty;
            decimal totalSum = 0;
            foreach (var row in query) {
                decimal CurrTotal = (decimal)0;
                CurrTotal += currentSelectedList.Where(w => w.Currency == row.Currency).Sum(s => (s.PayAmount.HasValue ? s.PayAmount.Value : (decimal)0));
                total += string.Format("<div style=\"width: 150px; text-align: right;\">{0}</div>", CurrTotal.ToString("#,###.00") + " " + row.Currency);
                totalSum += CurrTotal;
            }
            return new {
                show = totalSum > 0,
                innerHtml = total
            };
        } else {
            return new {
                show = false,
                innerHtml = ""
            };
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static object deleteSelectedList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\APMSelectList." + HttpContext.Current.Session.SessionID;
        if (File.Exists(path))
            File.Delete(path);

        List<AgencyPaymentMonitorFields> resList = ReadFile();
        foreach (AgencyPaymentMonitorFields row in resList)
            row.SelectedRow = false;

        CreateAndWriteFile(resList);

        return new {
            show = false,
            innerHtml = ""
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static object getJournalRef(string ResNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        bool localName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        StringBuilder sb = new StringBuilder();
        string errorMsg = string.Empty;

        List<ResPaymentsRecord> resPaymentList = new Reservation().getResPayment(UserData.Market, ResNo, ref errorMsg);
        if (resPaymentList.Count > 0) {
            sb.Append("<table id=\"gridResPaymentTable\" cellSpacing=\"0\" cellPadding=\"2\">");
            sb.Append("<tr class=\"gridResPaymentHeader\">");
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblReceiptType"));
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblPaymentType"));

            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblClientName"));

            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblReference"));
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblPaymentDate"));
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblAmount"));
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblCurr"));
            sb.AppendFormat("<td>{0}</td>", "&nbsp;");
            sb.Append("</tr>");
            bool alternatingRow = true;
            foreach (TvBo.ResPaymentsRecord row in resPaymentList) {
                Int16 recType = row.RecType.HasValue ? row.RecType.Value : Convert.ToInt16(-1);
                alternatingRow = alternatingRow ? false : true;
                sb.AppendFormat("<tr class=\"{0}\">", !alternatingRow ? "gridResPaymentRow" : "gridResPaymentRowAlternating");
                sb.AppendFormat("<td class=\"gridResPaymentReceipType\">{0}</td>", recType == 0 ? HttpContext.GetGlobalResourceObject("LibraryResource", "Payment").ToString() : (recType == 1 ? HttpContext.GetGlobalResourceObject("LibraryResource", "Refund").ToString() : "&nbsp;"));
                sb.AppendFormat("<td class=\"gridResPaymentPaymentType\">{0}</td>", localName ? row.PayTypeNameL : row.PayTypeName);
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                    sb.AppendFormat("<td class=\"gridResPaymentClientName\">{0}</td>", row.ClientName);
                sb.AppendFormat("<td class=\"gridResPaymentPaymentType\">{0}</td>", row.Reference);
                sb.AppendFormat("<td class=\"gridResPaymentPaymentDate\">{0}</td>", row.PayDate.HasValue ? row.PayDate.Value.ToShortDateString() : "&nbsp;");
                sb.AppendFormat("<td class=\"gridResPaymentAmount\">{0}</td>", row.PaidAmount.HasValue ? row.PaidAmount.Value.ToString("#,###.00") : "&nbsp;");
                sb.AppendFormat("<td class=\"gridResPaymentCur\">{0}</td>", row.PaidCur);
                sb.AppendFormat("<td class=\"gridResPaymentCur\"><img src=\"Images/print.png\" onclick=\"showReceipt({0})\"/></td>", row.JournalId);
                sb.Append("</tr>");
            }

            sb.Append("</table>");
            sb.Append("</fieldset>");
            return sb.ToString();
        } else
            return string.Empty;
    }    
}
