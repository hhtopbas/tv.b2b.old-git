﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Threading;
using System.Globalization;
using TvBo;
using System.Web.Script.Services;

public partial class OnlyExcursionResult : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string getGridHeaders()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<dataTableColumnsDef> aoColumns = new List<dataTableColumnsDef>();
        //96,18,30,200,30,100,80,50,50,130,100,30,28,28
        aoColumns.Add(new dataTableColumnsDef { IDNo = 1, ColName = "Book", sTitle = "&nbsp;", sClass = "center", sWidth = "10%", bSortable = false, bSearchable = false });        
        aoColumns.Add(new dataTableColumnsDef { IDNo = 2, ColName = "Name", sTitle = HttpContext.GetGlobalResourceObject("OnlyExcursion", "lblExcursionName").ToString(), sWidth = "35%", bSortable = false, bSearchable = false });        
        aoColumns.Add(new dataTableColumnsDef { IDNo = 3, ColName = "Location", sTitle = HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "Location").ToString(), sWidth = "20%", bSortable = false, bSearchable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 4, ColName = "CheckIn", sTitle = HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "CheckIn").ToString(), sWidth = "10%", bSortable = false, sType = "date", bSearchable = false });       
        aoColumns.Add(new dataTableColumnsDef { IDNo = 5, ColName = "Price", sTitle = HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "Price").ToString(), sWidth = "10%", bSortable = false, bSearchable = false, sClass="price" });
        var retVal = from q in aoColumns.AsEnumerable()
                     select new { sTitle = q.sTitle, sWidth = q.sWidth, bSortable = q.bSortable, sType = q.sType, sClass = q.sClass };
        getResult();
        return Newtonsoft.Json.JsonConvert.SerializeObject(retVal);
    }

    public static string getResult()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ExcuresionCriteria"] == null) return null;
        ExcursionSearchCriteria criteria = (ExcursionSearchCriteria)HttpContext.Current.Session["ExcuresionCriteria"];

        string errorMsg = string.Empty;        
        
        /*
        Guid? recID = null;
        String useLog = System.Configuration.ConfigurationManager.AppSettings["useLog"];
        if (!string.IsNullOrEmpty(useLog) && string.Equals(useLog, "1"))
        {
            string packType = string.Empty;
            switch (criteria.SType)
            {
                case SearchType.TourPackageSearch: packType = "T"; break;
                case SearchType.PackageSearch: packType = "H"; break;
                case SearchType.OnlyHotelSearch: packType = "O"; break;
            }
            recID = new WEBLog().saveWEBSearchLog(UserData.SID, UserData.UserID, UserData.AgencyID, SaleResource.B2B,
                            criteria.CheckIn, criteria.ExpandDate, criteria.NightFrom, criteria.NightTo, criteria.DepCity, criteria.ArrCity,
                            criteria.PriceFrom, criteria.PriceTo, criteria.Package, packType, null, ref errorMsg);
            if (recID.HasValue)
            {
                foreach (var row in criteria.RoomsInfo)
                    new WEBLog().saveWEBRoomInfoLog(recID.Value, row.Adult, row.Child, ref errorMsg);

                foreach (var row in criteria.Resort.Split('|'))
                    new WEBLog().saveWEBLocationLog(recID.Value, Conversion.getInt32OrNull(row.ToString()), ref errorMsg);

                foreach (var row in criteria.Hotel.Split('|'))
                    if (!string.IsNullOrEmpty(row))
                        new WEBLog().saveWEBHotelLog(recID.Value, row.ToString(), ref errorMsg);

                foreach (var row in criteria.Room.Split('|'))
                    if (!string.IsNullOrEmpty(row))
                        new WEBLog().saveWEBRoomLog(recID.Value, row.ToString(), ref errorMsg);

                foreach (var row in criteria.Board.Split('|'))
                    if (!string.IsNullOrEmpty(row))
                        new WEBLog().saveWEBBoardLog(recID.Value, row.ToString(), ref errorMsg);

                foreach (var row in criteria.Category.Split('|'))
                    if (!string.IsNullOrEmpty(row))
                        new WEBLog().saveWEBCategoryLog(recID.Value, row.ToString(), ref errorMsg);
            }
        }
        */
        List<ExcursionSearchResult> result = new ExcursionSearch().searchExcursion(UserData, criteria, ref errorMsg);
        
        if (result != null && result.Count > 0)
        {
            if (string.Equals(UserData.CustomRegID, Common.ctID_Falcon))
                result = result.OrderBy(o => o.NameL).ToList();
            CreateAndWriteFile(result);
            return string.Empty;
        }
        else
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\ExcursionSearch." + HttpContext.Current.Session.SessionID;
            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);
            return errorMsg;
        }
    }

    private static void CreateAndWriteFile(List<ExcursionSearchResult> result)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\ExcursionSearch." + HttpContext.Current.Session.SessionID;
        if (System.IO.File.Exists(path))
            System.IO.File.Delete(path);
        System.IO.FileStream f = new System.IO.FileStream(path, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
        try
        {
            System.IO.StreamWriter writer = new System.IO.StreamWriter(f);
            string compress = TvTools.GZipCompres.Compress(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            writer.Write(compress);
            writer.Close();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            f.Close();
        }
    }
}

