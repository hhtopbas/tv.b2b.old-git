﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlyTransferSearchFilter.aspx.cs" Inherits="OnlyTransferSearchFilter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Excursion</title>

  <script src="Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery.datepick/jquery-ui-i18n.js" type="text/javascript"></script>
  <script src="Scripts/jquery.json.js" type="text/javascript"></script>
  <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
  <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
  <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>
  <script src="Scripts/Tv.PackageSearchFilterV2.js" type="text/javascript"></script>
  <script src="Scripts/jquery.timepicker.min.js" type="text/javascript"></script>

  <link href="CSS/jquery.timepicker.css" rel="stylesheet" />
  <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="CSS/PriceSearchFilter.css" rel="stylesheet" type="text/css" />

  <script language="javascript" type="text/javascript">

    var twoLetterISOLanguageName = '<%= twoLetterISOLanguageName %>';
    var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
    var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
    var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
    var ComboSelect = '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
    var ComboAll = '<%=GetGlobalResourceObject("LibraryResource", "ComboAll") %>';

    if (typeof window.event != 'undefined')
      document.onkeydown = function () {
        if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
          return (event.keyCode != 8);
      }
    else
      document.onkeypress = function (e) {
        if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
          return (e.keyCode != 8);
      }

    function logout() {
      self.parent.logout();
    }

    var myWidth = 0, myHeight = 0;

    $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '</h1>';
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    function reSizeFrame() {
      self.parent.reSizeFilterFrame($('body').height());
    }

    function showDialogYesNo(msg) {
      $("#messages").html(msg);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        modal: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog('close');
            return true;
          }
        }, {
          text: btnCancel,
          click: function () {
            $(this).dialog('close');
            return false;
          }
        }]
      });
    }


    function showDialog(msg) {
      $("#messages").html(msg);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        modal: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog('close');
          }
        }]
      });
    }

    function showMessage(msg, transfer, trfUrl) {
      $(function () {
        $("#messages").html(msg);
        $("#dialog").dialog("destroy");
        $("#dialog-message").dialog({
          modal: true,
          buttons: [{
            text: btnOK,
            click: function () {
              $(this).dialog('close');
            }
          }, {
            text: btnCancel,
            click: function () {
              $(this).dialog('close');
            }
          }]
        });
      });
    }

    function getInf(adult, whois) {
      var Adult = parseInt(adult);
      $("#fltInfant" + whois).html("");
      for (i = 0; i < Adult + 1; i++) {
        $("#fltInfant" + whois).append("<option value='" + i.toString() + "'>" + i.toString() + "</option>");
      }
    }

    function SetChild(childCount, whois) {
      if (childCount > 0) $("#divRoomInfoChd1" + whois).show(); else $("#divRoomInfoChd1" + whois).hide();
      if (childCount > 1) $("#divRoomInfoChd2" + whois).show(); else $("#divRoomInfoChd2" + whois).hide();
      if (childCount > 2) $("#divRoomInfoChd3" + whois).show(); else $("#divRoomInfoChd3" + whois).hide();
      if (childCount > 3) $("#divRoomInfoChd4" + whois).show(); else $("#divRoomInfoChd4" + whois).hide();
    }

    function getChild(child, whois) {
      if ($("#divRoomInfoChd" + whois).length > 0) {
        var _divChild = $("#divRoomInfoChd" + whois);
        if (parseInt(child) == 0) {
          SetChild(parseInt(child), whois);
          _divChild.hide();
        }
        else {
          _divChild.show();
          SetChild(parseInt(child), whois);
        }
      }
    }

    function onChildChange(whois) {
      getChild($("#fltChild" + whois).val(), whois);
      reSizeFrame();
    }

    function getRoomPaxs() {
      var roomCount = parseInt($("#fltRoomCount").val());
      var result = '';
      for (i = 1; i < roomCount + 1; i++) {
        var adult = parseInt($("#fltAdult" + i.toString()).val());
        var child = parseInt($("#fltChild" + i.toString()).val());
        var chd1Age = parseInt($("#fltRoomInfoChd1" + i.toString()).val());
        var chd2Age = parseInt($("#fltRoomInfoChd2" + i.toString()).val());
        var chd3Age = parseInt($("#fltRoomInfoChd3" + i.toString()).val());
        var chd4Age = parseInt($("#fltRoomInfoChd4" + i.toString()).val());
        if (i > 1)
          result += ',(';
        else result += '(';
        result += '|Adult|:|' + adult.toString() + '|';
        result += ',|Child|:|' + child.toString() + '|';
        result += ',|Chd1Age|:|' + (child > 0 ? chd1Age : -1) + '|';
        result += ',|Chd2Age|:|' + (child > 1 ? chd2Age : -1) + '|';
        result += ',|Chd3Age|:|' + (child > 2 ? chd3Age : -1) + '|';
        result += ',|Chd4Age|:|' + (child > 3 ? chd4Age : -1) + '|';
        result += ')';
      }
      var hfRoomInfo = $("#hfRoomInfo");
      hfRoomInfo.val(result);
      reSizeFrame();
    }

    function filterclear() {
      self.parent.pageReLoad();
    }

    function setCriteria() {
      return;
      getRoomPaxs();
      var data = new Object();
      data.Direction = $("#fltDirection").val();
      data.CheckIn = $("#iCheckIn").datepicker('getDate').getTime();
      data.CheckOut = $("#iCheckOut").datepicker('getDate').getTime();
      data.RoomCount = $("#fltRoomCount").val();
      data.roomsInfo = $("#hfRoomInfo").val();
      data.Country = $("#fltCountry").val();
      data.Airport = $("#fltAirport").val();
      data.Resort = $("#fltResort").val();
      data.CurControl = ($("#CurrencyConvert")[0].checked ? 'Y' : 'N');
      data.CurrentCur = $("#currentCur").val();

      $.ajax({
        type: "POST",
        url: "OnlyTransferSearchFilter.aspx/SetCriterias",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getRoomInfo(setCrt) {
      var roomCount = $("#fltRoomCount").val();
      $.ajax({
        type: "POST",
        url: "OnlyTransferSearchFilter.aspx/getRoomInfo",
        data: '{"RoomCount":"' + roomCount + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#fltRoomInfo").html('');
          if (msg.hasOwnProperty('d') && msg.d != '') {
            $("#fltRoomInfo").html(msg.d);
          }
          reSizeFrame();
          if (setCrt == true)
            setCriteria();
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getResort() {
      var data = new Object();
      data.Airport = $("#fltAirport").val();
      data.Direction = $("#fltDirection").val();
      $.ajax({
        type: "POST",
        url: "OnlyTransferSearchFilter.aspx/getResort",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#fltResort").html('');
          $("#fltResort").append("<option value='' >" + ComboSelect + "</option>");
          if (msg.hasOwnProperty('d') && msg.d != null) {
            $.each(msg.d, function (i) {
              $("#fltResort").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function changeAirport() {
      $("#fltResort").html('');
      getResort();
    }

    function getAirports() {
      var data = new Object();
      data.Direction = $("#fltDirection").val();
      data.City = parseInt($("#fltCountry").val());
      $.ajax({
        type: "POST",
        url: "OnlyTransferSearchFilter.aspx/getAirports",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#fltAirport").html('');
          $("#fltAirport").append("<option value='' >" + ComboSelect + "</option>");
          if (msg.hasOwnProperty('d') && msg.d != null) {
            $.each(msg.d, function (i) {
              $("#fltAirport").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function changeCountry() {
      $("#fltAirport").html('');
      $("#fltResort").html('');
      getAirports();
    }

    function getCountry() {
      var data = new Object();
      data.Direction = $("#fltDirection").val();
      $.ajax({
        type: "POST",
        url: "OnlyTransferSearchFilter.aspx/getCountry",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var country = '';
          var first = true;
          $("#fltCountry").html('');
          $("#fltCountry").append("<option value='' >" + ComboSelect + "</option>");
          if (msg.hasOwnProperty('d') && msg.d != null) {
            $.each(msg.d, function (i) {
              if (this.Country != country) {
                if (!first) {
                  $("#fltCountry").append("</optgroup>");
                }
                $("#fltCountry").append("<optgroup label='" + this.CountryName + "'>");
                first = false;
                country = this.Country;
              }
              $("#fltCountry").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function onChange_fltDirection() {
      var value = $("#fltDirection").val();
      switch (value) {
        case 'F': // Forward
          $("#spCheckout").hide();
          $("#trCheckout").hide();
          break;
        case 'B': // Backward
          $("#spCheckout").hide();
          $("#trCheckout").hide();
          break;
        case 'R': // Round trip
          $("#spCheckout").show();
          $("#trCheckout").show();
          break;
      }
      $("#fltCountry").html('');
      $("#fltAirport").html('');
      $("#fltResort").html('');
      getCountry();
    }

    function onChange_fltTransferType() {

    }

    function search() {
      getRoomPaxs();
      var data = new Object();
      data.Direction = $("#fltDirection").val();
      data.CheckIn = $("#iCheckIn").datepicker('getDate').getTime();
      data.CheckOut = $("#iCheckOut").datepicker('getDate').getTime();
      data.RoomCount = $("#fltRoomCount").val();
      data.roomsInfo = $("#hfRoomInfo").val();
      data.Country = $("#fltCountry").val();
      data.Airport = $("#fltAirport").val();
      data.Resort = $("#fltResort").val();
      data.CurControl = ($("#CurrencyConvert")[0].checked ? 'Y' : 'N');
      data.CurrentCur = $("#currentCur").val();
      data.TrfType = $("#fltTransferType").val();
      $.ajax({
        type: "POST",
        url: "OnlyTransferSearchFilter.aspx/SetCriterias",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          self.parent.searchPrice();
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
      reSizeFrame();
    }

    function getFormData() {
      $.ajax({
        type: "POST",
        url: "OnlyTransferSearchFilter.aspx/getFormData",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var data = msg.d;
            if (data == null || data == undefined) {
              self.location = self.location;
              return false;
            }

            if (data.ShowTrfType) {
              $("#trfTypeDiv").show();
            } else {
              $("#trfTypeDiv").hide();
            }

            $("#fltTransferType").html('');
            $("#fltTransferType").append("<option value='' >" + ComboAll + "</option>");
            $.each(data.TransferTypes, function (i) {
              $("#fltTransferType").append("<option value='" + this.Code + "' >" + this.Name + "</option>");
            });

            $("#CustomRegID").val(data.CustomRegID);

            $("#fltRoomCount").html(''); $("#fltRoomCount").html(packageSearchFilterV2_fltRoomCount(data.fltRoomCount));

            $("#divCurrency").html('');
            $("#divCurrency").html(data.MarketCur);            

            var dateCIn = new Date(Date(eval(data.CheckIn)).toString());
            var dateCOut = new Date(Date(eval(data.CheckIn)).toString());
            $("#iCheckIn").datepicker('setDate', dateCIn);
            $("#iCheckOut").datepicker('setDate', dateCIn);

            getRoomInfo(true);
            onChange_fltDirection();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    $(document).ready(function () {

      $.datepicker.setDefaults($.datepicker.regional[twoLetterISOLanguageName != 'en' ? twoLetterISOLanguageName : 'en']);

      var minDate = new Date();
      var maxDate = new Date(minDate.getFullYear() + 2, minDate.getMonth(), minDate.getDate());
      $("#iCheckIn").datepicker({
        showOn: "button",
        buttonImage: "Images/Calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onSelect: function (dateText, inst) {
          if (dateText != '') {
            var dateCIn = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
            if ($("#iCheckOut").datepicker("getDate") != null) {
              var dateCOut = $("#iCheckOut").datepicker("getDate");
              if (dateCOut.getTime() < dateCIn.getTime()) {
                $("#iCheckOut").datepicker("setDate", dateCIn);
              }
            }
          }
        },
        minDate: minDate,
        maxDate: maxDate
      });
      $("#iCheckOut").datepicker({
        showOn: "button",
        buttonImage: "Images/Calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onSelect: function (dateText, inst) {
          if (dateText != '') {
            var dateCOut = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
            if ($("#iCheckIn").datepicker("getDate") != null) {
              var dateCIn = $("#iCheckIn").datepicker("getDate");
              if (dateCIn.getTime() > dateCOut.getTime()) {
                $("#iCheckIn").datepicker("setDate", dateCOut);
              }
            }
          }
        },
        minDate: minDate,
        maxDate: maxDate
      });
      getFormData();
    });

  </script>

</head>
<body>
  <form id="SearchFilterForm" runat="server">
    <input id="CustomRegID" type="hidden" value="" />
    <div style="width: 250px;">
      <table cellpadding="2" cellspacing="0" style="text-align: left; width: 250px;">
        <tr id="trfTypeDiv" class="ui-helper-hidden">
          <td>
            <strong><%= GetGlobalResourceObject("OnlyTransfer", "lblTransferType") %></strong>
            <br />
            <select id="fltTransferType" onchange="onChange_fltTransferType()" style="width: 80%;">
            </select>
          </td>
        </tr>
        <tr>
          <td style="height: 3px;">&nbsp;
          </td>
        </tr>
        <tr>
          <td>
            <strong><%= GetGlobalResourceObject("OnlyTransfer", "lblStartFromThe") %></strong>
            <br />
            <select id="fltDirection" onchange="onChange_fltDirection()" style="width: 80%;">
              <option value="F"><%= GetGlobalResourceObject("OnlyTransfer", "DirectionF") %></option>
              <option value="B"><%= GetGlobalResourceObject("OnlyTransfer", "DirectionB") %></option>
              <option value="R"><%= GetGlobalResourceObject("OnlyTransfer", "DirectionR") %></option>
            </select>
          </td>
        </tr>
        <tr>
          <td style="height: 3px;">&nbsp;
          </td>
        </tr>
        <tr>
          <td>
            <strong><%= GetGlobalResourceObject("OnlyTransfer", "lblCity") %></strong>
            <br />
            <select id="fltCountry" onchange="changeCountry()" style="width: 80%;">
            </select>
          </td>
        </tr>
        <tr>
          <td style="height: 3px;">&nbsp;
          </td>
        </tr>
        <tr>
          <td>
            <strong><%= GetGlobalResourceObject("OnlyTransfer", "lblAirport") %></strong>
            <br />
            <select id="fltAirport" onchange="changeAirport()" style="width: 80%;">
            </select>
          </td>
        </tr>
        <tr>
          <td style="height: 3px;">&nbsp;
          </td>
        </tr>
        <tr>
          <td>
            <strong><%= GetGlobalResourceObject("OnlyTransfer", "lblResort") %></strong>
            <br />
            <select id="fltResort" style="width: 80%;">
            </select>
          </td>
        </tr>
        <tr>
          <td style="height: 3px;">&nbsp;
          </td>
        </tr>
        <tr>
          <td>
            <div id="divCheckIn">
              <strong><%= GetGlobalResourceObject("OnlyTransfer", "lblTransferDate") %></strong>
              <br />
              <input id="iCheckIn" style="width: 100px;" />
            </div>
          </td>
        </tr>
        <tr id="spCheckout">
          <td style="height: 3px;">&nbsp;
          </td>
        </tr>
        <tr id="trCheckout">
          <td>
            <div id="divCheckOut">
              <strong><%= GetGlobalResourceObject("OnlyTransfer", "lblTransferReturnDate") %></strong>
              <br />
              <input id="iCheckOut" style="width: 100px;" />
            </div>
          </td>
        </tr>
        <tr>
          <td style="height: 3px;">&nbsp;
          </td>
        </tr>
        <tr>
          <td>
            <div style="display: none;">
              <strong>
                <%= GetGlobalResourceObject("PackageSearchFilter", "lblRoomCount") %></strong>
              <br />
              <select id="fltRoomCount" onchange="getRoomInfo(false);">
              </select>
            </div>
            <div id="divRoomInfo" style="width: 100%;">
              <div id="fltRoomInfo">
              </div>
            </div>
          </td>
        </tr>
        <tr>
          <td style="height: 3px;">&nbsp;
          </td>
        </tr>
        <tr>
          <td>
            <div id="divCurrency" style="clear: both; width: 100%;">
            </div>
          </td>
        </tr>
        <tr>
          <td style="height: 3px;">&nbsp;
          </td>
        </tr>
        <tr>
          <td style="z-index: 1; text-align: center;">
            <input type="button" name="btnSearch" value='<%= GetGlobalResourceObject("LibraryResource", "btnSearch") %>'
              style="width: 100px;" onclick="search()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />&nbsp;
                    <input type="button" name="btnClear" value='<%= GetGlobalResourceObject("LibraryResource", "btnClear") %>'
                      style="width: 100px;" onclick="filterclear()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
          </td>
        </tr>
      </table>
    </div>
    <div id="hiddenControls" style="display: none; visibility: hidden;">
      <input id="hfRoomInfo" type="hidden" />
    </div>
    <div id="dialog-listPopup" style="display: none;">
      <div id="divListPopup">
      </div>
    </div>
    <div id="dialog-message" title="" style="display: none;">
      <span id="messages">Message</span>
    </div>
  </form>
</body>
</html>
