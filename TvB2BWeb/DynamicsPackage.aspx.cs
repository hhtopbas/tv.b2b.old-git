﻿using System;
using System.Collections.Generic;
using System.Linq;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Services;
using TvTools;


namespace TvSearch
{
    public partial class DynamicsPackage : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User UserData = (User)Session["UserData"];
            CultureInfo ci = UserData.Ci;
            Thread.CurrentThread.CurrentCulture = ci;
        }

        [WebMethod(EnableSession = true)]
        public static string UserHasAuth()
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            return UserData.BlackList ? "N" : "Y";
        }

        [WebMethod(EnableSession = true)]
        public static string getFormData()
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
            string errorMsg = string.Empty;

            List<DynmcRouteToRecord> routeTo = new DynamicPack().getRouteTo(UserData, ref errorMsg);

            List<dynamicFormDataArrival> arr = (from q in routeTo
                                                orderby q.CountryNameL, q.CityNameL
                                                select new dynamicFormDataArrival { RecID = q.RecID, Name = q.CountryNameL + " -> " + q.CityNameL }).ToList<dynamicFormDataArrival>();

            dynamicFormData retObj = new dynamicFormData();
            retObj.Arrival = arr;

            string saleCur = string.Empty;
            currencyOptions curOpt = new currencyOptions();
            List<string> currencyList = curOpt.currencyList;
            currencyList = new List<string>();
            currencyList.Add(UserData.SaleCur);
            if (UserData.TvParams.TvParamPrice.IndSaleCurOpt.HasValue && UserData.TvParams.TvParamPrice.IndSaleCurOpt.Value == 2)
            {
                List<CurrencyRecord> currList = new Banks().getCurrencys(UserData.Market, ref errorMsg);
                if (currList != null && currList.Count() > 0)
                {
                    foreach (CurrencyRecord row in currList.Where(w => w.Code != UserData.SaleCur))
                        currencyList.Add(row.Code);
                }
            }
            Int16? saleCurOpt = new Reservation().getIndSaleCurOption(UserData);
            if (saleCurOpt.HasValue && saleCurOpt.Value == 1) saleCurOpt = 0;
            curOpt.showCurrencyDiv = false;
            curOpt.currencyCheck = false;
            curOpt.curOption = saleCurOpt.HasValue ? saleCurOpt.Value : Convert.ToInt16(0);
            if (!saleCurOpt.HasValue)
            {
                curOpt.currency = UserData.SaleCur;
                curOpt.showCurrencyDiv = false;
            }
            else
            {
                if (saleCurOpt.Value == 1)
                {
                    curOpt.currency = string.Empty;
                    curOpt.showCurrencyDiv = true;
                }
                else
                    if (saleCurOpt.Value == 2)
                    {
                        curOpt.currency = UserData.SaleCur;
                        curOpt.showCurrencyDiv = true;
                    }
                    else
                    {
                        curOpt.showCurrencyDiv = false;
                        curOpt.currency = UserData.SaleCur;
                    }
            }
            string indSaleCur = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "IndSaleCurr"));
            if (!string.IsNullOrEmpty(indSaleCur))
                curOpt.currency = indSaleCur;

            curOpt.typeLock = false;
            curOpt.currencyList = currencyList;

            retObj.Currency = curOpt;
            return Newtonsoft.Json.JsonConvert.SerializeObject(retObj);
        }

        [WebMethod(EnableSession = true)]
        public static string getBookReservation(int? Adult, int? Inf, int? Child, int? ArrCity, string Curr)
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci;
            Thread.CurrentThread.CurrentUICulture = UserData.Ci;
            HttpContext.Current.Session["ResData"] = null;

            string errorMsg = string.Empty;
            ResDataRecord ResData = new ResDataRecord();

            List<SearchResult> selectBook = ResData.SelectBook;
            SearchResult book = new SearchResult();
            book.ArrCity = ArrCity;
            book.ArrCityName = new Locations().getLocationName(UserData.Market, ArrCity.HasValue ? ArrCity.Value : Convert.ToInt32(-1), NameType.NormalName, ref errorMsg);
            book.ArrCityNameL = new Locations().getLocationName(UserData.Market, ArrCity.HasValue ? ArrCity.Value : Convert.ToInt32(-1), NameType.LocalName, ref errorMsg);
            book.DepCity = book.ArrCity;
            book.DepCityName = book.ArrCityName;
            book.DepCityNameL = book.ArrCityNameL;
            book.CheckIn = DateTime.Today;
            book.CheckOut = DateTime.Today.AddDays(1);
            book.Night = 1;
            book.HAdult = Convert.ToInt16(Adult.HasValue ? Adult.Value : 1);
            book.HChdAgeG1 = Inf.HasValue ? Convert.ToInt16(Inf.Value) : Convert.ToInt16(0);
            book.HChdAgeG2 = Child.HasValue ? Convert.ToInt16(Child.Value) : Convert.ToInt16(0); ;
            book.HChdAgeG3 = 0;
            book.HChdAgeG4 = 0;
            book.ChdG1Age1 = Convert.ToDecimal(0);
            book.ChdG1Age2 = Convert.ToDecimal(199/100);
            book.ChdG2Age1 = Convert.ToDecimal(2);
            book.ChdG2Age2 = Convert.ToDecimal(1599 / 100);
            //book.ChdG3Age1 = Convert.ToDecimal(0);
            //book.ChdG3Age2 = Convert.ToDecimal(0);
            //book.ChdG4Age1 = Convert.ToDecimal(0);
            //book.ChdG4Age2 = Convert.ToDecimal(0);

            book.SaleCur = string.IsNullOrEmpty(Curr) ? UserData.SaleCur : Curr;
            book.NetCur = book.SaleCur;
            book.PLCur = book.SaleCur;
            book.Operator = UserData.Operator;
            book.Market = UserData.Market;
            book.RefNo = 1;
            selectBook.Add(book);

            ResData = new DynamicPack().GenerateResMain(UserData, ResData, ref errorMsg);
            ResData = new Reservation().GenerateTourists(UserData, ResData, 1, ref errorMsg);
            ResData.ResCust[0].Leader = "Y";
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka))
            {
                ResMainRecord resMain = ResData.ResMain;
                resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
            }
            HttpContext.Current.Session["ResData"] = ResData;

            return "";
        }
    }
}

[Serializable()]
public class dynamicFormDataArrival
{
    public int? RecID { get; set; }
    public string Name { get; set; }
}


[Serializable()]
public class dynamicFormData
{
    public List<dynamicFormDataArrival> Arrival { get; set; }
    public currencyOptions Currency { get; set; }
}
