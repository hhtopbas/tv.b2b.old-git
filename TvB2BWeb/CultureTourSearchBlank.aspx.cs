﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;

public partial class CultureTourSearchBlank : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        if (!IsPostBack)
        {
            Session["SearchPLData"] = null;
            Session["Criteria"] = null;
            Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url);//Session["Menu"] = "CultureTourSearchBlank.aspx";
            Response.Redirect("~/CultureTourSearch.aspx" + Request.Url.Query);
        }
    }
}
