﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MakeReservationV2.aspx.cs"
    Inherits="MakeReservationV2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "MakeReservation") %></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <%--<script src="Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>--%>
    <script src="Scripts/jquery.maskedinput.js?ver=1" type="text/javascript"></script>

    <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="Scripts/stickyFloat.js" type="text/javascript"></script>

    <script src="Scripts/checkDate.js" type="text/javascript"></script>

    <script src="Scripts/linq.js" type="text/javascript"></script>

    <script src="Scripts/jquery.linq.js" type="text/javascript"></script>

    <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>

    <script src="Scripts/selectToUISlider.jQuery.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/ui.slider.extras.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="CSS/MakeReservationV2.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script type="text/javascript">
        var isMobile = false;
        var reservationSaved = false;

        var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
        var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
        var btnGotoResCard = '<%= GetGlobalResourceObject("LibraryResource", "btnGotoResCard") %>';
        var btnGotoSearchResult ='<%= GetGlobalResourceObject("LibraryResource", "btnGotoSearchResult") %>';
        var btnPaymentPage = '<%= GetGlobalResourceObject("LibraryResource", "btnPaymentPage") %>';
        var btnYes = '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>';
        var btnNo = '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>';

        var msgWhereInvoiceTo = '<%= GetGlobalResourceObject("LibraryResource", "WhereInvoiceTo") %>';
        var msglblAlreadyRes = '<%= GetGlobalResourceObject("BookTicket", "lblAlreadyRes") %>';
        var msgEnterChildOrInfantBirtDay = '<%= GetGlobalResourceObject("LibraryResource", "EnterChildOrInfantBirtDay") %>';
        var msglblNoResult = '<%= GetGlobalResourceObject("BookTicket", "lblNoResult") %>';
        var msgviewOldResSalePrice = '<%= GetGlobalResourceObject("Controls", "viewOldResSalePrice") %>';
        var msgviewNewResSalePrice = '<%= GetGlobalResourceObject("Controls", "viewNewResSalePrice") %>';
        var msgcancelService = '<%= GetGlobalResourceObject("LibraryResource", "cancelService") %>';
        var ComboSelect = '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
        var ForAgency = '<%=GetGlobalResourceObject("LibraryResource", "ForAgency") %>';
        var ForPassenger = '<%=GetGlobalResourceObject("LibraryResource", "ForPassenger") %>';
        var msglblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
        var msgInvalidBirthDate = '<%= GetGlobalResourceObject("LibraryResource","InvalidBirthDate").ToString() %>';

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }


        function logout() {
            self.parent.logout();
        }


        $(document).ajaxStart(function () {
            $.blockUI({
                message: '<h1>' + msglblPleaseWait + '</h1>'
            });
        }).ajaxStop(function () {
            $.unblockUI();
            return false;
        });

        var _dateMask = '';
        var _dateFormat = '';

        function SetAge(Id, cinDate, _formatDate) {
            try {
                var birthDate = dateValue(_formatDate, $("#iBirthDay" + Id).val());
                if (birthDate == null) {
                    $("#iBirthDay" + Id).val('');
                    if ($("#iAge" + Id).length > 0)
                        $("#iAge" + Id).val('');
                    return;
                }
                var checkIN = dateValue(_formatDate, cinDate);
                if (checkIN == null) return;
                var _minBirthDate = new Date(1, 1, 1910);
                var _birthDate = birthDate;
                var _cinDate = checkIN;
                if (_birthDate < _minBirthDate) {
                    if ($("#iAge" + Id).length > 0)
                        $("#iAge" + Id).val('');
                    birthDate = '';
                    showMsg(msgInvalidBirthDate);
                    return;
                }
                var age = Age(_birthDate, _cinDate);
                if ($("#iAge" + Id).length > 0)
                    $("#iAge" + Id).val(age);
            }
            catch (err) {
                $("#iAge" + Id).val('');
            }
        }

        function showDialogYesNo(msg) {
            var _buttons = [{
                text: btnOK,
                click: function () {
                    $(this).dialog('close');
                    return;
                }
            }, {
                text: btnCancel,
                click: function () {
                    $(this).dialog('close');
                    return;
                }
            }];

            $("#messages").html(msg);
            var _maxWidth = 880;
            var _maxHeight = 500;
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                maxWidth: _maxWidth,
                maxHeight: _maxHeight,
                modal: true,
                buttons: _buttons
            });

        }

        function showMsg(msg) {
            var _buttons = [{
                text: btnOK,
                click: function () {
                    $(this).dialog('close');
                }
            }];

            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                autoResize: true,
                resizable: true,
                resizeStop: function (event, ui) {
                    $(this).dialog({ position: 'center' });
                },
                buttons: _buttons
            });
        }
        function showMsgGotoSearchResultPageOrGotoResPage(msg, ResNo) {
            var _buttons = [{
                text: btnOK,
                click: function () {
                    $(this).dialog('close');
                    window.close;
                    self.parent.gotoResViewPage(ResNo);
                }
            }, {
                text: btnGotoSearchResult,
                click: function () {
                    $(this).dialog('close');
                    window.close;
                    self.parent.cancelMakeRes();
                }
            }
            ];

            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                autoResize: true,
                resizable: true,
                resizeStop: function (event, ui) {
                    $(this).dialog({ position: 'center' });
                },
                buttons: _buttons
            });
        }

        function showDialog(msg, transfer, trfUrl) {
            var _buttons = [{
                text: btnOK,
                click: function () {
                    $(this).dialog('close');
                    if (transfer == true) {
                        var url = trfUrl;
                        $(location).attr('href', url);
                    }
                }
            }];

            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                maxWidth: 880,
                maxHeight: 500,
                modal: true,
                buttons: _buttons
            });
        }

        function showDialogEndGotoPaymentPage(msg, ResNo) {

            var _buttons =
                [{
                    text: btnGotoResCard,
                    click: function () {
                        $(this).dialog('close');
                        window.close;
                        self.parent.gotoResViewPage(ResNo);
                    }
                }, {
                    text: btnPaymentPage,
                    click: function () {
                        $(this).dialog('close');
                        window.close;
                        self.parent.paymentPage(ResNo);
                    }
                }];

            $('<div>' + msg + '</div>').dialog({
                open: function () {
                    $(this).parent().children().children("a.ui-dialog-titlebar-close").remove();
                },
                closeOnEscape: false,
                buttons: _buttons
            });

        }

        function showDialogEndExit(msg, ResNo) {

            var _buttons =
                [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                        window.close;
                        if (ResNo != '') {
                            self.parent.gotoResViewPage(ResNo);
                        }
                        else {
                            self.parent.cancelMakeRes();
                        }
                    }
                }];
            $("#messages").html('');
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                resizable: true,
                autoResize: true,
                buttons: _buttons,
                open: function () {
                    $(this).parent().children().children("a.ui-dialog-titlebar-close").remove();
                },
                closeOnEscape: false
            });
        }

        function showMessage(msg, transfer, trfUrl) {

            var _buttons =
                [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                    }
                }, {
                    text: btnCancel,
                    click: function () {
                        $(this).dialog('close');
                    }
                }];
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                maxWidth: 880,
                maxHeight: 500,
                modal: true,
                buttons: _buttons
            });
        }

        function cancelMakeRes() {
            window.close;
            self.parent.cancelMakeRes();
        }

        function editResService(serviceUrl) {
            setCustomers();
            $("#dialog-resServiceEdit").dialog("open");
            $("#resServiceEdit").attr("src", serviceUrl);
            return false;
        }

        function returnEditResServices(save) {
            if (save == true) {
                $("#dialog-resServiceEdit").dialog("close");
                getResMainDiv(false, false);
            }
            else {
                $("#dialog-resServiceEdit").dialog("close");
            }
        }

        function getCustomers() {
            var cust = '';
            var hfCustCount = parseInt($("#hfCustCount").val());
            var custCount = parseInt(hfCustCount);
            var custList = [];
            for (var i = 1; i <= custCount; i++) {
                var cust = new Object();
                cust.SeqNo = parseInt($("#iSeqNo" + i).text());
                cust.Title = $("#iTitle" + i).length > 0 ? parseInt($("#iTitle" + i).val()) : null;
                cust.Surname = $("#iSurname" + i).length > 0 ? $("#iSurname" + i).val() : '';
                cust.SurnameL = $("#iSurnameL" + i).length > 0 ? $("#iSurnameL" + i).val() : '';
                cust.Name = $("#iName" + i).length > 0 ? $("#iName" + i).val() : '';
                cust.NameL = $("#iNameL" + i).length > 0 ? $("#iNameL" + i).val() : '';
                cust.BirtDay = $("#iBirthDay" + i).length > 0 ? $("#iBirthDay" + i).val() : '';
                cust.Age = $("#iAge" + i).length > 0 ? $("#iAge" + i).val() : '';
                cust.IDNo = $("#iIDNo" + i).length > 0 ? $("#iIDNo" + i).val() : '';
                cust.PassSerie = $("#iPassSerie" + i).length > 0 ? $("#iPassSerie" + i).val() : '';
                cust.PassNo = $("#iPassNo" + i).length > 0 ? $("#iPassNo" + i).val() : '';
                cust.PassIssueDate = $("#iPassIssueDate" + i).length > 0 ? $("#iPassIssueDate" + i).val() : '';
                cust.PassExpDate = $("#iPassExpDate" + i).length > 0 ? $("#iPassExpDate" + i).val() : '';

                if ($("#iPassGiven" + i).length > 0) {
                    if ($("#iPassGiven" + i).children("option").length > 0 && $("#iPassGiven" + i).val() == "") {
                        var passindex = $("#iPassGiven" + i).children("option:selected").index();
                        cust.PassGiven = passindex > 0 ? $("#iPassGiven" + i).children("option:selected").text(): "";
                    } else
                        cust.PassGiven = $("#iPassGiven" + i).val();
                } else
                    cust.PassGiven = '';
                cust.Phone = $("#iPhone" + i).length > 0 ? $("#iPhone" + i).val() : '';
                cust.Nation = $("#iNation" + i).length > 0 ? parseInt($("#iNation" + i).val()) : null;
                cust.Nationality = $("#iNationality" + i).length > 0 ? $("#iNationality" + i).val() : null;
                cust.Passport = $("#iHasPassport" + i).length > 0 ? $("#iHasPassport" + i)[0].checked : true;
                cust.Leader = $('input[name=Leader]:checked').val() == cust.SeqNo;
                cust.ResCustInfo = null;
                var addressInfo = $("#resCustInfo" + i)
                if ($("#resCustInfo" + i).length > 0) {
                    if (cust.Leader) {
                        var custInfo = new Object();
                        custInfo.CTitle = $("#CTitle" + i).length > 0 ? $("#CTitle" + i).val() : '';
                        custInfo.CName = $("#CName" + i).length > 0 ? $("#CName" + i).val() : '';
                        custInfo.CSurName = $("#CSurName" + i).length > 0 ? $("#CSurName" + i).val() : '';
                        custInfo.MobTel = ($("#MobTelCountryCode" + i).length > 0 ? $("#MobTelCountryCode" + i).text() : '') + ($("#MobTel" + i).length > 0 ? $("#MobTel" + i).val() : '');
                        custInfo.ContactAddr = 'H';
                        custInfo.InvoiceAddr = 'H';
                        custInfo.AddrHome = $("#AddrHome" + i).length > 0 ? $("#AddrHome" + i).val() : '';
                        custInfo.AddrHomeCity = $("#AddrHomeCity" + i).length > 0 ? $("#AddrHomeCity" + i).val() : '';
                        custInfo.AddrHomeCountry = $("#AddrHomeCountry" + i).length > 0 ? $("#AddrHomeCountry" + i).val() : '';

                        custInfo.AddrHomeZip = $("#AddrHomeZip" + i).length > 0 ? $("#AddrHomeZip" + i).val() : '';
                        custInfo.AddrHomeTel = ($("#AddrHomeTelCountryCode" + i).length > 0 ? $("#AddrHomeTelCountryCode" + i).text() : '') + ($("#AddrHomeTel" + i).length > 0 ? $("#AddrHomeTel" + i).val() : '');
                        custInfo.AddrHomeFax = ($("#AddrHomeFaxCountryCode" + i).length > 0 ? $("#AddrHomeFaxCountryCode" + i).text() : '') + ($("#AddrHomeFax" + i).length > 0 ? $("#AddrHomeFax" + i).val() : '');
                        custInfo.AddrHomeEmail = $("#AddrHomeEmail" + i).length > 0 ? $("#AddrHomeEmail" + i).val() : '';
                        custInfo.HomeTaxOffice = $("#HomeTaxOffice" + i).length > 0 ? $("#HomeTaxOffice" + i).val() : '';
                        custInfo.HomeTaxAccNo = $("#HomeTaxAccNo" + i).length > 0 ? $("#HomeTaxAccNo" + i).val() : '';
                        custInfo.WorkFirmName = $("#WorkFirmName" + i).length > 0 ? $("#WorkFirmName" + i).val() : '';
                        custInfo.AddrWork = $("#AddrWork" + i).length > 0 ? $("#AddrWork" + i).val() : '';
                        custInfo.AddrWorkCity = $("#AddrWorkCity" + i).length > 0 ? $("#AddrWorkCity" + i).val() : '';
                        custInfo.AddrWorkCountry = $("#AddrWorkCountry" + i).length > 0 ? $("#AddrWorkCountry" + i).val() : '';
                        custInfo.AddrWorkZip = $("#AddrWorkZip" + i).length > 0 ? $("#AddrWorkZip" + i).val() : '';
                        custInfo.AddrWorkTel = ($("#AddrWorkTelCountryCode" + i).length > 0 ? $("#AddrWorkTelCountryCode" + i).text() : '') + ($("#AddrWorkTel" + i).length > 0 ? $("#AddrWorkTel" + i).val() : '');
                        custInfo.AddrWorkFax = ($("#AddrWorkFaxCountryCode" + i).length > 0 ? $("#AddrWorkFaxCountryCode" + i).text() : '') + ($("#AddrWorkFax" + i).length > 0 ? $("#AddrWorkFax" + i).val() : '');
                        custInfo.AddrWorkEMail = $("#AddrWorkEMail" + i).length > 0 ? $("#AddrWorkEMail" + i).val() : '';
                        custInfo.WorkTaxOffice = $("#WorkTaxOffice" + i).length > 0 ? $("#WorkTaxOffice" + i).val() : '';
                        custInfo.WorkTaxAccNo = $("#WorkTaxAccNo" + i).length > 0 ? $("#WorkTaxAccNo" + i).val() : '';
                        custInfo.Bank = $("#Bank" + i).length > 0 ? $("#Bank" + i).val() : '';
                        custInfo.BankAccNo = $("#BankAccNo" + i).length > 0 ? $("#BankAccNo" + i).val() : '';
                        custInfo.BankIBAN = $("#BankIBAN" + i).length > 0 ? $("#BankIBAN" + i).val() : '';

                        cust.ResCustInfo = custInfo;
                    }
                }
                custList.push(cust);
                //if (cust.length > 0) cust += "@";
                //cust += record;
            }
            return $.json.encode(custList);
        }

        function selectPromomotion() {
            setCustomers();
            var url = 'Promotion.aspx';
            $("#dialog-promoSelect").dialog("open");
            $("#promoSelect").attr("src", url);
        }

        function returnSelectedPromotion(data) {
            if (data == '') {
                $("#dialog-promoSelect").dialog("close");
                saveReservation(data);
            }
            else {
                $("#dialog-promoSelect").dialog("close");
                saveReservation(data);
            }
        }

        function setCustomers() {
            var data = new Object();
            data.data = $.json.decode(getCustomers());
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservationV2.aspx/setCustomers",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 'OK') {
                        return true;
                    } else
                        if (msg.d == 'NO') {
                            return false;
                        }
                        else {
                            showMessage(msg.d);
                            return false;
                        }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                    return false;
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function blockUISave() {
            $("#dialog-SaveReservation").dialog('open');
        }

        function unBlockUISave() {
            $("#dialog-SaveReservation").dialog('close');
        }

        function saveMakeRes(selectedPromo) {

            if ($("#divWhereInvoice").css('display') == 'block') {
                if ($("#iInvoiceTo").val() == '' || $("#iInvoiceTo").val() == undefined) {
                    showMsg(msgWhereInvoiceTo);
                    return;
                }
            }

            var _buttons =
                [{
                    text: btnYes,
                    click: function () {
                        $(this).dialog('close');
                        window.close();
                    }
                }, {
                    text: btnNo,
                    click: function () {
                        $(this).dialog('close');
                        window.close;
                        if (retVal.Promotion) {
                            selectPromomotion();
                        } else {
                            saveReservation('');
                        }
                    }
                }];

            setCustomers();
            var data = new Object();
            var customerCode = $("#iCustomerCode").length > 0 ? $("#iCustomerCode").val() : '';
            var dosier = $("#iDosier").length > 0 ? $("#iDosier").val() : '';
            data.Honeymoon = $("#cbHoneymoon")[0].checked;
            data.PromoCode = $("#iPromoCode").val();
            data.Customers = getCustomers();
            data.Discount = $("#iAgencyDiscount").val();
            if ($("#divFixNote").css("display").toLowerCase() == "block") {
                var _fixResNote = $("#iFixNote").val() != '' ? $("#iFixNote").val() : '';
                data.ResNote = _fixResNote != null && _fixResNote != '' ? _fixResNote.toString().replace(/\"/g, '|') : '';
            }
            else {
                data.ResNote = $("#iNote").length > 0 ? $("#iNote").val().replace(/\"/g, "|") : '';
            }
            data.aceCustomerCode = customerCode;
            data.aceDosier = dosier;
            data.Code1 = $("#iCode1").length > 0 ? $("#iCode1").val() : '';
            data.Code2 = $("#iCode2").length > 0 ? $("#iCode2").val() : '';
            data.Code3 = $("#iCode3").length > 0 ? $("#iCode3").val() : '';
            data.Code4 = $("#iCode4").length > 0 ? $("#iCode4").val() : '';
            $.ajax({
                //async: false,
                type: "POST",
                url: "MakeReservationV2.aspx/getPromoList",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var retVal = msg.d;
                    if (retVal.Err) {
                        showMsg(retVal.ErrorMsg);
                    }
                    else if (retVal.SaleableService) {
                        $('<div>' + retVal.SaleableServiceMsg + '</div>').dialog({
                            buttons: _buttons
                        });
                    }
                    else if (retVal.Promotion) {
                        selectPromomotion();
                    }
                    else {
                        saveReservation('');
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function saveReservation(selectedPromo) {
            if ($("#divWhereInvoice").css('display') == 'block') {
                if ($("#iInvoiceTo").val() == '' || $("#iInvoiceTo").val() == undefined) {
                    showMsg(msgWhereInvoiceTo);
                    return;
                }
            }


            setCustomers();
            if (reservationSaved) {
                showMsg(msglblAlreadyRes);
                return;
            }
            else
                reservationSaved = true;
            var agencyBonus = $("#agencyBonus").length > 0 ? $("#agencyBonus").val() : "";
            var userBonus = $("#userBonus").length > 0 ? $("#userBonus").val() : "";
            var data = new Object();
            data.data = $.json.decode(getCustomers());
            data.SelectedPromo = selectedPromo;
            data.AgencyBonus = agencyBonus;
            data.UserBonus = userBonus;
            data.invoiceTo = $("#iInvoiceTo").val();
            data.saveOption = ($("#iSaveOption").attr('checked') != undefined) ? ($("#iSaveOption").attr('checked') == 'checked' ? true : false) : false;
            data.bonusID = $("#iBonusID").length > 0 ? $("#iBonusID").val() : '';
            data.optionTime = $("#withOption").length > 0 && $("#withOption").attr("checked") == 'checked' ? '1' : '0';
            data.goAhead = $("#goAhead").val() == '1' ? true : false;
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservationV2.aspx/saveReservation",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var retVal = $.json.decode(msg.d)[0];
                    if (retVal.ControlOK == true) {
                        if (retVal.GotoSearchResult)
                            showMsgGotoSearchResultPageOrGotoResPage(retVal.Message, retVal.ResNo);
                        else if (retVal.GotoPaymentPage == true)
                            showDialogEndGotoPaymentPage(retVal.Message, retVal.ResNo);
                        else if (retVal.GotoReservation)
                            showDialogEndExit(retVal.Message, retVal.ResNo);
                        else showDialogEndExit(retVal.Message, '');
                        $("#goAhead").val('0');
                    }
                    else {
                        if (retVal.OtherReturnValue == '32') {
                            showMsg(retVal.Message);
                            $("#goAhead").val('1');
                            reservationSaved = false;
                        }
                        else {
                            showMsg(retVal.Message);
                            reservationSaved = false;
                            $("#goAhead").val('0');
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                    reservationSaved = false;
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function cancelMakeRes() {
            window.close;
            self.parent.cancelMakeRes();
        }

        function checkCheckedAccept() {
            if ($('#msgAccept').attr('checked')) {
                if ($('#msgAcceptCustom').length > 0)
                    if ($('#msgAcceptCustom').attr('checked'))
                        $("#btnSave").show();
                    else
                        $("#btnSave").hide();
                else
                    $("#btnSave").show();
            }
            else {
                $("#btnSave").hide();
            }
        }

        function clickMsgAccept() {
            checkCheckedAccept();
        }
        function clickMsgAcceptCustom() {
            checkCheckedAccept();
        }

        function getHandicaps() {
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservationV2.aspx/getHandicaps",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divHandicap").html('');
                    $("#divHandicap").html(msg.d);
                    checkCheckedAccept();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getExtrasDiv() {
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservationV2.aspx/getExtrasDiv",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divExtras").html('');
                    $("#divExtras").html(msg.d);
                    getHandicaps();
                    firstAddRemoveServiceExt();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getResServiceDiv() {
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservationV2.aspx/getResServiceDiv",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divResService").html('');
                    $("#divResService").html(msg.d);
                    getExtrasDiv();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function homeCountryChanged(idNo) {
            var country = $("#AddrHomeCountry" + idNo).val();
            var countryList = $("#countryList").val();

            var queryResult = $.Enumerable.From($.json.decode(countryList))
                .Where(function (x) { return x.CountryNameL == country })
                .Select("$.CountryPhoneCode")
                .First();
            if (queryResult != '') {
                $("#MobTelCountryCode" + idNo).text('+' + queryResult);
                $("#AddrHomeTelCountryCode" + idNo).text('+' + queryResult);
                $("#AddrHomeFaxCountryCode" + idNo).text('+' + queryResult);
            } else {
                $("#MobTelCountryCode" + idNo).text('');
                $("#AddrHomeTelCountryCode" + idNo).text('');
                $("#AddrHomeFaxCountryCode" + idNo).text('');
            }
        }

        function leaderChange(idNo) {
            var leader = $("#iLeader" + idNo);
            var hfCustCount = parseInt($("#hfCustCount").val());
            var custCount = parseInt(hfCustCount);
            var custList = [];
            for (var i = 1; i <= custCount; i++) {
                var addressInfo = $("#resCustInfo" + i);
                if (i == idNo)
                    addressInfo.show();
                else addressInfo.hide();
            }
        }

        function onSurnameExit(elmID) {
            var surnameList = $(".nameSurnameCss");
            var elmValue = $("#" + elmID).val();
            var custNo = elmID.replace('iSurname', '');
            surnameList.each(function (index) {
                $("#iTitle" + custNo + " option:selected").text();
                var custNoIdx = $(this).attr('custNo');
                if (custNoIdx == custNo) {
                    var newValue = $("#iTitle" + $(this).attr('custNo') + " option:selected").text() + ". ";
                    newValue += $("#" + elmID).val() + " ";
                    newValue += $("#iName" + custNo).val();
                    $(this).html(newValue);
                }
            });
        }

        function onNameExit(elmID) {
            var surnameList = $(".nameSurnameCss");
            var elmValue = $(elmID).val();
            var custNo = elmID.replace('iName', '');
            surnameList.each(function (index) {
                $("#iTitle" + custNo + " option:selected").text();
                var custNoIdx = $(this).attr('custNo');
                if (custNoIdx == custNo) {
                    var newValue = $("#iTitle" + $(this).attr('custNo') + " option:selected").text() + ". ";
                    newValue += $("#iSurname" + custNo).val() + " ";
                    newValue += $("#" + elmID).val();
                    $(this).html(newValue);
                }
            });
        }

        function changeCustExtPrice(elm) {
            var id = elm.id.split("_");
            var serviceID = id[1];
            var custID = id[2];
            var checked = $('#' + elm.id).attr('checked') == 'checked';

            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservationV2.aspx/getResCustExtPrice",
                data: '{"serviceID":' + serviceID + ',"custID":' + custID + ',"selected":' + checked + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#saleExtPrice_" + serviceID).html(msg.d);
                    getResMainDiv(true, false);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changeCustPrice(elm) {
            var id = elm.id.split("_");
            var serviceID = id[1];
            var custID = id[2];
            var checked = $('#' + elm.id).attr('checked') == 'checked';

            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservationV2.aspx/getResCustPrice",
                data: '{"serviceID":' + serviceID + ',"custID":' + custID + ',"selected":' + checked + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#salePrice_" + serviceID).html(msg.d);
                    getResMainDiv(true, false);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getResCustDiv(onlyResCust) {
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservationV2.aspx/getResCustdiv",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divResCust").html('');
                    $("#divResCust").html(msg.d);
                    var _dateMask = $("#dateMask").val();
                    if (!isMobile)
                        $(".formatDate").mask(_dateMask);
                    if ($(".mobPhone").length > 0 && $("#mobPhoneMask").val() != '') {
                        $(".mobPhone").mask($("#mobPhoneMask").val());
                    }
                    if ($(".phone").length > 0 && $("#phoneMask").val() != '') {
                        $(".phone").mask($("#phoneMask").val());
                    }
                    if (onlyResCust == true) return;
                    getResServiceDiv();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getResMainDiv(onlyResMain, onlyResCust) {
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservationV2.aspx/getResMainDiv",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d')) {
                        var retVal = msg.d;
                        $("#divResMain").html('');
                        $("#divResMain").html(retVal.data);
                        if (onlyResMain == true) {
                            for (var i = 0; i < retVal.ppPrice.length; ++i) {
                                $("#ppSalePrice" + retVal.ppPrice[i].Code).html(retVal.ppPrice[i].Name);
                            }
                            return;
                        }
                        isMobile = retVal.isMobile;
                        if (retVal.mobPhoneMask != '') {
                            $("#mobPhoneMask").val(retVal.mobPhoneMask);
                        }
                        if (retVal.phoneMask != '') {
                            $("#phoneMask").val(retVal.phoneMask);
                        }

                        getResCustDiv(onlyResCust);

                        if (onlyResCust == true) {
                            return;
                        }
                        if (retVal.statusPromotion != "1") {
                            $("#divPromotion").hide();
                        }
                        else {
                            $("#divPromotion").show();
                            $("#iPromoCode").val(retVal.PromoCode);
                        }
                        if (retVal.divACE) $("#divACE").show();
                        if (retVal.divDiscountAgencyCom) $("#divDiscountAgencyCom").show();
                        if (retVal.divWhereInvoice) $("#divWhereInvoice").show();
                        $("#divSaveOption").html('');
                        $("#divSaveOption").html(retVal.divSaveOptionDiv);
                        if (retVal.divSaveOption) $("#divSaveOption").show();
                        if (retVal.divResNote) $("#divNote").show();
                        if (retVal.showSpecialCode == true) {
                            $("#divSpecialCodes").show();
                        } else {
                            $("#divSpecialCodes").hide();
                        }

                        if (retVal.divFixResNote) {
                            $("#divFixNote").show();
                            $("#iFixNote").html(retVal.ResNote);
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function returnCustInfoEdit(data, source) {
            if (source == "save") {
                $.ajax({
                    type: "POST",
                    url: "MakeReservationV2.aspx/setResCustInfo",
                    data: '{"data":"' + data + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (source == "save")
                            getResCustDiv(false);
                        $("#dialog-resCustomerAddress").dialog("close");
                    },
                    error: function (xhr, msg, e) {
                        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                            showMsg(xhr.responseText);
                    },
                    statusCode: {
                        408: function () {
                            logout();
                        }
                    }
                });
            }
            else {
                $("#dialog-resCustomerAddress").dialog("close");
            }
        }

        function showCustAddress(CustNo) {
            var data = new Object();
            data.data = $.json.decode(getCustomers());
            $.ajax({
                type: "POST",
                url: "MakeReservationV2.aspx/setResCustomers",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var custInfoUrl = 'CustomerAddress.aspx?CustNo=';

                    $("#dialog-resCustomerAddress").dialog("open");
                    $("#resCustomerAddress").attr("src", custInfoUrl + CustNo);
                    return false;
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function returnCustDetailEdit(dataS, source) {
            if (source == "save") {
                var data = new Object();
                data.data = dataS;
                $.ajax({
                    type: "POST",
                    url: "MakeReservationV2.aspx/setResCustomer",
                    data: $.json.encode(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (source == "save")
                            getResCustDiv(false);
                        $("#dialog-resCustOtherInfo").dialog("close");
                    },
                    error: function (xhr, msg, e) {
                        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                            showMsg(xhr.responseText);
                    },
                    statusCode: {
                        408: function () {
                            logout();
                        }
                    }
                });
            }
            else {
                $("#dialog-resCustOtherInfo").dialog("close");
            }
        }

        function showResCustInfo(CustNo) {
            var data = new Object();
            data.data = $.json.decode(getCustomers());
            $.ajax({
                type: "POST",
                url: "MakeReservationV2.aspx/setResCustomers",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var custInfoUrl = 'CustomerDetail.aspx?CustNo=';

                    $("#dialog-resCustOtherInfo").dialog("open");
                    $("#resCustOtherInfo").attr("src", custInfoUrl + CustNo);
                    return false;
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function returnSsrc(cancel, refresh) {
            if (cancel == true) {
                $("#dialog-Ssrc").dialog("close");
            }
            else {
                $("#dialog-Ssrc").dialog("close");
                if (refresh) {
                    getResMainDiv(false, false);
                    promotionDiv();
                }
                else {
                    showMsg(msg.d);
                }
            }
        }

        function showSSRC(CustNo) {
            setCustomers();
            var serviceUrl = 'Controls/Ssrc.aspx?CustNo=' + CustNo + '&Save=false';
            $("#dialog-Ssrc").dialog("open");
            $("#Ssrc").attr("src", serviceUrl);
        }

        function deleteCust(CustNo) {

        }

        function showExtServiceDetail(idNo) {
            $("#extraservice" + idNo).hide();
            $("#extraserviceDetail" + idNo).show();
        }

        function hideExtServiceDetail(idNo) {
            $("#extraserviceDetail" + idNo).hide();
            $("#extraservice" + idNo).show();
        }

        function returnAddResServices(reLoad, msg) {
            $("#dialog-resServiceAdd").dialog("close");
            if (reLoad == true)
                getResMainDiv(false, false);
        }

        function checkUsers() {
            var retVal = true;
            var CustList = $.json.decode(getCustomers());
            try {
                var i = 0;
                for (i = 0; i < CustList.length; i++) {
                    if (CustList[i].Title > 5 && CustList[i].BirtDay == '') {
                        retVal = false;
                    }
                }
            }
            catch (e) {
                retVal = false;
            }
            return retVal;
        }

        function showAddResService(serviceUrl) {
            var data = new Object();
            data.data = $.json.decode(getCustomers());
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservationV2.aspx/setCustomers",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var chkUsers = checkUsers();
                    if (chkUsers || msg.d == 'OK' || msg.d == 'NO') {
                        $("#dialog-resServiceAdd").dialog("open");
                        if (serviceUrl.toString().indexOf('?') > -1) {
                            $("#resServiceAdd").attr("src", serviceUrl + '&recordType=temp');
                        }
                        else {
                            $("#resServiceAdd").attr("src", serviceUrl + '?recordType=temp');
                        }
                    }
                    else {
                        showMsg(msgEnterChildOrInfantBirtDay);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showMsg(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function returnAddResServicesExt(save) {
            if (save == true) {
                $("#dialog-resServiceExtAdd").dialog("close");
                getResMainDiv(false, false);
            }
            else {
                $("#dialog-resServiceExtAdd").dialog("close");
            }
        }

        function searchSelectCust(CustNo) {
            $("#dialog-searchCust").dialog("close");
            var Custs = $.json.decode(getCustomers());  //getCustomers();
            var data = new Object();
            data.CustNo = CustNo;
            data.Custs = Custs;
            $.ajax({
                type: "POST",
                url: "MakeReservationV2.aspx/copyCustomer",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 'OK') {
                        getResMainDiv(false, true);
                        onSurnameExit($("#iSurname" + CustNo.split(';')[2])[0].id);
                        onNameExit($("#iName" + CustNo.split(';')[2])[0].id);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function searchCustListShow(_html) {
            $(function () {
                $("#searchCust").html('');
                $("#searchCust").html(_html);
                $("#dialog").dialog("destroy");
                $("#dialog-searchCust").dialog({
                    modal: true,
                    width: 650,
                    height: 450,
                    resizable: true,
                    autoResize: true
                });
            });
        }

        function searchCust(i) {
            $("#dialog").dialog("destroy");
            $("#searchCust").attr("src", "CustomerSearch.aspx?refNo=" + i + "&SeqNo=" + $("#iSeqNo" + i).text());
            $("#dialog-searchCust").dialog({
                modal: true,
                width: 750,
                height: 500
            });
        }

        function searchCustGetList(i) {
            var Surname = $("#iSurname" + i).val();
            var Name = $("#iName" + i).val();
            var BirthDate = $("#iBirthDay" + i).val();
            var SeqNo = $("#iSeqNo" + i).text();
            $.ajax({
                type: "POST",
                url: "MakeReservationV2.aspx/searchCustomer",
                data: '{"refNo":"' + i + '","SeqNo":"' + SeqNo + '","Surname":"' + Surname + '","Name":"' + Name + '","BirthDate":"' + BirthDate + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        if (msg.d.toString().substring(0, 4) == "Err:")
                            showMsg(msg.d.toString().substring(4));
                        else searchCustListShow(msg.d);
                    }
                    else {
                        showMsg(msglblNoResult);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function showAddResServiceExt(serviceUrl) {
            setCustomers();
            var data = new Object();
            data.data = $.json.decode(getCustomers());
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservationV2.aspx/setCustomers",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var chkUsers = checkUsers();
                    if (chkUsers) {
                        $("#dialog-resServiceExtAdd").dialog("open");
                        $("#resServiceExtAdd").attr("src", serviceUrl + '?recordType=temp');
                    } else {
                        showMsg(msgEnterChildOrInfantBirtDay);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                    return false;
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function promotionDiv() {
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservationV2.aspx/getPromotionDiv",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d.split(';')[0] == "0") {
                        $("#divPromotion").hide();
                    }
                    else {
                        $("#divPromotion").show();
                        if (msg.d.split(';')[1] == "1")
                            $("#divPromotionHoneymoon").show();
                        else $("#divPromotionHoneymoon").hide();
                        if (msg.d.split(';')[2] == "1")
                            $("#divPromotionPromotionCode").show();
                        else $("#divPromotionPromotionCode").hide();
                    }

                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function removeResService(recID) {
            setCustomers();
            $.ajax({
                type: "POST",
                url: "MakeReservationV2.aspx/removeService",
                data: '{"RecID":' + recID + ',"forPrice":false}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 'OK') {
                        getResMainDiv(false, false);
                        promotionDiv();
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function removeResServiceMsg(recID) {
            $.ajax({
                type: "POST",
                url: "MakeReservationV2.aspx/removeService",
                data: '{"RecID":' + recID + ',"forPrice":true}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var data = $.json.decode(msg.d);
                    var _html = '';
                    _html += msgviewOldResSalePrice;
                    _html += '<br />';
                    _html += '<strong>' + data.OldPrice + '</strong>';
                    _html += '<br />';
                    _html += msgviewNewResSalePrice;
                    _html += '<br />'
                    _html += '<strong>' + data.NewPrice + '</strong>';
                    _html += '<br />';
                    _html += msgcancelService;

                    var _buttons =
                        [{
                            text: btnYes,
                            click: function () {
                                $(this).dialog('close');
                                removeResService(recID);
                                return true;
                            }
                        }, {
                            text: btnNo,
                            click: function () {
                                $(this).dialog('close');
                                return false;
                            }
                        }];

                    $("#messages").html(_html);
                    $("#dialog").dialog("destroy");
                    $("#dialog-message").dialog({
                        maxWidth: 880,
                        maxHeight: 500,
                        modal: true,
                        buttons: _buttons
                    });
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function removeResServiceExt(recID) {
            setCustomers();
            $.ajax({
                type: "POST",
                url: "MakeReservationV2.aspx/removeServiceExt",
                data: '{"RecID":' + recID + ',"forPrice":false}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 'OK') {
                        getResMainDiv(false, false);
                        promotionDiv();
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function removeResServiceExtMsg(recID) {
            $.ajax({
                type: "POST",
                url: "MakeReservationV2.aspx/removeServiceExt",
                data: '{"RecID":' + recID + ',"forPrice":true}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var data = $.json.decode(msg.d);
                    var _html = '';
                    _html += msgviewOldResSalePrice;
                    _html += '<br />';
                    _html += '<strong>' + data.OldPrice + '</strong>';
                    _html += '<br />';
                    _html += msgviewNewResSalePrice;
                    _html += '<br />'
                    _html += '<strong>' + data.NewPrice + '</strong>';
                    _html += '<br />';
                    _html += msgcancelService;

                    var _buttons =
                        [{
                            text: btnYes,
                            click: function () {
                                $(this).dialog('close');
                                removeResServiceExt(recID);
                                return true;
                            }
                        }, {
                            text: btnNo,
                            click: function () {
                                $(this).dialog('close');
                                return false;
                            }
                        }];

                    $("#messages").html(_html);
                    $("#dialog").dialog("destroy");
                    $("#dialog-message").dialog({
                        maxWidth: 880,
                        maxHeight: 500,
                        modal: true,
                        buttons: _buttons
                    });
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changePickup(pickupPoint, serviceID) {
            $.ajax({
                type: "POST",
                url: "MakeReservationV2.aspx/setPickupPoint",
                data: '{"PickupPoint":"' + pickupPoint + '","ServiceID":"' + serviceID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == '') { }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changeFlight() {
            setCustomers();
            var serviceUrl = 'ChangePLFlight.aspx';
            $("#dialog-changeFlight").dialog("open");
            $("#changeFlight").attr("src", serviceUrl);
        }

        function retutnChangeFlight() {
            $("#dialog-changeFlight").dialog("close");
            getResMainDiv(false, false);
            promotionDiv();
        }

        function serviceDateChanged(elm) {
            var startDay = $("#" + elm.id).val();
            var id = elm.id.split("_");
            var recID = id[1];
            var begStartDay = null;
            var endStartDay = null;
            if (id[0] == 'serviceBegDate')
                begStartDay = parseInt($("#" + elm.id).val());
            if (id[0] == 'serviceEndDate')
                endStartDay = parseInt($("#" + elm.id).val());
            $.ajax({
                type: "POST",
                url: "MakeReservationV2.aspx/updateDate",
                data: '{"ServiceID":' + recID + ',"ServiceExtID":null,"begStartDay":' + begStartDay + ',"endStartDay":' + endStartDay + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == '') {
                        getResMainDiv(true, false);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function firstAddRemoveServiceExt() {
            $("input:checkbox[name='serviceExtSelect']").each(function () {
                if ($(this).attr('Comp') == 'true') {
                    $(this).attr('checked', 'checked');
                    addremoveServiceExt($(this).val());
                }
            });
        }

        function addremoveServiceExt(recID) {
            setCustomers();
            var recID2 = null;
            var changeService = $("#serviceExtSelect_" + recID.toString());

            if (changeService != undefined && changeService != null) {
                var chkSel = changeService.attr('checked') ? true : false;
                var Code = changeService.attr('Code');
                var RTSale = changeService.attr('RTSale');
                var FlightDir = changeService.attr('FlightDir');
                var Comp = changeService.attr('Comp');
                var ServCat = changeService.attr('ServCat');
                if (chkSel) {
                    $("input:checkbox[name='custExtPrice_" + recID + "']").each(function () {
                        $(this).attr('checked', 'checked');
                    });
                    $("#custPriceExtUL_" + recID.toString()).show();
                    if (RTSale == 'true' && FlightDir == 'true') {
                        $("input:[name='serviceExtSelect']").each(function () {
                            if ($(this).attr('Code') == Code) {
                                $(this).attr('checked', 'checked');
                                var RecID = $(this).attr('RecID');
                                var disabled = $(this).attr('disabled') == 'disabled' ? true : false;
                                recID2 = RecID != undefined && RecID != '' ? RecID : null;
                                $("input:checkbox[name='custExtPrice_" + RecID + "']").each(function () {
                                    $(this).attr('checked', 'checked');
                                    if (disabled) $(this).attr('disabled', 'disabled');
                                });
                                $("#custPriceExtUL_" + RecID.toString()).show();
                            }
                        });
                        if (Comp == 'true') {
                            $("input:[ServCat='" + ServCat + "']").each(function () {
                                if ($(this).attr('Code') != Code) {
                                    $(this).removeAttr('checked');
                                    $("#custPriceExtUL_" + $(this).val()).hide();
                                }
                            });
                        }
                        else {
                            $("input:[ServCat='" + ServCat + "']").each(function () {
                                if ($(this).attr('Comp') == 'true') {
                                    $(this).removeAttr('checked');
                                    $("#custPriceExtUL_" + $(this).val()).hide();
                                }
                            });
                        }
                    }
                }
                else {
                    $("input:checkbox[name='custExtPrice_" + recID + "']").each(function () {
                        $(this).removeAttr('checked');
                    });
                    $("#custPriceExtUL_" + recID.toString()).hide();
                    if (RTSale == 'true' && FlightDir == 'true') {
                        $("input:[name='serviceExtSelect']").each(function () {
                            if ($(this).attr('Code') == Code) {
                                $(this).removeAttr('checked');
                                var RecID = $(this).attr('RecID');
                                recID2 = RecID != undefined && RecID != '' ? RecID : null;
                                $("input:checkbox[name='custExtPrice_" + RecID + "']").each(function () {
                                    $(this).removeAttr('checked');
                                });
                                $("#custPriceExtUL_" + RecID.toString()).hide();
                            }
                        });
                        if (ServCat == '1' && $("input:[ServCat='" + ServCat + "'][checked='checked']").length == 0) {
                            $("input:[ServCat='" + ServCat + "']").each(function () {
                                if ($(this).attr('Comp') == 'true') {
                                    $(this).attr('checked', 'checked');
                                    $("#custPriceExtUL_" + $(this).val()).show();
                                }
                            });
                        }
                    }
                }

                var obj = new Object();
                obj.data = $.json.decode(getCustomers());
                obj.ServiceID = null;
                obj.ServiceExtID = recID;
                obj.ServiceExtID2 = recID2;
                obj.ChkSel = chkSel;

                $.ajax({
                    async: false,
                    type: "POST",
                    url: "MakeReservationV2.aspx/reCalcPrice",
                    data: $.json.encode(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != null && msg.d.ReCalcOk == true) {
                            getResMainDiv(true, false);
                            var salePrice = msg.d.SalePrice;
                            $("#saleExtPrice_" + recID).html(salePrice);
                        } else {
                            if (msg.d != null && msg.d.ReCalcOk == false) {
                                showMessage(msg.d.ErrorMsg);
                            }
                        }
                    },
                    error: function (xhr, msg, e) {
                        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                            showMsg(xhr.responseText);
                    },
                    statusCode: {
                        408: function () {
                            logout();
                        }
                    }
                });
            }
        }

        function addremoveService(recID) {
            setCustomers();
            if (checkUsers() == false) {
                var changeService = $("#serviceSelect_" + recID.toString());
                if (changeService != undefined && changeService != null) {
                    var chkSel = false;
                    if (chkSel) {
                        $("input:checkbox[name='custPrice_" + recID + "']").each(function () {
                            $(this).attr('checked', 'checked');
                        });
                        $("#custPriceUL_" + recID.toString()).show();
                    }
                    else {
                        $("input:checkbox[name='custPrice_" + recID + "']").each(function () {
                            $(this).removeAttr('checked');
                        });
                        $("#custPriceUL_" + recID.toString()).hide();
                    }
                    $("#serviceSelect_" + recID.toString()).removeAttr('checked');
                }
                showMsg(msgEnterChildOrInfantBirtDay);
                return;
            }
            else {
                var changeService = $("#serviceSelect_" + recID.toString());
                if (changeService != undefined && changeService != null) {
                    var chkSel = changeService.attr('checked') ? true : false;
                    if (chkSel) {
                        $("input:checkbox[name='custPrice_" + recID + "']").each(function () {
                            $(this).attr('checked', 'checked');
                        });
                        $("#custPriceUL_" + recID.toString()).show();
                    }
                    else {
                        $("input:checkbox[name='custPrice_" + recID + "']").each(function () {
                            $(this).removeAttr('checked');
                        });
                        $("#custPriceUL_" + recID.toString()).hide();
                    }
                    var obj = new Object();
                    obj.data = $.json.decode(getCustomers());
                    obj.ServiceID = recID;
                    obj.ServiceExtID = null;
                    obj.ServiceExtID2 = null;
                    obj.ChkSel = chkSel;
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "MakeReservationV2.aspx/reCalcPrice",
                        data: $.json.encode(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (msg.d != null && msg.d.ReCalcOk == true) {
                                getResMainDiv(true, false);
                                var salePrice = msg.d.SalePrice;
                                $("#salePrice_" + recID).html(salePrice);
                            } else {
                                if (msg.d != null && msg.d.ReCalcOk == false) {
                                    var chkSel = false;
                                    if (chkSel) {
                                        $("input:checkbox[name='custPrice_" + recID + "']").each(function () {
                                            $(this).attr('checked', 'checked');
                                        });
                                        $("#custPriceUL_" + recID.toString()).show();
                                    }
                                    else {
                                        $("input:checkbox[name='custPrice_" + recID + "']").each(function () {
                                            $(this).removeAttr('checked');
                                        });
                                        $("#custPriceUL_" + recID.toString()).hide();
                                    }
                                    $("#serviceSelect_" + recID.toString()).removeAttr('checked');
                                    showMessage(msg.d.ErrorMsg);
                                }
                            }
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                showMsg(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                }
            }
        }

        function dialogInitialize() {
            $("#dialog-Ssrc").dialog({ autoOpen: false, modal: true, width: 640, height: 550, resizable: true, autoResize: true });
            $("#dialog-resServiceEdit").dialog({ autoOpen: false, modal: true, width: 675, height: 600, resizable: true, autoResize: true });
            $("#dialog-promoSelect").dialog({ autoOpen: false, modal: true, width: 820, height: 440, resizable: true, autoResize: true });
            $("#dialog-resCustOtherInfo").dialog({ autoOpen: false, modal: true, width: 440, height: 440, resizable: true, autoResize: true });
            $("#dialog-resCustomerAddress").dialog({ autoOpen: false, modal: true, width: 600, height: 535, resizable: false, autoResize: true });
            $("#dialog-resServiceAdd").dialog({ autoOpen: false, modal: true, width: 675, height: 600, resizable: true, autoResize: true });
            $("#dialog-resServiceExtAdd").dialog({ autoOpen: false, modal: true, width: 870, height: 525, resizable: true, autoResize: true });
            $("#dialog-changeFlight").dialog({ autoOpen: false, modal: true, width: 900, height: 525, resizable: true, autoResize: true });
        }

        function onClickOptionTime() {
            if ($("#withOption").length > 0) {
                if ($("#withOption").attr("checked") == "checked") {
                    $("#withOptionMsg").show();
                }
                else {
                    $("#withOptionMsg").hide();
                }
            }
        }

        $(document).ready(function () {
            $.query = $.query.load(location.href);
            if ($.query.get('forb2c') == '1')
                self.parent.b2cRes();
            $("#iInvoiceTo").append("<option value='' >" + ComboSelect + "</option>");
            $("#iInvoiceTo").append("<option value='0' >" + ForAgency + "</option>");
            $("#iInvoiceTo").append("<option value='1' >" + ForPassenger + "</option>");

            $("#iAgencyDiscount").on('change', function () {
                $("#iAgencyDiscount").val($("#iAgencyDiscount").val().replace('-', ''));
            });

            $.ajax({
                type: "POST",
                url: "MakeReservationV2.aspx/userBlackList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == '') {
                        getResMainDiv(false, false);
                        promotionDiv();
                        $('#divResMain').stickyfloat({ duration: 400 });
                        if ($("#makeResV2Header").length > 0) {
                            divResMainInnerHeight = $('#makeResV2Header').height() + 10;
                            divResMainInnerWidth = $('#makeResV2Header').width();
                            $("#topDiv").height(divResMainInnerHeight + 'px');
                            $("#topDiv").width(divResMainInnerWidth + 'px');
                        }
                        else {
                            $('#topDiv').height('175px');
                            $('#topDiv').width('900px');
                        }

                        top.window.moveTo(0, 0);
                    }
                    else { showMsg(msg.d); }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });

            dialogInitialize();
        });

    </script>

</head>
<body>
    <form id="MakeReservationV2Form" runat="server">
        <input id="phoneMask" type="hidden" />
        <input id="mobPhoneMask" type="hidden" />
        <input id="goAhead" type="hidden" value="0" />
        <div style="width: 900px" class="ui-widget">
            <div id="divResMain" class="ui-helper-clearfix ui-widget-content ui-corner-all">
            </div>
            <div id="topDiv">
                &nbsp;
            </div>
            <div>
                <div style="text-align: center; clear: both;">
                    <div id="divResCust" class="resCustGridCss">
                        <table cellpadding="0" cellspacing="0" width="100%">
                        </table>
                    </div>
                </div>
                <br />
                <div style="text-align: center;">
                    <div id="divResService">
                    </div>
                </div>
            </div>
            <div>
                <div id="divExtras">
                </div>
            </div>
            <div id="divSpecialCodes" class="divPromotionCss ui-helper-hidden">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td id="Td1" style="width: 25%;">
                            <strong>BR. FISKALNOG RACUNA </strong>
                        </td>
                        <td id="Td2" style="width: 75%;">
                            <input id="iCode1" type="text" maxlength="10" style="width: 75px; border: solid 1px #333;" />
                        </td>
                    </tr>
                    <tr>
                        <td id="Td3" style="width: 25%;">
                            <strong>DATUM IZDAVANJA FISK. RAC.</strong>
                        </td>
                        <td id="Td4" style="width: 75%;">
                            <input id="iCode2" type="text" maxlength="10" style="width: 75px; border: solid 1px #333;" />
                        </td>
                    </tr>
                    <tr>
                        <td id="Td5" style="width: 25%;">
                            <strong>UPLACEN IZNOS U DINARIMA</strong>
                        </td>
                        <td id="Td6" style="width: 75%;">
                            <input id="iCode3" type="text" maxlength="10" style="width: 75px; border: solid 1px #333;" />
                        </td>
                    </tr>
                    <tr>
                        <td id="Td7" style="width: 25%;">
                            <strong>NAPOMENA</strong>
                        </td>
                        <td id="Td8" style="width: 75%;">
                            <input id="iCode4" type="text" maxlength="10" style="width: 75px; border: solid 1px #333;" />
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <div class="divButtons">
                    <div id="divPromotion" class="divPromotionCss" style="display: none;">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td id="divPromotionHoneymoon" style="width: 50%;">
                                    <strong>
                                        <%= GetGlobalResourceObject("MakeReservation", "lblHoneymoon") %>: </strong>
                                    <input id="cbHoneymoon" type="checkbox" />
                                </td>
                                <td id="divPromotionPromotionCode" style="width: 50%;">
                                    <strong>
                                        <%= GetGlobalResourceObject("MakeReservation", "lblPromotionCode") %>: </strong>
                                    <input id="iPromoCode" type="text" style="width: 75px; border: solid 1px #333;" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="text-align: center;">
                        <div id="divHandicap">
                        </div>
                    </div>
                    <div style="text-align: center;">
                        <div id="divNote" style="display: none; text-align: left;">
                            <strong>
                                <%= GetGlobalResourceObject("BookTicket", "saveDivNote") %>: </strong>
                            <br />
                            <input id="iNote" type="text" style="width: 95%; height: 30px;" />
                        </div>
                        <div id="divFixNote" style="display: none; text-align: left;">
                            <strong>
                                <%= GetGlobalResourceObject("BookTicket", "saveDivNote") %>: </strong>
                            <br />
                            <select id="iFixNote" style="width: 450px;">
                            </select>
                        </div>
                        <div id="divACE" style="display: none;">
                            <div style="width: 100%; height: 5px;">
                                &nbsp;
                            </div>
                            <div style="float: left; width: 150px; text-align: right; padding-left: 100px; margin-top: 3px;">
                                <strong>
                                    <%= GetGlobalResourceObject("BookTicket", "saveDivCustomerCode")%>: </strong>
                            </div>
                            <div style="float: left; width: 150px; text-align: left;">
                                <input id="iCustomerCode" type="text" />
                            </div>
                            <div style="float: left; width: 150px; text-align: right; margin-top: 3px;">
                                <strong>
                                    <%= GetGlobalResourceObject("BookTicket", "saveDivDosier")%>: </strong>
                            </div>
                            <div style="float: left; width: 150px; text-align: left;">
                                <input id="iDosier" type="text" />
                            </div>
                        </div>
                        <div id="divDiscountAgencyCom" style="clear: both; display: none;">
                            <div style="width: 100%; height: 5px;">
                                &nbsp;
                            </div>
                            <div style="float: left; width: 160px; text-align: right; padding-left: 80px; margin-top: 3px;">
                                <strong><%= GetGlobalResourceObject("MakeReservation", "agencyDiscount")%>(%): </strong>
                            </div>
                            <div style="float: left; width: 145px; text-align: left;">
                                <input id="iAgencyDiscount" type="text" />
                            </div>
                        </div>
                        <div id="divWhereInvoice" style="display: none;">
                            <strong>
                                <%= GetGlobalResourceObject("MakeReservation", "whereInvoice")%>:</strong>
                            <select id="iInvoiceTo" style="width: 200px;">
                            </select>
                        </div>
                        <div id="divSaveOption" style="display: none;">
                        </div>
                    </div>
                    <div style="width: 100%; height: 5px;">
                        &nbsp;
                    </div>
                    <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource","btnSaveReservation").ToString() %>'
                        onclick="saveMakeRes('');" style="min-width: 100px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                    &nbsp;&nbsp;
                <input id="btnCancel" type="button" value='<%= GetGlobalResourceObject("LibraryResource","btnCancel").ToString() %>'
                    onclick="cancelMakeRes();" style="min-width: 100px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                </div>
            </div>
            <br />
        </div>
        <div id="dialog-message" title="" style="display: none;">
            <span id="messages">Message</span>
        </div>
        <div id="dialog-resCustomerAddress" title='<%= GetGlobalResourceObject("MakeReservation", "lblCustomerAddress") %>'
            style="display: none;">
            <iframe id="resCustomerAddress" runat="server" height="490px" width="570px" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-resCustOtherInfo" title='<%= GetGlobalResourceObject("MakeReservation", "lblCustomerOtherInfo") %>'
            style="display: none;">
            <iframe id="resCustOtherInfo" runat="server" height="400px" width="410px" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-promoSelect" title='<%= GetGlobalResourceObject("MakeReservation", "lblSelectPromotion") %>'
            style="display: none;">
            <iframe id="promoSelect" runat="server" height="400px" width="795px" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-resServiceAdd" title='<%= GetGlobalResourceObject("MakeReservation", "lblService") %>'
            style="display: none;">
            <iframe id="resServiceAdd" runat="server" height="100%" width="100%" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-resServiceExtAdd" title='<%= GetGlobalResourceObject("MakeReservation", "lblExtService") %>'
            style="display: none;">
            <iframe id="resServiceExtAdd" runat="server" height="100%" width="830px" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-Ssrc" title='<%= GetGlobalResourceObject("MakeReservation", "lblSpecialServiceReqCode") %>'
            style="display: none;">
            <iframe id="Ssrc" runat="server" height="100%" width="100%" frameborder="0" style="clear: both;"></iframe>
        </div>
        <div id="dialog-searchCust" title='<%= GetGlobalResourceObject("MakeReservation", "searchCustLabel") %>'
            style="display: none;">
            <iframe id="searchCust" runat="server" height="100%" width="100%" frameborder="0"
                style="clear: both;"></iframe>
            <%--<div id="searchCust">
            </div--%>>
        </div>
        <div id="dialog-resServiceEdit" title='<%= GetGlobalResourceObject("ResView", "lblEditService") %>'
            style="display: none;">
            <iframe id="resServiceEdit" runat="server" height="100%" width="100%" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-changeFlight" title='<%= GetGlobalResourceObject("ResView", "lblEditService") %>'
            style="display: none;">
            <iframe id="changeFlight" runat="server" height="100%" width="100%" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-SaveReservation" title='<%= GetGlobalResourceObject("MakeReservation", "Information") %>'
            style="display: none; text-align: center;">
            <h3>
                <%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait")%></h3>
        </div>
    </form>
</body>
</html>
"