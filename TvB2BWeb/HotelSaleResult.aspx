﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HotelSaleResult.aspx.cs"
    Inherits="HotelSaleResult" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "PackageSearchResult")%></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="Scripts/json2.js" type="text/javascript"></script>
    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script src="Scripts/jquery.textTruncate.js" type="text/javascript"></script>
    <script src="Scripts/NumberFormat154.js" type="text/javascript"></script>
    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
    <script src="Scripts/jquery.cluetip.js" type="text/javascript"></script>
    <script src="Scripts/mustache.js" type="text/javascript"></script>
    <script src="Scripts/smartpaginator.js?v=4" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/HotelSaleResult.css?v=4" rel="stylesheet" type="text/css" />
    <link href="CSS/jquery.cluetip.css" rel="stylesheet" type="text/css" />
    <link href="CSS/smartpaginator.css?v=3" rel="stylesheet" />

    <style type="text/css">
        #divNote { height: 30px; color: #CC0000; font-weight: bold; text-decoration: underline; cursor: help; }
    </style>

    <script language="javascript" type="text/javascript">

        var templatePath = '<%= templatePath %>';

        var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
        var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
        var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
        var CurrencyTypeNotTheSame = '<%= GetGlobalResourceObject("PackageSearchResult", "CurrencyTypeNotTheSame") %>';
        var PlsSelectTheSameFlight = '<%= GetGlobalResourceObject("PackageSearchResult", "PlsSelectTheSameFlight") %>';
        var stopSale = '<%= GetGlobalResourceObject("PackageSearchResult", "stopSale") %>';
        var lblbook = '<%= GetGlobalResourceObject("PackageSearchResult", "book") %>';
        var sFirst = '<%= GetGlobalResourceObject("LibraryResource", "sFirst") %>';
        var sPrevious = '<%= GetGlobalResourceObject("LibraryResource", "sPrevious") %>';
        var sNext = '<%= GetGlobalResourceObject("LibraryResource", "sNext") %>';
        var sLast = '<%= GetGlobalResourceObject("LibraryResource", "sLast") %>';
        var sPage = '<%= GetGlobalResourceObject("LibraryResource", "sPage") %>';
        var sGotoPage = '<%= GetGlobalResourceObject("LibraryResource", "sGotoPage") %>';

        var cultureNumber;
        var cultureDate;

        $(document).ajaxStart(function () {
            $.blockUI({
                message: '<h1>' + lblPleaseWait + '</h1>'
            });
        }).ajaxStop(function () {
            $.unblockUI();
        });

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }


        function logout() {
            self.parent.logout();
        }

        function showHotelInfo(_hotelUrl) {
            window.open(_hotelUrl);
        }

        function tryNumberFormat(obj) {
            var nf = new NumberFormat(obj);
            if (cultureNumber != null) {
                nf.PERIOD = cultureNumber.CurrencyGroupSeparator;
                nf.COMMA = cultureNumber.CurrencyDecimalSeparator;
            }
            var rsdoS = nf.PERIOD;
            var rsdoD = nf.COMMA;
            nf.setPlaces(2);
            nf.setSeparators(true, rsdoS, rsdoD);
            obj = nf.toFormatted();
            return obj;
        }

        function showDialog(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: [{
                        text: btnOK,
                        click: function () {
                            $(this).dialog('close');
                        }
                    }]
                });
            });
        }

        function clearOffer() {
            $.ajax({
                type: "POST",
                url: "HotelSaleResult.aspx/clearOffer",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function offerReportShow(param1, param2) {
            if (param1 == '' && (param2 != null || param2 != ''))
                showAlert(param2);
            else window.open(param1);
        }

        function createOffer() {
            self.parent.createOffer();
        }

        function createOfferV2() {
            self.parent.createOfferV2();
        }

        function addHotelOffer(hotel, checkIn, night) {
            self.parent.createHotelOffer(hotel, checkIn, night);

        }

        function addOffer(rowNumber) {
            var tablePrice = $('#tablePrice_' + rowNumber.toString());
            var hotelCheckInNightPrice = tablePrice.find("input");
            var priceRefNoList = '';
            $.each(hotelCheckInNightPrice, function (i) {
                if (this.checked) {
                    if (priceRefNoList.length > 0) priceRefNoList += ',';
                    priceRefNoList += this.value;
                }
            });

            $.ajax({
                type: "POST",
                url: "HotelSaleResult.aspx/addOffer",
                data: '{"BookList":"' + priceRefNoList + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function Book(hotelGroupID) {

            var roomAccomAllPrices = $('input[name^=RoomAccomPrice_' + hotelGroupID.toString() + '_' + ']');            
            var priceRefNoList = '';
            var stopSale = false;
            var allotment = false;
            var allotmentMsg = '';
            var stopSaleMsg = '';
            var first = true;
            $.each(roomAccomAllPrices, function (i) {
                if (this.checked) {
                    if (priceRefNoList.length > 0) priceRefNoList += ',';
                    priceRefNoList += this.value;

                    var _stopSale = $(this).attr("stopSale");
                    var _stopSaleMsg = $(this).attr("stopSaleMsg");
                    if (_stopSale == '1') {
                        stopSale = true;
                        stopSaleMsg = _stopSaleMsg;
                    }
                }
            });

            $.ajax({
                async: false,
                type: "POST",
                url: "HotelSaleResult.aspx/getAllotmentControl",
                data: '{"RefNoList":"' + priceRefNoList + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        allotment = true;
                        allotmentMsg = msg.d;
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });

            if (stopSale == true) {
                $("#messages").html(stopSaleMsg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: [{
                        text: btnOK,
                        click: function () {
                            $(this).dialog('close');
                            self.parent.bookRooms(priceRefNoList);
                        }
                    }, {
                        text: btnCancel,
                        click: function () {
                            $(this).dialog('close');
                        }
                    }]
                });
            }
            else if (allotment == true) {
                $("#messages").html(allotmentMsg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: [{
                        text: btnOK,
                        click: function () {
                            $(this).dialog('close');
                        }
                    }]
                });
            }
            else {
                self.parent.bookRooms(priceRefNoList);
            }
        }

        function textTrancateRun() {

            $("#divSearchResult .HotelGroup .RoomPriceDetail .RoomAccomPrice .Board span").textTruncate($("#divSearchResult .HotelGroup .RoomPriceDetail .RoomAccomPrice .Board").width() - 5);
            $("#divSearchResult .HotelGroup .RoomPriceDetail .RoomAccomPrice .Room span").textTruncate($("#divSearchResult .HotelGroup .RoomPriceDetail .RoomAccomPrice .Room").width() - 5);
            $("#divSearchResult .HotelGroup .RoomPriceDetail .RoomAccomPrice .Accom span").textTruncate($("#divSearchResult .HotelGroup .RoomPriceDetail .RoomAccomPrice .Accom").width() - 5);

        }

        function filterResult(field, value) {
            getPage(1, true, false);
            /*
            $.ajax({
                type: "POST",
                url: "HotelSaleResult.aspx/setResultFilter",
                data: '{"FieldName":"' + field + '","Value":"' + value + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null && msg.d != '') {
                        $("#gridResultSpan").html('');
                        $("#gridResultSpan").html(msg.d);
                        textTrancateRun();
                        reSizeFrame($("#gridResultSpan")[0].scrollHeight);
                        var cultureNumberStr = $("#cultureNumber").val();
                        if (cultureNumberStr != '') {
                            cultureNumber = $.json.decode(cultureNumberStr);
                        }
                        else cultureNumber = null;
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
            */
        }

        function showBrochure(holPack, CheckIn, CheckOut, Market) {
            self.parent.showBrochure(holPack, CheckIn, CheckOut, Market);
        }

        function reSizeFrame() {
            self.parent.reSizeResultFrame(document.body.offsetHeight);
        }

        function changeRoomPrice(hotelGroupID, roomNumber, refNo) {
            var roomAccomAllPrices = $('input[name^=RoomAccomPrice_' + hotelGroupID.toString() + '_' + ']');
            var roomAccomPrices = $('input[name=RoomAccomPrice_' + hotelGroupID.toString() + '_' + roomNumber + ']');

            var hotelPrice = $("#TotalPrice_" + hotelGroupID);
            hotelPrice.html('');

            var priceRefNoList = '';
            var price = 0.0;
            var salecur = '';
            var first = true;
            $.each(roomAccomAllPrices, function (i) {
                if (this.checked) {
                    var _price = $(this).attr("price");
                    var _salecur = $(this).attr("salecur");
                    var _stopSale = $(this).attr("stopSale");
                    if (priceRefNoList.length > 0) priceRefNoList += ',';
                    priceRefNoList += this.value;
                    price += parseFloat(_price) / 100;

                    if (salecur != '' && salecur != _salecur) {
                        hotelPrice.html(CurrencyTypeNotTheSame);
                        $("#btnBook_" + hotelGroupID.toString()).attr("disabled", true);
                        return;
                    }
                    else {
                        $("#btnBook_" + hotelGroupID.toString()).removeAttr("disabled");
                        salecur = _salecur;
                    }

                    hotelPrice.html(tryNumberFormat(price) + ' ' + salecur);

                    if (_stopSale == '2') {
                        $("#btnBook_" + hotelGroupID.toString()).val(stopSale);
                        $("#btnBook_" + hotelGroupID.toString()).attr("disabled", true);
                    }
                    else {
                        $("#btnBook_" + hotelGroupID.toString()).val(lblbook);
                        $("#btnBook_" + hotelGroupID.toString()).removeAttr("disabled");
                    }
                }
            });
        }

        function getPageItemCount() {
            return 10;
        }

        function onSortChange() {            
            getPage(1, false, false);
        }

        function newFilter(fieldName, value) {
            this.FieldName = fieldName;
            this.Value = value;
        }

        function getResultGrid(pageNo, filtered, newSearch, htmlTemplatte) {

            var params = new Object();
            var filters = [];
            var filter = { FieldName: '', Value: '' };

            filters[0] = new newFilter('rfPaxLocation', $("#rfResort").length > 0 ? $("#rfResort").val() : '');
            filters[1] = new newFilter('rfCategory', $("#rfCategory").length > 0 ? $("#rfCategory").val() : '');
            filters[2] = new newFilter('rfHotel', $("#rfHotel").length > 0 ? $("#rfHotel").val() : '');
            filters[3] = new newFilter('rfRoom', $("#rfRoom").length > 0 ? $("#rfRoom").val() : '');
            filters[4] = new newFilter('rfBoard', $("#rfBoard").length > 0 ? $("#rfBoard").val() : '');

            var params = new Object();
            params.Page = pageNo;
            params.PageItemCount = getPageItemCount();
            params.Filtered = filtered;
            params.NewSearch = newSearch;
            params.Sorting = $("#fltSort").length > 0 ? $("#fltSort").val() : "";
            params.FilterData = filters;
            $.ajax({
                async: false,
                type: "POST",
                url: "HotelSaleResult.aspx/getResultGrid",
                data: JSON.stringify(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var data = msg.d;
                    var html = Mustache.render(htmlTemplatte, data);
                    $("#divSearchResult").html(html);

                    textTrancateRun();

                    cultureNumber = data.NumberFormat;

                    if ($("#fltSort").length > 0) {
                        $("#fltSort").val(data.Sorting);
                    }

                    $("#divSearchResult").data("TotalResultCount", data.totalResultCount);
                    $("#divSearchResult").data("PageItemCount", data.PageItemCount);
                    $("#divSearchResult").data("CurrentPage", data.CurrentPage);

                    $('#navigation').smartpaginator({
                        totalrecords: data.totalResultCount,
                        recordsperpage: data.PageItemCount,
                        length: 4,
                        next: sNext,
                        prev: sPrevious,
                        first: sFirst,
                        last: sLast,
                        go: sGotoPage,
                        theme: 'red',
                        controlsalways: true,
                        onchange: function (newPage) {
                            getPage(newPage, false, false);
                        },
                        initval: data.CurrentPage
                    });

                    reSizeFrame($("#divSearchResult")[0].scrollHeight);

                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getPage(pageNo, filtered, newSearch) {
            $.get(templatePath + 'HotelSaleSearch.html?v=201602031445', function (html) {
                getResultGrid(pageNo, filtered, newSearch, html);
            }).fail(function (jqxhr, textStatus, error) {
                var err = textStatus + ', ' + error;
                showAlert("Request Failed: " + err);
            });
        }

        $(document).ready(function () {
            $(".my-navigation").on("click", function () {
                setTimeout(onClickNavigation, 500);
            });
            $(".simple-pagination-items-per-page").on("change", function () {
                onClickNavigation();
            });
            $(".simple-pagination-select-specific-page").on("change", function () {
                onClickNavigation();
            });

            getPage(1, false, true);
        });
    </script>

</head>
<body>
    <form id="formPackageSearchResult" runat="server">
        <div id="divSearchResult" class="ui-helper-clearfix">
        </div>
        <div id="navigation">
        </div>
        <div id="dialog-message" title="" style="display: none;">
            <span id="messages">Message</span>
        </div>
    </form>
</body>
</html>
