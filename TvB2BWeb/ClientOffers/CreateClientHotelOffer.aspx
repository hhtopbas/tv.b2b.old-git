﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CreateClientHotelOffer.aspx.cs" Inherits="CreateClientOffer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" id="htmlCreateOffer">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "CreateOffer") %></title>

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="../Scripts/mustache.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.dialogextend.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="HotelOfferStyles.css" rel="stylesheet" />

    <style type="text/css">
        .noPDF { display: none; visibility: hidden; }

        @media print {
            .noPrint { display: none; visibility: hidden; }
        }
    </style>

    <script type="text/javascript">

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function logout() {
            self.parent.logout();
        }

        function pdfOffers() {
            var btnClose = '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>';
            var htmlUrl = 'CreateClientHotelOffer.aspx';

            var DocName = 'ClientOffer';

            var width = $('.mainPage').width();
            var viewReport = $(".mainPage");
            var params = new Array();
            viewReport.find('input').each(function () {
                var o = new Object();
                o.Code = $(this).attr('name');
                o.Name = $(this).val();
                params.push(o);
            });

            var _html = viewReport.html();
            var content = '<html xmlns="http://www.w3.org/1999/xhtml" id="htmlCreateOffer">';
            content += '<head>';
            var webRoot = "<%= Global.getBasePageRoot().ToString() %>";            
            content += '<link href="' + webRoot + 'ClientOffers/HotelOfferStyles.css" rel="stylesheet" />';
            content += '<link href="' + webRoot + 'CSS/jquery-ui.css" rel="stylesheet" type="text/css" />';
            content += '<style type="text/css">.noPDF { display: none; visibility: hidden; } @media print {.noPrint { display: none; visibility: hidden; }}</style>';
            content += '</head>';
            content += '<body>';
            content += ($(".mainPage").html()).toString();
            var noprint = $(".mainPage").find(".noPrint");

            for (var i = 0; i < noprint.length; i++) {
                content = content.replace(noprint[i].outerHTML, '&nbsp;');
            }
            content += '</body>';
            content += '</html>';
            var obj = new Object();
            obj.reportType = DocName;
            obj._html = content;
            obj.pageWidth = width;
            obj.urlBase = htmlUrl;
            obj.param = params;
            $.ajax({
                type: "POST",
                url: "CreateClientHotelOffer.aspx/viewPDFReport",
                data: $.json.encode(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        $("#viewReport").removeAttr("src");
                        var _pdfViewPage = "../ViewPDF.aspx"
                        $("#viewReport").attr("src", _pdfViewPage + "?url=" + msg.d);
                        $("#dialog-viewReport").dialog(
                            {
                                autoOpen: true,
                                modal: true,
                                buttons: [{
                                    text: btnClose,
                                    click: function () {
                                        $("#dialog-viewReport").dialog('close');
                                        $("#viewReport").attr("src", "");
                                        $("#dialog-viewReport").hide();
                                        return false;
                                    }
                                }]
                            }).dialogExtend({
                                "maximize": true,
                                "icons": {
                                    "maximize": "ui-icon-circle-plus",
                                    "restore": "ui-icon-pause"
                                }
                            });
                        $("#dialog-viewReport").dialogExtend("maximize");
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function send(emails, subject, comment) {
            var params = new Object();
            params.Email = emails;
            params.Subject = subject;
            params.Comment = comment;
            params.Content = '<link href="HotelOfferStyles.css" rel="stylesheet" />';
            params.Content += ($(".mainPage").html()).toString().replace(/class="noPrint"/g, "style=\"display: none; visibility: hidden;\"");
            $.ajax({
                async: false,
                type: "POST",
                url: "CreateClientHotelOffer.aspx/send",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null && msg.d == true) {
                        alert('Client offer sended.');
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function sendToOffers() {
            $("#dialog").dialog("destroy");
            $("#dialog-sendOffer").dialog({
                modal: true,
                height: 400,
                width: 720,
                buttons: {
                    'Send': function () {
                        var emailAddr = $("#emailAddr").val();
                        var emailSubject = $("#emailSubject").val();
                        var agencyComment = $("#agencyComments").val();
                        send(emailAddr, emailSubject, agencyComment);
                        $("#dialog-sendOffer").dialog("close");
                        return false;
                    },
                    'Close': function () {
                        $("#dialog-sendOffer").dialog("close");
                        return false;
                    }
                }
            });
        }

        function getResultGrid(data) {
            $.get($("#tmplPath").val() + 'HotelDetails.html?v=201911201430', function (tmpHtml) {
                var html = Mustache.render(tmpHtml, data);
                $(".mainPage").html(html);
            }).fail(function (jqxhr, textStatus, error) {
                var err = textStatus + ', ' + error;
                showAlert("Request Failed: " + err);
            });
        }

        function getFormData(hotel, checkin, night) {
            var prm = new Object();
            prm.Hotel = hotel;
            prm.checkIn = checkin;
            prm.night = night;
            $.ajax({
                async: false,
                type: "POST",
                url: "CreateClientHotelOffer.aspx/getFormData",
                data: $.json.encode(prm),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = msg.d;
                        getResultGrid(data);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        $(document).ready(function () {
            $.query = $.query.load(location.href);
            var hotel = $.query.get('hotel');
            var checkin = $.query.get('checkin');
            var night = $.query.get('night');
            getFormData(hotel, checkin, night);
        });
    </script>

</head>
<body class="ui-helper-reset ui-helper-clearfix">
    <%--<div style="width: 1000px; position: absolute;">--%>
    <form id="form1" runat="server">

        <div class="ui-helper-clearfix clientOfferContent">
            <input id="param1" type="hidden" value="" />
            <asp:HiddenField ID="tmplPath" runat="server" />
            <div class="noPrint">
                <%--<input type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="sendToOffers()" value="<%= GetGlobalResourceObject("LibraryResource","btnSend") %>" style="font-size: 80%;" />
                        &nbsp;&nbsp;--%>
                <input type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="pdfOffers()" value="<%= GetGlobalResourceObject("LibraryResource","btnPDF") %>" style="font-size: 80%;" />
            </div>
            <div class="mainPage">
            </div>
        </div>
    </form>
    <div class="noPrint">
        <div id="dialog-sendOffer" title="" class="ui-helper-hidden ui-helper-reset">
            <div id="sendOffer" class="sendOffer">
                <br />
                <table>
                    <tr>
                        <td class="emailLabel">Email address: </td>
                        <td class="emailSepar"></td>
                        <td class="emailInput">
                            <input id="emailAddr" type="text" />
                        </td>
                    </tr>
                    <tr>
                        <td class="emailLabel">Subject: </td>
                        <td class="emailSepar"></td>
                        <td class="emailInput">
                            <input id="emailSubject" type="text" />
                        </td>
                    </tr>
                    <tr>
                        <td class="emailLabel">Agents comments: </td>
                        <td class="emailSepar"></td>
                        <td class="emailInput">
                            <textarea id="agencyComments" rows="4"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="dialog-viewReport" title='' style="display: none; text-align: center;">
            <iframe id="viewReport" runat="server" frameborder="0" style="clear: both; width: 100%; height: 100%; text-align: left;"></iframe>
        </div>
    </div>
    <%--</div>--%>
</body>
</html>
