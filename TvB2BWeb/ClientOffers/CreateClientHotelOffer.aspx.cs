﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using TvBo;
using TvTools;
using Winnovative.WnvHtmlConvert;

public partial class CreateClientOffer : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        string _tmpPath = WebRoot.BasePageRoot + "ClientOffers/OfferTemplates/" + new UICommon().getWebID() + "/" + UserData.Market + "/";
        tmplPath.Value = _tmpPath;
    }

    public static List<TvReport.basketLangRec> getMarketLangList(User UserData)
    {
        string Market = UserData.Market;

        string fileName = HttpContext.Current.Server.MapPath("Data\\" + new UICommon().getWebID().ToString() + "\\BasketMarketLang.xml");
        if (!File.Exists(fileName)) return null;
        List<TvReport.basketLangRec> list = new List<TvReport.basketLangRec>();
        System.Data.DataTable dt = new System.Data.DataTable();
        dt.ReadXml(fileName);
        foreach (System.Data.DataRow row in dt.Rows)
            list.Add(new TvReport.basketLangRec { Lang = row["Market"].ToString(), Labels = row["Labels"].ToString(), Texts = row["Texts"].ToString() });
        return list;
    }

    public static string getHotelUrl(User UserData, List<SearchResult> searchResult, List<HotelMarOptRecord> hotelMarOpt, string Hotel)
    {
        string errorMsg = string.Empty;
        HotelMarOptRecord hotelUrl = hotelMarOpt.Find(f => f.Hotel == Hotel);
        string _hotelUrl = (hotelUrl != null && !string.IsNullOrEmpty(hotelUrl.InfoWeb)) ? hotelUrl.InfoWeb : "";
        return _hotelUrl;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData(string Hotel, string checkIn, string night)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        SearchResult offerHotel = new SearchResult();
        MultiRoomResult searchResult = (MultiRoomResult)HttpContext.Current.Session["SearchResult"];

        string errorMsg = string.Empty;

        long _checkIn = Convert.ToInt64(checkIn);
        DateTime CheckIn = DateTime.MinValue.AddTicks(_checkIn);
        Int16 _night = Convert.ToInt16(night);
        DateTime CheckOut = CheckIn.AddDays(_night);

        List<SearchResult> hotels = searchResult.ResultSearch.Where(w => w.Hotel == Hotel && w.CheckIn.Value == CheckIn && w.Night == _night).ToList<SearchResult>();
        SearchResult firstRecord = hotels.FirstOrDefault();

        List<HotelPresTextWidthCategory> hotelPressText = new Hotels().getHotelPressText(UserData, Hotel, CheckIn, CheckOut, "GENERAL", ref errorMsg);
        bool? showHotelImages = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "ShowHotelImages"));
        string imageUrl = string.Empty;
        string webPage = string.Empty;
        HotelRecord hotel = new Hotels().getHotelDetail(UserData, Hotel, ref errorMsg);
        if (hotel != null)
        {
            List<ClientOfferService> serviceList = new Offers().getPriceListServices(UserData, firstRecord.CatPackID, ref errorMsg);
            int ix = 0;
            foreach (var s in serviceList)
                if (s.Name == "FLIGHT")
                {
                    if (searchResult.FlightInfo.Skip(ix).FirstOrDefault().FlyDate.HasValue)
                        s.BegDate = searchResult.FlightInfo.Skip(ix).FirstOrDefault().FlyDate.Value.ToShortDateString();
                    if (searchResult.FlightInfo.Skip(ix).FirstOrDefault().DepTime.HasValue)
                        s.BegDate += " " + searchResult.FlightInfo.Skip(ix).FirstOrDefault().DepTime.Value.ToShortTimeString();
                    s.BegDate += " - " + searchResult.FlightInfo.Skip(ix).FirstOrDefault().FlightNo;
                    ix++;
                }

            webPage = Conversion.getStrOrNull(hotel.Homepage);
            if (showHotelImages.HasValue && showHotelImages.Value)
            {
                if (hotel != null)
                {
                    string hotelImagesUrl = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "HotelImagesUrl"));
                    List<HotelImageUrl> hotelImgUrl = string.IsNullOrEmpty(hotelImagesUrl) ? new List<HotelImageUrl>() : Newtonsoft.Json.JsonConvert.DeserializeObject<List<HotelImageUrl>>(hotelImagesUrl);

                    HotelImageUrl hotelImg = hotelImgUrl.Find(f => f.Market == UserData.Market);
                    if (hotelImg != null)
                    {
                        imageUrl = hotelImg.Url + "/" + hotel.RecID.ToString() + "/0_M.jpg";
                    }
                }
            }
            int count = 0;
            var rooms = from q in hotels
                        select new
                        {
                            RoomName = q.RoomNameL,
                            BoardName = q.BoardNameL,
                            SalePrice = q.LastPrice.HasValue ? "€" + q.LastPrice.Value.ToString("#,###.00") : "",
                            OfferStr = "",
                            oddeven = count++ % 2 != 0
                        };
            return new
            {
                HotelName = hotel.LocalName,
                HotelPressTextList = hotelPressText,
                HotelImage = imageUrl,
                WebPage = webPage,
                ServiceList = serviceList.OrderBy(o => o.StepNo),
                //flights = "",
                roomPrice = rooms,
                AgencyInfo = UserData.AgencyNameL
            };
        }
        else
        {
            return null;
        }

    }

    public static byte[] getOfferPdf(User UserData, string htmlString)
    {
        int PageWidth = 700;

        PdfConverter pdfConverter = new PdfConverter();

        //pdfConverter.PdfDocumentOptions.PdfPageSize = Winnovative.WnvHtmlConvert.PdfPageSize.A4;
        pdfConverter.PdfDocumentOptions.FitWidth = true;
        pdfConverter.PdfDocumentOptions.JpegCompressionEnabled = true;
        pdfConverter.PdfDocumentOptions.JpegCompressionLevel = 50;
        pdfConverter.PdfDocumentOptions.EmbedFonts = true;

        pdfConverter.PdfDocumentOptions.ShowHeader = false;
        pdfConverter.PdfDocumentOptions.ShowFooter = false;
        pdfConverter.PdfDocumentOptions.LeftMargin = 10;
        pdfConverter.PdfDocumentOptions.TopMargin = 10;


        // set the demo license key
        pdfConverter.LicenseKey = ConfigurationManager.AppSettings["Winnovative"]; ;

        // get the base url for string conversion which is the url from where the html code was retrieved
        // the base url is a hint for the converter to find the external CSS and images referenced by relative URLs                
        try
        {
            return pdfConverter.GetPdfBytesFromHtmlString(htmlString.ToString());
        }
        catch
        {
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object send(string Email, string Subject, string Comment, string Content)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        if (string.IsNullOrEmpty(Email) || string.IsNullOrEmpty(Subject) || string.IsNullOrEmpty(Comment) || string.IsNullOrEmpty(Content))
        {
            return false;
        }

        TvParameters tvParams = new TvBo.Common().getTvParameters(UserData.Market, Conversion.getDecimalOrNull(UserData.TvVersion), UserData.WebVersion, ref errorMsg);

        if (tvParams == null) return false;

        StringBuilder body = new StringBuilder();
        body.AppendLine("<p>");
        body.AppendFormat("<span>{0}</span><br />", UserData.AgencyRec.Name);
        body.AppendFormat("<span>{0}</span><br />", UserData.UserName);
        body.AppendFormat("<span>{0}</span><br />", UserData.EMail);
        body.AppendFormat("<span>{0}</span><br />", UserData.AgencyRec.Address + " " + UserData.AgencyRec.AddrZip + " " + UserData.AgencyRec.AddrCity);
        body.AppendFormat("<span>{0}</span><br />", UserData.AgencyRec.Phone1 + " " + UserData.AgencyRec.Phone2);
        body.AppendLine("<br />");
        body.AppendFormat("<span>{0}</span><br />", Comment.Replace("\n", "<br />").Replace("\r", "<br />"));
        body.AppendLine("</p>");
        SendMail sm = new SendMail();
        AsyncMailSenderParams param = new AsyncMailSenderParams
        {
            Body = body.ToString(),
            DisplayName = "no-reply",
            FileBinary = getOfferPdf(UserData, Content),
            FileName = "Offer.Pdf",
            Port = tvParams.TvParamSystem.SMTPPort,
            Subject = Subject,
            ToAddress = Email,
            SenderMail = tvParams.TvParamSystem.SMTPAccount,
            SenderPassword = tvParams.TvParamSystem.SMTPPass,
            SMTPServer = tvParams.TvParamSystem.SMTPServer
        };

        if (sm.AsyncMailSender(param))
        {
            return true;
        }
        else
        {
            return false;
        }
        /*
        - Agency name (from data base according login)
        - Agency user name (from data base according login)
        - Agency user e-mail (from data base according login)
        - Agency address (from data base according login)
        - Agency user phone number (from data base according login)
        - Place for entering client e-mail address (agency user entering by him self)
        - Agency comment (agency user entering if he need)
        */
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object resetList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ClientOfferList"] == null)
            return true;

        HttpContext.Current.Session["ClientOfferList"] = null;

        return true;

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getOfferList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        List<SearchResult> offers = new List<SearchResult>();
        if (HttpContext.Current.Session["ClientOfferList"] != null)
            offers = (List<SearchResult>)HttpContext.Current.Session["ClientOfferList"];
        else return null;

        offers = removeOldOffers(UserData, offers);

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        List<TvReport.basketLangRec> list = getMarketLangList(UserData);
        List<HotelMarOptRecord> hotelMarOpt = new Search().getHotelMarOptList(UserData, offers, ref errorMsg);

        string lblHotel = list != null && list.Find(f => f.Labels == "lblHotel" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblHotel" && f.Lang == UserData.Market).Texts : "Hotel";
        string lblCheckIn = list != null && list.Find(f => f.Labels == "lblCheckIn" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblCheckIn" && f.Lang == UserData.Market).Texts : "Check/in";
        string lblDuration = list != null && list.Find(f => f.Labels == "lblDuration" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblDuration" && f.Lang == UserData.Market).Texts : "Night";

        string lblRoomType = list != null && list.Find(f => f.Labels == "lblRoomType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblRoomType" && f.Lang == UserData.Market).Texts : "Room";
        string lblBoardType = list != null && list.Find(f => f.Labels == "lblBoardType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblBoardType" && f.Lang == UserData.Market).Texts : "Board";
        string lblAccom = list != null && list.Find(f => f.Labels == "lblAccom" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblAccom" && f.Lang == UserData.Market).Texts : "Accommodation";
        string lblPrice = list != null && list.Find(f => f.Labels == "lblPrice" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblPrice" && f.Lang == UserData.Market).Texts : "Price";
        string lblComment = list != null && list.Find(f => f.Labels == "lblComment" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblComment" && f.Lang == UserData.Market).Texts : "Agency comment";
        string lblTransport = list != null && list.Find(f => f.Labels == "lblTransport" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblTransport" && f.Lang == UserData.Market).Texts : "Transport";
        string lblTransportType = list != null && list.Find(f => f.Labels == "lblTransportType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblTransportType" && f.Lang == UserData.Market).Texts : "Charter flight";
        string lblTransportBusType = list != null && list.Find(f => f.Labels == "lblTransportBusType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblTransportBusType" && f.Lang == UserData.Market).Texts : "Charter bus";

        var offerlist = from q in offers
                        select new
                        {
                            lblHotel = lblHotel,
                            lblCheckIn = lblCheckIn,
                            lblDuration = lblDuration,
                            lblRoomType = lblRoomType,
                            lblBoardType = lblBoardType,
                            lblAccom = lblAccom,
                            lblPrice = lblPrice,
                            lblComment = lblComment,
                            RefNo = q.RefNo,
                            offerGUID = q.RefNo.ToString() + "_" + q.SearchTime.Ticks.ToString(),
                            DepCityName = useLocalName ? q.DepCityNameL : q.DepCityName,
                            ArrCityName = useLocalName ? q.ArrCityNameL : q.ArrCityName,
                            HotelName = useLocalName ? q.HotelNameL : q.HotelName,
                            HotCat = q.HotCat,
                            HotLocationName = useLocalName ? q.HotLocationNameL : q.HotLocationName,
                            InfoWeb = q.InfoWeb,
                            CheckIn = q.CheckIn.HasValue ? q.CheckIn.Value.ToShortDateString() : string.Empty,
                            Night = q.Night.HasValue ? q.Night.Value.ToString() : string.Empty,
                            RoomName = useLocalName ? q.RoomNameL : q.RoomName,
                            BoardName = useLocalName ? q.BoardNameL : q.BoardName,
                            AccomFullName = q.AccomFullName,
                            LastPrice = q.LastPrice.HasValue ? q.LastPrice.Value.ToString("#,###.00") : string.Empty,
                            SaleCur = q.SaleCur
                        };

        return new
        {
            offerList = offerlist
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object removeClientDetail(int? RefNo, long? SearchTime)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (!SearchTime.HasValue) return false;

        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["ClientOfferList"] == null)
            return false;
        DateTime searchTime = new DateTime(SearchTime.Value);

        List<SearchResult> offerList = (List<SearchResult>)HttpContext.Current.Session["ClientOfferList"];

        List<SearchResult> retVal = offerList.Where(w => !(w.RefNo == RefNo && w.SearchTime == searchTime)).ToList<SearchResult>();

        HttpContext.Current.Session["ClientOfferList"] = retVal;

        return new
        {
            ID = RefNo.ToString() + "_" + SearchTime.ToString()
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object addDetail(int? RefNo, long? SearchTime)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (!SearchTime.HasValue) return false;

        string errorMsg = string.Empty;

        List<Location> locList = new Locations().getLocationList(UserData, LocationType.None, null, null, null, null, ref errorMsg);

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        List<SearchResult> offerList = new ClientOffer().readClientOfferList(UserData);
        DateTime searchTime = new DateTime(SearchTime.Value);
        SearchResult of = offerList.Find(w => w.RefNo == RefNo && w.SearchTime == searchTime);
        List<SearchResult> saveOffers = new List<SearchResult>();
        if (HttpContext.Current.Session["ClientOfferList"] != null)
            saveOffers = (List<SearchResult>)HttpContext.Current.Session["ClientOfferList"];
        saveOffers.Add(of);

        HttpContext.Current.Session["ClientOfferList"] = saveOffers;

        return true;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormHeader()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        List<SearchResult> offerList = new List<SearchResult>();
        offerList = new ClientOffer().readClientOfferList(UserData);

        List<HotelMarOptRecord> hotelMarOpt = new Search().getHotelMarOptList(UserData, offerList, ref errorMsg);
        List<TvReport.basketLangRec> list = getMarketLangList(UserData);
        AgencyRecord agency = UserData.AgencyRec;
        StringBuilder sb = new StringBuilder();
        string operatorLogoStr = CacheObjects.getOperatorLogoUrl(UserData.Operator);
        byte[] agencyLogo = new TvReport.AllOperator().getLogo("LogoSmall", "Agency", "Code", UserData.MainAgency ? UserData.MainOffice : UserData.AgencyID, null, ref errorMsg);
        string agencyLogoStr = agencyLogo != null ? TvBo.UICommon.WriteBinaryImage(agencyLogo, UserData.AgencyID, "AG_", "", "") : "";
        string userPhone = new TvReport.AllOperator().getAgencyUserPhone(UserData.AgencyID, UserData.UserID, ref errorMsg);
        FixNotesRecord fixNote = new TvBo.Common().getFixNotes(UserData.Market, "PriceList", ref errorMsg);
        string fixNoteText = string.Empty;
        if (fixNote != null)
        {
            TvTools.RTFtoHTML rtf = new RTFtoHTML();
            rtf.rtf = fixNote.Note;
            fixNoteText = rtf.html();
        }
        string createdBy = list != null && list.Find(f => f.Labels == "Header1" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "Header1" && f.Lang == UserData.Market).Texts : "Created ";

        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda))
            operatorLogoStr = "";
        htmlData.Add(new TvReport.htmlCodeData { IdName = "imgOperator", TagName = "img", Data = operatorLogoStr });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "imgAgency", TagName = "img", Data = agencyLogoStr });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtCreatedBy", TagName = "span", Data = createdBy });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAgencyName", TagName = "span", Data = string.IsNullOrEmpty(agency.FirmName) ? agency.Name : agency.FirmName });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtUserName", TagName = "span", Data = UserData.UserName });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAgencyTel", TagName = "span", Data = userPhone });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtEmail", TagName = "span", Data = UserData.EMail });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtDateTime", TagName = "span", Data = DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "fixNoteText", TagName = "div", Data = fixNoteText });
        return htmlData;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object removeOfferListDetail(int? RefNo, long? SearchTime)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (!SearchTime.HasValue) return false;

        string errorMsg = string.Empty;

        List<Location> locList = new Locations().getLocationList(UserData, LocationType.None, null, null, null, null, ref errorMsg);

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        List<SearchResult> offerList = new ClientOffer().readClientOfferList(UserData);
        List<SearchResult> retVal = new List<SearchResult>();
        DateTime searchTime = new DateTime(SearchTime.Value);
        retVal = offerList.Where(w => w.RefNo != RefNo && w.SearchTime != searchTime).ToList<SearchResult>();
        if (new ClientOffer().saveClientOfferList(UserData, retVal))
            return true;
        else return false;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getClientOfferList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        List<Location> locList = new Locations().getLocationList(UserData, LocationType.None, null, null, null, null, ref errorMsg);

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        List<SearchResult> offerList = new ClientOffer().readClientOfferList(UserData);
        var retVal = from q in offerList
                     select new
                     {
                         offerGUID = q.RefNo.ToString() + "_" + q.SearchTime.Ticks.ToString(),
                         CheckIn = q.CheckIn.Value.ToString("dd MMM yyyy ddd"),
                         Night = q.Night.ToString() + " " + HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblNight").ToString(),
                         CountryName = getLocationForCountryName(locList, useLocalName, q.HotelLocation),
                         HotelLocationName = useLocalName ? q.HotLocationNameL : q.HotLocationName,
                         HotelName = useLocalName ? q.HotelNameL : q.HotelName,
                         HotelCategory = q.HotCat,
                         RoomName = useLocalName ? q.RoomNameL : q.RoomName,
                         FullAccomName = q.AccomFullName,
                         Board = q.Board,
                         Price = q.LastPrice.HasValue ? q.LastPrice.Value.ToString("#,###.00") : "",
                         Curr = q.SaleCur
                     };
        return new
        {
            cODetail = retVal
        };
    }

    [WebMethod(EnableSession = true)]
    public static string viewPDFReport(string reportType, string _html, string pageWidth, string urlBase, List<CodeName> param)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int PageWidth = Conversion.getInt32OrNull(pageWidth).HasValue ? Conversion.getInt32OrNull(pageWidth).Value : 1000;

        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("~") + "\\";
        string pdfFileName = "ClientOffer" + "_" + UserData.AgencyID + "_" + UserData.UserID + "_" + Guid.NewGuid() + ".pdf";


        PdfConverter pdfConverter = new PdfConverter();

        pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
        pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
        pdfConverter.PdfDocumentOptions.FitWidth = true;
        pdfConverter.PdfDocumentOptions.JpegCompressionEnabled = true;
        pdfConverter.PdfDocumentOptions.JpegCompressionLevel = 50;
        pdfConverter.PdfDocumentOptions.EmbedFonts = true;

        pdfConverter.PdfDocumentOptions.ShowHeader = false;
        pdfConverter.PdfDocumentOptions.ShowFooter = false;
        pdfConverter.PdfDocumentOptions.LeftMargin = 10;
        pdfConverter.PdfDocumentOptions.TopMargin = 10;

        // set the demo license key
        pdfConverter.LicenseKey = ConfigurationManager.AppSettings["Winnovative"]; ;

        // get the base url for string conversion which is the url from where the html code was retrieved
        // the base url is a hint for the converter to find the external CSS and images referenced by relative URLs                         
        try
        {
            pdfConverter.SavePdfFromHtmlStringToFile(_html, siteFolderISS + "ACE\\" + pdfFileName);
            //pdfConverter.SavePdfFromUrlToFile(basePageUrl + "/ClientOffers/CreateClientHotelOffer.aspx", siteFolderISS + "ACE\\" + pdfFileName);
            string returnUrl = basePageUrl + "ACE/" + pdfFileName;
            return returnUrl;
        }
        catch
        {
            return string.Empty;
        }
    }

    public static string getLocationForCountryName(List<Location> locList, bool useLocalName, int? location)
    {
        Location loc = locList.Find(f => f.RecID == location);
        if (loc != null)
        {
            Location country = locList.Find(f => f.RecID == loc.Country);
            if (country != null)
            {
                return useLocalName ? country.NameL : country.Name;
            }
            else
                return string.Empty;
        }
        else
            return string.Empty;
    }

    public static List<SearchResult> removeOldOffers(User UserData, List<SearchResult> offerList)
    {
        List<SearchResult> retVal = (from q in offerList
                                     where q.SearchTime >= DateTime.Now.AddDays(-2)
                                     select q).ToList<SearchResult>();
        if (offerList.Count != retVal.Count)
            new ClientOffer().saveClientOfferList(UserData, retVal);
        return retVal;
    }
}