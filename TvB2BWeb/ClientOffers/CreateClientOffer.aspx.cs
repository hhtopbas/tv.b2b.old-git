﻿//using Spire;
//using Spire.Pdf;
//using Spire.Pdf.HtmlConverter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Winnovative;
using TvBo;
using TvTools;
using System.Web.Security;

public partial class CreateClientOffer : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        string _tmpPath = WebRoot.BasePageRoot + "ClientOffers/OfferTemplates/" + new UICommon().getWebID() + "/" + UserData.Market + "/";
        tmplPath.Value = _tmpPath;
        Session["ClientOfferList"] = null;
    }

    public static List<TvReport.basketLangRec> getMarketLangList(User UserData)
    {
        string Market = UserData.Market;

        string fileName = HttpContext.Current.Server.MapPath("Data\\" + new UICommon().getWebID().ToString() + "\\BasketMarketLang.xml");
        if (!File.Exists(fileName)) return null;
        List<TvReport.basketLangRec> list = new List<TvReport.basketLangRec>();
        System.Data.DataTable dt = new System.Data.DataTable();
        dt.ReadXml(fileName);
        foreach (System.Data.DataRow row in dt.Rows)
            list.Add(new TvReport.basketLangRec { Lang = row["Market"].ToString(), Labels = row["Labels"].ToString(), Texts = row["Texts"].ToString() });
        return list;
    }

    public static string getHotelUrl(User UserData, List<HotelMarOptRecord> hotelMarOpt, string Hotel)
    {
        string errorMsg = string.Empty;
        HotelMarOptRecord hotelUrl = hotelMarOpt.Find(f => f.Hotel == Hotel);
        string _hotelUrl = (hotelUrl != null && !string.IsNullOrEmpty(hotelUrl.InfoWeb)) ? hotelUrl.InfoWeb : "";
        if (string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt) && !string.IsNullOrEmpty(_hotelUrl) && !string.IsNullOrEmpty(UserData.AgencyRec.CIF))
        {
            _hotelUrl += UserData.AgencyRec.CIF;
        }
        return _hotelUrl;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<SearchResult> offerList = new List<SearchResult>();

        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ClientOfferList"] == null) return "";
        offerList = (List<SearchResult>)HttpContext.Current.Session["ClientOfferList"];

        List<HotelMarOptRecord> hotelMarOpt = new Search().getHotelMarOptList(UserData, offerList, ref errorMsg);
        List<TvReport.basketLangRec> list = getMarketLangList(UserData);
        AgencyRecord agency = UserData.AgencyRec;

        string lblHotel = list != null && list.Find(f => f.Labels == "lblHotel" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblHotel" && f.Lang == UserData.Market).Texts : "Hotel";
        string lblCheckIn = list != null && list.Find(f => f.Labels == "lblCheckIn" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblCheckIn" && f.Lang == UserData.Market).Texts : "Check/in";
        string lblDuration = list != null && list.Find(f => f.Labels == "lblDuration" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblDuration" && f.Lang == UserData.Market).Texts : "Night";

        string lblRoomType = list != null && list.Find(f => f.Labels == "lblRoomType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblRoomType" && f.Lang == UserData.Market).Texts : "Room";
        string lblBoardType = list != null && list.Find(f => f.Labels == "lblBoardType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblBoardType" && f.Lang == UserData.Market).Texts : "Board";
        string lblAccom = list != null && list.Find(f => f.Labels == "lblAccom" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblAccom" && f.Lang == UserData.Market).Texts : "Accommodation";
        string lblPrice = list != null && list.Find(f => f.Labels == "lblPrice" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblPrice" && f.Lang == UserData.Market).Texts : "Price";
        string lblComment = list != null && list.Find(f => f.Labels == "lblComment" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblComment" && f.Lang == UserData.Market).Texts : "Agency comment";
        string lblTransport = list != null && list.Find(f => f.Labels == "lblTransport" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblTransport" && f.Lang == UserData.Market).Texts : "Transport";
        string lblTransportType = list != null && list.Find(f => f.Labels == "lblTransportType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblTransportType" && f.Lang == UserData.Market).Texts : "Charter flight";

        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();
        #region Search Note
        string searchTextRTF = new Search().getSearchText(UserData, ref errorMsg);
        StringBuilder sb = new StringBuilder();
        if (!string.IsNullOrEmpty(searchTextRTF))
        {
            TvTools.RTFtoHTML rtf = new RTFtoHTML();
            rtf.rtf = searchTextRTF;

            string searchTextHtml = rtf.html();

            sb.Append("<div class=\"divResultNote\">");
            sb.Append(searchTextHtml);
            sb.Append("</div>");
        }
        #endregion
        htmlData.Add(new TvReport.htmlCodeData { IdName = "fixNoteText", TagName = "div", Data = sb.ToString() });
        return htmlData;
    }

    public static byte[] getOfferPdf(User UserData, string htmlString, string pageWidth)
    {
        PdfConverter pdfConverter = new PdfConverter(int.Parse(pageWidth));

        pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
        pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Best;
        pdfConverter.PdfDocumentOptions.ShowHeader = false;
        pdfConverter.PdfDocumentOptions.ShowFooter = false;
        pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
        pdfConverter.PdfDocumentOptions.LeftMargin = 50;
        pdfConverter.PdfDocumentOptions.TopMargin = 10;

        // set the demo license key
        pdfConverter.LicenseKey = ConfigurationManager.AppSettings["Winnovative"];// "RG92ZHVkdXF0cGRxanRkd3VqdXZqfX19fQ==";

        // get the base url for string conversion which is the url from where the html code was retrieved
        // the base url is a hint for the converter to find the external CSS and images referenced by relative URLs                
        try
        {
            return pdfConverter.GetPdfBytesFromHtmlString(htmlString.ToString());
        }
        catch
        {
            return null;
        }

    }

    [WebMethod(EnableSession = true)]
    public static string viewPDFReport(string reportType, string _html, string pageWidth, string urlBase, List<CodeName> param)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;


        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("~") + "\\";
        string pdfFileName = "ClientOffer" + "_" + UserData.AgencyID + "_" + UserData.UserID + "_" + Guid.NewGuid() + ".pdf";

        PdfConverter pdfConverter = new PdfConverter(int.Parse(pageWidth));

        pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
        pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Best;
        pdfConverter.PdfDocumentOptions.ShowHeader = false;
        pdfConverter.PdfDocumentOptions.ShowFooter = false;
        pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
        pdfConverter.PdfDocumentOptions.LeftMargin = 50;
        pdfConverter.PdfDocumentOptions.TopMargin = 10;
        // set the demo license key
        pdfConverter.LicenseKey = ConfigurationManager.AppSettings["Winnovative"];// "RG92ZHVkdXF0cGRxanRkd3VqdXZqfX19fQ==";

        // get the base url for string conversion which is the url from where the html code was retrieved
        // the base url is a hint for the converter to find the external CSS and images referenced by relative URLs                
        try
        {
            pdfConverter.SavePdfFromHtmlStringToFile(_html.ToString(), siteFolderISS + "ACE\\" + pdfFileName/*, urlBase + "ClientOffers/CreateClientOffer.aspx"*/);
            string returnUrl = basePageUrl + "ACE/" + pdfFileName;
            return returnUrl;
        }
        catch
        {
            return string.Empty;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object send(string Email, string Subject, string Comment, string Content, string pageWidth)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        if (string.IsNullOrEmpty(Content))
        {
            return false;
        }

        if (!string.IsNullOrEmpty(UserData.EMail))
            Email += ";" + UserData.EMail;

        string fileName = "Offer.Pdf";
        switch (UserData.Ci.TwoLetterISOLanguageName)
        {
            case "en": fileName = "OFFER.Pdf"; break;
            case "lt": fileName = "PASIULYMAS.Pdf"; break;
            case "lv": fileName = "PIEDAVAJUMS.Pdf"; break;
            case "et": fileName = "PAKKUMINE.Pdf"; break;
            case "ru": fileName = "OFFER.Pdf"; break;
            default: fileName = "OFFER.Pdf"; break;
        }

        TvParameters tvParams = new TvBo.Common().getTvParameters(UserData.Market, Conversion.getDecimalOrNull(UserData.TvVersion), UserData.WebVersion, ref errorMsg);

        if (tvParams == null) return false;

        StringBuilder body = new StringBuilder();
        body.AppendLine("<p>");
        body.AppendFormat("<span>{0}</span><br />", UserData.AgencyRec.Name);
        body.AppendFormat("<span>{0}</span><br />", UserData.UserName);
        body.AppendFormat("<span>{0}</span><br />", UserData.EMail);
        body.AppendFormat("<span>{0}</span><br />", UserData.AgencyRec.Address + " " + UserData.AgencyRec.AddrZip + " " + UserData.AgencyRec.AddrCity);
        body.AppendFormat("<span>{0}</span><br />", UserData.AgencyRec.Phone1 + " " + UserData.AgencyRec.Phone2);
        body.AppendLine("<br />");
        body.AppendFormat("<span>{0}</span><br />", Comment.Replace("\n", "<br />").Replace("\r", "<br />"));
        body.AppendLine("</p>");
        SendMail sm = new SendMail();
        AsyncMailSenderParams param = new AsyncMailSenderParams
            {
                Body = body.ToString(),
                DisplayName = "no-reply",
                FileBinary = getOfferPdf(UserData, Content, pageWidth),
                FileName = fileName,
                Port = tvParams.TvParamSystem.SMTPPort,
                Subject = Subject,
                ToAddress = Email,
                SenderMail = tvParams.TvParamSystem.SMTPAccount,
                SenderPassword = tvParams.TvParamSystem.SMTPPass,
                SMTPServer = tvParams.TvParamSystem.SMTPServer
            };

        if (sm.AsyncMailSender(param))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object resetList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ClientOfferList"] == null)
            return true;

        HttpContext.Current.Session["ClientOfferList"] = null;

        return true;

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object clearList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        List<Location> locList = new Locations().getLocationList(UserData, LocationType.None, null, null, null, null, ref errorMsg);

        HttpContext.Current.Session["ClientOfferList"] = null;
        List<SearchResult> offerList = new ClientOffer().readClientOfferList(UserData);
        if (new ClientOffer().saveClientOfferList(UserData, new List<SearchResult>()))
            return true;
        else return false;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getOfferList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        List<SearchResult> offers = new List<SearchResult>();
        if (HttpContext.Current.Session["ClientOfferList"] != null)
            offers = (List<SearchResult>)HttpContext.Current.Session["ClientOfferList"];
        else return null;

        offers = removeOldOffers(UserData, offers);

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        List<TvReport.basketLangRec> list = getMarketLangList(UserData);
        List<HotelMarOptRecord> hotelMarOpt = new Search().getHotelMarOptList(UserData, offers, ref errorMsg);

        string btnReview = HttpContext.GetGlobalResourceObject("LibraryResource", "btnReview").ToString();
        string lblCreated = HttpContext.GetGlobalResourceObject("CreateOffer", "Created").ToString();
        string lblHotel = HttpContext.GetGlobalResourceObject("CreateOffer", "lblHotel").ToString();
        string lblCheckIn = HttpContext.GetGlobalResourceObject("CreateOffer", "lblCheckIn").ToString();
        string lblDuration = HttpContext.GetGlobalResourceObject("CreateOffer", "lblDuration").ToString();

        string lblRoomType = HttpContext.GetGlobalResourceObject("CreateOffer", "lblRoomType").ToString();
        string lblBoardType = HttpContext.GetGlobalResourceObject("CreateOffer", "lblBoardType").ToString();
        string lblAccom = HttpContext.GetGlobalResourceObject("CreateOffer", "lblAccom").ToString();
        string lblPrice = HttpContext.GetGlobalResourceObject("CreateOffer", "lblPrice").ToString();
        string lblComment = HttpContext.GetGlobalResourceObject("CreateOffer", "lblComment").ToString();
        string lblTransport = HttpContext.GetGlobalResourceObject("CreateOffer", "lblTransport").ToString();
        string lblTransportType = HttpContext.GetGlobalResourceObject("CreateOffer", "lblTransportType").ToString();
        string lblTransportBusType = list != null && list.Find(f => f.Labels == "lblTransportBusType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblTransportBusType" && f.Lang == UserData.Market).Texts : "Charter bus";

        var offerlist = from q in offers
                        select new {
                            lblHotel = lblHotel,
                            lblCheckIn = lblCheckIn,
                            lblDuration = lblDuration,
                            lblRoomType = lblRoomType,
                            lblBoardType = lblBoardType,
                            lblAccom = lblAccom,
                            lblPrice = lblPrice,
                            lblComment = lblComment,
                            lblCreated = lblCreated,
                            RefNo = q.RefNo,
                            offerGUID = q.RefNo.ToString() + "_" + q.SearchTime.Ticks.ToString(),
                            DepCityName = useLocalName ? q.DepCityNameL : q.DepCityName,
                            ArrCityName = useLocalName ? q.ArrCityNameL : q.ArrCityName,
                            HotelName = useLocalName ? q.HotelNameL : q.HotelName,
                            HotCat = q.HotCat,
                            HotLocationName = useLocalName ? q.HotLocationNameL : q.HotLocationName,
                            InfoWeb = getHotelUrl(UserData, hotelMarOpt, q.Hotel),
                            CheckIn = q.CheckIn.HasValue ? q.CheckIn.Value.ToShortDateString() : string.Empty,
                            Night = q.Night.HasValue ? q.Night.Value.ToString() : string.Empty,
                            RoomName = useLocalName ? q.RoomNameL : q.RoomName,
                            BoardName = useLocalName ? q.BoardNameL : q.BoardName,
                            AccomName = useLocalName ? q.AccomNameL : q.AccomName,
                            AccomFullName = q.AccomFullName,
                            LastPrice = q.LastPrice.HasValue ? q.LastPrice.Value.ToString("#,###.00") : string.Empty,
                            LastPriceEU = q.LastPrice.HasValue ? (q.LastPrice.Value * ((decimal)34528 / 10000)).ToString("#,###.00") : string.Empty,
                            SaleCur = q.SaleCur,
                            CreateTime = q.SearchTime.ToShortDateString() + " " + q.SearchTime.ToShortTimeString(),
                            Description = q.Description,
                            ShowFlight = string.IsNullOrEmpty(strFunc.Trim(q.DepFlight, ' ')) || string.IsNullOrEmpty(strFunc.Trim(q.RetFlight, ' ')),
                            DepFlight = q.DepFlight,
                            DepFlightDate = !(string.IsNullOrEmpty(strFunc.Trim(q.DepFlight, ' ')) || string.IsNullOrEmpty(strFunc.Trim(q.RetFlight, ' '))) ? (q.DEPFlightTime.HasValue ? q.DEPFlightTime.Value.ToShortDateString() + " " + q.DEPFlightTime.Value.ToShortTimeString() : "") : "",
                            RetFlight = q.RetFlight,
                            RetFlightDate = !(string.IsNullOrEmpty(strFunc.Trim(q.DepFlight, ' ')) || string.IsNullOrEmpty(strFunc.Trim(q.RetFlight, ' '))) ? (q.RETFlightTime.HasValue ? q.RETFlightTime.Value.ToShortDateString() + " " + q.RETFlightTime.Value.ToShortTimeString() : "") : ""
                        };

        return new
        {
            offerList = offerlist,
            btnReview = btnReview
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object removeClientDetail(int? RefNo, long? SearchTime)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (!SearchTime.HasValue) return false;

        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["ClientOfferList"] == null)
            return false;
        DateTime searchTime = new DateTime(SearchTime.Value);

        List<SearchResult> offerList = (List<SearchResult>)HttpContext.Current.Session["ClientOfferList"];

        List<SearchResult> retVal = offerList.Where(w => !(w.RefNo == RefNo && w.SearchTime == searchTime)).ToList<SearchResult>();

        HttpContext.Current.Session["ClientOfferList"] = retVal;

        return new
        {
            ID = RefNo.ToString() + "_" + SearchTime.ToString()
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object addDetail(int? RefNo, long? SearchTime)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (!SearchTime.HasValue) return false;

        string errorMsg = string.Empty;

        List<Location> locList = new Locations().getLocationList(UserData, LocationType.None, null, null, null, null, ref errorMsg);

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        List<SearchResult> offerList = new ClientOffer().readClientOfferList(UserData);
        DateTime searchTime = new DateTime(SearchTime.Value);
        SearchResult of = offerList.Find(w => w.RefNo == RefNo && w.SearchTime == searchTime);
        List<SearchResult> saveOffers = new List<SearchResult>();
        if (HttpContext.Current.Session["ClientOfferList"] != null)
            saveOffers = (List<SearchResult>)HttpContext.Current.Session["ClientOfferList"];
        saveOffers.Add(of);

        HttpContext.Current.Session["ClientOfferList"] = saveOffers;

        return true;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormHeader()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        List<SearchResult> offerList = new List<SearchResult>();
        offerList = new ClientOffer().readClientOfferList(UserData);

        List<HotelMarOptRecord> hotelMarOpt = new Search().getHotelMarOptList(UserData, offerList, ref errorMsg);
        List<TvReport.basketLangRec> list = getMarketLangList(UserData);
        AgencyRecord agency = UserData.AgencyRec;
        StringBuilder sb = new StringBuilder();
        byte[] operatorLogo = new TvReport.AllOperator().getLogo("Logo", "Operator", "Code", UserData.Operator, null, ref errorMsg);
        string operatorLogoStr = "data:image/jpg;base64," + Convert.ToBase64String(operatorLogo);//CacheObjects.getOperatorLogoUrl(UserData.Operator);

        byte[] agencyLogo = new TvReport.AllOperator().getLogo("LogoSmall", "Agency", "Code", UserData.MainAgency ? UserData.MainOffice : UserData.AgencyID, null, ref errorMsg);
        string base64AgencyLogo = "data:image/jpg;base64," + Convert.ToBase64String(agencyLogo);
        //string agencyLogoStr = agencyLogo != null ? TvBo.UICommon.WriteBinaryImage(agencyLogo, UserData.AgencyID, "AG_", "", "") : "";
        string userPhone = new TvReport.AllOperator().getAgencyUserPhone(UserData.AgencyID, UserData.UserID, ref errorMsg);
        FixNotesRecord fixNote = new TvBo.Common().getFixNotes(UserData.Market, "PriceList", ref errorMsg);
        string fixNoteText = string.Empty;
        if (fixNote != null)
        {
            TvTools.RTFtoHTML rtf = new RTFtoHTML();
            rtf.rtf = fixNote.Note;
            fixNoteText = rtf.html();
        }
        string createdBy = HttpContext.GetGlobalResourceObject("CreateOffer", "Created").ToString();

        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda))
            operatorLogoStr = "";

        htmlData.Add(new TvReport.htmlCodeData { IdName = "imgOperator", TagName = "img", Data = operatorLogoStr });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "imgAgency", TagName = "img", Data = base64AgencyLogo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtCreatedBy", TagName = "span", Data = createdBy });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAgencyName", TagName = "span", Data = string.IsNullOrEmpty(agency.FirmName) ? agency.Name : agency.FirmName });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtUserName", TagName = "span", Data = UserData.UserName });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAgencyTel", TagName = "span", Data = userPhone });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtEmail", TagName = "span", Data = UserData.EMail });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtDateTime", TagName = "span", Data = DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "fixNoteText", TagName = "div", Data = fixNoteText });
        return new {
            ShowTemplateHeader = string.Equals(UserData.CustomRegID, Common.crID_SunOrient),
            Header = htmlData,
            HeaderSpec = new {
                imgAgency = base64AgencyLogo,
                txtCreatedBy = DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToShortTimeString(),
                txtAgencyName = string.IsNullOrEmpty(agency.FirmName) ? agency.Name : agency.FirmName,
                txtAgencyTel = userPhone,
                txtUserName = UserData.UserName,
                txtEmail = UserData.EMail
            }
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object removeOfferListDetail(int? RefNo, long? SearchTime)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (!SearchTime.HasValue) return false;

        string errorMsg = string.Empty;

        List<Location> locList = new Locations().getLocationList(UserData, LocationType.None, null, null, null, null, ref errorMsg);

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        List<SearchResult> offerList = new ClientOffer().readClientOfferList(UserData);
        List<SearchResult> retVal = new List<SearchResult>();
        DateTime searchTime = new DateTime(SearchTime.Value);
        retVal = offerList.Where(w => !(w.RefNo == RefNo && w.SearchTime == searchTime)).ToList<SearchResult>();
        if (new ClientOffer().saveClientOfferList(UserData, retVal))
            return true;
        else return false;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getClientOfferList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string btnReview = HttpContext.GetGlobalResourceObject("LibraryResource", "btnReview").ToString();
        string btnClear = HttpContext.GetGlobalResourceObject("LibraryResource", "btnclear").ToString();

        List<Location> locList = new Locations().getLocationList(UserData, LocationType.None, null, null, null, null, ref errorMsg);

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        string lblCreated = HttpContext.GetGlobalResourceObject("CreateOffer", "Created").ToString();
        List<SearchResult> offerList = new ClientOffer().readClientOfferList(UserData);
        var retVal = from q in offerList
                     orderby q.SearchTime descending
                     select new
                     {
                         offerGUID = q.RefNo.ToString() + "_" + q.SearchTime.Ticks.ToString(),
                         CheckIn = q.CheckIn.Value.ToShortDateString(),
                         Night = q.Night.ToString() + " " + HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblNight").ToString(),
                         CountryName = getLocationForCountryName(locList, useLocalName, q.HotelLocation),
                         HotelLocationName = useLocalName ? q.HotLocationNameL : q.HotLocationName,
                         HotelName = useLocalName ? q.HotelNameL : q.HotelName,
                         HotelCategory = q.HotCat,
                         RoomName = useLocalName ? q.RoomNameL : q.RoomName,
                         FullAccomName = q.AccomFullName,
                         Board = q.Board,
                         Price = q.LastPrice.HasValue ? q.LastPrice.Value.ToString("#,###.00") : "",
                         PriceEU = q.LastPrice.HasValue ? (q.LastPrice.Value * ((decimal)34528 / 10000)).ToString("#,###.00") : "",
                         Curr = q.SaleCur,
                         DepCityName = useLocalName ? q.DepCityNameL : q.DepCityName,
                         ArrCityName = useLocalName ? q.ArrCityNameL : q.ArrCityName,
                         CreateTime = q.SearchTime.ToShortDateString() + " " + q.SearchTime.ToShortTimeString(),
                         lblCreated = lblCreated,
                         ShowFlight = string.IsNullOrEmpty(strFunc.Trim(q.DepFlight, ' ')) || string.IsNullOrEmpty(strFunc.Trim(q.RetFlight, ' ')),
                         Departure = q.DepFlight,
                         Arrival = q.RetFlight
                     };
        return new
        {
            cODetail = retVal,
            btnReview = btnReview,
            btnClear = btnClear
        };
    }

    public static string getLocationForCountryName(List<Location> locList, bool useLocalName, int? location)
    {
        Location loc = locList.Find(f => f.RecID == location);
        if (loc != null)
        {
            Location country = locList.Find(f => f.RecID == loc.Country);
            if (country != null)
            {
                return useLocalName ? country.NameL : country.Name;
            }
            else
                return string.Empty;
        }
        else
            return string.Empty;
    }

    public static List<SearchResult> removeOldOffers(User UserData, List<SearchResult> offerList)
    {
        int? durationDay = Conversion.getInt32OrNull(ConfigurationManager.AppSettings["ClientOfferDurationDay"]);
        List<SearchResult> retVal = (from q in offerList
                                     where q.SearchTime >= DateTime.Now.AddDays(durationDay.HasValue ? durationDay.Value * (-1) : -2)
                                     select q).ToList<SearchResult>();
        if (offerList.Count != retVal.Count)
            new ClientOffer().saveClientOfferList(UserData, retVal);
        return retVal;
    }
}
