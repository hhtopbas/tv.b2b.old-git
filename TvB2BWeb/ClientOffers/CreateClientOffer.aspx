﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CreateClientOffer.aspx.cs" Inherits="CreateClientOffer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" id="htmlCreateOffer">
<head runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    <%= GetGlobalResourceObject("PageTitle", "CreateOffer") %></title>

  <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="../Scripts/jquery.json.js" type="text/javascript"></script>
  <script src="../Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
  <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
  <script src="../Scripts/mustache.js" type="text/javascript"></script>
  <script src="../Scripts/jquery.dialogextend.js" type="text/javascript"></script>

  <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="CreateClientOffer.css" rel="stylesheet" />

  <style type="text/css">
    .noPDF { display: none; visibility: hidden; }
    .titleColor .ui-widget-header { background-color: #6666CC; background-image: none; color: white; }
  </style>

  <script type="text/javascript">

    if (typeof window.event != 'undefined')
      document.onkeydown = function () {
        if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
          return (event.keyCode != 8);
      }
    else
      document.onkeypress = function (e) {
        if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
          return (e.keyCode != 8);
      }

    function logout() {
      self.parent.logout();
    }

    function getFormData() {
      $.ajax({
        async: false,
        type: "POST",
        url: "CreateClientOffer.aspx/getFormData",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {

          if (msg.hasOwnProperty('d') && msg.d != null) {
            var data = msg.d;
            $.each(data, function (i) {
              switch (this.TagName) {
                case 'span':
                  $("#" + this.IdName).text('');
                  $("#" + this.IdName).text(this.Data);
                  break;
                case 'div':
                  $("#" + this.IdName).html('');
                  $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                  break;
                case 'img':
                  $("#" + this.IdName).removeAttr("src");
                  $("#" + this.IdName).attr("src", this.Data);
                  if (this.Data == '')
                    $("#" + this.IdName).hide();
                  break;
                default: $("#" + this.IdName).val('');
                  $("#" + this.IdName).val(this.Data);
              }

            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });

      $.query = $.query.load(location.href);

      if ($.query.get('print')) {
        $('input:checkbox').removeAttr('checked');

        $.each($.query.keys, function (i) {
          if (i != 'ResNo' && i != 'docName' && i != 'print' && i != '__VIEWSTATE') {
            var elm = $("#" + i);
            if (this == 'on' || this == 'off') {
              if (this == 'on')
                elm.attr('checked', 'checked');
              else
                elm.removeAttr('checked');
            }
            else if (!(this == true || this == false)) {
              elm.val(this);
            }
          }
        });
        window.print();
        window.close();
      }
    }

    function reCalc() {
      getFormData();
    }

    function clearList() {
      $.ajax({
        async: false,
        type: "POST",
        url: "CreateClientOffer.aspx/clearList",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null && msg.d == true) {
            getResultGrid();
            getOfferListHtml();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function resetList() {
      $.ajax({
        async: false,
        type: "POST",
        url: "CreateClientOffer.aspx/resetList",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null && msg.d == true) {
            getResultGrid();
            getOfferListHtml();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function removeClientDetail(detailID) {
      var params = new Object();
      params.RefNo = detailID.toString().split('_')[0];
      params.SearchTime = detailID.toString().split('_')[1];
      $.ajax({
        async: false,
        type: "POST",
        url: "CreateClientOffer.aspx/removeClientDetail",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var elm = $("#" + msg.d.ID);
            $("#" + msg.d.ID).removeAttr('disabled');
            $("#" + msg.d.ID + "> * ").removeAttr('disabled');
            $("#img_" + detailID).show();
            $("#remove_" + detailID).show();
            getOfferListHtml();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function removeOfferListDetail(detailID) {
      var params = new Object();
      params.RefNo = detailID.toString().split('_')[0];
      params.SearchTime = detailID.toString().split('_')[1];
      $.ajax({
        async: false,
        type: "POST",
        url: "CreateClientOffer.aspx/removeOfferListDetail",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != '' && msg.d == true) {
            getResultGrid();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getOfferList(tmpHtml) {
      $.ajax({
        async: false,
        type: "POST",
        url: "CreateClientOffer.aspx/getOfferList",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != '') {
            var data = msg.d;
            var html = Mustache.render(tmpHtml, data);
            $("#dataDiv").html(html);

            var erasedFlight = $(".showFlight");
            for (var i = 0; i < erasedFlight.length; i++) {
              if ($(erasedFlight[i]).css("display") == "none") {
                $(erasedFlight[i]).html('');
              }
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getOfferListHtml() {
      $.get($("#tmplPath").val() + 'SendClientOffers.html?v=201603181010', function (html) {
        getOfferList(html);
      }).fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        showAlert("Request Failed: " + err);
      });
    }

    function selectOffer(detailID) {
      var params = new Object();
      params.RefNo = detailID.toString().split('_')[0];
      params.SearchTime = detailID.toString().split('_')[1];
      $.ajax({
        async: false,
        type: "POST",
        url: "CreateClientOffer.aspx/addDetail",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != '' && msg.d == true) {
            $("#" + detailID).attr('disabled', true);
            $("#" + detailID + "> * ").attr('disabled', true);
            $("#img_" + detailID).hide();
            $("#remove_" + detailID).hide();
            getOfferListHtml();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getClientOfferList(tmpHtml) {
      $.ajax({
        async: false,
        type: "POST",
        url: "CreateClientOffer.aspx/getClientOfferList",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != '') {
            var data = msg.d;
            var html = Mustache.render(tmpHtml, data);
            $("#clientOfferList").html(html);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getResultGrid() {
      $.get($("#tmplPath").val() + 'ClientOfferList.html?v=201603181010', function (html) {
        getClientOfferList(html);
      }).fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        showAlert("Request Failed: " + err);
      });
    }

    function createClientOfferHeader(tmpHtml, data) {
      $("#headerDiv").html('');
      var html = Mustache.render(tmpHtml, data.HeaderSpec);
      $("#headerDiv").html(html);
    }

    function getHeader(data) {
      $.get($("#tmplPath").val() + 'ClientOfferHeader.html?v=201602251721', function (html) {
        createClientOfferHeader(html, data);
      }).fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        showAlert("Request Failed: " + err);
      });
    }

    function getFormHeader() {
      $.ajax({
        async: false,
        type: "POST",
        url: "CreateClientOffer.aspx/getFormHeader",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var data = msg.d;
            if (data.ShowTemplateHeader) {
              getHeader(data);
            } else {
              $.each(data.Header, function (i) {
                switch (this.TagName) {
                  case 'span':
                    $("#" + this.IdName).text('');
                    $("#" + this.IdName).text(this.Data);
                    break;
                  case 'div':
                    $("#" + this.IdName).html('');
                    $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                    break;
                  case 'img':
                    $("#" + this.IdName).removeAttr("src");
                    $("#" + this.IdName).attr("src", this.Data);
                    if (this.Data == '')
                      $("#" + this.IdName).hide();
                    break;
                  default: $("#" + this.IdName).val('');
                    $("#" + this.IdName).val(this.Data);
                }
              });
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function send(emails, subject, comment) {
      var params = new Object();
      params.Email = emails;
      params.Subject = subject;
      params.Comment = comment;

      if (params.Email == '' || params.Subject == '') {
        alert('Please fill out email and subject field.')
        return;
      }

      var htmlUrl = 'CreateClientOffer.aspx';

      var DocName = 'ClientOffer';

      var width = $(".mainPage").width();
      var viewReport = $(".mainPage").clone();
      viewReport.css("width", width);
      viewReport.find('input').each(function () {
        var tmp = $('<span/>').append($(this).clone(true).html().replace(/input/i, 'span')).html($(this).val());
        $(this).after(tmp).remove();
      });

      var _html = '<link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />';
      _html += '<link href="CreateClientOffer.css" rel="stylesheet" />';
      _html += '<html><head><link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" /><link href="CreateClientOffer.css" rel="stylesheet" /></head><body style="width:' + width + 'px;">' + viewReport.html() + '</body></html>';

      params.Content = _html;
      params.pageWidth = width;
      $.ajax({
        async: false,
        type: "POST",
        url: "CreateClientOffer.aspx/send",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null && msg.d == true) {
            alert('<%= GetGlobalResourceObject("CreateOffer","OfferSentToClient")%>');
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function sendToOffers() {
      var btnSend = '<%= GetGlobalResourceObject("LibraryResource","btnSend")%>';
      var btnClose = '<%= GetGlobalResourceObject("LibraryResource","btnClose")%>';
      $("#emailAddr").val('');
      $("#emailSubject").val('');
      $("#agencyComments").val('');

      $("#dialog").dialog("destroy");
      $("#dialog-sendOffer").dialog({
        modal: true,
        height: 400,
        width: 720,
        buttons: [{
          text: btnSend,
          click: function () {
            var emailAddr = $("#emailAddr").val();
            var emailSubject = $("#emailSubject").val();
            var agencyComment = $("#agencyComments").val();
            send(emailAddr, emailSubject, agencyComment);
            if (emailAddr != '' && emailSubject != '')
              $("#dialog-sendOffer").dialog("close");

            return false;
          }
        }, {
          text: btnClose,
          click: function () {
            $("#dialog-sendOffer").dialog("close");
            return false;
          }
        }]
      });
    }

    function pdfOffers() {
      var htmlUrl = 'CreateClientOffer.aspx';

      var DocName = 'ClientOffer';

      var width = $(".mainPage").width();
      var viewReport = $(".mainPage").clone();
      viewReport.css("width", width);
      viewReport.find('input').each(function () {
        var tmp = $('<span/>').append($(this).clone(true).html().replace(/input/i, 'span')).html($(this).val());
        $(this).after(tmp).remove();
      });

      var _html = '<link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />';
      _html += '<link href="CreateClientOffer.css" rel="stylesheet" />';
      _html += '<html><head><link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" /><link href="CreateClientOffer.css" rel="stylesheet" /></head><body style="width:' + width + 'px;">' + viewReport.html() + '</body></html>';

      var obj = new Object();
      obj.reportType = DocName;
      obj._html = _html;
      obj.pageWidth = width;
      obj.urlBase = htmlUrl;
      obj.param = null;
      $.ajax({
        type: "POST",
        url: "CreateClientOffer.aspx/viewPDFReport",
        data: $.json.encode(obj),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d != '') {
            $("#viewReport").removeAttr("src");
            var _pdfViewPage = "../ViewPDF.aspx"
            $("#viewReport").attr("src", _pdfViewPage + "?url=" + msg.d);
            $("#dialog-viewReport").dialog(
                {
                  dialogClass: 'titleColor',
                  autoOpen: true,
                  modal: true,
                  close: function () {
                    $("#viewReport").attr("src", "");
                    $("#dialog-viewReport").hide();
                  }
                }).dialogExtend({
                  "maximize": true,
                  "icons": {
                    "maximize": "ui-icon-circle-plus",
                    "restore": "ui-icon-pause"
                  }
                });
            $("#dialog-viewReport").dialogExtend("maximize");
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    $(document).ready(function () {
      getFormHeader();
      getResultGrid();
    });
  </script>

</head>
<body class="ui-helper-reset ui-helper-clearfix">
  <form id="form1" runat="server">
    <div class="noPrint " style="width: 300px; float: left;">
      <div id="clientOfferList" class="clientOfferList">
      </div>
    </div>
    <div class="ui-helper-clearfix clientOfferContent">
      <input id="param1" type="hidden" value="" />
      <asp:HiddenField ID="tmplPath" runat="server" />
      <div class="noPrint">
        <div class="sendOfferDiv ui-widget-content ui-state-highlight">
          <input type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="sendToOffers()" value="<%= GetGlobalResourceObject("LibraryResource","btnSend") %>" style="font-size: 80%;" />
          &nbsp;&nbsp;
          <input type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="pdfOffers()" value="<%= GetGlobalResourceObject("LibraryResource","btnPDF") %>" style="font-size: 80%;" />
        </div>
      </div>
      <div class="mainPage">
        <table id="headerDiv" style="width: 100%; border-bottom: solid 2px #000000;">
          <tr>
            <td style="width: 130px; text-align: center;">
              <img alt="" title="" id="imgOperator" style="width: 120px;" src="" />
            </td>
            <td style="min-width: 120px; text-align: right; vertical-align: bottom;">
              <table style="width: 98%;">
                <tr>
                  <td style="text-align: right; vertical-align: bottom;">
                    <img alt="" title="" id="imgAgency" style="width: 120px;" src="" />
                    <br />
                    <span id="txtCreatedBy" style="font-size: 9pt;"></span>
                  </td>
                  <td style="font-size: 9pt; vertical-align: bottom;">
                    <span id="txtAgencyName"></span>
                    <br />
                    <span id="txtUserName"></span>
                    <br />
                    <span id="txtAgencyTel"></span>
                    <br />
                    <span id="txtEmail"></span>
                    <br />
                    <span id="txtDateTime"></span>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <div class="font8pt">
          <div id="fixNoteText">
          </div>
        </div>
        <div class="clearDiv">
        </div>
        <div id="dataDiv">
        </div>
      </div>
    </div>
  </form>
  <div class="noPrint">
    <div id="dialog-sendOffer" title="" class="ui-helper-hidden ui-helper-reset">
      <div id="sendOffer" class="sendOffer">
        <br />
        <table>
          <tr>
            <td class="emailLabel"><%= GetGlobalResourceObject("CreateOffer","Email") %>: </td>
            <td class="emailSepar"></td>
            <td class="emailInput">
              <input id="emailAddr" type="text" />
            </td>
          </tr>
          <tr>
            <td class="emailLabel"><%= GetGlobalResourceObject("CreateOffer","Subject") %>: </td>
            <td class="emailSepar"></td>
            <td class="emailInput">
              <input id="emailSubject" type="text" />
            </td>
          </tr>
          <tr>
            <td class="emailLabel"><%= GetGlobalResourceObject("CreateOffer","AgentsComments") %>: </td>
            <td class="emailSepar"></td>
            <td class="emailInput">
              <textarea id="agencyComments" rows="4" cols="1"></textarea>
            </td>
          </tr>
        </table>
      </div>
    </div>
    <div id="dialog-viewReport" title='' style="display: none; text-align: center;">
      <iframe id="viewReport" runat="server" frameborder="0" style="clear: both; width: 100%; height: 100%; text-align: left;"></iframe>
    </div>
  </div>
</body>
</html>
