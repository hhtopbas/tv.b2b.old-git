﻿using BankIntegration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using TvBo;
using TvTools;
using Winnovative;

public partial class OnlyHotelSearchV2 : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];

        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
    }

    [WebMethod(EnableSession = true)]
    public static string UserHasAuth()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        return UserData.BlackList ? "N" : "Y";
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static PackageSearchMakeRes getBookReservation(string BookList)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        object extAllotControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ExtAllotControl");
        bool ExtAllotCont = Conversion.getBoolOrNull(extAllotControl).HasValue ? Conversion.getBoolOrNull(extAllotControl).Value : false;
        object _checkAvailableFlightSeat = new TvBo.Common().getFormConfigValue("SearchPanel", "CheckAvailableFlightSeat");
        bool checkAvailableFlightSeat = Conversion.getBoolOrNull(_checkAvailableFlightSeat).HasValue ? Conversion.getBoolOrNull(_checkAvailableFlightSeat).Value : false;
        object _checkAvailableRoom = new TvBo.Common().getFormConfigValue("SearchPanel", "CheckAvailableRoom");
        bool checkAvailableRoom = Conversion.getBoolOrNull(_checkAvailableRoom).HasValue ? Conversion.getBoolOrNull(_checkAvailableRoom).Value : false;

        HttpContext.Current.Session["ResData"] = null;        
        if (HttpContext.Current.Session["Criteria"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.SearchCriteria criteria = (TvBo.SearchCriteria)HttpContext.Current.Session["Criteria"];
        if (HttpContext.Current.Session["SearchResultOH"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        List<SearchResultOH> searchResult = (List<SearchResultOH>)HttpContext.Current.Session["SearchResultOH"];
        List<int> _bookList = new List<int>();
        for (int i = 0; i < BookList.Split(',').Length; i++)
            _bookList.Add(Convert.ToInt32(BookList.Split(',')[i]));
        List<SearchResultOH> selectedRoom = (from q in searchResult
                                             join q1 in _bookList.AsEnumerable() on q.RefNo equals q1
                                             select q).ToList<SearchResultOH>();
        ResDataRecord ResData = new ResDataRecord();
        ResData.SelectBookOH = new List<SearchResultOH>();
        foreach (int row in _bookList)
        {
            SearchResultOH sr = new SearchResultOH();
            SearchCriteriaRooms sc = new SearchCriteriaRooms();
            sr = searchResult.Find(f => f.RefNo == row);
            sc = criteria.RoomsInfo.Find(f => f.RoomNr == sr.RoomNr);
            sr.ChdAge1 = sc.Chd1Age;
            sr.ChdAge2 = sc.Chd2Age;
            sr.ChdAge3 = sc.Chd3Age;
            sr.ChdAge4 = sc.Chd4Age;            
            ResData.SelectBookOH.Add(sr);
        }

        ResData = new MakeSearch2Res().getResData(UserData, ResData, criteria, ref errorMsg);

        ResData.FirstData = new ResTables().getFirstResData(ResData);
        ResData.FirstData.ReservationType = SearchType.OnlyHotelSearch;

        List<ResServiceRecord> resServiceList = ResData.ResService;
        foreach (ResServiceRecord row in resServiceList)
            row.ChkCelMakeRes2 = false;
        List<ResServiceExtRecord> resServiceExtList = ResData.ResServiceExt;
        foreach (ResServiceExtRecord row in resServiceExtList)
            row.ChkCelMakeRes2 = false;

        string version = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "Version"));
        if (string.Equals(version, "V2"))
        {
            ResData = new ReservationV2().getResDataExtras(UserData, ResData, ref errorMsg);
        }
        if (ResData.ExtrasData == null)
        {
            ResData.ExtrasData = ResData;
        }

        bool CreateChildAge = false;
        if (UserData.WebService)
            CreateChildAge = true;
        else
        {
            object _createChildAge = new TvBo.Common().getFormConfigValue("MakeReservation", "CreateChildAge");
            CreateChildAge = _createChildAge != null ? (bool)_createChildAge : false;
        }

        if (!CreateChildAge)
        {
            foreach (ResCustRecord row in ResData.ResCust)
            {
                row.Age = null;
                row.Birtday = null;
            }
            if (ResData.ExtrasData == null)
            {
                ResData.ExtrasData = new ResDataRecord();
                ResData.ExtrasData = ResData;
            }
            foreach (ResCustRecord row in ResData.ExtrasData.ResCust)
            {
                row.Age = null;
                row.Birtday = null;
            }
        }
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka))
        {
            ResMainRecord resMain = ResData.ResMain;
            resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
        }
        HttpContext.Current.Session["ResData"] = ResData;
        return new PackageSearchMakeRes { resOK = true, errMsg = string.Empty, version = version };        
    }

    [WebMethod(EnableSession = true)]
    public static string viewPDFReport(string reportType, string _html, string pageWidth, string urlBase)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        _html = _html.Replace('|', '"');
        _html = _html.Replace("<div id=\"noPrintDiv\">", "<div id=\"noPrintDiv\" style=\"display: none; visibility: hidden;\">");
        string errorMsg = string.Empty;
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        string pdfFileName = reportType + "_" + UserData.AgencyID + ".pdf";
        StringBuilder html = new StringBuilder(_html);
        int PageWidth = Conversion.getInt32OrNull(pageWidth).HasValue ? Conversion.getInt32OrNull(pageWidth).Value : 700;
        //initialize the PdfConvert object
        PdfConverter pdfConverter = new PdfConverter();
        pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
        pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.NoCompression;
        pdfConverter.PdfDocumentOptions.ShowHeader = false;
        pdfConverter.PdfDocumentOptions.ShowFooter = false;
        ////pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
        ////pdfConverter.PageWidth = PageWidth + 20;
        pdfConverter.PdfDocumentOptions.LeftMargin = 10;
        pdfConverter.PdfDocumentOptions.TopMargin = 10;
        // set the demo license key
        pdfConverter.LicenseKey = ConfigurationManager.AppSettings["Winnovative"];;

        // get the base url for string conversion which is the url from where the html code was retrieved
        // the base url is a hint for the converter to find the external CSS and images referenced by relative URLs
        string thisPageURL = HttpContext.Current.Request.Url.AbsoluteUri;
        string baseUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/";
        try
        {
            pdfConverter.SavePdfFromHtmlStringToFile(html.ToString(), siteFolderISS + "ACE\\" + pdfFileName, urlBase);
            string returnUrl = basePageUrl + "ACE/" + pdfFileName;
            return returnUrl;
        }
        catch
        {
            return "";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getBrochure(string holPack, long? CheckIn, long? CheckOut, string Market)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        DateTime begDate = CheckIn.HasValue ? new DateTime(CheckIn.Value) : DateTime.Today;
        DateTime endDate = CheckOut.HasValue ? new DateTime(CheckOut.Value) : DateTime.Today;

        string errorMsg = string.Empty;
        HolPackBrochureRecord holPackBrochure = new TvBo.Common().getHolPackBrochure(Market, holPack, begDate, endDate, ref errorMsg);
        if (holPackBrochure != null)
        {
            string fileName = holPack + "_" + CheckIn.ToString() + "_" + CheckOut.ToString() + ".PDF";
            if (File.Exists(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName))
                File.Delete(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName);
            FileStream fileStream = new FileStream(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName, FileMode.Create, FileAccess.ReadWrite);
            fileStream.Write((byte[])holPackBrochure.FileImage, 0, ((byte[])holPackBrochure.FileImage).Length);
            fileStream.Close();

            return WebRoot.BasePageRoot + "/ACE/" + fileName;
        }
        else return "";

    }

    [WebMethod(EnableSession = true)]
    public static string getAdvertising(string BookList)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["Criteria"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.SearchCriteria criteria = (TvBo.SearchCriteria)HttpContext.Current.Session["Criteria"];

        List<AdvertisingRecord> advList = new List<AdvertisingRecord>();
        string advListJSon = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "Advertising"));
        List<int> _bookList = new List<int>();
        if (!string.IsNullOrEmpty(advListJSon))
        {
            advList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AdvertisingRecord>>(advListJSon);
        }
        if (advList != null && advList.Count > 0)
        {
            if (advList.Where(w => string.Equals(w.Market, UserData.Market)).Count() < 1) return string.Empty;

            for (int i = 0; i < BookList.Split(',').Length; i++)
                _bookList.Add(Convert.ToInt32(BookList.Split(',')[i]));
            string retVal = string.Empty;
            MultiRoomResult searchResult = (MultiRoomResult)HttpContext.Current.Session["SearchResult"];
            foreach (int row in _bookList)
            {
                SearchResult sr = new SearchResult();
                SearchCriteriaRooms sc = new SearchCriteriaRooms();
                sr = searchResult.ResultSearch.Find(f => f.RefNo == row);
                var query = from q in advList
                            where string.Equals(q.Market, UserData.Market) &&
                               q.ArrCity == sr.ArrCity
                            select new { advUrl = q.AdvUrl };
                if (query != null && query.Count() > 0)
                    retVal = query.FirstOrDefault().advUrl;
            }
            return retVal;
        }
        else return string.Empty;
    }

    [WebMethod(EnableSession = true)]
    public static string getOffersUrl()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        object _ppd = new TvBo.Common().getFormConfigValue("General", "b2cOffersUrl");
        List<BIPaymentPageDefination> ppd = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BIPaymentPageDefination>>(Conversion.getStrOrNull(_ppd));
        if (ppd != null)
        {
            BIPaymentPageDefination offersUrl = ppd.Find(w => w.Market == UserData.Market && string.Equals(w.Code, "PACK"));
            return offersUrl != null ? offersUrl.B2CUrl : string.Empty;
        }
        else
            return string.Empty;

    }
}