﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SautinSoft;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Web;
using System.Text;
using System.Web.Services;
using TvTools;
using System.IO;
using BankIntegration;

namespace TvSearch
{
    public partial class CultureTourSearch : BasePage
    {       
        protected void Page_Load(object sender, EventArgs e)
        {
            User UserData = (User)Session["UserData"];
            Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url);  //Session["Menu"] = "CultureTourSearchBlank.aspx";
            Thread.CurrentThread.CurrentCulture = UserData.Ci;                     
        }        
                                     
        [WebMethod(EnableSession=true)]
        public static string UserHasAuth()
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;            
            return UserData.BlackList ? "N" : "Y";            
        }

        [WebMethod(EnableSession = true)]
        public static string getBookReservation(string BookList)
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
            
            object extAllotControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ExtAllotControl");
            bool ExtAllotCont = Conversion.getBoolOrNull(extAllotControl).HasValue ? Conversion.getBoolOrNull(extAllotControl).Value : false;
            object _checkAvailableFlightSeat = new TvBo.Common().getFormConfigValue("SearchPanel", "CheckAvailableFlightSeat");
            bool checkAvailableFlightSeat = Conversion.getBoolOrNull(_checkAvailableFlightSeat).HasValue ? Conversion.getBoolOrNull(_checkAvailableFlightSeat).Value : false;
            object _checkAvailableRoom = new TvBo.Common().getFormConfigValue("SearchPanel", "CheckAvailableRoom");
            bool checkAvailableRoom = Conversion.getBoolOrNull(_checkAvailableRoom).HasValue ? Conversion.getBoolOrNull(_checkAvailableRoom).Value : false;

            HttpContext.Current.Session["ResData"] = null;
            if (HttpContext.Current.Session["Criteria"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.SearchCriteria criteria = (TvBo.SearchCriteria)HttpContext.Current.Session["Criteria"];
            List<int> _bookList = new List<int>();
            for (int i = 0; i < BookList.Split(',').Length; i++)
                _bookList.Add(Convert.ToInt32(BookList.Split(',')[i]));
            ResDataRecord ResData = new ResDataRecord();
            ResData.SelectBook = new List<SearchResult>();
            MultiRoomResult searchResult = (MultiRoomResult)HttpContext.Current.Session["SearchResult"];
            var query = from q1 in searchResult.ResultSearch
                        join q2 in _bookList on q1.RefNo equals q2
                        select q1;
            foreach (int row in _bookList)
            {
                SearchResult sr = new SearchResult();
                SearchCriteriaRooms sc = new SearchCriteriaRooms();                
                sr = searchResult.ResultSearch.Find(f => f.RefNo == row);
                sc = criteria.RoomsInfo.Find(f => f.RoomNr == sr.RoomNr);
                sr.Child1Age = sc.Chd1Age;
                sr.Child2Age = sc.Chd2Age;
                sr.Child3Age = sc.Chd3Age;
                sr.Child4Age = sc.Chd4Age;         
                ResData.SelectBook.Add(sr);
            }
            string errorMsg = string.Empty;

            Guid? logID = null;
            String useLog = System.Configuration.ConfigurationManager.AppSettings["useLog"];
            if (!string.IsNullOrEmpty(useLog) && string.Equals(useLog, "1"))
            {
                if (ResData.SelectBook.FirstOrDefault().LogID.HasValue)
                    logID = new WEBLog().saveWEBBookLog(ResData.SelectBook.FirstOrDefault().LogID.Value, DateTime.Now, null, ref errorMsg);
            }

            ResData = new Reservation().getResData(UserData, ResData, criteria, SearchType.TourPackageSearch, false, ref errorMsg);
            if (!string.IsNullOrEmpty(errorMsg))            
                return errorMsg;

            ResData.LogID = logID;

            ResDataRecord extResdata = ResData.ExtrasData;
            if (extResdata == null)
            {
                extResdata = ResData;
                ResData.ExtrasData = new ResTables().copyData(extResdata);
            }
            bool CreateChildAge = false;
            if (UserData.WebService)
                CreateChildAge = true;
            else
            {
                object _createChildAge = new TvBo.Common().getFormConfigValue("MakeReservation", "CreateChildAge");
                CreateChildAge = _createChildAge != null ? (bool)_createChildAge : false;
            }

            if (!CreateChildAge)
            {
                foreach (ResCustRecord row in ResData.ResCust)
                {
                    row.Age = null;
                    row.Birtday = null;
                }
                foreach (ResCustRecord row in ResData.ExtrasData.ResCust)
                {
                    row.Age = null;
                    row.Birtday = null;
                }
            }

            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_SunFun) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka))
            {
                ResMainRecord resMain = ResData.ResMain;
                resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
            }

            HttpContext.Current.Session["ResData"] = ResData;

            return "";
        }

        [WebMethod(EnableSession = true)]
        public static string getBrochure(string holPack, long? CheckIn, long? CheckOut, string Market)
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            DateTime begDate = CheckIn.HasValue ? new DateTime(CheckIn.Value) : DateTime.Today;
            DateTime endDate = CheckOut.HasValue ? new DateTime(CheckOut.Value) : DateTime.Today;

            string errorMsg = string.Empty;
            HolPackBrochureRecord holPackBrochure = new TvBo.Common().getHolPackBrochure(Market, holPack, begDate, endDate, ref errorMsg);
            if (holPackBrochure != null)
            {
                string fileName = holPack + "_" + CheckIn.ToString() + "_" + CheckOut.ToString() + ".PDF";
                if (File.Exists(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName))
                    File.Delete(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName);
                FileStream fileStream = new FileStream(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName, FileMode.Create, FileAccess.ReadWrite);
                fileStream.Write((byte[])holPackBrochure.FileImage, 0, ((byte[])holPackBrochure.FileImage).Length);
                fileStream.Close();

                return WebRoot.BasePageRoot + "/ACE/" + fileName;
            }
            else return "";

        }


        [WebMethod(EnableSession = true)]
        public static string getOffersUrl()
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            object _ppd = new TvBo.Common().getFormConfigValue("General", "b2cOffersUrl");
            List<BIPaymentPageDefination> ppd = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BIPaymentPageDefination>>(Conversion.getStrOrNull(_ppd));
            if (ppd != null)
            {
                BIPaymentPageDefination offersUrl = ppd.Find(w => w.Market == UserData.Market && string.Equals(w.Code, "PACK"));
                return offersUrl != null ? offersUrl.B2CUrl : string.Empty;
            }
            else
                return string.Empty;

        }
    }

}