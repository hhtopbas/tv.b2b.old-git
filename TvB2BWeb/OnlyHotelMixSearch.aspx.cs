﻿using System;
using System.Collections.Generic;
using System.Linq;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Text;
using System.Web.Services;
using TvTools;
using System.IO;
using BankIntegration;
using System.Web.Script.Services;
using Winnovative;
using System.Configuration;

namespace TvSearch
{
    public partial class OnlyHotelMixSearch : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User UserData = (User)Session["UserData"];
            CultureInfo ci = UserData.Ci;
            Thread.CurrentThread.CurrentCulture = ci;
            if (Session["SearchMixData"] == null)
            {
                bool GroupSearch = false;
                object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
                if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
                MixSearchFilterCacheData filterData = MixSearch.getMixPackageSearchData(UserData, GroupSearch, SearchType.OnlyHotelMixSearch);
                Session["SearchMixData"] = filterData;
            }
        }

        [WebMethod]
        public static string UserHasAuth()
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            return UserData.BlackList ? "N" : "Y";
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
        public static PackageSearchMakeRes getBookReservation(string BookList)
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            object extAllotControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ExtAllotControl");
            bool ExtAllotCont = Conversion.getBoolOrNull(extAllotControl).HasValue ? Conversion.getBoolOrNull(extAllotControl).Value : false;

            object _checkAvailableRoom = new TvBo.Common().getFormConfigValue("SearchPanel", "CheckAvailableRoom");
            bool checkAvailableRoom = Conversion.getBoolOrNull(_checkAvailableRoom).HasValue ? Conversion.getBoolOrNull(_checkAvailableRoom).Value : false;

            string version = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "Version"));
            string onlyHotelVersion = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "OnlyHotelVersion"));

            HttpContext.Current.Session["ResData"] = null;
            TvBo.MixSearchCriteria paxCriteria = (TvBo.MixSearchCriteria)HttpContext.Current.Session["MixCriteria"];
            List<int> _bookList = new List<int>();
            for (int i = 0; i < BookList.Split(',').Length; i++)
                _bookList.Add(Convert.ToInt32(BookList.Split(',')[i]));
            ResDataRecord ResData = new ResDataRecord();
            ResData.SelectBook = new List<SearchResult>();
            MultiRoomResult searchResult = (MultiRoomResult)HttpContext.Current.Session["SearchResult"];
            var query = from q1 in searchResult.ResultSearch
                        join q2 in _bookList on q1.RefNo equals q2
                        select q1;
            foreach (int row in _bookList)
            {
                SearchResult sr = new SearchResult();
                MixSearchCriteriaRoom sc = new MixSearchCriteriaRoom();
                sr = searchResult.ResultSearch.Find(f => f.RefNo == row);
                sc = paxCriteria.RoomInfoList.Find(f => f.RoomNr == sr.RoomNr);
                sr.Child1Age = sc.ChildAge.Count > 0 ? (short?)sc.ChildAge[0] : null;
                sr.Child2Age = sc.ChildAge.Count > 1 ? (short?)sc.ChildAge[1] : null;
                sr.Child3Age = sc.ChildAge.Count > 2 ? (short?)sc.ChildAge[2] : null;
                sr.Child4Age = sc.ChildAge.Count > 3 ? (short?)sc.ChildAge[3] : null;
                ResData.SelectBook.Add(sr);
            }
            string paxCriteriaJson = Newtonsoft.Json.JsonConvert.SerializeObject(paxCriteria);
            SearchCriteria criteria = Newtonsoft.Json.JsonConvert.DeserializeObject<SearchCriteria>(paxCriteriaJson);
            foreach (var row in paxCriteria.RoomInfoList)
            {
                criteria.RoomsInfo.Add(new SearchCriteriaRooms()
                {
                    Adult = row.AdultCount,
                    Child = row.ChildCount,
                    Infant = row.InfantCount,
                    RoomNr = row.RoomNr,
                    Chd1Age = row.ChildAge.Count > 0 ? (short?)row.ChildAge[0] : null,
                    Chd2Age = row.ChildAge.Count > 1 ? (short?)row.ChildAge[1] : null,
                    Chd3Age = row.ChildAge.Count > 2 ? (short?)row.ChildAge[2] : null,
                    Chd4Age = row.ChildAge.Count > 3 ? (short?)row.ChildAge[3] : null
                });
            }
            criteria.RoomCount = (short)criteria.RoomsInfo.Count;
            string errorMsg = string.Empty;

            Guid? logID = null;
            String useLog = System.Configuration.ConfigurationManager.AppSettings["useLog"];
            if (!string.IsNullOrEmpty(useLog) && string.Equals(useLog, "1"))
            {
                if (ResData.SelectBook.FirstOrDefault().LogID.HasValue)
                    logID = new WEBLog().saveWEBBookLog(ResData.SelectBook.FirstOrDefault().LogID.Value, DateTime.Now, null, ref errorMsg);
            }

            int totalPax = ResData.SelectBook.Sum(s => (s.HAdult + s.HChdAgeG2 + s.HChdAgeG3 + s.HChdAgeG4 + (s.ChdG1Age2.HasValue && s.ChdG1Age2.Value > (decimal)(199 / 100) ? s.HChdAgeG1 : 0)));
            int maxTotalPax = UserData.AgencyRec.MaxPaxCnt.HasValue ? UserData.AgencyRec.MaxPaxCnt.Value : (UserData.TvParams.TvParamReser.MaxPaxCnt.HasValue ? UserData.TvParams.TvParamReser.MaxPaxCnt.Value : 10);
            if (maxTotalPax < totalPax)
            {
                string msg = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "MaxPaxReservation").ToString(), maxTotalPax.ToString());
                return new PackageSearchMakeRes { resOK = false, errMsg = msg, version = version };
            }
            if (!ExtAllotCont)
                if (!(new Hotels().AllotmentControlForMultipleRoom(UserData, ResData.SelectBook, "O", ref errorMsg)))
                    return new PackageSearchMakeRes { resOK = false, errMsg = errorMsg, version = version };
            if (string.Equals(onlyHotelVersion, "V2"))
                ResData = new Reservation().getResDataV2(UserData, ResData, criteria, ref errorMsg);
            else ResData = new Reservation().getResData(UserData, ResData, criteria, SearchType.OnlyHotelSearch, false, ref errorMsg);

            ResData.FirstData = new ResTables().getFirstResData(ResData);
            ResData.FirstData.ReservationType = SearchType.OnlyHotelSearch;

            if (!string.IsNullOrEmpty(errorMsg))
                return new PackageSearchMakeRes { resOK = false, errMsg = errorMsg, version = version };

            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            {
                ResMainRecord resMain = ResData.ResMain;
                resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
            }

            if (string.Equals(version, "V2"))
                ResData = new ReservationV2().getResDataExtras(UserData, ResData, ref errorMsg);

            if (ResData.ExtrasData == null)
                ResData.ExtrasData = ResData;

            ResData.LogID = logID;

            bool CreateChildAge = false;
            if (UserData.WebService)
                CreateChildAge = true;
            else
            {
                object _createChildAge = new TvBo.Common().getFormConfigValue("MakeReservation", "CreateChildAge");
                CreateChildAge = _createChildAge != null ? (bool)_createChildAge : false;
            }

            if (!CreateChildAge)
            {
                foreach (ResCustRecord row in ResData.ResCust)
                {
                    row.Age = null;
                    row.Birtday = null;
                }
                foreach (ResCustRecord row in ResData.ExtrasData.ResCust)
                {
                    row.Age = null;
                    row.Birtday = null;
                }
            }

            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka))
            {
                ResMainRecord resMain = ResData.ResMain;
                resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
            }

            HttpContext.Current.Session["ResData"] = ResData;

            return new PackageSearchMakeRes { resOK = true, errMsg = string.Empty, version = version };
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
        public static PackageSearchMakeRes getMixBookReservation(string BookList)
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            string errorMsg = string.Empty;
            HttpContext.Current.Session["ResData"] = null;
            TvBo.MixSearchCriteria criteria = (TvBo.MixSearchCriteria)HttpContext.Current.Session["MixCriteria"];
            List<string> _bookList = new List<string>();
            for (int i = 0; i < BookList.Split(',').Length; i++)
                _bookList.Add(BookList.Split(',')[i]);
            MixResData ResData = new MixResData();

            GetHotelOfferDetailsResponse offerDetails = new MixSearchPaximum().getOfferDetails(UserData, BookList, ref errorMsg);
            if (offerDetails == null)
            {
                switch (errorMsg)
                {
                    case "StopSale":
                        errorMsg = "Your offer's availability has been changed. Please search another criteries";
                        break;
                    default:
                        errorMsg = "There is an error in the system. Try again.";
                        break;
                }
                return new PackageSearchMakeRes { resOK = false, errMsg = errorMsg, version = "MIX" };
            }

            CreateOrder pParams = new CreateOrder();

            //pParams.UserId = "ee98baab-8207-4bee-b792-a2ed0479d19f";//paximumUserToken.sub;
            pParams.Travellers = new Traveller[0];
            Room[] rooms = offerDetails.Rooms;
            List<BookRoom> bookRoom = new List<BookRoom>();
            List<Traveller> customers = new List<Traveller>();
            bool leader = true;
            foreach (Room row in rooms)
            {
                BookRoom bookR = new BookRoom();
                foreach (var cRow in row.Travellers)
                {
                    cRow.TravellerNo = Guid.NewGuid().ToString();
                    cRow.isLead = leader;
                    leader = false;
                    customers.Add(cRow);
                }
                bookR.RoomId = row.Id;
                bookR.Travellers = row.Travellers.Select(s => s.TravellerNo).ToArray();
                bookRoom.Add(bookR);
            }

            pParams.Travellers = customers.ToArray();
            pParams.HotelBookings = new HotelBooking[] {new HotelBooking(){
                    OfferId=offerDetails.Id,
                    ReservationNote="",
                    Rooms= bookRoom.ToArray()
                }
            };

            ResData.offerDetail = offerDetails;
            ResData.order = pParams;

            HttpContext.Current.Session["ResData"] = ResData;

            errorMsg = string.Empty;

            if (offerDetails.Warnings != null && offerDetails.Warnings.Length > 0)
            {
                foreach (string row in offerDetails.Warnings)
                {
                    if (string.Equals(row, "PriceChanged"))
                    {

                        errorMsg += string.Format("Booking amount have been changed as {0}.{1}. Would you like to continue this booking?", offerDetails.Price.Amount.ToString("#,###.00"), offerDetails.Price.Currency);

                    }
                    if (string.Equals(row, "AvailabilityChanged"))
                    {
                        if (string.IsNullOrEmpty(errorMsg)) errorMsg += "<br />";
                        errorMsg += "Booking status have been changed as on request.Would you like to continue this booking ?";
                    }
                }
            }

            return new PackageSearchMakeRes
            {
                resOK = true,
                errMsg = errorMsg,
                version = "MIX"
            };

        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
        public static PackageSearchMakeRes getNewMixBookReservation(string pOfferId, Hotel pHotelDetail)
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci;
            Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            string errorMsg = string.Empty;
            HttpContext.Current.Session["ResData"] = null;
            
            MixResData ResData = new MixResData();
            ResData.hotelDetail = pHotelDetail;

            GetHotelOfferDetailsResponse offerDetails = new MixSearchPaximum().getOfferDetails(UserData, pOfferId, ref errorMsg);
            if (offerDetails == null) {
                switch (errorMsg) {
                    case "StopSale":
                        errorMsg = "Your offer's availability has been changed. Please search another criteries";
                        break;
                    default:
                        errorMsg = "There is an error in the system. Try again.";
                        break;
                }
                return new PackageSearchMakeRes { resOK = false, errMsg = errorMsg, version = "MIX" };
            }

            CreateOrder pParams = new CreateOrder();

            //pParams.UserId = "ee98baab-8207-4bee-b792-a2ed0479d19f";//paximumUserToken.sub;
            pParams.Travellers = new Traveller[0];
            Room[] rooms = offerDetails.Rooms;
            List<BookRoom> bookRoom = new List<BookRoom>();
            List<Traveller> customers = new List<Traveller>();
            bool leader = true;
            foreach (Room row in rooms) {
                BookRoom bookR = new BookRoom();
                foreach (var cRow in row.Travellers) {
                    cRow.TravellerNo = Guid.NewGuid().ToString();
                    cRow.isLead = leader;
                    leader = false;
                    customers.Add(cRow);
                }
                bookR.RoomId = row.Id;
                bookR.Travellers = row.Travellers.Select(s => s.TravellerNo).ToArray();
                bookRoom.Add(bookR);
            }

            pParams.Travellers = customers.ToArray();
            pParams.HotelBookings = new HotelBooking[] {new HotelBooking(){
                    OfferId=offerDetails.Id,
                    ReservationNote="",
                    Rooms= bookRoom.ToArray()
                }
            };

            ResData.offerDetail = offerDetails;
            ResData.order = pParams;

            HttpContext.Current.Session["ResData"] = ResData;

            errorMsg = string.Empty;

            if (offerDetails.Warnings != null && offerDetails.Warnings.Length > 0) {
                foreach (string row in offerDetails.Warnings) {
                    if (string.Equals(row, "PriceChanged")) {

                        errorMsg += string.Format("Booking amount have been changed as {0}.{1}. Would you like to continue this booking?", offerDetails.Price.Amount.ToString("#,###.00"), offerDetails.Price.Currency);

                    }
                    if (string.Equals(row, "AvailabilityChanged")) {
                        if (string.IsNullOrEmpty(errorMsg))
                            errorMsg += "<br />";
                        errorMsg += "Booking status have been changed as on request.Would you like to continue this booking ?";
                    }
                }
            }

            return new PackageSearchMakeRes {
                resOK = true,
                errMsg = errorMsg,
                version = "MIX"
            };
        }

        private static BookRoom[] BookRoomMap(Room[] room)
        {
            var bookRoom = new List<BookRoom>();
            bookRoom = room.Select(x =>
            {
                List<string> cust = new List<string>();
                foreach (Traveller r1 in x.Travellers)
                    cust.Add(Guid.NewGuid().ToString());
                return new BookRoom
                {
                    RoomId = x.Id,
                    Travellers = cust.ToArray()
                };
            }).ToList();
            return bookRoom.ToArray();
        }

        [WebMethod(EnableSession = true)]
        public static string viewPDFReport(string reportType, string _html, string pageWidth, string urlBase)
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci;
            Thread.CurrentThread.CurrentUICulture = UserData.Ci;
            _html = _html.Replace('|', '"');
            _html = _html.Replace("<div id=\"noPrintDiv\">", "<div id=\"noPrintDiv\" style=\"display: none; visibility: hidden;\">");
            string errorMsg = string.Empty;
            string basePageUrl = WebRoot.BasePageRoot;
            string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
            string pdfFileName = reportType + "_" + UserData.AgencyID + ".pdf";
            StringBuilder html = new StringBuilder(_html);
            int PageWidth = Conversion.getInt32OrNull(pageWidth).HasValue ? Conversion.getInt32OrNull(pageWidth).Value : 700;
            //initialize the PdfConvert object
            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.NoCompression;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.LeftMargin = 10;
            pdfConverter.PdfDocumentOptions.TopMargin = 10;
            // set the demo license key
            pdfConverter.LicenseKey = ConfigurationManager.AppSettings["Winnovative"]; ;

            // get the base url for string conversion which is the url from where the html code was retrieved
            // the base url is a hint for the converter to find the external CSS and images referenced by relative URLs
            string thisPageURL = HttpContext.Current.Request.Url.AbsoluteUri;
            string baseUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/";
            try
            {
                pdfConverter.SavePdfFromHtmlStringToFile(html.ToString(), siteFolderISS + "ACE\\" + pdfFileName, urlBase);
                string returnUrl = basePageUrl + "ACE/" + pdfFileName;
                return returnUrl;
            }
            catch
            {
                return "";
            }
        }

        [WebMethod(EnableSession = true)]
        public static string getBrochure(string holPack, long? CheckIn, long? CheckOut, string Market)
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            DateTime begDate = CheckIn.HasValue ? new DateTime(CheckIn.Value) : DateTime.Today;
            DateTime endDate = CheckOut.HasValue ? new DateTime(CheckOut.Value) : DateTime.Today;

            string errorMsg = string.Empty;
            HolPackBrochureRecord holPackBrochure = new TvBo.Common().getHolPackBrochure(Market, holPack, begDate, endDate, ref errorMsg);
            if (holPackBrochure != null)
            {
                string fileName = holPack + "_" + CheckIn.ToString() + "_" + CheckOut.ToString() + ".PDF";
                if (File.Exists(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName))
                    File.Delete(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName);
                FileStream fileStream = new FileStream(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName, FileMode.Create, FileAccess.ReadWrite);
                fileStream.Write((byte[])holPackBrochure.FileImage, 0, ((byte[])holPackBrochure.FileImage).Length);
                fileStream.Close();

                return WebRoot.BasePageRoot + "/ACE/" + fileName;
            }
            else return "";

        }
        [WebMethod(EnableSession = true)]

        public static string getOffersUrl()
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            object _ppd = new TvBo.Common().getFormConfigValue("General", "b2cOffersUrl");
            List<BIPaymentPageDefination> ppd = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BIPaymentPageDefination>>(Conversion.getStrOrNull(_ppd));
            if (ppd != null)
            {
                BIPaymentPageDefination offersUrl = ppd.Find(w => w.Market == UserData.Market && string.Equals(w.Code, "PACK"));
                return offersUrl != null ? offersUrl.B2CUrl : string.Empty;
            }
            else
                return string.Empty;

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
        public static object getHotelDetais(string hotelId)
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci;
            Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            string errorMsg = string.Empty;

            HotelDetailsResponse hotelDetails = new MixSearchPaximum().getHotelDetais(UserData, hotelId, ref errorMsg);

            return new
            {
                Raiting = hotelDetails.Rating.ToString("#.00"),
                HotelImageUrl = "http://media.dev.paximum.com/hotelimages_125x125/" + hotelDetails.Photos.FirstOrDefault(),
                HotelName = hotelDetails.Name,
                StarStr = hotelDetails.Stars.ToString().Replace(",", "").Replace(".", ""),
                HotelLocation = hotelDetails.City.name + " / " + hotelDetails.Country.name,
                Description = hotelDetails.Description,
                Facilities = hotelDetails.Facilities.Select(s => s.Description).ToArray(),
                HotelDescription = hotelDetails.Content.FirstOrDefault().Description,
                Photos = hotelDetails.Photos
            };
        }
    }

}