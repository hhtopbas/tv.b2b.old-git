﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Comment.ascx.cs" Inherits="Controls_Comment" %>
<link href="CSS/Comment.css" rel="stylesheet" type="text/css" />
<div id="divComment_Controls" class="divComment_Controls">
    <span style="font-size: 12pt"><b><%= GetGlobalResourceObject("ResMonitor", "lblResNo")%>:&nbsp; <span id="lblCommentHeader"></span></b>
    </span>
    <br />
    <div id="divgridComment" class="divgridComment">
        <div id="gridComment" class="gridComment"></div>
    </div>
    <br />
    <asp:TextBox ID="txtMessage" runat="server" Width="775px" Height="125px" TextMode="MultiLine" />
</div>
