﻿using System;
using System.Threading;
using TvBo;

public partial class HotelSaleBlank : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (!IsPostBack)
        {
            Session["SearchMixData"] = null;
            Session["MixCriteria"] = null;            
            Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url); 
            Response.Redirect("~/HotelSale.aspx" + Request.Url.Query);
        }
    }
}
