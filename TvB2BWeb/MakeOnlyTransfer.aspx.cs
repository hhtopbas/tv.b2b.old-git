﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using TvTools;

public partial class MakeOnlyTransfer : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object userBlackList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        return new
        {
            isBlackList = UserData.BlackList,
            message = UserData.BlackList ? HttpContext.GetGlobalResourceObject("LibraryResource", "NoAuthResForBlacklist").ToString() : string.Empty
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["OnlyTransferFilter"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        OnlyTransferFilter criteria = (OnlyTransferFilter)HttpContext.Current.Session["OnlyTransferFilter"];
        return new
        {
            showWhereInvoiceDiv = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr),
            Direction = criteria.Direction
        };
    }

    internal static object getObjectCustInfo(TvBo.ResCustInfoRecord row, string fieldName)
    {
        System.Reflection.PropertyInfo[] oProps = null;
        oProps = row.GetType().GetProperties();
        List<System.Reflection.PropertyInfo> _pi = (from q in oProps
                                                    where q.Name == fieldName
                                                    select q).ToList<System.Reflection.PropertyInfo>();
        object value = null;
        if (_pi != null && _pi.Count() > 0)
        {
            System.Reflection.PropertyInfo pi = _pi[0];
            value = pi.GetValue(row, null) == null ? DBNull.Value : pi.GetValue(row, null);
        }
        return value;
    }

    internal static string getResCustInfoImport(User UserData, ResDataRecord ResData, int? resCustNo, int? cnt, bool visable, string tmpCustInfoEdit, ref string errorMsg)
    {
        if (string.IsNullOrEmpty(tmpCustInfoEdit)) return string.Empty;

        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        ResCustRecord cust = ResData.ResCust.Find(f => f.CustNo == resCustNo);
        ResCustInfoRecord custInfo = ResData.ResCustInfo.Find(f => f.CustNo == resCustNo);
        if (custInfo == null) custInfo = new ResCustInfoRecord();
        int lastPosition = 0;
        string retVal = string.Empty;
        string tmpTemplate = tmpCustInfoEdit;
        List<IntCountryListRecord> countryList = new TvBo.Common().getCountryList(UserData, ref errorMsg);
        List<Location> LocationList = CacheObjects.getLocationList(UserData.Market);
        Location country = LocationList.Find(f => f.RecID == UserData.Country);
        string countryListStr = string.Empty;
        string selectedCountryCode = string.Empty;
        foreach (IntCountryListRecord row in countryList)
        {
            string selectStr = string.Empty;
            if (Equals(custInfo.AddrHomeCountry, row.CountryName) || Equals(custInfo.AddrHomeCountryCode, row.IntCode))
            {
                selectStr = "selected=\"selected\"";
                selectedCountryCode = row.CountryPhoneCode;
            }
            else if (Equals(row.IntCode, country != null ? country.CountryCode.ToString() : ""))
            {
                selectStr = "selected=\"selected\"";
                selectedCountryCode = row.CountryPhoneCode;
            }
            countryListStr += string.Format("<option value='{0}' {2}>{1}</option>",
                                                row.CountryName,
                                                row.CountryName,
                                                selectStr);
        }
        bool exit = true;
        while (exit)
        {
            string valueTemp = string.Empty;
            int first = tmpTemplate.IndexOf("{[");
            if (first > -1)
            {
                lastPosition = first;
                tmpTemplate = tmpTemplate.Remove(0, first + 2);
                int last = tmpTemplate.IndexOf("]}");
                valueTemp = tmpTemplate.Substring(0, last);
                tmpTemplate = tmpTemplate.Remove(0, last + 2);
                string[] local = valueTemp.Trim().Split(',');
                if (local.Length > 1)
                {
                    string classKey = local[0].Trim('"');
                    string resourceKey = local[1].Trim('"');
                    string localStr = HttpContext.GetGlobalResourceObject(classKey, resourceKey).ToString();
                    tmpCustInfoEdit = tmpCustInfoEdit.Replace("{[" + valueTemp + "]}", localStr);
                }
            }
            else exit = false;
        }
        tmpTemplate = tmpCustInfoEdit;
        exit = true;
        while (exit)
        {
            string valueTemp = string.Empty;
            int first = tmpTemplate.IndexOf("{");
            if (first > -1)
            {
                lastPosition = first;
                tmpTemplate = tmpTemplate.Remove(0, first + 1);
                int last = tmpTemplate.IndexOf("}");
                valueTemp = tmpTemplate.Substring(0, last);
                valueTemp = valueTemp.Trim('"');
                tmpTemplate = tmpTemplate.Remove(0, last + 1);

                string valueStr = string.Empty;
                if (valueTemp.IndexOf('.') > -1)
                {
                    if (Equals(valueTemp, "UserData.Ci.LCID"))
                        valueStr = UserData.Ci.LCID.ToString();
                }
                else
                    if (valueTemp.IndexOf('-') > -1)
                    {
                        string[] valuelist = valueTemp.Split('-');
                        List<TvTools.objectList> objectValues = new List<objectList>();
                        Type _type = typeof(System.String);
                        for (int i = 0; i < valuelist.Length; i++)
                        {
                            object value = getObjectCustInfo(custInfo, valuelist[i]);
                            objectValues.Add(new TvTools.objectList
                            {
                                TypeName = value.GetType().Name,
                                Value = value
                            });
                        }
                        object obj = mathLib.returnFormulaMinus(objectValues, ref _type);
                        valueStr = Conversion.getObjectToString(obj, _type);
                    }
                    else
                        if (valueTemp.IndexOf('+') > -1)
                        {
                            string[] valuelist = valueTemp.Split('+');
                            List<TvTools.objectList> objectValues = new List<objectList>();
                            Type _type = typeof(System.String);
                            for (int i = 0; i < valuelist.Length; i++)
                            {
                                object value = getObjectCustInfo(custInfo, valuelist[i]);
                                objectValues.Add(new TvTools.objectList
                                {
                                    TypeName = value.GetType().Name,
                                    Value = value
                                });
                            }
                            object obj = mathLib.returnFormulaPlus(objectValues, ref _type);
                            valueStr = Conversion.getObjectToString(obj, _type);
                        }
                        else
                        {
                            if (string.Equals("VisableElement", valueTemp))
                            {
                                if (!visable)
                                    valueStr = "elementHidden";
                            }
                            else
                                if (string.Equals("AddrHomeCountryCode", valueTemp))
                                {
                                    valueStr = countryListStr;
                                }
                                else
                                    if (string.Equals("MobTelCountryCode", valueTemp))
                                    {
                                        valueStr = "+" + selectedCountryCode;
                                    }
                                    else
                                        if (string.Equals("AddrHomeTelCountryCode", valueTemp))
                                        {
                                            valueStr = "+" + selectedCountryCode;
                                        }
                                        else
                                            if (string.Equals("AddrHomeFaxCountryCode", valueTemp))
                                            {
                                                valueStr = "+" + selectedCountryCode;
                                            }
                                            else
                                            {
                                                object value = getObjectCustInfo(custInfo, valueTemp);
                                                valueStr = Conversion.getObjectToString(value, value.GetType());
                                            }
                        }
                tmpCustInfoEdit = tmpCustInfoEdit.Replace("{[]}", custInfo.CustNo.ToString());
                tmpCustInfoEdit = tmpCustInfoEdit.Replace("[]", cnt.ToString());
                tmpCustInfoEdit = tmpCustInfoEdit.Replace("{\"" + valueTemp + "\"}", valueStr);
            }
            else exit = false;
        }
        return tmpCustInfoEdit;
    }

    internal static string getResCustInfoTemplate(User UserData)
    {
        string sb = string.Empty;
        string filePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + new UICommon().getWebID() + "\\" + UserData.Market + "\\OnlyTransferResCustInfoEdit.tmpl";
        if (System.IO.File.Exists(filePath))
        {
            System.IO.StreamReader Tex = new System.IO.StreamReader(filePath);
            sb = Tex.ReadToEnd();
            Tex.Close();
        }
        return sb;
    }

    internal static string getTemplate(User UserData)
    {
        string sb = string.Empty;
        string filePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + new UICommon().getWebID() + "\\" + UserData.Market + "\\OnlyTransferResCustEdit.tmpl";
        if (System.IO.File.Exists(filePath))
        {
            System.IO.StreamReader Tex = new System.IO.StreamReader(filePath);
            sb = Tex.ReadToEnd();
            Tex.Close();
        }
        return sb;
    }

    internal static string getResCustDivTmp(User UserData, ResDataRecord ResData, string tmpCustEdit, ref string errorMsg)
    {
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        int lastPosition = 0;
        string retVal = string.Empty;
        string tmpTemplate = tmpCustEdit;

        string dateMaskA = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ');
        string[] dateMaskB = dateMaskA.Split(UserData.Ci.DateTimeFormat.DateSeparator[0]);
        string _dateMask = string.Empty;
        string _dateFormat = string.Empty;
        foreach (string row in dateMaskB)
            if (!string.IsNullOrEmpty(row.ToString()))
                switch (row[0].ToString().ToLower())
                {
                    case "d": _dateMask += "/99"; _dateFormat += "/dd"; break;
                    case "m": _dateMask += "/99"; _dateFormat += "/MM"; break;
                    case "y": _dateMask += "/9999"; _dateFormat += "/yyyy"; break;
                    default: break;
                }
        _dateMask = _dateMask.Remove(0, 1);
        _dateFormat = _dateFormat.Remove(0, 1);
        string dateFormat = new TvBo.Common().getDateFormat(UserData.Ci);
        string _phoneMask = string.Empty;
        if (UserData.PhoneMask != null)
            _phoneMask = UserData.PhoneMask.MobilPhoneMask != null ? UserData.PhoneMask.MobilPhoneMask.Replace('#', '9') : "";

        List<string> SrrcServiceTypeList = new Ssrcs().getSsrcServiceTypes(UserData.Market, ResData.ResMain.PLMarket, ref errorMsg);

        string _dateFormatRegion = new TvBo.Common().getDateFormatRegion(UserData.Ci);
        tmpCustEdit = tmpCustEdit.Replace("{\"dateFormatRegion\"}", _dateFormatRegion);
        string tmpCustInfo = getResCustInfoTemplate(UserData);

        bool seachCust = new Agency().getRole(UserData, 504);

        bool exit = true;
        while (exit)
        {
            string valueTemp = string.Empty;
            int first = tmpTemplate.IndexOf("[{");
            if (first > -1)
            {
                lastPosition = first;
                tmpTemplate = tmpTemplate.Remove(0, first + 2);
                int last = tmpTemplate.IndexOf("}]");
                valueTemp = tmpTemplate.Substring(0, last);
                tmpTemplate = tmpTemplate.Remove(0, last + 2);
                string[] local = valueTemp.Trim().Split(',');
                if (local.Length > 1)
                {
                    string classKey = local[0].Trim('\"');
                    string resourceKey = local[1].Trim('\"');
                    string localStr = HttpContext.GetGlobalResourceObject(classKey, resourceKey).ToString();
                    tmpCustEdit = tmpCustEdit.Replace("[{" + valueTemp + "}]", localStr);
                }
            }
            else exit = false;
        }

        tmpTemplate = tmpCustEdit;

        string tmpLoopSection = tmpCustEdit.Substring(tmpCustEdit.IndexOf("{loop"));
        tmpLoopSection = tmpLoopSection.Substring(5, tmpLoopSection.IndexOf("loop}") - 5);
        string loopSection = string.Empty;
        Int16 cnt = 0;
        List<DDLData> nations = CacheObjects.getNations(UserData.Market);
        List<Nationality> nationalityList = CacheObjects.getNationality(UserData);
        List<TitleAgeRecord> title = ResData.TitleCust;

        foreach (ResCustRecord row in ResData.ExtrasData.ResCust)
        {
            string nationList = string.Empty;

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
                foreach (Nationality nRow in nationalityList)
                    nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.Code3, Equals(nRow.Code3, row.Nationality) ? "selected=\"selected\"" : "", nRow.Name);
            else
                foreach (DDLData nRow in nations)
                    nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.FieldData, Equals(nRow.FieldData, row.Nation.ToString()) ? "selected=\"selected\"" : "", nRow.TextData);

            string titleList = string.Empty;
            if (row.Title < 6)
            {
                foreach (TitleAgeRecord trow in title.Where(w => w.TitleNo < 6).Select(s => s).ToList<TitleAgeRecord>())
                    titleList += string.Format("<option value=\"{0}\" {1}>{2}</option>", trow.TitleNo, Equals(trow.TitleNo.ToString(), row.Title.ToString()) ? "selected=\"selected\"" : "", trow.Code);
            }
            else
            {
                foreach (TitleAgeRecord trow in title.Where(w => w.TitleNo > 5).Select(s => s).ToList<TitleAgeRecord>())
                    titleList += string.Format("<option value=\"{0}\" {1}>{2}</option>", trow.TitleNo, Equals(trow.TitleNo.ToString(), row.Title.ToString()) ? "selected=\"selected\"" : "", trow.Code);
            }

            cnt += 1;
            string _tmpl = tmpLoopSection;
            string tmpl = tmpLoopSection;

            exit = true;

            while (exit)
            {
                string valueTemp = string.Empty;

                int first = _tmpl.IndexOf("{\"");
                if (first > -1)
                {
                    lastPosition = first;
                    _tmpl = _tmpl.Remove(0, first + 2);
                    int last = _tmpl.IndexOf("\"}");
                    valueTemp = _tmpl.Substring(0, last);
                    _tmpl = _tmpl.Remove(0, last + 2);


                    string valueStr = string.Empty;
                    if (valueTemp.IndexOf('.') > -1)
                    {
                        if (Equals(valueTemp, "UserData.Ci.LCID"))
                            valueStr = UserData.Ci.LCID.ToString();
                    }
                    else
                    {
                        System.Reflection.PropertyInfo[] oProps = null;
                        oProps = row.GetType().GetProperties();
                        List<System.Reflection.PropertyInfo> _pi = (from q in oProps
                                                                    where q.Name == valueTemp
                                                                    select q).ToList<System.Reflection.PropertyInfo>();
                        if (_pi != null && _pi.Count() > 0)
                        {
                            System.Reflection.PropertyInfo pi = _pi[0];
                            object value = pi.GetValue(row, null) == null ? DBNull.Value : pi.GetValue(row, null);
                            valueStr = value.ToString();
                            if (valueTemp == "ppSalePrice")
                            {
                                decimal? ppSalePrice = Conversion.getDecimalOrNull(value);
                                valueStr = ppSalePrice.HasValue ? (ppSalePrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur) : "";
                            }
                        }
                    }

                    switch (valueTemp)
                    {
                        case "@import:ResCustInfo@":
                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", getResCustInfoImport(UserData, ResData, row.CustNo, cnt, string.Equals(row.Leader, "Y"), tmpCustInfo, ref errorMsg));
                            break;
                        case "PhoneCountryCode":
                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", UserData.PhoneMask != null ? UserData.PhoneMask.CountryCode : "");
                            break;
                        case "dateFormat":
                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", dateFormat);
                            break;
                        case "BirtDay":
                            string birthDate = row.Birtday.HasValue ? row.Birtday.Value.ToString(_dateFormat) : "";
                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", birthDate);
                            break;
                        case "ResDate":
                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToString(dateFormat).Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], '/') : "");
                            break;
                        case "Title":
                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", titleList);
                            break;
                        case "Nation":
                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", nationList);
                            break;
                        case "Nationality":
                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", nationList);
                            break;
                        case "LeaderCheck":
                            if (row.Title < 6)
                            {
                                if (Equals(row.Leader, "Y"))
                                    tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", "checked='checked'");
                                else tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", " ");
                            }
                            else tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", "disabled='disabled'");
                            break;
                        case "PassportCheck":
                            if ((row.HasPassport.HasValue && row.HasPassport.Value) || !row.HasPassport.HasValue)
                                tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", "checked='checked'");
                            else tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", " ");
                            break;
                        case "searchCustVisible":
                            if (seachCust)
                                tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", "display: none;");
                            else tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", " ");
                            break;
                        default:
                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", valueStr);
                            break;
                    }

                    tmpl = tmpl.Replace("{[]}", row.CustNo.ToString());
                    tmpl = tmpl.Replace("[]", cnt.ToString());
                }
                else exit = false;
            }

            loopSection += tmpl;
        }

        tmpTemplate = tmpTemplate.Replace("{loop" + tmpLoopSection + "loop}", loopSection);
        StringBuilder sb = new StringBuilder();
        StringBuilder custDiv = new StringBuilder();

        custDiv.Append("<fieldset><legend><label>»</label>");
        custDiv.AppendFormat("<span class=\"lblGridResCust\">{0}</span></legend>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "lblGridResCust"));
        sb.Append("<div id=\"resCustGrid\">");
        List<IntCountryListRecord> countryList = new TvBo.Common().getCountryList(UserData, ref errorMsg);

        sb.AppendFormat("<input id=\"countryList\" type=\"hidden\" value='{0}' />", Newtonsoft.Json.JsonConvert.SerializeObject(countryList).Replace('#', '9'));
        sb.AppendFormat("<input id=\"hfCustCount\" type=\"hidden\" value=\"{0}\" />", ResData.ResCust.Count);
        sb.AppendFormat("<input id=\"dateMask\" type=\"hidden\" value=\"{0}\" />", _dateMask);
        sb.AppendFormat("<input id=\"phone_Mask\" type=\"hidden\" value=\"{0}\" />", _phoneMask);

        sb.Append(tmpTemplate);
        sb.Append("</div>");

        bool custSearchBtn = sb.ToString().ToLower().IndexOf("search16.png") > 0;
        bool custInfoBtn = sb.ToString().ToLower().IndexOf("info.gif") > 0;
        bool custAddressBtn = sb.ToString().ToLower().IndexOf("address.gif") > 0;
        bool custSsrcBtn = sb.ToString().ToLower().IndexOf("ssrc.png") > 0;
        if (custSearchBtn || custInfoBtn || custAddressBtn || custSsrcBtn)
        {
            custDiv.Append("<div style=\"white-space: nowrap;\">");

            if (custSearchBtn)
            {
                custDiv.Append("<img alt=\"\" src=\"Images/search16.png\" height=\"18px\" />");
                custDiv.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustLabel"));
            }
            if (custInfoBtn)
            {
                if (custSearchBtn) custDiv.Append("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                custDiv.Append("<img alt=\"\" src=\"Images/info.gif\" height=\"18px\" />");
                custDiv.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("MakeReservation", "lblCustomerOtherInfo"));
            }
            if (custAddressBtn)
            {
                if (custInfoBtn || custSearchBtn) custDiv.Append("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                custDiv.Append("<img alt=\"\" src=\"Images/address.gif\" height=\"18px\" />");
                custDiv.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("MakeReservation", "lblCustomerAddress"));
            }
            if (custSsrcBtn)
            {
                if (custInfoBtn || custSearchBtn || custAddressBtn) custDiv.Append("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                custDiv.Append("<img alt=\"\" src=\"Images/Ssrc.png\" height=\"18px\" />");
                custDiv.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("MakeReservation", "lblSpecialServiceReqCode"));
            }
            custDiv.Append("</div><br />");
        }
        custDiv.Append(sb.ToString());
        custDiv.Append("<br /></fieldset>");
        return custDiv.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getResCustDiv()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }

        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        string tmplResCustEdit = getTemplate(UserData);

        if (!string.IsNullOrEmpty(tmplResCustEdit))
        {
            return getResCustDivTmp(UserData, ResData, tmplResCustEdit, ref errorMsg);
        }
        string dateMaskA = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ');
        string[] dateMaskB = dateMaskA.Split(UserData.Ci.DateTimeFormat.DateSeparator[0]);
        string _dateMask = string.Empty;
        string _dateFormat = string.Empty;
        foreach (string row in dateMaskB)
            if (!string.IsNullOrEmpty(row.ToString()))
                switch (row[0].ToString().ToLower())
                {
                    case "d": _dateMask += UserData.Ci.DateTimeFormat.DateSeparator[0] + "99"; _dateFormat += UserData.Ci.DateTimeFormat.DateSeparator[0] + "dd"; break;
                    case "m": _dateMask += UserData.Ci.DateTimeFormat.DateSeparator[0] + "99"; _dateFormat += UserData.Ci.DateTimeFormat.DateSeparator[0] + "MM"; break;
                    case "y": _dateMask += UserData.Ci.DateTimeFormat.DateSeparator[0] + "9999"; _dateFormat += UserData.Ci.DateTimeFormat.DateSeparator[0] + "yyyy"; break;
                    default: break;
                }
        _dateMask = _dateMask.Remove(0, 1);
        _dateFormat = _dateFormat.Remove(0, 1);
        string dateFormat = new TvBo.Common().getDateFormat(UserData.Ci).Replace('/', UserData.Ci.DateTimeFormat.DateSeparator[0]);
        string _phoneMask = string.Empty;
        if (UserData.PhoneMask != null)
            _phoneMask = UserData.PhoneMask.MobilPhoneMask != null ? UserData.PhoneMask.MobilPhoneMask.Replace('#', '9') : "";

        List<string> SrrcServiceTypeList = new Ssrcs().getSsrcServiceTypes(UserData.Market, ResData.ResMain.PLMarket, ref errorMsg);

        string _dateFormatRegion = new TvBo.Common().getDateFormatRegion(UserData.Ci);

        bool? showPIN = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "showPIN"));
        if (UserData.TvParams.TvParamReser.NeedPIN.HasValue && UserData.TvParams.TvParamReser.NeedPIN.Value)
            showPIN = true;
        bool? showPassportInfo = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ShowPassportInfo"));
        StringBuilder sb = new StringBuilder();
        StringBuilder custDiv = new StringBuilder();
        custDiv.Append("<fieldset><legend><label>»</label>");
        custDiv.AppendFormat("<span class=\"lblGridResCust\">{0}</span></legend>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "lblGridResCust"));

        sb.Append("<div id=\"resCustGrid\" class=\"resCustGridCss\">");
        sb.AppendFormat("<input id=\"hfCustCount\" type=\"hidden\" value=\"{0}\" />", ResData.ResCust.Count);
        sb.AppendFormat("<input id=\"dateMask\" type=\"hidden\" value=\"{0}\" />", _dateMask);
        sb.AppendFormat("<input id=\"phone_Mask\" type=\"hidden\" value=\"{0}\" />", _phoneMask);
        sb.Append("<div class=\"resCustGridCss\"><div class=\"header ui-widget-header ui-priority-secondary\">");

        sb.AppendFormat("<div class=\"SeqNoH\">{0}</div>", "&nbsp;");
        sb.AppendFormat("<div class=\"TitleH\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustTitle"));
        sb.AppendFormat("<div class=\"Surname{1}H\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustSurname"),
                !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
        sb.AppendFormat("<div class=\"Name{1}H\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustName"),
                !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
        sb.AppendFormat("<div class=\"BirthDayH\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustBirthDate").ToString() + "<br />" + _dateFormatRegion);
        sb.AppendFormat("<div class=\"AgeH\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustAge"));
        if (showPIN == null || (showPIN.HasValue && showPIN.Value))
            sb.AppendFormat("<div class=\"IdNoH\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPIN"));
        if (showPassportInfo == null || (showPassportInfo.HasValue && showPassportInfo.Value))
        {
            sb.AppendFormat("<div class=\"PassSerieH\">{0}</div>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPassSerie"));
            sb.AppendFormat("<div class=\"PassNoH\">{0}</div>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPassNo"));
            sb.AppendFormat("<div class=\"HasPassportH\">{0}</div>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPass"));
        }
        if (showPIN == null || (showPIN.HasValue && showPIN.Value))
        {
            sb.AppendFormat("<div class=\"PhoneH\">{0}</div>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPhone"));
        }
        else
        {
            string CountryCodeStr = string.Empty;
            if (UserData.PhoneMask != null)
                CountryCodeStr = !string.IsNullOrEmpty(UserData.PhoneMask.CountryCode) ? " (" + UserData.PhoneMask.CountryCode + ")" : "";
            sb.AppendFormat("<div class=\"PhoneLH\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPhone").ToString() + CountryCodeStr);
        }
        sb.AppendFormat("<div class=\"Nation{1}H\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustCitizenship"),
                !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
        sb.AppendFormat("<div class=\"LeaderH\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustLeader"));
        sb.AppendFormat("<div class=\"ViewEditH\">{0}</div>", "#");
        sb.Append("</div>");
        List<DDLData> nations = CacheObjects.getNations(UserData.Market);
        List<Nationality> nationalityList = CacheObjects.getNationality(UserData);
        List<TitleAgeRecord> title = ResData.TitleCust;
        Int16 cnt = 0;

        bool seachCust = new Agency().getRole(UserData, 504);

        foreach (TvBo.ResCustRecord row in ResData.ResCust)
        {
            bool Ssrcode = (from q1 in ResData.ResCon
                            join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                            join q3 in SrrcServiceTypeList on q2.ServiceType equals q3
                            where q1.CustNo == row.CustNo
                            select new { q2.RecID }).Count() > 0;

            string nationList = string.Empty;
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
                foreach (Nationality nRow in nationalityList)
                    nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.Code3, Equals(nRow.Code3, row.Nationality) ? "selected=\"selected\"" : "", nRow.Name);
            else
                foreach (DDLData nRow in nations)
                    nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.FieldData, Equals(nRow.FieldData, row.Nation.ToString()) ? "selected=\"selected\"" : "", nRow.TextData);
            string titleList = string.Empty;
            if (row.Title < 6)
            {
                foreach (TitleAgeRecord trow in title.Where(w => w.TitleNo < 6).Select(s => s).ToList<TitleAgeRecord>())
                    titleList += string.Format("<option value=\"{0}\" {1}>{2}</option>", trow.TitleNo, Equals(trow.TitleNo.ToString(), row.Title.ToString()) ? "selected=\"selected\"" : "", trow.Code);
            }
            else
            {
                foreach (TitleAgeRecord trow in title.Where(w => w.TitleNo > 5).Select(s => s).ToList<TitleAgeRecord>())
                    titleList += string.Format("<option value=\"{0}\" {1}>{2}</option>", trow.TitleNo, Equals(trow.TitleNo.ToString(), row.Title.ToString()) ? "selected=\"selected\"" : "", trow.Code);
            }
            cnt++;
            if (Convert.ToInt32(cnt / 2.0) == (cnt / 2.0))
                sb.Append("<div class=\"row ui-helper-clearfix\" style=\"background-color: #E2E2E2;\">");
            else sb.Append("<div class=\"row ui-helper-clearfix\">");
            sb.AppendFormat("<div class=\"SeqNo floatLeft\"><span id=\"iSeqNo{0}\">{1}</span></div>", cnt, cnt);
            sb.AppendFormat("<div class=\"Title floatLeft\"><select id=\"iTitle{0}\">{1}</select></div>", cnt, titleList);
            sb.AppendFormat("<div class=\"Surname{0} floatLeft\">", !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
            sb.AppendFormat("<input id=\"iSurname{0}\" type=\"text\" value=\"{1}\" maxlength=\"30\" style=\"width:96%; {2}\" onkeypress=\"return isNonUniCodeChar(event);\" />",
                cnt,
                row.Surname,
                new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.surnameWrittingRules));
            sb.AppendFormat("<input id=\"iSurnameL{0}\" type=\"hidden\" value=\"{1}\" /></div>",
                cnt,
                row.SurnameL);
            sb.AppendFormat("<div class=\"Name{0} floatLeft\">", !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
            sb.AppendFormat("<input id=\"iName{0}\" type=\"text\" value=\"{1}\" maxlength=\"30\" style=\"width:96%; {2}\" onkeypress=\"return isNonUniCodeChar(event);\" />",
                cnt,
                row.Name,
                new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.nameWrittingRules));
            sb.AppendFormat("<input id=\"iNameL{0}\" type=\"hidden\" value=\"{1}\" /></div>",
                cnt,
                row.NameL);

            string birthDate = row.Birtday.HasValue ? row.Birtday.Value.ToString(_dateFormat) : "";

            sb.AppendFormat("<div class=\"BirthDay floatLeft\"><input id=\"iBirthDay{0}\" type=\"text\" name=\"formatDate\" style=\"width:95%;\" onblur=\"return SetAge({1},'{2}', '{3}')\" value=\"{4}\"/></div>",
                                cnt,
                                cnt,
                                ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToString(dateFormat) : "",
                                dateFormat,
                                birthDate);
            sb.AppendFormat("<div class=\"Age floatLeft\"><input id=\"iAge{0}\" type=\"text\" value=\"{1}\" style=\"width:90%;\" disabled=\"disabled\" /></div>", cnt, row.Age);
            if (showPIN == null || (showPIN.HasValue && showPIN.Value))
                sb.AppendFormat("<div class=\"IdNo floatLeft\"><input id=\"iIDNo{0}\" type=\"text\" value=\"{1}\" style=\"width:93%;\" maxlength=\"20\" /></div>", cnt, row.IDNo);

            if (!showPassportInfo.HasValue || showPassportInfo.Value)
            {
                sb.AppendFormat("<div class=\"PassSerie floatLeft\"><input id=\"iPassSerie{0}\" type=\"text\" value=\"{1}\" style=\"width:90%;\" maxlength=\"5\" /></div>", cnt, row.PassSerie);
                sb.AppendFormat("<div class=\"PassNo floatLeft\"><input id=\"iPassNo{0}\" type=\"text\" value=\"{1}\" style=\"width:93%;\" maxlength=\"10\" /></div>", cnt, row.PassNo);
                sb.AppendFormat("<div class=\"HasPassport floatLeft\"><input id=\"iHasPassport{0}\" type=\"checkbox\" {1} {2} />", cnt, (row.HasPassport.HasValue ? row.HasPassport.Value : true) ? "checked=\"checked\"" : "", row.Title < 6 ? "disabled=\"disabled\"" : "");
                sb.AppendFormat("<input id=\"iPassIssueDate{0}\" type=\"hidden\" value=\"{1}\"/>", cnt,
                                row.PassIssueDate.HasValue ? row.PassIssueDate.Value.ToString(_dateFormat) : "");
                sb.AppendFormat("<input id=\"iPassExpDate{0}\" type=\"hidden\" value=\"{1}\"/>", cnt,
                                row.PassExpDate.HasValue ? row.PassExpDate.Value.ToString(_dateFormat) : "");
                sb.AppendFormat("<input id=\"iPassGiven{0}\" type=\"hidden\" value=\"{1}\" maxlength=\"30\" />", cnt,
                                row.PassGiven);
                sb.Append("</div>");
            }
            else
            {
                sb.Append("<div style=\"display: none; visibility: hidden;\">");
                sb.AppendFormat("<input id=\"iPassSerie{0}\" type=\"hidden\" value=\"{1}\"  />", cnt, row.PassSerie);
                sb.AppendFormat("<input id=\"iPassNo{0}\" type=\"hidden\" value=\"{1}\" />", cnt, row.PassNo);
                sb.AppendFormat("<input id=\"iHasPassport{0}\" type=\"checkbox\" {1} />", cnt, cnt, (row.HasPassport.HasValue ? row.HasPassport.Value : true) ? "checked=\"checked\"" : "");
                sb.AppendFormat("<input id=\"iPassIssueDate{0}\" type=\"hidden\" value=\"{1}\"/>", cnt, "");
                sb.AppendFormat("<input id=\"iPassExpDate{0}\" type=\"hidden\" value=\"{1}\"/>", cnt, "");
                sb.AppendFormat("<input id=\"iPassGiven{0}\" type=\"hidden\" value=\"{1}\" maxlength=\"30\" />", cnt, row.PassGiven);
                sb.Append("</div>");
            }

            if (showPIN == null || (showPIN.HasValue && showPIN.Value))
                sb.AppendFormat("<div class=\"Phone floatLeft\"><input id=\"iPhone{0}\" type=\"text\" value=\"{1}\" style=\"width:90%;\" maxlength=\"15\" onkeypress=\"return isPhoneChar(event);\" class=\"mobPhone\" /></div>",
                                    cnt,
                                    string.IsNullOrEmpty(row.Phone) ? row.Phone : strFunc.Trim(row.Phone, ' '));
            else sb.AppendFormat("<div class=\"PhoneL floatLeft\"><input id=\"iPhone{0}\" type=\"text\" value=\"{1}\" style=\"width:98%;\" maxlength=\"15\" onkeypress=\"return isPhoneChar(event);\" class=\"mobPhone\" /></div>",
                                    cnt,
                                    string.IsNullOrEmpty(row.Phone) ? row.Phone : strFunc.Trim(row.Phone, ' '));

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
                sb.AppendFormat("<div class=\"Nation{2} floatLeft\"><select id=\"iNationality{0}\">{1}</select></div>", cnt, nationList, !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
            else
                sb.AppendFormat("<div class=\"Nation{2} floatLeft\"><select id=\"iNation{0}\">{1}</select></div>", cnt, nationList, !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");

            if (row.Title < 6)
                sb.AppendFormat("<div class=\"Leader floatLeft\"><input id=\"iLeader{0}\" type=\"radio\" name=\"Leader\" value=\"{1}\" {2} /></div>", cnt, cnt, Equals(row.Leader, "Y") ? "checked=\"checked\"" : "");
            else sb.Append("<div class=\"Leader floatLeft\">&nbsp;</div>");
            string searchStr = string.Format("<img alt=\"{0}\" src=\"Images/search16.png\" onclick=\"searchCust({1})\" />", HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustLabel"), cnt);
            string infoStr = string.Format("<img alt=\"{0}\" src=\"Images/info.gif\" onclick=\"showResCustInfo({1})\" />", HttpContext.GetGlobalResourceObject("MakeReservation", "lblCustomerOtherInfo"), row.CustNo);
            string addrInfoStr = string.Format("<img alt=\"{0}\" src=\"Images/address.gif\" onclick=\"showCustAddress({1})\" />", HttpContext.GetGlobalResourceObject("MakeReservation", "lblCustomerAddress"), row.CustNo);
            string ssrcStr = string.Format("<img alt=\"{0}\" src=\"Images/Srrc.png\" onclick=\"showSSRC({1})\" height=\"18\" />", HttpContext.GetGlobalResourceObject("MakeReservation", "lblSpecialServiceReqCode"), row.CustNo);
            string ccard = string.Format("<img id=\"cCardBtn_{1}\" name=\"cCardBtn\" title=\"{0}\" alt=\"{0}\" src=\"Images/CCard.png\" onclick=\"showCCard({1})\" height=\"18\" />",
                                    HttpContext.GetGlobalResourceObject("MakeReservation", "lblCCardApp"),
                                    row.CustNo);

            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                sb.AppendFormat("<div class=\"ViewEdit floatLeft\">{0}&nbsp;&nbsp;{1}&nbsp;&nbsp;{2}&nbsp;&nbsp;{3}</div>",
                    Equals(UserData.Market, "SWEMAR") ? "" : infoStr,
                addrInfoStr,
                Ssrcode && !Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) ? ssrcStr : "",
                seachCust ? searchStr : "");
            else
                sb.AppendFormat("<div class=\"ViewEdit floatLeft\">{0}&nbsp;{1}&nbsp;{2}&nbsp;{3}&nbsp;{4}</div>",
                    infoStr,
                    addrInfoStr,
                    Ssrcode && !Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) ? ssrcStr : "",
                    seachCust ? searchStr : "",
                    (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && row.Title < 6) ? ccard : string.Empty);
            decimal? PPPrice;
            if (row.ppPasPayable.HasValue)
            {
                PPPrice = row.ppPasPayable.Value;
            }
            else
            {
                List<ResCustPriceRecord> ppPrice = ResData.ResCustPrice.Where(w => w.CustNo == row.CustNo).Select(s => s).ToList<ResCustPriceRecord>();
                PPPrice = ppPrice.Sum(s => s.SalePrice);
            }
            string ppPriceStr = PPPrice.HasValue ? PPPrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "&nbsp;";

            sb.AppendFormat("<div class=\"Price\"><h3>{0} : {1}</h3></div>", HttpContext.GetGlobalResourceObject("MakeReservation", "lblPerPerson"), ppPriceStr);
            sb.Append("</div>");
        }
        sb.Append("</div>");
        sb.Append("</div>");
        bool custSearchBtn = sb.ToString().IndexOf("search16.png") > 0;
        bool custInfoBtn = sb.ToString().IndexOf("info.gif") > 0;
        bool custAddressBtn = sb.ToString().IndexOf("address.gif") > 0;
        bool custSsrcBtn = sb.ToString().IndexOf("Ssrc.png") > 0;
        if (custSearchBtn || custInfoBtn || custAddressBtn || custSsrcBtn)
        {
            custDiv.Append("<div style=\"white-space: nowrap;\">");

            if (custSearchBtn)
            {
                custDiv.Append("<img alt=\"\" src=\"Images/search16.png\" />");
                custDiv.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustLabel"));
            }
            if (custInfoBtn)
            {
                if (custSearchBtn) custDiv.Append("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                custDiv.Append("<img alt=\"\" src=\"Images/info.gif\" />");
                custDiv.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("MakeReservation", "lblCustomerOtherInfo"));
            }
            if (custAddressBtn)
            {
                if (custInfoBtn || custSearchBtn) custDiv.Append("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                custDiv.Append("<img alt=\"\" src=\"Images/address.gif\" />");
                custDiv.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("MakeReservation", "lblCustomerAddress"));
            }
            if (custSsrcBtn)
            {
                if (custInfoBtn || custSearchBtn || custAddressBtn) custDiv.Append("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                custDiv.Append("<img alt=\"\" src=\"Images/Ssrc.png\" />");
                custDiv.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("MakeReservation", "lblSpecialServiceReqCode"));
            }
            custDiv.Append("</div><br />");
        }
        custDiv.Append(sb.ToString());
        custDiv.Append("<br /></fieldset>");

        return custDiv.ToString();

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getResServiceDiv()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        List<ResServiceRecord> services = (from q in ResData.ResService
                                           orderby q.BegDate, q.SeqNo
                                           select q).ToList<ResServiceRecord>();
        ResServiceRecord firstService = services.FirstOrDefault();
        ResServiceRecord lastService = services.LastOrDefault();
        string direction = string.Empty;
        bool isRT = false;
        if (Equals(firstService, lastService))
        {
            TransferRecord transfer = new Transfers().getTransfer(UserData.Market, firstService.Service, ref errorMsg);
            if (transfer != null)
            {
                direction = transfer.Direction;
                if (string.Equals(direction, "R"))
                    isRT = true;
                else isRT = ResData.ResService.Where(w => string.Equals(w.ServiceType, "TRANSFER")).Count() > 1;
            }
        }
        List<string> days = new List<string>();
        int year = DateTime.Today.Year;
        int month = DateTime.Today.Month - 1;
        for (int i = 0; i < 24; i++)
        {
            if (month == 12)
            {
                month = 1;
                year++;
            }
            else month++;
            DateTime monthFirstDay = new DateTime(year, month, 1);

            int lastDayOfMonth = DateTime.DaysInMonth(year, month);
            string monthStr = monthFirstDay.ToString("MMMM yyy") + "|" + lastDayOfMonth.ToString() + "|" + year.ToString() + "_" + month.ToString();
            days.Add(monthStr);
        }
        List<FlightDayRecord> flights = string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_ZemExpert)
            ?
            new Flights().getRouteFlightsForOnlyFlightDays(UserData, UserData.Market, firstService.BegDate, null, new Locations().getLocationForCity(firstService.DepLocation.Value, ref errorMsg), ref errorMsg)
            :
            new Flights().getRouteFlights(UserData, UserData.Market, firstService.BegDate, null, new Locations().getLocationForCity(firstService.DepLocation.Value, ref errorMsg), string.Empty, ref errorMsg);

        var depFlights = from q in flights
                         select new { q.FlightNo };
        flights = isRT ? (string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_ZemExpert)
                            ?
                            new Flights().getRouteFlightsForOnlyFlightDays(UserData, UserData.Market, lastService.EndDate, new Locations().getLocationForCity(firstService.DepLocation.Value, ref errorMsg), null, ref errorMsg)
                            :
                            new Flights().getRouteFlights(UserData, UserData.Market, lastService.EndDate, new Locations().getLocationForCity(firstService.DepLocation.Value, ref errorMsg), null, string.Empty, ref errorMsg)
                         )
                       :
                         (string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_ZemExpert)
                            ?
                            new Flights().getRouteFlightsForOnlyFlightDays(UserData, UserData.Market, lastService.BegDate, new Locations().getLocationForCity(lastService.ArrLocation.Value, ref errorMsg), null, ref errorMsg)
                            :
                            new Flights().getRouteFlights(UserData, UserData.Market, lastService.BegDate, new Locations().getLocationForCity(lastService.ArrLocation.Value, ref errorMsg), null, string.Empty, ref errorMsg)
                         );
        var arrFlights = from q in flights
                         select new { q.FlightNo };
        List<CodeName> hotels = new Hotels().getTrfLocationHotelCodeName(UserData.Market, !isRT && string.Equals(direction, "B") ? firstService.DepLocation.Value : firstService.ArrLocation.Value, null, ref errorMsg);
        List<CodeName> arrivalHotels = new OnlyTransfers().getCityHotels(UserData, new Locations().getLocationForCity(firstService.DepLocation.Value, ref errorMsg), useLocalName, ref errorMsg);
        return new
        {
            showReturn = services.Count > 1,
            departureDescrition = useLocalName ? firstService.ServiceNameL : firstService.ServiceName,
            departureDestination = useLocalName ? (firstService.DepLocationNameL + " -> " + firstService.ArrLocationNameL) : (firstService.DepLocationName + " -> " + firstService.ArrLocationName),
            departurePrice = firstService.SalePrice.HasValue ? firstService.SalePrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "",
            departureDestinationList = hotels,
            departureDestinationLoc = new { Code = firstService.ArrLocation.ToString(), Name = useLocalName ? firstService.ArrLocationNameL : firstService.ArrLocationName },
            departureFlights = depFlights,
            arrivalDescrition = isRT ? "" : useLocalName ? lastService.ServiceNameL : lastService.ServiceName,
            arrivalDestination = isRT ? (useLocalName ? (lastService.ArrLocationNameL + " -> " + lastService.DepLocationNameL) : (lastService.ArrLocationName + " -> " + lastService.DepLocationName)) : (useLocalName ? (lastService.DepLocationNameL + " -> " + lastService.ArrLocationNameL) : (lastService.DepLocationName + " -> " + lastService.ArrLocationName)),
            arrivalPrice = isRT ? "" : lastService.SalePrice.HasValue ? lastService.SalePrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "",
            arrivalFlights = arrFlights,
            arrivalDestinationList = arrivalHotels,
            arrivalDestinationLoc = new { Code = (isRT ? firstService.ArrLocation.ToString() : lastService.DepLocation.ToString()), Name = isRT ? (useLocalName ? firstService.ArrLocationNameL : firstService.ArrLocationName) : (useLocalName ? lastService.DepLocationNameL : lastService.DepLocationName) },
            calendar = days,
            selectYearMonth = firstService.BegDate.Value.Year.ToString() + "_" + firstService.BegDate.Value.Month.ToString(),
            selectDay = firstService.BegDate.Value.Day,
            selectYearMonthReturn = isRT ? (firstService.EndDate.Value.Year.ToString() + "_" + firstService.EndDate.Value.Month.ToString()) : (lastService.BegDate.Value.Year.ToString() + "_" + lastService.BegDate.Value.Month.ToString()),
            selectDayReturn = isRT ? firstService.EndDate.Value.Day : lastService.BegDate.Value.Day,
            isRT = isRT
        };
    }

    [WebMethod(EnableSession = true)]
    public static string setCustomers(List<resCustjSonData> data)
    {
        if (HttpContext.Current.Session["UserData"] == null || data == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);

        try
        {
            resCustjSonData cust = new resCustjSonData();
            for (int i = 0; i < data.Count; i++)
            {
                cust = data[i];
                ResCustRecord _cust = ResData.ResCust.Find(f => f.SeqNo == cust.SeqNo);
                TitleRecord _title = ResData.Title.Find(f => f.TitleNo == cust.Title);
                _cust.Title = cust.Title;
                _cust.TitleStr = _title != null ? _title.Code : "";
                _cust.Surname = cust.Surname;
                _cust.Name = cust.Name;
                _cust.Birtday = Conversion.getDateTimeOrNull(cust.Birtday);
                _cust.Age = TvBo.Common.getAge(_cust.Birtday, UserData.TvParams.TvParamReser.AgeCalcType > 2 ? ResData.ResMain.EndDate : ResData.ResMain.BegDate);
                _cust.IDNo = cust.IDNo;
                _cust.PassSerie = cust.PassSerie;
                _cust.PassNo = cust.PassNo;
                _cust.PassExpDate = Conversion.getDateTimeOrNull(cust.PassExpDate);
                _cust.PassIssueDate = Conversion.getDateTimeOrNull(cust.PassIssueDate);
                _cust.PassGiven = cust.PassGiven;
                _cust.Phone = cust.Phone;
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
                {
                    if (!string.IsNullOrEmpty(cust.Nationality))
                        _cust.Nationality = cust.Nationality;
                }
                else
                {
                    if (cust.Nation.HasValue)
                        _cust.Nation = cust.Nation;
                }
                _cust.HasPassport = cust.Passport;
                _cust.Leader = cust.Leader.HasValue ? (cust.Leader.Value ? "Y" : "N") : "N";
            }

            HttpContext.Current.Session["ResData"] = ResData;
            return "OK";
        }
        catch
        {
            return "NO";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string setResCustInfo(string data)
    {
        if (HttpContext.Current.Session["UserData"] == null || string.IsNullOrEmpty(data)) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        string _data = data.Replace('<', '{').Replace('>', '}').Replace('|', '"');
        TvBo.ResCustInfojSon resCustInfojSon = Newtonsoft.Json.JsonConvert.DeserializeObject<TvBo.ResCustInfojSon>(_data);

        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == Conversion.getInt32OrNull(resCustInfojSon.CustNo));
        List<ResCustInfoRecord> _ResCustInfo = ResData.ResCustInfo;

        ResCustInfoRecord resCustInfoRec = new ResCustInfoRecord();
        if (ResData.ResCustInfo.Count > 0 && _ResCustInfo.Find(f => f.CustNo == resCust.CustNo) != null)
        {
            resCustInfoRec = _ResCustInfo.Find(f => f.CustNo == resCust.CustNo);
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                resCustInfoRec.AddrHomeTel = string.IsNullOrEmpty(resCust.Phone) ? strFunc.Trim(resCust.Phone, ' ') : resCustInfoRec.AddrHomeTel;
            else resCustInfoRec.MobTel = string.IsNullOrEmpty(resCust.Phone) ? strFunc.Trim(resCust.Phone, ' ') : resCustInfoRec.MobTel;
        }
        else
        {
            resCustInfoRec.CustNo = resCust.CustNo;
            resCustInfoRec.CTitle = resCust.Title;
            resCustInfoRec.RecID = _ResCustInfo.Count > 0 ? _ResCustInfo.LastOrDefault().RecID + 1 : 1;
            resCustInfoRec.RecordID = resCustInfoRec.RecID;
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                resCustInfoRec.AddrHomeTel = string.IsNullOrEmpty(resCust.Phone) ? strFunc.Trim(resCust.Phone, ' ') : resCustInfoRec.AddrHomeTel;
            else resCustInfoRec.MobTel = string.IsNullOrEmpty(resCust.Phone) ? strFunc.Trim(resCust.Phone, ' ') : resCustInfoRec.MobTel;
            IntCountryListRecord country = new TvBo.Common().getCountryIntCode(UserData.Market, UserData.Country, ref errorMsg);
            resCustInfoRec.AddrHomeCountryCode = country != null ? country.IntCode : string.Empty;
            resCustInfoRec.MemTable = false;

            _ResCustInfo.Add(resCustInfoRec);
        }
        resCustInfoRec.CTitle = Conversion.getInt16OrNull(resCustInfojSon.CTitle);
        resCustInfoRec.CName = resCustInfojSon.CName;
        resCustInfoRec.CSurName = resCustInfojSon.CSurName;
        resCustInfoRec.MobTel = resCustInfojSon.MobTel.Trim(' ');
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            resCust.Phone = string.IsNullOrEmpty(resCustInfojSon.AddrHomeTel) ? resCust.Phone : resCustInfojSon.AddrHomeTel;
        else resCust.Phone = string.IsNullOrEmpty(resCustInfojSon.MobTel) ? resCust.Phone : resCustInfojSon.MobTel;
        resCustInfoRec.ContactAddr = resCustInfojSon.ContactAddr;
        resCustInfoRec.InvoiceAddr = resCustInfojSon.InvoiceAddr;
        resCustInfoRec.AddrHome = resCustInfojSon.AddrHome;
        resCustInfoRec.AddrHomeCity = resCustInfojSon.AddrHomeCity;
        resCustInfoRec.AddrHomeCountry = resCustInfojSon.AddrHomeCountry;
        resCustInfoRec.AddrHomeCountryCode = resCustInfojSon.AddrHomeCountryCode;
        resCustInfoRec.AddrHomeZip = resCustInfojSon.AddrHomeZip;
        resCustInfoRec.AddrHomeTel = strFunc.Trim(resCustInfojSon.AddrHomeTel, ' ');
        resCustInfoRec.AddrHomeFax = strFunc.Trim(resCustInfojSon.AddrHomeFax, ' ');
        resCustInfoRec.AddrHomeEmail = resCustInfojSon.AddrHomeEmail;
        resCustInfoRec.HomeTaxOffice = resCustInfojSon.HomeTaxOffice;
        resCustInfoRec.HomeTaxAccNo = resCustInfojSon.HomeTaxAccNo;
        resCustInfoRec.WorkFirmName = resCustInfojSon.WorkFirmName;
        resCustInfoRec.AddrWork = resCustInfojSon.AddrWork;
        resCustInfoRec.AddrWorkCity = resCustInfojSon.AddrWorkCity;
        resCustInfoRec.AddrWorkCountry = resCustInfojSon.AddrWorkCountry;
        resCustInfoRec.AddrWorkCountryCode = resCustInfojSon.AddrWorkCountryCode;
        resCustInfoRec.AddrWorkZip = resCustInfojSon.AddrWorkZip;
        resCustInfoRec.AddrWorkTel = strFunc.Trim(resCustInfojSon.AddrWorkTel, ' ');
        resCustInfoRec.AddrWorkFax = strFunc.Trim(resCustInfojSon.AddrWorkFax, ' ');
        resCustInfoRec.AddrWorkEMail = resCustInfojSon.AddrWorkEMail;
        resCustInfoRec.WorkTaxOffice = resCustInfojSon.WorkTaxOffice;
        resCustInfoRec.WorkTaxAccNo = resCustInfojSon.WorkTaxAccNo;
        resCustInfoRec.Bank = resCustInfojSon.Bank;
        resCustInfoRec.BankAccNo = resCustInfojSon.BankAccNo;
        resCustInfoRec.BankIBAN = resCustInfojSon.BankIBAN;

        HttpContext.Current.Session["ResData"] = ResData;

        return "OK";
    }

    [WebMethod(EnableSession = true)]
    public static string setResCustomers(List<resCustjSonData> data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        List<ResCustRecord> resCust = ResData.ResCust;
        string nameRules = new UICommon().nameWrittingRuleValue(UserData, NameWrittingRuleTypes.nameWrittingRules);
        string surnameRules = new UICommon().nameWrittingRuleValue(UserData, NameWrittingRuleTypes.surnameWrittingRules);

        resCustjSonData cust = new resCustjSonData();
        for (int i = 0; i < data.Count; i++)
        {
            cust = data[i];
            ResCustRecord _cust = resCust.Find(f => f.SeqNo == cust.SeqNo);
            TitleRecord _title = ResData.Title.Find(f => f.TitleNo == cust.Title);
            _cust.Title = cust.Title;
            _cust.TitleStr = _title != null ? _title.Code : "";
            _cust.Surname = cust.Surname;
            if (!string.IsNullOrEmpty(_cust.Surname))
            {
                if (surnameRules == "uppercase")
                    _cust.Surname = _cust.Surname.ToUpper();
                else if (surnameRules == "capitalize")
                {
                    _cust.Surname = _cust.Surname.ToLower();
                    string FirstChar = _cust.Surname[0].ToString();
                    _cust.Surname = _cust.Surname.Remove(0, 1);
                    _cust.Surname = FirstChar + _cust.Surname;
                }
            }
            _cust.SurnameL = cust.SurnameL;
            _cust.Name = cust.Name;
            if (!string.IsNullOrEmpty(_cust.Name))
            {
                if (nameRules == "uppercase")
                    _cust.Name = _cust.Name.ToUpper();
                else if (nameRules == "capitalize")
                {
                    _cust.Name = _cust.Name.ToLower();
                    string FirstChar = _cust.Name[0].ToString();
                    _cust.Name = _cust.Name.Remove(0, 1);
                    _cust.Name = FirstChar + _cust.Name;
                }
            }
            _cust.NameL = cust.NameL;
            _cust.Birtday = Conversion.convertDateTime(cust.Birtday.Replace("/", UserData.Ci.DateTimeFormat.DateSeparator).Replace(".", UserData.Ci.DateTimeFormat.DateSeparator).Replace("-", UserData.Ci.DateTimeFormat.DateSeparator),
                                                        strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' '));
            _cust.Age = Conversion.getInt16OrNull(cust.Age);
            _cust.IDNo = cust.IDNo;
            _cust.PassSerie = cust.PassSerie;
            _cust.PassNo = cust.PassNo;
            _cust.PassIssueDate = Conversion.getDateTimeOrNull(cust.PassIssueDate);
            _cust.PassExpDate = Conversion.getDateTimeOrNull(cust.PassExpDate);
            _cust.PassGiven = cust.PassGiven;
            _cust.Phone = cust.Phone;
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
            {
                if (!string.IsNullOrEmpty(cust.Nationality))
                    _cust.Nationality = cust.Nationality;
            }
            else
            {
                if (cust.Nation.HasValue)
                    _cust.Nation = cust.Nation;
            }
            _cust.HasPassport = cust.Passport;
            _cust.Leader = cust.Leader.HasValue ? (cust.Leader.Value ? "Y" : "N") : "N";
        }

        HttpContext.Current.Session["ResData"] = ResData;

        return "OK";
    }

    [WebMethod(EnableSession = true)]
    public static string searchCustomer(string refNo, string SeqNo, string Surname, string Name, string BirthDate)
    {        
        return "";
    }

    [WebMethod(EnableSession = true)]
    public static string copyCustomer(string CustNo, List<resCustjSonData> Custs)
    {
        int? oldCustNo = Conversion.getInt32OrNull(CustNo.Split(';')[0]);
        int? newCustNo = Conversion.getInt32OrNull(CustNo.Split(';')[1]);

        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (setCustomers(Custs) != "OK") return "";

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);
        List<ResCustRecord> resCust = ResData.ResCust;
        List<ResCustInfoRecord> resCustInfo = ResData.ResCustInfo;

        ResCustRecord oldCust = new ResTables().getResCustRecord(UserData, oldCustNo, ref errorMsg);
        ResCustInfoRecord oldCustInfo = new ResTables().getResCustInfoRecord(oldCustNo, ref errorMsg);

        ResCustRecord newCust = resCust.Find(f => f.SeqNo == newCustNo);
        ResCustInfoRecord newCustInfo = resCustInfo.Find(f => f.CustNo == (newCust != null ? newCust.CustNo : -1));
        if (newCust != null)
        {
            Int16? seqNo = newCust.SeqNo;
            int custNo = newCust.CustNo;
            int? custNoT = newCust.CustNoT;

            newCust.Title = oldCust.Title;
            newCust.TitleStr = oldCust.TitleStr;
            newCust.Surname = oldCust.Surname;
            newCust.SurnameL = oldCust.SurnameL;
            newCust.Name = oldCust.Name;
            newCust.NameL = oldCust.NameL;
            newCust.Birtday = oldCust.Birtday;
            newCust.Age = oldCust.Age;
            newCust.HasPassport = oldCust.HasPassport;
            newCust.IDNo = oldCust.IDNo;
            newCust.Nation = oldCust.Nation;
            newCust.NationName = oldCust.NationName;
            newCust.NationNameL = oldCust.NationNameL;
            newCust.Nation = oldCust.Nation;
            newCust.NationName = oldCust.NationName;
            newCust.PassExpDate = oldCust.PassExpDate;
            newCust.PassGiven = oldCust.PassGiven;
            newCust.PassIssueDate = oldCust.PassIssueDate;
            newCust.PassNo = oldCust.PassNo;
            newCust.PassSerie = oldCust.PassSerie;
            newCust.Phone = strFunc.Trim(oldCust.Phone, ' ');

            if (newCustInfo != null && oldCustInfo != null)
            {
                newCustInfo.AddrHome = oldCustInfo.AddrHome;
                newCustInfo.AddrHomeCity = oldCustInfo.AddrHomeCity;
                newCustInfo.AddrHomeCountry = oldCustInfo.AddrHomeCountry;
                newCustInfo.AddrHomeCountryCode = oldCustInfo.AddrHomeCountryCode;
                newCustInfo.AddrHomeEmail = oldCustInfo.AddrHomeEmail;
                newCustInfo.AddrHomeFax = strFunc.Trim(oldCustInfo.AddrHomeFax, ' ');
                newCustInfo.AddrHomeTel = strFunc.Trim(oldCustInfo.AddrHomeTel, ' ');
                newCustInfo.AddrHomeZip = oldCustInfo.AddrHomeZip;
                newCustInfo.AddrWork = oldCustInfo.AddrWork;
                newCustInfo.AddrWorkCity = oldCustInfo.AddrWorkCity;
                newCustInfo.AddrWorkCountry = oldCustInfo.AddrWorkCountry;
                newCustInfo.AddrWorkCountryCode = oldCustInfo.AddrWorkCountryCode;
                newCustInfo.AddrWorkEMail = oldCustInfo.AddrWorkEMail;
                newCustInfo.AddrWorkFax = strFunc.Trim(oldCustInfo.AddrWorkFax, ' ');
                newCustInfo.AddrWorkTel = strFunc.Trim(oldCustInfo.AddrWorkTel, ' ');
                newCustInfo.AddrWorkZip = oldCustInfo.AddrWorkZip;
                newCustInfo.Bank = oldCustInfo.Bank;
                newCustInfo.BankAccNo = oldCustInfo.BankAccNo;
                newCustInfo.BankIBAN = oldCustInfo.BankIBAN;
                newCustInfo.CName = oldCustInfo.CName;
                newCustInfo.ContactAddr = oldCustInfo.ContactAddr;
                newCustInfo.CSurName = oldCustInfo.CSurName;
                newCustInfo.CTitle = oldCustInfo.CTitle;
                newCustInfo.CTitleName = oldCustInfo.CTitleName;
                newCustInfo.HomeTaxAccNo = oldCustInfo.HomeTaxAccNo;
                newCustInfo.HomeTaxOffice = oldCustInfo.HomeTaxOffice;
                newCustInfo.InvoiceAddr = oldCustInfo.InvoiceAddr;
                newCustInfo.Jobs = oldCustInfo.Jobs;
                newCustInfo.MobTel = strFunc.Trim(oldCustInfo.MobTel, ' ');
                newCust.Phone = newCustInfo.MobTel;
                newCustInfo.Note = oldCustInfo.Note;
                newCustInfo.WorkFirmName = oldCustInfo.WorkFirmName;
                newCustInfo.WorkTaxAccNo = oldCustInfo.WorkTaxAccNo;
                newCustInfo.WorkTaxOffice = oldCustInfo.WorkTaxOffice;
            }
            else
            {
                if (oldCustInfo != null)
                {
                    newCustInfo = new ResCustInfoRecord();
                    newCustInfo = oldCustInfo;
                    newCustInfo.CustNo = custNo;
                    ResData.ResCustInfo.Add(newCustInfo);
                }
            }
            if (ResData.ExtrasData != null)
            {
                ResData.ExtrasData.ResCust = ResData.ResCust;
                ResData.ExtrasData.ResCustInfo = ResData.ResCustInfo;
            }
            HttpContext.Current.Session["ResData"] = ResData;
        }
        return "OK";
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<ReservastionSaveErrorRecord> saveReservation(List<resCustjSonData> CustData,
                string DepMonthYear, string DepDay, string DepFlightNo, string DepHour, string DepMinute, string DepPickupNote, string DepArrival,
                string RetMonthYear, string RetDay, string RetFlightNo, string RetHour, string RetMinute, string RetPickupNote, string RetDeparture,
                string PickupType, string InvoiceTo, string Direction, string DepRegion, string RetRegion)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        setCustomers(CustData);

        List<ReservastionSaveErrorRecord> returnData = new List<ReservastionSaveErrorRecord>();
        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        ResMainRecord resMain = ResData.ResMain;

        bool saveAsDraft = false;
        bool? setOptionTime = null;


        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr))
        {
            if (Conversion.getByteOrNull(InvoiceTo).HasValue)
                resMain.InvoiceTo = Conversion.getByteOrNull(InvoiceTo).Value;
        }

        List<ResServiceRecord> services = ResData.ResService;
        ResServiceRecord firstService = services.OrderBy(o => o.BegDate).First();
        TransferRecord firstTransfer = new Transfers().getTransfer(UserData.Market, firstService.Service, ref errorMsg);
        ResServiceRecord lastService = services.OrderBy(o => o.BegDate).Last();
        TransferRecord lastTransfer = new Transfers().getTransfer(UserData.Market, lastService.Service, ref errorMsg);

        DateTime? begDate = firstService.BegDate;
        DateTime? endDate = lastService.EndDate;

        if (!string.IsNullOrEmpty(DepMonthYear) && !string.IsNullOrEmpty(DepDay))
        {
            int depYear = (DepMonthYear.Split('_').Length > 1) ? int.Parse(DepMonthYear.Split('_')[0]) : firstService.BegDate.Value.Year;
            int depMonth = (DepMonthYear.Split('_').Length > 1) ? int.Parse(DepMonthYear.Split('_')[1]) : firstService.BegDate.Value.Month;
            int depDay = int.Parse(DepDay);
            begDate = new DateTime(depYear, depMonth, depDay);
        }
        if (!string.IsNullOrEmpty(RetMonthYear) && !string.IsNullOrEmpty(RetDay))
        {
            int endYear = (RetMonthYear.Split('_').Length > 1) ? int.Parse(RetMonthYear.Split('_')[0]) : firstService.EndDate.Value.Year;
            int endMonth = (RetMonthYear.Split('_').Length > 1) ? int.Parse(RetMonthYear.Split('_')[1]) : firstService.EndDate.Value.Month;
            int endDay = int.Parse(RetDay);
            endDate = new DateTime(endYear, endMonth, endDay);
        }

        int? depHour = Conversion.getInt32OrNull(DepHour != "undefined" ? DepHour : "");
        int? depMin = Conversion.getInt32OrNull(DepMinute != "undefined" ? DepMinute : "");
        DateTime? depPickupTime = null;
        if (depHour.HasValue && depHour.HasValue)
            depPickupTime = begDate.Value.AddHours(depHour.Value).AddMinutes(depMin.Value);

        int? retHour = Conversion.getInt32OrNull(RetHour != "undefined" ? RetHour : "");
        int? retMin = Conversion.getInt32OrNull(RetMinute != "undefined" ? RetMinute : "");
        DateTime? retPickupTime = null;
        if (retHour.HasValue && retHour.HasValue)
            retPickupTime = endDate.Value.AddHours(retHour.Value).AddMinutes(retMin.Value);

        if (services.Count() > 1)
        {
            #region Two way
            ResServiceRecord transferFirst = services.Find(f => f.RecID == firstService.RecID);
            ResServiceRecord transferLast = services.Find(f => f.RecID == lastService.RecID);
            HotelRecord hotel = null;
            Location location = null;

            transferFirst.BegDate = begDate;
            transferFirst.EndDate = begDate;
            transferFirst.TrfDT = 0;
            transferFirst.TrfDTName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceFlight").ToString();
            if (DepRegion == "1")
            {
                transferFirst.TrfAT = 1;
                transferFirst.TrfATName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceHotel").ToString();
            }
            else
            {
                transferFirst.TrfAT = 2;
                transferFirst.TrfATName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceOther").ToString();
            }
            transferFirst.TrfDep = DepFlightNo;
            transferFirst.TrfDepName = DepFlightNo;
            transferFirst.TrfDepNameL = DepFlightNo;
            transferFirst.TrfArr = DepArrival;
            if (DepRegion == "1")
            {
                hotel = new Hotels().getHotelDetail(UserData, transferFirst.TrfArr, ref errorMsg);
                if (hotel != null)
                {
                    transferFirst.TrfArrName = hotel.Name;
                    transferFirst.TrfArrNameL = hotel.LocalName;
                }
            }
            else
            {
                location = new Locations().getLocation(UserData.Market, Conversion.getInt32OrNull(DepArrival), ref errorMsg);
                if (location != null)
                {
                    transferFirst.TrfArrName = location.Name;
                    transferFirst.TrfArrNameL = location.NameL;
                }
            }
            transferFirst.PickupTime = depPickupTime;
            transferFirst.PickupNote = DepPickupNote;
            transferLast.BegDate = endDate;
            transferLast.EndDate = endDate;
            if (DepRegion == "1")
            {
                transferLast.TrfDT = 1;
                transferLast.TrfDTName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceHotel").ToString();
            }
            else
            {
                transferLast.TrfDT = 2;
                transferLast.TrfDTName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceOther").ToString();
            }
            transferLast.TrfAT = 0;
            transferLast.TrfATName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceFlight").ToString();
            transferLast.TrfArr = RetFlightNo;
            transferLast.TrfArrName = RetFlightNo;
            transferLast.TrfArrNameL = RetFlightNo;
            if (string.Equals(PickupType, "1"))
                transferLast.TrfDep = RetDeparture;
            else transferLast.TrfDep = DepArrival;
            if (DepRegion == "1")
            {
                hotel = new Hotels().getHotelDetail(UserData, !string.Equals(PickupType, "1") ? RetDeparture : transferLast.TrfDep, ref errorMsg);
                if (hotel != null)
                {
                    transferLast.TrfDepName = hotel.Name;
                    transferLast.TrfDepNameL = hotel.LocalName;
                }
            }
            else
            {
                location = new Locations().getLocation(UserData.Market, Conversion.getInt32OrNull(RetDeparture), ref errorMsg);
                if (location != null)
                {
                    transferLast.TrfArrName = location.Name;
                    transferLast.TrfArrNameL = location.NameL;
                }
            }

            transferLast.PickupTime = retPickupTime;
            transferLast.PickupNote = RetPickupNote;
            #endregion
        }
        else
        {
            #region One way
            ResServiceRecord transfer = services.Find(f => f.RecID == firstService.RecID);
            HotelRecord hotel = null;
            Location location = null;
            switch (Direction)
            {
                case "R":
                    transfer.BegDate = begDate;
                    transfer.EndDate = endDate;
                    transfer.TrfDT = 0;
                    transfer.TrfDTName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceFlight").ToString();
                    if (DepRegion == "1")
                    {
                        transfer.TrfAT = 1;
                        transfer.TrfATName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceHotel").ToString();
                    }
                    else
                    {
                        transfer.TrfAT = 2;
                        transfer.TrfATName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceOther").ToString();
                    }
                    transfer.TrfDep = DepFlightNo;
                    transfer.TrfDepName = DepFlightNo;
                    transfer.TrfDepNameL = DepFlightNo;

                    transfer.TrfArr = DepArrival;
                    hotel = new Hotels().getHotelDetail(UserData, transfer.TrfArr, ref errorMsg);
                    if (hotel != null)
                    {
                        transfer.TrfArrName = hotel.Name;
                        transfer.TrfArrNameL = hotel.LocalName;
                    }
                    if (DepRegion == "1")
                    {
                        hotel = new Hotels().getHotelDetail(UserData, DepArrival, ref errorMsg);
                        if (hotel != null)
                        {
                            transfer.TrfArrName = hotel.Name;
                            transfer.TrfArrNameL = hotel.LocalName;
                        }
                    }
                    else
                    {
                        location = new Locations().getLocation(UserData.Market, Conversion.getInt32OrNull(DepArrival), ref errorMsg);
                        if (location != null)
                        {
                            transfer.TrfArrName = location.Name;
                            transfer.TrfArrNameL = location.NameL;
                        }
                    }

                    if (DepRegion == "1")
                    {
                        transfer.RTrfDT = 1;
                        transfer.RTrfDTName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceHotel").ToString();
                    }
                    else
                    {
                        transfer.RTrfDT = 2;
                        transfer.RTrfDTName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceOther").ToString();
                    }

                    transfer.RTrfAT = 0;
                    transfer.RTrfATName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceFlight").ToString();
                    transfer.RTrfArr = RetFlightNo;
                    transfer.RTrfArrName = RetFlightNo;
                    transfer.RTrfArrNameL = RetFlightNo;

                    if (string.Equals(PickupType, "1"))
                        transfer.RTrfDep = RetDeparture;
                    else
                        transfer.RTrfDep = DepArrival;

                    if (DepRegion == "1")
                    {
                        hotel = new Hotels().getHotelDetail(UserData, transfer.RTrfDep, ref errorMsg);
                        if (hotel != null)
                        {
                            transfer.RTrfDepName = hotel.Name;
                            transfer.RTrfDepName = hotel.LocalName;
                        }
                    }
                    else
                    {
                        location = new Locations().getLocation(UserData.Market, Conversion.getInt32OrNull(transfer.RTrfDep), ref errorMsg);
                        if (location != null)
                        {
                            transfer.RTrfDepName = location.Name;
                            transfer.RTrfDepName = location.NameL;
                        }
                    }

                    transfer.PickupTime = depPickupTime;
                    transfer.PickupNote = DepPickupNote;
                    transfer.RPickupTime = retPickupTime;
                    transfer.RPickupNote = RetPickupNote;
                    break;
                case "F":
                    transfer.BegDate = begDate;
                    transfer.EndDate = endDate;
                    transfer.TrfDT = 0;
                    transfer.TrfDTName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceFlight").ToString();
                    if (DepRegion == "1")
                    {
                        transfer.TrfAT = 1;
                        transfer.TrfATName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceHotel").ToString();
                    }
                    else
                    {
                        transfer.TrfAT = 2;
                        transfer.TrfATName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceOther").ToString();
                    }
                    transfer.TrfDep = DepFlightNo;
                    transfer.TrfDepName = DepFlightNo;
                    transfer.TrfDepNameL = DepFlightNo;
                    transfer.TrfArr = DepArrival;

                    if (DepRegion == "1")
                    {
                        hotel = new Hotels().getHotelDetail(UserData, transfer.TrfArr, ref errorMsg);
                        if (hotel != null)
                        {
                            transfer.TrfArrName = hotel.Name;
                            transfer.TrfArrNameL = hotel.LocalName;
                        }
                    }
                    else
                    {
                        location = new Locations().getLocation(UserData.Market, Conversion.getInt32OrNull(transfer.TrfArr), ref errorMsg);
                        if (location != null)
                        {
                            transfer.TrfArrName = location.Name;
                            transfer.TrfArrNameL = location.NameL;
                        }
                    }
                    transfer.PickupTime = depPickupTime;
                    transfer.PickupNote = DepPickupNote;
                    break;
                case "B":
                    transfer.BegDate = begDate;
                    transfer.EndDate = endDate;
                    if (DepRegion == "1")
                    {
                        transfer.TrfDT = 1;
                        transfer.TrfDTName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceHotel").ToString();
                    }
                    else
                    {
                        transfer.TrfDT = 2;
                        transfer.TrfDTName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceOther").ToString();
                    }
                    transfer.TrfAT = 0;
                    transfer.TrfATName = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceFlight").ToString();
                    transfer.TrfArr = DepFlightNo;
                    transfer.TrfArrName = DepFlightNo;
                    transfer.TrfArrNameL = DepFlightNo;
                    transfer.TrfDep = DepArrival;
                    if (DepRegion == "1")
                    {
                        hotel = new Hotels().getHotelDetail(UserData, transfer.TrfDep, ref errorMsg);
                        if (hotel != null)
                        {
                            transfer.TrfDepName = hotel.Name;
                            transfer.TrfDepNameL = hotel.LocalName;
                        }
                    }
                    else
                    {
                        location = new Locations().getLocation(UserData.Market, Conversion.getInt32OrNull(transfer.TrfDep), ref errorMsg);
                        if (location != null)
                        {
                            transfer.TrfDepName = location.Name;
                            transfer.TrfDepNameL = location.NameL;
                        }
                    }
                    transfer.PickupTime = depPickupTime;
                    transfer.PickupNote = DepPickupNote;
                    break;
            }
            #endregion
        }


        resMain.BegDate = begDate;
        resMain.EndDate = endDate;
        resMain.Days = Conversion.getInt16OrNull((endDate.Value - begDate.Value).Days.ToString());

        #region Credit control
        Int16 CreditCont = new TvBo.Reservation().CreditControl(UserData.AgencyID, UserData.Market, ref errorMsg);

        if (CreditCont == 2)
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = HttpContext.GetGlobalResourceObject("LibraryResource", "NoCreditLimit").ToString()
            });
            return returnData;
        }
        #endregion

        #region No servis error
        if (ResData.ResService.Count < 1)
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = HttpContext.GetGlobalResourceObject("LibraryResource", "NoResServiceMsg").ToString()
            });
            return returnData;
        }
        #endregion

        #region Check flight time
        if (!(new Flights().CheckFlightTime(UserData, ResData.ResService, ref errorMsg)))
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = HttpContext.GetGlobalResourceObject("LibraryResource", "CheckInTimeOver").ToString()
            });
            return returnData;
        }
        #endregion

        List<CustControlErrorRecord> custControl = new Reservation().CustControl(UserData, ResData, ref errorMsg);
        if ((custControl == null || custControl.Count == 1) && (custControl.Where(w => w.ErrorCode == 0).Count() == 1 || custControl.Where(w => w.ErrorCode == 30).Count() == 1))
        {
            resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
            if (setOptionTime.HasValue && setOptionTime.Value == false)
                resMain.OptDate = null;

            returnData = new Reservation().SaveReservation(UserData, ref ResData, saveAsDraft);

            if (returnData == null || returnData.Count < 1)
            {
                returnData.Add(new ReservastionSaveErrorRecord
                {
                    ControlOK = false,
                    Message = HttpContext.GetGlobalResourceObject("LibraryResource", "ResNotSaved").ToString(),
                    OtherReturnValue = custControl.FirstOrDefault().ErrorCode.ToString()
                });
                return returnData;
            }
            else
            {
                bool? saveResAfterResView = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "SaveResAfterResView"));
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                {
                    if (UserData.AgencyRec != null && UserData.AgencyRec.AceExport)
                    {
                        string AceFileName = string.Empty;
                        if (!(new Aces().SendAceTo(UserData, ResData, AppDomain.CurrentDomain.BaseDirectory + "ACE//", ref AceFileName, ref errorMsg)))
                        {
                        }
                    }

                    returnData.Clear();
                    ReservastionSaveErrorRecord retVal = new ReservastionSaveErrorRecord();
                    if (custControl.Where(w => w.ErrorCode == 30).Count() == 1)
                    {
                        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
                        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
                        string msg = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString(), ResData.ResMain.ResNo);
                        msg += "<br />";
                        msg += HttpContext.GetGlobalResourceObject("MakeReservation", "parentAuthorizationMsg").ToString();
                        msg += "<br />";
                        msg += string.Format("<a href=\"#\" onClick=\"window.open('{0}','mywindow')\" style=\"font-style:italic; font-weight:bold;\">{1}</a>",
                                             WebRoot.BasePageRoot + DocumentFolder + "/" + UserData.Market + "/authorization.pdf",
                                             HttpContext.GetGlobalResourceObject("MakeReservation", "parentAuthorizationLnk"));
                        retVal.ControlOK = true;
                        retVal.Message = msg;
                        retVal.OtherReturnValue = custControl != null && custControl.Count > 0 ? custControl.FirstOrDefault().ErrorCode.ToString() : string.Empty;
                        retVal.GotoReservation = !saveResAfterResView.HasValue || (saveResAfterResView.HasValue && saveResAfterResView.Value);
                        returnData.Add(retVal);
                    }
                    else
                    {
                        List<resPayPlanRecord> resPayPlan = new ReservationMonitor().getPaymentPlan(ResData.ResMain.ResNo, ref errorMsg);
                        string paymentPlanStr = new TvBo.ReservationMonitor().getPaymentPlanHTML(ResData.ResMain.ResNo, UserData.Ci.LCID);
                        retVal.ControlOK = true;
                        if (resPayPlan != null && resPayPlan.Count > 0)
                        {
                            resPayPlanRecord firstPayment = resPayPlan.OrderBy(o => o.DueDate).FirstOrDefault();
                            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                                retVal.Message = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString() + "<br />" + paymentPlanStr,
                                                                        ResData.ResMain.ResNo);
                            else
                            {
                                string paidMessage = string.Empty;
                                if (firstPayment.Amount.HasValue)
                                    paidMessage = string.Format(HttpContext.GetGlobalResourceObject("MakeReservation", "lblAmount").ToString(),
                                                                           firstPayment.Amount.Value.ToString("#,###.00") + " " + firstPayment.Cur,
                                                                           firstPayment.DueDate.ToShortDateString());
                                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Orex))
                                    paidMessage = string.Empty;
                                retVal.Message = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString() + "<br />{1}",
                                                                        ResData.ResMain.ResNo,
                                                                        paidMessage);
                            }
                        }
                        else
                            retVal.Message = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString(),
                                ResData.ResMain.ResNo);

                        retVal.OtherReturnValue = custControl != null && custControl.Count > 0 ? custControl.FirstOrDefault().ErrorCode.ToString() : string.Empty;
                        retVal.SendEmailB2C = getEmailUrl(UserData, ResData);
                        retVal.GotoPaymentPage = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur);
                        retVal.GotoReservation = !saveResAfterResView.HasValue || (saveResAfterResView.HasValue && saveResAfterResView.Value);
                        retVal.ResNo = ResData.ResMain.ResNo;
                        returnData.Add(retVal);
                    }

                    String useLog = System.Configuration.ConfigurationManager.AppSettings["useLog"];
                    if (!string.IsNullOrEmpty(useLog) && string.Equals(useLog, "1"))
                    {
                        if (ResData.LogID.HasValue)
                            new WEBLog().updateWEBBookLog(ResData.LogID.Value, DateTime.Now, ResData.ResMain.ResNo, ref errorMsg);
                    }

                    return returnData;
                }
                else
                {
                    return returnData;
                }
            }
        }
        else
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = new Reservation().getCustControlErrorMessage(custControl),
                OtherReturnValue = custControl != null && custControl.Count > 0 ? custControl.FirstOrDefault().ErrorCode.ToString() : string.Empty
            });
            return returnData;
        }

    }

    public static string getEmailUrl(User UserData, ResDataRecord ResData)
    {
        try
        {
            if (string.Equals(UserData.CustomRegID, Common.crID_CelexTravel))
            {
                ResServiceRecord hotel = ResData.ResService.Where(w => w.ServiceType == "HOTEL").FirstOrDefault();
                if (hotel != null)
                {
                    AsyncMailSenderParams mailParam = new AsyncMailSenderParams();
                    mailParam.SenderMail = UserData.TvParams.TvParamSystem.SMTPAccount;
                    mailParam.DisplayName = UserData.TvParams.TvParamSystem.SMTPAccount;
                    mailParam.SenderPassword = UserData.TvParams.TvParamSystem.SMTPPass;
                    string fromEmails = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "resSendEmails"));
                    mailParam.ToAddress = string.IsNullOrEmpty(fromEmails) ? "celex@celextravel.com;mehmet@celextravel.com;meral@celextravel.com" : fromEmails;
                    mailParam.Subject = "New reservation.Res no: " + ResData.ResMain.ResNo;
                    mailParam.Port = UserData.TvParams.TvParamSystem.SMTPPort;
                    mailParam.SMTPServer = UserData.TvParams.TvParamSystem.SMTPServer;
                    mailParam.EnableSSL = UserData.TvParams.TvParamSystem.SMTPUseSSL.HasValue && UserData.TvParams.TvParamSystem.SMTPUseSSL.Value == 1 ? true : false;
                    string body = ResData.ResMain.ResNo + UserData.AgencyName + hotel.ServiceName + hotel.BegDate.Value.ToString("dd/MM/yyyy") + hotel.EndDate.Value.ToString("dd/MM/yyyy") + hotel.RoomName + hotel.AccomName + hotel.BoardName;
                    mailParam.Body = body;

                    new SendMail().MailSender(mailParam);
                }
                return "";
            }
            else
                if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) || Equals(UserData.CustomRegID, TvBo.Common.crID_Kenba))
                {
                    string url = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "b2cEmailUrl"));
                    url = url.Trim().Trim('\n').Trim('\r');

                    ResCustRecord leader = ResData.ResCust.Find(f => f.Leader == "Y");
                    ResCustInfoRecord leaderInfo = leader != null ? ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo) : null;
                    if (!string.IsNullOrEmpty(url) && leaderInfo != null)
                    {
                        if ((Equals(leaderInfo.ContactAddr, "H") ? (string.IsNullOrEmpty(leaderInfo.AddrHomeEmail) ? true : false) : (string.IsNullOrEmpty(leaderInfo.AddrWorkEMail) ? true : false)) == false)
                        {
                            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) || Equals(UserData.CustomRegID, TvBo.Common.crID_Kenba))
                            {
                                fi.detur.SendMail sm = new fi.detur.SendMail();
                                sm.Url = url;
                                sm.MailSend(ResData.ResMain.ResNo, UserData.Ci.Name, ResData.ResMain.Agency);
                            }
                            return string.Format("'serviceUrl':'{0}','ResNo':'{1}','CultureID':'{2}','AgencyID':'{3}'",
                                                    url,
                                                    ResData.ResMain.ResNo,
                                                    UserData.Ci.Name,
                                                    ResData.ResMain.Agency);
                        }
                        else return string.Empty;

                    }
                    else return string.Empty;
                }
                else return string.Empty;
        }
        catch
        {
            return string.Empty;
        }
    }

}