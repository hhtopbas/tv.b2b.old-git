﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Text;
using System.Web.Services;
using TvTools;
using System.Web.Script.Services;
using System.Collections;

public partial class OnlyExcursionSearchFilter : BasePage
{
    public static string twoLetterISOLanguageName = "en";

    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<string> lang = new List<string>() { "ar-DZ", "ar-DZ", "ar", "en-AU", "en-GB", "en-NZ", "fr-CH", "fr", "pt-BR", "pt", "sr-SR", "sr-CS", "sr", "zh-CN", "zh-HK", "zh-TW" };
        if (lang.Contains(UserData.Ci.Name))
            twoLetterISOLanguageName = UserData.Ci.Name;
        else
            twoLetterISOLanguageName = UserData.Ci.TwoLetterISOLanguageName != "en" ? UserData.Ci.TwoLetterISOLanguageName : "";
    }    

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static bool? CreateCriteria()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        ExcursionSearchCriteria criteria = new ExcursionSearchCriteria
        {
            SType = SearchType.OnlyExcursion,
            RoomCount = 1,
            RoomsInfo = new List<SearchCriteriaRooms>(),
            CheckIn = DateTime.Today,
            ExpandDate = 0,
            Hour = null,
            HourExpand = null,
            Duration = 1
        };
        HttpContext.Current.Session["ExcuresionCriteria"] = criteria;
        return true;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        if (!CreateCriteria().HasValue)
        {
            HttpContext.Current.Response.StatusCode = 408;
            return null;
        }

        ExcursionSearchCriteria criteria = (ExcursionSearchCriteria)HttpContext.Current.Session["ExcuresionCriteria"];
        bool? currencyConvert = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "ShowMarketCur"));
        List<ReadyCurrencyRecord> getCurrencyList = new TvBo.Common().getReadyCurrency(UserData.Market, UserData.SaleCur, ref errorMsg);

        List<Int32> fltCheckInDay = new List<Int32>();
        string lblDay = Conversion.getStrOrNull(HttpContext.GetGlobalResourceObject("LibraryResource", "lblDay"));
        for (int i = 0; i <= 3; i++) fltCheckInDay.Add(i);

        List<Int32> fltHourExpand = new List<Int32>();
        for (int i = 0; i <= 12; i++) fltHourExpand.Add(i);

        List<Int32> fltRoomCount = new List<Int32>();
        fltRoomCount.Add(1);

        List<Int32> fltDuration = new List<Int32>();
        for (int i = 1; i <= 12; i++) fltDuration.Add(i);

        string curList = string.Empty;
        curList += "<select id=\"currentCur\">";
        foreach (ReadyCurrencyRecord row in getCurrencyList)
        {
            curList += string.Format("<option value=\"{0}\" {2} >{1}</option>",
                                        row.Code,
                                        Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && Equals(UserData.Market, "SWEMAR") ? row.Code : row.NameL,
                                        Equals(UserData.SaleCur, row.Code) ? "selected=\"selected\"" : "");
        }
        curList += "</select>";
        string marketCur = string.Format("<div {3}><input id=\"CurrencyConvert\" type=\"checkbox\" {0} /><label for=\"CurrencyConvert\">{1}</label>&nbsp;{2}</div>",
                currencyConvert.HasValue && currencyConvert.Value ? "checked='checked'" : "",
                HttpContext.GetGlobalResourceObject("LibraryResource", "lblShowInPrice2"),
                curList,
                Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && Equals(UserData.Market, "FINMAR") ? "style=\"display: none;\"" : "");
        List<AirportRecord> _airports = new Flights().getCityAirportLists(UserData, null, null, ref errorMsg);
        var airports = from q in _airports
                       select new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name };
        return new
        {
            CheckIn = DateTime.Today,
            CustomRegID = UserData.CustomRegID,
            CurrencyConvert = currencyConvert.HasValue ? currencyConvert.Value : true,
            MarketCur = marketCur,
            DateFormat = UserData.Ci.TwoLetterISOLanguageName,
            fltCheckInDay = new fltComboDayRecord { maxDayList = fltCheckInDay, selectedDay = 0 },
            fltRoomCount = new fltComboDayRecord { maxDayList = fltRoomCount, selectedDay = 0 },
            fltHourExpand = new fltComboDayRecord { maxDayList = fltHourExpand, selectedDay = 1 },
            fltDuration = new fltComboDayRecord { maxDayList = fltDuration, selectedDay = 1 },
            Airport = airports
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getAirports(int? city)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        List<AirportRecord> _airports = new Flights().getCityAirportLists(UserData, null, city, ref errorMsg);
        var airports = from q in _airports
                       select new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name };
        return airports;
    }

    [WebMethod(EnableSession = true)]
    public static string getRoomInfo(string RoomCount)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        //Int16 maxChildAge = UserData.TvParams.TvParamReser.MaxChdAge.HasValue ? Convert.ToInt16(Math.Floor(UserData.TvParams.TvParamReser.MaxChdAge.Value)) : (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ? Convert.ToInt16(18) : Convert.ToInt16(12));
        Int16 maxChildAge = UserData.TvParams.TvParamReser.MaxChdAgeB2Bsrc.HasValue ? Convert.ToInt16(Math.Floor(UserData.TvParams.TvParamReser.MaxChdAgeB2Bsrc.Value)) : (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ? Convert.ToInt16(18) : Convert.ToInt16(12));
        StringBuilder html = new StringBuilder();
        plMaxPaxCounts maxPaxData = new plMaxPaxCounts();
        object _paxData = new TvBo.Common().getFormConfigValue("SearchPanel", "PaxData");
        if (_paxData != null)
        {
            plMaxPaxCounts paxData = Newtonsoft.Json.JsonConvert.DeserializeObject<plMaxPaxCounts>(_paxData.ToString());
            maxPaxData = paxData;
        }
        if (HttpContext.Current.Session["SearchPLData"] != null && (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Elsenal)))
            if (((SearchPLData)HttpContext.Current.Session["SearchPLData"]).PlMaxPaxCount != null)
                maxPaxData = ((SearchPLData)HttpContext.Current.Session["SearchPLData"]).PlMaxPaxCount;
        if (string.Equals(UserData.CustomRegID, Common.crID_Rezeda) || string.Equals(UserData.CustomRegID, Common.crID_KidyTour))
            maxPaxData.maxAdult = 12;
        Int32 j = 0;
        int i = 1;

        html.AppendFormat("<div id=\"divRoomInfo{0}\" style=\"width:250px; clear:both;\">", i.ToString());

        html.AppendFormat("  <div id=\"divAdult{0}\" style=\"float: left; width:100px;\">", i.ToString());
        html.AppendFormat("  <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblAdult").ToString());
        html.AppendFormat("  <select id=\"fltAdult{0}\" style=\"width: 50px;\">", i.ToString(), i.ToString());
        if (string.Equals(UserData.CustomRegID, TvBo.Common.ctID_Falcon))
            maxPaxData.maxAdult = 60;
        for (j = 1; j <= maxPaxData.maxAdult; j++)
        {
            if (j == 2)
                html.AppendFormat("     <option value='{0}' selected=\"selected\">{1}</option>", j.ToString(), j.ToString());
            else html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        }
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");

        html.AppendFormat("  <div id=\"divChild{0}\" style=\"float: left; width:50px;\">", i.ToString());
        html.AppendFormat("  <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChild").ToString());
        html.AppendFormat("  <select id=\"fltChild{0}\" onchange=\"onChildChange('{1}')\" style=\"width: 50px;\">", i.ToString(), i.ToString());
        for (j = 0; j < 5; j++)
            html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");
        html.AppendFormat("</div>");

        html.AppendFormat("<div id=\"divRoomInfoChd{0}\" style=\"width:250px; clear:both; display:none;\">", i.ToString());
        html.AppendFormat(" <div id=\"divRoomInfoChd1{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
        html.AppendFormat("   <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 1");
        html.AppendFormat("   <select id=\"fltRoomInfoChd1{0}\" style=\"width: 100%;\">", i.ToString());
        for (j = 0; j <= maxChildAge; j++)
            html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");
        html.AppendFormat(" <div id=\"divRoomInfoChd2{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
        html.AppendFormat("   <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 2");
        html.AppendFormat("   <select id=\"fltRoomInfoChd2{0}\" style=\"width: 100%;\">", i.ToString());
        for (j = 0; j <= maxChildAge; j++)
            html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");
        html.AppendFormat(" <div id=\"divRoomInfoChd3{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
        html.AppendFormat("   <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 3");
        html.AppendFormat("   <select id=\"fltRoomInfoChd3{0}\" style=\"width: 100%;\">", i.ToString());
        for (j = 0; j <= maxChildAge; j++)
            html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");
        html.AppendFormat(" <div id=\"divRoomInfoChd4{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
        html.AppendFormat("   <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 4");
        html.AppendFormat("   <select id=\"fltRoomInfoChd4{0}\" style=\"width: 100%;\">", i.ToString());
        for (j = 0; j <= maxChildAge; j++)
            html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");

        html.AppendFormat("</div>");

        return html.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string SetCriterias(Int16? Type, long? CheckIn, string ExpandDate, string Hour, string HourExpand, string Duration,
            string RoomCount, string roomsInfo, string ArrCity, string Category, string CurControl, string CurrentCur, string FlightNo, string Airport)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["ExcuresionCriteria"] == null) CreateCriteria();
        ExcursionSearchCriteria criteria = (ExcursionSearchCriteria)HttpContext.Current.Session["ExcuresionCriteria"];
        criteria.Type = Type.HasValue ? (ExcursionType)((Int16)Type.Value) : ExcursionType.Regular;
        DateTime checkIn = new DateTime(1970, 1, 1);
        checkIn = checkIn.AddDays((Convert.ToInt64(CheckIn) / 86400000) + 1);
        criteria.CheckIn = checkIn;
        criteria.ExpandDate = Convert.ToInt16(ExpandDate);
        criteria.Hour = Conversion.getDateTimeOrNull(Hour);
        criteria.HourExpand = Conversion.getInt16OrNull(HourExpand);
        criteria.Duration = Conversion.getInt16OrNull(Duration);
        criteria.RoomCount = Convert.ToInt16(RoomCount);
        string _room = "[" + roomsInfo.Replace("|", "\"").Replace(')', '}').Replace('(', '{') + "]";
        List<SearchCriteriaRooms> rooms = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SearchCriteriaRooms>>(_room);
        criteria.RoomsInfo.Clear();
        int i = 0;
        foreach (SearchCriteriaRooms row in rooms)
        {
            ++i;
            criteria.RoomsInfo.Add(new SearchCriteriaRooms
            {
                RoomNr = i,
                Adult = row.Adult,
                Child = row.Child,
                Chd1Age = row.Chd1Age >= 0 ? row.Chd1Age : null,
                Chd2Age = row.Chd2Age >= 0 ? row.Chd2Age : null,
                Chd3Age = row.Chd3Age >= 0 ? row.Chd3Age : null,
                Chd4Age = row.Chd4Age >= 0 ? row.Chd4Age : null
            });
        }
        criteria.ArrCity = Conversion.getInt32OrNull(!Equals(ArrCity, "null") ? ArrCity : "");
        criteria.Category = Conversion.getStrOrNull(Category);
        criteria.CurControl = Equals(CurControl, "Y");
        criteria.CurrentCur = CurrentCur;
        criteria.SType = SearchType.OnlyExcursion;
        criteria.FlightNo = FlightNo;
        criteria.AirPort = Airport;

        HttpContext.Current.Session["ExcuresionCriteria"] = criteria;
        return "OK";
    }

    public static bool? getSearchFilterData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["SearchExcData"] == null)
        {
            List<ExcursionSearchFilterData> filterData = new ExcursionSearch().getExcursionFilterData(UserData, null, ref errorMsg);
            HttpContext.Current.Session["SearchExcData"] = filterData;
        }
        return true;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static IEnumerable getArrival(string Type)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["SearchExcData"] == null) getSearchFilterData();
        if (HttpContext.Current.Session["SearchExcData"] == null) return null;
        List<ExcursionSearchFilterData> data = (List<ExcursionSearchFilterData>)HttpContext.Current.Session["SearchExcData"];
        Int16? type = Conversion.getInt16OrNull(Type);
        var retVal = from q in data
                     where !type.HasValue || q.Type == (ExcursionType)type.Value
                     orderby (useLocalName ? q.LocationNameL : q.LocationName)
                     group q by new
                     {
                         RecID = q.Location,
                         Name = useLocalName ? q.LocationNameL : q.LocationName
                     } into k
                     select new
                     {
                         RecID = k.Key.RecID,
                         Name = k.Key.Name
                     };
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static IEnumerable getCategoryData(string ArrCity)
    {
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? arrival = Conversion.getInt32OrNull(ArrCity);
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        if (HttpContext.Current.Session["SearchExcData"] == null) getSearchFilterData();
        if (HttpContext.Current.Session["SearchExcData"] == null) return null;
        List<ExcursionSearchFilterData> data = (List<ExcursionSearchFilterData>)HttpContext.Current.Session["SearchExcData"];

        var retVal = from q in data
                     where (!arrival.HasValue || q.Location == arrival.Value) && !string.IsNullOrEmpty(q.Category)
                     group q by new
                     {
                         Code = q.Category,
                         Name = useLocalName ? q.CategoryNameL : q.CategoryName
                     } into k
                     select new
                     {
                         Code = k.Key.Code,
                         Name = k.Key.Name
                     };

        return retVal;
    }
}