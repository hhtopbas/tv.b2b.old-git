﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Data;
using System.Globalization;
using System.Threading;
using System.Text;
using System.Web.Services;
using System.Web;
using TvTools;
using System.Web.Script.Services;
using System.IO;
using System.Net;
using System.Configuration;

public partial class MakeReservation : BasePage
{
    protected string CopyPasteIsActive;
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        if (UserData.Ci.Name.ToLower() == "lt-lt")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "yyyy.MM.dd";
        else if (UserData.Ci.Name.ToLower() == "lv-lv")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        var copyPasteIsActive = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "CopyPasteIsActive"));
        if (copyPasteIsActive.HasValue && copyPasteIsActive.Value)
            CopyPasteIsActive = "true";
        else
            CopyPasteIsActive = "false";

    }

    [WebMethod(EnableSession = true)]
    public static string DrawButtons()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        bool onlyTicket = false; // ResData.ResMain.SaleResource == 2 || ResData.ResMain.SaleResource == 3 || ResData.ResMain.SaleResource == 5;
        List<SaleServiceRecord> saleRecords = new ReservationCommon().getSaleServices(UserData, ResData, ref errorMsg);
        StringBuilder MenuStr = new StringBuilder();
        MenuStr.Append("<div id=\"divAddResServiceMenu\"  class=\"ui-widget-header\"><div style=\"min-width: 100px;\">");
        List<SaleServiceRecord> saleService = new List<SaleServiceRecord>();
        if (string.IsNullOrEmpty(ResData.ResMain.PackType))
            saleService = saleRecords.Where(w => w.B2BIndPack.HasValue && w.B2BIndPack.Value == true).ToList<SaleServiceRecord>();
        else
            saleService = saleRecords.Where(w => string.Equals(w.WebSale, "Y")).ToList<SaleServiceRecord>();

        List<AgencySaleSer> agencySaleService = new Agency().getAgencySaleRestriction(UserData.AgencyID, ref errorMsg);

        foreach (SaleServiceRecord row in saleService)
        {
            if (agencySaleService.Find(f => f.ServiceType == row.Service) == null)
            {
                switch (row.Service)
                {
                    case "HOTEL":
                        int? cultureTourIndividualMaxHotelCount = Conversion.getInt32OrNull(new TvBo.Common().getFormConfigValue("General", "CultureTourIndividualMaxHotelCount"));
                        if (!ResData.OnlyTransfer)
                            if (!cultureTourIndividualMaxHotelCount.HasValue || (cultureTourIndividualMaxHotelCount.HasValue && ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL") && string.Equals(w.IncPack, "N")).Count() < cultureTourIndividualMaxHotelCount.Value))
                                MenuStr.AppendFormat(" <div onclick=\"showAddResService('Controls/RSAdd_Hotel{0}.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/hotel.gif\" width=\"32\" height=\"32\"><br />" +
                                        HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceHotel").ToString() + " </div>",
                                        Equals(ConfigurationManager.AppSettings["CalendarV2"], "1") ? "NC" : string.Empty);
                        break;
                    case "FLIGHT":
                        if (!ResData.OnlyTransfer)
                            MenuStr.AppendFormat(" <div onclick=\"showAddResService('Controls/RSAdd_Flight{0}.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/flight.gif\" width=\"32\" height=\"32\"><br />" +
                                    HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceFlight").ToString() + " </div>",
                                    Equals(ConfigurationManager.AppSettings["CalendarV2"], "1") ? "NC" : string.Empty);
                        break;
                    case "TRANSPORT":
                        if (!ResData.OnlyTransfer)
                            MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Transport.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/transport.gif\" width=\"32\" height=\"32\"><br />" +
                                        HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceTransport").ToString() + " </div>");
                        break;
                    case "RENTING":
                        if (!ResData.OnlyTransfer)
                            MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Renting.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/renting.gif\" width=\"32\" height=\"32\"><br />" +
                                        HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceRenting").ToString() + " </div>");
                        break;
                    case "EXCURSION":
                        if (!ResData.OnlyTransfer)
                            MenuStr.AppendFormat(" <div onclick=\"showAddResService('Controls/RSAdd_Excursion{0}.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/excursion.gif\" width=\"32\" height=\"32\"><br />" +
                                                HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceExcursion").ToString() + " </div>",
                                                Equals(ConfigurationManager.AppSettings["CalendarV2"], "1") ? "NC" : string.Empty);
                        break;
                    case "INSURANCE":
                        if (!ResData.OnlyTransfer)
                            MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Insurance.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/insurance.gif\" width=\"32\" height=\"32\"><br />" +
                                        HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceInsurance").ToString() + " </div>");
                        break;
                    case "VISA":
                        if (!ResData.OnlyTransfer)
                            MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Visa.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/visa.gif\" width=\"32\" height=\"32\"><br />" +
                                        HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceVisa").ToString() + " </div>");
                        break;
                    case "TRANSFER":
                        if (ResData.OnlyTransfer)
                            MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_OnlyTransfer.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/transfer.gif\" width=\"32\" height=\"32\"><br />" +
                                        HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceTransfer").ToString() + " </div>");
                        else if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) ||
                                 string.Equals(UserData.CustomRegID, TvBo.Common.crID_Elsenal) ||
                                 string.Equals(UserData.CustomRegID, TvBo.Common.crID_YekTravel) ||
                                 string.Equals(UserData.CustomRegID, TvBo.Common.crID_FilipTravel) ||
                                 string.Equals(UserData.CustomRegID, TvBo.Common.crID_ZemExpert) ||
                                 string.Equals(UserData.CustomRegID, TvBo.Common.crID_Calypso) ||
                                 string.Equals(UserData.CustomRegID, TvBo.Common.crID_MbnTour_Ir) ||
                                 string.Equals(UserData.CustomRegID, TvBo.Common.crID_MbnTour_Tr) ||
                                 string.Equals(UserData.CustomRegID, TvBo.Common.crID_Qasswa) ||
                                 string.Equals(UserData.CustomRegID, TvBo.Common.crID_SeaTravel) ||
                                 string.Equals(UserData.CustomRegID, TvBo.Common.crID_CelexTravel))
                            MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_TransferV2.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/transfer.gif\" width=\"32\" height=\"32\"><br />" +
                                        HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceTransfer").ToString() + " </div>");
                        else if (!onlyTicket || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Go2Holiday) || !(Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && Equals(UserData.Market, "SWEMAR")))
                            MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Transfer.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/transfer.gif\" width=\"32\" height=\"32\"><br />" +
                                        HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceTransfer").ToString() + " </div>");
                        break;
                    case "HANDFEE":
                        if (!ResData.OnlyTransfer)
                            MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Handfee.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/handfee.gif\" width=\"32\" height=\"32\"><br />" +
                                        HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceHandfee").ToString() + " </div>");
                        break;
                    default:
                        if (!ResData.OnlyTransfer)
                            MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Other.aspx?serviceCode=" + row.Service + "');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/other.gif\" width=\"32\" height=\"32\"><br />" +
                                /*HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceOther").ToString()*/ (string.IsNullOrEmpty(row.NameL) ? row.Name : row.NameL) + " </div>");
                        break;
                }
            }
        }
        if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_FitTurizm))
            MenuStr.Append(" <div onclick=\"showAddResServiceExt('Controls/ExtraService_Add.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/extraservice.gif\" width=\"32\" height=\"32\"><br />" +
                                            HttpContext.GetGlobalResourceObject("LibraryResource", "AddExtraService").ToString() + " </div>");
        MenuStr.Append("</div></div>");
        return MenuStr.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string userBlackList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (UserData.BlackList)
            return HttpContext.GetGlobalResourceObject("LibraryResource", "NoAuthResForBlacklist").ToString();
        else
            return string.Empty;
    }

    [WebMethod(EnableSession = true)]
    public static string getPromoList(string Honeymoon, string PromoCode, string Customers, string Discount, string ResNote, string aceCustomerCode, string aceDosier,
                         string Code1, string Code2, string Code3, string Code4)
    {
        if (HttpContext.Current.Session["UserData"] == null || string.IsNullOrEmpty(Customers)) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        ResDataRecord _ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        ResMainRecord _resMain = _ResData.ResMain;
        _resMain.ResNote = ResNote.Replace('|', '\"');

        _resMain.Code1 = Code1;
        _resMain.Code2 = Code2;
        _resMain.Code3 = Code3;
        _resMain.Code4 = Code4;

        HttpContext.Current.Session["ResData"] = _ResData;

        ResDataRecord ResData = new ResTables().copyData(_ResData);
        ResMainRecord resMain = ResData.ResMain;

        if (!(Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) ||
            Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ||
            Equals(UserData.CustomRegID, TvBo.Common.crID_Anex) ||
            Equals(UserData.CustomRegID, TvBo.Common.crID_Go2Holiday) ||
            Equals(UserData.CustomRegID, TvBo.Common.crID_HolidayPlus) ||
            Equals(UserData.CustomRegID, TvBo.Common.crID_FilipTravel) ||
            Equals(UserData.CustomRegID, TvBo.Common.ctID_TivronaTours) ||
            Equals(UserData.CustomRegID, TvBo.Common.crID_KidyTour)||
            (Equals(UserData.CustomRegID, TvBo.Common.crID_Kenba) && ResData.Resource == ResResource.B2B)))
            return "{\"retVal\":\"OK\",\"data\":\"|!|\",\"CustomRegID\":\"" + UserData.CustomRegID.ToString() + "\"}";

        if (setResCustomers(Newtonsoft.Json.JsonConvert.DeserializeObject<List<resCustjSonData>>(Customers)) != "OK")
            return string.Format("{\"retVal\":\"{0}\",\"data\":\"\",\"CustomRegID\":\"{1}\"}",
                HttpContext.GetGlobalResourceObject("MakeReservation", "CustomerDataError"),
                UserData.CustomRegID);
        string errorMsg = string.Empty;
        decimal? agencyDisPasPer = Conversion.getDecimalOrNull(Discount);

        resMain.PromoCode = PromoCode;
        resMain.HoneyMoonRes = Conversion.getBoolOrNull(Honeymoon.ToLower());
        resMain.AgencyDisPasPer = agencyDisPasPer;
        resMain.AceCustomerCode = aceCustomerCode;
        resMain.AceTfNumber = aceDosier;

        if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
        {
            HttpContext.Current.Session["ResData"] = ResData;
            if (ResData.PromoList.Count > 0)
                return "{\"retVal\":\"OK\",\"data\":\"OK\",\"CustomRegID\":\"" + UserData.CustomRegID.ToString() + "\"}";
            else
                return "{\"retVal\":\"OK\",\"data\":\"|!|\",\"CustomRegID\":\"" + UserData.CustomRegID.ToString() + "\"}";
        }
        else
            return "{\"retVal\":\"" + errorMsg + "\",\"data\":\"\",\"CustomRegID\":\"" + UserData.CustomRegID.ToString() + "\"}";
    }

    [WebMethod(EnableSession = true)]
    public static string setResCustInfo(string data)
    {
        if (HttpContext.Current.Session["UserData"] == null || string.IsNullOrEmpty(data)) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        string _data = data.Replace('<', '{').Replace('>', '}').Replace('|', '"');
        TvBo.ResCustInfojSon resCustInfojSon = Newtonsoft.Json.JsonConvert.DeserializeObject<TvBo.ResCustInfojSon>(_data);

        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == Conversion.getInt32OrNull(resCustInfojSon.CustNo));
        List<ResCustInfoRecord> _ResCustInfo = ResData.ResCustInfo;

        ResCustInfoRecord resCustInfoRec = new ResCustInfoRecord();
        if (ResData.ResCustInfo.Count > 0 && _ResCustInfo.Find(f => f.CustNo == resCust.CustNo) != null)
        {
            resCustInfoRec = _ResCustInfo.Find(f => f.CustNo == resCust.CustNo);
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                resCustInfoRec.AddrHomeTel = string.IsNullOrEmpty(resCust.Phone) ? strFunc.Trim(resCust.Phone, ' ') : resCustInfoRec.AddrHomeTel;
            else
                resCustInfoRec.MobTel = string.IsNullOrEmpty(resCust.Phone) ? strFunc.Trim(resCust.Phone, ' ') : resCustInfoRec.MobTel;
        }
        else
        {
            resCustInfoRec.CustNo = resCust.CustNo;
            resCustInfoRec.CTitle = resCust.Title;
            resCustInfoRec.RecID = _ResCustInfo.Count > 0 ? _ResCustInfo.LastOrDefault().RecID + 1 : 1;
            resCustInfoRec.RecordID = resCustInfoRec.RecID;
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                resCustInfoRec.AddrHomeTel = string.IsNullOrEmpty(resCust.Phone) ? strFunc.Trim(resCust.Phone, ' ') : resCustInfoRec.AddrHomeTel;
            else
                resCustInfoRec.MobTel = string.IsNullOrEmpty(resCust.Phone) ? strFunc.Trim(resCust.Phone, ' ') : resCustInfoRec.MobTel;
            IntCountryListRecord country = new TvBo.Common().getCountryIntCode(UserData.Market, UserData.Country, ref errorMsg);
            resCustInfoRec.AddrHomeCountryCode = country != null ? country.IntCode : string.Empty;
            resCustInfoRec.MemTable = false;

            _ResCustInfo.Add(resCustInfoRec);
        }
        resCustInfoRec.CTitle = Conversion.getInt16OrNull(resCustInfojSon.CTitle);
        resCustInfoRec.CName = resCustInfojSon.CName;
        resCustInfoRec.CSurName = resCustInfojSon.CSurName;
        resCustInfoRec.MobTel = resCustInfojSon.MobTel.Trim(' ');
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            resCust.Phone = string.IsNullOrEmpty(resCustInfojSon.AddrHomeTel) ? resCust.Phone : resCustInfojSon.AddrHomeTel;
        else
            resCust.Phone = string.IsNullOrEmpty(resCustInfojSon.MobTel) ? resCust.Phone : resCustInfojSon.MobTel;
        resCustInfoRec.ContactAddr = resCustInfojSon.ContactAddr;
        resCustInfoRec.InvoiceAddr = resCustInfojSon.InvoiceAddr;
        resCustInfoRec.AddrHome = resCustInfojSon.AddrHome;
        resCustInfoRec.AddrHomeCity = resCustInfojSon.AddrHomeCity;
        resCustInfoRec.AddrHomeCountry = resCustInfojSon.AddrHomeCountry;
        resCustInfoRec.AddrHomeCountryCode = resCustInfojSon.AddrHomeCountryCode;
        resCustInfoRec.AddrHomeZip = resCustInfojSon.AddrHomeZip;
        resCustInfoRec.AddrHomeTel = strFunc.Trim(resCustInfojSon.AddrHomeTel, ' ');
        resCustInfoRec.AddrHomeFax = strFunc.Trim(resCustInfojSon.AddrHomeFax, ' ');
        resCustInfoRec.AddrHomeEmail = resCustInfojSon.AddrHomeEmail;
        resCustInfoRec.HomeTaxOffice = resCustInfojSon.HomeTaxOffice;
        resCustInfoRec.HomeTaxAccNo = resCustInfojSon.HomeTaxAccNo;
        resCustInfoRec.WorkFirmName = resCustInfojSon.WorkFirmName;
        resCustInfoRec.AddrWork = resCustInfojSon.AddrWork;
        resCustInfoRec.AddrWorkCity = resCustInfojSon.AddrWorkCity;
        resCustInfoRec.AddrWorkCountry = resCustInfojSon.AddrWorkCountry;
        resCustInfoRec.AddrWorkCountryCode = resCustInfojSon.AddrWorkCountryCode;
        resCustInfoRec.AddrWorkZip = resCustInfojSon.AddrWorkZip;
        resCustInfoRec.AddrWorkTel = strFunc.Trim(resCustInfojSon.AddrWorkTel, ' ');
        resCustInfoRec.AddrWorkFax = strFunc.Trim(resCustInfojSon.AddrWorkFax, ' ');
        resCustInfoRec.AddrWorkEMail = resCustInfojSon.AddrWorkEMail;
        resCustInfoRec.WorkTaxOffice = resCustInfojSon.WorkTaxOffice;
        resCustInfoRec.WorkTaxAccNo = resCustInfojSon.WorkTaxAccNo;
        resCustInfoRec.Bank = resCustInfojSon.Bank;
        resCustInfoRec.BankAccNo = resCustInfojSon.BankAccNo;
        resCustInfoRec.BankIBAN = resCustInfojSon.BankIBAN;

        HttpContext.Current.Session["ResData"] = ResData;

        return "OK";
    }

    [WebMethod(EnableSession = true)]
    public static string setResCustomer(resCustjSonData data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        List<ResCustRecord> resCust = ResData.ResCust;
        string nameRules = new UICommon().nameWrittingRuleValue(UserData, NameWrittingRuleTypes.nameWrittingRules);
        string surnameRules = new UICommon().nameWrittingRuleValue(UserData, NameWrittingRuleTypes.surnameWrittingRules);

        string dateSeperator = UserData.Ci.DateTimeFormat.DateSeparator.Replace(" ", "");

        resCustjSonData cust = new resCustjSonData();
        //data = data.Replace('|', '"').Replace('[', '{').Replace(']', '}').Replace('<', '{').Replace('>', '}');
        //string[] _data = data.Split('@');

        cust = data;
        ResCustRecord _cust = resCust.Find(f => f.SeqNo == cust.SeqNo);
        TitleRecord _title = ResData.Title.Find(f => f.TitleNo == cust.Title);
        _cust.Title = cust.Title;
        _cust.TitleStr = _title != null ? _title.Code : "";
        _cust.Surname = cust.Surname;
        if (!string.IsNullOrEmpty(_cust.Surname))
        {
            if (surnameRules == "uppercase")
                _cust.Surname = _cust.Surname.ToUpper();
            else if (surnameRules == "capitalize")
            {
                _cust.Surname = _cust.Surname.ToLower();
                string FirstChar = _cust.Surname[0].ToString();
                _cust.Surname = _cust.Surname.Remove(0, 1);
                _cust.Surname = FirstChar + _cust.Surname;
            }
        }
        _cust.SurnameL = cust.SurnameL;
        _cust.Name = cust.Name;
        if (!string.IsNullOrEmpty(_cust.Name))
        {
            if (nameRules == "uppercase")
                _cust.Name = _cust.Name.ToUpper();
            else if (nameRules == "capitalize")
            {
                _cust.Name = _cust.Name.ToLower();
                string FirstChar = _cust.Name[0].ToString();
                _cust.Name = _cust.Name.Remove(0, 1);
                _cust.Name = FirstChar + _cust.Name;
            }
        }
        _cust.NameL = cust.NameL;
        DateTime dCustBDay;
        DateTime.TryParse(cust.Birtday, UserData.Ci, DateTimeStyles.None, out dCustBDay);
        _cust.Birtday = dCustBDay;
        //_cust.Birtday = Conversion.convertDateTime(cust.Birtday.Replace("/", dateSeperator).Replace(".", dateSeperator).Replace("-", dateSeperator),
        //                                            strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' '));
        _cust.Age = Conversion.getInt16OrNull(cust.Age);
        _cust.IDNo = cust.IDNo;
        _cust.PassSerie = cust.PassSerie;
        _cust.PassNo = cust.PassNo;
        _cust.PassIssueDate = Conversion.getDateTimeOrNull(cust.PassIssueDate);
        _cust.PassExpDate = Conversion.getDateTimeOrNull(cust.PassExpDate);
        _cust.PassGiven = cust.PassGiven;
        _cust.Phone = cust.Phone;

        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
        {
            if (!string.IsNullOrEmpty(cust.Nationality))
                _cust.Nationality = cust.Nationality;
        }
        else
        {
            if (cust.Nation.HasValue)
                _cust.Nation = cust.Nation;
        }

        _cust.HasPassport = cust.Passport;
        _cust.Leader = cust.Leader.HasValue ? (cust.Leader.Value ? "Y" : "N") : "N";


        HttpContext.Current.Session["ResData"] = ResData;

        return "OK";
    }

    [WebMethod(EnableSession = true)]
    public static string setResCustomers(List<resCustjSonData> data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        List<ResCustRecord> resCust = ResData.ResCust;
        string nameRules = new UICommon().nameWrittingRuleValue(UserData, NameWrittingRuleTypes.nameWrittingRules);
        string surnameRules = new UICommon().nameWrittingRuleValue(UserData, NameWrittingRuleTypes.surnameWrittingRules);
        string dateSeperator = UserData.Ci.DateTimeFormat.DateSeparator.Replace(" ", "");

        resCustjSonData cust = new resCustjSonData();
        //data = data.Replace('|', '"').Replace('[', '{').Replace(']', '}').Replace('<', '{').Replace('>', '}');
        //string[] _data = data.Split('@');
        for (int i = 0; i < data.Count; i++)
        {
            cust = data[i];
            ResCustRecord _cust = resCust.Find(f => f.SeqNo == cust.SeqNo);
            TitleRecord _title = ResData.Title.Find(f => f.TitleNo == cust.Title);
            _cust.Title = cust.Title;
            _cust.TitleStr = _title != null ? _title.Code : "";
            _cust.Surname = cust.Surname;
            if (!string.IsNullOrEmpty(_cust.Surname))
            {
                if (surnameRules == "uppercase")
                    _cust.Surname = _cust.Surname.ToUpper();
                else if (surnameRules == "capitalize")
                {
                    _cust.Surname = _cust.Surname.ToLower();
                    string FirstChar = _cust.Surname[0].ToString();
                    _cust.Surname = _cust.Surname.Remove(0, 1);
                    _cust.Surname = FirstChar + _cust.Surname;
                }
            }
            _cust.SurnameL = cust.SurnameL;
            _cust.Name = cust.Name;
            if (!string.IsNullOrEmpty(_cust.Name))
            {
                if (nameRules == "uppercase")
                    _cust.Name = _cust.Name.ToUpper();
                else if (nameRules == "capitalize")
                {
                    _cust.Name = _cust.Name.ToLower();
                    string FirstChar = _cust.Name[0].ToString();
                    _cust.Name = _cust.Name.Remove(0, 1);
                    _cust.Name = FirstChar + _cust.Name;
                }
            }
            _cust.NameL = cust.NameL;
            //_cust.Birtday = Conversion.convertDateTime(cust.Birtday.Replace("/", dateSeperator).Replace(".", dateSeperator).Replace("-", dateSeperator),strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' '));
            DateTime dCustBDay;
            DateTime.TryParse(cust.Birtday, UserData.Ci, DateTimeStyles.None, out dCustBDay);
            _cust.Birtday = dCustBDay;
            _cust.Age = Conversion.getInt16OrNull(cust.Age);
            _cust.IDNo = cust.IDNo;
            _cust.PassSerie = cust.PassSerie;
            _cust.PassNo = cust.PassNo;
            _cust.PassIssueDate = Conversion.getDateTimeOrNull(cust.PassIssueDate);
            _cust.PassExpDate = Conversion.getDateTimeOrNull(cust.PassExpDate);
            _cust.PassGiven = cust.PassGiven;
            _cust.Phone = cust.Phone;

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
            {
                if (!string.IsNullOrEmpty(cust.Nationality))
                    _cust.Nationality = cust.Nationality;
            }
            else
            {
                if (cust.Nation.HasValue)
                    _cust.Nation = cust.Nation;
            }

            _cust.HasPassport = cust.Passport;
            _cust.Leader = cust.Leader.HasValue ? (cust.Leader.Value ? "Y" : "N") : "N";
        }

        HttpContext.Current.Session["ResData"] = ResData;

        return "OK";
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static resMainDivRecord getResMainDiv()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        resMainDivRecord retval = new resMainDivRecord();
        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        bool? resNote = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ResNote"));
        bool? showAgencyComView = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyComView"));

        bool showAgencyCom = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyCom")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyCom")).Value : true;
        bool showAgencyEB = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyEB")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyEB")).Value : true;
        bool showPassengerEB = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showPassengerEB")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showPassengerEB")).Value : true;

        if (!Users.checkCurrentUserOperator(TvBo.Common.crID_Anex))
        {
            #region for Anex
            if (showAgencyComView.HasValue && (UserData.ShowAllRes || showAgencyComView.Value))
            {
                showAgencyCom = true;
                showAgencyEB = true;
                showPassengerEB = true;
            }
            #endregion
        }
        else
        {
            if (showAgencyComView.HasValue && showAgencyComView.Value)
            {
                showAgencyCom = true;
                showAgencyEB = true;
                showPassengerEB = true;
            }
        }

        StringBuilder sb = new StringBuilder();
        #region ResMain
        sb.Append("<div class=\"ui-helper-clearfix ui-widget ui-widget-content resMainCss\">");
        sb.AppendFormat("<div class=\"Caption\"><strong>{0}: </strong></div>", HttpContext.GetGlobalResourceObject("MakeReservation", "resMainAgency"));
        sb.AppendFormat("<div class=\"Field\"><span>{0}</span></div>", ResData.ResMain.AgencyNameL);
        sb.AppendFormat("<div class=\"Caption\"><strong>{0}: </strong></div>", HttpContext.GetGlobalResourceObject("MakeReservation", "resMainAgencyuser"));
        sb.AppendFormat("<div class=\"Field\"><span>{0}</span></div>", ResData.ResMain.AgencyUserNameL);
        sb.Append("</div>");
        #endregion

        #region Payment
        sb.Append("<div class=\"ui-widget ui-widget-content paymentCss\">");
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
        {
            sb.AppendFormat("<br /><strong>{0}: </strong>{1} {2}<br />",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentSalePrice"),
                    ResData.ResMain.PasPayable.HasValue ? ResData.ResMain.PasPayable.Value.ToString("#,###.00") : "&nbsp",
                    ResData.ResMain.SaleCur);
        }
        else
        {
            sb.Append("<div class=\"ui-helper-clearfix row\">");
            sb.AppendFormat("<div class=\"ui-priority-primary Caption\"><strong>{0}: </strong></div>",
                   HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentCurrency"));
            sb.AppendFormat("<div style=\"text-align:left; float: left; width: 100px; height: 25px; padding-left: 4px; padding-top: 4px;\"><span style=\"font-size: 130%; font-style:italic;\"><b>{0}</b></span></div>", ResData.ResMain.SaleCur);
            sb.Append("</div>");
            sb.Append("<div class=\"ui-helper-clearfix row\">");
            sb.AppendFormat("<div class=\"ui-priority-primary Caption\"><strong>{0}: </strong></div>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentSalePrice"));
            sb.AppendFormat("<div class=\"Field ui-state-active\"><span><b>{0}</b></span></div>", ResData.ResMain.SalePrice.HasValue ? ResData.ResMain.SalePrice.Value.ToString("#,###.00") : "&nbsp");
            if (showPassengerEB)
            {
                sb.AppendFormat("<div class=\"ui-priority-primary Caption\"><strong>{0}: </strong></div>",
                        HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentPassEB"));
                sb.AppendFormat("<div class=\"Field ui-state-active\"><span>{0}</span></div>", ResData.ResMain.EBPas.HasValue && !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Elsenal) ? ResData.ResMain.EBPas.Value.ToString("#,###.00") : "&nbsp");
            }
            sb.AppendFormat("<div class=\"ui-priority-primary Caption\" style=\"width:150px;\"><strong>{0}: </strong></div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentPassAmountToPay"));
            sb.AppendFormat("<div class=\"Field ui-state-active\"><span>{0}</span></div>",
                ResData.ResMain.PasPayable.HasValue ? ResData.ResMain.PasPayable.Value.ToString("#,###.00") : "&nbsp");
            if (showAgencyCom)
            {
                sb.AppendFormat("<div class=\"ui-priority-primary Caption\"><strong>{0}: </strong></div>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentAgencyCom"));
                decimal agencyCom = (ResData.ResMain.AgencyCom.HasValue ? ResData.ResMain.AgencyCom.Value : 0) + (ResData.ResMain.AgencyComSup.HasValue ? ResData.ResMain.AgencyComSup.Value : 0) - (ResData.ResMain.AgencyDisPasVal.HasValue ? ResData.ResMain.AgencyDisPasVal.Value : 0);

                //decimal agencyCom = (ResData.ResMain.AgencyCom.HasValue ? ResData.ResMain.AgencyCom.Value : (decimal)0) +
                //                    (ResData.ResMain.AgencyComSup.HasValue ? ResData.ResMain.AgencyComSup.Value : (decimal)0);
                sb.AppendFormat("<div class=\"Field ui-state-active\"><span>{0}</span></div>",
                    agencyCom > 0 ? agencyCom.ToString("#,###.00") : "&nbsp");
            }
            sb.Append("</div>");
            sb.Append("<div class=\"row\">");
            sb.AppendFormat("<div class=\"ui-priority-primary Caption\"><strong>{0}: </strong></div>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentSupDis"));
            sb.AppendFormat("<div class=\"Field ui-state-active\"><span>{0}</span></div>", ResData.ResMain.Discount.HasValue ? ResData.ResMain.Discount.Value.ToString("#,###.00") : "&nbsp");
            if (showAgencyEB)
            {
                sb.AppendFormat("<div class=\"ui-priority-primary Caption\"><strong>{0}: </strong></div>",
                        HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentAgencyEB"));
                sb.AppendFormat("<div class=\"Field ui-state-active\"><span>{0}</span></div>", ResData.ResMain.EBAgency.HasValue && !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Elsenal) ? ResData.ResMain.EBAgency.Value.ToString("#,###.00") : "&nbsp");
            }
            if (showAgencyCom)
                if (!Users.checkCurrentUserOperator(TvBo.Common.crID_Azur))
                {
                    sb.AppendFormat("<div class=\"ui-priority-primary Caption\" style=\"width:150px;\"><strong>{0}</strong></div>",
                        !Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && !Equals(UserData.Market, "SWEMAR") ? HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentAgencyAmountToPay") + ": " : "&nbsp;");
                    sb.AppendFormat("<div class=\"Field ui-state-active\"><span>{0}</span></div>",
                        !Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && !Equals(UserData.Market, "SWEMAR") ? (ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") : "&nbsp") : "&nbsp;");
                }
            if (showAgencyCom)
                if (!Users.checkCurrentUserOperator(TvBo.Common.crID_Azur))
                {
                    sb.AppendFormat("<div class=\"ui-priority-primary Caption\"><strong>{0}</strong></div>",
                        !Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && !Equals(UserData.Market, "SWEMAR") ? HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentBalance") + ": " : "&nbsp;");
                    sb.AppendFormat("<div class=\"Field ui-state-active\"><span><b>{0}</b></span></div>",
                        !Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && !Equals(UserData.Market, "SWEMAR") ? (ResData.ResMain.Balance.HasValue ? ResData.ResMain.Balance.Value.ToString("#,###.00") : "&nbsp") : "&nbsp;");
                }
            sb.Append("</div>");
        }
        sb.Append("</div>");
        #endregion

        #region Status
        sb.Append("<div class=\"ui-helper-clearfix statusCss\">");
        sb.AppendFormat("<div class=\"Caption\"><strong>{0}: </strong>{1}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentReservationStatus"),
                ResData.ResMain.ResStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + ResData.ResMain.ResStat.Value.ToString()).ToString() : "&nbsp;");
        sb.AppendFormat("<div class=\"Caption\"><strong>{0}: </strong>{1}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentConfirmation"),
                ResData.ResMain.ConfStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + ResData.ResMain.ConfStat.Value.ToString()).ToString() : "&nbsp;");
        sb.Append("<div class=\"Caption\"><strong>&nbsp;</strong></div>");
        sb.Append("<div class=\"Caption\"><strong>&nbsp;</strong></div>");
        sb.Append("</div>");

        #endregion

        #region Bonus
        if (UserData.Bonus.BonusAgencySeeOwnW || UserData.Bonus.BonusUserSeeOwnW)
        {
            sb.Append("<div class=\"divBonus\">");
            sb.Append("<table><tr>");
            sb.AppendFormat("<td><strong>{0}</strong></td>", HttpContext.GetGlobalResourceObject("Bonus", "BLBonus"));
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td></tr>");
            UserBonusTotalRecord agencyBonus = new Bonus().getBonus(ResData.ResMain.BegDate, ResData.ResMain.SaleCur, "A", UserData.AgencyID, ref errorMsg);
            if (agencyBonus != null && UserData.Bonus.BonusAgencySeeOwnW)
            {
                sb.Append("<tr>");
                sb.AppendFormat("<td> {0}</td>",
                    HttpContext.GetGlobalResourceObject("Bonus", "lblOnYourAgency").ToString() + "&nbsp;&nbsp;<b>&nbsp;" + (agencyBonus.UseableBonus.HasValue ? agencyBonus.UseableBonus.Value.ToString("#,###.00") : "0") + "</b>");
                if (UserData.Bonus.AgencyBonus)
                    sb.AppendFormat("<td>{0}</td><td><input id=\"agencyBonus\" type=\"text\" /></td>",
                        HttpContext.GetGlobalResourceObject("Bonus", "lblUseBonusAmount").ToString());
                sb.Append("</tr>");
            }
            UserBonusTotalRecord userBonus = new Bonus().getBonus(ResData.ResMain.BegDate, ResData.ResMain.SaleCur, "U", UserData.PIN, ref errorMsg);
            if (userBonus != null && UserData.Bonus.BonusUserSeeOwnW)
            {
                sb.Append("<tr>");
                sb.AppendFormat("<td> {0}</td>",
                    HttpContext.GetGlobalResourceObject("Bonus", "lblOnYourAccount").ToString() + "&nbsp;&nbsp;<b>&nbsp;" + (userBonus.UseableBonus.HasValue ? userBonus.UseableBonus.Value.ToString("#,###.00") : "0") + "</b>");
                if (UserData.Bonus.UserBonus)
                    sb.AppendFormat("<td>{0}</td><td><input id=\"userBonus\" type=\"text\" /></td>",
                        HttpContext.GetGlobalResourceObject("Bonus", "lblUseBonusAmount").ToString());
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            sb.Append("</div>");
        }

        #endregion

        #region Save Option
        string saveOptionStr = string.Empty;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Orex) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr))
            saveOptionStr = string.Format("<input id=\"iSaveOption\" type=\"checkbox\" {1} /><label for=\"iSaveOption\"><b>{0}</b></label>",
                                            HttpContext.GetGlobalResourceObject("MakeReservation", "saveOption"),
                                            !(string.Equals(UserData.CustomRegID, TvBo.Common.crID_Orex) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr)) || (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) && UserData.OwnAgency) ? "" : "checked=\"checked\"");
        else
            //if ((string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ||
            //     string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka)) && ResData.ResMain.OptDate.HasValue)
            // TVB2B-3913 ile güncellenmiştir.
            if (ResData.ResMain.OptDate.HasValue)
        {
            string optionTimeMsg = string.Format(HttpContext.GetGlobalResourceObject("MakeReservation", "optionTimeWithOptionNT").ToString(),
                                    ResData.ResMain.OptDate.Value.ToShortDateString() + " " + ResData.ResMain.OptDate.Value.ToShortTimeString());
            saveOptionStr = string.Format("<label for=\"withOption\">{0}</label><input id=\"withOption\" type=\"checkbox\" name=\"optionTime\" onclick=\"onClickOptionTime()\" /><br /><br /><span id=\"withOptionMsg\" style=\"font-color: Red; display: none;\"><b>{1}</b></span>",
                                    optionTimeMsg,
                                    (UserData.TvParams.TvParamSystem.TVCCOptionDate.HasValue && UserData.TvParams.TvParamSystem.TVCCOptionDate.Value ? HttpContext.GetGlobalResourceObject("MakeReservation", "novaBeforeSaveMessage"):""));
        }

        #endregion

        string promotionStatus = Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) ||
                                 Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ||
                                 Equals(UserData.CustomRegID, TvBo.Common.crID_FilipTravel) ||
                                 Equals(UserData.CustomRegID, TvBo.Common.crID_HolidayPlus) ? "1" : "0";
        retval.data = sb.ToString();
        retval.statusPromotion = promotionStatus;
        retval.divACE = UserData.AgencyRec != null && UserData.AgencyRec.AceExport;
        retval.divDiscountAgencyCom = (Equals(UserData.CustomRegID, Common.crID_KidyTour) || Equals(UserData.CustomRegID, TvBo.Common.ctID_TivronaTours) || Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || Equals(UserData.CustomRegID, TvBo.Common.crID_Go2Holiday) || Equals(UserData.CustomRegID, TvBo.Common.crID_Go2HolidayBg)) && UserData.AgencyCanDisCom;
        retval.divWhereInvoice = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr);
        retval.divSaveOption = string.Equals(UserData.CustomRegID, TvBo.Common.crID_HolidayPlus) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Orex) || ((string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_SunFun) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka)) && ResData.ResMain.OptDate.HasValue);
        retval.divSaveOptionDiv = saveOptionStr;
        retval.divResNote = (resNote.HasValue && resNote.Value);
        retval.ResNote = ResData.ResMain.ResNote;
        retval.ResNoteOpt = string.Empty;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Karavan))
        {
            if (ResData.ResService.Where(w => w.ServiceType == "HOTEL").Count() > 0)
            {
                string resNoteOptStr = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ResNoteOpt"));
                if (!string.IsNullOrEmpty(resNoteOptStr))
                {
                    StringBuilder rNote = new StringBuilder();
                    List<ResNoteOpt> resNoteList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ResNoteOpt>>(resNoteOptStr);
                    ResNoteOpt noteList = resNoteList.Find(f => f.Market == UserData.Market);
                    if (noteList != null)
                    {
                        foreach (ResNoteOptDetail row in noteList.Details.Where(w => w.Visible))
                        {
                            rNote.AppendFormat("<input id=\"resNote{0}\" name=\"resNoteOpt\" type=\"checkbox\" onclick=\"resNoteOptChange()\" value=\"{1}\"/><label for=\"resNote{0}\">{1}</label><br /> \n", row.Code, row.Descrition);
                        }
                        if (rNote.Length > 0)
                            retval.ResNoteOpt = rNote.ToString();
                    }
                }
            }
        }
        retval.divBonusID = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex) && string.Equals(ResData.ResMain.Market, "ANEXRU");
        retval.CustomRegID = UserData.CustomRegID;
        retval.PromoCode = Conversion.getStrOrNull(ResData.ResMain.PromoCode);

        retval.showArrivalInformation = false;
        retval.arrivalAirPorts = null;// query;

        string _phoneMask = string.Empty;
        string _mobPhoneMask = string.Empty;
        if (UserData.PhoneMask != null)
        {
            _phoneMask = UserData.PhoneMask.PhoneMask != null ? UserData.PhoneMask.PhoneMask.Replace('#', '9') : "";
            _mobPhoneMask = UserData.PhoneMask.MobilPhoneMask != null ? UserData.PhoneMask.MobilPhoneMask.Replace('#', '9') : "";
        }
        retval.mobPhoneMask = _mobPhoneMask;
        retval.phoneMask = _phoneMask;
        retval.showSpecialCode = string.Equals(UserData.CustomRegID, Common.crID_FilipTravel);
        return retval;
    }

    public static object getObjectCustInfo(TvBo.ResCustInfoRecord row, string fieldName)
    {
        System.Reflection.PropertyInfo[] oProps = null;
        oProps = row.GetType().GetProperties();
        List<System.Reflection.PropertyInfo> _pi = (from q in oProps
                                                    where q.Name == fieldName
                                                    select q).ToList<System.Reflection.PropertyInfo>();
        object value = null;
        if (_pi != null && _pi.Count() > 0)
        {
            System.Reflection.PropertyInfo pi = _pi[0];
            value = pi.GetValue(row, null) == null ? DBNull.Value : pi.GetValue(row, null);
        }
        return value;
    }

    public static string getResCustInfoImport(User UserData, ResDataRecord ResData, int? resCustNo, int? cnt, bool visable, string tmpCustInfoEdit, ref string errorMsg)
    {
        if (string.IsNullOrEmpty(tmpCustInfoEdit))
            return string.Empty;

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        ResCustRecord cust = ResData.ResCust.Find(f => f.CustNo == resCustNo);
        ResCustInfoRecord custInfo = ResData.ResCustInfo.Find(f => f.CustNo == resCustNo);
        if (custInfo == null)
            custInfo = new ResCustInfoRecord();
        int lastPosition = 0;
        string retVal = string.Empty;
        string tmpTemplate = tmpCustInfoEdit;
        List<IntCountryListRecord> countryList = new TvBo.Common().getCountryList(UserData, ref errorMsg);
        countryList = countryList.OrderBy(o => o.CountryNameL).ThenBy(t => t.CountryName).ToList();
        List<Location> LocationList = CacheObjects.getLocationList(UserData.Market);
        Location country = LocationList.Find(f => f.RecID == UserData.Country);
        string countryListStr = string.Empty;
        string selectedCountryCode = string.Empty;
        foreach (IntCountryListRecord row in countryList)
        {
            string selectStr = string.Empty;
            if (Equals(custInfo.AddrHomeCountry, row.CountryName) || Equals(custInfo.AddrHomeCountryCode, row.IntCode))
            {
                selectStr = "selected=\"selected\"";
                selectedCountryCode = row.CountryPhoneCode;
            }
            else if (Equals(row.IntCode, country != null ? country.CountryCode.ToString() : ""))
            {
                selectStr = "selected=\"selected\"";
                selectedCountryCode = row.CountryPhoneCode;
            }
            countryListStr += string.Format("<option value='{0}' {2} IntPhoneCode='{3}'>{1}</option>",
                                                useLocalName ? row.CountryNameL : row.CountryName,
                                                useLocalName ? row.CountryNameL : row.CountryName,
                                                selectStr,
                                                row.CountryPhoneCode);
        }
        bool exit = true;
        while (exit)
        {
            string valueTemp = string.Empty;
            int first = tmpTemplate.IndexOf("{[");
            if (first > -1)
            {
                lastPosition = first;
                tmpTemplate = tmpTemplate.Remove(0, first + 2);
                int last = tmpTemplate.IndexOf("]}");
                valueTemp = tmpTemplate.Substring(0, last);
                tmpTemplate = tmpTemplate.Remove(0, last + 2);
                string[] local = valueTemp.Trim().Split(',');
                if (local.Length > 1)
                {
                    string classKey = local[0].Trim('"');
                    string resourceKey = local[1].Trim('"');
                    string localStr = HttpContext.GetGlobalResourceObject(classKey, resourceKey).ToString();
                    tmpCustInfoEdit = tmpCustInfoEdit.Replace("{[" + valueTemp + "]}", localStr);
                }
            }
            else
                exit = false;
        }
        tmpTemplate = tmpCustInfoEdit;
        exit = true;
        while (exit)
        {
            string valueTemp = string.Empty;
            int first = tmpTemplate.IndexOf("{");
            if (first > -1)
            {
                lastPosition = first;
                tmpTemplate = tmpTemplate.Remove(0, first + 1);
                int last = tmpTemplate.IndexOf("}");
                valueTemp = tmpTemplate.Substring(0, last);
                valueTemp = valueTemp.Trim('"');
                tmpTemplate = tmpTemplate.Remove(0, last + 1);

                string valueStr = string.Empty;

                if (string.Equals("MobTel", valueTemp))
                {
                    object value = getObjectCustInfo(custInfo, valueTemp);
                    valueStr = Conversion.getObjectToString(value, value.GetType());
                    IntCountryListRecord intCodeRecord = countryList.Find(f => f.CountryPhoneCode == custInfo.AddrHomeCountryCode);
                    if (intCodeRecord != null && !string.IsNullOrEmpty(intCodeRecord.CountryPhoneCode) && valueStr.IndexOf(intCodeRecord.CountryPhoneCode) > -1)
                    {
                        valueStr = valueStr.Trim(' ').Replace(intCodeRecord.CountryPhoneCode.Trim(' '), "").Trim(' ');
                    }
                }
                else
                    if (string.Equals("AddrHomeTel", valueTemp))
                {
                    object value = getObjectCustInfo(custInfo, valueTemp);
                    valueStr = Conversion.getObjectToString(value, value.GetType());
                    IntCountryListRecord intCodeRecord = countryList.Find(f => f.CountryPhoneCode == custInfo.AddrHomeCountryCode);
                    if (intCodeRecord != null && !string.IsNullOrEmpty(intCodeRecord.CountryPhoneCode) && valueStr.IndexOf(intCodeRecord.CountryPhoneCode) > -1)
                    {
                        valueStr = valueStr.Trim(' ').Replace(intCodeRecord.CountryPhoneCode.Trim(' '), "").Trim(' ');
                    }
                }
                else
                        if (string.Equals("AddrHomeFax", valueTemp))
                {
                    object value = getObjectCustInfo(custInfo, valueTemp);
                    valueStr = Conversion.getObjectToString(value, value.GetType());
                    IntCountryListRecord intCodeRecord = countryList.Find(f => f.CountryPhoneCode == custInfo.AddrHomeCountryCode);
                    if (intCodeRecord != null && !string.IsNullOrEmpty(intCodeRecord.CountryPhoneCode) && valueStr.IndexOf(intCodeRecord.CountryPhoneCode) > -1)
                    {
                        valueStr = valueStr.Trim(' ').Replace(intCodeRecord.CountryPhoneCode.Trim(' '), "").Trim(' ');
                    }
                }
                else
                            if (valueTemp.IndexOf('.') > -1)
                {
                    if (Equals(valueTemp, "UserData.Ci.LCID"))
                        valueStr = UserData.Ci.LCID.ToString();
                }
                else
                                if (valueTemp.IndexOf('-') > -1)
                {
                    string[] valuelist = valueTemp.Split('-');
                    List<TvTools.objectList> objectValues = new List<objectList>();
                    Type _type = typeof(System.String);
                    for (int i = 0; i < valuelist.Length; i++)
                    {
                        object value = getObjectCustInfo(custInfo, valuelist[i]);
                        objectValues.Add(new TvTools.objectList
                        {
                            TypeName = value.GetType().Name,
                            Value = value
                        });
                    }
                    object obj = mathLib.returnFormulaMinus(objectValues, ref _type);
                    valueStr = Conversion.getObjectToString(obj, _type);
                }
                else
                                    if (valueTemp.IndexOf('+') > -1)
                {
                    string[] valuelist = valueTemp.Split('+');
                    List<TvTools.objectList> objectValues = new List<objectList>();
                    Type _type = typeof(System.String);
                    for (int i = 0; i < valuelist.Length; i++)
                    {
                        object value = getObjectCustInfo(custInfo, valuelist[i]);
                        objectValues.Add(new TvTools.objectList
                        {
                            TypeName = value.GetType().Name,
                            Value = value
                        });
                    }
                    object obj = mathLib.returnFormulaPlus(objectValues, ref _type);
                    valueStr = Conversion.getObjectToString(obj, _type);
                }
                else
                {
                    if (string.Equals("VisableElement", valueTemp))
                    {
                        if (!visable)
                            valueStr = "elementHidden";
                    }
                    else
                        if (string.Equals("AddrHomeCountryCode", valueTemp))
                    {
                        valueStr = countryListStr;
                    }
                    else
                            if (string.Equals("MobTelCountryCode", valueTemp))
                    {
                        valueStr = "+" + selectedCountryCode;
                    }
                    else
                                if (string.Equals("AddrHomeTelCountryCode", valueTemp))
                    {
                        valueStr = "+" + selectedCountryCode;
                    }
                    else
                                    if (string.Equals("AddrHomeFaxCountryCode", valueTemp))
                    {
                        valueStr = "+" + selectedCountryCode;
                    }
                    else
                    {
                        object value = getObjectCustInfo(custInfo, valueTemp);
                        valueStr = Conversion.getObjectToString(value, value.GetType());
                    }
                }
                tmpCustInfoEdit = tmpCustInfoEdit.Replace("{[]}", custInfo.CustNo.ToString());
                tmpCustInfoEdit = tmpCustInfoEdit.Replace("[]", cnt.ToString());
                tmpCustInfoEdit = tmpCustInfoEdit.Replace("{\"" + valueTemp + "\"}", valueStr);
            }
            else
                exit = false;
        }
        return tmpCustInfoEdit;
    }

    public static string getResCustInfoTemplate(User UserData)
    {
        string sb = string.Empty;
        string filePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + new UICommon().getWebID() + "\\" + UserData.Market + "\\ResCustInfoEdit.tmpl";
        if (System.IO.File.Exists(filePath))
        {
            System.IO.StreamReader Tex = new System.IO.StreamReader(filePath);
            sb = Tex.ReadToEnd();
            Tex.Close();
        }
        return sb;
    }

    public static string getResCustDivTmp(User UserData, ResDataRecord ResData, string tmpCustEdit, ref string errorMsg)
    {
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        int lastPosition = 0;
        string retVal = string.Empty;
        string tmpTemplate = tmpCustEdit;

        string dateMaskA = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ');
        string[] dateMaskB = dateMaskA.Split(UserData.Ci.DateTimeFormat.DateSeparator[0]);
        string _dateMask = string.Empty;
        string _dateFormat = string.Empty;
        foreach (string row in dateMaskB)
            if (!string.IsNullOrEmpty(row.ToString()))
                switch (row[0].ToString().ToLower())
                {
                    case "d":
                        _dateMask += "/99";
                        _dateFormat += "/dd";
                        break;
                    case "m":
                        _dateMask += "/99";
                        _dateFormat += "/MM";
                        break;
                    case "y":
                        _dateMask += "/9999";
                        _dateFormat += "/yyyy";
                        break;
                    default:
                        break;
                }
        _dateMask = _dateMask.Remove(0, 1);
        _dateFormat = _dateFormat.Remove(0, 1);
        string dateFormat = new TvBo.Common().getDateFormat(UserData.Ci);
        string _phoneMask = string.Empty;
        if (UserData.PhoneMask != null)
            _phoneMask = UserData.PhoneMask.MobilPhoneMask != null ? UserData.PhoneMask.MobilPhoneMask.Replace('#', '9') : "";

        List<string> SrrcServiceTypeList = new Ssrcs().getSsrcServiceTypes(UserData.Market, ResData.ResMain.PLMarket, ref errorMsg);
        string tmpCustInfo = getResCustInfoTemplate(UserData);
        string _dateFormatRegion = new TvBo.Common().getDateFormatRegion(UserData.Ci);
        tmpCustEdit = tmpCustEdit.Replace("{\"dateFormatRegion\"}", _dateFormatRegion);

        bool seachCust = new Agency().getRole(UserData, 504);

        bool exit = true;
        while (exit)
        {
            string valueTemp = string.Empty;
            int first = tmpTemplate.IndexOf("[{");
            if (first > -1)
            {
                lastPosition = first;
                tmpTemplate = tmpTemplate.Remove(0, first + 2);
                int last = tmpTemplate.IndexOf("}]");
                valueTemp = tmpTemplate.Substring(0, last);
                tmpTemplate = tmpTemplate.Remove(0, last + 2);
                string[] local = valueTemp.Trim().Split(',');
                if (local.Length > 1)
                {
                    string classKey = local[0].Trim('\"');
                    string resourceKey = local[1].Trim('\"');
                    string localStr = HttpContext.GetGlobalResourceObject(classKey, resourceKey).ToString();
                    tmpCustEdit = tmpCustEdit.Replace("[{" + valueTemp + "}]", localStr);
                }
            }
            else
                exit = false;
        }

        tmpTemplate = tmpCustEdit;

        string tmpLoopSection = tmpCustEdit.Substring(tmpCustEdit.IndexOf("{loop"));
        tmpLoopSection = tmpLoopSection.Substring(5, tmpLoopSection.IndexOf("loop}") - 5);
        string loopSection = string.Empty;
        Int16 cnt = 0;
        List<DDLData> nations = CacheObjects.getNations(UserData.Market);
        List<Nationality> nationalityList = CacheObjects.getNationality(UserData);
        bool? defaultCountrySelected = Conversion.getBoolOrNull(new Common().getFormConfigValue("MakeRes", "ResCustDefaultCountrySelected"));
        List<TitleAgeRecord> title = ResData.TitleCust;

        foreach (ResCustRecord row in ResData.ResCust)
        {
            string nationList = string.Empty;
            if (defaultCountrySelected.HasValue && defaultCountrySelected.Value)
                nationList = string.Format("<option value=\"\" selected=\"selected\">{0}</option>", HttpContext.GetGlobalResourceObject("LibraryResource", "ComboSelect").ToString());
            else
                nationList = "";

            //if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
            //    foreach (Nationality nRow in nationalityList)
            //        nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.Code3,(notSelectDefaultNationality.HasValue && !notSelectDefaultNationality.Value) && Equals(nRow.Code3, row.Nationality) ? "selected=\"selected\"" : "", nRow.Name);
            //else
            //    foreach (DDLData nRow in nations)
            //        nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.FieldData, Equals(nRow.FieldData, row.Nation.ToString()) ? "selected=\"selected\"" : "", nRow.TextData);

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
                foreach (Nationality nRow in nationalityList.OrderBy(o => o.OrderNo).ThenBy(t => t.Name))
                    if (defaultCountrySelected.HasValue && defaultCountrySelected.Value && row.IDNo==null)
                        nationList += string.Format("<option value=\"{0}\">{1}</option>", nRow.Code3, nRow.Name);
                    else
                        nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.Code3, Equals(nRow.Code3, row.Nationality) ? "selected=\"selected\"" : "", nRow.Name);
            else
                foreach (DDLData nRow in nations)
                    nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.FieldData, Equals(nRow.FieldData, row.Nation.ToString()) ? "selected=\"selected\"" : "", nRow.TextData);


            string titleList = string.Empty;
            if (row.Title < 6)
            {
                foreach (TitleAgeRecord trow in title.Where(w => w.TitleNo < 6).Select(s => s).ToList<TitleAgeRecord>())
                    titleList += string.Format("<option value=\"{0}\" {1}>{2}</option>", trow.TitleNo, Equals(trow.TitleNo.ToString(), row.Title.ToString()) ? "selected=\"selected\"" : "", trow.Code);
            }
            else
            {
                foreach (TitleAgeRecord trow in title.Where(w => w.TitleNo > 5).Select(s => s).ToList<TitleAgeRecord>())
                    titleList += string.Format("<option value=\"{0}\" {1}>{2}</option>", trow.TitleNo, Equals(trow.TitleNo.ToString(), row.Title.ToString()) ? "selected=\"selected\"" : "", trow.Code);
            }

            cnt += 1;
            string _tmpl = tmpLoopSection;
            string tmpl = tmpLoopSection;

            exit = true;

            while (exit)
            {
                string valueTemp = string.Empty;

                int first = _tmpl.IndexOf("{\"");
                if (first > -1)
                {
                    lastPosition = first;
                    _tmpl = _tmpl.Remove(0, first + 2);
                    int last = _tmpl.IndexOf("\"}");
                    valueTemp = _tmpl.Substring(0, last);
                    _tmpl = _tmpl.Remove(0, last + 2);


                    string valueStr = string.Empty;
                    if (valueTemp.IndexOf('.') > -1)
                    {
                        if (Equals(valueTemp, "UserData.Ci.LCID"))
                            valueStr = UserData.Ci.LCID.ToString();
                    }
                    else
                    {
                        System.Reflection.PropertyInfo[] oProps = null;
                        oProps = row.GetType().GetProperties();
                        List<System.Reflection.PropertyInfo> _pi = (from q in oProps
                                                                    where q.Name == valueTemp
                                                                    select q).ToList<System.Reflection.PropertyInfo>();
                        if (_pi != null && _pi.Count() > 0)
                        {
                            System.Reflection.PropertyInfo pi = _pi[0];
                            object value = pi.GetValue(row, null) == null ? DBNull.Value : pi.GetValue(row, null);
                            valueStr = value.ToString();
                        }
                    }


                    if (Equals(valueTemp, "@import:ResCustInfo@"))
                    {
                        tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", getResCustInfoImport(UserData, ResData, row.CustNo, cnt, string.Equals(row.Leader, "Y"), tmpCustInfo, ref errorMsg));
                    }
                    else
                        if (Equals(valueTemp, "PhoneCountryCode"))
                    {
                        tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", UserData.PhoneMask != null ? UserData.PhoneMask.CountryCode : "");
                    }
                    else
                            if (Equals(valueTemp, "dateFormat"))
                    {
                        tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", dateFormat);
                    }
                    else
                                if (Equals(valueTemp, "BirtDay"))
                    {
                        string birthDate = row.Birtday.HasValue && row.Birtday.Value>DateTime.MinValue ? row.Birtday.Value.ToString(_dateFormat) : "";
                        tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", birthDate);
                    }
                    else
                                    if (Equals(valueTemp, "ResDate"))
                    {
                        tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToString(dateFormat).Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], '/') : "");
                    }
                    else
                                        if (Equals(valueTemp, "Title"))
                    {
                        tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", titleList);
                    }
                    else
                                            if (Equals(valueTemp, "Nation"))
                    {
                        tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", nationList);
                    }
                    else
                                                if (Equals(valueTemp, "Nationality"))
                    {
                        tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", nationList);
                    }
                    else
                                                    if (Equals(valueTemp, "LeaderCheck"))
                    {
                        if (row.Title < 6)
                        {
                            if (Equals(row.Leader, "Y"))
                                tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", "checked='checked'");
                            else
                                tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", " ");
                        }
                        else
                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", "disabled='disabled'");
                    }
                    else
                                                        if (Equals(valueTemp, "PassportCheck"))
                    {
                        if ((row.HasPassport.HasValue && row.HasPassport.Value) || !row.HasPassport.HasValue)
                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", "checked='checked'");
                        else
                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", " ");
                    }
                    else
                                                            if (Equals(valueTemp, "searchCustVisible"))
                    {
                        if (!seachCust)
                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", "display: none;");
                        else
                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", " ");
                    }
                    else
                        tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", valueStr);

                    tmpl = tmpl.Replace("{[]}", row.CustNo.ToString());
                    tmpl = tmpl.Replace("[]", cnt.ToString());
                }
                else
                    exit = false;
            }

            loopSection += tmpl;
        }

        tmpTemplate = tmpTemplate.Replace("{loop" + tmpLoopSection + "loop}", loopSection);
        StringBuilder sb = new StringBuilder();
        StringBuilder custDiv = new StringBuilder();

        custDiv.Append("<fieldset><legend><label>»</label>");
        custDiv.AppendFormat("<span class=\"lblGridResCust\">{0}</span></legend>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "lblGridResCust"));
        sb.Append("<div id=\"resCustGrid\">");
        List<IntCountryListRecord> countryList = new TvBo.Common().getCountryList(UserData, ref errorMsg);

        sb.AppendFormat("<input id=\"countryList\" type=\"hidden\" value='{0}' />", Newtonsoft.Json.JsonConvert.SerializeObject(countryList).Replace('#', '9'));
        sb.AppendFormat("<input id=\"hfCustCount\" type=\"hidden\" value=\"{0}\" />", ResData.ResCust.Count);
        sb.AppendFormat("<input id=\"dateMask\" type=\"hidden\" value=\"{0}\" />", _dateMask);
        sb.AppendFormat("<input id=\"phone_Mask\" type=\"hidden\" value=\"{0}\" />", _phoneMask);
        sb.Append(tmpTemplate);
        sb.Append("</div>");

        bool custSearchBtn = sb.ToString().ToLower().IndexOf("search16.png") > 0;
        bool custInfoBtn = sb.ToString().ToLower().IndexOf("info.gif") > 0;
        bool custAddressBtn = sb.ToString().ToLower().IndexOf("address.gif") > 0;
        bool custSsrcBtn = sb.ToString().ToLower().IndexOf("ssrc.png") > 0;
        if (custSearchBtn || custInfoBtn || custAddressBtn || custSsrcBtn)
        {
            custDiv.Append("<div style=\"white-space: nowrap;\">");

            if (custSearchBtn)
            {
                custDiv.Append("<img alt=\"\" src=\"Images/search16.png\" height=\"18px\" />");
                custDiv.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustLabel"));
            }
            if (custInfoBtn)
            {
                if (custSearchBtn)
                    custDiv.Append("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                custDiv.Append("<img alt=\"\" src=\"Images/info.gif\" height=\"18px\" />");
                custDiv.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("MakeReservation", "lblCustomerOtherInfo"));
            }
            if (custAddressBtn)
            {
                if (custInfoBtn || custSearchBtn)
                    custDiv.Append("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                custDiv.Append("<img alt=\"\" src=\"Images/address.gif\" height=\"18px\" />");
                custDiv.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("MakeReservation", "lblCustomerAddress"));
            }
            if (custSsrcBtn)
            {
                if (custInfoBtn || custSearchBtn || custAddressBtn)
                    custDiv.Append("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                custDiv.Append("<img alt=\"\" src=\"Images/Ssrc.png\" height=\"18px\" />");
                custDiv.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("MakeReservation", "lblSpecialServiceReqCode"));
            }
            custDiv.Append("</div><br />");
        }
        custDiv.Append(sb.ToString());
        custDiv.Append("<br /></fieldset>");
        return custDiv.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getResCustdiv()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }

        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        string tmplResCustEdit = getTemplate(UserData);

        if (!string.IsNullOrEmpty(tmplResCustEdit))
        {
            return getResCustDivTmp(UserData, ResData, tmplResCustEdit, ref errorMsg);
        }

        string dateMaskA = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ');
        string dateSeperator = UserData.Ci.DateTimeFormat.DateSeparator[0].ToString();
        if (UserData.Ci.Name.ToLower() == "lt-lt")
            dateSeperator = ".";
        string[] dateMaskB = dateMaskA.Split(Convert.ToChar(dateSeperator));
        string _dateMask = string.Empty;
        string _dateFormat = string.Empty;
        foreach (string row in dateMaskB)
            if (!string.IsNullOrEmpty(row.ToString()))
                switch (row[0].ToString().ToLower())
                {
                    case "d":
                        _dateMask += UserData.Ci.DateTimeFormat.DateSeparator[0] + "99";
                        _dateFormat += UserData.Ci.DateTimeFormat.DateSeparator[0] + "dd";
                        break;
                    case "m":
                        _dateMask += UserData.Ci.DateTimeFormat.DateSeparator[0] + "99";
                        _dateFormat += UserData.Ci.DateTimeFormat.DateSeparator[0] + "MM";
                        break;
                    case "y":
                        _dateMask += UserData.Ci.DateTimeFormat.DateSeparator[0] + "9999";
                        _dateFormat += UserData.Ci.DateTimeFormat.DateSeparator[0] + "yyyy";
                        break;
                    default:
                        break;
                }
        _dateMask = _dateMask.Remove(0, 1);
        _dateFormat = _dateFormat.Remove(0, 1);
        string dateFormat = new TvBo.Common().getDateFormat(UserData.Ci).Replace('/', UserData.Ci.DateTimeFormat.DateSeparator[0]);
        string _phoneMask = string.Empty;
        if (UserData.PhoneMask != null)
            _phoneMask = UserData.PhoneMask.MobilPhoneMask != null ? UserData.PhoneMask.MobilPhoneMask.Replace('#', '9') : "";

        List<string> SrrcServiceTypeList = new Ssrcs().getSsrcServiceTypes(UserData.Market, ResData.ResMain.PLMarket, ref errorMsg);

        string _dateFormatRegion = new TvBo.Common().getDateFormatRegion(UserData.Ci);

        bool? showPIN = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "showPIN"));
        if (UserData.TvParams.TvParamReser.NeedPIN.HasValue && UserData.TvParams.TvParamReser.NeedPIN.Value)
            showPIN = true;
        bool? showPassportInfo = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ShowPassportInfo"));
        bool? defaultCountrySelected = Conversion.getBoolOrNull(new Common().getFormConfigValue("MakeRes", "ResCustDefaultCountrySelected"));

        StringBuilder sb = new StringBuilder();
        StringBuilder custDiv = new StringBuilder();
        custDiv.Append("<fieldset><legend><label>»</label>");
        custDiv.AppendFormat("<span class=\"lblGridResCust\">{0}</span></legend>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "lblGridResCust"));

        sb.Append("<div id=\"resCustGrid\" class=\"resCustGridCss\">");
        sb.AppendFormat("<input id=\"hfCustCount\" type=\"hidden\" value=\"{0}\" />", ResData.ResCust.Count);
        sb.AppendFormat("<input id=\"dateMask\" type=\"hidden\" value=\"{0}\" />", _dateMask);
        sb.AppendFormat("<input id=\"phone_Mask\" type=\"hidden\" value=\"{0}\" />", _phoneMask);
        sb.Append("<div class=\"resCustGridCss\"><div class=\"header ui-widget-header ui-priority-secondary\">");

        sb.AppendFormat("<div class=\"SeqNoH\">{0}</div>", "&nbsp;");
        sb.AppendFormat("<div class=\"TitleH\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustTitle"));
        sb.AppendFormat("<div class=\"Surname{1}H\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustSurname"),
                !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
        sb.AppendFormat("<div class=\"Name{1}H\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustName"),
                !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
        sb.AppendFormat("<div class=\"BirthDayH\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustBirthDate").ToString() + "<br />" + _dateFormatRegion);
        sb.AppendFormat("<div class=\"AgeH\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustAge"));
        if (showPIN == null || (showPIN.HasValue && showPIN.Value))
            sb.AppendFormat("<div class=\"IdNoH\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPIN"));
        if (showPassportInfo == null || (showPassportInfo.HasValue && showPassportInfo.Value))
        {
            sb.AppendFormat("<div class=\"PassSerieH\">{0}</div>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPassSerie"));
            sb.AppendFormat("<div class=\"PassNoH\">{0}</div>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPassNo"));
            sb.AppendFormat("<div class=\"HasPassportH\">{0}</div>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPass"));
        }
        if (showPIN == null || (showPIN.HasValue && showPIN.Value))
        {
            sb.AppendFormat("<div class=\"PhoneH\">{0}</div>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPhone"));
        }
        else
        {
            string CountryCodeStr = string.Empty;
            if (UserData.PhoneMask != null)
                CountryCodeStr = !string.IsNullOrEmpty(UserData.PhoneMask.CountryCode) ? " (" + UserData.PhoneMask.CountryCode + ")" : "";
            sb.AppendFormat("<div class=\"PhoneLH\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPhone").ToString() + CountryCodeStr);
        }
        sb.AppendFormat("<div class=\"Nation{1}H\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustCitizenship"),
                !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
        sb.AppendFormat("<div class=\"LeaderH\">{0}</div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustLeader"));
        sb.AppendFormat("<div class=\"ViewEditH\">{0}</div>", "#");
        sb.Append("</div>");
        List<DDLData> nations = CacheObjects.getNations(UserData.Market);
        List<Nationality> nationalityList = CacheObjects.getNationality(UserData);
        List<TitleAgeRecord> title = ResData.TitleCust;
        Int16 cnt = 0;
        bool seachCust = new Agency().getRole(UserData, 504);
        foreach (TvBo.ResCustRecord row in ResData.ResCust)
        {
            bool Ssrcode = (from q1 in ResData.ResCon
                            join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                            join q3 in SrrcServiceTypeList on q2.ServiceType equals q3
                            where q1.CustNo == row.CustNo
                            select new { q2.RecID }).Count() > 0;

            string nationList = string.Empty;
            if (defaultCountrySelected.HasValue && defaultCountrySelected.Value)
                nationList = string.Format("<option value=\"\" selected=\"selected\">{0}</option>", HttpContext.GetGlobalResourceObject("LibraryResource", "ComboSelect").ToString());
            else
                nationList = "";

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
                foreach (Nationality nRow in nationalityList.OrderBy(o => o.OrderNo).ThenBy(t => t.Name))
                    if (defaultCountrySelected.HasValue && defaultCountrySelected.Value && row.IDNo==null)
                        nationList += string.Format("<option value=\"{0}\">{1}</option>", nRow.Code3, nRow.Name);
                    else
                        nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.Code3, Equals(nRow.Code3, row.Nationality) ? "selected=\"selected\"" : "", nRow.Name);
            else
                foreach (DDLData nRow in nations)
                    nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.FieldData, Equals(nRow.FieldData, row.Nation.ToString()) ? "selected=\"selected\"" : "", nRow.TextData);


            string titleList = string.Empty;
            if (row.Title < 6)
            {
                if (title != null)
                    foreach (TitleAgeRecord trow in title.Where(w => w.TitleNo < 6).Select(s => s).ToList<TitleAgeRecord>())
                        titleList += string.Format("<option value=\"{0}\" {1}>{2}</option>", trow.TitleNo, Equals(trow.TitleNo.ToString(), row.Title.ToString()) ? "selected=\"selected\"" : "", trow.Code);
                else
                    foreach (TitleRecord trow in ResData.Title.Where(w => w.TitleNo < 6).Select(s => s).ToList<TitleRecord>())
                        titleList += string.Format("<option value=\"{0}\" {1}>{2}</option>", trow.TitleNo, Equals(trow.TitleNo.ToString(), row.Title.ToString()) ? "selected=\"selected\"" : "", trow.Code);
            }
            else
            {
                if (title != null)
                    foreach (TitleAgeRecord trow in title.Where(w => w.TitleNo > 5).Select(s => s).ToList<TitleAgeRecord>())
                        titleList += string.Format("<option value=\"{0}\" {1}>{2}</option>", trow.TitleNo, Equals(trow.TitleNo.ToString(), row.Title.ToString()) ? "selected=\"selected\"" : "", trow.Code);
                else
                    foreach (TitleRecord trow in ResData.Title.Where(w => w.TitleNo > 5).Select(s => s).ToList<TitleRecord>())
                        titleList += string.Format("<option value=\"{0}\" {1}>{2}</option>", trow.TitleNo, Equals(trow.TitleNo.ToString(), row.Title.ToString()) ? "selected=\"selected\"" : "", trow.Code);
            }
            cnt++;
            if (Convert.ToInt32(cnt / 2.0) == (cnt / 2.0))
                sb.Append("<div class=\"row ui-helper-clearfix\" style=\"background-color: #E2E2E2;\">");
            else
                sb.Append("<div class=\"row ui-helper-clearfix\">");
            sb.AppendFormat("<div class=\"SeqNo floatLeft\"><span id=\"iSeqNo{0}\">{1}</span></div>", cnt, cnt);
            sb.AppendFormat("<div class=\"Title floatLeft\"><select id=\"iTitle{0}\">{1}</select></div>", cnt, titleList);
            sb.AppendFormat("<div class=\"Surname{0} floatLeft\">", !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
            sb.AppendFormat("<input id=\"iSurname{0}\" type=\"text\" value=\"{1}\" maxlength=\"30\" style=\"width:96%; {2}\" onkeypress=\"return isNonUniCodeChar(event);\" />",
                cnt,
                row.Surname,
                new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.surnameWrittingRules));
            sb.AppendFormat("<input id=\"iSurnameL{0}\" type=\"hidden\" value=\"{1}\" /></div>",
                cnt,
                row.SurnameL);
            sb.AppendFormat("<div class=\"Name{0} floatLeft\">", !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
            sb.AppendFormat("<input id=\"iName{0}\" type=\"text\" value=\"{1}\" maxlength=\"30\" style=\"width:96%; {2}\" onkeypress=\"return isNonUniCodeChar(event);\" />",
                cnt,
                row.Name,
                new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.nameWrittingRules));
            sb.AppendFormat("<input id=\"iNameL{0}\" type=\"hidden\" value=\"{1}\" /></div>",
                cnt,
                row.NameL);

            string birthDate = row.Birtday.HasValue && row.Birtday>DateTime.MinValue ? row.Birtday.Value.ToString(_dateFormat) : "";

            sb.AppendFormat("<div class=\"BirthDay floatLeft\"><input id=\"iBirthDay{0}\" type=\"text\" class=\"formatDate\" style=\"width:95%;\" onblur=\"return SetAge({1},'{2}', '{3}')\" value=\"{4}\"/></div>",
                                cnt,
                                cnt,
                                ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToString(dateFormat) : "",
                                dateFormat,
                                birthDate);
            sb.AppendFormat("<div class=\"Age floatLeft\"><input id=\"iAge{0}\" type=\"text\" value=\"{1}\" style=\"width:90%;\" disabled=\"disabled\" /></div>", cnt, row.Age);
            if (showPIN == null || (showPIN.HasValue && showPIN.Value))
                sb.AppendFormat("<div class=\"IdNo floatLeft\"><input id=\"iIDNo{0}\" type=\"text\" value=\"{1}\" style=\"width:93%;\" maxlength=\"20\" /></div>", cnt, row.IDNo);

            if (!showPassportInfo.HasValue || showPassportInfo.Value)
            {
                if (string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt))
                    sb.AppendFormat("<div class=\"PassSerie floatLeft\"><select id=\"iPassSerie{0}\" style=\"width:90%;\">" +
                        "<option {1}>P</option>" +
                        "<option {2}>ID</option>" +
                        "<option {3}>OT</option>" +
                        "</select></div>", cnt, row.PassSerie.Equals("P") ? "selected" : "", row.PassSerie.Equals("ID") ? "selected" : "", row.PassSerie.Equals("OT") ? "selected" : "");
                else
                    sb.AppendFormat("<div class=\"PassSerie floatLeft\"><input id=\"iPassSerie{0}\" type=\"text\" value=\"{1}\" style=\"width:90%;\" maxlength=\"5\" /></div>", cnt, row.PassSerie);
                sb.AppendFormat("<div class=\"PassNo floatLeft\"><input id=\"iPassNo{0}\" type=\"text\" value=\"{1}\" style=\"width:93%;\" maxlength=\"10\" /></div>", cnt, row.PassNo);
                sb.AppendFormat("<div class=\"HasPassport floatLeft\"><input id=\"iHasPassport{0}\" type=\"checkbox\" {1} {2} />", cnt, (row.HasPassport.HasValue ? row.HasPassport.Value : true) ? "checked=\"checked\"" : "", row.Title < 6 ? "disabled=\"disabled\"" : "");
                sb.AppendFormat("<input id=\"iPassIssueDate{0}\" type=\"hidden\" value=\"{1}\"/>", cnt,
                                row.PassIssueDate.HasValue ? row.PassIssueDate.Value.ToString(_dateFormat) : "");
                sb.AppendFormat("<input id=\"iPassExpDate{0}\" type=\"hidden\" value=\"{1}\"/>", cnt,
                                row.PassExpDate.HasValue ? row.PassExpDate.Value.ToString(_dateFormat) : "");
                sb.AppendFormat("<input id=\"iPassGiven{0}\" type=\"hidden\" value=\"{1}\" maxlength=\"30\" />", cnt,
                                row.PassGiven);
                sb.Append("</div>");
            }
            else
            {
                sb.Append("<div style=\"display: none; visibility: hidden;\">");
                sb.AppendFormat("<input id=\"iPassSerie{0}\" type=\"hidden\" value=\"{1}\"  />", cnt, row.PassSerie);
                sb.AppendFormat("<input id=\"iPassNo{0}\" type=\"hidden\" value=\"{1}\" />", cnt, row.PassNo);
                sb.AppendFormat("<input id=\"iHasPassport{0}\" type=\"checkbox\" {1} />", cnt, cnt, (row.HasPassport.HasValue ? row.HasPassport.Value : true) ? "checked=\"checked\"" : "");
                sb.AppendFormat("<input id=\"iPassIssueDate{0}\" type=\"hidden\" value=\"{1}\"/>", cnt, "");
                sb.AppendFormat("<input id=\"iPassExpDate{0}\" type=\"hidden\" value=\"{1}\"/>", cnt, "");
                sb.AppendFormat("<input id=\"iPassGiven{0}\" type=\"hidden\" value=\"{1}\" maxlength=\"30\" />", cnt, row.PassGiven);
                sb.Append("</div>");
            }

            if (showPIN == null || (showPIN.HasValue && showPIN.Value))
                sb.AppendFormat("<div class=\"Phone floatLeft\"><input id=\"iPhone{0}\" type=\"text\" value=\"{1}\" style=\"width:90%;\" maxlength=\"15\" onkeypress=\"return isPhoneChar(event);\" class=\"mobPhone\" /></div>",
                                    cnt,
                                    string.IsNullOrEmpty(row.Phone) ? row.Phone : strFunc.Trim(row.Phone, ' '));
            else
                sb.AppendFormat("<div class=\"PhoneL floatLeft\"><input id=\"iPhone{0}\" type=\"text\" value=\"{1}\" style=\"width:98%;\" maxlength=\"15\" onkeypress=\"return isPhoneChar(event);\" class=\"mobPhone\" /></div>",
                                   cnt,
                                   string.IsNullOrEmpty(row.Phone) ? row.Phone : strFunc.Trim(row.Phone, ' '));

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
                sb.AppendFormat("<div class=\"Nation{2} floatLeft\"><select id=\"iNationality{0}\">{1}</select></div>", cnt, nationList, !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
            else
                sb.AppendFormat("<div class=\"Nation{2} floatLeft\"><select id=\"iNation{0}\">{1}</select></div>", cnt, nationList, !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");

            if (row.Title < 6)
                sb.AppendFormat("<div class=\"Leader floatLeft\"><input id=\"iLeader{0}\" type=\"radio\" name=\"Leader\" value=\"{1}\" {2} /></div>", cnt, cnt, Equals(row.Leader, "Y") ? "checked=\"checked\"" : "");
            else
                sb.Append("<div class=\"Leader floatLeft\">&nbsp;</div>");
            string searchStr = string.Format("<img alt=\"{0}\" src=\"Images/search16.png\" onclick=\"searchCust({1})\" />", HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustLabel"), cnt);
            string infoStr = string.Format("<img alt=\"{0}\" src=\"Images/info.gif\" onclick=\"showResCustInfo({1})\" />", HttpContext.GetGlobalResourceObject("MakeReservation", "lblCustomerOtherInfo"), row.CustNo);
            string addrInfoStr = string.Format("<img alt=\"{0}\" src=\"Images/address.gif\" onclick=\"showCustAddress({1})\" />", HttpContext.GetGlobalResourceObject("MakeReservation", "lblCustomerAddress"), row.CustNo);
            string ssrcStr = string.Format("<img alt=\"{0}\" src=\"Images/Srrc.png\" onclick=\"showSSRC({1})\" height=\"18\" />", HttpContext.GetGlobalResourceObject("MakeReservation", "lblSpecialServiceReqCode"), row.CustNo);
            string ccard = string.Format("<img id=\"cCardBtn_{1}\" name=\"cCardBtn\" title=\"{0}\" alt=\"{0}\" src=\"Images/CCard.png\" onclick=\"showCCard({1})\" height=\"18\" />",
                                    HttpContext.GetGlobalResourceObject("MakeReservation", "lblCCardApp"),
                                    row.CustNo);

            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                sb.AppendFormat("<div class=\"ViewEdit floatLeft\">{0}&nbsp;&nbsp;{1}&nbsp;&nbsp;{2}&nbsp;&nbsp;{3}</div>",
                    Equals(UserData.Market, "SWEMAR") ? "" : infoStr,
                addrInfoStr,
                Ssrcode && !Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) ? ssrcStr : "",
                seachCust ? searchStr : "");
            else
                sb.AppendFormat("<div class=\"ViewEdit floatLeft\">{0}&nbsp;{1}&nbsp;{2}&nbsp;{3}&nbsp;{4}</div>",
                    infoStr,
                    addrInfoStr,
                    Ssrcode && !Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) ? ssrcStr : "",
                    seachCust ? searchStr : "",
                    (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && row.Title < 6) ? ccard : string.Empty);
            decimal? PPPrice;
            if (row.ppPasPayable.HasValue)
            {
                PPPrice = row.ppPasPayable.Value;
            }
            else
            {
                List<ResCustPriceRecord> ppPrice = ResData.ResCustPrice.Where(w => w.CustNo == row.CustNo).Select(s => s).ToList<ResCustPriceRecord>();
                PPPrice = ppPrice.Sum(s => s.SalePrice);
            }
            string ppPriceStr = PPPrice.HasValue ? PPPrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "&nbsp;";

            sb.AppendFormat("<div class=\"Price\"><h3>{0} : {1}</h3></div>", HttpContext.GetGlobalResourceObject("MakeReservation", "lblPerPerson"), ppPriceStr);
            sb.Append("</div>");
        }
        sb.Append("</div>");
        sb.Append("</div>");
        bool custSearchBtn = sb.ToString().IndexOf("search16.png") > 0;
        bool custInfoBtn = sb.ToString().IndexOf("info.gif") > 0;
        bool custAddressBtn = sb.ToString().IndexOf("address.gif") > 0;
        bool custSsrcBtn = sb.ToString().IndexOf("Ssrc.png") > 0;
        if (custSearchBtn || custInfoBtn || custAddressBtn || custSsrcBtn)
        {
            custDiv.Append("<div style=\"white-space: nowrap;\">");

            if (custSearchBtn)
            {
                custDiv.Append("<img alt=\"\" src=\"Images/search16.png\" />");
                custDiv.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustLabel"));
            }
            if (custInfoBtn)
            {
                if (custSearchBtn)
                    custDiv.Append("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                custDiv.Append("<img alt=\"\" src=\"Images/info.gif\" />");
                custDiv.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("MakeReservation", "lblCustomerOtherInfo"));
            }
            if (custAddressBtn)
            {
                if (custInfoBtn || custSearchBtn)
                    custDiv.Append("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                custDiv.Append("<img alt=\"\" src=\"Images/address.gif\" />");
                custDiv.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("MakeReservation", "lblCustomerAddress"));
            }
            if (custSsrcBtn)
            {
                if (custInfoBtn || custSearchBtn || custAddressBtn)
                    custDiv.Append("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                custDiv.Append("<img alt=\"\" src=\"Images/Ssrc.png\" />");
                custDiv.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("MakeReservation", "lblSpecialServiceReqCode"));
            }
            custDiv.Append("</div><br />");
        }
        custDiv.Append(sb.ToString());
        custDiv.Append("<br /></fieldset>");

        return custDiv.ToString();
    }

    public static string getTemplate(User UserData)
    {
        string sb = string.Empty;
        string filePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + new UICommon().getWebID() + "\\" + UserData.Market + "\\ResCustEdit.tmpl";
        if (System.IO.File.Exists(filePath))
        {
            System.IO.StreamReader Tex = new System.IO.StreamReader(filePath);
            sb = Tex.ReadToEnd();
            Tex.Close();
        }
        return sb;
    }

    [WebMethod(EnableSession = true)]
    public static string getResServiceDiv()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        StringBuilder sbHeader = new StringBuilder();
        StringBuilder sb = new StringBuilder();
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<CatPackPlanRecord> catPackPlan = new Services().getCatPackPlan(UserData, ResData.ResMain.PriceListNo, ref errorMsg);

        bool? _serviceEdit = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ServiceEdit"));
        bool? _serviceDelete = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ServiceDelete"));

        bool? _showCancelService = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showCancelService"));
        bool showCancelService = _showCancelService.HasValue ? _showCancelService.Value : true;

        if (!_serviceEdit.HasValue)
            _serviceEdit = false;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
        {
            _serviceEdit = true;
        }
        else
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel))
            _serviceEdit = false;

        List<ServiceExtMarOpt> showExtraService = new Reservation().getServiceExtMarOpt(UserData, UserData.Market, ref errorMsg);

        ResServiceRecord lastFlight = ResData.ResService.Where(w => w.ServiceType == "FLIGHT" && string.Equals(w.IncPack, "Y")).OrderBy(o => o.BegDate).LastOrDefault();
        ResServiceRecord firstFlight = ResData.ResService.Where(w => w.ServiceType == "FLIGHT" && string.Equals(w.IncPack, "Y")).OrderBy(o => o.BegDate).FirstOrDefault();
        string lastFlightNo = lastFlight != null ? lastFlight.Service : string.Empty;
        string firstFlightNo = firstFlight != null ? firstFlight.Service : string.Empty;
        bool lastColumnHeader = false;
        int cnt = 0;
        foreach (TvBo.ResServiceRecord row in ResData.ResService)
        {
            bool showService = true;
            if (!showCancelService && row.StatSer == 2)
                showService = false;
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel))
                _serviceEdit = false;
            string ServiceDesc = string.Empty;
            switch (row.ServiceType)
            {
                case "HOTEL":
                    if (Equals(UserData.CustomRegID, TvBo.Common.crID_Magellan))
                        ServiceDesc += (useLocalName ? row.RoomNameL : row.RoomName) + "," + row.Accom + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    else
                        ServiceDesc += (useLocalName ? row.RoomNameL : row.RoomName) + "," + row.Board + " , " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "FLIGHT":
                    FlightDayRecord flight = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                    if (flight != null)
                        ServiceDesc += flight.TDepDate.HasValue ? flight.TDepDate.Value.ToShortDateString() : row.BegDate.Value.ToShortDateString();
                    else
                        ServiceDesc += row.BegDate.Value.ToShortDateString();
                    break;
                case "TRANSPORT":
                    ServiceDesc += row.BegDate.Value.ToShortDateString();
                    break;
                case "TRANSFER":
                    if (Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel))
                        _serviceEdit = true;
                    TransferRecord trf = new Transfers().getTransfer(UserData.Market, row.Service, ref errorMsg);
                    if (trf != null && string.Equals(trf.Direction, "R"))
                        ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    else
                        ServiceDesc += row.BegDate.Value.ToShortDateString();
                    break;
                case "RENTING":
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "EXCURSION":
                    ServiceDesc += row.BegDate.Value.ToShortDateString();
                    if (row.GrpPackID.HasValue)
                    {
                        ExcursionPack package = new Excursions().getExcursionPack(UserData, row.GrpPackID.Value, ref errorMsg);
                        if (package != null)
                            ServiceDesc += " " + (useLocalName ? package.NameL : package.Name);
                    }
                    else
                        if (row.ExcTimeID.HasValue)
                    {
                        ExcursionTimeTable timeTable = new Excursions().getExcursionTimes(UserData, row.ExcTimeID, ref errorMsg);
                        if (timeTable != null)
                        {
                            if (timeTable.TransFromTime.HasValue && timeTable.TransToTime.HasValue)
                            {
                                ServiceDesc += " (" + timeTable.PriceCode + " [" + timeTable.TransFromTime.Value.ToString("HH:mm") + " < - > " + timeTable.TransToTime.Value.ToString("HH:mm") + "])";
                            }
                            if (timeTable.FreeFromTime.HasValue && timeTable.FreeToTime.HasValue)
                            {
                                ServiceDesc += "<br />";
                                ServiceDesc += "<span style='font-size: 7pt;'>Free time : " + timeTable.FreeFromTime.ToString() + "-" + timeTable.FreeToTime.ToString() + "</span>";
                            }
                        }
                    }
                    break;
                case "INSURANCE":
                    if (string.Equals(UserData.CustomRegID, Common.crID_SunFun))
                        ServiceDesc += string.Empty;
                    else
                        ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "VISA":
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "HANDFEE":
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                default:
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    if (!(new Reservation().showAdditionalService(UserData, ResData.ResMain, row.ServiceType, row.Service, ref errorMsg)))
                        showService = false;
                    break;
            }
            if (showService)
            {
                cnt++;
                if (Convert.ToInt32(cnt / 2.0) == (cnt / 2.0))
                    sb.Append("<tr>");
                else
                    sb.Append("<tr>");
                sb.AppendFormat("<td style=\"width: 150px;\">{0}</td>", useLocalName ? row.ServiceTypeNameL : row.ServiceTypeName);

                bool showPickupPoint = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ||
                                       string.Equals(UserData.CustomRegID, TvBo.Common.crID_Doris) ||
                                       string.Equals(UserData.CustomRegID, TvBo.Common.crID_Emerald);
                bool? _showPickupPointFromConfig= Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ShowPickupPoint"));

                showPickupPoint = _showPickupPointFromConfig.HasValue && _showPickupPointFromConfig.Value ? true: showPickupPoint;

                if (showPickupPoint && string.Equals(row.ServiceType, "TRANSPORT"))
                {
                    List<TransportStatRecord> serviceList = new Transports().getPickups(UserData.Market, row.Service, row.DepLocation, ref errorMsg);
                    string pickupPoint = string.Empty;
                    // Kayako id=4921
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Emerald))
                    {
                        pickupPoint += string.Format("<option value=\"\" selected=\"selected\">{0}</option>",
                                           HttpContext.GetGlobalResourceObject("LibraryResource", "ComboSelect"));
                    }
                    foreach (TransportStatRecord r1 in serviceList)
                        pickupPoint += string.Format("<option value=\"{0}\" {2}>{1}</option>",
                                        r1.Location,
                                        useLocalName ? r1.LocationLocalName : r1.LocationName,
                                        // Kayako id=4921
                                        !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Emerald) && string.Equals(r1.Location, row.Pickup) ? "selected=\"selected\"" : "");
                    pickupPoint = "<select style=\"width:99%;\" onchange=\"changePickup(this.value, '" + row.RecID.ToString() + "')\">" + pickupPoint + "</select>";
                    sb.AppendFormat("<td><div style=\"float: left;\">{0}</div><div style=\"float:right; width:125px; text-align: left;\">{1}</div></td>",
                        (useLocalName ? row.ServiceNameL : row.ServiceName) + " (" + ServiceDesc + ")",
                        HttpContext.GetGlobalResourceObject("Controls", "viewPickupPoint") + " :<br />" + pickupPoint);
                    if (serviceList.Count > 0 && !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Emerald))
                    {
                        row.Pickup = serviceList.FirstOrDefault().Location;
                        row.PickupName = serviceList.FirstOrDefault().LocationName;
                        row.PickupNameL = serviceList.FirstOrDefault().LocationLocalName;
                    }
                }
                else
                    sb.AppendFormat("<td>{0}</td>", (useLocalName ? row.ServiceNameL : row.ServiceName) + " (" + ServiceDesc + ")");

                string compulsoryStr = string.Empty;
                if (row.Compulsory.HasValue && row.Compulsory.Value)
                    compulsoryStr = "<img alt=\"\" src=\"Images/accept.gif\" />";
                else
                    compulsoryStr = "&nbsp;";
                sb.AppendFormat("<td style=\"width: 60px; text-align: center;\">{0}</td>", compulsoryStr);

                string IncludePackStr = string.Empty;
                if (Equals(row.IncPack, "Y"))
                    IncludePackStr = "<img alt=\"\" src=\"Images/accept.gif\" />";
                else
                    IncludePackStr = "&nbsp;";
                sb.AppendFormat("<td style=\"width: 60px; text-align: center;\">{0}</td>", IncludePackStr);

                sb.AppendFormat("<td style=\"width: 50px; text-align: center;\">{0}</td>", row.Unit);

                object _showIncSrvPrice = new TvBo.Common().getFormConfigValue("MakeRes", "ShowIncSrvPrice");
                bool? ShowIncSrvPrice = Conversion.getBoolOrNull(_showIncSrvPrice);
                string salePrice = string.Empty;

                if (!Equals(row.IncPack, "Y") || (ShowIncSrvPrice.HasValue ? ShowIncSrvPrice.Value : false))
                    salePrice = row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") : "&nbsp;";
                else
                    salePrice = HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
                sb.AppendFormat("<td style=\"width: 90px; text-align:right;\"><b>{0}</b></td>", salePrice);

                string viewPageUrl = string.Empty;
                string editPageUrl = string.Empty;
                #region url
                switch (row.ServiceType)
                {
                    case "HOTEL":
                        editPageUrl = "RSEdit_Hotel.aspx";
                        break;
                    case "FLIGHT":
                        editPageUrl = "RSEdit_Flight.aspx";
                        break;
                    case "TRANSPORT":
                        editPageUrl = "RSEdit_Transport.aspx";
                        break;
                    case "TRANSFER":
                        editPageUrl = "RSEdit_Transfer.aspx";
                        break;
                    case "HANDFEE":
                        editPageUrl = "RSEdit_Handfee.aspx";
                        break;
                    case "EXCURSION":
                        editPageUrl = "RSEdit_Excursion.aspx";
                        break;
                    case "INSURANCE":
                        editPageUrl = "RSEdit_Insurance.aspx";
                        break;
                    case "RENTING":
                        editPageUrl = "RSEdit_Renting.aspx";
                        break;
                    case "VISA":
                        editPageUrl = "RSEdit_Visa.aspx";
                        break;
                    default:
                        editPageUrl = "RSEdit_Other.aspx";
                        break;
                }
                string editServiceUrl = string.Format("Controls/{0}?RecID={1}&NewRes=1",
                        editPageUrl,
                        row.RecID.ToString());
                #endregion
                string editBtn = string.Empty;
                string deleteBtn = string.Empty;
                if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
                {
                    #region Sunrise case
                    if (catPackPlan != null)
                    {
                        CatPackPlanRecord catPackService = catPackPlan.Find(f => string.Equals(f.Service, row.ServiceType) && f.StepNo == row.StepNo);
                        if (catPackService != null && catPackService.CanEdit.HasValue && catPackService.CanEdit.Value)
                            editBtn = string.Format("<img alt=\"{0}\" src=\"Images/edit_16.gif\" onclick=\"editResService('{1}');\" />",
                                        HttpContext.GetGlobalResourceObject("ResView", "lblEditService"),
                                        editServiceUrl);
                        if (catPackService != null && catPackService.CanDelete.HasValue && catPackService.CanDelete.Value)
                            deleteBtn = string.Format("<img alt=\"{0}\" src=\"Images/cancel.png\" onclick=\"removeResServiceMsg('{1}');\" />",
                                        HttpContext.GetGlobalResourceObject("ResView", "lblCancelService"),
                                        row.RecID);
                    }
                    else
                        if (!Equals(row.ServiceType, "HOTEL") && !Equals(row.ServiceType, "FLIGHT"))
                    {
                        if (Equals(row.ServiceType, "FLIGHT"))
                        {
                            if (!Equals(row.Service, lastFlightNo))
                                editBtn = string.Format("<img alt=\"{0}\" src=\"Images/edit_16.gif\" onclick=\"editResService('{1}');\" />",
                                    HttpContext.GetGlobalResourceObject("ResView", "lblEditService"),
                                    (_serviceEdit.HasValue && _serviceEdit.Value) ? editServiceUrl : "");
                        }
                        else
                        {
                            editBtn = string.Format("<img alt=\"{0}\" src=\"Images/edit_16.gif\" onclick=\"editResService('{1}');\" />",
                                    HttpContext.GetGlobalResourceObject("ResView", "lblEditService"),
                                    (_serviceEdit.HasValue && _serviceEdit.Value) ? editServiceUrl : "");
                        }
                    }
                    #endregion
                }
                else
                {
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) && string.Equals(row.ServiceType, "FLIGHT") && string.Equals(row.Service, firstFlightNo))
                    {
                        editBtn = string.Format("<img alt=\"{0}\" src=\"Images/refresh.png\" onclick=\"changeFlight();\" />",
                            HttpContext.GetGlobalResourceObject("ResView", "lblEditService"));
                    }
                    else
                        editBtn = string.Format("<img alt=\"{0}\" src=\"Images/edit_16.gif\" onclick=\"editResService('{1}');\" />",
                            HttpContext.GetGlobalResourceObject("ResView", "lblEditService"),
                            (_serviceEdit.HasValue && _serviceEdit.Value) ? editServiceUrl : "");
                }
                if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && catPackPlan == null)
                {
                    #region Sunrise case
                    if (!Equals(row.ServiceType, "HOTEL"))
                        if (!Equals(row.ServiceType, "INSURANCE"))
                        {
                            if (Equals(row.ServiceType, "FLIGHT"))
                            {
                                TvBo.CatalogPackRecord catalogPack = new Search().getCatalogPack(UserData, ResData.ResMain.PriceListNo, ref errorMsg);
                                bool? PLFlightClassEquals = null;
                                if (catalogPack != null && Equals(catalogPack.FlightClass, row.FlgClass) && Equals(catalogPack.PackType, "H"))
                                    PLFlightClassEquals = string.Equals(catalogPack.FlightClass, row.FlgClass);

                                if (string.Equals(row.IncPack, "N"))
                                {
                                    if (row.Compulsory.HasValue && !row.Compulsory.Value)
                                        deleteBtn = string.Format("<img alt=\"{0}\" src=\"Images/cancel.png\" onclick=\"removeResServiceMsg('{1}');\" />",
                                                                HttpContext.GetGlobalResourceObject("ResView", "lblCancelService"),
                                                                row.RecID);
                                }
                                else
                                    if (ResData.ResService.Where(w => w.ServiceType == "FLIGHT" && w.Compulsory != true).Count() > 1)
                                    if ((row.Compulsory.HasValue && !row.Compulsory.Value))
                                        deleteBtn = string.Format("<img alt=\"{0}\" src=\"Images/cancel.png\" onclick=\"removeResServiceMsg('{1}');\" />",
                                                            HttpContext.GetGlobalResourceObject("ResView", "lblCancelService"),
                                                            row.RecID);
                            }
                            else
                                if (Equals(row.ServiceType, "TRANSFER") || Equals(row.ServiceType, "VISA"))
                                deleteBtn = string.Format("<img alt=\"{0}\" src=\"Images/cancel.png\" onclick=\"removeResServiceMsg('{1}');\" />",
                                                    HttpContext.GetGlobalResourceObject("ResView", "lblCancelService"),
                                                    row.RecID);
                            else
                                    if ((row.Compulsory.HasValue && !row.Compulsory.Value) && Equals(row.IncPack, "N"))
                                deleteBtn = string.Format("<img alt=\"{0}\" src=\"Images/cancel.png\" onclick=\"removeResServiceMsg('{1}');\" />",
                                                    HttpContext.GetGlobalResourceObject("ResView", "lblCancelService"),
                                                    row.RecID);
                        }
                    #endregion
                }
                else
                    if ((row.Compulsory.HasValue && !row.Compulsory.Value) && Equals(row.IncPack, "N"))
                    deleteBtn = string.Format("<img alt=\"{0}\" src=\"Images/cancel.png\" onclick=\"removeResServiceMsg('{1}');\" />",
                                        HttpContext.GetGlobalResourceObject("ResView", "lblCancelService"),
                                        row.RecID);
                string extraBtnsStr = string.Empty;
                extraBtnsStr = string.Format("{0}&nbsp;{1}",
                                                ((_serviceEdit.HasValue && _serviceEdit.Value) || (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) && string.Equals(row.ServiceType, "FLIGHT") && string.Equals(row.Service, firstFlightNo))) ? editBtn : "",
                                                (_serviceDelete.HasValue && _serviceDelete.Value) ? deleteBtn : "");
                if (((_serviceEdit.HasValue && _serviceEdit.Value) || (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) && string.Equals(row.ServiceType, "FLIGHT") && string.Equals(row.Service, firstFlightNo))) || (_serviceDelete.HasValue && _serviceDelete.Value))
                {
                    sb.AppendFormat("<td style=\"width: 50px; text-align: left;\">{0}</td>", extraBtnsStr);
                    lastColumnHeader = true;
                }
                sb.Append("</tr>");

                List<ResServiceExtRecord> resServiceExt = ResData.ResServiceExt.Where(w => w.ServiceID == row.RecID).Select(s => s).ToList<ResServiceExtRecord>();

                List<ResServiceExtRecord> showResServiceExt = (from q in resServiceExt
                                                               where !showExtraService.Where(w => !w.ShowInResDetB2B.Value).Select(s => s.ServiceCode).Contains(q.ExtService)
                                                               select q).ToList<ResServiceExtRecord>();

                if (showResServiceExt != null && showResServiceExt.Count > 0)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>&nbsp;</td>");
                    if ((_serviceEdit.HasValue && _serviceEdit.Value) || (_serviceDelete.HasValue && _serviceDelete.Value))
                        sb.Append("<td colspan=\"6\" style=\"vertical-align: top;\">");
                    else
                        sb.Append("<td colspan=\"5\" style=\"vertical-align: top;\">");

                    sb.AppendFormat("<table id=\"extraservice{0}\" class=\"extratotal\" cellpadding=\"0\" cellspacing=\"0\">", row.RecID.ToString() + "_" + row.RecID.ToString());
                    sb.AppendFormat("<tr><td class=\"ui-widget-header\"><strong>{0}</strong></td>",
                            HttpContext.GetGlobalResourceObject("MakeReservation", "lblExtraService"));
                    sb.AppendFormat("<td class=\"ui-widget-header\" style=\"text-align:right; width: 125px; height: 100%;\"><input type=\"button\" value=\"{0}\" onClick=\"showExtServiceDetail('{1}');\" class=\"down ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" style=\"height: 100%;\" /></td>",
                            HttpContext.GetGlobalResourceObject("MakeReservation", "resSerExtShowdetail"),
                            row.RecID.ToString() + "_" + row.RecID.ToString());
                    sb.Append("<td class=\"ui-widget-header\" style=\"width: 60px;\">&nbsp;</td>");
                    sb.Append("<td class=\"ui-widget-header\" style=\"width: 60px;\">&nbsp;</td>");
                    sb.Append("<td class=\"ui-widget-header\" style=\"width: 50px;\">&nbsp;</td>");
                    decimal? totalExt = (from q in resServiceExt
                                         where q.IncPack != "Y" || (ShowIncSrvPrice.HasValue ? ShowIncSrvPrice.Value : false)
                                         select new { salePrice = q.SalePrice }).Sum(s => s.salePrice);
                    sb.AppendFormat("<td class=\"ui-widget-header\" style=\"width: 90px; text-align: right;\"><b>{0}</b></td>", totalExt.HasValue && (ShowIncSrvPrice.HasValue ? ShowIncSrvPrice.Value : false) ? totalExt.Value.ToString("#,###.00") : "&nbsp;");
                    if ((_serviceEdit.HasValue && _serviceEdit.Value) || (_serviceDelete.HasValue && _serviceDelete.Value))
                        sb.Append("<td class=\"ui-widget-header\" style=\"width: 50px;\">&nbsp;</td>");
                    sb.Append("</tr></table>");

                    sb.AppendFormat("<table id=\"extraserviceDetail{0}\" class=\"extradetail\" cellpadding=\"0\" cellspacing=\"0\">", row.RecID.ToString() + "_" + row.RecID.ToString());
                    sb.Append("<tr style=\"background:#888; color:#FFF;\"><td class=\"ui-widget-header\">");
                    sb.AppendFormat("<div class=\"showdetaildiv\"><strong>{0}</strong>",
                            HttpContext.GetGlobalResourceObject("MakeReservation", "resSerExtDesc"));
                    sb.AppendFormat("<input type=\"button\" value=\"{0}\" onClick=\"hideExtServiceDetail('{1}');\" class=\"up ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" style=\"height: 100%;\" /></div></td>",
                            HttpContext.GetGlobalResourceObject("MakeReservation", "resSerExtHidedetail"),
                            row.RecID.ToString() + "_" + row.RecID.ToString());
                    sb.AppendFormat("<td class=\"ui-widget-header\" style=\"width: 60px; text-align: center;\"><strong>{0}</strong></td>",
                            HttpContext.GetGlobalResourceObject("MakeReservation", "resSerExtComp"));
                    sb.AppendFormat("<td class=\"ui-widget-header\" style=\"width: 60px; text-align: center;\"><strong>{0}</strong></td>",
                            HttpContext.GetGlobalResourceObject("MakeReservation", "resSerExtIncPack"));
                    sb.AppendFormat("<td class=\"ui-widget-header\" style=\"width: 50px; text-align: center;\"><strong>{0}</strong></td>",
                            HttpContext.GetGlobalResourceObject("MakeReservation", "resSerExtUnit"));
                    sb.AppendFormat("<td class=\"ui-widget-header\" style=\"width: 90px; text-align: right;\"><strong>{0}</strong></td>",
                            HttpContext.GetGlobalResourceObject("MakeReservation", "resSerExtPrice"));
                    if ((_serviceEdit.HasValue && _serviceEdit.Value) || (_serviceDelete.HasValue && _serviceDelete.Value))
                        sb.Append("<td class=\"ui-widget-header\" style=\"width: 50px;\">");
                    sb.Append("</tr>");

                    foreach (TvBo.ResServiceExtRecord r in showResServiceExt)
                    {
                        string extraServiceDesc = string.Empty;
                        DateTime? begDate = r.BegDate;
                        if (string.Equals(r.ServiceType, "FLIGHT"))
                        {
                            FlightDayRecord flight = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                            if (flight != null)
                                begDate = flight.TDepDate.HasValue ? flight.TDepDate : r.BegDate;
                            else
                                begDate = r.BegDate;
                        }
                        if (Equals(r.BegDate, r.EndDate))
                            extraServiceDesc = " (" + (begDate.HasValue ? begDate.Value.ToShortDateString() : "") + ")";
                        else
                            extraServiceDesc = " (" + (begDate.HasValue ? begDate.Value.ToShortDateString() : "") + " - " + (r.EndDate.HasValue ? r.EndDate.Value.ToShortDateString() : "") + ")";
                        sb.AppendFormat("<tr><td>{0}</td>", (useLocalName ? r.ExtServiceNameL : r.ExtServiceName) + extraServiceDesc);
                        compulsoryStr = Equals(r.Compulsory, "Y") ? "<img alt=\"\" src=\"Images/accept.gif\" />" : "&nbsp;";
                        sb.AppendFormat("<td style=\"width: 60px; text-align: center;\">{0}</td>", compulsoryStr);
                        IncludePackStr = Equals(r.IncPack, "Y") ? "<img alt=\"\" src=\"Images/accept.gif\" />" : "&nbsp;";
                        sb.AppendFormat("<td style=\"width: 60px; text-align: center;\">{0}</td>", IncludePackStr);
                        sb.AppendFormat("<td style=\"width: 50px; text-align: center;\">{0}</td>", r.Unit);
                        if (!Equals(r.IncPack, "Y") || (ShowIncSrvPrice.HasValue ? ShowIncSrvPrice.Value : false))
                            salePrice = r.SalePrice.HasValue ? r.SalePrice.Value.ToString("#,###.00") : "&nbsp;";
                        else
                            salePrice = "&nbsp;";
                        sb.AppendFormat("<td style=\"width: 90px; text-align: right;\">{0}</td>", salePrice);

                        editBtn = string.Empty;
                        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && Equals(row.ServiceType, "HOTEL"))
                            editBtn = "";
                        else
                            if (!Equals(r.Compulsory, "N") && !Equals(r.IncPack, "N"))
                            editBtn = string.Format("<img alt=\"{0}\" src=\"Images/edit_16.gif\" />", HttpContext.GetGlobalResourceObject("ResView", "lblEditService"));
                        deleteBtn = "";
                        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
                        {
                            if (!Equals(r.Compulsory, "Y"))
                                deleteBtn = string.Format("<img alt=\"{0}\" src=\"Images/cancel.png\" onclick=\"removeResServiceExtMsg('{1}');\" />",
                                    HttpContext.GetGlobalResourceObject("ResView", "lblCancelService"),
                                    r.RecID);
                        }
                        else
                            if (Equals(r.Compulsory, "N") && Equals(r.IncPack, "N"))
                            deleteBtn = string.Format("<img alt=\"{0}\" src=\"Images/cancel.png\" onclick=\"removeResServiceExtMsg('{1}');\" />",
                                    HttpContext.GetGlobalResourceObject("ResView", "lblCancelService"),
                                    r.RecID);
                        extraBtnsStr = string.Empty;
                        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
                            extraBtnsStr = string.Format("{0}&nbsp;{1}", /*editBtn*/ "", deleteBtn);
                        else
                            extraBtnsStr = string.Format("{0}&nbsp;{1}", /*_serviceEdit.HasValue && _serviceEdit.Value ?*/ editBtn /*: ""*/, /*_serviceDelete.HasValue && _serviceDelete.Value ?*/ deleteBtn /*: ""*/);
                        //if ((_serviceEdit.HasValue && _serviceEdit.Value) || (_serviceDelete.HasValue && _serviceDelete.Value))
                        sb.AppendFormat("<td style=\"width: 50px; text-align: left;\">{0}</td>", extraBtnsStr);
                        //}
                        sb.Append("</tr>");
                    }
                    sb.Append("</table>");
                    sb.Append("<td>");
                    sb.Append("</tr>");
                }
            }
        }
        sb.Append("</table>");
        sb.Append("</div>");
        sb.Append("<div id=\"divServiceMenu\">");

        sbHeader.Append("<fieldset><legend><label>»</label>");
        sbHeader.AppendFormat("<span class=\"lblGridResCust\">{0}</span></legend>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "lblGridService"));

        bool cancelBtnShow = sb.ToString().ToLower().IndexOf("cancel.png") > 0;
        bool editBtnShow = sb.ToString().ToLower().IndexOf("edit_16.gif") > 0;
        bool changeFlightBtnShow = sb.ToString().ToLower().IndexOf("refresh.png") > 0;
        if (cancelBtnShow || editBtnShow || changeFlightBtnShow)
        {
            sbHeader.Append("<div style=\"white-space: nowrap;\">");
            if (cancelBtnShow)
            {
                sbHeader.Append("<img alt=\"\" src=\"Images/cancel.png\" height=\"18px\" />");
                sbHeader.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("ResView", "lblCancelService"));
            }
            if (editBtnShow)
            {
                if (cancelBtnShow)
                    sbHeader.Append("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                sbHeader.Append("<img alt=\"\" src=\"Images/edit_16.gif\" height=\"18px\" />");
                sbHeader.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("ResView", "lblEditService"));
            }
            if (changeFlightBtnShow)
            {
                if (editBtnShow || cancelBtnShow)
                    sbHeader.Append("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                sbHeader.Append("<img alt=\"\" src=\"Images/refresh.png\" height=\"18px\" />");
                sbHeader.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("ResView", "lblEditService"));
            }
            sbHeader.Append("</div><br />");
        }
        //sbHeader.Append("<br />");        
        sbHeader.Append("<div id=\"gridResServiceDiv\" style=\"padding-left: 1%; padding-right: 1%; text-align: left;\">");
        sbHeader.Append("<table style=\"width: 100%; border-collapse: collapse; font-family: Arial; color: #333333\" id=\"gridResService\" border=\"0\" cellSpacing=\"0\" cellPadding=\"3\">");
        sbHeader.Append("<tr>");
        sbHeader.AppendFormat("<td class=\"ui-widget-header\" style=\"width: 150px;\"><strong>{0}</strong></td>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resSerServiceType"));
        sbHeader.AppendFormat("<td class=\"ui-widget-header\"><strong>{0}</strong></td>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resSerServiceDesc"));
        sbHeader.AppendFormat("<td class=\"ui-widget-header\" style=\"width: 60px; text-align: center;\"><strong>{0}</strong></td>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resSerComp"));
        sbHeader.AppendFormat("<td class=\"ui-widget-header\" style=\"width: 60px; text-align: center;\"><strong>{0}</strong></td>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resSerIncPack"));
        sbHeader.AppendFormat("<td class=\"ui-widget-header\" style=\"width: 50px; text-align: center;\"><strong>{0}</strong></td>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resSerUnit"));
        sbHeader.AppendFormat("<td class=\"ui-widget-header\" style=\"width: 90px; text-align:right;\"><strong>{0}</strong></td>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resSerPrice"));
        if (lastColumnHeader)
            sbHeader.AppendFormat("<td class=\"ui-widget-header\" style=\"width: 50px; text-align:right;\"><strong>{0}</strong></td>", "&nbsp;");
        sbHeader.Append("</tr>");

        sb.Append(DrawButtons());
        sb.Append("<div>");
        sb.Append("</fieldset>");

        HttpContext.Current.Session["ResData"] = ResData;
        return sbHeader.ToString() + sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string setCustomers(List<resCustjSonDataV2> data)
    {
        if (HttpContext.Current.Session["UserData"] == null || /*string.IsNullOrEmpty(data)*/ data == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);
        List<ResCustRecord> resCustList = ResData.ResCust;
        List<ResCustInfoRecord> resCustInfoList = ResData.ResCustInfo;
        try
        {
            resCustjSonDataV2 cust = new resCustjSonDataV2();
            for (int i = 0; i < data.Count; i++)
            {
                cust = data[i];

                ResCustRecord _cust = resCustList.Find(f => f.SeqNo == cust.SeqNo);
                TitleRecord _title = ResData.Title.Find(f => f.TitleNo == cust.Title);
                _cust.Title = cust.Title;
                _cust.TitleStr = _title != null ? _title.Code : "";
                _cust.Surname = cust.Surname.Trim();
                _cust.Name = cust.Name.Trim();
                _cust.Birtday = Conversion.getDateTimeOrNull(cust.Birtday);
                _cust.Age = TvBo.Common.getAge(_cust.Birtday, UserData.TvParams.TvParamReser.AgeCalcType > 2 ? ResData.ResMain.EndDate : ResData.ResMain.BegDate);
                _cust.IDNo = cust.IDNo;
                _cust.PassSerie = cust.PassSerie;
                _cust.PassNo = cust.PassNo;
                _cust.PassExpDate = Conversion.getDateTimeOrNull(cust.PassExpDate);
                _cust.PassIssueDate = Conversion.getDateTimeOrNull(cust.PassIssueDate);
                _cust.PassGiven = cust.PassGiven;
                _cust.Phone = cust.Phone;

                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
                {
                    if (!string.IsNullOrEmpty(cust.Nationality))
                        _cust.Nationality = cust.Nationality;
                }
                else
                {
                    if (cust.Nation.HasValue)
                        _cust.Nation = cust.Nation;
                }
                _cust.HasPassport = cust.Passport;
                //if (cust.ResCustInfo == null) cust.ResCustInfo = new ResCustInfojSonV2();
                if ((cust.Leader.HasValue && cust.Leader.Value) || !string.IsNullOrEmpty(_cust.Phone))
                {
                    bool importetResCust = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) ||
                                           string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ||
                                           string.Equals(UserData.CustomRegID, TvBo.Common.crID_Dallas);

                    if (cust.ResCustInfo == null)
                        cust.ResCustInfo = new ResCustInfojSonV2();

                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                    {
                        if (string.IsNullOrEmpty(strFunc.Trim(cust.ResCustInfo.AddrHomeTel, ' ')))
                            cust.ResCustInfo.AddrHomeTel = _cust.Phone;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(strFunc.Trim(cust.ResCustInfo.MobTel, ' ')))
                            cust.ResCustInfo.MobTel = _cust.Phone;
                    }

                    ResCustInfoRecord custInfo = resCustInfoList.Find(f => f.CustNo == _cust.CustNo);
                    if (custInfo == null)
                    {
                        custInfo = new ResCustInfoRecord();
                        custInfo.CustNo = _cust.CustNo;
                        custInfo.CTitle = _cust.Title;
                        IntCountryListRecord country = new TvBo.Common().getCountryIntCode(UserData.Market, UserData.Country, ref errorMsg);
                        custInfo.AddrHomeCountryCode = country != null ? country.IntCode : string.Empty;

                        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                            custInfo.AddrHomeTel = _cust.Phone;
                        else
                            custInfo.MobTel = _cust.Phone;
                        custInfo.MemTable = false;

                        resCustInfoList.Add(custInfo);
                    }

                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                    {
                        custInfo.AddrHomeTel = string.IsNullOrEmpty(cust.Phone) ? strFunc.Trim(cust.Phone, ' ') : cust.ResCustInfo.AddrHomeTel;
                        if (string.IsNullOrEmpty(_cust.Phone))
                            _cust.Phone = cust.ResCustInfo.AddrHomeTel;
                    }
                    else
                    {
                        custInfo.MobTel = string.IsNullOrEmpty(cust.Phone) ? strFunc.Trim(cust.Phone, ' ') : cust.ResCustInfo.MobTel;
                        if (string.IsNullOrEmpty(_cust.Phone))
                            _cust.Phone = cust.ResCustInfo.MobTel;
                    }

                    if (importetResCust)
                    {
                        custInfo.CTitle = Conversion.getInt16OrNull(cust.ResCustInfo.CTitle);
                        custInfo.CName = cust.ResCustInfo.CName.Trim();
                        custInfo.CSurName = cust.ResCustInfo.CSurName.Trim();
                        custInfo.MobTel = cust.ResCustInfo.MobTel.Trim(' ');

                        custInfo.ContactAddr = cust.ResCustInfo.ContactAddr;
                        custInfo.InvoiceAddr = cust.ResCustInfo.InvoiceAddr;
                        custInfo.AddrHome = cust.ResCustInfo.AddrHome;
                        custInfo.AddrHomeCity = cust.ResCustInfo.AddrHomeCity;
                        custInfo.AddrHomeCountry = cust.ResCustInfo.AddrHomeCountry;
                        if (cust.ResCustInfo.AddrHomeCountryCode != null)
                            custInfo.AddrHomeCountryCode = cust.ResCustInfo.AddrHomeCountryCode;
                        custInfo.AddrHomeZip = cust.ResCustInfo.AddrHomeZip;
                        custInfo.AddrHomeTel = strFunc.Trim(cust.ResCustInfo.AddrHomeTel, ' ');
                        custInfo.AddrHomeFax = strFunc.Trim(cust.ResCustInfo.AddrHomeFax, ' ');
                        custInfo.AddrHomeEmail = cust.ResCustInfo.AddrHomeEmail;
                        custInfo.HomeTaxOffice = cust.ResCustInfo.HomeTaxOffice;
                        custInfo.HomeTaxAccNo = cust.ResCustInfo.HomeTaxAccNo;
                        custInfo.WorkFirmName = cust.ResCustInfo.WorkFirmName;
                        custInfo.AddrWork = cust.ResCustInfo.AddrWork;
                        custInfo.AddrWorkCity = cust.ResCustInfo.AddrWorkCity;
                        custInfo.AddrWorkCountry = cust.ResCustInfo.AddrWorkCountry;
                        if (cust.ResCustInfo.AddrWorkCountryCode != null)
                            custInfo.AddrWorkCountryCode = cust.ResCustInfo.AddrWorkCountryCode;
                        custInfo.AddrWorkZip = cust.ResCustInfo.AddrWorkZip;
                        custInfo.AddrWorkTel = strFunc.Trim(cust.ResCustInfo.AddrWorkTel, ' ');
                        custInfo.AddrWorkFax = strFunc.Trim(cust.ResCustInfo.AddrWorkFax, ' ');
                        custInfo.AddrWorkEMail = cust.ResCustInfo.AddrWorkEMail;
                        custInfo.WorkTaxOffice = cust.ResCustInfo.WorkTaxOffice;
                        custInfo.WorkTaxAccNo = cust.ResCustInfo.WorkTaxAccNo;
                        custInfo.Bank = cust.ResCustInfo.Bank;
                        custInfo.BankAccNo = cust.ResCustInfo.BankAccNo;
                        custInfo.BankIBAN = cust.ResCustInfo.BankIBAN;
                    }
                }
            }

            if (resCustList.Where(w => w.Title > 5 && !w.Birtday.HasValue).Count() > 0)
            {
                return HttpContext.GetGlobalResourceObject("LibraryResource", "EnterChildOrInfantBirtDay").ToString();
            }

            if (ResData.ExtrasData != null)
            {
                ResData.ExtrasData.ResCust = ResData.ResCust;
                ResData.ExtrasData.ResCustInfo = ResData.ResCustInfo;
            }
            HttpContext.Current.Session["ResData"] = ResData;
            return "OK";
        }
        catch
        {
            return "NO";
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object serviceControl()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Emerald))
            return true;

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();
        ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        List<ResServiceRecord> transports = ResData.ResService.Where(w => w.ServiceType == "TRANSPORT").ToList<ResServiceRecord>();
        if (transports != null && transports.Count > 0)
        {
            foreach (ResServiceRecord row in transports)
            {
                if (!row.Pickup.HasValue)
                {
                    return false;
                }
            }
        }
        return true;
    }

    [WebMethod(EnableSession = true)]
    public static string saveReservation(List<resCustjSonDataV2> data, string SelectedPromo, string AgencyBonus, string UserBonus, string invoiceTo, bool? saveOption, string bonusID, string optionTime, bool goAhead)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        List<ReservastionSaveErrorRecord> returnData = new List<ReservastionSaveErrorRecord>();
        string checkCustomer = setCustomers(data);
        if (!string.IsNullOrEmpty(checkCustomer) && !checkCustomer.Equals("OK"))
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = checkCustomer
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }



        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        ResMainRecord resMain = ResData.ResMain;
        resMain.OptDate = null;


        bool saveAsDraft = false;
        bool? setOptionTime = null;


        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_SunFun))
        {
            if (string.Equals(optionTime, "1"))
                setOptionTime = true;
            else
                setOptionTime = false;
        }

        if (saveOption.HasValue)
            saveAsDraft = saveOption.Value;

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr))
        {
            if (Conversion.getByteOrNull(invoiceTo).HasValue)
                resMain.InvoiceTo = Conversion.getByteOrNull(invoiceTo).Value;
        }

        #region Selected promo
        if (!string.IsNullOrEmpty(SelectedPromo))
        {
            List<ResPromoRecord> resPromo = new List<ResPromoRecord>();
            string[] promoList = SelectedPromo.Split(';');
            var query = from q1 in ResData.PromoList
                        join q2 in promoList on q1.RecID equals Conversion.getInt32OrNull(q2)
                        select q1;
            
            foreach (var row in query)
            {
                ResPromoRecord record = new ResPromoRecord();
                record.Amount = row.Amount;
                record.Confirmed = row.ConfReq.HasValue ? (row.ConfReq.Value ? false : true) : false;
                record.CustNo = row.CustNo;
                record.PromoID = row.PromoID;
                record.PromoPriceID = row.PromoPriceID;
                record.PromoType = row.PromoType;
                record.RecID = (resPromo != null && resPromo.Count > 0) ? resPromo.Last().RecID + 1 : 1;
                record.ResNo = ResData.ResMain.ResNo;
                record.SaleCur = row.SaleCur;
                record.SeqNo = (resPromo != null && resPromo.Count > 0) ? Convert.ToInt16(resPromo.Last().SeqNo + 1) : Convert.ToInt16(1);
                record.Status = true;
                resPromo.Add(record);

                if (row.PasEBValid.HasValue && !row.PasEBValid.Value)
                    if (resMain.EBPasChk.HasValue && resMain.EBPasChk.Value != 1)
                    {
                        resMain.EBPasChk = 1;
                        resMain.EBAgencyChk = 1;
                    }

                if (row.SpoValid.HasValue && !row.SpoValid.Value)
                    if (resMain.PLSpoChk.HasValue && resMain.PLSpoChk.Value != 1)
                        resMain.PLSpoChk = 1;
            }
            ResData.ResPromo = resPromo;
        }
        #endregion

        #region Credit control
        Int16 CreditCont = new TvBo.Reservation().CreditControl(UserData.AgencyID, UserData.Market, ref errorMsg);

        if (CreditCont == 2)
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = HttpContext.GetGlobalResourceObject("LibraryResource", "NoCreditLimit").ToString()
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }
        #endregion

        #region No servis error
        if (ResData.ResService.Count < 1)
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = HttpContext.GetGlobalResourceObject("LibraryResource", "NoResServiceMsg").ToString()
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }
        #endregion

        #region Check flight time
        if (!(new Flights().CheckFlightTime(UserData, ResData.ResService, ref errorMsg)))
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = HttpContext.GetGlobalResourceObject("LibraryResource", "CheckInTimeOver").ToString()
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }
        #endregion

        #region Bonus Use

        if (!string.IsNullOrEmpty(bonusID))
            resMain.BonusID2 = bonusID;

        decimal? agencyBonus = null;
        decimal useableAgencyBonus = 0;
        decimal? userBonus = null;
        decimal useableUserBonus = 0;

        if (UserData.Bonus.AgencyBonus || UserData.Bonus.UserBonus)
        {
            UserBonusTotalRecord _useableAgencyBonus = new Bonus().getBonus(ResData.ResMain.BegDate, ResData.ResMain.SaleCur, "A", UserData.AgencyID, ref errorMsg);
            useableAgencyBonus = _useableAgencyBonus != null ? (_useableAgencyBonus.UseableBonus.HasValue ? _useableAgencyBonus.UseableBonus.Value : 0) : 0;
            UserBonusTotalRecord _useableUserBonus = new Bonus().getBonus(ResData.ResMain.BegDate, ResData.ResMain.SaleCur, "U", UserData.PIN, ref errorMsg);
            useableUserBonus = _useableUserBonus != null ? (_useableUserBonus.UseableBonus.HasValue ? _useableUserBonus.UseableBonus.Value : 0) : 0;
        }

        try
        {
            agencyBonus = Conversion.getDecimalOrNull(AgencyBonus);
            userBonus = Conversion.getDecimalOrNull(UserBonus);
        }
        catch
        {

        }

        int? ErrorCode = 9;
        if (useableUserBonus >= userBonus)
        {
            UsedBonusRecord _userBonus = new Bonus().usedBonusTemp(UserData, ResData, "U", UserData.PIN, userBonus, ref ErrorCode, ref errorMsg);
            if (_userBonus != null)
            {
                if (ErrorCode.HasValue && ErrorCode.Value != 0)
                {
                    string msgError = string.Empty;
                    switch (ErrorCode)
                    {
                        case 1:
                            msgError = HttpContext.GetGlobalResourceObject("Bonus", "bonusSpMsg1").ToString(); /*"No Bonus Amount."*/
                            break;
                        case 2:
                            msgError = HttpContext.GetGlobalResourceObject("Bonus", "bonusSpMsg2").ToString(); /*"Conversion table is not specified."*/
                            break;
                        case 3:
                            msgError = HttpContext.GetGlobalResourceObject("Bonus", "bonusSpMsg3").ToString(); /*"Bonus conversion table has some mistake."*/
                            break;
                    }
                    returnData.Add(new ReservastionSaveErrorRecord
                    {
                        ControlOK = false,
                        Message = msgError
                    });
                    return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
                }
                else
                {
                    decimal userMaxPerc = (UserData.TvParams.TvParamSystem.Bonus_UserMaxPerc.HasValue ? UserData.TvParams.TvParamSystem.Bonus_UserMaxPerc.Value : Convert.ToDecimal(100)) / 100;
                    if (_userBonus.UsedBonusAmount.HasValue && (ResData.ResMain.SalePrice.Value * userMaxPerc <= _userBonus.UsedBonusAmount))
                    {
                        ResData.ResMain.UserBonusAmount = ResData.ResMain.SalePrice.Value * userMaxPerc;
                        ResData.ResMain.UserBonus = ResData.ResMain.SalePrice.Value * userMaxPerc;
                    }
                    else
                    {
                        ResData.ResMain.UserBonusAmount = _userBonus.UsedBonusAmount;
                        ResData.ResMain.UserBonus = _userBonus.UsedBonus;
                    }
                }
            }
            else
            {
                returnData.Add(new ReservastionSaveErrorRecord
                {
                    ControlOK = false,
                    Message = errorMsg
                });
                return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
            }
        }
        ErrorCode = 9;
        if (useableAgencyBonus >= agencyBonus)
        {
            UsedBonusRecord _agencyBonus = new Bonus().usedBonusTemp(UserData, ResData, "A", UserData.AgencyID, agencyBonus, ref ErrorCode, ref errorMsg);
            if (_agencyBonus != null)
            {
                if (ErrorCode.HasValue && ErrorCode.Value != 0)
                {
                    string msgError = string.Empty;
                    switch (ErrorCode)
                    {
                        case 1:
                            msgError = HttpContext.GetGlobalResourceObject("Bonus", "bonusSpMsg1").ToString(); /*"No Bonus Amount."*/
                            break;
                        case 2:
                            msgError = HttpContext.GetGlobalResourceObject("Bonus", "bonusSpMsg2").ToString(); /*"Conversion table is not specified."*/
                            break;
                        case 3:
                            msgError = HttpContext.GetGlobalResourceObject("Bonus", "bonusSpMsg3").ToString(); /*"Bonus conversion table has some mistake."*/
                            break;
                    }
                    returnData.Add(new ReservastionSaveErrorRecord
                    {
                        ControlOK = false,
                        Message = msgError
                    });
                    return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
                }
                else
                {
                    decimal agencyMaxPerc = (UserData.TvParams.TvParamSystem.Bonus_AgencyMaxPerc.HasValue ? UserData.TvParams.TvParamSystem.Bonus_AgencyMaxPerc.Value : Convert.ToDecimal(100)) / 100;
                    if (_agencyBonus.UsedBonusAmount.HasValue && (ResData.ResMain.SalePrice.Value * agencyMaxPerc <= _agencyBonus.UsedBonusAmount))
                    {
                        ResData.ResMain.AgencyBonus = ResData.ResMain.SalePrice.Value * agencyMaxPerc;
                        ResData.ResMain.AgencyBonusAmount = ResData.ResMain.SalePrice.Value * agencyMaxPerc;
                    }
                    else
                    {
                        ResData.ResMain.AgencyBonus = _agencyBonus.UsedBonus;
                        ResData.ResMain.AgencyBonusAmount = _agencyBonus.UsedBonusAmount;
                    }
                }
            }
            else
            {
                returnData.Add(new ReservastionSaveErrorRecord
                {
                    ControlOK = false,
                    Message = errorMsg
                });
                return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
            }
        }
        #endregion

        List<CustControlErrorRecord> custControl = new Reservation().CustControl(UserData, ResData, ref errorMsg);
        if ((custControl == null) || (custControl.Count == 1 && (custControl.Where(w => w.ErrorCode == 0).Count() == 1 || custControl.Where(w => w.ErrorCode == 30).Count() == 1)))
        {
            resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
            if (setOptionTime.HasValue && setOptionTime.Value == false)
                resMain.OptDate = null;

            returnData = new Reservation().SaveReservation(UserData, ref ResData, saveAsDraft);

            if (returnData == null || returnData.Count < 1)
            {
                returnData.Add(new ReservastionSaveErrorRecord
                {
                    ControlOK = false,
                    Message = HttpContext.GetGlobalResourceObject("LibraryResource", "ResNotSaved").ToString(),
                    OtherReturnValue = custControl.FirstOrDefault().ErrorCode.ToString()
                });
                return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
            }
            else
            {
                bool? saveResAfterResView = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "SaveResAfterResView"));
                bool? sendSMS = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "SendSMS"));
                if (sendSMS.HasValue && sendSMS.Value)
                {
                    Sms sms = new Sms();
                    sms.AddSmsQueue(UserData.Market, ResData, SmsBo.SmsType.NewReservation, UserData.UserName, ResData.ResCust.Where(w => w.Leader == "Y").FirstOrDefault().Phone, ref errorMsg);
                }
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                {
                    if (UserData.AgencyRec != null && UserData.AgencyRec.AceExport)
                    {
                        string AceFileName = string.Empty;
                        if (!(new Aces().SendAceTo(UserData, ResData, AppDomain.CurrentDomain.BaseDirectory + "ACE//", ref AceFileName, ref errorMsg)))
                        {
                        }
                    }

                    returnData.Clear();
                    ReservastionSaveErrorRecord retVal = new ReservastionSaveErrorRecord();
                    if (custControl.Where(w => w.ErrorCode == 30).Count() == 1)
                    {
                        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
                        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
                        string msg = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString(), ResData.ResMain.ResNo);
                        msg += "<br />";
                        msg += HttpContext.GetGlobalResourceObject("MakeReservation", "parentAuthorizationMsg").ToString();
                        msg += "<br />";
                        msg += string.Format("<a href=\"#\" onClick=\"window.open('{0}','mywindow')\" style=\"font-style:italic; font-weight:bold;\">{1}</a>",
                                             WebRoot.BasePageRoot + DocumentFolder + "/" + UserData.Market + "/authorization.pdf",
                                             HttpContext.GetGlobalResourceObject("MakeReservation", "parentAuthorizationLnk"));
                        retVal.ControlOK = true;
                        retVal.Message = msg;
                        retVal.OtherReturnValue = custControl != null && custControl.Count > 0 ? custControl.FirstOrDefault().ErrorCode.ToString() : string.Empty;
                        retVal.GotoReservation = !saveResAfterResView.HasValue || (saveResAfterResView.HasValue && saveResAfterResView.Value);
                        returnData.Add(retVal);
                    }
                    else
                        if (custControl.Where(w => w.ErrorCode == 32).Count() > 1 && goAhead != true)
                    {
                        retVal.ControlOK = false;
                        retVal.Message = custControl.Where(w => w.ErrorCode == 32).FirstOrDefault().ErrorMessage;
                        retVal.OtherReturnValue = custControl != null && custControl.Count > 0 ? custControl.FirstOrDefault().ErrorCode.ToString() : string.Empty;
                        retVal.GotoReservation = false;
                        returnData.Add(retVal);
                    }
                    else
                    {
                        List<resPayPlanRecord> resPayPlan = new ReservationMonitor().getPaymentPlan(ResData.ResMain.ResNo, ref errorMsg);
                        string paymentPlanStr = new TvBo.ReservationMonitor().getPaymentPlanHTML(ResData.ResMain.ResNo, UserData.Ci.LCID);
                        bool? showPaymentPlan = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "ShowPaymentPlan"));
                        retVal.ControlOK = true;
                        if (resPayPlan != null && resPayPlan.Count > 0 && (!showPaymentPlan.HasValue || showPaymentPlan.Value == true))
                        {
                            resPayPlanRecord firstPayment = resPayPlan.OrderBy(o => o.DueDate).FirstOrDefault();
                            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                                retVal.Message = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString() + "<br />" + paymentPlanStr,
                                                                        ResData.ResMain.ResNo);
                            else
                            {
                                string paidMessage = string.Empty;
                                if (firstPayment.Amount.HasValue)
                                    paidMessage = string.Format(HttpContext.GetGlobalResourceObject("MakeReservation", "lblAmount").ToString(),
                                                                           firstPayment.Amount.Value.ToString("#,###.00") + " " + firstPayment.Cur,
                                                                           firstPayment.DueDate.ToShortDateString());
                                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Orex) ||
                                 string.Equals(UserData.CustomRegID, TvBo.Common.crID_ViyaTurizm) ||
                                  string.Equals(UserData.CustomRegID, TvBo.Common.crID_SummerTour))
                                    paidMessage = string.Empty;
                                retVal.Message = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString() + "<br />{1}",
                                                                        ResData.ResMain.ResNo,
                                                                        paidMessage);
                            }
                        }
                        else
                            retVal.Message = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString(),
                                ResData.ResMain.ResNo);

                        retVal.OtherReturnValue = custControl != null && custControl.Count > 0 ? custControl.FirstOrDefault().ErrorCode.ToString() : string.Empty;
                        retVal.SendEmailB2C = getEmailUrl(UserData, ResData);
                        retVal.GotoPaymentPage = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur);
                        retVal.PaymentPageUrl = string.Equals(UserData.CustomRegID, TvBo.Common.crID_ViyaTurizm) ? "Custom" : "";
                        retVal.GotoReservation = !saveResAfterResView.HasValue || (saveResAfterResView.HasValue && saveResAfterResView.Value);
                        retVal.ResNo = ResData.ResMain.ResNo;
                        returnData.Add(retVal);
                    }

                    String useLog = System.Configuration.ConfigurationManager.AppSettings["useLog"];
                    if (!string.IsNullOrEmpty(useLog) && string.Equals(useLog, "1"))
                    {
                        if (ResData.LogID.HasValue)
                            new WEBLog().updateWEBBookLog(ResData.LogID.Value, DateTime.Now, ResData.ResMain.ResNo, ref errorMsg);
                    }

                    return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
                }
                else
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
                }
            }
        }
        else
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = new Reservation().getCustControlErrorMessage(custControl),
                OtherReturnValue = custControl != null && custControl.Count > 0 ? custControl.FirstOrDefault().ErrorCode.ToString() : string.Empty
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }
    }

    public static string getEmailUrl(User UserData, ResDataRecord ResData)
    {
        try
        {
            if (string.Equals(UserData.CustomRegID, Common.crID_CelexTravel) || string.Equals(UserData.CustomRegID, Common.ctID_TandemMayak) || string.Equals(UserData.CustomRegID,Common.ctID_DuaTour))
            {
                ResServiceRecord hotel = ResData.ResService.Where(w => w.ServiceType == "HOTEL").FirstOrDefault();
                if (hotel != null)
                {
                    AsyncMailSenderParams mailParam = new AsyncMailSenderParams();
                    mailParam.SenderMail = UserData.TvParams.TvParamSystem.SMTPAccount;
                    mailParam.DisplayName = UserData.TvParams.TvParamSystem.SMTPAccount;
                    mailParam.SenderPassword = UserData.TvParams.TvParamSystem.SMTPPass;
                    string fromEmails = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "resSendEmails"));
                    //mailParam.ToAddress = string.IsNullOrEmpty(fromEmails) ? "celex@celextravel.com;mehmet@celextravel.com;meral@celextravel.com" : fromEmails;
                    mailParam.ToAddress = fromEmails;
                    mailParam.Subject = "New reservation.Res no: " + ResData.ResMain.ResNo;
                    mailParam.Port = UserData.TvParams.TvParamSystem.SMTPPort;
                    if (string.Equals(UserData.CustomRegID, Common.crID_CelexTravel))
                        mailParam.Port = 587;
                    mailParam.SMTPServer = UserData.TvParams.TvParamSystem.SMTPServer;
                    mailParam.EnableSSL = UserData.TvParams.TvParamSystem.SMTPUseSSL.HasValue && UserData.TvParams.TvParamSystem.SMTPUseSSL.Value == 1 ? true : false;
                    if (string.Equals(UserData.CustomRegID, Common.ctID_DuaTour))
                        mailParam.EnableSSL = true;
                        string body = ResData.ResMain.ResNo + UserData.AgencyName + hotel.ServiceName + hotel.BegDate.Value.ToString("dd/MM/yyyy") + hotel.EndDate.Value.ToString("dd/MM/yyyy") + hotel.RoomName + hotel.AccomName + hotel.BoardName;
                    mailParam.Body = body;
                    new SendMail().MailSender(mailParam);
                }
                return "";
            }
            else
                if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) || Equals(UserData.CustomRegID, TvBo.Common.crID_Kenba) || Equals(UserData.CustomRegID, TvBo.Common.crID_HolidayPlus))
            {
                string url = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "b2cEmailUrl"));
                url = url.Trim().Trim('\n').Trim('\r');

                ResCustRecord leader = ResData.ResCust.Find(f => f.Leader == "Y");
                ResCustInfoRecord leaderInfo = leader != null ? ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo) : null;
                if (!string.IsNullOrEmpty(url) && leaderInfo != null)
                {
                    if ((Equals(leaderInfo.ContactAddr, "H") ? (string.IsNullOrEmpty(leaderInfo.AddrHomeEmail) ? true : false) : (string.IsNullOrEmpty(leaderInfo.AddrWorkEMail) ? true : false)) == false)
                    {
                        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) || Equals(UserData.CustomRegID, TvBo.Common.crID_Kenba))
                        {
                            fi.detur.SendMail sm = new fi.detur.SendMail();
                            sm.Url = url;
                            sm.MailSend(ResData.ResMain.ResNo, UserData.Ci.Name, ResData.ResMain.Agency);
                        }
                        return string.Format("'serviceUrl':'{0}','ResNo':'{1}','CultureID':'{2}','AgencyID':'{3}'",
                                                url,
                                                ResData.ResMain.ResNo,
                                                UserData.Ci.Name,
                                                ResData.ResMain.Agency);
                    }
                    else
                        return string.Empty;

                }
                else
                    return string.Empty;
            }
            else
            {
                return string.Empty;
            }
        }
        catch
        {
            return string.Empty;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string searchCustomer(string refNo, string SeqNo, string Surname, string Name, string BirthDate)
    {
        /*
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        DateTime? birthDate = Conversion.getDateTimeOrNull(BirthDate);

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);
        string dateFormat = new TvBo.Common().getDateFormat(UserData.Ci);

        SearchCustomerOption searchOptions = SearchCustomerOption.AllData;

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            searchOptions = SearchCustomerOption.OnlyAgency;
        else
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                searchOptions = SearchCustomerOption.OnlyMainAgency;
            else
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                    searchOptions = SearchCustomerOption.OnlyMarket;
                else
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Dallas))
                        searchOptions = SearchCustomerOption.AllData;
                    else
                        searchOptions = SearchCustomerOption.OnlyAgency;

        List<ResCustRecord> list = new Reservation().searchCustomers(UserData, searchOptions, Surname, Name, birthDate, ref errorMsg);

        if (list != null && list.Count > 0 && string.IsNullOrEmpty(errorMsg))
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div id=\"divSearcCustList\">");
            sb.Append("<table>");
            sb.Append("<tr class=\"sercahCustListHeader\">");
            sb.Append("<td class=\"sselectH\">&nbsp;</td>");
            sb.AppendFormat("<td class=\"sSurnameH\">{0}</td>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustSurname"));
            sb.AppendFormat("<td class=\"sNameH\">{0}</td>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustName"));
            sb.AppendFormat("<td class=\"sBirthDateH\">{0}</td>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustBirthDate"));
            sb.AppendFormat("<td class=\"sPassSerieH\">{0}</td>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustPassSerie"));
            sb.AppendFormat("<td class=\"sPassNoH\">{0}</td>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustPassNo"));
            sb.Append("</tr>");

            foreach (ResCustRecord row in list)
            {
                sb.Append("<tr class=\"sercahCustListRow\">");
                sb.AppendFormat("<td class=\"sselect\"><input type=\"button\" value=\"{0}\" onclick=\"searchSelectCust('{1}')\" /></td>",
                        HttpContext.GetGlobalResourceObject("LibraryResource", "btnSelect"),
                        row.CustNo.ToString() + ";" + SeqNo + ";" + refNo);
                sb.AppendFormat("<td class=\"sSurname\">{0}</td>", row.Surname);
                sb.AppendFormat("<td class=\"sName\">{0}</td>", row.Name);
                sb.AppendFormat("<td class=\"sBirthDate\">{0}</td>", row.Birtday.HasValue ? row.Birtday.Value.ToString(dateFormat) : "&nbsp;");
                sb.AppendFormat("<td class=\"sPassSerie\">{0}</td>", row.PassSerie);
                sb.AppendFormat("<td class=\"sPassNo\">{0}</td>", row.PassNo);
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            return sb.ToString();
        }
        else return "Err:" + errorMsg;
         */
        return "";
    }

    [WebMethod(EnableSession = true)]
    public static string copyCustomer(string CustNo, List<resCustjSonDataV2> Custs)
    {
        int? oldCustNo = Conversion.getInt32OrNull(CustNo.Split(';')[0]);
        int? newCustNo = Conversion.getInt32OrNull(CustNo.Split(';')[1]);

        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (setCustomers(Custs) != "OK")
            return "";

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);
        List<ResCustRecord> resCust = ResData.ResCust;
        List<ResCustInfoRecord> resCustInfo = ResData.ResCustInfo;

        ResCustRecord oldCust = new ResTables().getResCustRecord(UserData, oldCustNo, ref errorMsg);
        ResCustInfoRecord oldCustInfo = new ResTables().getResCustInfoRecord(oldCustNo, ref errorMsg);

        ResCustRecord newCust = resCust.Find(f => f.SeqNo == newCustNo);
        ResCustInfoRecord newCustInfo = resCustInfo.Find(f => f.CustNo == (newCust != null ? newCust.CustNo : -1));
        if (newCust != null)
        {
            Int16? seqNo = newCust.SeqNo;
            int custNo = newCust.CustNo;
            int? custNoT = newCust.CustNoT;

            newCust.Title = oldCust.Title;
            newCust.TitleStr = oldCust.TitleStr;
            newCust.Surname = oldCust.Surname;
            newCust.SurnameL = oldCust.SurnameL;
            newCust.Name = oldCust.Name;
            newCust.NameL = oldCust.NameL;
            newCust.Birtday = oldCust.Birtday;
            newCust.Age = oldCust.Age;
            newCust.HasPassport = oldCust.HasPassport;
            newCust.IDNo = oldCust.IDNo;
            newCust.Nation = oldCust.Nation;
            newCust.NationName = oldCust.NationName;
            newCust.NationNameL = oldCust.NationNameL;
            newCust.Nationality = oldCust.Nationality;
            newCust.NationalityName = oldCust.NationalityName;
            newCust.PassExpDate = oldCust.PassExpDate;
            newCust.PassGiven = oldCust.PassGiven;
            newCust.PassIssueDate = oldCust.PassIssueDate;
            newCust.PassNo = oldCust.PassNo;
            newCust.PassSerie = oldCust.PassSerie;
            newCust.Phone = strFunc.Trim(oldCust.Phone, ' ');

            if (newCustInfo != null && oldCustInfo != null)
            {
                newCustInfo.AddrHome = oldCustInfo.AddrHome;
                newCustInfo.AddrHomeCity = oldCustInfo.AddrHomeCity;
                newCustInfo.AddrHomeCountry = oldCustInfo.AddrHomeCountry;
                newCustInfo.AddrHomeCountryCode = oldCustInfo.AddrHomeCountryCode;
                newCustInfo.AddrHomeEmail = oldCustInfo.AddrHomeEmail;
                newCustInfo.AddrHomeFax = strFunc.Trim(oldCustInfo.AddrHomeFax, ' ');
                newCustInfo.AddrHomeTel = strFunc.Trim(oldCustInfo.AddrHomeTel, ' ');
                newCustInfo.AddrHomeZip = oldCustInfo.AddrHomeZip;
                newCustInfo.AddrWork = oldCustInfo.AddrWork;
                newCustInfo.AddrWorkCity = oldCustInfo.AddrWorkCity;
                newCustInfo.AddrWorkCountry = oldCustInfo.AddrWorkCountry;
                newCustInfo.AddrWorkCountryCode = oldCustInfo.AddrWorkCountryCode;
                newCustInfo.AddrWorkEMail = oldCustInfo.AddrWorkEMail;
                newCustInfo.AddrWorkFax = strFunc.Trim(oldCustInfo.AddrWorkFax, ' ');
                newCustInfo.AddrWorkTel = strFunc.Trim(oldCustInfo.AddrWorkTel, ' ');
                newCustInfo.AddrWorkZip = oldCustInfo.AddrWorkZip;
                newCustInfo.Bank = oldCustInfo.Bank;
                newCustInfo.BankAccNo = oldCustInfo.BankAccNo;
                newCustInfo.BankIBAN = oldCustInfo.BankIBAN;
                newCustInfo.CName = oldCustInfo.CName;
                newCustInfo.ContactAddr = oldCustInfo.ContactAddr;
                newCustInfo.CSurName = oldCustInfo.CSurName;
                newCustInfo.CTitle = oldCustInfo.CTitle;
                newCustInfo.CTitleName = oldCustInfo.CTitleName;
                newCustInfo.HomeTaxAccNo = oldCustInfo.HomeTaxAccNo;
                newCustInfo.HomeTaxOffice = oldCustInfo.HomeTaxOffice;
                newCustInfo.InvoiceAddr = oldCustInfo.InvoiceAddr;
                newCustInfo.Jobs = oldCustInfo.Jobs;
                newCustInfo.MobTel = strFunc.Trim(oldCustInfo.MobTel, ' ');
                newCust.Phone = newCustInfo.MobTel;
                newCustInfo.Note = oldCustInfo.Note;
                newCustInfo.WorkFirmName = oldCustInfo.WorkFirmName;
                newCustInfo.WorkTaxAccNo = oldCustInfo.WorkTaxAccNo;
                newCustInfo.WorkTaxOffice = oldCustInfo.WorkTaxOffice;
            }
            else
            {
                if (oldCustInfo != null)
                {
                    newCustInfo = new ResCustInfoRecord();
                    newCustInfo = oldCustInfo;
                    newCustInfo.CustNo = custNo;
                    ResData.ResCustInfo.Add(newCustInfo);
                }
            }
            if (ResData.ExtrasData != null)
            {
                ResData.ExtrasData.ResCust = ResData.ResCust;
                ResData.ExtrasData.ResCustInfo = ResData.ResCustInfo;
            }
            HttpContext.Current.Session["ResData"] = ResData;
        }
        return "OK";
    }

    [WebMethod(EnableSession = true)]
    public static string getHandicaps()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        StringBuilder sb = new StringBuilder();
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        List<HandicapsRecord> holpackHandicaps = new TvBo.Common().getHandicaps(UserData, ResData.ResMain.BegDate, ResData.ResMain.EndDate, "HOLPACK", ResData.ResMain.HolPack, HandicapTypes.Package, ref errorMsg);
        if (holpackHandicaps != null && !string.IsNullOrEmpty(ResData.ResMain.HolPack))
        {
            string holpackHandicapStr = string.Format("<b>{0}</b><br />", useLocalName ? ResData.ResMain.HolPackNameL : ResData.ResMain.HolPackName);
            foreach (HandicapsRecord r1 in holpackHandicaps)
                holpackHandicapStr += string.Format("{0}<br />", useLocalName ? r1.NameL : r1.Name);
            if (holpackHandicapStr.Length > 0)
            {
                sb.Append(holpackHandicapStr);
                sb.Append("<br />");
            }
        }
        var hotels = from q in ResData.ResService
                     where q.ServiceType == "HOTEL"
                     group q by new
                     {
                         Hotel = q.Service,
                         ServiceName = q.ServiceName,
                         ServiceNameL = q.ServiceNameL,
                         BegDate = q.BegDate,
                         EndDate = q.EndDate
                     } into k
                     select new { Hotel = k.Key.Hotel, ServiceName = k.Key.ServiceName, ServiceNameL = k.Key.ServiceNameL, BegDate = k.Key.BegDate, EndDate = k.Key.EndDate };

        if (hotels != null && hotels.Count() > 0)
        {
            string handicapServiceLabel = string.Format("<span class=\"handicapServiceLabel\">{0}</span><br />", HttpContext.GetGlobalResourceObject("MakeReservation", "hotelHandicapLabel"));
            string hotelHandicapStr = string.Empty;
            foreach (var row in hotels)
            {
                List<HotelHandicapRecord> hotelHandicapList = new Hotels().getHotelHandicaps(UserData.Market, ResData.ResMain.PLMarket, row.Hotel, row.BegDate, row.EndDate, ref errorMsg);
                if (hotelHandicapList != null && hotelHandicapList.Count() > 0)
                {
                    hotelHandicapStr += string.Format("<b>{0}</b><br />", useLocalName ? row.ServiceNameL : row.ServiceName);
                    foreach (HotelHandicapRecord r1 in hotelHandicapList)
                        hotelHandicapStr += string.Format("{0}<br />", useLocalName ? r1.NameL : r1.Name);
                }
            }
            if (hotelHandicapStr.Length > 0)
            {
                sb.Append(handicapServiceLabel);
                sb.Append(hotelHandicapStr);
                sb.Append("<br />");
            }
        }

        var flights = from q in ResData.ResService
                      where q.ServiceType == "FLIGHT"
                      group q by new
                      {
                          Flight = q.Service,
                          ServiceName = q.ServiceName,
                          ServiceNameL = q.ServiceNameL,
                          BegDate = q.BegDate,
                          EndDate = q.EndDate
                      } into k
                      select new { Flight = k.Key.Flight, ServiceName = k.Key.ServiceName, ServiceNameL = k.Key.ServiceNameL, BegDate = k.Key.BegDate, EndDate = k.Key.EndDate };

        if (flights != null && flights.Count() > 0)
        {
            string handicapFlightServiceLabel = string.Format("<span class=\"handicapServiceLabel\">{0}</span><br />", HttpContext.GetGlobalResourceObject("MakeReservation", "flightHandicapLabel"));
            string flightHandicapStr = string.Empty;
            foreach (var row in flights)
            {
                List<HandicapsRecord> flightHandicapList = new TvBo.Common().getHandicaps(UserData, row.BegDate, null, "FLIGHT", row.Flight, HandicapTypes.Package, ref errorMsg);
                if (flightHandicapList != null && flightHandicapList.Count() > 0)
                {
                    flightHandicapStr += string.Format("<b>{0}</b><br />", useLocalName ? row.ServiceNameL : row.ServiceName);
                    foreach (HandicapsRecord r1 in flightHandicapList)
                        flightHandicapStr += string.Format("{0}<br />", useLocalName ? r1.NameL : r1.Name);
                }

                FlightDayRecord flight = new Flights().getFlightDay(UserData, row.Flight, row.BegDate.Value, ref errorMsg);
                List<HandicapsRecord> depAiprPortHandicapList = new TvBo.Common().getHandicaps(UserData, row.BegDate, null, "AIRPORT", flight.DepAirport, HandicapTypes.Package, ref errorMsg);
                if (depAiprPortHandicapList != null && depAiprPortHandicapList.Count() > 0)
                {
                    AirportRecord airport = new Flights().getAirport(UserData.Market, flight.DepAirport, ref errorMsg);
                    flightHandicapStr += string.Format("<b>{0}</b><br />", airport.Code + " (" + (useLocalName ? airport.LocalName : airport.Name) + ")");
                    foreach (HandicapsRecord r1 in depAiprPortHandicapList)
                        flightHandicapStr += string.Format("{0}<br />", useLocalName ? r1.NameL : r1.Name);
                }
            }
            if (flightHandicapStr.Length > 0)
            {
                sb.Append(handicapFlightServiceLabel);
                sb.Append(flightHandicapStr);
                sb.Append("<br />");
            }
        }

        var location = from q in ResData.ResService
                       group q by new
                       {
                           Location = q.DepLocation,
                           LocationName = q.DepLocationName,
                           LocationNameL = q.DepLocationNameL,
                       } into k
                       select new { Location = k.Key.Location, LocationName = k.Key.LocationName, LocationNameL = k.Key.LocationNameL };

        if (location != null && location.Count() > 0)
        {
            string handicapLocationServiceLabel = string.Format("<span class=\"handicapServiceLabel\">{0}</span><br />", HttpContext.GetGlobalResourceObject("MakeReservation", "locationHandicapLabel"));
            string locationHandicapStr = string.Empty;
            foreach (var row in location)
            {
                Location loc = new Locations().getLocation(UserData.Market, row.Location, ref errorMsg);
                if (loc != null)
                {
                    List<HandicapsRecord> locationHandicapList = new TvBo.Common().getHandicaps(UserData, ResData.ResMain.BegDate, ResData.ResMain.EndDate, "LOCATION", loc.Code, HandicapTypes.Package, ref errorMsg);
                    if (locationHandicapList != null && locationHandicapList.Count() > 0)
                    {
                        locationHandicapStr += string.Format("<b>{0}</b><br />", useLocalName ? loc.NameL : loc.Name);
                        foreach (HandicapsRecord r1 in locationHandicapList)
                            locationHandicapStr += string.Format("{0} <br />", useLocalName ? r1.NameL : r1.Name);
                    }
                }
            }
            if (locationHandicapStr.Length > 0)
            {
                sb.Append(handicapLocationServiceLabel);
                sb.Append(locationHandicapStr);
                sb.Append("<br />");
            }
        }

        if (sb.Length > 0)
            sb.AppendFormat("<br /><input id=\"msgAccept\" type=\"checkbox\" onclick=\"clickMsgAccept();\" /><label for=\"msgAccept\">{0}</label><br />", HttpContext.GetGlobalResourceObject("MakeReservation", "messageAccept"));
        else
            sb.Append("<input id=\"msgAccept\" type=\"checkbox\" checked=\"checked\" style=\"display:none;\" />");
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getPromotionDiv()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) ||
            Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ||
            Equals(UserData.CustomRegID, TvBo.Common.crID_HolidayPlus) ||
            Equals(UserData.CustomRegID, TvBo.Common.crID_FilipTravel))
        {
            if (UserData.TvParams.TvParamReser.HoneymoonChkW.HasValue && UserData.TvParams.TvParamReser.HoneymoonChkW.Value)
                return "1;1;1";
            else
                return "1;0;1";
        }
        else
            return "0;0;0";
    }

    [WebMethod(EnableSession = true)]
    public static string removeService(int? RecID, bool? forPrice)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResDataRecord OldResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);
        List<ResServiceRecord> resServiceList = ResData.ResService;
        ResServiceRecord resService = resServiceList.Find(f => f.RecID == RecID);
        if (resService == null)
            return ""; // Unknown error
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
        {
            if (Equals(resService.ServiceType, "FLIGHT") && resServiceList.Where(w => w.ServiceType == "FLIGHT").Count() == 1)
                return ""; //Service can not be deleted.
            if (Equals(resService.ServiceType, "FLIGHT") && (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0))
            {
                ResServiceRecord firstFlight = resServiceList.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "FLIGHT" && (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0)).FirstOrDefault();
                if (firstFlight.Service != resService.Service)
                {
                    ResData = new Reservation().DeleteResServiceFromResData(UserData, ResData, RecID, true, ref errorMsg);
                    if (string.IsNullOrEmpty(errorMsg))
                    {
                        if (forPrice.HasValue && forPrice.Value)
                        {
                            return "{" + string.Format("\"OldPrice\":\"{0}\",\"NewPrice\":\"{1}\"",
                        OldResData.ResMain.SalePrice.HasValue ? (OldResData.ResMain.SalePrice.Value.ToString("#,###.00") + " " + OldResData.ResMain.SaleCur) : "",
                        ResData.ResMain.SalePrice.HasValue ? (ResData.ResMain.SalePrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur) : "") + "}";
                        }
                        else
                        {
                            HttpContext.Current.Session["ResData"] = ResData;
                            return "OK";
                        }
                    }
                }
                else
                {
                    ResServiceRecord firstFlg = resServiceList.Find(f => f.RecID == (resServiceList.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "FLIGHT" && (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0)).LastOrDefault().RecID));
                    firstFlg.PriceSource = 0;
                    ResData = new Reservation().DeleteResServiceFromResData(UserData, ResData, RecID, true, ref errorMsg);
                    if (string.IsNullOrEmpty(errorMsg))
                    {
                        if (forPrice.HasValue && forPrice.Value)
                        {
                            return "{" + string.Format("\"OldPrice\":\"{0}\",\"NewPrice\":\"{1}\"",
                        OldResData.ResMain.SalePrice.HasValue ? (OldResData.ResMain.SalePrice.Value.ToString("#,###.00") + " " + OldResData.ResMain.SaleCur) : "",
                        ResData.ResMain.SalePrice.HasValue ? (ResData.ResMain.SalePrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur) : "") + "}";
                        }
                        else
                        {
                            HttpContext.Current.Session["ResData"] = ResData;
                            return "OK";
                        }
                    }
                }
            }
            else
            {
                ResData = new Reservation().DeleteResServiceFromResData(UserData, ResData, RecID, true, ref errorMsg);
                if (string.IsNullOrEmpty(errorMsg))
                {
                    if (forPrice.HasValue && forPrice.Value)
                    {
                        return "{" + string.Format("\"OldPrice\":\"{0}\",\"NewPrice\":\"{1}\"",
                        OldResData.ResMain.SalePrice.HasValue ? (OldResData.ResMain.SalePrice.Value.ToString("#,###.00") + " " + OldResData.ResMain.SaleCur) : "",
                        ResData.ResMain.SalePrice.HasValue ? (ResData.ResMain.SalePrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur) : "") + "}";
                    }
                    else
                    {
                        HttpContext.Current.Session["ResData"] = ResData;
                        return "OK";
                    }
                }
            }
        }
        else
        {
            ResData = new Reservation().DeleteResServiceFromResData(UserData, ResData, RecID, true, ref errorMsg);
            if (string.IsNullOrEmpty(errorMsg))
            {
                if (forPrice.HasValue && forPrice.Value)
                {
                    return "{" + string.Format("\"OldPrice\":\"{0}\",\"NewPrice\":\"{1}\"",
                        OldResData.ResMain.SalePrice.HasValue ? (OldResData.ResMain.SalePrice.Value.ToString("#,###.00") + " " + OldResData.ResMain.SaleCur) : "",
                        ResData.ResMain.SalePrice.HasValue ? (ResData.ResMain.SalePrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur) : "") + "}";
                }
                else
                {
                    HttpContext.Current.Session["ResData"] = ResData;
                    return "OK";
                }
            }
        }
        return "NO";
    }

    [WebMethod(EnableSession = true)]
    public static string removeServiceExt(int? RecID, bool? forPrice)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResDataRecord OldResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);
        List<ResServiceExtRecord> resServiceExtList = ResData.ResServiceExt;
        ResServiceExtRecord resServiceExt = resServiceExtList.Find(f => f.RecID == RecID);
        if (resServiceExt == null)
            return ""; // Unknown error
        if (Equals(resServiceExt.Compulsory, "Y"))
            return ""; //Service can not be deleted.
        ResData = new Reservation().DeleteResServiceExtFromResData(UserData, ResData, RecID, ref errorMsg);
        if (string.IsNullOrEmpty(errorMsg))
        {
            if (forPrice.HasValue && forPrice.Value)
            {
                return "{" + string.Format("\"OldPrice\":\"{0}\",\"NewPrice\":\"{1}\"",
                    OldResData.ResMain.SalePrice.HasValue ? (OldResData.ResMain.SalePrice.Value.ToString("#,###.00") + " " + OldResData.ResMain.SaleCur) : "",
                    ResData.ResMain.SalePrice.HasValue ? (ResData.ResMain.SalePrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur) : "") + "}";
            }
            else
            {
                HttpContext.Current.Session["ResData"] = ResData;
                return "OK";
            }
        }
        return "NO";
    }

    [WebMethod(EnableSession = true)]
    public static string checkCustomers()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string retMsg = "\"Checked\":\"{0}\",\"Message\":\"{1}\"";
        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);
        List<ResCustRecord> resCust = ResData.ResCust;
        List<CustControlErrorRecord> custControl = new Reservation().CustControl(UserData, ResData, ref errorMsg);
        if (custControl.Where(w => w.ErrorCode != 0 && w.ErrorCode != 30).Count() > 0)
        {
            string Message = new Reservation().getCustControlErrorMessage(custControl);
            return "{" + string.Format(retMsg, "0", Message) + "}";
        }
        else
            return "{" + string.Format(retMsg, "1", "") + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string setPickupPoint(string PickupPoint, string ServiceID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; };
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        List<ResServiceRecord> resServiceList = ResData.ResService;
        ResServiceRecord resService = resServiceList.Find(f => f.RecID == Conversion.getInt32OrNull(ServiceID));
        if (resService != null && Conversion.getInt32OrNull(PickupPoint).HasValue)
        {
            Location loc = new Locations().getLocation(UserData.Market, Conversion.getInt32OrNull(PickupPoint).Value, ref errorMsg);
            if (loc != null)
            {
                resService.Pickup = Conversion.getInt32OrNull(PickupPoint);
                resService.PickupName = loc.Name;
                resService.PickupNameL = loc.NameL;
            }
            HttpContext.Current.Session["ResData"] = ResData;
        }
        return "";
    }

    private static void addTransLog(string trans)
    {
        string path = AppDomain.CurrentDomain.BaseDirectory + "Log\\CreditCardApply.log";
        System.IO.FileStream f = new System.IO.FileStream(path, System.IO.FileMode.Append, System.IO.FileAccess.Write);
        try
        {
            System.IO.StreamWriter writer = new System.IO.StreamWriter(f);
            writer.WriteLine(trans);
            writer.Close();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            f.Close();
        }
    }

    [WebMethod(EnableSession = true)]
    public static string sendCCardApplyInfo(int custNo, string name, string surName, string phone)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            return string.Empty;

        string result = string.Empty;
        string retVal = string.Empty;
        StreamWriter swr = null;
        StreamReader sr = null;
        try
        {
            string message = "app_type_id=1" +
                             "&app_subtype_id=011201" +
                             "&completed=1" +
                             "&name=" + name +
                             "&surname=" + surName +
                             "&phone_mobile=8" + phone +
                             "&channel=all_airlines_short" +
                             "&utm_source=sunrise_short_aa" +
                             "&form_type=credit" +
                             "&wm=" + UserData.AgencyID +
                             "&agent_code=" + UserData.UserID +
                             "&track=true";
            string url = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "SunriseCCard"));
            url = url.Trim().Trim('\n').Trim('\r');
            if (string.IsNullOrEmpty(url))
                return string.Empty;

            WebRequest request = HttpWebRequest.Create(url); //("https://uat.tcsbank.ru/api/v1/add_application/");
            request.Method = "POST";
            request.ContentLength = message.Length;
            request.ContentType = "application/x-www-form-urlencoded";
            swr = new StreamWriter(request.GetRequestStream());
            swr.Write(message);
            swr.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            sr = new StreamReader(response.GetResponseStream());
            result = sr.ReadToEnd();
            sr.Close();
            response.Close();
            request = null;
            if (result.IndexOf("\"resultCode\":\"OK\"") > 0)
                retVal = "OK";
            else
                retVal = result;
            addTransLog(DateTime.Now.ToString("dd/MM/yyyy HHmmss") + "_" + message);
            addTransLog(DateTime.Now.ToString("dd/MM/yyyy HHmmss") + "_" + result);
        }
        catch (Exception ex)
        {
            retVal = ex.Message;
        }
        return retVal;
    }
}