﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CustomerSearch.aspx.cs" Inherits="Default"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.datepick/jquery-ui-i18n.js" type="text/javascript"></script>
    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="Scripts/jquery.url.js" type="text/javascript"></script>
    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/CustomerSearch.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
        var lblNoResult = '<%= GetGlobalResourceObject("BookTicket", "lblNoResult") %>';
        var dateFormat = '<%= dateFormatCurrentCulture %>';

        function showMsg(msg) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                autoResize: true,
                resizable: true,
                resizeStop: function (event, ui) {
                    $(this).dialog({ position: 'center' });
                },
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                    }
                }]
            });
        }

        function searchSelectCust(CustNo) {
            parent.searchSelectCust(CustNo);
        }

        function getCustomers() {
            var params = new Object();
            params.refNo = $("#hfRefNo").val();
            params.SeqNo = $("#hfSeqNo").val();
            params.Surname = $("#iSurname").val();
            params.Name = $("#iName").val();
            params.BirthDate = $("#iBirthDate").datepicker('getDate') != null ? $("#iBirthDate").datepicker('getDate').getTime() : null;
            params.PIN = $("#iID").val();
            params.PassNo = $("#iPassNo").val();
            params.MobilPhone = $("#iMobTel").val();
            $("#divResult").html('');
            $.ajax({
                type: "POST",
                url: "CustomerSearch.aspx/searchCustomer",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        if (msg.d.toString().substring(0, 4) == "Err:") {
                            var err = msg.d.toString().substring(4);
                            showMsg(err.length > 0 ? err : lblNoResult);
                        }
                        else {
                            $("#divResult").html('');
                            $("#divResult").html(msg.d);
                        }
                    } else {
                        showMsg(lblNoResult);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        $(document).ready(function () {
            $.datepicker.setDefaults($.datepicker.regional['<%= twoLetterISOLanguageName %>' != 'en' ? '<%= twoLetterISOLanguageName %>' : '']);

            $("#iBirthDate").attr("placeholder", dateFormat).datepicker({
                showOn: "button",
                buttonImage: "Images/Calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                maxDate: "+1D",
                yearRange: "-120:+0",
                constrainInput: false
            });

            $.query = $.query.load(location.href);
            $("#hfRefNo").val($.query.get('refNo'));
            $("#hfSeqNo").val($.query.get('SeqNo'));
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <input id="hfRefNo" type="hidden" />
        <input id="hfSeqNo" type="hidden" />
        <div class="ui-widget-content">
            <table class="grid-view">
                <tr>
                    <td><%= GetGlobalResourceObject("MakeReservation", "resCustSurname") %><br />
                        <input id="iSurname" type="text" value="" maxlength="30" />
                    </td>
                    <td><%= GetGlobalResourceObject("MakeReservation", "resCustName") %><br />
                        <input id="iName" type="text" value="" maxlength="30" />
                    </td>
                </tr>
                <tr>
                    <td><%= GetGlobalResourceObject("MakeReservation", "resCustBirthDate") %><br />
                        <input id="iBirthDate" type="text" value="" style="width: 110px;" readonly="readonly" />
                    </td>
                    <td><%= GetGlobalResourceObject("MakeReservation", "resCustPIN") %><br />
                        <input id="iID" type="text" value="" maxlength="20" />
                    </td>
                </tr>
                <tr>
                    <td><%= GetGlobalResourceObject("MakeReservation", "resCustPassNo") %><br />
                        <input id="iPassNo" type="text" value="" maxlength="10" />
                    </td>
                    <td>
                        <%= GetGlobalResourceObject("MakeReservation", "resCustPhone") %><br />
                        <input id="iMobTel" type="text" value="" maxlength="15" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="text-align: right; vertical-align: bottom; font-size: 80%;">
                        <input type="button" value='<%=GetGlobalResourceObject("MakeReservation", "searchCustLabel") %>' onclick="getCustomers()"
                            class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divResult">
        </div>
    </form>
    <div id="dialog-message" title="" style="display: none;">
        <span id="messages">Message</span>
    </div>
</body>
</html>
