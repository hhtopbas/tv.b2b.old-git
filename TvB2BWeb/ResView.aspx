﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResView.aspx.cs" Inherits="ResView"
  EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<%@ Register Src="~/Comment.ascx" TagName="ResComment" TagPrefix="resCom1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    <%= GetGlobalResourceObject("PageTitle", "ResView") %></title>

  <script src="Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <%--<script src="Scripts/jquery.bgiframe-2.1.1.js" type="text/javascript"></script>--%>
  <script src="Scripts/jquery.json.js" type="text/javascript"></script>
  <script src="Scripts/json2.js" type="text/javascript"></script>
  <script src="Scripts/jquery.simplemodal.js" type="text/javascript"></script>
  <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
  <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
  <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
  <script src="Scripts/jquery.printPage.js" type="text/javascript"></script>
  <script src="Scripts/jquery.browser.js" type="text/javascript"></script>
  <script src="Scripts/jquery.dialogextend.js" type="text/javascript"></script>
  <%--<script src="Scripts/jquery.Base64.js" type="text/javascript"></script>--%>
  <script src="Scripts/jquery.url.js" type="text/javascript"></script>

  <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="CSS/main.css" rel="stylesheet" type="text/css" />
  <link href="CSS/ResView.css" rel="stylesheet" type="text/css" />
  <style type="text/css">
        </style>

  <script language="javascript" type="text/javascript">

    var basePageRoot = '<%= Global.getBasePageRoot() %>';
    var EmailSendedMsg = '<%= GetGlobalResourceObject("DetReport", "EmailSended") %>';
    var lblSessionEnd = '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>';
    var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
    var btnChangeLeader = '<%= GetGlobalResourceObject("LibraryResource", "btnChangeLeader") %>';
    var btnClear = '<%= GetGlobalResourceObject("LibraryResource", "btnClear") %>';
    var btnClose = '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>';
    var btnNo = '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>';
    var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
    var btnPDF = '<%= GetGlobalResourceObject("LibraryResource", "btnPDF") %>';
    var btnPrint = '<%= GetGlobalResourceObject("LibraryResource", "btnPrint") %>';
    var btnPrintNext = '<%= GetGlobalResourceObject("LibraryResource", "btnPrintNext") %>';
    var btnRefresh = '<%= GetGlobalResourceObject("LibraryResource", "btnRefresh") %>';
    var btnSave = '<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>';
    var btnSend = '<%= GetGlobalResourceObject("LibraryResource", "btnSend") %>';
    var btnYes = '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>';
    var cancelService = '<%= GetGlobalResourceObject("LibraryResource", "cancelService") %>';
    var deleteCustomer = '<%= GetGlobalResourceObject("LibraryResource", "deleteCustomer") %>';
    var fillLeaderInfo = '<%= GetGlobalResourceObject("LibraryResource", "fillLeaderInfo") %>';
    var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
    var lblTimeOutMesage = '<%= GetGlobalResourceObject("LibraryResource", "lblTimeOutMesage") %>';
    var lblNotSaved = '<%= GetGlobalResourceObject("LibraryResource","lblNotSaved") %>';
    var lblErrorSysAdmin = '<%= GetGlobalResourceObject("ResView","lblErrorSysAdmin") %>';
    var commentMsg = '<%= (resComment.FindControl("txtMessage")).ClientID %>';
      

    var resViewTimeout = null;
    var resOpenTime = null;
    var resIsOpen = false;

    if (typeof window.event != 'undefined')
      document.onkeydown = function () {
        if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
          return (event.keyCode != 8);
      }
    else
      document.onkeypress = function (e) {
        if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
          return (e.keyCode != 8);
      }

    var NS = document.all;
    var resNo = '';
    var pxmResNo = '';
    var _ChangeLeader = 0;
    var _CustNo = -1;

    function showAlertWithLink(message, link) {
        var alink = "<a href='" + link + "'>" + link + "</a>";
        message = message.replace("[LINK]", alink);
        showAlert(message);
    }
    function logout() {
      $('<div>' + lblSessionEnd + '</div>').dialog({
        autoOpen: true,
        position: {
          my: 'center',
          at: 'center'
        },
        modal: true,
        resizable: true,
        autoResize: true,
        bigframe: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog("close");
            $(this).dialog("destroy");
            window.location = 'Default.aspx';
          }
        }]
      });
      window.setTimeout(function () { window.location = "<%=VirtualPathUtility.ToAbsolute("~/Default.aspx")%>"; }, 10000);
    }

    $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '</h1>';
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

      function showAlert(msg) {
          if (msg == undefined) return;
      $(function () {
        $("#messages").html(msg);
        $("#dialog").dialog("destroy");
        $("#dialog-message").dialog({
          position: 'center',
          modal: true,
          buttons: [{
            text: btnOK,
            click: function () {
              $(this).dialog('close');
              return true;
            }
          }]
        });
      });
    }

    function showDialogYesNo(msg) {
      $(function () {
        $("#messages").html(msg);
        $("#dialog").dialog("destroy");
        $("#dialog-message").dialog({
          position: {
            my: 'center',
            at: 'center'
          },
          modal: true,
          buttons: [{
            text: btnOK,
            click: function () {
              $(this).dialog('close');
              return true;
            }
          }, {
            text: btnCancel,
            click: function () {
              $(this).dialog('close');
              return false;
            }
          }]
        });
      });
    }

      function showDialog(msg, transfer, trfUrl) {
          if (msg == '') return;

      $("#messages").html(msg);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        maxWidth: 880,
        maxHeight: 500,
        modal: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog('close');
            if (transfer == true) {
              var url = trfUrl;
              $(location).attr('href', url);
            }
          }
        }]
      });
    }

    function maximize() {
      var maxW = screen.availWidth;
      var maxH = screen.availHeight;
      if (location.href.indexOf('pic') == -1) {
        if (window.opera) { } else {
          top.window.moveTo(0, 0);
          if (document.all) { top.window.resizeTo(maxW, maxH); }
          else
            if (document.layers || document.getElementById) {
              if ((top.window.outerHeight < maxH) || (top.window.outerWidth < maxW)) {
                top.window.outerHeight = maxH; top.window.outerWidth = maxW;
              }
            }
        }
      }
    }

    var selectedSeqNo = null;

    function returnPassportScan(passportData, ready) {
      $("#dialog-passportScan").dialog('close');
      if (ready != undefined && ready != null && ready == true) {
        var params = new Object();
        params.SeqNo = selectedSeqNo;
        params.PassportData = passportData;
        $.ajax({
          type: "POST",
          url: "ReservationRF.aspx/updateCustomerData",
          data: JSON.stringify(params),
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (msg) {
            if (msg.hasOwnProperty('d') && msg.d != null && msg.d == true) {
              getResCustRecords(resNo);
            }
          },
          error: function (xhr, msg, e) {
            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
              showMessage(xhr.responseText);
          },
          complette: function (e, xhr, settings) {
            if (e.status === 200) {

            } else if (e.status === 304) {

            } else if (e.status === 408) {
              logout();
            } else {

            }
          }
        });
      }
      selectedSeqNo = null;
    }

    function onclickPassportScanBtn(seqNo) {
      selectedSeqNo = seqNo;      

      window.scrollTo(0, 0);
      $('html').css('overflow', 'hidden');
      $("#passportScan").attr("src", basePageRoot + 'PassportReader.aspx');
      $("#dialog-passportScan").dialog(
          {
            autoOpen: true,
            modal: true,
            width: 540,
            height: 530,
            resizable: true,
            autoResize: true,
            bigframe: true,
            close: function (event, ui) {
              $('html').css('overflow', 'auto');
            }
          });

    }

    function saveAndCloseComplaint() {
      $("#dialog-ViewEditComplaint").dialog('close');
    }

    function closeComplaint() {
      $("#dialog-ViewEditComplaint").dialog('close');
    }

    function addEditComplaint() {
      window.scrollTo(0, 0);
      $('html').css('overflow', 'hidden');
      $("#ViewEditComplaint").attr("src", basePageRoot + 'ComplaintEdit.aspx?RecID=0&Edit=0');
      $("#dialog-ViewEditComplaint").dialog(
          {
            autoOpen: true,
            modal: true,
            width: 1000,
            height: 700,
            resizable: true,
            autoResize: true,
            bigframe: true,
            close: function (event, ui) { $('html').css('overflow', 'auto'); }
          }).dialogExtend({
            "maximize": true,
            "icons": {
              "maximize": "ui-icon-circle-plus",
              "restore": "ui-icon-pause"
            }
          });
      $("#dialog-ViewEditComplaint").dialogExtend("maximize");
    }

    function getResPaymentRecords(resNo) {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/getResPayment",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#divResPayment").html('');
          $("#divResPayment").html(msg.d);
          return true;
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getResSupDisRecords(resNo) {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/getResSupDis",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#divResSupDis").html('');
          $("#divResSupDis").html(msg.d);
          return true;
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getResServiceExtRecords(resNo) {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/getResServiceExtRecords",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#gridResServiceExt").html('');
          $("#gridResServiceExt").html(msg.d);
          getResSupDisRecords(resNo);
          getResPaymentRecords(resNo);
          return true;
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getResServiceRecords(resNo) {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/getResServiceRecords",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#gridResService").html('');
          $("#gridResService").html(msg.d);
          getResServiceExtRecords(resNo);
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getResCustRecords(resNo) {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/getResCustRecords",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#gridResCust").html('');
          $("#gridResCust").html(msg.d);
          getResServiceRecords(resNo);
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getResMainRecord(reLoad) {
      var params = new Object();
      params.reLoad = reLoad;
      params.resNo = resNo;
      params.paxResNo = pxmResNo;
      $.ajax({
        type: "POST",
        url: "ResView.aspx/getResMainRecord",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#tableResMain").html('');
          $("#tableResMain").html(msg.d);
          if ($("#btnCancel")[0] == undefined) {
            $("#paymentBtn").hide();
          }
          drawSubMenu();
          getResCustRecords(resNo);
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
            return false;
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function showAddTurist(serviceUrl) {
      $("#dialog-AddTourist").dialog("open");
      $("#AddTourist").attr("src", serviceUrl);
      return false;
    }

    function saveChangeCustInfo(str, source) {
      if (source == 'save') {
        str = str.replace('"', '|').replace('{', '<').replace('}', '>');
        $.ajax({
          type: "POST",
          url: "ResView.aspx/returnResCustInfo",
          data: '{"data":"' + str + '"}',
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (msg) {
            if (msg.d == '') {
              $("#dialog-resCustInfo").dialog("close");
              var retVal = getResMainRecord('Y', resNo);
              getResCustRecords();
              if (_ChangeLeader = 1) {
                if (_CustNo > 0)
                  saveChangeLeader(_CustNo);
              }
              return true;
            } else {
              showAlert(msg.d);
            }
          },
          error: function (xhr, msg, e) {
            if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
              showAlert(xhr.responseText);
            }
          },
          statusCode: {
            408: function () {
              logout();
            }
          }
        });
      }
    }

    function returnCustVisaEdit(custNo, data, source) {
      if (source == 'save') {
        str = $.json.encode(data).replace(/"/g, '|').replace(/{/g, '<').replace(/}/g, '>');
        $.ajax({
          type: "POST",
          url: "ResView.aspx/saveResCustVisa",
          data: '{"custNo":' + custNo + ',"data":"' + str + '"}',
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (msg) {
            if (msg.d == '') {
              $("#dialog-resCustInfo").dialog("close");
              var retVal = getResMainRecord('Y', resNo);
              getResCustRecords();
              if (_ChangeLeader = 1) {
                if (_CustNo > 0) {
                  saveChangeLeader(_CustNo);
                }
              }
              return true;
            } else {
              showAlert(msg.d);
            }
          },
          error: function (xhr, msg, e) {
            if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
              showAlert(xhr.responseText);
            }
          },
          statusCode: {
            408: function () {
              logout();
            }
          }
        });
      } else $("#dialog-resCustInfo").dialog("close");
    }

    function returnCustInfoEdit(str, source) {
      if (source == 'save') {
        str = str.replace('"', '|').replace('{', '<').replace('}', '>');
        $.ajax({
          async: false,
          type: "POST",
          url: "ResView.aspx/custChgFee",
          data: '{"data":"' + str + '"}',
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (msg) {
            var retVal = $.json.decode(msg.d);
            if (retVal.retVal == 1) {
              $("#dialog-resCustInfo").dialog("close");
              $(function () {
                $("#messages").html(retVal.Message);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                  position: {
                    my: 'center',
                    at: 'center'
                  },
                  modal: true,
                  buttons: [{
                    text: btnSave,
                    click: function () {
                      $(this).dialog('close');
                      saveChangeCustInfo(str, source);
                    }
                  }, {
                    text: btnCancel,
                    click: function () {
                      $(this).dialog('close');
                      return false;
                    }
                  }]
                });
              });
            } else {
              saveChangeCustInfo(str, source);
            }
          },
          error: function (xhr, msg, e) {
            if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
              showAlert(xhr.responseText);
            }
          },
          statusCode: {
            408: function () {
              logout();
            }
          }
        });
      } else {
        if (source == 'cancel') {
          $("#dialog-resCustInfo").dialog("close");
        }
      }
    }

    function showResCustInfo(CustNo, onlyView) {
      var custInfoUrl = 'CustomerInfo.aspx?CustNo=' + CustNo + '&onlyView=' + onlyView;
      window.scrollTo(0, 0);
      $("#resCustInfo").attr("src", custInfoUrl);
      $("#dialog-resCustInfo").dialog({ autoOpen: true, modal: true, width: 780, /*height: 550,*/resizable: true, autoResize: true, position: ['center', 20] });
      return false;
    }

    function showResCustVisa(CustNo) {
      var custInfoUrl = 'CustomerVisaInfo.aspx?CustNo=';
      $("#resCustInfo").attr("src", custInfoUrl + CustNo);
      $("#dialog-resCustInfo").dialog({ autoOpen: open, position: 'center', modal: true, width: 630, height: 550, resizable: true, autoResize: true });
      $("#dialog-resCustInfo").position({ my: "center", at: "center", of: window });
      return false;
    }

    function ChangeRoomAndAccomType(ServiceID, Adult, Child, CustNo) {
      $("#dialog-changeRoomAccom").dialog("open");
      $("#changeRoomAccom").attr("src", "Controls/ServiceChangeRoomAndAccom.aspx?ServiceID=" + ServiceID + "&Adult=" + Adult + "&Child=" + Child + "&CustNo=" + CustNo);
      return false;
    }

    function cancelAddARoom() {
      $("#dialog-changeRoomAccom").dialog("close");
    }

    function changedRoomAccom(retValues, serviceID, custNo) {
      $("#dialog-changeRoomAccom").dialog("close");
      deleteTourist(custNo, '');
    }

    function AddPaxRoomAccom(hotel, room, accom, adult, child) {
    }

    function deleteTourist(CustNo, ResNo) {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/deleteCustomers",
        data: '{"ResNo":"' + ResNo + '","CustNo":"' + CustNo + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var retVal = $.json.decode(msg.d);
          if (retVal[0].ControlOK == true) {
            getResData(true);
          } else {
            var errorCode = 0;
            try { errorCode = parseInt(retVal[0].Message); }
            catch (e) { errorCode = 0; }

            if (errorCode > 0) {
              otherRetVal = $.json.decode(retVal[0].OtherReturnValue.replace(/!/g, '"').replace('<', '{'));

              var serviceID = parseInt(otherRetVal.ServiceID);
              var adult = parseInt(otherRetVal.Adult);
              var child = parseInt(otherRetVal.Child);
              ChangeRoomAndAccomType(serviceID, adult, child, CustNo);
            } else {
              showAlert(retVal[0].Message);
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function showDeleteCust(CustNo, ResNo) {
      var params = new Object();
      params.ChgType = 44;
      params.CustNo = CustNo;
      $.ajax({
        type: "POST",
        url: "ResView.aspx/getResChgFee",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d != '') {
            deleteCustomer += '<br />' + msg.d;
          }

          $("#messages").html(deleteCustomer);
          $("#dialog").dialog("destroy");
          $("#dialog-message").dialog({
            position: {
              my: 'center',
              at: 'center'
            },
            modal: true,
            buttons: [{
              text: btnYes,
              click: function () {
                $(this).dialog('close');
                deleteTourist(CustNo, ResNo);
                return true;
              }
            }, {
              text: btnNo,
              click: function () {
                $(this).dialog('close');
                return false;
              }
            }]
          });
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function showResService(serviceUrl) {
      $("#dialog-resService").dialog("open");
      $("#resService").attr("src", serviceUrl);
      return false;
    }

    function showAddResService(serviceUrl) {
      $("#dialog-resServiceAdd").dialog("open");
      if (serviceUrl.toString().indexOf('?') > -1) {
        $("#resServiceAdd").attr("src", serviceUrl /*+ '&recordType=temp'*/);
      } else {
        $("#resServiceAdd").attr("src", serviceUrl /*+ '?recordType=temp'*/);
      }
      return false;
    }

    function showAddResServiceExt(serviceUrl) {
      window.scrollTo(0, 0);
      $('html').css('overflow', 'hidden');
      $("#resServiceExtAdd").attr("src", serviceUrl);
      $("#dialog-resServiceExtAdd").dialog({
        autoOpen: true,
        modal: true,
        resizable: true,
        autoResize: true,
        open: function (event, ui) { },
        close: function (event, ui) { $('html').css('overflow', 'auto'); },
        buttons: [{
          text: btnClose,
          click: function () {
            $(this).dialog('close');
            return false;
          }
        }]
      }).dialogExtend({
        "maximize": true,
        "icons": { "maximize": "ui-icon-circle-plus", "restore": "ui-icon-pause" }
      });
      $("#dialog-resServiceExtAdd").dialogExtend("maximize");
    }

    function showPaymentPlan(ResNo) {
      $.ajax({
        async: false,
        type: "POST",
        url: "ResView.aspx/getPaymentPlan",
        data: '{"ResNo":"' + ResNo + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#divPaymentPlan").html('');
          $("#divPaymentPlan").html(msg.d);
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function paymentPlan(ResNo) {
      showPaymentPlan(ResNo);
      $("#dialog").dialog("destroy");
      $("#dialog-paymentPlan").dialog({
        bgiframe: true,
        autoOpen: true,
        modal: true,
        center: true,
        resizable: true,
        autoResize: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog('close');
            return true;
          }
        }]
      });
    }

    function returnAddResServicesExt(save, message) {
      if (save == true) {
        $("#dialog-resServiceExtAdd").dialog("close");
        getResData(true);
      } else {
        $("#dialog-resServiceExtAdd").dialog("close");
      }
      if (message != '') {
        showAlert(message);
      }
    }

    function editResService(serviceUrl) {
      $("#dialog-resServiceEdit").dialog("open");
      $("#resServiceEdit").attr("src", serviceUrl);
      return false;
    }

    function showCancelReservation(serviceUrl) {
      $("#dialog-cancelReservation").dialog("open");
      $("#cancelReservation").attr("src", serviceUrl);
      return false;
    }

    function returnAddResServices(save, message) {
      if (save == true) {
        $("#dialog-resServiceAdd").dialog("close");
        getResData(true);
      }
      else {
        $("#dialog-resServiceAdd").dialog("close");
      }
      if (message != '') {
        showAlert(message);
      }
    }

    function returnEditResServices(save) {
      if (save == true) {
        $("#dialog-resServiceEdit").dialog("close");
        $.query = $.query.load(location.href);
        var resNo = $.query.get('ResNo');
        var retVal = getResMainRecord('Y', resNo);
        getResServiceRecords();
        getResServiceExtRecords();
      }
      else {
        $("#dialog-resServiceEdit").dialog("close");
      }
    }

    function busSeatSelect(recID) {
      window.scrollTo(0, 0);
      $('html').css('overflow', 'hidden');
      $("#dialog-seatSelect").dialog(
      {
        bgiframe: true,
        autoOpen: true,
        modal: true,
        open: function (event, ui) {
          $("#seatSelect").removeAttr("src");
          $("#seatSelect").attr("src", "BusSeatSelect.aspx?ServiceID=" + recID);
        },
        close: function (event, ui) {
          $('html').css('overflow', 'auto');
          getResData(true);
        },
        buttons: [{
          text: btnClose,
          click: function () {
            $(this).dialog('close');
            return true;
          }
        }]
      }).dialogExtend({
        "maximize": true,
        "icons": {
          "maximize": "ui-icon-circle-plus",
          "restore": "ui-icon-pause"
        }
      });
      $("#dialog-seatSelect").dialogExtend("maximize");
    }

    function flightSeatSelectClose() {
      $("#dialog-seatSelect").dialog('close');
    }

    function flightSeatSelect(recID) {

      window.scrollTo(0, 0);
      $('html').css('overflow', 'hidden');
      $("#dialog-seatSelect").dialog(
      {
        bgiframe: true,
        autoOpen: true,
        modal: true,
        open: function (event, ui) {
          $("#seatSelect").removeAttr("src");
          $("#seatSelect").attr("src", "Controls/FlightSeatCheckIn.aspx?ServiceID=" + recID);
        },
        close: function (event, ui) {
          $('html').css('overflow', 'auto');
          getResData(true);
        },
        buttons: [{
          text: btnClose,
          click: function () {
            $(this).dialog('close');
            return true;
          }
        }]
      }).dialogExtend({
        "maximize": true,
        "icons": {
          "maximize": "ui-icon-circle-plus",
          "restore": "ui-icon-pause"
        }
      });
      $("#dialog-seatSelect").dialogExtend("maximize");
    }

    function returnCancelRes_Cancel() {
      $("#dialog-cancelReservation").dialog("close");
    }

    function returnCancelRes_Save() {
      $("#dialog-cancelReservation").dialog("close");
      getResData(true);
    }

    function returnAddTourist(refresh, custs) {
      $("#dialog-AddTourist").dialog("close");
      if (refresh == true) {
        $.ajax({
          type: "POST",
          url: "ResView.aspx/AddingCustomersServices",
          data: '{"Custs":' + $.json.encode(custs) + '}',
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (msg) {
            if (msg.d != '') {
              $("#divAddingCustsServices").html('');
              $("#divAddingCustsServices").html(msg.d);
              $("#dialog-AddingCustsServices").dialog({
                autoOpen: true,
                modal: true,
                width: 1000,
                height: 550,
                resizable: true,
                autoResize: true,
                close: function (event, ui) { getResData(refresh); },
                buttons: [{
                  text: btnOK,
                  click: function () {
                    $(this).dialog('close');
                    return true;
                  }
                }]
              });
            }
          },
          error: function (xhr, msg, e) {
            if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
              showAlert(xhr.responseText);
            }
          },
          statusCode: {
            408: function () {
              logout();
            }
          }
        });
      }
    }

    function deleteService(recID) {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/cancelResService",
        data: '{"RecID":' + recID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d == '') {
            getResData(true);
          } else {
            showAlert(msg.d);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function deleteServiceMsg(recID) {
      $("#messages").html(cancelService);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        position: {
          my: 'center',
          at: 'center'
        },
        modal: true,
        buttons: [{
          text: btnYes,
          click: function () {
            $(this).dialog('close');
            deleteService(recID);
            return true;
          }
        },
        {
          text: btnNo,
          click: function () {
            $(this).dialog('close');
            return false;
          }
        }]
      });
    }

    function deleteServiceExt(recID) {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/cancelResServiceExt",
        data: '{"RecID":' + recID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d == '') {
            getResData(true);
          } else {
            showAlert(msg.d);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function deleteServiceExtMsg(recID) {
      $("#messages").html(cancelService);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        position: {
          my: 'center',
          at: 'center'
        },
        modal: true,
        buttons: [{
          text: btnYes,
          click: function () {
            $(this).dialog('close');
            deleteServiceExt(recID);
            return true;
          }
        },
        {
          text: btnNo,
          click: function () {
            $(this).dialog('close');
            return false;
          }
        }]
      });
    }

    function cancelChangeResDate() {
      $("#dialog-changeDate").dialog("close");
    }

    function returnCancelChangeDate(refresh) {
      $("#dialog-changeDate").dialog("close");
      getResData(refresh);
    }

    function changeResBegDate(resNo, ver) {

      var url = 'Controls/ChangeResDate.aspx?ResNo=' + resNo;
      if (ver == '1')
        url = 'Controls/ChangeResDateV2.aspx?ResNo=' + resNo;
      $("#dialog-changeDate").dialog("open");
      $("#changeDate").attr("src", url);
      return false;
    }

    function getPaymentRes(paymentUrl, param2) {
      $.query = $.query.load(location.href);
      var ResNo = $.query.get('ResNo');
      window.open(paymentUrl + '?ResNo=' + ResNo);
      return false;
    }

    function showAddRoom(_url) {

    }

    function getResData(refresh) {
      $.query = $.query.load(location.href);
      resNo = $.query.get('ResNo');
      pxmResNo = $.query.get('PaxResNo');
      $.ajax({
        async: false,
        type: "POST",
        data: '{"resNo":"' + resNo + '"}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'ResView.aspx/checkResUser',
        success: function (msg) {
          if (msg.d == 'true') {
            var retVal = getResMainRecord(refresh ? 'Y' : 'N', resNo);
          } else {
            showAlert(msg.d);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function showResComment() {
      var paraList = '';
      $.ajax({
        type: "POST",
        data: paraList,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'ResView.aspx/getCommentData',
        success: function (msg) {
          $('#lblCommentHeader').html(resNo);
          $('#gridComment').html(msg.d);
          showCommentDiv();
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function showCommentDiv() {

      $("#dialog").dialog("destroy");
      $("#dialog-resComment").dialog({
        position: {
          my: 'center',
          at: 'center'
        },
        modal: true,
        height: 500,
        width: 820,
        buttons: [
            {
              text: btnClear,
              click: function () {
                var _txtMsg = resCom1.FindControl("txtMessage")
                var comment = document.getElementById(commentMsg);
                comment.value = "";
                return true;
              }
            },
        {
          text: btnSend,
          click: function () {
            var comment = document.getElementById(commentMsg);
            if (comment.value != '') {
              var paraList = '{"Comment":"' + comment.value.replace(/"/g, ' ') + '"}';
              $.ajax({
                type: "POST",
                data: paraList,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResView.aspx/setCommentData',
                success: function (msg) {
                  $('#lblCommentHeader').html(resNo);
                  $('#gridComment').html('');
                  $('#gridComment').html(msg.d);
                  comment.value = '';
                },
                error: function (xhr, msg, e) {
                  if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                    showAlert(lblNotSaved);
                },
                statusCode: {
                  408: function () {
                    logout();
                  }
                }
              });
            }
            return true;
          }
        }, {
          text: btnRefresh,
          click: function () {
            var paraList = '';
            $.ajax({
              type: "POST",
              data: paraList,
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              url: 'ResView.aspx/getCommentData',
              success: function (msg) {
                $('#lblCommentHeader').html(resNo);
                $('#gridComment').html('');
                $('#gridComment').html(msg.d);
              },
              error: function (xhr, msg, e) {
                if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                  showAlert(xhr.responseText);
              },
              statusCode: {
                408: function () {
                  logout();
                }
              }
            });
            return true;
          }
        }, {
          text: btnClose,
          click: function () {
            $(this).dialog('close');
            return false;
          }
        }]
      });
    }

    function returnSsrc(cancel, refresh) {
      if (cancel == true) {
        $("#dialog-Ssrc").dialog("close");
      }
      else {
        $("#dialog-Ssrc").dialog("close");
        if (refresh) {
          getResData(false);
        }
      }
    }

    function showSSRC(CustNo) {
      var serviceUrl = 'Controls/Ssrc.aspx?CustNo=' + CustNo + '&Save=true';
      $("#dialog-Ssrc").dialog("open");
      $("#Ssrc").attr("src", serviceUrl);
    }

    function editResCust(custNo, msg) {
      $("#messages").html(fillLeaderInfo + '<br />' + msg);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        position: {
          my: 'center',
          at: 'center'
        },
        modal: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog('close');
            _ChangeLeader = 1;
            _CustNo = custNo;
            showResCustInfo(custNo, false);
          }
        }]
      });
    }

    function saveChangeLeader(custNo) {
      $.ajax({
        type: "POST",
        data: '{"CustNo":' + custNo + '}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'ResView.aspx/saveChangeLeaderData',
        success: function (msg) {
          var returnVal = $.json.decode(msg.d);
          if (returnVal.retVal == '0') {
            _ChangeLeader = 0;
            _CustNo = -1;
            getResData(true);
          } else {
            if (returnVal.retVal == '1') {
              _ChangeLeader = 1;
              _CustNo = custNo;
              editResCust(custNo, returnVal.retMsg);
            } else {
              showAlert(returnVal.retMsg);
              _ChangeLeader = 2;
              _CustNo = custNo;
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function showChangeLeader() {
      $("#dialog-changeLeader").dialog(
      {
        autoOpen: true,
        position: {
          my: 'center',
          at: 'center'
        },
        modal: true,
        width: 450,
        height: 350,
        resizable: true,
        autoResize: true,
        buttons: [{
          text: btnChangeLeader,
          click: function () {
            var leaderCustNo = 0;
            var leader = $('input[name=leader]:checked').val();
            if (leader == '') {
              $(this).dialog('close');
              showAlert(lblErrorSysAdmin);
            }
            else {
              leaderCustNo = parseInt(leader);
              $(this).dialog('close');
              saveChangeLeader(leaderCustNo);
            }
          }
        }, {
          text: btnCancel,
          click: function () {
            $(this).dialog('close');
            return true;
          }
        }]
      });
    }

    function changeLeader() {
      $.ajax({
        type: "POST",
        data: '{}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'ResView.aspx/getChangeLeaderData',
        success: function (msg) {
          $("#divChangeLeader").html('');
          $("#divChangeLeader").html(msg.d);
          showChangeLeader();
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function drawSubMenu() {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/drawSubMenu",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d != '') {
            $("#divReport").html('');
            $("#divReport").html(msg.d);
            $.query = $.query.load(location.href);
            resNo = $.query.get('ResNo');
            isPayment = $.query.get('Payment');
            if (isPayment == "1") {
                var jInputs = $(msg.d).filter("input");
                $.each(jInputs, function (i,v) {
                    if($(v).attr("onclick").indexOf("Payment")>=0)
                    {
                        var functionVal = $(v).attr("onclick").split(",");
                        getPayments(basePageRoot + functionVal[1].replace(/[' ]/g,""));
                    }
                });

            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function reportList(_html) {
      $("#divReportList").html('');
      $("#divReportList").html(_html);
      $("#dialog-reportList").dialog(
      {
        autoOpen: true,
        position: {
          my: 'center',
          at: 'center'
        },
        modal: true,
        width: 450,
        height: 350,
        resizable: true,
        autoResize: true,
        buttons: {
          btnClose: function () {
            $(this).dialog('close');
            return true;
          }
        }
      });
    }

    function EmailSended(message) {
      if (message != '') {
        showAlert(message);
      } else {
        showAlert(EmailSendedMsg);
      }
    }

    function reportResultShow(param1, param2, ResultType, DocType, emailSender) {
      if (param1 == '' && (param2 != null || param2 != '')) {
        showAlert(param2);
      } else {
        if (ResultType == 'PDF') {
          pdfFileView(param1, DocType, emailSender);
        } else {
          if (ResultType == 'HTMLPDF') {

          }
        }
      }
    }

    function reportResultListShow(NewWindow, param1, param2, DocType, ResultType) {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/getReportList",
        data: '{"Param1":"' + param1 + '","Param2":"' + param2 + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (NewWindow == '') {
            reportResultShow(param1, param2, DocType, ResultType);
          } else {
            reportList(msg.d);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function Insurance0969801(ResNo, Service, CustNo) {
      $.ajax({
        type: "POST",
        cache: false,
        url: "ResView.aspx/getReportRun",
        data: '{"ResNo":"' + ResNo + '","Service":' + Service + ',"CustNo":' + CustNo + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var retVal = $.json.decode(msg.d);
          if (retVal.Param1 != '' && retVal.Param2 != '') {
            $("#divReportList").html('');
            $("#divReportList").html(retVal.Param2.replace(/!/g, '"'));
            retVal.Param2 = '';
          }
          pdfFileView(retVal.Param1, false);

          //sunriseReportShow(retVal.Param1, retVal.Param2);
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function Voucher0969801(ResNo, Service) {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/getReportVoucher",
        data: '{"ResNo":"' + ResNo + '","Service":' + Service + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var retVal = $.json.decode(msg.d);
          if (retVal.NewWindow == -99) {
            showAlert(retVal.Param2);
          } else {
            if (retVal.Param1 != '' && retVal.Param2 != '') {
              $("#divReportList").html('');
              $("#divReportList").html(retVal.Param2.replace(/!/g, '"'));
              retVal.Param2 = '';
            }
            pdfFileView(retVal.Param1, false);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function TicketRef(htmlUrl, DocName, docNameResource, ResultType) {
      htmlPageRender(htmlUrl, DocName, docNameResource, ResultType);
    }

    function Receipt(ResNo, ReceiptNo) {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/getReportReceipt",
        data: '{"ResNo":"' + ResNo + '","ReceiptNo":' + ReceiptNo + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var retVal = $.json.decode(msg.d);
          if (retVal.NewWindow == -99) {
            showAlert(retVal.Param2);
          } else {
            if (retVal.Param1 != '' && retVal.Param2 != '') {
              $("#divReportList").html('');
              $("#divReportList").html(retVal.Param2.replace(/!/g, '"'));
              retVal.Param2 = '';
            }
            pdfFileView(retVal.Param1, false);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function Ticket0969801(ResNo, CustNo) {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/getReportTicket",
        data: '{"ResNo":"' + ResNo + '","CustNo":' + CustNo + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var retVal = $.json.decode(msg.d);
          if (retVal.NewWindow == -99) {
            showAlert(retVal.Param2);
          } else {
            if (retVal.Param1 != '' && retVal.Param2 != '') {
              $("#divReportList").html('');
              $("#divReportList").html(retVal.Param2.replace(/!/g, '"'));
              retVal.Param2 = '';
            }
            pdfFileView(retVal.Param1, false);
          }

        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    $.extend($.ui.dialog.prototype, {
      'addbutton': function (buttonName, func) {
        var buttons = this.element.dialog('option', 'buttons');
        buttons.push({ text: buttonName, click: func });
        //buttons[buttonName] = func; //2014-09-06 Nova bug
        this.element.dialog('option', 'buttons', buttons);
      }
    });

    $.extend($.ui.dialog.prototype, {
      'removebutton': function (buttonName) {
        var tmpbuttons = this.element.dialog('option', 'buttons');
        var buttons = [];
        for (var i in tmpbuttons) {
          if (tmpbuttons[i].text != buttonName) {
            buttons.push({ text: tmpbuttons[i].text, click: tmpbuttons[i].click });
          }
        }
        //delete buttons[buttonName];
        this.element.dialog('option', 'buttons', buttons);
      }
    });

    function viewReportAddButton(btnName, func) {
      $("#dialog-viewReport").dialog('addbutton', btnName, func);
    }

    function viewReportRemoveButton(btnName) {
      $("#dialog-viewReport").dialog('removebutton', btnName);
    }

    function viewReportPrint(url, width, height) {
      if ($.browser.msie) {
        var windowSizeArray = ["width=" + width + ",height=" + height + ",scrollbars=yes"];
        var windowName = "popUp";
        var windowSize = windowSizeArray[0];
        window.open(url, windowName, windowSize);
      } else {
        if ($.browser.chrome) {
          $("#viewReport").width(width + 32);
          $("#viewReport").height(height + 32);
          id = 'viewReport';
          var iframe = document.frames ? document.frames[id] : document.getElementById(id);
          var ifWin = iframe.contentWindow || iframe; iframe.focus(); ifWin.print();
          return false;
        }
        else {
          $("#viewReport").width(width + 32);
          $("#viewReport").height(height + 32);
          $("#viewReport").contents().find(".mainPage").printPage($("#viewReport").contents().find('style'), width);
          id = 'viewReport';
          var iframe = document.frames ? document.frames[id] : document.getElementById(id);
          var ifWin = iframe.contentWindow || iframe; iframe.focus(); ifWin.print();
          return false;
        }
      }
    }

    function sendEmailDocument(htmlUrl, docType) {
      var params = new Object();
      params.DocUrl = htmlUrl;
      params.DocType = docType;
      $.ajax({
        async: false,
        type: "POST",
        url: "ResView.aspx/sendDocumentEmail",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d != null) {

          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function pdfFileView(htmlUrl, docType, emailButton) {
      $("#viewPDF").removeAttr("src");
      var _pdfViewPage = "ViewPDF.aspx"
      $("#viewPDF").attr("src", htmlUrl + "?url=" + encodeURIComponent(htmlUrl));
      $('html').css('overflow', 'hidden');
      var buttons = [];
      if (emailButton) {
        buttons.push({
          text: "E-mail",
          click: function () {
            $(this).dialog('close');

            sendEmailDocument(htmlUrl, docType);

            return true;
          }
        });
      }
      buttons.push({
        text: btnClose,
        click: function () {
          $(this).dialog('close');
          return true;
        }
      });

      $("#dialog-viewPDF").dialog(
      {
        bgiframe: true,
        autoOpen: true,
        modal: true,
        open: function (event, ui) { },
        close: function (event, ui) { $('html').css('overflow', 'auto'); },
        buttons: buttons
      }).dialogExtend({
        "maximize": true,
        "icons": {
          "maximize": "ui-icon-circle-plus",
          "restore": "ui-icon-pause"
        }
      });
      $("#dialog-viewPDF").dialogExtend("maximize");
    }

    function htmlPageRender_btnPrint(htmlUrl, DocName, docNameResource, ResultType) {
      if ($("#viewReport").attr("src").toString().indexOf('/ACE/') > -1) {
        $("#viewReport").removeAttr("src");
        $("#viewReport").attr("src", htmlUrl + "?ResNo=" + resNo + "&docName=" + docNameResource);
      }

      var width = $("#viewReport").contents().find(".mainPage").width();
      if ($("#viewReport").contents().find(".page").length > 0) {
        width = $("#viewReport").contents().find(".page").width();
      }
      else {
        width = $("#viewReport").contents().find("#page1").width();
      }

      var height = $("#viewReport").contents().find(".mainPage").height();
      if ($("#viewReport").contents().find(".page").length > 0) {
        height = $("#viewReport").contents().find(".page").height();
      }
      else {
        height = $("#viewReport").contents().find("#page1").height();
      }

      var param1 = $("#viewReport").contents().find("#param1").val();

      var html = $("#viewReport").contents().find("html");

      viewReportPrint(htmlUrl + "?ResNo=" + resNo + "&docName=" + docNameResource + "&print=1" + (param1 != undefined && param1 != '' ? "&param1=" + param1 : "") + "&" + html.find('input').serialize(), width, height);
    }

    function htmlPageRender_btnNext(htmlUrl, DocName, docNameResource, ResultType, nextHtmlUrl) {
      $("#viewReport").removeAttr("src");
      $("#viewReport").attr("src", nextHtmlUrl + "?ResNo=" + resNo + "&docName=" + docNameResource + "&next=" + htmlUrl);
    }

    function htmlPageRender_btnPDF(htmlUrl, DocName, docNameResource) {
      var params = $("#viewReport").contents().find('input').serialize();

      var width = $("#viewReport").contents().find(".mainPage").width();
      if ($("#viewReport").contents().find(".page").length > 0) {
        width = $("#viewReport").contents().find(".page").width();
      }
      else {
        width = $("#viewReport").contents().find("#page1").width();
      }

      var height = $("#viewReport").contents().find(".mainPage").height();
      if ($("#viewReport").contents().find(".page").length > 0) {
        height = $("#viewReport").contents().find(".page").height();
      }
      else {
        height = $("#viewReport").contents().find("#page1").height();
      }

      var html = $("#viewReport").contents().find("html");
      var htmlInputArray = html.find("input:text");
      $.each(htmlInputArray, function (i) {
        var value = $(this).val();
        $(this).replaceWith("<span>" + value + "</span>");
      });
      var _html = html.html();
      var noPrint = $("#viewReport").contents().find("#noPrintDiv").html();
      var _html = _html.replace(noPrint, '&nbsp;').replace(/"/g, '|').replace(/\\/g, '¨');
      $.ajax({
        async: false,
        type: "POST",
        url: "ResView.aspx/viewPDFReport",
        data: '{"reportType":"' + DocName + '","_html":"' + _html + '","pageWidth":"' + width + '","ResNo":"' + resNo + '","urlBase":"' + htmlUrl + '","pdfConvSerial":"' + $("#pdfConvSerial").val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d != null) {
            $("#viewReport").removeAttr("src");
            var _pdfViewPage = "ViewPDF.aspx"
            $("#viewReport").attr("src", _pdfViewPage + "?url=" + msg.d);
            viewReportRemoveButton(btnPrint);
            viewReportRemoveButton(btnPDF);
            viewReportRemoveButton(btnPrintNext);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
      return false;
    }

    function setViewReportSrc(src) {
      $("#viewReportSrc").val('');
      $("#viewReportSrc").val(src);
    }

    function htmlPageRenderForNextPage(htmlUrl, DocName, docNameResource, ResultType, nextHtmlUrl) {

      $("#viewReport").removeAttr("src");
      $("#viewReport").attr("src", htmlUrl + "?ResNo=" + resNo + "&docName=" + docNameResource);
      window.scrollTo(0, 0);
      $('html').css('overflow', 'hidden');
      $("#dialog-viewReport").dialog(
      {
        bgiframe: true,
        autoOpen: true,
        modal: true,
        open: function (event, ui) {

        },
        close: function (event, ui) {
          $('html').css('overflow', 'auto');
        },
        buttons: [
            {
              text: btnPrint,
              click: function () {
                var baseUrl;
                if ($("#viewReportSrc").val() == '')
                  baseUrl = $.url(htmlUrl);
                else baseUrl = $.url($("#viewReportSrc").val());
                htmlPageRender_btnPrint(baseUrl.data.attr.base + baseUrl.data.attr.path, DocName, docNameResource, ResultType);
              }
            },
            {
              text: btnPDF,
              click: function () {
                htmlPageRender_btnPDF(htmlUrl, DocName, docNameResource, ResultType);
              }
            },
            {
              text: btnClose,
              click: function () {
                $(this).dialog('close');
                return true;
              }
            }
        ]
      }).dialogExtend({
        "maximize": true,
        "icons": {
          "maximize": "ui-icon-circle-plus",
          "restore": "ui-icon-pause"
        }
      });
      $("#dialog-viewReport").dialogExtend("maximize");
    }

    function htmlPageRender(htmlUrl, DocName, docNameResource, ResultType) {

      $("#viewReport").removeAttr("src");
      $("#viewReport").attr("src", htmlUrl + "?ResNo=" + resNo + "&docName=" + docNameResource);
      window.scrollTo(0, 0);
      $('html').css('overflow', 'hidden');
      $("#dialog-viewReport").dialog(
      {
        bgiframe: true,
        autoOpen: true,
        modal: true,
        open: function (event, ui) {
          if (ResultType == 'HTMLPDF') {
            viewReportRemoveButton(btnPrint);
          }
        },
        close: function (event, ui) {
          $('html').css('overflow', 'auto');
        },
        buttons: [
            {
              text: btnPrint,
              click: function () {
                var baseUrl;
                if ($("#viewReportSrc").val() == '')
                  baseUrl = $.url(htmlUrl);
                else baseUrl = $.url($("#viewReportSrc").val());
                htmlPageRender_btnPrint(baseUrl.data.attr.base + baseUrl.data.attr.path, DocName, docNameResource, ResultType);
              }
            }, {
              text: btnPDF,
              click: function () {
                htmlPageRender_btnPDF(htmlUrl, DocName, docNameResource, ResultType);
              }
            }, {
              text: btnClose,
              click: function () {
                if ($("#viewReport").attr("src") != htmlUrl + "?ResNo=" + resNo + "&docName=" + docNameResource) {
                  viewReportAddButton(btnPrint, function () {
                    htmlPageRender_btnPrint(htmlUrl, DocName, docNameResource, ResultType)
                  });
                  viewReportAddButton(btnPDF, function () {
                    htmlPageRender_btnPDF(htmlUrl, DocName, docNameResource, ResultType)
                  });
                  $("#viewReport").removeAttr("src");
                  $("#viewReport").attr("src", htmlUrl + "?ResNo=" + resNo + "&docName=" + docNameResource);
                }
                else {
                  $(this).dialog('close');
                  return true;
                }
              }
            }
        ]
      }).dialogExtend({
        "maximize": true,
        "icons": {
          "maximize": "ui-icon-circle-plus",
          "restore": "ui-icon-pause"
        }
      });
      $("#dialog-viewReport").dialogExtend("maximize");
    }

    function dialogviewReportClose() {
      $("#dialog-viewReport").dialog('close');
    }

    function getPaymentsReturn(refresh) {
      $("#dialog-viewReport").dialog('close');
      if (refresh == true)
        getResData(true);
      }

      function paymentCloseAndRefresh() {
          $("#dialog-viewReport").dialog("close");
          window.location = window.location;
      }

    function getPayments(htmlUrl, DocName) {
      $("#viewReport").removeAttr("src");
      $("#viewReport").attr("src", htmlUrl + "?ResNo=" + resNo);
      window.scrollTo(0, 0);
      $('html').css('overflow', 'hidden');
      $("#dialog-viewReport").dialog(
      {
        bgiframe: true,
        autoOpen: true,
        modal: true,
        open: function (event, ui) { },
        close: function (event, ui) {
          $('html').css('overflow', 'auto');
          if (htmlUrl.indexOf('QuickPayment.aspx') > -1) {
            getResData(true);
          }
        },
        buttons: [{
          text: btnClose,
          click: function () {
            $(this).dialog('close');
            return true;
          }
        }]
      }).dialogExtend({
        "maximize": true,
        "icons": {
          "maximize": "ui-icon-circle-plus",
          "restore": "ui-icon-pause"
        }
      });
      $("#dialog-viewReport").dialogExtend("maximize");
    }

    function printPaximumDocument(url) {
      window.open(url, "_blank", "location=no,toolbar=0,directories=no,status=no,menubar=no");
    }

    function printDocumentFinall(DocName, docUrl, docNameResource, subFolder, resIsBusyControl, languageID, formID, param1, external, emailSender) {
      if (docUrl == 'TVPDF1') {
        window.open("TvReportPage.aspx?DocName=" + DocName, "", "location=no,toolbar=0,directories=no,status=no,menubar=no");
      } else {
        var param = new Object();
        param.DocName = DocName;
        param.DocUrl = docUrl;
        param.SubFolder = subFolder;
        param.MultiLangID = languageID;
        param.param1 = param1;
        param.External = external != undefined ? external : '';
        param.FormID = formID != '' ? parseInt(formID) : null;
        $.ajax({
          async: true,
          type: "POST",
          url: "ResView.aspx/printDocument",
          data: $.json.encode(param),
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (msg) {
            var retVal = $.json.decode(msg.d);
            $("#lastDocName").val(retVal.Param1);
            if (retVal.Param1 == '' && retVal.Param2 != '') {
              showAlert(retVal.Param2);
            }
            if (retVal.JavaScriptName == 'htmlPageRender') {
              htmlPageRender(retVal.Param1, DocName, docNameResource, retVal.ResultType);
            }
            if (retVal.JavaScriptName == 'htmlPageRenderForNextPage') {
              htmlPageRenderForNextPage(retVal.Param1, DocName, docNameResource, retVal.ResultType, retVal.nextHtmlUrl);
            }
            if (retVal.JavaScriptName == 'showResComment') {
              showResComment();
            }
            if (retVal.JavaScriptName == 'showComplaint') {
              addEditComplaint();
            }
            if (retVal.JavaScriptName == 'getPaymentRes') {
              getPaymentRes(retVal.Param1, retVal.Param2);
            }

            if (retVal.JavaScriptName == 'getPayments') {
              getPayments(retVal.Param1, retVal.Param2);
            }

            if (retVal.JavaScriptName == 'deturReport') {
              reportResultShow(retVal.Param1, retVal.Param2, retVal.ResultType, DocName, emailSender);
            }

            if (retVal.JavaScriptName == 'sunriseReport') {
              reportResultListShow(retVal.NewWindow, retVal.Param1, retVal.Param2, DocName, retVal.ResultType);
            }

            if (retVal.JavaScriptName == 'sunriseReportShow') {
              reportResultShow(retVal.Param1, retVal.Param2, retVal.ResultType, DocName, emailSender);
            }
            if (retVal.JavaScriptName == 'EmailSended') {
              EmailSended(retVal.Param1);
            }

            if (retVal.JavaScriptName == 'paximum') {
              printPaximumDocument(retVal.Param1);
            }

            if (retVal.JavaScriptName == 'tvHtml') {

            }
            if (retVal.JavaScriptName == 'FileView') {

            }
          },
          error: function (xhr, msg, e) {
            if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
              showAlert(xhr.responseText);
            }
          },
          statusCode: {
            408: function () {
              logout();
            }
          }
        });
      }
    }

    function getLanguage(DocName, docUrl, docNameResource, subFolder, resIsBusyControl, multiLang, param1, external, emailSender) {
      $("#dialog-SelectReportLang").dialog('close');
      printDocumentFinall(DocName, docUrl, docNameResource, subFolder, resIsBusyControl, multiLang, param1, external, emailSender);
    }

    function getLanguageID(DocName, docUrl, docNameResource, subFolder, resIsBusyControl, formID, param1, external, emailSender) {
      var multiLanguageID = null;
      var param = new Object();
      param.DocCode = DocName;
      $.ajax({
        async: false,
        type: "POST",
        data: $.json.encode(param),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'ResView.aspx/getMultiLangID',
        success: function (msg) {
          var html = '';
          if (msg.hasOwnProperty('d') && msg.d != null && msg.d.length > 0) {
            $("#langListDiv").html('');
            $.each(msg.d, function (i) {
              var clickBtn = '\'' + DocName + '\',\'' + docUrl + '\',\'' + docNameResource + '\',\'' + subFolder + '\',' + resIsBusyControl + ',\'' + this.Id + '\',\'' + param1 + '\',\'' + external + '\',' + (emailSender == true ? true : false);
              $("#langListDiv").append('<a href="#" onclick="getLanguage(' + clickBtn + ')">' + this.Name + '</a><br />');
            });
            $("#dialog").dialog("destroy");
            $("#dialog-SelectReportLang").dialog({
              position: 'center',
              modal: true
            });
          } else {
            multiLanguageID = null;
            printDocumentFinall(DocName, docUrl, docNameResource, subFolder, resIsBusyControl, null, formID, param1, external, emailSender);
          }

        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function printDocument(DocName, docUrl, docNameResource, subFolder, resIsBusyControl, multiLang, formID, param1, external, emailSender) {
      if (multiLang == true && external == '0') {
        getLanguageID(DocName, docUrl, docNameResource, subFolder, resIsBusyControl, formID, param1, external, emailSender);
        return;
      }

      if (resIsBusyControl == true) {
        $.ajax({
          async: true,
          type: "POST",
          data: '{}',
          dataType: "json",
          contentType: "application/json; charset=utf-8",
          url: 'ResView.aspx/reservationIsBusy',
          success: function (msg) {
            if (msg.d != '') {
              showAlert(msg.d);
              return true;
            }
            else {
              printDocumentFinall(DocName, docUrl, docNameResource, subFolder, resIsBusyControl, null, formID, param1, external, emailSender);
            }

          },
          error: function (xhr, msg, e) {
            if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
              showAlert(xhr.responseText);
            }
          },
          statusCode: {
            408: function () {
              logout();
            }
          }
        });
      } else {
        printDocumentFinall(DocName, docUrl, docNameResource, subFolder, resIsBusyControl, null, formID, param1, external, emailSender);
      }
    }

    function readComment(resNo, userID, recID, ci) {
      var paraList = '{"ResNo":"' + resNo + '","UserID":"' + userID + '","RecID":' + recID + ',"lcID":' + ci + '}';
      $.ajax({
        type: "POST",
        data: paraList,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'ResView.aspx/setReadCommentData',
        success: function (msg) {
          $('#lblCommentHeader').html(resNo);
          $('#gridComment').html('');
          $('#gridComment').html(msg.d);
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(lblNotSaved);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }

      });
    }

    function setResStatus() {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/setResStatus",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d == '') {
            getResData(true);
          } else {
            showAlert(msg.d);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function clearOptionTime() {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/clearOption",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d == '') {
            getResData(true);
          } else {
            showAlert(msg.d);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function changeVehicleUnit(vehicleUnit, serviceID) {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/setVehicleUnit",
        data: '{"VehicleUnit":"' + vehicleUnit + '","ServiceID":"' + serviceID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d == '') {
            getResData(true);
          } else {
            showAlert(msg.d);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function changeVehicleCat(vehicleCatID, serviceID) {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/setVehicleCategory",
        data: '{"VehicleCatID":"' + vehicleCatID + '","ServiceID":"' + serviceID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d == '') {
            getResData(true);
          } else {
            showAlert(msg.d);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function changePickup(pickupPoint, serviceID) {
      $.ajax({
        type: "POST",
        url: "ResView.aspx/setPickupPoint",
        data: '{"PickupPoint":"' + pickupPoint + '","ServiceID":"' + serviceID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d == '') {
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function gotoResMonitor(_url) {
      window.location = _url;
    }

    function showPaymentDetail() {
      if ($("#tablePaymentDiv").length > 0) {
        if ($("#tablePayment").css('display') == 'none') {
          $("#tablePayment").show();
          $("#passAmountDiv").hide();
          $("#onOffImg").attr('src', 'Images/infoClose.gif');
        }
        else {
          $("#tablePayment").hide();
          $("#passAmountDiv").show();
          $("#onOffImg").attr('src', 'Images/infoOpen.gif');
        }
      }
    }

    function showReceipt(journalId) {
      getLanguageID("Receipt", "TVPDF", "", "", false, null, journalId, false, false);
      //DocName, docUrl, docNameResource, subFolder, resIsBusyControl, formID, param1, external, emailSender
    }

    window.onunload = function () {
      unLockRes();
    }

    window.onbeforeunload = function () {
      unLockRes();
    }

    function unLockRes() {
      $.ajax({
        async: false,
        type: "POST",
        url: "ResView.aspx/unLockReservation",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d == '') {
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () { logout(); }
        }
      });
    }

    function lockReservation() {
      $.ajax({
        async: false,
        type: "POST",
        url: "ResView.aspx/setLock",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d == '') {
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () { logout(); }
        }
      });
    }

    function setLock() {
        if (resViewTimeout != null) {
            
        var timeDifference = new Date().getTime() - resOpenTime;
        if (timeDifference > (resViewTimeout * 60000)) {

          setTimeout(function () {
            if (!resIsOpen) {
              unLockRes();
              closeReservation();
            }
          }, 15000);

          resIsOpen = false;

          $("#messages").html(lblTimeOutMesage);
          $("#dialog").dialog("destroy");
          $("#dialog-message").dialog({
            position: {
              my: 'center',
              at: 'center'
            },
            modal: true,
            buttons: [{
              text: btnOK,
              click: function () {
                $(this).dialog('close');
                resOpenTime = new Date().getTime();
                lockReservation();
                resIsOpen = true;
              }
            }]
          });
        } else {
          lockReservation();
          resIsOpen = true;
        }
      }
      else {
        lockReservation();
      }
    }

    function ResLockCheck(resNo, pxmResNo) {
      $("#mnAddingService").html('');
      var params = new Object();
      params.resNo = resNo;
      params.pxmResNo = pxmResNo;
      $.ajax({
        async: false,
        type: "POST",
        url: "ResView.aspx/getFormData",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            if (msg.d.hasOwnProperty('ReservationMenu')) {
              resViewTimeout = msg.d.ResViewTimeOut;
              resOpenTime = new Date().getTime();
              var resMenu = msg.d.ReservationMenu;
              var showCloseResButton = msg.d.showCloseResButton;
              if (msg.d.ResCheck == '1') {
                var t = setInterval(function () { setLock() }, 59000);
                getResData(true);
              }
              else if (msg.d.ResCheck == '0') {
                var msg = msg.d.ResCheckMsg;
                showDialog(msg, true, document.referrer);
              } else {
                getResData(true);
              }

              if (showCloseResButton == true) {
                $("#resMonitorUrl").val(msg.d.resMonitorUrl);
                $("#closeReservation").show();
              }

              $("#mnAddingService").html(resMenu);
            }
            else {
              showAlert(msg.d);
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () { logout(); }
        }
      });

    }

    function closeReservation() {
      if ($("#resMonitorUrl").val() != '') {
        location.href = $("#resMonitorUrl").val();
      } else {
        window.history.back();
      }
    }


    function editSpecialCode() {
      $("#iCode1").prop('disabled', false);
      $("#iCode2").prop('disabled', false);
      $("#iCode3").prop('disabled', false);
      $("#iCode4").prop('disabled', false);
      $("#btnEditSpecialCode").prop('disabled', true);
      $("#btnSaveSpecialCode").prop('disabled', false);
    }

    function saveSpecialCode() {
      $("#iCode1").prop('disabled', true);
      $("#iCode2").prop('disabled', true);
      $("#iCode3").prop('disabled', true);
      $("#iCode4").prop('disabled', true);
      $("#btnEditSpecialCode").prop('disabled', false);
      $("#btnSaveSpecialCode").prop('disabled', true);
      var params = new Object();
      params.Code1 = $("#iCode1").val();
      params.Code2 = $("#iCode2").val();
      params.Code3 = $("#iCode3").val();
      params.Code4 = $("#iCode4").val();
      $.ajax({
        async: false,
        type: "POST",
        url: "ResView.aspx/saveSpecialCode",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            if (msg.d.Saved == false) {
              showAlert(msg.d.Message);
            }
            else {

            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showAlert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () { logout(); }
        }
      });
    }

    $(document).ready(function () {

      $.query = $.query.load(location.href);
      resNo = $.query.get('ResNo');
      pxmResNo = $.query.get('PaxResNo');

      $("#dialog-cancelReservation").dialog({ autoOpen: false, modal: true, width: 650, height: 300, resizable: true, autoResize: true });
      $("#dialog-AddTourist").dialog({ autoOpen: false, modal: true, width: 540, height: 490, resizable: true, autoResize: true });
      $("#dialog-resService").dialog({
        autoOpen: false,
        modal: true,
        width: 675,
        height: 400,
        resizable: true,
        autoResize: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog('close'); return true;
          }
        }]
      });
      $("#dialog-resServiceEdit").dialog({ autoOpen: false, modal: true, width: 675, height: 600, resizable: true, autoResize: true });
      $("#dialog-resServiceAdd").dialog({ autoOpen: false, modal: true, width: 675, height: 600, resizable: true, autoResize: true });
      $("#dialog-changeRoomAccom").dialog({ autoOpen: false, modal: true, width: 420, height: 390, resizable: true, autoResize: true });
      $("#dialog-resPayment").dialog({ autoOpen: false, modal: true, width: 1000, height: 600, resizable: true, autoResize: true });
      $("#dialog-changeDate").dialog({ autoOpen: false, modal: true, width: 450, height: 400, resizable: true, autoResize: true });
      $("#dialog-Ssrc").dialog({ autoOpen: false, modal: true, width: 640, height: 550, resizable: true, autoResize: true });
      maximize();
      var maxH = screen.availHeight;
      var height = maxH - 330;
      var divworkArea = document.getElementById('workArea1');
      divworkArea.style.minheight = height;
      ResLockCheck(resNo, pxmResNo);
    });


  </script>

</head>
<body>
  <form id="ResViewForm" runat="server">
    <asp:HiddenField ID="resCheck" runat="server" Value="1" />
    <input id="lastDocName" type="hidden" value="" />
    <input id="resMonitorUrl" type="hidden" value="" />
    <div class="Page">
      <tv1:Header ID="tvHeader" runat="server" />
      <tv1:MainMenu ID="tvMenu" runat="server" />
      <div id="closeReservation" class="ui-helper-hidden ui-helper-clearfix closeReservationDiv">
        <input id="btnCloseRes" type="button" value='<%= GetGlobalResourceObject("ResView", "CloseReservation") %>' onclick="closeReservation()"
          class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
      </div>
      <div id="divSubMenuLiteral" class="ui-helper-clearfix">
        <asp:Literal ID="SubMenuLiteral" runat="server" />
      </div>
      <div id="workArea1" class="Content">
        <div id="divResMain">
          <fieldset>
            <legend>
              <label>
                »</label><strong><%= GetGlobalResourceObject("ResView", "lblResMain") %></strong>
            </legend>
            <div style="text-align: center;">
              <div id="tableResMain"></div>
              <br />
            </div>
          </fieldset>
        </div>
        <div id="divReport">
        </div>
        <div id="divResCust">
          <fieldset>
            <legend>
              <label>
                »</label><strong><%= GetGlobalResourceObject("ResView", "lblGridCust") %></strong>
            </legend>
            <div style="text-align: center;">
              <div id="gridResCust"></div>
            </div>
          </fieldset>
        </div>
        <div id="divResService">
          <fieldset>
            <legend>
              <label>
                »</label><strong><%= GetGlobalResourceObject("ResView", "lblGridService") %></strong>
            </legend>
            <div style="text-align: center;">
              <div id="gridResService"></div>
            </div>
            <div id="divAddingService">
              <div id="mnAddingService" class="ui-helper-clearfix"></div>
            </div>
          </fieldset>
        </div>
        <div id="divResServiceExt">
          <br />
          <fieldset>
            <legend>
              <label>
                »</label><strong><%= GetGlobalResourceObject("ResView", "lblGridResServiceExt") %></strong>
            </legend>
            <div style="text-align: center;">
              <div id="gridResServiceExt"></div>
            </div>
            <br />
          </fieldset>
        </div>
        <div id="divResSupDis">
        </div>
        <div id="divResPayment">
        </div>
        <%--<div class="tampon">
            </div>--%>
      </div>
      <div class="Footer">
        <tv1:Footer ID="tvfooter" runat="server" />
      </div>
    </div>
    <div id="dialog-seatSelect" title='<%= GetGlobalResourceObject("ResView", "lblEditService") %>'
      style="display: none;">
      <iframe id="seatSelect" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-resCustInfo" title='<%= GetGlobalResourceObject("ResView", "lblCustomerInfo") %>'
      style="display: none;">
      <iframe id="resCustInfo" runat="server" height="580" width="750px" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-resService" title='<%= GetGlobalResourceObject("ResView", "lblViewService") %>'
      class="ui-helper-hidden">
      <iframe id="resService" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-resServiceEdit" title='<%= GetGlobalResourceObject("ResView", "lblEditService") %>'
      style="display: none;">
      <iframe id="resServiceEdit" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-resServiceAdd" title='<%= GetGlobalResourceObject("ResView", "lblAddingService") %>'
      style="display: none;">
      <iframe id="resServiceAdd" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-resServiceExtAdd" title='<%= GetGlobalResourceObject("ResView", "lblAddingExtraService") %>'
      style="display: none;">
      <iframe id="resServiceExtAdd" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-cancelReservation" title='<%= GetGlobalResourceObject("ResView", "lblCancelReservation") %>'
      style="display: none;">
      <iframe id="cancelReservation" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-AddTourist" title='<%= GetGlobalResourceObject("ResView", "lblAddingTurist") %>'
      style="display: none;">
      <iframe id="AddTourist" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-paymentPlan" title='<%= GetGlobalResourceObject("ResView", "lblPaymetPlan") %>'
      style="display: none;">
      <span id="divPaymentPlan"></span>
    </div>
    <div id="dialog-message" title='<%= GetGlobalResourceObject("ResView", "lblMessage") %>'
      style="display: none;">
      <span id="messages">Message</span>
    </div>
    <div id="dialog-changeRoomAccom" title='<%= GetGlobalResourceObject("ResView", "lblChangeRoomOrAccommodation") %>'
      style="display: none;">
      <iframe id="changeRoomAccom" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-resPayment" title='<%= GetGlobalResourceObject("ResView", "lblPaymentReservation") %>'
      style="display: none;">
      <iframe id="resPayment" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-changeDate" title='<%= GetGlobalResourceObject("ResView", "lblChangeDate") %>'
      style="display: none;">
      <iframe id="changeDate" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-resComment" title='<%= GetGlobalResourceObject("ResMonitor", "titleComment")%>'
      style="display: none;">
      <resCom1:ResComment ID="resComment" runat="server" />
    </div>
    <div id="dialog-Ssrc" title='<%= GetGlobalResourceObject("ResView","lblSpecialServiceReqCode") %>'
      style="display: none;">
      <iframe id="Ssrc" runat="server" height="100%" width="100%" frameborder="0" style="clear: both;"></iframe>
    </div>
    <div id="dialog-changeLeader" title='<%= GetGlobalResourceObject("ResView", "btnChangeLeader")%>'
      style="display: none;">
      <div id="divChangeLeader">
      </div>
    </div>
    <div id="dialog-reportList" title='' style="display: none;">
      <div id="divReportList">
      </div>
    </div>
    <div id="dialog-viewReport" title='' style="display: none;">
      <iframe id="viewReport" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
      <input type="hidden" id="viewReportSrc" />
    </div>
    <div id="dialog-viewPDF" title='' style="display: none;">
      <iframe id="viewPDF" runat="server" height="100%" width="100%" frameborder="0" style="clear: both;"></iframe>
    </div>
    <div id="dialog-AddingCustsServices" title='' style="display: none;">
      <h1>
        <%= GetGlobalResourceObject("ResView", "msgAdding") %></h1>
      <br />
      <div id="divAddingCustsServices" style="width: 100%;">
      </div>
    </div>
    <div id="dialog-SelectReportLang" title='<%= GetGlobalResourceObject("ResView", "headerLanguageSelect") %>' style="display: none;">
      <div id="langListDiv">
      </div>
    </div>
    <%--Add / Edit Complaint--%>
    <div id="dialog-ViewEditComplaint" title='Add / Edit Complaint'
      style="display: none; text-align: center;">
      <iframe id="ViewEditComplaint" runat="server" height="100%" width="1000px" frameborder="0"
        style="clear: both; text-align: left;"></iframe>
    </div>
    <%--Add / Edit Complaint--%>
    <asp:HiddenField ID="pdfConvSerial" runat="server" Value="RG92ZHVkdXF0cGRxanRkd3VqdXZqfX19fQ==" />
  </form>
  <div id="dialog-passportScan" title='Read Passport Data' style="display: none;">
    <iframe id="passportScan" height="100%" width="550" frameborder="0" style="clear: both;"></iframe>
  </div>
</body>
</html>
