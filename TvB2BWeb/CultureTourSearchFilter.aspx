﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CultureTourSearchFilter.aspx.cs"
    Inherits="CultureTourSearchFilter" EnableEventValidation="false" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "CultureTourSearchFilter") %></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <script src="Scripts/jquery.textTruncate.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/PriceSearchFilter.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">

        var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
        var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
        var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
        var btnClear = '<%= GetGlobalResourceObject("LibraryResource", "btnClear") %>';
        var ComboSelect = '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
        var addPleaseSelectDeparture = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectDeparture") %>';
        var myWidth = 0, myHeight = 0;

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }


        $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '</h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        function reSizeFrame() {
            self.parent.reSizeFilterFrame($('body').height());
        }

        function showDialogYesNo(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: [{
                        text: btnOK,
                        click: function () {
                            $(this).dialog('close');
                            return true;
                        }
                    }, {
                        text: btnCancel,
                        click: function () {
                            $(this).dialog('close');
                            return false;
                        }
                    }]
                });
            });
        }

        function showDialog(msg) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                        return true;
                    }
                }]
            });
        }

        function showMessage(msg, transfer, trfUrl) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: [{
                        text: btnOK,
                        click: function () {
                            $(this).dialog('close');
                            return true;
                        }
                    }, {
                        text: btnCancel,
                        click: function () {
                            $(this).dialog('close');
                            return false;
                        }
                    }]
                });
            });
        }

        function dateChange(_TextBox, _PopCal) {
            var iDate = $("#hfDate");
            if ((!_TextBox) || (!_PopCal)) return
            var _format = _TextBox.getAttribute("Format");
            var _Date = _PopCal.getDate(_TextBox.value, _format);
            if (_Date) {
                iDate.val(Date.parse(_Date));
            }
            else {
                iDate.val(Date.parse(new Date()));
            }
        }


        function getInf(adult, whois) {
            var Adult = parseInt(adult);
            $("#fltInfant" + whois).html("");
            for (i = 0; i < Adult + 1; i++) {
                $("#fltInfant" + whois).append("<option value='" + i.toString() + "'>" + i.toString() + "</option>");
            }
        }

        function SetChild(childCount, whois) {
            if (childCount > 0) $("#divRoomInfoChd1" + whois).show(); else $("#divRoomInfoChd1" + whois).hide();
            if (childCount > 1) $("#divRoomInfoChd2" + whois).show(); else $("#divRoomInfoChd2" + whois).hide();
            if (childCount > 2) $("#divRoomInfoChd3" + whois).show(); else $("#divRoomInfoChd3" + whois).hide();
            if (childCount > 3) $("#divRoomInfoChd4" + whois).show(); else $("#divRoomInfoChd4" + whois).hide();
        }

        function getChild(child, whois) {
            var _divChild = $("#divRoomInfoChd" + whois);
            if (parseInt(child) == 0) {
                SetChild(parseInt(child), whois);
                _divChild.hide();
            }
            else {
                _divChild.show();
                SetChild(parseInt(child), whois);
            }
        }

        function onChildChange(whois) {
            getChild($("#fltChild" + whois).val(), whois);
            reSizeFrame();
        }


        function getRoomPaxs() {
            var roomCount = parseInt($("#fltRoomCount").val());
            var result = '';
            for (i = 1; i < roomCount + 1; i++) {
                var adult = parseInt($("#fltAdult" + i.toString()).val());
                var child = parseInt($("#fltChild" + i.toString()).val());
                var chd1Age = parseInt($("#fltRoomInfoChd1" + i.toString()).val());
                var chd2Age = parseInt($("#fltRoomInfoChd2" + i.toString()).val());
                var chd3Age = parseInt($("#fltRoomInfoChd3" + i.toString()).val());
                var chd4Age = parseInt($("#fltRoomInfoChd4" + i.toString()).val());
                if (i > 1)
                    result += ',(';
                else result += '(';
                result += '|Adult|:|' + adult.toString() + '|';
                result += ',|Child|:|' + child.toString() + '|';
                result += ',|Chd1Age|:|' + (child > 0 ? chd1Age : -1) + '|';
                result += ',|Chd2Age|:|' + (child > 1 ? chd2Age : -1) + '|';
                result += ',|Chd3Age|:|' + (child > 2 ? chd3Age : -1) + '|';
                result += ',|Chd4Age|:|' + (child > 3 ? chd4Age : -1) + '|';
                result += ')';
            }
            var hfRoomInfo = $("#hfRoomInfo");
            hfRoomInfo.val(result);
            reSizeFrame();
        }

        function getArrival(_first) {
            $.ajax({
                async: false,
                type: "POST",
                url: "CultureTourSearchFilter.aspx/getArrival",
                data: '{"DepCity":"' + $("#fltDepCity").val() + '","Direction":"' + $("#fltDirection").val() + '","HolPackCat":"' + $("#fltCategory").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#fltArrCity").html("");
                    $("#fltArrCity").append("<option value='' >" + ComboSelect + "</option>");
                    var country = '';
                    var first = true;
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        $.each($.json.decode(msg.d), function (i) {
                            $("#fltArrCity").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getHolpack() {
            $.ajax({
                type: "POST",
                url: "CultureTourSearchFilter.aspx/getHolpack",
                data: '{"DepCity":"' + $("#fltDepCity").val() + '","ArrCity":"' + $("#fltArrCity").val() + '","Direction":"' + $("#fltDirection").val() + '","HolPackCat":"' + $("#fltCategory").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#fltHolpack").html("");
                    $("#fltHolpack").append("<option value=';;' >" + ComboSelect + "</option>");
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        $.each($.json.decode(msg.d), function (i) {
                            $("#fltHolpack").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changeDepCity() {
            $("#txtResort").html(''); $("#hfResort").val('');
            $("#txtCategory").html(''); $("#hfCategory").val('');
            $("#txtHotel").html(''); $("#hfHotel").val('');
            $("#txtRoom").html(''); $("#hfRoom").val('');
            $("#txtBoard").html(''); $("#hfBoard").val('');

            $("#divListPopup").html('');
            getArrival(false);            
            setFlightDays();
        }

        function changeHolpack() {
            var holpackCode = $("#fltHolpack").val();            
            $("#txtHotel").html(''); $("#hfHotel").val('');
            $("#txtRoom").html(''); $("#hfRoom").val('');
            $("#txtBoard").html(''); $("#hfBoard").val('');
            $("#hfHolPack").val(holpackCode.split(';')[0]);
            getDeparture();
            $("#fltDepCity").val(holpackCode.split(';')[1]);
            getArrival(false);            
            $("#fltArrCity").val(holpackCode.split(';')[2]);
            setFlightDays();            
        }

        function setFlightDays() {
            $("#ppcCheckIn").attr('disabled', 'disabled');
            var params = new Object();
            params.DepCity = $("#fltDepCity").val();
            params.ArrCity = $("#fltArrCity").val();
            params.Direction = $("#fltDirection").val();
            params.HolPackCat = $("#hfHolPack").val();

            $.ajax({
                type: "POST",
                url: "CultureTourSearchFilter.aspx/getFlightDays",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var objPopCalId = PopCalGetCalendarIndex('ppcCheckIn');
                    objPopCal = objPopCalList[objPopCalId];
                    objPopCal.Holidays = [];
                    objPopCal.HolidaysCounter = 0;
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = msg.d;
                        if (data != null)
                            $.each(data, function (i) {
                                PopCalAddSpecialDay(this.Day, this.Month, this.Year, this.Text, 0);
                            });
                    }
                    $("#ppcCheckIn").removeAttr('disabled');
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                    $("#ppcCheckIn").removeAttr('disabled');
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changeArrCity() {
            $("#txtResort").html(''); $("#hfResort").val('');
            $("#txtCategory").html(''); $("#hfCategory").val('');
            $("#txtHotel").html(''); $("#hfHotel").val('');
            $("#txtRoom").html(''); $("#hfRoom").val('');
            $("#txtBoard").html(''); $("#hfBoard").val('');

            $("#divListPopup").html('');
            setFlightDays();
            //getHolpack();
        }

        function getDeparture() {
            $.ajax({
                async: false,
                type: "POST",
                url: "CultureTourSearchFilter.aspx/getDeparture",
                data: '{"Direction":"' + $("#fltDirection").val() + '","HolPackCat":"' + $("#fltCategory").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#fltDepCity").html("");
                    $("#fltDepCity").append("<option value='' >" + ComboSelect + "</option>");
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        if (data != null)
                            $.each(data, function (i) {
                                $("#fltDepCity").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
                            });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        String.prototype.ltrim = function () {
            return this.replace(/^\s+/, "");
        }

        function autoCompletteChange(key) {
            var autoCList = $('div[name$="listPopupRow"]');
            $.each(autoCList, function (i) {
                if (key != '') {
                    if ($.browser.msie) {
                        if (this.innerText.ltrim().toLowerCase().indexOf(key.toLowerCase()) > -1) //.substring(0, key.toString().length).toLowerCase() == key.toLowerCase())
                            $(this).show();
                        else $(this).hide();
                    }
                    else {
                        if (this.textContent.ltrim().toLowerCase().indexOf(key.toLowerCase()) > -1) //.substring(0, key.toString().length).toLowerCase() == key.toLowerCase())
                            $(this).show();
                        else $(this).hide();
                    }
                } else $(this).show();
            });
        }

        function listPopupFillData(sourceBtn, clear) {
            var depCity = $("#fltDepCity").val();
            var arrCity = $("#fltArrCity").val();
            var resort = $("#hfResort").val();
            var category = $("#hfCategory").val();
            var hotel = $("#hfHotel").val();
            var room = $("#hfRoom").val();
            var board = $("#hfBoard").val();
            var holpack = $("#fltHolpack") != null && $("#fltHolpack").val() != '' > 0 ? $("#fltHolpack").val().split(';')[0] : '';
            var paraList = '';
            var serviceUrl = '';
            switch (sourceBtn) {
                case 'resort':
                    paraList = '{"ArrCity":"' + arrCity + '","Direction":"' + $("#fltDirection").val() + '","HolPackCat":"' + $("#fltCategory").val() + '"}';
                    serviceUrl = 'CultureTourSearchFilter.aspx/getResortData';
                    $.ajax({
                        async: false,
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: serviceUrl,
                        success: function (msg) {
                            $("#txtCategory").html(''); $("#hfCategory").val('');
                            $("#txtHotel").html(''); $("#hfHotel").val('');
                            $("#txtRoom").html(''); $("#hfRoom").val('');
                            $("#txtBoard").html(''); $("#hfBoard").val('');
                            if (msg.hasOwnProperty('d') && msg.d != '') {
                                var inputDataArray = msg.d;
                                var listChk = $("#hfResort");
                                var _input = '';
                                var mydata = inputDataArray;
                                if (mydata != null)
                                    $.each(mydata, function (index, inputData) {
                                        var checked = '';
                                        if (listChk.val().length > 0) {
                                            var chkListStr = listChk.val();
                                            var chkList = chkListStr.split('|');
                                            var _chkList = $.json.encode(chkList);

                                            if (chkList.length > 0 && _chkList.indexOf(inputData.RecID.toString()) > 0)
                                                checked = 'checked=\'checked\'';
                                            else checked = '';
                                        } else checked = '';
                                        _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.RecID + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
                                    });
                                $("#divListPopup").html('');
                                $("#divListPopup").html(_input);
                            }
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                showDialog(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                    break;
                case 'category':
                    paraList = '{"ArrCity":"' + arrCity + '","Resort":"' + resort + '","Direction":"' + $("#fltDirection").val() + '","HolPackCat":"' + $("#fltCategory").val() + '"}';
                    serviceUrl = 'CultureTourSearchFilter.aspx/getCategoryData';
                    $.ajax({
                        async: false,
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: serviceUrl,
                        success: function (msg) {
                            $("#txtHotel").html(''); $("#hfHotel").val('');
                            $("#txtRoom").html(''); $("#hfRoom").val('');
                            $("#txtBoard").html(''); $("#hfBoard").val('');
                            if (msg.hasOwnProperty('d') && msg.d != '') {
                                var inputDataArray = msg.d;
                                var listChk = $("#hfCategory");
                                var _input = '';
                                var mydata = $.json.decode(inputDataArray);
                                if (mydata != null)
                                    $.each(mydata, function (index, inputData) {
                                        var checked = '';
                                        if (listChk.val().length > 0) {
                                            var chkListStr = listChk.val();
                                            var chkList = chkListStr.split('|');
                                            if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                                                checked = 'checked=\'checked\'';
                                            else checked = '';
                                        } else checked = '';
                                        _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
                                    });
                                $("#divListPopup").html('');
                                $("#divListPopup").html(_input);
                            }
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                showDialog(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                    break;
                case 'hotel':
                    paraList = '{"ArrCity":"' + arrCity + '","Resort":"' + resort + '","Category":"' + category + '","Holpack":"' + holpack + '","Direction":"' + $("#fltDirection").val() + '","HolPackCat":"' + $("#fltCategory").val() + '"}';
                    serviceUrl = 'CultureTourSearchFilter.aspx/getHotelData';
                    $.ajax({
                        async: false,
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: serviceUrl,
                        success: function (msg) {
                            if (clear == true) $("#hfHotel").val('');
                            $("#txtRoom").html(''); $("#hfRoom").val('');
                            $("#txtBoard").html(''); $("#hfBoard").val('');
                            if (msg.hasOwnProperty('d') && msg.d != '') {
                                var inputDataArray = msg.d;
                                var listChk = $("#hfHotel");
                                var _input = '';
                                var mydata = $.json.decode(inputDataArray);
                                if (mydata != null) {
                                    _input += '<input type="text" id="hotelAutoComplette"  onkeyup="autoCompletteChange(this.value);" >';
                                    $.each(mydata, function (index, inputData) {
                                        var checked = '';
                                        if (listChk.val().length > 0) {
                                            var chkListStr = listChk.val();
                                            var chkList = chkListStr.split('|');
                                            if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                                                checked = 'checked=\'checked\'';
                                            else checked = '';
                                        } else checked = '';

                                        if (index % 2)
                                            _input += '<div class="divListPopupRow" name="listPopupRow" id="divListPopupRow_' + index.toString() + '">';
                                        else _input += '<div class="divListPopupRowAlter" name="listPopupRow" id="divListPopupRow_' + index.toString() + '">';
                                        _input += '<div class="divListPopupleftDiv">';
                                        _input += " <input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " hotelName=\"" + inputData.Name + "(" + inputData.Category + ")" + "\" />";
                                        _input += '</div>';
                                        _input += '<div class="divListPopuprightDiv">';
                                        _input += '<div class=\"tranCss\">';
                                        _input += " <label for='" + sourceBtn + "_" + index.toString() + "'><b>" + inputData.Name + "(" + inputData.Category + ")" + "</b></label>";
                                        _input += "<br />" + inputData.LocationName;
                                        _input += '</div>';
                                        _input += '</div>';
                                        _input += '</div>';
                                    });
                                }
                                $("#divListPopup").html('');
                                $("#divListPopup").html(_input);
                                if (mydata != null) { $("#hotelAutoComplette").focus(); }
                            }
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                showDialog(xhr.responseText);
                        }
                    });
                    break;
                case 'room':
                    paraList = '{"DepCity":"' + depCity + '","ArrCity":"' + arrCity + '","Resort":"' + resort + '","Hotel":"' + hotel + '","Direction":"' + $("#fltDirection").val() + '","HolPackCat":"' + $("#fltCategory").val() + '"}';
                    serviceUrl = 'CultureTourSearchFilter.aspx/getRoomData';
                    $.ajax({
                        async: false,
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: serviceUrl,
                        success: function (msg) {
                            $("#txtBoard").html(''); $("#hfBoard").val('');
                            if (msg.hasOwnProperty('d') && msg.d != '') {
                                var inputDataArray = msg.d;
                                var listChk = $("#hfRoom");
                                var _input = '';
                                var mydata = $.json.decode(inputDataArray);
                                if (mydata != null)
                                    $.each(mydata, function (index, inputData) {
                                        var checked = '';
                                        if (listChk.val().length > 0) {
                                            var chkListStr = listChk.val();
                                            var chkList = chkListStr.split('|');
                                            if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                                                checked = 'checked=\'checked\'';
                                            else checked = '';
                                        } else checked = '';

                                        _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
                                    });
                                $("#divListPopup").html('');
                                $("#divListPopup").html(_input);
                            }
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                showDialog(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                    break;
                case 'board':
                    paraList = '{"DepCity":"' + depCity + '","ArrCity":"' + arrCity + '","Resort":"' + resort + '","Hotel":"' + hotel + '","Direction":"' + $("#fltDirection").val() + '","HolPackCat":"' + $("#fltCategory").val() + '"}';
                    serviceUrl = 'CultureTourSearchFilter.aspx/getBoardData';
                    $.ajax({
                        async: false,
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: serviceUrl,
                        success: function (msg) {
                            if (msg.hasOwnProperty('d') && msg.d != '') {
                                var inputDataArray = msg.d;
                                var listChk = $("#hfBoard");
                                var _input = '';
                                var mydata = $.json.decode(inputDataArray);
                                if (mydata != null)
                                    $.each(mydata, function (index, inputData) {
                                        var checked = '';
                                        if (listChk.val().length > 0) {
                                            var chkListStr = listChk.val();
                                            var chkList = chkListStr.split('|');
                                            if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                                                checked = 'checked=\'checked\'';
                                            else checked = '';
                                        } else checked = '';

                                        _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
                                    });
                                $("#divListPopup").html('');
                                $("#divListPopup").html(_input);
                            }
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                showDialog(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                    break;
                default: break;
            }
            reSizeFrame();
        }

        function listPopupGetSelected(sourceBtn) {
            switch (sourceBtn) {
                case 'resort':
                    var hfResort = $("#hfResort");
                    var iResort = $("#txtResort");
                    var listChk = '';
                    var listTxt = '';
                    iResort.text('');
                    $(":checked", "#divListPopup").each(function () {
                        if (listChk.length > 0) {
                            listChk += '|';
                            listTxt += '<br />';
                        }
                        listChk += $(this).val();
                        listTxt += $(this).next().text();
                    });
                    hfResort.val(listChk);
                    iResort.html(listTxt);
                    break;
                case 'category':
                    var hfCategory = $("#hfCategory");
                    var iCategory = $("#txtCategory");
                    var listChk = '';
                    var listTxt = '';
                    iCategory.text('');
                    $(":checked", "#divListPopup").each(function () {
                        if (listChk.length > 0) {
                            listChk += '|';
                            listTxt += '<br />';
                        }
                        listChk += $(this).val();
                        listTxt += $(this).next().text();
                    });

                    hfCategory.val(listChk);
                    iCategory.html(listTxt);
                    break;
                case 'hotel':
                    var hfHotel = $("#hfHotel");
                    var iHotel = $("#txtHotel");
                    var listChk = '';
                    var listTxt = '';
                    iHotel.text('');
                    $(":checked", "#divListPopup").each(function () {
                        if (listChk.length > 0) {
                            listChk += '|';
                            listTxt += '<br />';
                        }
                        listChk += $(this).val();
                        listTxt += $(this).attr("hotelName");
                    });

                    hfHotel.val(listChk);
                    iHotel.html(listTxt);
                    break;
                case 'room':
                    var hfRoom = $("#hfRoom");
                    var iRoom = $("#txtRoom");
                    var listChk = '';
                    var listTxt = '';
                    iRoom.text('');
                    $(":checked", "#divListPopup").each(function () {
                        if (listChk.length > 0) {
                            listChk += '|';
                            listTxt += '<br />';
                        }
                        listChk += $(this).val();
                        listTxt += $(this).next().text();
                    });

                    hfRoom.val(listChk);
                    iRoom.html(listTxt);
                    break;
                case 'board':
                    var hfBoard = $("#hfBoard");
                    var iBoard = $("#txtBoard");
                    var listChk = '';
                    var listTxt = '';
                    iBoard.text('');
                    $(":checked", "#divListPopup").each(function () {
                        if (listChk.length > 0) {
                            listChk += '|';
                            listTxt += '<br />';
                        }
                        listChk += $(this).val();
                        listTxt += $(this).next().text();
                    });
                    hfBoard.val(listChk);
                    iBoard.html(listTxt);
                    break;
                default: break;
            }
            reSizeFrame();
        }

        function showListPopupAddButton(btnName, func) {
            $.extend($.ui.dialog.prototype, {
                'addbutton': function (buttonName, func) {
                    var buttons = this.element.dialog('option', 'buttons');
                    buttons[buttonName] = func;
                    this.element.dialog('option', 'buttons', buttons);
                }
            });
            $("#dialog-listPopup").dialog('addbutton', btnName, func);
        }

        function showListPopup(_title, sourceBtn) {
            $("#divListPopup").html('');
            listPopupFillData(sourceBtn, false);
            $(function () {
                $("#dialog").dialog("destroy");
                $("#dialog-listPopup").dialog({
                    title: _title,
                    modal: true,
                    height: 400,
                    width: 240,
                    buttons: [{
                        text: btnOK,
                        click: function () {
                            listPopupGetSelected(sourceBtn);
                            $(this).dialog('close');
                            return true;
                        }
                    }, {
                        text: btnCancel,
                        click: function () {
                            $(this).dialog('close');
                            return false;
                        }
                    }]
                }).css('font-size', '7px');

                if ($("#CustomRegID").val() != '0970301') {
                    showListPopupAddButton(btnClear, function () {
                        $("#divListPopup").html('');
                        listPopupFillData(sourceBtn, true);
                        return false;
                    });
                }
            });
            reSizeFrame();
        }

        function getRoomInfo(setCrt) {
            var roomCount = $("#fltRoomCount").val();
            $.ajax({
                async: false,
                type: "POST",
                url: "CultureTourSearchFilter.aspx/getRoomInfo",
                data: '{"RoomCount":"' + roomCount + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#fltRoomInfo").html('');
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        $("#fltRoomInfo").html(msg.d);
                    }
                    reSizeFrame();
                    if (setCrt == true)
                        setCriteria();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function filterclear() {
            self.parent.pageLoad();
        }

        function setCriteria() {
            getRoomPaxs();
            var data = new Object();
            data.CheckIn = $("#hfDate").val();
            data.ExpandDate = $("#fltCheckInDay").val();
            data.NightFrom = $("#fltNight1").val();
            data.NightTo = $("#fltNight2").val();
            data.RoomCount = $("#fltRoomCount").val();
            data.roomsInfo = $("#hfRoomInfo").val();
            data.Board = $("#hfBoard").val();
            data.Room = $("#hfRoom").val();
            data.DepCity = $("#fltDepCity").val();
            data.ArrCity = $("#fltArrCity").val();
            data.Resort = $("#hfResort").val();
            data.Category = $("#hfCategory").val();
            data.Hotel = $("#hfHotel").val();
            data.CurControl = $("#CurrencyConvert")[0].checked ? 'Y' : 'N';
            data.CurrentCur = $("#currentCur").val();
            data.ShowStopSaleHotels = $("#showStopSale")[0].checked ? 'Y' : 'N';
            data.HolPack = $("#hfHolPack").val();
            data.Direction = $("#fltDirection").val();
            data.HolPackCat = $("#fltCategory").val();
            $.ajax({
                type: "POST",
                url: "CultureTourSearchFilter.aspx/SetCriterias",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function search() {
            getRoomPaxs();
            var data = new Object();
            data.CheckIn = $("#hfDate").val();
            data.ExpandDate = $("#fltCheckInDay").val();
            data.NightFrom = $("#fltNight1").val();
            data.NightTo = $("#fltNight2").val();
            data.RoomCount = $("#fltRoomCount").val();
            data.roomsInfo = $("#hfRoomInfo").val();
            data.Board = $("#hfBoard").val();
            data.Room = $("#hfRoom").val();
            data.DepCity = $("#fltDepCity").val();
            data.ArrCity = $("#fltArrCity").val();
            data.Resort = $("#hfResort").val();
            data.Category = $("#hfCategory").val();
            data.Hotel = $("#hfHotel").val();
            data.CurControl = $("#CurrencyConvert")[0].checked ? 'Y' : 'N';
            data.CurrentCur = $("#currentCur").val();
            data.ShowStopSaleHotels = $("#showStopSale")[0].checked ? 'Y' : 'N';
            data.HolPack = $("#hfHolPack").val();
            data.Direction = $("#fltDirection").val();
            data.HolPackCat = $("#fltCategory").val();

            if ($("#hfDepCityCompulsory").val() == 'true') {
                if ($("#fltDepCity").val() == '') {
                    showDialog(addPleaseSelectDeparture);
                    return false;
                }
            }
            $.ajax({
                type: "POST",
                url: "CultureTourSearchFilter.aspx/SetCriterias",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    self.parent.searchPrice();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
            reSizeFrame();
        }

        function getStopSaleHotels(showStopSale) {
            if (showStopSale == true)
                $("#showStopSale").attr("checked", "checked");
        }

        function getFormData() {
            $.ajax({
                type: "POST",
                url: "CultureTourSearchFilter.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        if (data == null || data == undefined) {
                            self.location = self.location;
                            return false;
                        }
                        if (data.CustomRegID == '0970301' && data.Market == 'SWEMAR') {
                            $("#divNight2").hide();
                        }
                        if (data.CustomRegID != '0970301') {
                            $("#hfDepCityCompulsory").val('true');
                        }
                        else {
                            $("#hfDepCityCompulsory").val('false');
                        }
                        $("#fltCheckInDay").html(''); $("#fltCheckInDay").html(data.fltCheckInDay);
                        $("#fltNight1").html(''); $("#fltNight1").html(data.fltNight);
                        $("#fltNight2").html(''); $("#fltNight2").html(data.fltNight2);
                        $("#fltRoomCount").html(''); $("#fltRoomCount").html(data.fltRoomCount);
                        if (data.DisableRoomFilter == true) {
                            $("#fltRoomCountDiv").show();
                        }
                        else {
                            $("#fltRoomCountDiv").hide();
                        }
                        $("#divCurrency").html('');
                        $("#divCurrency").html(data.MarketCur);
                        $("#hfDate").val('');
                        $("#hfDate").val(data.CheckIn);
                        var _date1 = new Date(parseInt(data.CheckIn) + 86400000);
                        $("#iCheckIn").val($.format.date(_date1, data.DateFormat));
                        if (data.ShowExpandDay == false) $("#divExpandDate").hide();
                        if (data.ShowSecondDay == false) $("#divNight2").hide();
                        getDeparture();
                        getArrival(true);
                        getRoomInfo(true);
                        if (data.CustomRegID == '0970301' || data.CustomRegID == '1076601') {
                            $("#divStopSaleHotels").hide();
                        }
                        else getStopSaleHotels(data.ShowStopSaleHotels);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changedNight1() {
            $("#fltNight2").val($("#fltNight1").val());
        }

        function changedNight2() {
            var night1 = parseInt($("#fltNight1").val());
            var night2 = parseInt($("#fltNight2").val());
            if (night1 > night2)
                $("#fltNight2").val($("#fltNight1").val());
        }

        function changeTourCategory() {
            $("#txtResort").html(''); $("#hfResort").val('');
            $("#txtCategory").html(''); $("#hfCategory").val('');
            $("#txtHotel").html(''); $("#hfHotel").val('');
            $("#txtRoom").html(''); $("#hfRoom").val('');
            $("#txtBoard").html(''); $("#hfBoard").val('');
            getHolpack();
            getDeparture();
        }

        function changeDirection() {
            $.ajax({
                type: "POST",
                url: "CultureTourSearchFilter.aspx/getHolPackCat",
                data: '{"Direction":"' + $("#fltDirection").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null && msg.d != '') {
                        $("#fltCategory").html("");
                        $("#fltCategory").append("<option value='' >" + ComboSelect + "</option>");
                        if (msg.hasOwnProperty('d') && msg.d != '') {
                            $.each($.json.decode(msg.d), function (i) {
                                $("#fltCategory").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                            });
                        }
                        $("#txtResort").html(''); $("#hfResort").val('');
                        $("#txtCategory").html(''); $("#hfCategory").val('');
                        $("#txtHotel").html(''); $("#hfHotel").val('');
                        $("#txtRoom").html(''); $("#hfRoom").val('');
                        $("#txtBoard").html(''); $("#hfBoard").val('');
                        getDeparture();
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        $(document).ready(function () {
            getFormData();
        });
    </script>

</head>
<body>
    <form id="SearchFilterForm" runat="server">
        <div style="width: 250px;">
            <table cellpadding="2" cellspacing="0" style="text-align: left; width: 250px;">
                <tr>
                    <td width="100%" valign="top">
                        <strong>
                            <%= GetGlobalResourceObject("PackageSearchFilter", "lblDirection") %></strong>
                        <br />
                        <select id="fltDirection" onchange="changeDirection();">
                            <option value="">
                                <%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %></option>
                            <option value="0">
                                <%= GetGlobalResourceObject("PackageSearchFilter", "liAbroad")%></option>
                            <option value="1">
                                <%= GetGlobalResourceObject("PackageSearchFilter", "liDomestic")%></option>
                            <option value="9">
                                <%= GetGlobalResourceObject("PackageSearchFilter", "liOther")%></option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="100%" valign="top">
                        <strong>
                            <%= GetGlobalResourceObject("PackageSearchFilter", "lblTourCategory")%></strong>
                        <br />
                        <select id="fltCategory" onchange="changeTourCategory();">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <%= GetGlobalResourceObject("PackageSearchFilter", "lblHolpack")%></strong>
                        <br />
                        <select id="fltHolpack" onchange="changeHolpack();">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td width="100%" valign="top">
                        <strong>
                            <%= GetGlobalResourceObject("PackageSearchFilter", "lblDepCity") %></strong>
                        <br />
                        <select id="fltDepCity" onchange="changeDepCity();">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <%= GetGlobalResourceObject("PackageSearchFilter", "lblArrCity") %></strong>
                        <br />
                        <select id="fltArrCity" onchange="changeArrCity();">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>

                <tr>
                    <td>
                        <div id="divCheckIn" style="width: 148px; float: left;">
                            <strong>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblCheckIn") %></strong>
                            <br />
                            <asp:TextBox ID="iCheckIn" runat="server" Width="100px" />
                            <rjs:PopCalendar ID="ppcCheckIn" runat="server" Control="iCheckIn" ClientScriptOnDateChanged="dateChange" />
                        </div>
                        <div id="divExpandDate" style="width: 95px; float: left;">
                            <strong>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblExpandDate") %></strong>
                            <br />
                            <select id="fltCheckInDay">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="divNight1" style="width: 122px; float: left;">
                            <strong>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblNight1") %></strong>
                            <br />
                            <select id="fltNight1" onchange="changedNight1();">
                            </select>
                        </div>
                        <div id="divNight2" style="width: 122px; float: left;">
                            <strong>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblNight2") %></strong>
                            <br />
                            <select id="fltNight2" onchange="changedNight2();">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <%= GetGlobalResourceObject("PackageSearchFilter", "lblRoomCount") %></strong>
                        <br />
                        <div id="fltRoomCountDiv" class="ui-helper-hidden">
                            <select id="fltRoomCount" onchange="getRoomInfo(false);">
                            </select>
                        </div>
                        <div id="divRoomInfo" style="width: 100%;">
                            <div id="fltRoomInfo">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="detailSearch" valign="middle">
                        <strong>
                            <%= GetGlobalResourceObject("PackageSearchFilter", "lblDetailedSearch") %></strong>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%; height: 22px; position: relative; vertical-align: bottom;">
                            <span style="position: absolute; bottom: 2px;"><b>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblResort") %></b></span>
                            <img alt="" src="Images/search.png" height="22px" style="position: absolute; right: 2px;"
                                onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleResortList") %>', 'resort');" />
                        </div>
                        <div id="txtResort" class="multiField">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%; height: 22px; position: relative; vertical-align: bottom;">
                            <span style="position: absolute; bottom: 2px;"><b>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblCategory") %></b></span>
                            <img alt="" src="Images/search.png" height="22px" style="position: absolute; right: 2px;"
                                onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleCategoryList") %>', 'category');" />
                        </div>
                        <div id="txtCategory" class="multiField">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%; height: 22px; position: relative; vertical-align: bottom;">
                            <span style="position: absolute; bottom: 2px;"><b>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblHotel") %></b></span>
                            <img alt="" src="Images/search.png" height="22px" style="position: absolute; right: 2px;"
                                onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleHotelList") %>', 'hotel');" />
                        </div>
                        <div id="txtHotel" class="multiField">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%; height: 22px; position: relative; vertical-align: bottom;">
                            <span style="position: absolute; bottom: 2px;"><b>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblRoom") %></b></span>
                            <img alt="" src="Images/search.png" height="22px" style="position: absolute; right: 2px;"
                                onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleRoomList") %>', 'room');" />
                        </div>
                        <div id="txtRoom" class="multiField">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%; height: 22px; position: relative; vertical-align: bottom;">
                            <span style="position: absolute; bottom: 2px;"><b>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblBoard") %></b></span>
                            <img alt="" src="Images/search.png" height="22px" style="position: absolute; right: 2px;"
                                onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleBoardList") %>', 'board');" />
                        </div>
                        <div id="txtBoard" class="multiField">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <div id="divCurrency" style="clear: both; width: 100%;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="divStopSaleHotels" style="clear: both; width: 100%;">
                            <br />
                            <input id="showStopSale" type="checkbox" /><label for="showStopSale"><%= GetGlobalResourceObject("PackageSearchFilter", "lblShowStopSaleHotels") %></label>
                            <br />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="z-index: 1; text-align: center;">
                        <input type="button" name="btnSearch" value='<%= GetGlobalResourceObject("LibraryResource", "btnSearch") %>'
                            style="width: 100px;" onclick="search()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />&nbsp;
                    <input type="button" name="btnClear" value='<%= GetGlobalResourceObject("LibraryResource", "btnClear") %>'
                        style="width: 100px;" onclick="filterclear()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="hiddenControls" style="display: none; visibility: hidden;">
            <input id="hfDate" type="hidden" />
            <input id="hfResort" type="hidden" />
            <input id="hfCategory" type="hidden" />
            <input id="hfHotel" type="hidden" />
            <input id="hfRoomInfo" type="hidden" />
            <input id="hfRoom" type="hidden" />
            <input id="hfBoard" type="hidden" />
            <input id="hfHolPack" type="hidden" />
            <input id="hfDepCityCompulsory" type="hidden" />
        </div>
        <div id="dialog-listPopup" style="display: none;">
            <div id="divListPopup">
            </div>
        </div>
        <div id="dialog-message" title="" style="display: none;">
            <span id="messages">Message</span>
        </div>
    </form>
</body>
</html>
