﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Threading;
using System.Web.Script.Serialization;
using System.Collections;
using TvTools;

public partial class AgencyPaymentMonitorV2Data : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScriptSerializer ser = new JavaScriptSerializer();
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        bool? showAgencyComView = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyComView"));
        bool showAgencyCom = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyCom")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyCom")).Value : true;
        bool showAgencyEB = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyEB")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyEB")).Value : true;
        bool showPassengerEB = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showPassengerEB")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showPassengerEB")).Value : true;

        if (!Users.checkCurrentUserOperator(TvBo.Common.crID_Anex))
        {
            #region for Anex
            if (showAgencyComView.HasValue && (UserData.ShowAllRes || showAgencyComView.Value))
            {
                showAgencyCom = true;
                showAgencyEB = true;
                showPassengerEB = true;
            }
            #endregion
        }
        else
        {
            if (showAgencyComView.HasValue && showAgencyComView.Value)
            {
                showAgencyCom = true;
                showAgencyEB = true;
                showPassengerEB = true;
            }
        }

        int iDisplayLength = Convert.ToInt32(HttpContext.Current.Request["iDisplayLength"]);
        int iDisplayStart = Convert.ToInt32(HttpContext.Current.Request["iDisplayStart"]);
        int iEcho = Convert.ToInt32(HttpContext.Current.Request["sEcho"]);
        IQueryable<AgencyPaymentMonitorFields> allRecords = ReadFile().AsQueryable();

        List<requestFormData> requestForm = new List<requestFormData>();
        for (int i = 0; i < Request.Form.Count; i++) requestForm.Add(new requestFormData { Name = Request.Form.GetKey(i), Value = Request[Request.Form.GetKey(i)] });
        string sortingStr = string.Empty;
        var iSortingColsQ = from q in requestForm
                            where q.Name == "iSortingCols"
                            select q.Value;
        Int16? iSortingCols = Conversion.getInt16OrNull(iSortingColsQ.FirstOrDefault());
        string sort = string.Empty;
        if (iSortingCols.HasValue)
        {
            for (int i = 0; i < iSortingCols.Value; i++)
            {
                var iSortCol = (from q in requestForm
                                where q.Name == "iSortCol_" + i
                                select q.Value).FirstOrDefault();
                string sSortDir = (from q in requestForm
                                   where q.Name == "sSortDir_" + i
                                   select q.Value).FirstOrDefault();
                string bSortable = (from q in requestForm
                                    where q.Name == "bSortable_" + iSortCol
                                    select q.Value).FirstOrDefault();
                if (string.Equals(bSortable, "true"))
                {
                    //if (sortingStr.Length > 0) sortingStr += ", ";
                    switch (iSortCol)
                    {
                        case "0": sortingStr = "ResNo";
                            sort = sSortDir;
                            break;
                        case "1": sortingStr = "DepCityName";
                            sort = sSortDir;
                            break;
                        case "2": sortingStr = "ArrCityName";
                            sort = sSortDir;
                            break;
                        case "3": sortingStr = "BegDate";
                            sort = sSortDir;
                            break;
                        case "4": sortingStr = "EndDate";
                            sort = sSortDir;
                            break;
                        case "5": sortingStr = "Days";
                            sort = sSortDir;
                            break;
                        case "7": sortingStr = "ResDate";
                            sort = sSortDir;
                            break;
                        case "8": sortingStr = "Leader";
                            sort = sSortDir;
                            break;
                    }
                }
            }
        }

        if (sortingStr.Length > 0)
        {
            if (sort.ToLower() == "asc")
                allRecords = allRecords.OrderBy(sortingStr);
            else
                allRecords = allRecords.OrderByDescending(sortingStr);
        }
        IQueryable<AgencyPaymentMonitorFields> filteredRecords = allRecords;
        bool showPayment = (string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt) && UserData.MainAgency) && VersionControl.CheckWebVersion(UserData.WebVersion, 48, VersionControl.Equality.gt);
        filteredRecords = filteredRecords.Skip(iDisplayStart).Take(iDisplayLength);
        var retval = from q in filteredRecords
                     select new {
                         SelectRes = CheckSelectedRes(UserData, q.ResNo, q.Balance, q.SaleCur, q.SelectedRow),
                         ResNo = string.Format("<strong>{0}</strong>", q.ResNo) + "<br />" +
                                 string.Format("<span style=\"text-decoration: underline;cursor: pointer;\" onclick=\"showResView('{0}');\">{1}</span>",
                                                q.ResNo, HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "View")) +
                                 (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ? (string.Format("&nbsp;&nbsp;|&nbsp;&nbsp;<span style=\"text-decoration: underline;cursor: pointer;\" onclick=\"showResInv('{0}');\">{1}</span>", q.ResNo, HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Invoice"))) : ""),
                         DepCityName = q.DepCityName,
                         ArrCityName = q.ArrCityName,
                         BegDate = q.BegDate.HasValue ? q.BegDate.Value.ToShortDateString() : "",
                         EndDate = q.EndDate.HasValue ? q.EndDate.Value.ToShortDateString() : "",
                         Days = q.Days.ToString(),
                         statusStr = string.Format("<img src=\"Images/Status/ResStatus{0}.gif\" width=\"16\" height=\"16\" title=\"{1}\"><img src=\"Images/Status/ConfStatus{2}.gif\" width=\"16\" height=\"16\" title=\"{3}\">",
                                                    q.ResStat,
                                                    HttpContext.GetGlobalResourceObject("ResMonitor", "lblResStatus").ToString() + ", " +
                                                    HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + q.ResStat.ToString()).ToString(),
                                                    q.ConfStat,
                                                    HttpContext.GetGlobalResourceObject("ResMonitor", "lblConfStatus").ToString() + ", " +
                                                    HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + q.ConfStat.ToString()).ToString()),
                         ResDate = q.ResDate.HasValue ? q.ResDate.Value.ToShortDateString() : "",
                         LeaderName = q.Leader,
                         Adult = q.Adult.ToString(),
                         Child = q.Child.ToString(),
                         SaleCur = q.SaleCur,
                         SalePrice = q.SalePrice.ToString(),
                         AgencyCom = ((q.AgencyCom.HasValue ? q.AgencyCom.Value : 0) - (q.DiscountFromCom.HasValue ? q.DiscountFromCom.Value : 0)).ToString(),
                         DiscountFromCom = q.DiscountFromCom.ToString(),
                         Discount = q.Discount.ToString(),
                         BrokerCom = q.BrokerCom.ToString(),
                         AgencySupDis = q.AgencySupDis.ToString(),
                         AgencyComSup = q.AgencyComSup.ToString(),
                         EBAgency = q.EBAgency.ToString(),
                         EBPas = q.EBPas.ToString(),
                         AgencyPayable = q.AgencyPayable.ToString(),
                         AgencyPayable2 = q.AgencyPayable2.ToString(),
                         AgencyPayment = q.AgencyPayment.ToString(),
                         Balance = q.Balance.ToString(),
                         UserBonus = q.UserBonus.ToString(),
                         AgencyBonus = q.AgencyBonus.ToString(),
                         JournalRef = "<span onclick=\"showJournalRef('" + q.ResNo + "');\" style=\"font-style:italic; text-decoration:underline; cursor: pointer;\">Journal</span>",
                         OptionDate=q.OptDate.HasValue?q.OptDate.Value.ToShortDateString()+" "+ q.OptDate.Value.ToShortTimeString():""
                     };
        lightTable returnTable = new lightTable();
        returnTable.sEcho = iEcho;
        returnTable.iTotalRecords = allRecords.Count();
        returnTable.iTotalDisplayRecords = allRecords.Count();
        string selectLinq = string.Format("new({5}ResNo, DepCityName, ArrCityName, BegDate, EndDate, Days, statusStr, ResDate, LeaderName, Adult, Child, SaleCur, SalePrice{3}, Discount{6}, AgencySupDis, AgencyComSup{4}{2}, AgencyPayable, AgencyPayable2, AgencyPayment, Balance{7}{1}{0})",
            ((UserData.Bonus.BonusAgencySeeOwnW && UserData.Bonus.BonusUserSeeAgencyW) || (UserData.Bonus.BonusAgencySeeOwnW && UserData.MasterAgency)) ? ", AgencyBonus" : "",
            UserData.Bonus.BonusUserSeeOwnW ? ", UserBonus" : "",
            showPassengerEB ? ", EBPas" : "",
            showAgencyCom ? ", AgencyCom, DiscountFromCom" : "",
            showAgencyEB ? ", EBAgency" : "",
            showPayment ? "SelectRes, " : string.Empty,
            UserData.MainAgency ? ", BrokerCom" : "",
            string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt) ? ", JournalRef, OptionDate" : "");

        returnTable.aaData = retval.Select(selectLinq);

        Response.Clear();

        Response.ContentType = ("application/json");/*text/html*/
        Response.BufferOutput = true;
        Response.Write(ser.Serialize(returnTable));
        Response.End();
    }

    internal string CheckSelectedRes(User UserData, string ResNo, decimal? Balance, string Currency, bool selected)
    {
        if (Balance.HasValue && Balance.Value > 0)
        {
            return string.Format("<input name=\"selectedReservation\" type=\"checkbox\" value=\"{0}\" onclick=\"selectRow(this)\" balance=\"{1}\" curr=\"{2}\" {3} />",
                        ResNo, (Balance.HasValue ? (Balance.Value * 100).ToString("#.") : ""),
                        Currency,
                        selected ? "checked=\"checked\"" : "");
        }
        else
        {
            return "&nbsp;";
        }
    }

    protected List<AgencyPaymentMonitorFields> ReadFile()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\AgencyPaymentMonitor." + HttpContext.Current.Session.SessionID;
        System.IO.StreamReader reader = new System.IO.StreamReader(path);
        List<AgencyPaymentMonitorFields> list = new List<AgencyPaymentMonitorFields>();
        try
        {
            string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
            list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgencyPaymentMonitorFields>>(uncompressed);
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            reader.Close();
        }
        return list;
    }
}

