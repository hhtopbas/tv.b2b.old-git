﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Services;
using System.Threading;
using System.Globalization;
using TvBo;
using TvTools;
using System.Web.Script.Services;
using System.Configuration;
using System.Text.RegularExpressions;


public partial class PackageSearchResultV2 : BasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getGridHeaders()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<dataTableColumnsDef> aoColumns = new List<dataTableColumnsDef>();
        //96,18,30,200,30,100,80,50,50,130,100,30,28,28
        aoColumns.Add(new dataTableColumnsDef { IDNo = 1, ColName = "Book", sTitle = "&nbsp;", sClass = "center", sWidth = "10%", bSortable = false, bSearchable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 2, ColName = "AddOffer", sTitle = "&nbsp;", sClass = "center", sWidth = "2%", bSortable = false, bSearchable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 3, ColName = "OprText", sTitle = "&nbsp;", sClass = "center", sWidth = "3%", bSortable = false, bSearchable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 4, ColName = "Hotel", sTitle = HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "Hotel").ToString(), sWidth = "21%", bSortable = true, bSearchable = true });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 5, ColName = "FreeRoom", sTitle = "<img alt=\"\" src=\"Images/hotel.png\" style=\"width:22px\" />", sClass = "center", sWidth = "3%", bSortable = false, bSearchable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 6, ColName = "Location", sTitle = HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "Location").ToString(), sWidth = "10%", bSortable = true, bSearchable = true });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 7, ColName = "CheckIn", sTitle = HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "CheckIn").ToString(), sWidth = "8%", bSortable = true, sType = "date", bSearchable = true });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 8, ColName = "Nights", sTitle = HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "Nights").ToString(), sClass = "center", sWidth = "5%", bSortable = false, bSearchable = true });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 9, ColName = "Board", sTitle = HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "Board").ToString(), sClass = "center", sWidth = "5%", bSortable = true, bSearchable = true });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 10, ColName = "Accom", sTitle = HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "Accom").ToString(), sWidth = "14%", bSortable = false, bSearchable = true });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 11, ColName = "Price", sTitle = HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "Price").ToString(), sWidth = "10%", bSortable = true, bSearchable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 12, ColName = "Discount", sTitle = "&nbsp;", sClass = "center", sWidth = "3%", bSortable = false, bSearchable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 13, ColName = "Departure", sTitle = "<img alt=\"\" src=\"Images/DFlight.gif\" />", sWidth = "3%", sClass = "center", bSortable = false, bSearchable = true });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 14, ColName = "Return", sTitle = "<img alt=\"\" src=\"Images/RFlight.gif\" />", sWidth = "3%", sClass = "center", bSortable = false, bSearchable = true });
        var retVal = from q in aoColumns.AsEnumerable()
                     select new { sTitle = q.sTitle, sWidth = q.sWidth, bSortable = q.bSortable, sType = q.sType, sClass = q.sClass };
        getResult();

        #region Search Note
        string errorMsg = string.Empty;
        
        //string searchTextRTF = new Search().getSearchText(UserData, ref errorMsg);
        string searchTextHtml =CacheObjects.getMarketPriceListFixNote(UserData.Market);
        StringBuilder sb = new StringBuilder();
        if (!string.IsNullOrEmpty(searchTextHtml))
        {
            //TvTools.RTFtoHTML rtf = new RTFtoHTML();
            //searchTextRTF = Regex.Replace(searchTextRTF, @"\{\*?\\[^{}]+}|[{}]|\\\n?[A-Za-z]+\n?(?:-?\d+)?[ ]?", "");
            //rtf.rtf = searchTextRTF;
            //string searchTextHtml = rtf.html();
            sb.Append("<div class=\"divResultNote\">");
            sb.Append(searchTextHtml);
            sb.Append("</div>");
        }
        #endregion

        #region Client Offer

        bool newOffer = string.Equals(Conversion.getStrOrNull(ConfigurationManager.AppSettings["ClientOfferVersion"]), "V2");
        StringBuilder cO = new StringBuilder();
        if (newOffer)
        {
            cO.AppendLine("&nbsp;&nbsp");
            cO.AppendFormat("<input id=\"btnCreateOffer\" type=\"button\" value=\"{0}\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" onclick=\"createOfferV2()\" />",
                    HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "btnCreateOffer"));
            cO.AppendLine("&nbsp;&nbsp");
            //cO.AppendFormat("<input id=\"btnClearOffer\" type=\"button\" value=\"{0}\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" onclick=\"ClearOffer()\" />",
            //        HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "btnClearOffer"));
        }


        #endregion

        return new
        {
            data = retVal,
            searchNote = sb.ToString(),
            newOffer = newOffer,
            newOfferHtml = cO.ToString()
        };
    }

    public static string getResult()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["Criteria"] == null) return null;
        SearchCriteria criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];

        string errorMsg = string.Empty;
        
        bool ExtAllotCont = false;
        object extAllotControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ExtAllotControl");
        if (extAllotControl != null) ExtAllotCont = (bool)extAllotControl;
        Guid? recID = null;
        String useLog = System.Configuration.ConfigurationManager.AppSettings["useLog"];
        if (!string.IsNullOrEmpty(useLog) && string.Equals(useLog, "1"))
        {
            string packType = string.Empty;
            switch (criteria.SType)
            {
                case SearchType.TourPackageSearch: packType = "T"; break;
                case SearchType.PackageSearch: packType = "H"; break;
                case SearchType.OnlyHotelSearch: packType = "O"; break;
            }
            recID = new WEBLog().saveWEBSearchLog(UserData.SID, UserData.UserID, UserData.AgencyID, SaleResource.B2B,
                            criteria.CheckIn, criteria.ExpandDate, criteria.NightFrom, criteria.NightTo, criteria.DepCity, criteria.ArrCity,
                            criteria.PriceFrom, criteria.PriceTo, criteria.Package, packType, null, ref errorMsg);
            if (recID.HasValue)
            {
                foreach (var row in criteria.RoomsInfo)
                    new WEBLog().saveWEBRoomInfoLog(recID.Value, row.Adult, row.Child, ref errorMsg);

                foreach (var row in criteria.Resort.Split('|'))
                    new WEBLog().saveWEBLocationLog(recID.Value, Conversion.getInt32OrNull(row.ToString()), ref errorMsg);

                foreach (var row in criteria.Hotel.Split('|'))
                    if (!string.IsNullOrEmpty(row))
                        new WEBLog().saveWEBHotelLog(recID.Value, row.ToString(), ref errorMsg);

                foreach (var row in criteria.Room.Split('|'))
                    if (!string.IsNullOrEmpty(row))
                        new WEBLog().saveWEBRoomLog(recID.Value, row.ToString(), ref errorMsg);

                foreach (var row in criteria.Board.Split('|'))
                    if (!string.IsNullOrEmpty(row))
                        new WEBLog().saveWEBBoardLog(recID.Value, row.ToString(), ref errorMsg);

                foreach (var row in criteria.Category.Split('|'))
                    if (!string.IsNullOrEmpty(row))
                        new WEBLog().saveWEBCategoryLog(recID.Value, row.ToString(), ref errorMsg);
            }
        }

        List<SearchResult> result = null;
        if (criteria.ArrCity.HasValue)
            result = new Search().getPackPriceSearchV2(UserData, criteria, false, criteria.SType, false, false, recID, ref errorMsg);
        else
        {

            #region Herhangi bir arrival belirtilmezse
            SearchPLData data = (SearchPLData)HttpContext.Current.Session["SearchPLData"];
            string sessionId=HttpContext.Current.Session.SessionID;
            //Thread threadSearch = new Thread(asyncSearch => 
            GetResultWithOutArrival(UserData, criteria,data, recID,sessionId);
            //threadSearch.Start();
            #endregion
        }

        if (result != null && result.Count > 0)
        {
            CreateAndWriteFile(result);
            return string.Empty;
        }
        else
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\PriceSearchV2." + HttpContext.Current.Session.SessionID;
            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);
            return errorMsg;
        }
    }

    private static void GetResultWithOutArrival(TvBo.User UserData, SearchCriteria criteria, SearchPLData data, Guid? recID,string sessionId)
    {
        #region Process Kontrol İçin Doysa Oluşturma
        //Thread.CurrentThread.CurrentCulture = UserData.Ci;
        //Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = "";// AppDomain.CurrentDomain.BaseDirectory + "Images\\Process." + sessionId;
        //if (System.IO.File.Exists(path))
        //    System.IO.File.Delete(path);
        //System.IO.FileStream f = new System.IO.FileStream(path, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
        #endregion
        string errorMsg = "";
        
        List<plLocationData> arrCityData = new Search().getSearchArrCitys(false, data, criteria.DepCity, ref errorMsg);
        List<SearchResult> result = new List<SearchResult>();
        List<SearchResult> tempResult = null;
        foreach (plLocationData arr in arrCityData)
        {
            criteria.ArrCity = arr.ArrCity;
            tempResult = new Search().getPackPriceSearchV2(UserData, criteria, false, criteria.SType, false, false, recID, ref errorMsg);
            if (result.Any())
            {
                int maxid = result.Max(m => m.RoomNr);
                tempResult.ForEach(ff => { ff.RefNo += maxid; ff.RoomNr += maxid; });
            }
            result.AddRange(tempResult);
        }
        if (result != null && result.Count > 0)
        {
            CreateAndWriteFile(result);
            //return string.Empty;
        }
        else
        {
            path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\PriceSearchV2." +sessionId;
            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);
            //return errorMsg;
        }
        #region Process Kontrol İçin Dosya Kaldırma
        //path = AppDomain.CurrentDomain.BaseDirectory + "Images\\Process." + sessionId;
        //if (System.IO.File.Exists(path))
        //    System.IO.File.Delete(path);
        #endregion
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object checkAsyncProcess()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        bool process = true;
        string path = AppDomain.CurrentDomain.BaseDirectory + "Images\\Process." + HttpContext.Current.Session.SessionID;
        if (System.IO.File.Exists(path))
            process = true;
        else
            process = false;
        return new { Process = process };
    }

    private static void CreateAndWriteFile(List<SearchResult> result)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\PriceSearchV2." + HttpContext.Current.Session.SessionID;
        if (System.IO.File.Exists(path))
            System.IO.File.Delete(path);
        System.IO.FileStream f = new System.IO.FileStream(path, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
        try
        {
            System.IO.StreamWriter writer = new System.IO.StreamWriter(f);
            string compress = TvTools.GZipCompres.Compress(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            writer.Write(compress);
            writer.Close();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            f.Close();
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static basketTableRecord getBasket()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        basketTableRecord retVal = new basketTableRecord
        {
            basketListOK = false,
            message = string.Empty,
            basketDiv = string.Empty
        };

        StringBuilder sb = new StringBuilder();
        List<SearchResult> searchData = readSearchData();
        List<SearchResult> basketData = readBasketData();
        if (basketData.Count > 0)
        {
            sb.AppendFormat("<div style=\"text-align:center; font-size: 11pt;\"><strong>{0}</strong></div>", "Basket");
            sb.Append("<table id=\"basketTable\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">");
            sb.Append("<tr class=\"ui-widget-header\" style=\"height: 30px;\">");
            sb.AppendFormat("<th class=\"hotel\"><strong>{0}</strong></th>", "Hotel");
            sb.AppendFormat("<th class=\"room\"><strong>{0}</strong></th>", "Room");
            sb.AppendFormat("<th class=\"accom\"><strong>{0}</strong></th>", "Accommodation");
            sb.AppendFormat("<th class=\"board\"><strong>{0}</strong></th>", "Board");
            sb.AppendFormat("<th class=\"checkin\"><strong>{0}</strong></th>", "Check In");
            sb.AppendFormat("<th class=\"night\"><strong>{0}</strong></th>", "Night");
            sb.AppendFormat("<th class=\"price\"><strong>{0}</strong></th>", "Price");
            sb.AppendFormat("<th class=\"cur\"><strong>{0}</strong></th>", "Cur");
            sb.Append("<th class=\"info\">&nbsp;</th></tr>");
            int i = 0;
            foreach (SearchResult row in basketData)
            {
                i++;
                row.RefNo = i;
                sb.AppendFormat("<tr {0} style=\"height: 25px;\">", Convert.ToInt32(i / 2.0) == (i / 2.0) ? "class=\"gradeA\"" : "");
                sb.AppendFormat("<td>{0}</td>", row.HotelName);
                sb.AppendFormat("<td>{0}</td>", row.RoomName);
                sb.AppendFormat("<td>{0}</td>", row.AccomName);
                sb.AppendFormat("<td>{0}</td>", row.Board);
                sb.AppendFormat("<td>{0}</td>", row.CheckIn.HasValue ? row.CheckIn.Value.ToShortDateString() : "");
                sb.AppendFormat("<td style=\"text-align: center;\">{0}</td>", row.Night.ToString());
                sb.AppendFormat("<td style=\"text-align: center;\">{0}</td>", row.LastPrice.HasValue ? row.LastPrice.Value.ToString("#,###.00") : "");
                sb.AppendFormat("<td style=\"text-align: center;\">{0}</td>", row.SaleCur.ToString());
                sb.AppendFormat("<td style=\"text-align: center;\"><img alt=\"\" src=\"Images/cancel.png\" onclick=\"removeBasket({0});\" /></td>", row.RefNo);
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            sb.Append("<div style=\"text-align:center;\">");
            sb.AppendFormat("<input id=\"makeResBtn\" type=\"button\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" value=\"{1}\" onclick=\"clearBasketList()\" />&nbsp;|&nbsp;<input id=\"makeResBtn\" type=\"button\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" value=\"{0}\" onclick=\"basketMakeRes()\" /></div><br />", "Make Reservation", "Clear Basket List");
            writeBasketData(basketData);
            retVal.basketDiv = sb.ToString();
            retVal.basketListOK = true;
        }
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static basketTableRecord setBasket(int? CatPackId, int? ARecNo, int? PRecNo, int? HapRecId)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        basketTableRecord retVal = new basketTableRecord
        {
            basketListOK = false,
            message = string.Empty,
            basketDiv = string.Empty
        };

        StringBuilder sb = new StringBuilder();
        List<SearchResult> searchData = readSearchData();
        List<SearchResult> basketData = readBasketData();
        List<SearchResult> _basketData = (List<SearchResult>)TvBo.Common.DeepClone(basketData);
        SearchResult selectedData = searchData.Find(f => f.CatPackID == CatPackId && f.ARecNo == ARecNo && f.PRecNo == PRecNo && f.HAPRecId == HapRecId);
        if (selectedData != null)
        {

            if (selectedData.StopSaleStd == 2 && selectedData.StopSaleGuar == 2)
            {
                retVal.basketListOK = true;
                retVal.message = HttpContext.GetGlobalResourceObject("PackageSearchResult", "stopSale").ToString();
            }

            if (basketData.Count < UserData.MaxRoomCount)
            {
                basketData.Add(selectedData);
                retVal.basketListOK = true;
                retVal.message = string.Empty;
            }
            else
            {
                retVal.basketListOK = true;
                retVal.message = string.Format(HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "MaxValueOfRoom").ToString(), UserData.MaxRoomCount);
            }

            // Check Allotment & Add Basket
            string errorMsg = string.Empty;
            if (!(new Hotels().AllotmentControlForMultipleRoom(UserData, basketData, "H", ref errorMsg)))
            {
                retVal.basketListOK = true;
                retVal.message = errorMsg;
                basketData.Remove(basketData.LastOrDefault());
            } // Check Allotment & Add Basket
            else
                if (basketData.FirstOrDefault().CatPackID != selectedData.CatPackID ||
                    basketData.FirstOrDefault().Hotel != selectedData.Hotel ||
                    basketData.FirstOrDefault().CheckIn != selectedData.CheckIn ||
                    basketData.FirstOrDefault().Night != selectedData.Night ||
                    basketData.FirstOrDefault().HolPack != selectedData.HolPack ||
                    basketData.FirstOrDefault().DepCity != selectedData.DepCity ||
                    basketData.FirstOrDefault().ArrCity != selectedData.ArrCity ||
                    basketData.FirstOrDefault().DepFlight != selectedData.DepFlight)
                {
                    retVal.basketListOK = true;
                    retVal.message = HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "AddBasketCheckValue").ToString();
                    basketData.Remove(basketData.LastOrDefault());
                }

            if (basketData.Count > 0)
            {
                sb.AppendFormat("<div style=\"text-align:center; font-size: 11pt;\"><strong>{0}</strong></div>", "Basket");
                sb.Append("<table id=\"basketTable\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">");
                sb.Append("<tr class=\"ui-widget-header\" style=\"height: 30px;\">");
                sb.AppendFormat("<th class=\"hotel\"><strong>{0}</strong></th>", HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "Hotel"));
                sb.AppendFormat("<th class=\"room\"><strong>{0}</strong></th>", HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblRoom"));
                sb.AppendFormat("<th class=\"accom\"><strong>{0}</strong></th>", HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblAccom"));
                sb.AppendFormat("<th class=\"board\"><strong>{0}</strong></th>", HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "Board"));
                sb.AppendFormat("<th class=\"checkin\"><strong>{0}</strong></th>", HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "CheckIn"));
                sb.AppendFormat("<th class=\"night\"><strong>{0}</strong></th>", HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "Nights"));
                sb.AppendFormat("<th class=\"price\"><strong>{0}</strong></th>", HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "Price"));
                sb.AppendFormat("<th class=\"cur\"><strong>{0}</strong></th>", HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentCurrency"));
                sb.Append("<th class=\"info\">&nbsp;</th></tr>");
                int i = 0;
                foreach (SearchResult row in basketData)
                {
                    i++;
                    row.RefNo = i;
                    sb.AppendFormat("<tr {0} style=\"height: 25px;\">", Convert.ToInt32(i / 2.0) == (i / 2.0) ? "class=\"gradeA\"" : "");
                    sb.AppendFormat("<td>{0}</td>", row.HotelName);
                    sb.AppendFormat("<td>{0}</td>", row.RoomName);
                    sb.AppendFormat("<td>{0}</td>", row.AccomName);
                    sb.AppendFormat("<td>{0}</td>", row.Board);
                    sb.AppendFormat("<td>{0}</td>", row.CheckIn.HasValue ? row.CheckIn.Value.ToShortDateString() : "");
                    sb.AppendFormat("<td style=\"text-align: center;\">{0}</td>", row.Night.ToString());
                    sb.AppendFormat("<td style=\"text-align: center;\">{0}</td>", row.LastPrice.HasValue ? row.LastPrice.Value.ToString("#,###.00") : "");
                    sb.AppendFormat("<td style=\"text-align: center;\">{0}</td>", row.SaleCur.ToString());
                    sb.AppendFormat("<td style=\"text-align: center;\"><img alt=\"\" src=\"Images/cancel.png\" onclick=\"removeBasket({0});\" /></td>", row.RefNo);
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
                sb.Append("<div style=\"text-align:center;\">");
                sb.AppendFormat("<input id=\"makeResBtn\" type=\"button\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" value=\"{1}\" onclick=\"clearBasketList()\" />&nbsp;|&nbsp;<input id=\"makeResBtn\" type=\"button\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" value=\"{0}\" onclick=\"basketMakeRes()\" /></div><br />",
                    HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "MakeReservation"),
                    HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "ClearBasketList"));
                writeBasketData(basketData);
                retVal.basketDiv = sb.ToString();
            }
            else
                retVal.basketListOK = false;
        }
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static basketTableRecord removeBasket(int? rowID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        StringBuilder sb = new StringBuilder();
        List<SearchResult> basketData = readBasketData();
        List<SearchResult> selectedData = basketData.Where(w => w.RefNo != rowID).ToList<SearchResult>();
        basketTableRecord retVal = new basketTableRecord();
        if (selectedData.Count > 0)
        {
            sb.AppendFormat("<div style=\"text-align:center; font-size: 11pt;\"><strong>{0}</strong></div>", "Basket");
            sb.Append("<table id=\"basketTable\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">");
            sb.Append("<tr class=\"ui-widget-header\" style=\"height: 30px;\">");
            sb.AppendFormat("<th class=\"hotel\"><strong>{0}</strong></th>", "Hotel");
            sb.AppendFormat("<th class=\"room\"><strong>{0}</strong></th>", "Room");
            sb.AppendFormat("<th class=\"accom\"><strong>{0}</strong></th>", "Accommodation");
            sb.AppendFormat("<th class=\"board\"><strong>{0}</strong></th>", "Board");
            sb.AppendFormat("<th class=\"checkin\"><strong>{0}</strong></th>", "Check In");
            sb.AppendFormat("<th class=\"night\"><strong>{0}</strong></th>", "Night");
            sb.AppendFormat("<th class=\"price\"><strong>{0}</strong></th>", "Price");
            sb.AppendFormat("<th class=\"cur\"><strong>{0}</strong></th>", "Cur");
            sb.Append("<th class=\"info\">&nbsp;</th></tr>");
            int i = 0;
            foreach (SearchResult row in selectedData)
            {
                i++;
                row.RefNo = i;
                sb.AppendFormat("<tr {0} style=\"height: 25px;\">", Convert.ToInt32(i / 2.0) == (i / 2.0) ? "class=\"gradeA\"" : "");
                sb.AppendFormat("<td>{0}</td>", row.HotelName);
                sb.AppendFormat("<td>{0}</td>", row.RoomName);
                sb.AppendFormat("<td>{0}</td>", row.AccomName);
                sb.AppendFormat("<td>{0}</td>", row.Board);
                sb.AppendFormat("<td>{0}</td>", row.CheckIn.HasValue ? row.CheckIn.Value.ToShortDateString() : "");
                sb.AppendFormat("<td style=\"text-align: center;\">{0}</td>", row.Night.ToString());
                sb.AppendFormat("<td style=\"text-align: center;\">{0}</td>", row.LastPrice.HasValue ? row.LastPrice.Value.ToString("#,###.00") : "");
                sb.AppendFormat("<td style=\"text-align: center;\">{0}</td>", row.SaleCur.ToString());
                sb.AppendFormat("<td style=\"text-align: center;\"><img alt=\"\" src=\"Images/cancel.png\" onclick=\"removeBasket({0});\" /></td>", row.RefNo);
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            sb.Append("<div style=\"text-align:center;\">");
            sb.AppendFormat("<input id=\"makeResBtn\" type=\"button\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" value=\"{1}\" onclick=\"clearBasketList()\" />&nbsp;|&nbsp;<input id=\"makeResBtn\" type=\"button\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" value=\"{0}\" onclick=\"basketMakeRes()\" /></div><br />", "Make Reservation", "Clear Basket List");
            writeBasketData(selectedData);
            retVal = new basketTableRecord
            {
                basketListOK = true,
                message = string.Empty,
                basketDiv = sb.ToString()
            };
        }
        else
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\PriceSearchBasketV2." + HttpContext.Current.Session.SessionID;
            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);
            retVal = new basketTableRecord
            {
                basketListOK = false,
                message = string.Empty,
                basketDiv = string.Empty
            };
        }
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static basketTableRecord removeBasketList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\PriceSearchBasketV2." + HttpContext.Current.Session.SessionID;
        if (System.IO.File.Exists(path))
            System.IO.File.Delete(path);

        return new basketTableRecord
        {
            basketListOK = false,
            message = string.Empty,
            basketDiv = string.Empty
        };
    }

    [WebMethod(EnableSession = true)]
    public static string addOffer(string BookList)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        List<SearchResult> searchResult = readSearchData();
        List<SearchResult> _bookList = new List<SearchResult>();
        for (int i = 0; i < BookList.Split(',').Length; i++)
        {
            int refNo = Convert.ToInt32(BookList.Split(',')[i]);
            SearchResult bookRow = searchResult.Find(f => f.RefNo == refNo);
            _bookList.Add(bookRow);
        }
        List<SearchResult> offerList = new List<SearchResult>();
        if (string.Equals(Conversion.getStrOrNull(System.Configuration.ConfigurationManager.AppSettings["ClientOfferVersion"]), "V2"))
        {
            offerList = new ClientOffer().readClientOfferList(UserData);
        }
        else
        {
            if (HttpContext.Current.Session["OfferList"] != null)
                offerList = (List<SearchResult>)HttpContext.Current.Session["OfferList"];
        }
        foreach (SearchResult row in _bookList)
            offerList.Add(row);
        if (string.Equals(Conversion.getStrOrNull(System.Configuration.ConfigurationManager.AppSettings["ClientOfferVersion"]), "V2"))
        {
            new ClientOffer().saveClientOfferList(UserData, offerList);
        }
        else
        {
            HttpContext.Current.Session["OfferList"] = offerList;
        }
        return "";
    }

    [WebMethod(EnableSession = true)]
    public static string clearOffer()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (string.Equals(Conversion.getStrOrNull(System.Configuration.ConfigurationManager.AppSettings["ClientOfferVersion"]), "V2"))
        {
            new ClientOffer().removeClientOfferList(UserData);
        }
        else
        {
            HttpContext.Current.Session["OfferList"] = null;
        }
        return "";
    }

    internal static void writeBasketData(List<SearchResult> result)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\PriceSearchBasketV2." + HttpContext.Current.Session.SessionID;
        if (System.IO.File.Exists(path))
            System.IO.File.Delete(path);
        System.IO.FileStream f = new System.IO.FileStream(path, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
        try
        {
            System.IO.StreamWriter writer = new System.IO.StreamWriter(f);
            string compress = TvTools.GZipCompres.Compress(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            writer.Write(compress);
            writer.Close();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            f.Close();
        }
    }

    internal static List<SearchResult> readBasketData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\PriceSearchBasketV2." + HttpContext.Current.Session.SessionID;
        List<SearchResult> list = new List<SearchResult>();
        if (System.IO.File.Exists(path))
        {
            System.IO.StreamReader reader = new System.IO.StreamReader(path);

            try
            {
                string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
                list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SearchResult>>(uncompressed);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                reader.Close();
            }
        }
        return list;
    }

    internal static List<SearchResult> readSearchData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\PriceSearchV2." + HttpContext.Current.Session.SessionID;
        List<SearchResult> list = new List<SearchResult>();
        if (System.IO.File.Exists(path))
        {
            System.IO.StreamReader reader = new System.IO.StreamReader(path);

            try
            {
                string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
                list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SearchResult>>(uncompressed);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                reader.Close();
            }
        }
        return list;
    }
}

public class basketTableRecord
{
    public bool basketListOK { get; set; }
    public string message { get; set; }
    public string basketDiv { get; set; }
}