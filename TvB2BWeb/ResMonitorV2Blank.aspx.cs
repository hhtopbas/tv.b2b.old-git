﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;

public partial class ResMonitorV2Blank : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url);
        Session["ResMonFilterDef"] = null;
        Response.Redirect("~/ResMonitorV2.aspx" + Request.Url.Query);
        
    }
}
