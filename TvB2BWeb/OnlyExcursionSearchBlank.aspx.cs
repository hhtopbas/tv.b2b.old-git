﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Threading;

public partial class OnlyExcursionSearchBlank : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (!IsPostBack)
        {
            Session["SearchExcData"] = null;
            Session["ExcuresionCriteria"] = null;            
            Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url);
            Response.Redirect("~/OnlyExcursionSearch.aspx" + Request.Url.Query);
        }
    }
}
