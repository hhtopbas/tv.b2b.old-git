﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MakeOnlyTransfer.aspx.cs" Inherits="MakeOnlyTransfer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    <%= GetGlobalResourceObject("PageTitle", "MakeReservation") %></title>

  <script src="Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery.json.js" type="text/javascript"></script>
  <script src="Scripts/json2.js" type="text/javascript"></script>
  <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
  <script src="Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
  <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>
  <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
  <script src="Scripts/checkDate.js" type="text/javascript"></script>
  <script src="Scripts/linq.js" type="text/javascript"></script>
  <script src="Scripts/jquery.linq.js" type="text/javascript"></script>
  <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>

  <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="CSS/main.css" rel="stylesheet" type="text/css" />
  <link href="CSS/MakeOnlyTransfer.css" rel="stylesheet" />

  <script type="text/javascript">


    var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
    var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
    var btnPaymentPage = '<%= GetGlobalResourceObject("LibraryResource", "btnPaymentPage") %>';
    var InvalidBirthDate = '<%= GetGlobalResourceObject("LibraryResource","InvalidBirthDate") %>';
    var lblNoResult = '<%= GetGlobalResourceObject("BookTicket", "lblNoResult") %>';
    var WhereInvoiceTo = '<%= GetGlobalResourceObject("LibraryResource", "WhereInvoiceTo") %>';
    var lblAlreadyRes = '<%= GetGlobalResourceObject("BookTicket", "lblAlreadyRes") %>';
    var mkPleaseCompleteAllTheInformation = '<%= GetGlobalResourceObject("OnlyTransfer", "mkPleaseCompleteAllTheInformation") %>';


    var reservationSaved = false;
    if (typeof window.event != 'undefined') {
      document.onkeydown = function () {
        if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
          return (event.keyCode != 8);
      }
    }
    else {
      document.onkeypress = function (e) {
        if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
          return (e.keyCode != 8);
      }
    }

    function logout() {
      self.parent.logout();
    }

    $(document).ajaxStart(function () {
      $.blockUI({
        message: '<h1>' + lblPleaseWait + '</h1>'
      });
    }).ajaxStop(function () {
      $.unblockUI();
    });

    function showMessage(msg) {
      $("#messages").html(msg);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        modal: true,
        autoResize: true,
        resizable: true,
        resizeStop: function (event, ui) {
          $(this).dialog({ position: 'center' });
        },
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog('close');
          }
        }]
      });
    }

    function showDialogEndGotoPaymentPage(msg, ResNo) {
      $(function () {
        $('<div>' + msg + '</div>').dialog({
          buttons: [{
            text: btnOK,
            click: function () {
              $(this).dialog('close');
              window.close;
              self.parent.gotoResViewPage(ResNo);
            }
          }, {
            text: btnPaymentPage,
            click: function () {
              $(this).dialog('close');
              window.close;
              self.parent.paymentPage(ResNo);
            }
          }]
        });
      });
    }

    function showDialogEndExit(msg, ResNo) {
      $(function () {
        $("#messages").html('');
        $("#messages").html(msg);
        $("#dialog").dialog("destroy");
        $("#dialog-message").dialog({
          modal: true,
          resizable: true,
          autoResize: true,
          buttons: [{
            text: btnOK,
            click: function () {
              $(this).dialog('close');
              window.close;
              if (ResNo != '')
                self.parent.gotoResViewPage(ResNo);
              else self.parent.cancelMakeRes();
            }
          }]
        });
      });
    }

    var _dateMask = '';
    var _dateFormat = '';

    function SetAge(Id, cinDate, _formatDate) {
      try {
        var birthDate = dateValue(_formatDate, $("#iBirthDay" + Id).val());
        if (birthDate == null) {
          $("#iBirthDay" + Id).val('');
          $("#iAge" + Id).val('');
          return;
        }
        var checkIN = dateValue(_formatDate, cinDate);
        if (checkIN == null) return;
        var _minBirthDate = new Date(1, 1, 1910);
        var _birthDate = birthDate;
        var _cinDate = checkIN;
        if (_birthDate < _minBirthDate) {
          $("#iAge" + Id).val('');
          birthDate = '';
          showMsg(InvalidBirthDate);
          return;
        }
        var age = Age(_birthDate, _cinDate);
        $("#iAge" + Id).val(age);
      }
      catch (err) {
        $("#iAge" + Id).val('');
      }
    }

    function homeCountryChanged(idNo) {
      var country = $("#AddrHomeCountry" + idNo).val();
      var countryList = $("#countryList").val();

      var queryResult = $.Enumerable.From(JSON.parse(countryList))
                         .Where(function (x) { return x.CountryName == country })
                         .Select("$.CountryPhoneCode")
                         .First();
      if (queryResult != '') {
        $("#MobTelCountryCode" + idNo).text('+' + queryResult);
        $("#AddrHomeTelCountryCode" + idNo).text('+' + queryResult);
        $("#AddrHomeFaxCountryCode" + idNo).text('+' + queryResult);
      }
    }

    function leaderChange(idNo) {
      var leader = $("#iLeader" + idNo);
      var hfCustCount = parseInt($("#hfCustCount").val());
      var custCount = parseInt(hfCustCount);
      var custList = [];
      for (var i = 1; i <= custCount; i++) {
        var addressInfo = $("#resCustInfo" + i);
        if (i == idNo)
          addressInfo.show();
        else addressInfo.hide();
      }
    }

    function onSurnameExit(elmID) {
      var surnameList = $(".nameSurnameCss");
      var elmValue = $("#" + elmID).val();
      var custNo = elmID.replace('iSurname', '');
      surnameList.each(function (index) {
        $("#iTitle" + custNo + " option:selected").text();
        var custNoIdx = $(this).attr('custNo');
        if (custNoIdx == custNo) {
          var newValue = $("#iTitle" + $(this).attr('custNo') + " option:selected").text() + ". ";
          newValue += $("#" + elmID).val() + " ";
          newValue += $("#iName" + custNo).val();
          $(this).html(newValue);
        }
      });
    }

    function onNameExit(elmID) {
      var surnameList = $(".nameSurnameCss");
      var elmValue = $(elmID).val();
      var custNo = elmID.replace('iName', '');
      surnameList.each(function (index) {
        $("#iTitle" + custNo + " option:selected").text();
        var custNoIdx = $(this).attr('custNo');
        if (custNoIdx == custNo) {
          var newValue = $("#iTitle" + $(this).attr('custNo') + " option:selected").text() + ". ";
          newValue += $("#iSurname" + custNo).val() + " ";
          newValue += $("#" + elmID).val();
          $(this).html(newValue);
        }
      });
    }

    function changeDepYear(value) {
      var v = value.toString().split('_');
      $("#iDepDay").html('');
      var lastDay = $("#iDepMonthYear option[value='" + value + "']").attr("lastDay");
      $("#iDepDay").append('<option value=""></option>');
      for (var i = 1; i < parseInt(lastDay) + 1; i++) {
        var val = (i < 10 ? '0' : '') + i.toString();
        $("#iDepDay").append('<option value="' + val + '">' + val + '</option>');
      }
    }

    function changeRetYear(value) {
      var v = value.toString().split('_');
      $("#iRetDay").html('');
      var lastDay = $("#iRetMonthYear option[value='" + value + "']").attr("lastDay");
      $("#iRetDay").append('<option value=""></option>');
      for (var i = 1; i < parseInt(lastDay) + 1; i++) {
        var val = (i < 10 ? '0' : '') + i.toString();
        $("#iRetDay").append('<option value="' + val + '">' + val + '</option>');
      }
    }

    function changePickupType() {
      if ($("#pickupType").val() == '0') {
        $("#retDepartureDiv").hide();
      }
      else {
        $("#retDepartureDiv").show();
      }
    }

    function changeDepFlightInputType() {
      if ($("#depFlightInputType").attr("checked") == "checked") {
        $("#iDepFlightNo").show();
        $("#sDepFlightNo").hide();
      }
      else {
        $("#iDepFlightNo").hide();
        $("#sDepFlightNo").show();
      }
    }

    function changeRetFlightInputType() {
      if ($("#retFlightInputType").attr("checked") == "checked") {
        $("#iRetFlightNo").show();
        $("#sRetFlightNo").hide();
      }
      else {
        $("#iRetFlightNo").hide();
        $("#sRetFlightNo").show();
      }
    }

    function changeRegionFrom() {
      var data = $("#divResService").data('ServiceData');
      $("#iDepArrival").html('');
      $("#iDepArrival").append('<option value=""></option>');
      if ($("#regionDep").val() == '1') {
        $.each(data.departureDestinationList, function (i) {
          $("#iDepArrival").append('<option value="' + this.Code + '">' + this.Name + '</option>');
        });
      } else {
        $("#iDepArrival").append('<option value="' + data.departureDestinationLoc.Code + '">' + data.departureDestinationLoc.Name + '</option>');
      }
    }

    function changeRegionReturn() {
      var data = $("#divResService").data('ServiceData');
      $("#iRetDeparture").html('');
      $("#iRetDeparture").append('<option value=""></option>');
      if ($("#regionRet").val() == '1') {
        $.each(data.arrivalDestinationList, function (i) {
          $("#iRetDeparture").append('<option value="' + this.Code + '">' + this.Name + '</option>');
        });
      }
      else {
        $("#iRetDeparture").append('<option value="' + data.arrivalDestinationLoc.Code + '">' + data.arrivalDestinationLoc.Name + '</option>');
      }
    }

    function setDay(day) {
      if (day == undefined) return;
      if (day.toString().length == 1) { day = '0' + day; }
      $("#iDepDay").val(day);
    }

    function setDayReturn(day) {
      if (day == undefined) return;
      if (day.toString().length == 1) { day = '0' + day; }
      $("#iRetDay").val(day);
    }

    function getResServiceDiv() {
      $.ajax({
        async: false,
        type: "POST",
        url: "MakeOnlyTransfer.aspx/getResServiceDiv",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            $("#divResService").data('ServiceData', msg.d);
            $("#isRT").val(msg.d.isRT == true ? '1' : '0');
            $("#departureDescrition").html(msg.d.departureDescrition);
            $("#departureDestination").html(msg.d.departureDestination);
            $("#departurePrice").html(msg.d.departurePrice);
            $("#iDepMonthYear").html(''); $("#iDepMonthYear").append('<option value=""></option>');
            $("#iRetMonthYear").html('');
            $.each(msg.d.calendar, function (i) {
              var p = this.toString().split('|');
              if (p.length > 1) {
                $("#iDepMonthYear").append('<option value="' + p[2] + '" lastDay="' + p[1] + '">' + p[0] + '</option>');
              }
            });
            $("#iRetMonthYear").html($("#iDepMonthYear").html());
            changeDepYear(msg.d.selectYearMonth);
            setDay(msg.d.selectDay);
            $("#sDepFlightNo").html('');
            $("#sDepFlightNo").append('<option value=""></option>');
            $.each(msg.d.departureFlights, function () {
              $("#sDepFlightNo").append('<option value="' + this.FlightNo + '">' + this.FlightNo + '</option>');
            });
            $("#iDepMonthYear").val(msg.d.selectYearMonth);
            if (msg.d.showReturn == true || msg.d.isRT == true) {
              $("#iRetMonthYear").val(msg.d.selectYearMonthReturn);
              changeRetYear(msg.d.selectYearMonthReturn);
              setDayReturn(msg.d.selectDayReturn);
            }
            $("#iDepArrival").html('');
            $("#iDepArrival").append('<option value=""></option>');
            if ($("#regionDep").val() == '1') {
              $.each(msg.d.departureDestinationList, function (i) {
                $("#iDepArrival").append('<option value="' + this.Code + '">' + this.Name + '</option>');
              });
            } else {
              $("#iDepArrival").append('<option value="' + msg.d.departureDestinationLoc.Code + '">' + msg.d.departureDestinationLoc.Name + '</option>');
            }

            if (msg.d.showReturn == true || msg.d.isRT == true) {
              $("#returnTransferDiv").show();
              $("#arrivalDescrition").html(msg.d.arrivalDescrition);
              $("#arrivalDestination").html(msg.d.arrivalDestination);
              $("#arrivalPrice").html(msg.d.arrivalPrice);
              $("#iRetDeparture").html('');
              $("#iRetDeparture").append('<option value=""></option>');
              if ($("#regionRet").val() == '1') {
                $.each(msg.d.arrivalDestinationList, function (i) {
                  $("#iRetDeparture").append('<option value="' + this.Code + '">' + this.Name + '</option>');
                });
              }
              else {
                $("#iRetDeparture").append('<option value="' + msg.d.arrivalDestinationLoc.Code + '">' + msg.d.arrivalDestinationLoc.Name + '</option>');
              }
              $("#sRetFlightNo").html('');
              $("#sRetFlightNo").append('<option value=""></option>');
              $.each(msg.d.arrivalFlights, function () {
                $("#sRetFlightNo").append('<option value="' + this.FlightNo + '">' + this.FlightNo + '</option>');
              });
            } else {
              $("#returnTransferDiv").hide();
            }

          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMsg(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getCustomers() {
      var cust = '';
      var hfCustCount = parseInt($("#hfCustCount").val());
      var custCount = parseInt(hfCustCount);
      var custList = [];
      for (var i = 1; i <= custCount; i++) {
        var cust = new Object();
        cust.SeqNo = $("#iSeqNo" + i).text();
        cust.Title = $("#iTitle" + i).length > 0 ? $("#iTitle" + i).val() : '';
        cust.Surname = $("#iSurname" + i).length > 0 ? $("#iSurname" + i).val() : '';
        cust.SurnameL = $("#iSurnameL" + i).length > 0 ? $("#iSurnameL" + i).val() : '';
        cust.Name = $("#iName" + i).length > 0 ? $("#iName" + i).val() : '';
        cust.NameL = $("#iNameL" + i).length > 0 ? $("#iNameL" + i).val() : '';
        cust.BirtDay = $("#iBirthDay" + i).length > 0 ? $("#iBirthDay" + i).val() : '';
        cust.Age = $("#iAge" + i).length > 0 ? $("#iAge" + i).val() : '';
        cust.IDNo = $("#iIDNo" + i).length > 0 ? $("#iIDNo" + i).val() : '';
        cust.PassSerie = $("#iPassSerie" + i).length > 0 ? $("#iPassSerie" + i).val() : '';
        cust.PassNo = $("#iPassNo" + i).length > 0 ? $("#iPassNo" + i).val() : '';
        cust.PassIssueDate = $("#iPassIssueDate" + i).length > 0 ? $("#iPassIssueDate" + i).val() : '';
        cust.PassExpDate = $("#iPassExpDate" + i).length > 0 ? $("#iPassExpDate" + i).val() : '';
        cust.PassGiven = $("#iPassGiven" + i).length > 0 ? $("#iPassGiven" + i).val() : '';
        cust.Phone = $("#iPhone" + i).length > 0 ? $("#iPhone" + i).val() : '';
        cust.Nation = $("#iNation" + i).length > 0 ? $("#iNation" + i).val() : '';
        cust.Nationality = $("#iNationality" + i).length > 0 ? $("#iNationality" + i).val() : '';
        cust.Passport = $("#iHasPassport" + i).length > 0 ? $("#iHasPassport" + i)[0].checked : true;
        cust.Leader = $('input[name=Leader]:checked').val() == cust.SeqNo;
        custList.push(cust);
      }
      return JSON.stringify(custList);
    }

    function setCustomers() {
      var data = new Object();
      data.data = JSON.parse(getCustomers());
      $.ajax({
        async: false,
        type: "POST",
        url: "MakeOnlyTransfer.aspx/setCustomers",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          return true;
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMsg(xhr.responseText);
          return false;
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function returnCustInfoEdit(data, source) {
      if (source == "save") {
        $.ajax({
          type: "POST",
          url: "MakeOnlyTransfer.aspx/setResCustInfo",
          data: '{"data":"' + data + '"}',
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (msg) {
            if (source == "save")
              getResCustDiv();
            $("#dialog-resCustomerAddress").dialog("close");
          },
          error: function (xhr, msg, e) {
            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
              showMsg(xhr.responseText);
          },
          statusCode: {
            408: function () {
              logout();
            }
          }
        });
      }
      else {
        $("#dialog-resCustomerAddress").dialog("close");
      }
    }

    function showCustAddress(CustNo) {
      var data = new Object();
      data.data = JSON.parse(getCustomers());
      $.ajax({
        type: "POST",
        url: "MakeOnlyTransfer.aspx/setResCustomers",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var custInfoUrl = 'CustomerAddress.aspx?CustNo=';

          $("#dialog-resCustomerAddress").dialog("open");
          $("#resCustomerAddress").attr("src", custInfoUrl + CustNo);
          return false;
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMsg(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function returnCustDetailEdit(dataS, source) {
      if (source == "save") {
        var data = new Object();
        data.data = dataS;
        $.ajax({
          type: "POST",
          url: "MakeOnlyTransfer.aspx/setResCustomer",
          data: JSON.stringify(data),
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (msg) {
            if (source == "save")
              getResCustDiv();
            $("#dialog-resCustOtherInfo").dialog("close");
          },
          error: function (xhr, msg, e) {
            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
              showMsg(xhr.responseText);
          },
          statusCode: {
            408: function () {
              logout();
            }
          }
        });
      }
      else {
        $("#dialog-resCustOtherInfo").dialog("close");
      }
    }

    function showResCustInfo(CustNo) {
      var data = new Object();
      data.data = JSON.parse(getCustomers());
      $.ajax({
        type: "POST",
        url: "MakeOnlyTransfer.aspx/setResCustomers",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var custInfoUrl = 'CustomerDetail.aspx?CustNo=';

          $("#dialog-resCustOtherInfo").dialog("open");
          $("#resCustOtherInfo").attr("src", custInfoUrl + CustNo);
          return false;
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMsg(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function searchSelectCust(CustNo) {
      $("#dialog-searchCust").dialog("close");
      var Custs = JSON.parse(getCustomers());
      var data = new Object();
      data.CustNo = CustNo;
      data.Custs = Custs;
      $.ajax({
        type: "POST",
        url: "MakeOnlyTransfer.aspx/copyCustomer",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d == 'OK') {
            getResCustDiv(true);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMsg(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function searchCustListShow(_html) {
      $(function () {
        $("#searchCust").html('');
        $("#searchCust").html(_html);
        $("#dialog").dialog("destroy");
        $("#dialog-searchCust").dialog({
          modal: true,
          width: 650,
          height: 450,
          resizable: true,
          autoResize: true
        });
      });
    }

    function searchCust(i) {
      $("#dialog").dialog("destroy");
      $("#searchCust").attr("src", "CustomerSearch.aspx?refNo=" + i + "&SeqNo=" + $("#iSeqNo" + i).text());
      $("#dialog-searchCust").dialog({
        modal: true,
        width: 750,
        height: 500
      });
    }

    function searchCustGetList(i) {
      var Surname = $("#iSurname" + i).val();
      var Name = $("#iName" + i).val();
      var BirthDate = $("#iBirthDay" + i).val();
      var SeqNo = $("#iSeqNo" + i).text();
      $.ajax({
        type: "POST",
        url: "MakeOnlyTransfer.aspx/searchCustomer",
        data: '{"refNo":"' + i + '","SeqNo":"' + SeqNo + '","Surname":"' + Surname + '","Name":"' + Name + '","BirthDate":"' + BirthDate + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d != '') {
            if (msg.d.toString().substring(0, 4) == "Err:") {
              var err = msg.d.toString().substring(4);
              showMsg(err.length > 0 ? err : lblNoResult);
            } else {
              searchCustListShow(msg.d);
            }
          } else {
            showMsg(lblNoResult);
          }
      },
      error: function (xhr, msg, e) {
        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
          showMsg(xhr.responseText);
      },
      statusCode: {
        408: function () {
          logout();
        }
      }
    });
  }

  function getResCustDiv(onlyResCust) {
    $.ajax({
      async: false,
      type: "POST",
      url: "MakeOnlyTransfer.aspx/getResCustDiv",
      data: '{}',
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function (msg) {
        if (msg.hasOwnProperty('d') && msg.d != null) {
          $("#divResCust").html('');
          $("#divResCust").html(msg.d);
          var _dateMask = $("#dateMask").val();
          $(".formatDate").mask(_dateMask);
          if (onlyResCust == true) return;
          getResServiceDiv();
        }
      },
      error: function (xhr, msg, e) {
        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
          showMsg(xhr.responseText);
      },
      statusCode: {
        408: function () {
          logout();
        }
      }
    });
  }

  function getFormData() {
    $.ajax({
      type: "POST",
      url: "MakeOnlyTransfer.aspx/getFormData",
      data: '{}',
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function (msg) {
        if (msg.hasOwnProperty('d') && msg.d != null) {
          if (msg.d.showWhereInvoiceDiv == true) {
            $("#divWhereInvoice").show();
          }
          else {
            $("#divWhereInvoice").hide();
          }
          $("#Direction").val(msg.d.Direction);
          if (msg.d.Direction == 'B') {
            $("#lblDepDate").html('Departure date');
            $("#lblDepFlight").html('Departure flight number');
            $("#lblDepTimeNote").html('Departure time & Note');
            $("#lblDepAccom").html('Accommodation name');
          }

          getResCustDiv(false);
        }
      },
      error: function (xhr, msg, e) {
        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
          showMessage(xhr.responseText);
      },
      statusCode: {
        408: function () {
          logout();
        }
      }
    });
  }

  function cancelMakeRes() {
    window.close;
    self.parent.cancelMakeRes();
  }

  function getData() {
    var obj = new Object();
    obj.CustData = JSON.parse(getCustomers());
    obj.DepMonthYear = $("#iDepMonthYear").val();
    obj.DepDay = $("#iDepDay").val();
    obj.DepFlightNo = $("#depFlightInputType").attr('checked') == 'checked' ? $("#iDepFlightNo").val() : $("#sDepFlightNo").val();
    obj.DepHour = $("#depHour").val();
    obj.DepMinute = $("#depMinute").val();
    obj.DepPickupNote = $("#depPickupNote").val();
    obj.DepArrival = $("#iDepArrival").val();
    obj.RetMonthYear = $("#iRetMonthYear").val();
    obj.RetDay = $("#iRetDay").val();
    obj.RetFlightNo = $("#retFlightInputType").attr('checked') == 'checked' ? $("#iRetFlightNo").val() : $("#sRetFlightNo").val();
    obj.RetHour = $("#retHour").val();
    obj.RetMinute = $("#retMinute").val();
    obj.RetPickupNote = $("#retPickupNote").val();
    obj.RetDeparture = $("#iRetDeparture").val();
    obj.PickupType = $("#pickupType").val();
    obj.InvoiceTo = $("#iInvoiceTo").val();
    obj.Direction = $("#Direction").val();
    obj.DepRegion = $("#regionDep").val();
    obj.RetRegion = $("#regionRet").val();
    return obj;
  }

  function checkData() {
    var obj = getData();
    if (obj.DepMonthYear == '' ||
        obj.DepDay == '' ||
        obj.DepFlightNo == '' ||      
        obj.DepArrival == '' ||
        ($("#Direction").val() == "R" && obj.RetMonthYear == '') ||
        ($("#Direction").val() == "R" && obj.RetDay == '') ||
        ($("#Direction").val() == "R" && obj.RetFlightNo == '') ||      
        (obj.PickupType == '1' && obj.RetDeparture == '')) {
      return false;
    }
    else {
      return true;
    }
  }

  function saveMakeRes(params) {
    if (checkData()) {
      if ($("#divWhereInvoice").css('display') == 'block') {
        if ($("#iInvoiceTo").val() == '' || $("#iInvoiceTo").val() == undefined) {
          showMsg(WhereInvoiceTo);
          return;
        }
      }

      setCustomers();
      if (reservationSaved) {
        showMsg(lblAlreadyRes);
        return;
      }
      else {
        reservationSaved = true;
      }
      $.ajax({
        async: false,
        type: "POST",
        url: "MakeOnlyTransfer.aspx/saveReservation",
        data: JSON.stringify(getData()),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var retVal = msg.d[0];
            if (retVal.ControlOK == true) {
              if (retVal.GotoPaymentPage == true)
                showDialogEndGotoPaymentPage(retVal.Message, retVal.ResNo);
              else if (retVal.GotoReservation)
                showDialogEndExit(retVal.Message, retVal.ResNo);
              else showDialogEndExit(retVal.Message, '');
            }
            else {
              showMessage(retVal.Message);
              reservationSaved = false;
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }
    else {
      showMessage(mkPleaseCompleteAllTheInformation);
    }
  }

  function dialogInitialize() {
    $("#dialog-resCustomerAddress").dialog({ autoOpen: false, modal: true, width: 600, height: 535, resizable: false, autoResize: true });
    $("#dialog-resCustOtherInfo").dialog({ autoOpen: false, modal: true, width: 440, height: 470, resizable: true, autoResize: true });
  }

  $(document).ready(function () {
    dialogInitialize();
    $("#onlyTransfer").val($.query.get('onlytransfer') == '1' ? '1' : '0');
    $.ajax({
      async: false,
      type: "POST",
      url: "MakeOnlyTransfer.aspx/userBlackList",
      data: '{}',
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function (msg) {
        if (msg.hasOwnProperty('d') && msg.d != null) {
          if (msg.d.isBlackList) {
            showMessage(msg.d.message);
          }
          else {
            top.window.moveTo(0, 0);
            getFormData();
          }
        }
      },
      error: function (xhr, msg, e) {
        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
          showMessage(xhr.responseText);
      },
      statusCode: {
        408: function () {
          logout();
        }
      }
    });
  });

  </script>
</head>
<body>
  <form id="makeReservationForm" runat="server">
    <input id="customRegID" type="hidden" />
    <input id="onlyTransfer" type="hidden" />
    <input id="Direction" type="hidden" />
    <input id="isRT" type="hidden" value="0" />
    <div style="width: 900px" class="ui-widget">
      <br />
      <span class="MakeResOnlyTicketHeader"><%= GetGlobalResourceObject("OnlyTransfer","mkReviewAndCompleteYourBooking") %></span>
      <hr />
      <div id="divResCust" class="resCustGridCss">
      </div>
      <hr />
      <div id="divResService">
        <div id="departureTransferDiv" class="ui-helper-clearfix">
          <strong><%= GetGlobalResourceObject("OnlyTransfer","mkArrivalToTheDestination") %></strong>
          <br />
          <span id="departureDescrition"></span>
          <br />
          <span id="departureDestination"></span>
          <br />
          <span id="departurePrice"></span>
          <br />
          <div class="ui-helper-clearfix resServiceDiv">
            <div class="caption"><span id="lblDepDate"><%= GetGlobalResourceObject("OnlyTransfer","mkArrivalDate") %></span></div>
            <div class="seperator"><span>:</span></div>
            <div class="value">
              <select id="iDepMonthYear" class="dateMonthYear" onchange="changeDepYear(this.value)" disabled="disabled"></select>
              <select id="iDepDay" class="dateDay" disabled="disabled"></select>
            </div>
          </div>
          <div class="ui-helper-clearfix resServiceDiv">
            <div class="caption"><span id="lblDepFlight"><%= GetGlobalResourceObject("OnlyTransfer","mkArrivalFlightNumber") %></span></div>
            <div class="seperator"><span>:</span></div>
            <div class="value">
              <select id="sDepFlightNo" class="sFlightNo combo"></select>
              <input id="iDepFlightNo" type="text" class="flightNo inputText" style="display: none;" />
              <input id="depFlightInputType" type="checkbox" class="manuelInput" onclick="changeDepFlightInputType()" /><label for="depFlightInputType" class="manuelInputLabel"><%= GetGlobalResourceObject("OnlyTransfer","mkManuelInput") %></label>
            </div>
          </div>
          <div class="ui-helper-clearfix resServiceDiv" style="display: block;">
            <div class="caption"><span id="lblDepTimeNote"><%= GetGlobalResourceObject("OnlyTransfer","mkArrivalTimeNote") %></span></div>
            <div class="seperator"><span>:</span></div>
            <div class="value">
              <select id="depHour" class="hour">
                <option value=""></option>
                <option value="00">00</option>
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
              </select>
              <select id="depMinute" class="minute">
                <option value=""></option>
                <option value="00">00</option>
                <option value="05">05</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
                <option value="25">25</option>
                <option value="30">30</option>
                <option value="35">35</option>
                <option value="40">40</option>
                <option value="45">45</option>
                <option value="50">50</option>
                <option value="55">55</option>
              </select>
              <input id="depPickupNote" type="text" class="pickupNote" maxlength="50" />
            </div>
          </div>
          <div class="ui-helper-clearfix resServiceDiv">
            <div class="caption"><span id="lblDepAccom"><%= GetGlobalResourceObject("OnlyTransfer","mkAccommodationName") %></span></div>
            <div class="seperator"><span>:</span></div>
            <div class="value">
              <select id="regionDep" class="combo" onchange="changeRegionFrom()">
                <option value="1"><%=GetGlobalResourceObject("LibraryResource", "AddServiceHotel") %></option>
                <option value="2" selected="selected"><%=GetGlobalResourceObject("LibraryResource", "AddServiceOther") %></option>
              </select>
              <select id="iDepArrival" class="accom"></select>
            </div>
          </div>
        </div>
        <br />
        <br />
        <hr />
        <br />
        <br />
        <div id="returnTransferDiv" style="display: block;" class="ui-helper-clearfix">
          <strong><%= GetGlobalResourceObject("OnlyTransfer","mkDepartureFromTheDestination") %></strong>
          <br />
          <span id="arrivalDescrition"></span>
          <br />
          <span id="arrivalDestination"></span>
          <br />
          <span id="arrivalPrice"></span>
          <br />
          <div class="ui-helper-clearfix resServiceDiv">
            <div class="caption"><span><%= GetGlobalResourceObject("OnlyTransfer","mkDepartureDate") %></span></div>
            <div class="seperator"><span>:</span></div>
            <div class="value">
              <select id="iRetMonthYear" class="dateMonthYear" onchange="changeRetYear(this.value)" disabled="disabled"></select>
              <select id="iRetDay" class="dateDay" disabled="disabled"></select>
            </div>
          </div>
          <div class="ui-helper-clearfix resServiceDiv">
            <div class="caption"><span><%= GetGlobalResourceObject("OnlyTransfer","mkDepartureFlightNumber") %></span></div>
            <div class="seperator"><span>:</span></div>
            <div class="value">
              <select id="sRetFlightNo" class="sFlightNo combo"></select>
              <input id="iRetFlightNo" type="text" class="flightNo inputText" style="display: none;" />
              <input id="retFlightInputType" type="checkbox" class="manuelInput" onclick="changeRetFlightInputType()" /><label for="retFlightInputType" class="manuelInputLabel"><%= GetGlobalResourceObject("OnlyTransfer","mkManuelInput") %></label>
            </div>
          </div>
          <div class="ui-helper-clearfix resServiceDiv" style="display: block;">
            <div class="caption"><span><%= GetGlobalResourceObject("OnlyTransfer","mkDepartureFlightTimeNote") %></span></div>
            <div class="seperator"><span>:</span></div>
            <div class="value">
              <select id="retHour" class="hour">
                <option value=""></option>
                <option value="00">00</option>
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
              </select>
              <select id="retMinute" class="minute">
                <option value=""></option>
                <option value="00">00</option>
                <option value="05">05</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
                <option value="25">25</option>
                <option value="30">30</option>
                <option value="35">35</option>
                <option value="40">40</option>
                <option value="45">45</option>
                <option value="50">50</option>
                <option value="55">55</option>
              </select>
              <input id="retPickupNote" type="text" class="pickupNote" maxlength="50" />
            </div>
          </div>
          <div class="ui-helper-clearfix resServiceDiv">
            <div class="caption"><span><%= GetGlobalResourceObject("OnlyTransfer","mkPickupPoint") %></span></div>
            <div class="seperator"><span>:</span></div>
            <div class="value">
              <select id="pickupType" onchange="changePickupType()">
                <option value="0"><%= GetGlobalResourceObject("OnlyTransfer","mkWhereIGotDroppedOffOnArrival") %></option>
                <option value="1"><%= GetGlobalResourceObject("OnlyTransfer","mkDifferentThanArrival") %></option>
              </select>
            </div>
          </div>
          <div id="retDepartureDiv" class="ui-helper-clearfix resServiceDiv" style="display: none;">
            <div class="caption"><span><%= GetGlobalResourceObject("OnlyTransfer","mkAccomodationName") %></span></div>
            <div class="seperator"><span>:</span></div>
            <div class="value">
              <select id="regionRet" class="combo" onchange="changeRegionReturn()">
                <option value="1"><%=GetGlobalResourceObject("LibraryResource", "AddServiceHotel") %></option>
                <option value="2" selected="selected"><%=GetGlobalResourceObject("LibraryResource", "AddServiceOther") %></option>
              </select>
              <select id="iRetDeparture" class="accom"></select>
            </div>
          </div>
        </div>
      </div>
      <div id="divWhereInvoice" style="clear: both; display: none;">
        <strong>
          <%= GetGlobalResourceObject("MakeReservation", "whereInvoice")%>:</strong>
        <select id="iInvoiceTo" style="width: 200px;">
          <option value=''><%= GetGlobalResourceObject("LibraryResource", "ComboSelect") %></option>
          <option value='0'><%= GetGlobalResourceObject("LibraryResource", "ForAgency") %></option>
          <option value='1'><%= GetGlobalResourceObject("LibraryResource", "ForPassenger") %></option>
        </select>
      </div>
      <hr />
      <br />
      <div style="text-align: center;">
        <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource","btnSaveReservation").ToString() %>'
          onclick="saveMakeRes('');" style="min-width: 100px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
        &nbsp;&nbsp;
                <input id="btnCancel" type="button" value='<%= GetGlobalResourceObject("LibraryResource","btnCancel").ToString() %>'
                  onclick="cancelMakeRes();" style="min-width: 100px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
      </div>
    </div>
  </form>

  <div id="dialog-message" title="" style="display: none;">
    <span id="messages">Message</span>
  </div>
  <div id="dialog-resCustomerAddress" title='<%= GetGlobalResourceObject("MakeReservation", "lblCustomerAddress") %>'
    style="display: none;">
    <iframe id="resCustomerAddress" runat="server" style="clear: both; height: 490px; width: 570px;" frameborder="0"></iframe>
  </div>
  <div id="dialog-resCustOtherInfo" title='<%= GetGlobalResourceObject("MakeReservation", "lblCustomerOtherInfo") %>'
    style="display: none;">
    <iframe id="resCustOtherInfo" runat="server" style="clear: both; height: 430px; width: 410px;" frameborder="0"></iframe>
  </div>
  <div id="dialog-searchCust" title='<%= GetGlobalResourceObject("MakeReservation", "searchCustLabel") %>'
    style="display: none;">
    <iframe id="searchCust" runat="server" height="100%" width="100%" frameborder="0"
      style="clear: both;"></iframe>
    <%--<div id="searchCust">
            </div--%>>
  </div>
  <hr />
</body>
</html>
