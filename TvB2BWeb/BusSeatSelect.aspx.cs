﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Web.Services;
using System.Threading;
using System.Text;

public partial class BusSeatSelect : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [WebMethod(EnableSession = true)]
    public static string getSeatPlan(string Service, string Bus, long? Date, int? CustNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        StringBuilder sb = new StringBuilder();

        ResCustRecord cust = ResData.ResCust.Find(f => f.CustNo == CustNo);
        List<BusSeatPlanRecord> seatPlan = new Transports().getSeatPlan(UserData.Market, Bus, ref errorMsg);
        if (seatPlan != null && seatPlan.Count > 0)
        {
            List<TransportCInRecord> seatUse = new Transports().getUsedSeat(UserData.Market, Service, Date.HasValue ? new DateTime().AddTicks(Date.Value) : DateTime.Today, Bus, ref errorMsg);
            int? colCnt = seatPlan.Where(w => w.RowNo == 1).Count();
            var rowCntQ = from q in seatPlan
                          group q by new { q.RowNo } into k
                          select new { RowNo = k.Key.RowNo };
            sb.AppendFormat("<input id=\"custNo\" type=\"hidden\" value=\"{0}\" />", CustNo);
            sb.AppendFormat("<input id=\"busCode\" type=\"hidden\" value=\"{0}\" />", Bus);
            BusRecord bus = new Transports().getBusRecord(UserData.Market, Bus, ref errorMsg);
            sb.AppendFormat("<h2>{0} - {1}</h2>",
                    bus.Name,
                    new DateTime().AddTicks(Date.Value).ToShortDateString());
            sb.Append("<table cellpadding=\"0\" cellspacing=\"0\">");
            sb.AppendFormat("<tr><td colspan=\"{0}\" style=\"height: 56px;\"><img alt=\"\" src=\"Images/BusFront.png\" /></td></tr>", colCnt);

            string leftSideBorder = "border-left: solid 2px #000;";
            string rightSideBorder = "border-right: solid 2px #000;";
            foreach (var row in rowCntQ)
            {
                sb.Append("<tr>");
                for (int i = 1; i < colCnt + 1; i++)
                {
                    string selectSeatScript = string.Format("onclick=\"selectSeat({0},{1},'{2}','{3}',{4},{5});\"",
                                row.RowNo,
                                i,
                                Service,
                                Bus,
                                Date,
                                CustNo);

                    BusSeatPlanRecord seat = seatPlan.Find(f => f.RowNo == row.RowNo && f.ColNo == i);
                    //if (seat != null)
                        
                    TransportCInRecord inUseSeat = seatUse.Find(f =>seat!=null && f.SeatLetter == seat.SeatLetter);
                    bool inUse = inUseSeat != null;
                    bool selected = seat!=null && seat.ForB2B.HasValue && seat.ForB2B.Value && !inUse && !string.IsNullOrEmpty(seat.SeatLetter);
                    if (cust.Title > 5 && (seat!=null && seat.ChdRestriction == true || seat.IsExit == true))
                        selected = false;

                    sb.AppendFormat("<td style=\"{0}; {1} height: 30px;\" {2} >",
                                        i == 1 ? leftSideBorder : (i == colCnt ? rightSideBorder : ""),
                                        seat != null && !string.IsNullOrEmpty(seat.SeatLetter) ? "width: 22px;" : "width: 18px;",
                                        selected ? selectSeatScript : "");
                    if (seat != null)
                    {
                        if (!string.IsNullOrEmpty(seat.SeatLetter))
                            sb.AppendFormat("<img alt=\"{0}\" title=\"{0}\" src=\"Images/SeatBus{1}.png\" style=\"width: 22px;\" />",
                                                seat.SeatLetter,
                                                inUse ? "Full" : (selected ? "": "NoSelectV1"));
                        else
                            if (seat.CellType == 3)
                                sb.AppendFormat("WC");
                            else sb.AppendFormat("&nbsp;");
                    }
                    sb.Append("</td>");
                }
                sb.Append("</tr>");
            }
            sb.Append("</table>");
        }

        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getCustomers()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        StringBuilder sb = new StringBuilder();

        List<ResServiceRecord> resServiceList = ResData.ResService;

        List<ResServiceRecord> transportCustomers = (from q in resServiceList
                                                     where string.Equals(q.ServiceType, "TRANSPORT")
                                                     select q).ToList<ResServiceRecord>();

        var resBuses = from q in resServiceList
                       where string.Equals(q.ServiceType, "TRANSPORT")
                       group q by new { q.BegDate, q.Service, q.Bus } into k
                       select new { transportDate = k.Key.BegDate, Service = k.Key.Service, Bus = k.Key.Bus };

        foreach (var row in resBuses)
        {
            TransportDetailRecord busDetail = new Transports().getTransportDetail(UserData.Market, ResData.ResMain.PLMarket, row.Service, row.Bus, row.transportDate.Value, ref errorMsg);
            if (busDetail != null)
            {
                List<TransportCInRecord> seatUse = new Transports().getUsedSeat(UserData.Market, row.Service, row.transportDate.Value, row.Bus, ref errorMsg);
                var busCustomers = from rS in resServiceList
                                   join rC in ResData.ResCon on rS.RecID equals rC.ServiceID
                                   join C in ResData.ResCust on rC.CustNo equals C.CustNo
                                   where rS.Service == row.Service &&
                                        rS.BegDate == row.transportDate &&
                                        rS.Bus == row.Bus
                                   select new { C.CustNo, C.TitleStr, C.Surname, C.Name };

                if (busCustomers.Count() > 0)
                {
                    sb.AppendFormat("<span id=\"{1}\" name=\"buses\" class=\"transport\">{0}</span><br />",
                        busDetail.NameL + ", " + (row.transportDate.HasValue ? row.transportDate.Value.ToShortDateString() : "") + ", (" + busDetail.RouteFromNameL + " -> " + busDetail.RouteToNameL + ")",
                        row.Service + "_" + row.Bus + "_" + row.transportDate.Value.Ticks.ToString());
                    foreach (var r1 in busCustomers)
                    {
                        TransportCInRecord seat = seatUse.Find(f => f.Bus == row.Bus && f.Transport == row.Service && f.TransportDate == row.transportDate && f.CustNo == r1.CustNo);
                        if (seat == null)
                            sb.AppendFormat("<span class=\"custSelect\" name=\"custs\" onclick=\"selectCustumer({0},{1},'{2}','{3}',this);\"><img alt=\"\" src=\"Images/{5}\" /> {4}</span><br />",
                                r1.CustNo,
                                busDetail.TransDate.Ticks,
                                row.Service,
                                row.Bus,
                                r1.TitleStr + ". " + r1.Surname + " " + r1.Name,
                                "nochecked_16.gif");
                        else
                        {
                            string seatNo = string.Empty;
                            List<TransportCInRecord> seats = seatUse.Where(w => w.Bus == row.Bus && w.Transport == row.Service && w.TransportDate == row.transportDate && w.CustNo == r1.CustNo).ToList<TransportCInRecord>();
                            foreach (TransportCInRecord row1 in seats)
                            {
                                if (seatNo.Length > 0) seatNo += "-";
                                seatNo += row1.SeatLetter;
                            }
                            sb.AppendFormat("<span class=\"cust\">{0}</span><br />",
                                seatNo + " " + r1.TitleStr + ". " + r1.Surname + " " + r1.Name);
                        }
                    }
                }
            }
        }

        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getSeatCust(Int16? Row, Int16? Col, int? CustNo, string Bus, string Service, long? BusDate)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        StringBuilder sb = new StringBuilder();
        ResCustRecord cust = ResData.ResCust.Find(f => f.CustNo == CustNo);
        if (cust != null && Row.HasValue && Col.HasValue)
        {
            List<BusSeatPlanRecord> seatPlan = new Transports().getSeatPlan(UserData.Market, Bus, ref errorMsg);
            BusSeatPlanRecord seat = seatPlan.Find(f => f.RowNo == Row && f.ColNo == Col);
            var query = from q1 in ResData.ResService
                        where q1.Bus == Bus
                        select q1;
            DateTime Date = BusDate.HasValue ? new DateTime().AddTicks(BusDate.Value) : DateTime.MinValue;
            List<TransportCInRecord> seatUse = new Transports().getUsedSeat(UserData.Market, Service, Date, Bus, ref errorMsg);
            if (seatUse.Where(w => w.CustNo == CustNo).Count() > 0)
            {
                return string.Empty;
            }            
            var seatPlanRow = seatPlan.Where(f => f.RowNo == Row).OrderBy(o => o.ColNo);
            Int16? lastCol = seatPlanRow.LastOrDefault().ColNo;
            BusSeatPlanRecord extraSeat = null;
            BusSeatPlanRecord prevSeat = seatPlan.Find(f => f.RowNo == Row && f.ColNo == Col.Value - 1);
            BusSeatPlanRecord nextSeat = seatPlan.Find(f => f.RowNo == Row && f.ColNo == Col.Value + 1);
            bool extSeat = false;
            if (nextSeat != null && nextSeat.ForB2B == true && !string.IsNullOrEmpty(nextSeat.SeatLetter))
            {
                TransportCInRecord nextSeatUse = seatUse.Find(f => f.BusSeatNo == nextSeat.SeatNo);
                if (nextSeatUse == null)
                {
                    extraSeat = nextSeat;
                    extSeat = true;
                }
            }
            if (prevSeat != null && prevSeat.ForB2B == true && !string.IsNullOrEmpty(prevSeat.SeatLetter) && !extSeat)
            {
                TransportCInRecord prevSeatUse = seatUse.Find(f => f.BusSeatNo == prevSeat.SeatNo);
                if (prevSeatUse == null)
                {
                    extraSeat = prevSeat;
                    extSeat = true;
                }
            }
            sb.AppendFormat("<h1>Passenger : {0}</h1><br /><br />", cust.TitleStr + ". " + cust.Surname + " " + cust.Name);
            sb.AppendFormat("<h1>Seat : {0}</h1><br /><br />", seat.SeatLetter);
            //if (extraSeat != null)
            //    sb.AppendFormat("<h1>Extra seat: <input id=\"extraSeat\" type=\"checkbox\" value=\"{1}\" /><label for=\"extraSeat\">{0}</label></h1><br /><br />",
            //            extraSeat.SeatLetter,
            //            extraSeat.SeatNo);
            sb.AppendFormat("<input type=\"button\" value=\"{0}\" onclick=\"saveSeat({1},{2},'{3}',{4},'{5}');\" />",
                HttpContext.GetGlobalResourceObject("LibraryResource", "btnSave"),
                seat.SeatNo,
                CustNo,
                Service,
                BusDate,
                Bus);
        }
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string saveSeat(Int16? seatNo, int? custNo, string Service, long? BusDate, string Bus, Int16? extraSeat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        ResCustRecord cust = ResData.ResCust.Find(f => f.CustNo == custNo);
        if (cust != null && BusDate.HasValue) {
            DateTime transportDate = new DateTime().AddTicks(BusDate.Value);
            List<TitleRecord> titles = new TvBo.Common().getTitle(ref errorMsg);
            TitleRecord title = titles.Find(f => f.TitleNo == cust.Title);
            var query = from q1 in ResData.ResCon
                        join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                        where q1.CustNo == custNo
                          && q2.ServiceType == "TRANSPORT"
                          && q2.Service == Service
                          && q2.BegDate == transportDate
                          && q2.Bus == Bus
                        select new { RecID = q2.RecID };
            List<BusSeatPlanRecord> seatPlan = new Transports().getSeatPlan(UserData.Market, Bus, ref errorMsg);
            BusSeatPlanRecord seat = seatPlan.Find(f => f.SeatNo == seatNo);
            if (new Transports().setUsedSeat(UserData, Service, transportDate, Bus, custNo, seatNo, query.FirstOrDefault().RecID, seat != null ? seat.SeatLetter : "", title != null ? title.Sex : "", "", extraSeat, ref errorMsg)) {
                if (extraSeat.HasValue) {
                    seat = seatPlan.Find(f => f.SeatNo == extraSeat);
                    if (new Transports().setUsedSeat(UserData, Service, transportDate, Bus, custNo, extraSeat, query.FirstOrDefault().RecID, seat != null ? seat.SeatLetter : "", title != null ? title.Sex : "", "", 0, ref errorMsg)) {
                        return "OK";
                    } else {
                        return "Extra seat not used.";
                    }
                }
                return "OK";
            } else {
                return "Seat not used.";
            }
        } else {
            return "Customer not found or not valid bus date.";
        }
    }
}
