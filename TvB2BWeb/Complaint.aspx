﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Complaint.aspx.cs" Inherits="Complaint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title></title>
  <link rel="shortcut icon" href="http://www.santsg.com/wp-content/uploads/2015/04/favicon.png" />
  <script src="Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery.datepick/jquery-ui-i18n.js" type="text/javascript"></script>
  <script src="Scripts/json2.js" type="text/javascript"></script>
  <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
  <script src="Scripts/jquery.dialogextend.js" type="text/javascript"></script>
  <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
  <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
  <script src="Scripts/datatable/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="Scripts/datatable/jquery.dataTables.Pagination.ListBox.js" type="text/javascript"></script>

  <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="CSS/main.css" rel="stylesheet" type="text/css" />
  <link href="CSS/Complaint.css" rel="stylesheet" type="text/css" />
  <link href="CSS/datatable/jquery.dataTables.css" rel="stylesheet" />
  <link href="CSS/datatable/jquery.dataTables_themeroller.css" rel="stylesheet" />

  <style type="text/css">
    .dataTables_length { height: 30px !important; }
  </style>

  <script language="javascript" type="text/javascript">

    var twoLetterISOLanguageName = '<%= twoLetterISOLanguageName %>';
    var basePageRoot = '<%= Global.getBasePageRoot() %>';
    var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
    var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
    var btnClose = '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>';
    var ComboAll = '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>';
    var dataTableColVisText = '<%= GetGlobalResourceObject("LibraryResource", "dataTableColVisText") %>';
    var sProcessing = '<%= GetGlobalResourceObject("LibraryResource", "sProcessing") %>';
    var sLengthMenu = '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>';
    var sZeroRecords = '<%= GetGlobalResourceObject("LibraryResource", "sZeroRecords") %>';
    var sEmptyTable = '<%= GetGlobalResourceObject("LibraryResource", "sEmptyTable") %>';
    var sInfo = '<%= GetGlobalResourceObject("LibraryResource", "sInfo") %>';
    var sInfoEmpty = '<%= GetGlobalResourceObject("LibraryResource", "sInfoEmpty") %>';
    var sInfoFiltered = '<%= GetGlobalResourceObject("LibraryResource", "sInfoFiltered") %>';
    var sInfoPostFix = '<%= GetGlobalResourceObject("LibraryResource", "sInfoPostFix") %>';
    var sSearch = '<%= GetGlobalResourceObject("LibraryResource", "sSearch") %>';
    var sUrl = '<%= GetGlobalResourceObject("LibraryResource", "sUrl") %>';
    var sFirst = '<%= GetGlobalResourceObject("LibraryResource", "sFirst") %>';
    var sPrevious = '<%= GetGlobalResourceObject("LibraryResource", "sPrevious") %>';
    var sNext = '<%= GetGlobalResourceObject("LibraryResource", "sNext") %>';
    var sLast = '<%= GetGlobalResourceObject("LibraryResource", "sLast") %>';
    var sPage = '<%= GetGlobalResourceObject("LibraryResource", "sPage") %>';
    var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
    var lblSessionEnd = '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>';

    $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '</h1>';
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    function closeComplaint() {
      $("#dialog-ViewEditComplaint").dialog('close');
    }

    function saveAndCloseComplaint() {
      $("#dialog-ViewEditComplaint").dialog('close');
    }

    function onclickViewEdit(ComplaintID) {
      window.scrollTo(0, 0);
      $('html').css('overflow', 'hidden');
      $("#ViewEditComplaint").attr("src", basePageRoot + 'ComplaintEdit.aspx?RecID=' + ComplaintID + '&Edit=1');
      $("#dialog-ViewEditComplaint").dialog(
          {
            autoOpen: true,
            modal: true,
            width: 990,
            height: 700,
            resizable: true,
            autoResize: true,
            bigframe: true,
            close: function (event, ui) { $('html').css('overflow', 'auto'); }
          }).dialogExtend({
            "maximize": true,
            "icons": {
              "maximize": "ui-icon-circle-plus",
              "restore": "ui-icon-pause"
            }
          });
      $("#dialog-ViewEditComplaint").dialogExtend("maximize");
    }

    function getResult() {
      var oTable = null;
      var _aoColumns = [];
      $.ajax({
        async: false,
        type: "POST",
        data: '{}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'Complaint.aspx/getGridHeaders',
        success: function (msg) {
          if (msg.d != null && msg.d != '') {
            $.each(msg.d, function (i) {
              obj = new Object();
              if (this.sTitle != undefined && this.sTitle != null) obj.sTitle = this.sTitle;
              if (this.sWidth != undefined && this.sWidth != null) obj.sWidth = this.sWidth;
              if (this.bSortable != undefined && this.bSortable != null) obj.bSortable = this.bSortable;
              if (this.sType != undefined && this.sType != null) obj.sType = this.sType;
              if (this.sClass != undefined && this.sClass != null) obj.sClass = this.sClass;
              if (this.bSearchable != undefined && this.bSearchable != null) obj.bSearchable = this.bSearchable;
              _aoColumns.push(obj);
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });

      var oLanguage = {
        "sProcessing": sProcessing,
        "sLengthMenu": sLengthMenu,
        "sZeroRecords": sZeroRecords,
        "sEmptyTable": sEmptyTable,
        "sInfo": sInfo,
        "sInfoEmpty": sInfoEmpty,
        "sInfoFiltered": sInfoFiltered,
        "sInfoPostFix": sInfoPostFix,
        "sSearch": sSearch,
        "sUrl": sUrl,
        "oPaginate": {
          "sFirst": sFirst,
          "sPrevious": sPrevious,
          "sNext": sNext,
          "sLast": sLast,
          "sPage": sPage
        },
        "fnInfoCallback": null
      };
      var first = true;

      oTable = $('#resultTable').dataTable({
        "sDom": 'C<"clear">lfrtip',
        "bDestory": false,
        "bRetrieve": true,
        "bStateSave": true,        
        "bAutoWidth": false,
        "bScrollCollapse": false,
        "bPaginate": true,
        "sPaginationType": "listbox",
        "bFilter": false,
        "bJQueryUI": true,
        "oLanguage": oLanguage,
        "aoColumns": _aoColumns,
        "bProcessing": false,
        "bServerSide": true,        
        "sServerMethod": "POST",
        "sAjaxSource": "ComplaintData.aspx",
        "fnStateSave": function (oSettings, oData) {
          localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
          if (localStorage != undefined && localStorage.length > 0)
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        "fnStateLoadParams": function (oSettings, oData) {
          if (first) {
            oData.iStart = 0;
            oSettings._iDisplayStart = 0;
            if (oData.oFilter != null && oData.oFilter != undefined && oData.oFilter.sSearch != null && oData.oFilter.sSearch != undefined)
              oData.oFilter.sSearch = "";
          }
          first = false;
        },
        "fnServerData": function (sSource, aoData, fnCallback) {
          var tmp = sSource;
          $.ajax({
            async: false,
            "dataType": 'json',
            "type": "POST",
            "url": sSource,
            "data": aoData,
            "success": function (data, textStatus, xmlHttpRequest) {

              var json = {
                "sEcho": aoData[0].value,
                "aaData": [],
                "iTotalRecords": data.iTotalRecords,
                "iTotalDisplayRecords": data.iTotalDisplayRecords
              };
              $.each(data.aaData, function (i) {
                var obj = [];
                if (this.ViewEdit != undefined) obj.push(this.ViewEdit);
                if (this.Attach != undefined) obj.push(this.Attach);
                if (this.Department != undefined) obj.push(this.Department);
                if (this.Hotel != undefined) obj.push(this.Hotel);
                if (this.Source != undefined) obj.push(this.Source);
                if (this.Status != undefined) obj.push(this.Status);
                if (this.Category != undefined) obj.push(this.Category);
                if (this.ReceivedDate != undefined) obj.push(this.ReceivedDate);
                if (this.Active != undefined) obj.push(this.Active);
                if (this.ComplaintType != undefined) obj.push(this.ComplaintType);
                json.aaData.push(obj);
              });

              fnCallback(json);
            },
            error: function (xhr, msg, e) {
              if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                alert(xhr.responseText);
            },
            statusCode: {
              408: function () {
                //logout();
              }
            }
          });
        }
      });      
    }

    function getComplaintData() {

      $(".resultArea").html('');
      $(".resultArea").html('<table cellspacing="0" cellpadding="0" border="0" id="resultTable" class="display" style="margin-left: 0pt;"></table>');

      var params = new Object();
      params.BegDate = $("#fltReceivedDateBeg").datepicker('getDate') != null ? $("#fltReceivedDateBeg").datepicker('getDate') : null;
      params.EndDate = $("#fltReceivedDateEnd").datepicker('getDate') != null ? $("#fltReceivedDateEnd").datepicker('getDate') : null;
      params.Type = $("#fltType").val();
      params.Active = $("#fltActive").val() != '' ? $("#fltActive").val() == '1' : null;
      params.Department = $("#fltDepartment").val();
      params.Hotel = $("#fltHotel").val();

      $.ajax({
        type: "POST",
        data: JSON.stringify(params),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "Complaint.aspx/getComplaintData",
        success: function (response) {
          if (response.hasOwnProperty('d') && response.d != null) {
            if (response.d == true) {
              getResult();
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            alert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getFormData() {
      $.ajax({
        type: "POST",
        data: "{}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "Complaint.aspx/getFormData",
        success: function (response) {
          if (response.hasOwnProperty('d') && response.d != null) {
            if (response.d.hasOwnProperty('Department') && response.d.Department != null) {
              $("#fltDepartment").html('');
              $("#fltDepartment").append('<option value="">-</option>');
              $.each(response.d.Department, function (i) {
                $("#fltDepartment").append('<option value="' + this.Code + '">' + this.Name + '</option>');
              });
            }
            if (response.d.hasOwnProperty('HotelList') && response.d.HotelList != null) {
              $("#fltHotel").html('');
              $("#fltHotel").append('<option value="">-</option>');
              $.each(response.d.HotelList, function (i) {
                $("#fltHotel").append('<option value="' + this.Code + '">' + this.Name + '</option>');
              });
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            alert(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    $(document).ready(function () {

      $.datepicker.setDefaults($.datepicker.regional[twoLetterISOLanguageName != 'en' ? twoLetterISOLanguageName : '']);
      $("#fltReceivedDateBeg").datepicker({
        showOn: "button",
        buttonImage: "Images/Calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onSelect: function (dateText, inst) {
          if (dateText != '') {
            var fltReceivedDateBeg = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
            if ($("#fltReceivedDateEnd").datepicker('getDate') != null) {
              var fltReceivedDateEnd = $("#fltReceivedDateEnd").datepicker('getDate');
              if (fltReceivedDateBeg.getTime() > fltReceivedDateEnd.getTime()) {
                $("#fltReceivedDateEnd").datepicker("setDate", fltReceivedDateBeg);
              }
            }
          }
        }
      });
      $("#fltReceivedDateEnd").datepicker({
        showOn: "button",
        buttonImage: "Images/Calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onSelect: function (dateText, inst) {
          if (dateText != '') {
            var fltReceivedDateEnd = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
            if ($("#fltReceivedDateBeg").datepicker('getDate') != null) {
              var fltReceivedDateBeg = $("#fltReceivedDateBeg").datepicker('getDate');
              if (fltReceivedDateEnd.getTime() < fltReceivedDateBeg.getTime()) {
                $("#fltReceivedDateBeg").datepicker("setDate", fltReceivedDateEnd);
              }
            }
          }
        }
      });
      getFormData();
    });

  </script>
</head>
<body class="ui-helper-reset">
  <form id="form1" runat="server">
    <div class="Page">
      <tv1:Header ID="tvHeader" runat="server" />
      <tv1:MainMenu ID="tvMenu" runat="server" />
      <div class="ui-helper-clearfix filterArea">
        <div class="filterLeft">
          <div class="ui-helper-clearfix receivedDate">
            <div class="receivedDateLabel">
              <span>Received date :</span>
            </div>
            <div class="receivedDateInput">
              <input id="fltReceivedDateBeg" />&nbsp;&nbsp;~&nbsp;&nbsp;<input id="fltReceivedDateEnd" />
            </div>
          </div>
          <div class="ui-helper-clearfix type">
            <div class="typeLabel">
              <span>Type :</span>
            </div>
            <div class="typeInput">
              <select id="fltType">
                <option value="">-</option>
                <option value="0">Complaint</option>
                <option value="1">Request</option>
                <option value="2">Other</option>
              </select>
            </div>
          </div>
          <div class="ui-helper-clearfix active">
            <div class="activeLabel">
              <span>Active :</span>
            </div>
            <div class="activeInput">
              <select id="fltActive">
                <option value="">-</option>
                <option value="0">Closed</option>
                <option value="1">Pending</option>
              </select>
            </div>
          </div>
        </div>
        <div class="filterRight">
          <div class="ui-helper-clearfix department">
            <div class="departmentLabel">
              <span>Department :</span>
            </div>
            <div class="departmentInput">
              <select id="fltDepartment">
              </select>
            </div>
          </div>
          <div class="ui-helper-clearfix hotel">
            <div class="hotelLabel">
              <span>Hotel :</span>
            </div>
            <div class="hotelInput">
              <select id="fltHotel">
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="ui-helper-clearfix filterButtons">
        <input type="button" value="Filter" onclick="getComplaintData()" />&nbsp;&nbsp;|&nbsp;&nbsp;<input type="button" value="Clear filter" />
      </div>
      <div class="Content">
        <br />
        <div class="resultArea">
        </div>
      </div>
      <div class="Footer">
        <tv1:Footer ID="tvfooter" runat="server" />
      </div>
    </div>
    <%--Message, Confirm Area--%>
    <div id="dialog-message" title="" style="display: none;">
      <p>
        <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span><span id="messages">Message</span>
      </p>
    </div>
    <div id="waitMessage" style="display: none;">
      <img title="" src="Images/Wait.gif" />
    </div>
    <%--Message, Confirm Area--%>
    <%--View / Edit Complaint--%>
    <div id="dialog-ViewEditComplaint" title='View / Edit Complaint'
      style="display: none; text-align: center;">
      <iframe id="ViewEditComplaint" height="100%" width="1000px" frameborder="0"
        style="clear: both; text-align: left;"></iframe>
    </div>
    <%--View / Edit Complaint--%>
  </form>
</body>
</html>
