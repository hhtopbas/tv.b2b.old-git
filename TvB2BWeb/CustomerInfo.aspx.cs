﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Services;
using TvBo;
using TvTools;

public partial class CustomerInfo : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        if (ci.Name.ToLower() == "lt-lt")
            ci.DateTimeFormat.ShortDatePattern = "yyyy.MM.dd";
        else if (ci.Name.ToLower() == "lv-lv")
            ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
    }

    public static string createResCustDiv(TvBo.ResCustRecord resCust, bool? OnlyView)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        if (UserData.Ci.Name.ToLower() == "lt-lt")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "yyyy.MM.dd";
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        bool onlyView = !OnlyView.HasValue || (OnlyView.HasValue && OnlyView.Value);
        bool _custEdit = true;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ||
            Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) ||
            Equals(UserData.CustomRegID, TvBo.Common.crID_Anex) ||
            Equals(UserData.CustomRegID, TvBo.Common.crID_FamilyTour) ||
            Equals(UserData.CustomRegID, TvBo.Common.crID_KidyTour)||
            Equals(UserData.CustomRegID, TvBo.Common.crID_Emerald)
            )
            _custEdit = false;
        string dateMaskA = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ');
        string dateSeperator = UserData.Ci.DateTimeFormat.DateSeparator[0].ToString();
        if (UserData.Ci.Name.ToLower() == "lt-lt")
            dateSeperator = ".";
        string[] dateMaskB = dateMaskA.Split(Convert.ToChar(dateSeperator));

        string _dateMask = string.Empty;
        string _dateFormat = string.Empty;
        string _dateFormatRegion = new TvBo.Common().getDateFormatRegion(UserData.Ci);
        foreach (string row in dateMaskB)
            if (!string.IsNullOrEmpty(row.ToString()))
                switch (row[0].ToString().ToLower())
                {
                    case "d":
                        _dateMask += "/99";
                        _dateFormat += "/dd";
                        break;
                    case "m":
                        _dateMask += "/99";
                        _dateFormat += "/MM";
                        break;
                    case "y":
                        _dateMask += "/9999";
                        _dateFormat += "/yyyy";
                        break;
                    default:
                        break;
                }
        _dateMask = _dateMask.Remove(0, 1);
        _dateFormat = _dateFormat.Remove(0, 1);

        bool passEdit = true;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ||
            Equals(UserData.CustomRegID, TvBo.Common.crID_SunFun) ||
            Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel)||
            Equals(UserData.CustomRegID, TvBo.Common.crID_Emerald))
            passEdit = true;
        else
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && (ResData.ResMain.BegDate.Value - DateTime.Today).TotalDays < 8)
            passEdit = false;

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
            passEdit = false;

        if (OnlyView.HasValue && OnlyView.Value)
        {
            _custEdit = false;
            passEdit = false;
        }

        StringBuilder sb = new StringBuilder();
        sb.AppendFormat("<input type=\"hidden\" id=\"CustNo\" value=\"{0}\" /><input type=\"hidden\" id=\"hfLCID\" value=\"{1}\" />", resCust.CustNo, UserData.Ci.Name);
        sb.AppendFormat("<div class=\"divResNo\"><span id=\"lblResNo\">{0}</span></div>", resCust.ResNo);
        string customerStr = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ContactInfo").ToString(), resCust.Surname + " " + resCust.Name);
        sb.AppendFormat("<div class=\"divCustInfo\"><span id=\"txtCustInfo\">{0}</span></div>", customerStr);
        sb.AppendFormat("<div class=\"divDateInfo\">{0}</div>",
            "* " + HttpContext.GetGlobalResourceObject("CustomerInfo", "lblAllDateFormat").ToString() + " (" + _dateFormatRegion + ")");
        sb.Append("<div class=\"divTitleNameSurname\">");
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) || Equals(UserData.CustomRegID, TvBo.Common.crID_Kenba))
        {
            if (resCust.Title > 5)
                sb.AppendFormat("<div class=\"divTitle\">{0}<br /><input id=\"edtTitle\" type=\"text\" class=\"titleInput\" value=\"{1}\" {3} /><input id=\"edtTitleCode\" type=\"hidden\" value=\"{2}\" /></div>",
                                HttpContext.GetGlobalResourceObject("CustomerInfo", "lblTitle").ToString(),
                                resCust.TitleStr,
                                resCust.Title,
                                !onlyView && _custEdit ? "" : "disabled='disabled'");
            else
            {
                string titleStr = "<select id=\"edtTitleCode\">";
                if (onlyView)
                    titleStr = "<select id=\"edtTitleCode\" disabled='disabled' >";
                List<TitleRecord> title = new TvBo.Common().getAdultTypes();
                foreach (TitleRecord trow in title)
                    titleStr += string.Format("<option value=\"{0}\" {1}>{2}</option>", trow.TitleNo, Equals(trow.TitleNo.ToString(), resCust.Title.ToString()) ? "selected=\"selected\"" : "", trow.Code);

                titleStr += "</select>";
                sb.AppendFormat("<div class=\"divTitle\">{0}<br />{1}</div>",
                                HttpContext.GetGlobalResourceObject("CustomerInfo", "lblTitle").ToString(),
                                titleStr);
            }
        }
        else
            sb.AppendFormat("<div class=\"divTitle\">{0}<br /><input id=\"edtTitle\" type=\"text\" class=\"titleInput\" value=\"{1}\" {3} /><input id=\"edtTitleCode\" type=\"hidden\" value=\"{2}\" /></div>",
                                HttpContext.GetGlobalResourceObject("CustomerInfo", "lblTitle").ToString(),
                                resCust.TitleStr,
                                resCust.Title,
                                !onlyView && _custEdit ? "" : "disabled='disabled'");

        sb.AppendFormat("<div class=\"divSurname\">{0}<br /><input id=\"edtSurname\" type=\"text\" class=\"surnameInput\" maxlength=\"30\" value=\"{1}\" {2} {3} onkeypress=\"return isNonUniCodeChar(event);\" /></div>",
                            HttpContext.GetGlobalResourceObject("CustomerInfo", "lblSurname").ToString(),
                            resCust.Surname,
                            "style=\"" + new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.surnameWrittingRules) + "\"",
                            !onlyView && _custEdit ? "" : "disabled='disabled'");
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Emerald) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Doris) || string.Equals(UserData.CustomRegID, TvBo.Common.ctID_BelPreGo))
            sb.AppendFormat("<div class=\"divSurname\">{0}<br /><input id=\"edtSurnameL\" type=\"text\" class=\"surnameInput\" maxlength=\"30\" value=\"{1}\" {2} {3} /></div>",
                                HttpContext.GetGlobalResourceObject("CustomerInfo", "lblSurname").ToString() + "("+ HttpContext.GetGlobalResourceObject("CustomerInfo", "lblLocal").ToString() + ")",
                                resCust.SurnameL,
                                "",
                                "");

        sb.AppendFormat("<div class=\"divName\">{0}<br /><input id=\"edtName\" type=\"text\" class=\"nameInput\" maxlength=\"30\" value=\"{1}\" {2} {3} onkeypress=\"return isNonUniCodeChar(event);\" /></div>",
                            HttpContext.GetGlobalResourceObject("CustomerInfo", "lblName").ToString(),
                            resCust.Name,
                            "style=\"" + new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.nameWrittingRules) + "\"",
                            !onlyView && _custEdit ? "" : "disabled='disabled'");
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Emerald) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Doris) || string.Equals(UserData.CustomRegID, TvBo.Common.ctID_BelPreGo))
            sb.AppendFormat("<div class=\"divName\">{0}<br /><input id=\"edtNameL\" type=\"text\" class=\"nameInput\" maxlength=\"30\" value=\"{1}\" {2} {3} /></div>",
                            HttpContext.GetGlobalResourceObject("CustomerInfo", "lblName").ToString() + "(" + HttpContext.GetGlobalResourceObject("CustomerInfo", "lblLocal").ToString() + ")",
                            resCust.NameL,
                            "",
                            "");

        sb.Append("</div>");
        sb.Append("<div class=\"divBirthDayIDNoPassSeriePassNo\">");
        string birthDate = resCust.Birtday.HasValue ? resCust.Birtday.Value.ToLocalTime().ToString(_dateFormat) : "";
        sb.AppendFormat("<div class=\"divBirthday\">{0}<br /><input id=\"edtBirtday\" type=\"text\" class=\"birthdayInput formatDate\" maxlength=\"10\" value=\"{1}\" {2} /></div>",
                            HttpContext.GetGlobalResourceObject("CustomerInfo", "lblBirtday").ToString(),
                            birthDate,
                            ((!onlyView && (_custEdit || string.IsNullOrEmpty(birthDate))) ? "" : "disabled='disabled'"));//2013-10-09 Vidas kendi istedi kaldırılmasını
        bool? showPIN = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "showPIN"));
        if (UserData.TvParams.TvParamReser.NeedPIN.HasValue && UserData.TvParams.TvParamReser.NeedPIN.Value)
            showPIN = true;

        sb.AppendFormat("<div class=\"divIdNo {2}\">{0}<br /><input id=\"edtIDNo\" type=\"text\" class=\"idNoInput\" maxlength=\"20\" value=\"{1}\" {3} /></div>",
                            HttpContext.GetGlobalResourceObject("CustomerInfo", "lblIDNo").ToString(),
                            resCust.IDNo,
                            !showPIN.HasValue || showPIN.Value ? "" : "unVisible",
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Emerald) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_SunFun) || (!onlyView && (_custEdit || string.IsNullOrEmpty(resCust.IDNo))) ? "" : "disabled='disabled'");
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_SunFun))
            passEdit = true;
        else if (string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt))
            passEdit = passEdit && onlyView == false;
        else
            passEdit = passEdit && (!onlyView && ((_custEdit || string.IsNullOrEmpty(resCust.PassSerie))));

        bool? showPassportInfo = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ShowPassportInfo"));
        if (string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt))
            sb.AppendFormat("<div class=\"divPassSerie {6}\">{3} <br/> <select id=\"edtPassSerie\" style=\"width:40px;\">" +
                "<option></option>" +
                "<option {0}>P</option>" +
                "<option {1}>ID</option>" +
                "<option {2}>OT</option>" +
                "</select>-<input id=\"edtPassNo\" type=\"text\" class=\"passNoInput\" maxlength=\"10\" value=\"{5}\" {7} onkeypress=\"return isNonUniCodeChar(event);\" /></div>",
                resCust.PassSerie.Equals("P") ? "selected" : "",
                resCust.PassSerie.Equals("ID") ? "selected" : "",
                resCust.PassSerie.Equals("OT") ? "selected" : "",
                 HttpContext.GetGlobalResourceObject("CustomerInfo", "lblPassSerieNo").ToString(),
                            resCust.PassSerie,
                            resCust.PassNo,
                            !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "unVisible",
                            passEdit ? "" : "disabled='disabled'",
                            passEdit ? "" : "disabled='disabled'");
        else
            sb.AppendFormat("<div class=\"divPassSerie {3}\">{0}<br /><input id=\"edtPassSerie\" type=\"text\" class=\"passSerieInput\" maxlength=\"5\" value=\"{1}\" {5} onkeypress=\"return isNonUniCodeChar(event);\" />-<input id=\"edtPassNo\" type=\"text\" class=\"passNoInput\" maxlength=\"10\" value=\"{2}\" {4} onkeypress=\"return isNonUniCodeChar(event);\" /></div>",
                                HttpContext.GetGlobalResourceObject("CustomerInfo", "lblPassSerieNo").ToString(),
                                resCust.PassSerie,
                                resCust.PassNo,
                                !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "unVisible",
                                passEdit ? "" : "disabled='disabled'",
                                passEdit ? "" : "disabled='disabled'");
        sb.Append("</div>");
        sb.AppendFormat("<div class=\"divPassIssueDateExpDateGiven {0}\">", !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "unVisible");
        string passIssueDate = resCust.PassIssueDate.HasValue ? resCust.PassIssueDate.Value.ToLocalTime().ToString(_dateFormat) : "";
        sb.AppendFormat("<div class=\"divPassIssueDate\">{0}<br /><input id=\"edtPassIssueDate\" type=\"text\" class=\"passIssueDateInput formatDate\" value=\"{1}\" {2} /></div>",
                            HttpContext.GetGlobalResourceObject("CustomerInfo", "lblPassIssueDate").ToString(),
                            passIssueDate,
                            passEdit ? "" : "disabled='disabled'");
        string passExpDate = resCust.PassExpDate.HasValue ? resCust.PassExpDate.Value.ToLocalTime().ToString(_dateFormat) : "";
        sb.AppendFormat("<div class=\"divPassExpDate\">{0}<br /><input id=\"edtPassExpDate\" type=\"text\" class=\"passExpDateInput formatDate\" value=\"{1}\" {2} /></div>",
                            HttpContext.GetGlobalResourceObject("CustomerInfo", "lblPassExpDate").ToString(),
                            passExpDate,
                            passEdit ? "" : "disabled='disabled'");
        if (string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt))
        {
            List<Nationality> nationalityList = CacheObjects.getNationality(UserData);
            string nationList = "";
            nationList = string.Format("<option value=\"\">{0}</option>", HttpContext.GetGlobalResourceObject("LibraryResource", "ComboSelect").ToString());
            foreach (Nationality nRow in nationalityList.OrderBy(o => o.OrderNo).ThenBy(t => t.Name))
                    nationList += string.Format("<option value=\"{2}\" {1}>{2}</option>", nRow.Code3, Equals(nRow.Name, resCust.PassGiven) || Equals(nRow.Code3, resCust.PassGiven) ? "selected=\"selected\"" : "", nRow.Name);

            sb.AppendFormat("<div class=\"divPassGiven\">{0}<br /><select id=\"edtPassGiven\" class=\"passGivenInput\"  {2}>{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("CustomerInfo", "lblPassGiven").ToString(),
                            nationList,
                            passEdit ? "" : "disabled='disabled'");
        }
            
        else
            sb.AppendFormat("<div class=\"divPassGiven\">{0}<br /><input id=\"edtPassGiven\" type=\"text\" class=\"passGivenInput\" maxlength=\"30\" value=\"{1}\" {2} onkeypress=\"return isNonUniCodeChar(event);\" /></div>",
                            HttpContext.GetGlobalResourceObject("CustomerInfo", "lblPassGiven").ToString(),
                            resCust.PassGiven,
                            passEdit ? "" : "disabled='disabled'");
        sb.AppendFormat("</div><br />{0}<br />", "");
        return sb.ToString().Replace('"', '!');
    }

    public static string createResCustInfoDivs(TvBo.ResDataRecord ResData, int? _CustNo, bool? OnlyView)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        bool _custEdit = true;
        bool onlyView = !OnlyView.HasValue || (OnlyView.HasValue && OnlyView.Value);
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) || Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
            _custEdit = false;

        if (OnlyView.HasValue && OnlyView.Value)
        {
            _custEdit = false;
        }


        List<IntCountryListRecord> countryList = new TvBo.Common().getCountryList(UserData, ref errorMsg);
        countryList = countryList.OrderBy(o => o.CountryNameL).ThenBy(t => t.CountryName).ToList();
        List<Location> LocationList = CacheObjects.getLocationList(UserData.Market);
        Location country = LocationList.Find(f => f.RecID == UserData.Country);

        List<TitleRecord> adultTypes = new TvBo.Common().getAdultTypes();

        List<ResCustInfoRecord> _ResCustInfo = ResData.ResCustInfo;
        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == _CustNo.Value);
        ResCustInfoRecord resCustInfo = new ResCustInfoRecord();
        if (ResData.ResCustInfo.Count > 0 && _ResCustInfo.Find(f => f.CustNo == resCust.CustNo) != null)
        {
            resCustInfo = _ResCustInfo.Find(f => f.CustNo == resCust.CustNo);
            resCustInfo.MobTel = !string.IsNullOrEmpty(resCust.Phone) ? strFunc.Trim(resCust.Phone, ' ') : strFunc.Trim(resCustInfo.MobTel, ' ');
        }
        else
        {
            resCustInfo.CustNo = resCust.CustNo;
            resCustInfo.CTitle = resCust.Title;
            resCustInfo.RecID = _ResCustInfo.Count > 0 ? _ResCustInfo.LastOrDefault().RecID + 1 : 1;
            resCustInfo.RecordID = resCustInfo.RecID;
            resCustInfo.MobTel = strFunc.Trim(resCust.Phone, ' ');
            IntCountryListRecord _country = new TvBo.Common().getCountryIntCode(UserData.Market, UserData.Country, ref errorMsg);
            resCustInfo.AddrHomeCountryCode = _country != null ? _country.IntCode : string.Empty;

            resCustInfo.MemTable = false;

            _ResCustInfo.Add(resCustInfo);
        }
        string CustomerName = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ContactInfo").ToString(), resCust.Name + " " + resCust.Surname);
        string divCustomers = string.Format("<span style=\"font-size: 11pt;\"><b>{0}</b></span>", CustomerName);

        StringBuilder divContact = new StringBuilder();
        StringBuilder divHomeAddr = new StringBuilder();
        StringBuilder divWorkAddr = new StringBuilder();
        StringBuilder divBankInfo = new StringBuilder();

        string _phoneMask = string.Empty;
        string _mobphoneMask = string.Empty;
        string countryListStr = string.Empty;
        string countryWorkListStr = string.Empty;
        string selectedCountryCode = string.Empty;
        string selectedWorkCountryCode = string.Empty;
        string countryHome = string.Empty;
        string countryWork = string.Empty;

        countryListStr += string.Format("<option value='{0}' {2}>{1}</option>",
                                    "",
                                    " - ",
                                    "");

        countryWorkListStr += string.Format("<option value='{0}' {2}>{1}</option>",
                                    "",
                                    " - ",
                                    "");

        foreach (IntCountryListRecord row in countryList)
        {
            string selectStr = string.Empty;
            string selectWorkStr = string.Empty;
            if (Equals(resCustInfo.AddrHomeCountry, row.CountryNameL))
            {
                selectStr = "selected=\"selected\"";
                selectedCountryCode = row.CountryPhoneCode;
                countryHome = row.IntCode;
                _mobphoneMask = row.PhoneMaskMob.Replace('#', '9');
                _phoneMask = row.PhoneMask.Replace('#', '9');
            }
            else if (Equals(row.IntCode, country != null ? country.CountryCode.ToString() : ""))
            {
                selectStr = "selected=\"selected\"";
                selectedCountryCode = row.CountryPhoneCode;
                countryHome = row.IntCode;
                _mobphoneMask = row.PhoneMaskMob.Replace('#', '9');
                _phoneMask = row.PhoneMask.Replace('#', '9');
            }

            if (Equals(resCustInfo.AddrWorkCountry, row.CountryNameL))
            {
                selectWorkStr = "selected=\"selected\"";
                selectedWorkCountryCode = row.CountryPhoneCode;
                countryWork = row.IntCode;
                _mobphoneMask = row.PhoneMaskMob.Replace('#', '9');
                _phoneMask = row.PhoneMask.Replace('#', '9');
            }
            else if (Equals(row.IntCode, country != null ? country.CountryCode.ToString() : ""))
            {
                selectWorkStr = "selected=\"selected\"";
                selectedWorkCountryCode = row.CountryPhoneCode;
                countryWork = row.IntCode;
                _mobphoneMask = row.PhoneMaskMob.Replace('#', '9');
                _phoneMask = row.PhoneMask.Replace('#', '9');
            }

            countryListStr += string.Format("<option value='{0}' {2}>{1}</option>",
                                    row.CountryNameL,
                                    row.CountryNameL,
                                    selectStr);

            countryWorkListStr += string.Format("<option value='{0}' {2}>{1}</option>",
                                    row.CountryNameL,
                                    row.CountryNameL,
                                    selectWorkStr);
        }

        if (string.IsNullOrEmpty(_phoneMask) && string.IsNullOrEmpty(_mobphoneMask))
            if (UserData.PhoneMask != null)
            {
                _phoneMask = UserData.PhoneMask.MobilPhoneMask != null ? UserData.PhoneMask.PhoneMask.Replace('#', '9') : "";
                _mobphoneMask = UserData.PhoneMask.MobilPhoneMask != null ? UserData.PhoneMask.MobilPhoneMask.Replace('#', '9') : "";
            }

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && string.Equals(UserData.Market, "FINMAR"))
        {
            _phoneMask = string.Empty;
            _mobphoneMask = string.Empty;
        }

        divContact.AppendFormat("<input id=\"phone_Mask\" type=\"hidden\" value='{0}' />", _phoneMask);
        divContact.AppendFormat("<input id=\"mob_phone_Mask\" type=\"hidden\" value='{0}' />", _mobphoneMask);
        divContact.AppendFormat("<input id=\"countryList\" type=\"hidden\" value='{0}' />", Newtonsoft.Json.JsonConvert.SerializeObject(countryList).Replace('#', '9'));
        #region Contact
        divContact.AppendFormat("<input type=\"hidden\" id=\"CustNo\" value=\"{0}\" /><input type=\"hidden\" id=\"hfLCID\" value=\"{1}\" />", resCust.CustNo, UserData.Ci.Name);
        divContact.Append("<table><tr>");
        divContact.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divContactTitle"));
        string _adultTypes = string.Empty;
        foreach (TitleRecord row in adultTypes)
            _adultTypes += string.Format("<option value=\"{0}\" {1}>{2}</option>", row.TitleNo, row.TitleNo == resCustInfo.CTitle ? "selected=\"selected\"" : "", row.Code);
        divContact.AppendFormat("<td><select id=\"CTitle\" class=\"combo\" {1} >{0}</select></td>", _adultTypes, onlyView ? "disabled='disabled'" : "");
        divContact.Append("</tr><tr>");
        divContact.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divContactName"));
        divContact.AppendFormat("<td><input id=\"CName\" type=\"text\" maxlength=\"30\" class=\"pix250\" value=\"{0}\" {1} onkeypress=\"return isNonUniCodeChar(event);\" {2} /></td>",
            resCustInfo.CName,
            "style=\"" + new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.nameWrittingRules) + "\"",
            onlyView ? "disabled='disabled'" : "");
        divContact.Append("</tr><tr>");
        divContact.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divContactSurname"));
        divContact.AppendFormat("<td><input id=\"CSurName\" type=\"text\" maxlength=\"30\" class=\"pix250\" value=\"{0}\" {1} onkeypress=\"return isNonUniCodeChar(event);\" {2} /></td>",
            resCustInfo.CSurName,
            "style=\"" + new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.surnameWrittingRules) + "\"",
            onlyView ? "disabled='disabled'" : "");
        //divContact.Append("</tr><tr>");
        //divContact.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
        //        HttpContext.GetGlobalResourceObject("CustomerAddress", "divContactMobile"));
        //divContact.AppendFormat("<td><input id=\"MobTel\" type=\"text\" maxlength=\"30\" class=\"pix250\" value=\"{0}\" /></td>", resCustInfo.MobTel);
        divContact.Append("</tr><tr>");
        divContact.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divContactContactAddress"));
        string _contactAddr = string.Empty;
        _contactAddr += string.Format("<option value=\"{0}\" {1}>{2}</option>", "H", Equals(resCustInfo.ContactAddr, "H") ? "selected=\"selected\"" : "", HttpContext.GetGlobalResourceObject("LibraryResource", "cbContHome").ToString());
        _contactAddr += string.Format("<option value=\"{0}\" {1}>{2}</option>", "W", Equals(resCustInfo.ContactAddr, "W") ? "selected=\"selected\"" : "", HttpContext.GetGlobalResourceObject("LibraryResource", "cbContWork").ToString());
        divContact.AppendFormat("<td><select id=\"ContactAddr\" class=\"combo\" {1} >{0}</select></td>",
                _contactAddr,
                onlyView ? "disabled='disabled'" : "");
        divContact.Append("</tr><tr>");
        divContact.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divContactInvoiceAddress"));
        string _invoiceAddr = string.Empty;
        _invoiceAddr += string.Format("<option value=\"{0}\" {1}>{2}</option>", "H", Equals(resCustInfo.InvoiceAddr, "H") ? "selected=\"selected\"" : "", HttpContext.GetGlobalResourceObject("LibraryResource", "cbInvHome").ToString());
        _invoiceAddr += string.Format("<option value=\"{0}\" {1}>{2}</option>", "W", Equals(resCustInfo.InvoiceAddr, "W") ? "selected=\"selected\"" : "", HttpContext.GetGlobalResourceObject("LibraryResource", "cbInvWork").ToString());
        divContact.AppendFormat("<td><select id=\"InvoiceAddr\" class=\"combo\" {1} >{0}</select></td>",
                _invoiceAddr,
                onlyView ? "disabled='disabled'" : "");
        divContact.Append("</tr></table>");
        #endregion

        #region Home Address
        divHomeAddr.Append("<table><tr>");
        divHomeAddr.AppendFormat("<td colspan=\"2\" align=\"center\"><strong>{0}</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divHomeAddressLabel"));
        divHomeAddr.Append("</tr><tr>");
        divHomeAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblAddress"));
        divHomeAddr.AppendFormat("<td><textarea id=\"AddrHome\" cols=\"33\" name=\"AddrHome\" rows=\"3\" {1}>{0}</textarea></td>",
                resCustInfo.AddrHome,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.AddrHome)) ? "" : "disabled='disabled'");

        divHomeAddr.Append("</tr><tr>");
        divHomeAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblZipCode"));
        divHomeAddr.AppendFormat("<td><input id=\"AddrHomeZip\" type=\"text\" maxlength=\"10\" style=\"width: 80px;\" value=\"{0}\" {1} /></td>",
                resCustInfo.AddrHomeZip,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.AddrHomeZip)) ? "" : "disabled='disabled'");

        divHomeAddr.Append("</tr><tr>");
        divHomeAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblCityCountry"));
        divHomeAddr.AppendFormat("<td><input id=\"AddrHomeCity\" type=\"text\" maxlength=\"30\" style=\"width: 130px;\" value=\"{0}\" {1} />&nbsp;/&nbsp;",
                resCustInfo.AddrHomeCity,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.AddrHomeCity)) ? "" : "disabled='disabled'");
        divHomeAddr.AppendFormat("<input id='AddrHomeCountryCode' type='hidden' value='{1}' /><select id=\"AddrHomeCountry\" onchange=\"homeCountryChanged();\" {2}>{0}</select></td>",
                countryListStr,
                countryHome,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.AddrHomeCountry)) ? "" : "disabled='disabled'");

        divHomeAddr.Append("</tr><tr>");

        divHomeAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divContactMobile"));
        divHomeAddr.AppendFormat("<td>(<span id=\"MobTelCountryCode\">{1}</span> )<input id=\"MobTel\" type=\"text\" maxlength=\"15\" style=\"width: 100px;\" value=\"{0}\" onkeypress=\"return isPhoneChar(event);\" {2} /></td>",
                !string.IsNullOrEmpty(resCustInfo.MobTel) ? resCustInfo.MobTel.Replace("+" + selectedCountryCode, "") : resCustInfo.MobTel,
                "+" + selectedCountryCode,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.MobTel)) ? "" : "disabled='disabled'");
        divHomeAddr.Append("</tr><tr>");
        divHomeAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td><td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblTelFax"));
        divHomeAddr.AppendFormat("(<span id=\"AddrHomeTelCountryCode\">{1}</span> )<input id=\"AddrHomeTel\" type=\"text\" maxlength=\"15\" style=\"width: 100px;\" value=\"{0}\" onkeypress=\"return isPhoneChar(event);\" {2} />&nbsp;/&nbsp;",
                !string.IsNullOrEmpty(resCustInfo.AddrHomeTel) ? resCustInfo.AddrHomeTel.Replace("+" + selectedCountryCode, "") : resCustInfo.AddrHomeTel,
                "+" + selectedCountryCode,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.AddrHomeTel)) ? "" : "disabled='disabled'");
        divHomeAddr.AppendFormat("(<span id=\"AddrHomeFaxCountryCode\">{1}</span> )<input id=\"AddrHomeFax\" type=\"text\" maxlength=\"15\" style=\"width: 100px;\" value=\"{0}\" onkeypress=\"return isPhoneChar(event);\" {2} /></td>",
                !string.IsNullOrEmpty(resCustInfo.AddrHomeFax) ? resCustInfo.AddrHomeFax.Replace("+" + selectedCountryCode, "") : resCustInfo.AddrHomeFax,
                "+" + selectedCountryCode,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.AddrHomeFax)) ? "" : "disabled='disabled'");

        divHomeAddr.Append("</tr><tr>");
        divHomeAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblEMail"));
        divHomeAddr.AppendFormat("<td><input id=\"AddrHomeEmail\" type=\"text\" maxlength=\"100\" style=\"width: 250px;\" value=\"{0}\" {1} /></td>",
                resCustInfo.AddrHomeEmail,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.AddrHomeEmail)) ? "" : "disabled='disabled'");
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            divHomeAddr.Append("</tr><tr style=\"display: none;\">");
        else
            divHomeAddr.Append("</tr><tr>");
        divHomeAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblTaxOffice"));
        divHomeAddr.AppendFormat("<td><input id=\"HomeTaxOffice\" type=\"text\" maxlength=\"100\" style=\"width: 200px;\" value=\"{0}\" {1} /></td>",
                resCustInfo.HomeTaxOffice,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.HomeTaxOffice)) ? "" : "disabled='disabled'");
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            divHomeAddr.Append("</tr><tr style=\"display: none;\">");
        else
            divHomeAddr.Append("</tr><tr>");
        divHomeAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblTaxAccountNo"));
        divHomeAddr.AppendFormat("<td><input id=\"HomeTaxAccNo\" type=\"text\" maxlength=\"20\" style=\"width: 150px;\" value=\"{0}\" {1} /></td>",
                resCustInfo.HomeTaxAccNo,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.HomeTaxAccNo)) ? "" : "disabled='disabled'");
        divHomeAddr.Append("</tr></table>");
        #endregion

        #region Work Address
        divWorkAddr.AppendFormat("<table><tr><td colspan=\"2\" align=\"center\"><strong>{0}</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divWorkAddressLabel"));
        divWorkAddr.Append("</tr><tr>");
        divWorkAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblFirmName"));
        divWorkAddr.AppendFormat("<td><input id=\"WorkFirmName\" type=\"text\" maxlength=\"100\" style=\"width: 300px;\" value=\"{0}\" {1} /></td>",
                resCustInfo.WorkFirmName,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.WorkFirmName)) ? "" : "disabled='disabled'");
        divWorkAddr.Append("</tr><tr>");
        divWorkAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblAddress"));
        divWorkAddr.AppendFormat("<td><textarea id=\"AddrWork\" cols=\"33\" name=\"AddrHome\" rows=\"3\" {1} >{0}</textarea></td>",
                resCustInfo.AddrWork,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.AddrWork)) ? "" : "disabled='disabled'");
        divWorkAddr.Append("</tr><tr>");
        divWorkAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblZipCode"));
        divWorkAddr.AppendFormat("<td><input id=\"AddrWorkZip\" type=\"text\" maxlength=\"10\" style=\"width: 80px;\" value=\"{0}\" {1} /></td>",
                resCustInfo.AddrWorkZip,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.AddrWorkZip)) ? "" : "disabled='disabled'");
        divWorkAddr.Append("</tr><tr>");
        divWorkAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblCityCountry"));
        divWorkAddr.AppendFormat("<td><input id=\"AddrWorkCity\" type=\"text\" maxlength=\"30\" style=\"width: 130px;\" value=\"{0}\" {1} />&nbsp;/&nbsp;",
                resCustInfo.AddrWorkCity,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.AddrWorkCity)) ? "" : "disabled='disabled'");
        divWorkAddr.AppendFormat("<input id='AddrWorkCountryCode' type='hidden' value='{1}' /><select id=\"AddrWorkCountry\" onchange=\"workCountryChanged();\" {2}>{0}</select></td>",
                countryWorkListStr,
                countryWork,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.AddrWorkCountry)) ? "" : "disabled='disabled'");
        divWorkAddr.Append("</tr><tr>");
        divWorkAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>", HttpContext.GetGlobalResourceObject("CustomerAddress", "lblTelFax"));
        divWorkAddr.AppendFormat("<td>(<span id=\"AddrWorkTelCountryCode\">{1}</span> )<input id=\"AddrWorkTel\" type=\"text\" maxlength=\"30\" style=\"width: 100px;\" value=\"{0}\" onkeypress=\"return isPhoneChar(event);\" {2} />&nbsp;/&nbsp;",
                !string.IsNullOrEmpty(resCustInfo.AddrWorkTel) ? resCustInfo.AddrWorkTel.Replace("+" + selectedCountryCode, "") : resCustInfo.AddrWorkTel,
                "+" + selectedCountryCode,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.AddrWorkTel)) ? "" : "disabled='disabled'");
        divWorkAddr.AppendFormat("(<span id=\"AddrWorkFaxCountryCode\">{1}</span> )<input id=\"AddrWorkFax\" type=\"text\" maxlength=\"30\" style=\"width: 100px;\" value=\"{0}\" onkeypress=\"return isPhoneChar(event);\" {2} /></td>",
                !string.IsNullOrEmpty(resCustInfo.AddrWorkFax) ? resCustInfo.AddrWorkFax.Replace("+" + selectedCountryCode, "") : resCustInfo.AddrWorkFax,
                "+" + selectedCountryCode,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.AddrWorkFax)) ? "" : "disabled='disabled'");
        divWorkAddr.Append("</tr><tr>");
        divWorkAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblEMail"));
        divWorkAddr.AppendFormat("<td><input id=\"AddrWorkEMail\" type=\"text\" maxlength=\"100\" style=\"width: 250px;\" value=\"{0}\" {1} /></td>",
                resCustInfo.AddrWorkEMail,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.AddrWorkEMail)) ? "" : "disabled='disabled'");
        divWorkAddr.Append("</tr><tr>");
        divWorkAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblTaxOffice"));
        divWorkAddr.AppendFormat("<td><input id=\"WorkTaxOffice\" type=\"text\" maxlength=\"30\" style=\"width: 200px;\" value=\"{0}\" {1} /></td>",
                resCustInfo.WorkTaxOffice,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.WorkTaxOffice)) ? "" : "disabled='disabled'");
        divWorkAddr.Append("</tr><tr>");
        divWorkAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblTaxAccountNo"));
        divWorkAddr.AppendFormat("<td><input id=\"WorkTaxAccNo\" type=\"text\" maxlength=\"20\" style=\"width: 150px;\" value=\"{0}\" {1} /></td>",
                resCustInfo.WorkTaxAccNo,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.WorkTaxAccNo)) ? "" : "disabled='disabled'");
        divWorkAddr.Append("</tr></table>");
        #endregion

        #region Bank
        // HttpContext.GetGlobalResourceObject("CustomerAddress", )
        divBankInfo.AppendFormat("<table><tr><td colspan=\"2\" align=\"center\"><strong>{0}</strong></td></tr><tr>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divBankInfo"));
        divBankInfo.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblBank"));
        divBankInfo.AppendFormat("<td><input id=\"Bank\" type=\"text\" maxlength=\"50\" style=\"width: 250px;\" value=\"{0}\" {1} /></td>",
                resCustInfo.Bank,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.Bank)) ? "" : "disabled='disabled'");
        divBankInfo.Append("</tr><tr>");
        divBankInfo.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblAccount"));
        divBankInfo.AppendFormat("<td><input id=\"BankAccNo\" type=\"text\" maxlength=\"20\" style=\"width: 150px;\" value=\"{0}\" {1} /></td>",
                resCustInfo.BankAccNo,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.BankAccNo)) ? "" : "disabled='disabled'");
        divBankInfo.Append("</tr><tr>");
        divBankInfo.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblIBAN"));
        divBankInfo.AppendFormat("<td><input id=\"BankIBAN\" type=\"text\" maxlength=\"50\" style=\"width: 350px;\" value=\"{0}\" {1} /></td>",
                resCustInfo.BankIBAN,
                !onlyView && (_custEdit || string.IsNullOrEmpty(resCustInfo.BankIBAN)) ? "" : "disabled='disabled'");
        divBankInfo.Append("</tr></table>");
        #endregion

        string retVal = string.Empty;
        retVal += "\"Customers\":\"" + divCustomers.Replace('"', '#') + "\"";
        retVal += ",\"Contact\":\"" + divContact.ToString().Replace('"', '#') + "\"";
        retVal += ",\"HomeAddr\":\"" + divHomeAddr.ToString().Replace('"', '#') + "\"";
        retVal += ",\"WorkAddr\":\"" + divWorkAddr.ToString().Replace('"', '#') + "\"";
        if (!Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            retVal += ",\"Bank\":\"" + divBankInfo.ToString().Replace('"', '#') + "\"";
        else
            retVal += ",\"Bank\":\"\"";

        return retVal.Replace('"', '!').Replace('\'', '~').Replace("\n", " ");
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData(int? custNo, bool? OnlyView)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        if (UserData.Ci.Name.ToLower() == "lt-lt")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "yyyy.MM.dd";
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        string errorMsg = "";
        //var hi = new Hotels().getHotelDetail(UserData, "MAGNOL", ref errorMsg);
        //new Hotels().
        string dateMaskA = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ');
        string dateSeperator = UserData.Ci.DateTimeFormat.DateSeparator[0].ToString();
        if (UserData.Ci.Name.ToLower() == "lt-lt")
            dateSeperator = ".";
        string[] dateMaskB = dateMaskA.Split(Convert.ToChar(dateSeperator));
        string _dateMask = string.Empty;
        string _dateFormat = string.Empty;
        foreach (string row in dateMaskB)
            if (!string.IsNullOrEmpty(row.ToString()))
                switch (row[0].ToString().ToLower())
                {
                    case "d":
                        _dateMask += "/99";
                        _dateFormat += "/dd";
                        break;
                    case "m":
                        _dateMask += "/99";
                        _dateFormat += "/MM";
                        break;
                    case "y":
                        _dateMask += "/9999";
                        _dateFormat += "/yyyy";
                        break;
                    default:
                        break;
                }
        _dateMask = _dateMask.Remove(0, 1);
        _dateFormat = _dateFormat.Remove(0, 1);

        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == custNo);
        ResCustInfoRecord resCustInfo = ResData.ResCustInfo.Find(f => f.CustNo == custNo);
        if (resCustInfo == null)
        {
            resCustInfo = new ResCustInfoRecord();
            resCustInfo.CustNo = resCust.CustNo;
            resCustInfo.MemTable = false;
            resCustInfo.RecID = ResData.ResCustInfo.Count > 0 ? ResData.ResCustInfo.LastOrDefault().RecID + 1 : 1;
            resCustInfo.RecordID = resCustInfo.RecID;
        }
        string retVal = "{\"ResCust\":\"" + createResCustDiv(resCust, OnlyView) + "\",\"ResCustInfo\":\"" + createResCustInfoDivs(ResData, custNo, OnlyView) + "\"";
        retVal += ",\"DateFormat\":\"" + _dateFormat + "\"";
        retVal += ",\"DateMask\":\"" + _dateMask + "\"}";

        return retVal;
    }

}
