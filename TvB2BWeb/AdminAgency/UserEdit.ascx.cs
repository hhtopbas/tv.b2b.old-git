﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using TvBo;
using TvTools;
using System.Globalization;
using System.Threading;

public partial class AgencyAdmin_UserEdit : System.Web.UI.UserControl
{
    private string errorMsg = string.Empty;
    AdmAgencyUserRecord oDataSource;
    public AdmAgencyUserRecord ODataSource
    {
        get { return getDataSource(); }
        set { setDataSource(value); oDataSource = value; }
    }

    protected override void OnDataBinding(EventArgs e)
    {
        base.OnDataBinding(e);
    }

    protected void setDataSource(AdmAgencyUserRecord value)
    {
        txtRecID.Value = value.RecID.ToString();
        txtAgencyID.Value = value.Agency;
        txtUserID.Value = value.Code;
        txtCode.Text = value.Code;
        if (!string.IsNullOrEmpty(value.Code))
            txtCode.ReadOnly = true;
        else txtCode.ReadOnly = false;

        txtName.Text = value.Name;
        cbStatus.Checked = value.Status;
        txtPIN.Text = value.PIN;
        txtPhone.Text = value.Phone;
        txtMobile.Text = value.Mobile;
        txtEMail.Text = value.EMail;
        txtHAddress.Text = value.HAddress;
        cbShowAllRes.Checked = value.ShowAllRes;
        txtPassword.Text = value.Pass;
        if (Conversion.getDateTimeOrNull(value.ExpDate).HasValue)
            ppcExpDate.SetDateValue(value.ExpDate.Value);
        else txtExpDate.Text = "";
        //txtExpedientID.Text = value.ExpedientID;
    }

    protected AdmAgencyUserRecord getDataSource()
    {
        AdmAgencyUserRecord rec = new AdmAgencyUserRecord();
        if (oDataSource != null)
        {
            rec = oDataSource;

            if (!string.IsNullOrEmpty(txtRecID.Value)) rec.RecID = Convert.ToInt32(txtRecID.Value);
            if (!string.IsNullOrEmpty(txtAgencyID.Value)) rec.Agency = txtAgencyID.Value;
            if (!string.IsNullOrEmpty(txtUserID.Value)) rec.Code = txtUserID.Value;
            if (!string.IsNullOrEmpty(txtCode.Text)) rec.Code = txtCode.Text;
            if (!string.IsNullOrEmpty(txtName.Text)) rec.Name = txtName.Text;
            if (cbStatus != null) rec.Status = cbStatus.Checked;
            if (!string.IsNullOrEmpty(txtPIN.Text)) rec.PIN = txtCode.Text;
            if (!string.IsNullOrEmpty(txtPhone.Text)) rec.Phone = txtCode.Text;
            if (!string.IsNullOrEmpty(txtMobile.Text)) rec.Mobile = txtCode.Text;
            if (!string.IsNullOrEmpty(txtEMail.Text)) rec.EMail = txtCode.Text;
            if (!string.IsNullOrEmpty(txtHAddress.Text)) rec.HAddress = txtCode.Text;
            if (cbShowAllRes != null) rec.ShowAllRes = cbShowAllRes.Checked;
            if (!string.IsNullOrEmpty(txtPassword.Text)) rec.Pass = txtCode.Text;
            if (!string.IsNullOrEmpty(txtExpDate.Text)) rec.ExpDate = ppcExpDate.DateValue.Date;
            //if (!string.IsNullOrEmpty(txtExpedientID.Text)) rec.ExpedientID = txtCode.Text;
        }
        return rec;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ppcExpDate.Culture = "ru-ru";
        if (HttpContext.Current.Session["Culture"] != null)
        {
            CultureInfo CulInfo = (CultureInfo)HttpContext.Current.Session["Culture"];
            Thread.CurrentThread.CurrentCulture = CulInfo;
            Thread.CurrentThread.CurrentUICulture = CulInfo;
            ppcExpDate.Culture = CulInfo.Name;
        }
        User userData = (User)Session["UserData"];
        if (userData.CustomRegID == Common.crID_SummerTour)
        {
            trSummerPin.Visible = false;
        }
        cbShowAllRes.Text = HttpContext.GetGlobalResourceObject("AdminAgencyControlPage", "ShowAllResTitle").ToString();
        cbStatus.Text = HttpContext.GetGlobalResourceObject("AdminAgencyControlPage", "StatusEnable").ToString();
    }
    
}

