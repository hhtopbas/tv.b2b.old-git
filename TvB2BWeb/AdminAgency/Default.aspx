﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="AgencyAdmin_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Agency Management Page</title>

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    
    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <style type="text/css">        
        table td { padding: 2px 2px 2px 4px; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="font-family: Verdana;">
        <div style="text-align: center; background-color: #BCBCBC">
            <br />
            <span style="font-size: 12pt;"><b>
                <%= GetGlobalResourceObject("AdminAgencyLogin","PageTitle") %></b></span>
            <br />
            <br />
        </div>
        <br />
        <div style="text-align: center">
            <table style="border: solid 1px #000000;">
                <tr>
                    <td align="right" style=>
                        <strong>
                            <%= GetGlobalResourceObject("AdminAgencyLogin","AgencyCode") %>:</strong>
                    </td>
                    <td align="right" style="height: 22px; text-align: left !important;">
                        <asp:TextBox ID="txtAgencyCode" runat="server" Width="200px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <%= GetGlobalResourceObject("AdminAgencyLogin","MasterPassword") %>:</strong>
                    </td>
                    <td style="height: 22px; text-align: left !important;">
                        <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" Width="200px" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">

                        <asp:Button runat="server" ID="btnNewAgency" Text="<%$ Resources:AdminAgencyLogin, btnNewAgency %>" OnClick="btnNewAgency_Click" />
                        <asp:Button ID="btnLogin" runat="server" Text="<%$ Resources:AdminAgencyLogin, btnLogin %>"
                            OnClick="btnLogin_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
