﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using TvBo;
using System.Threading;
using System.Configuration;
using TvTools;

public partial class AgencyAdmin_Default : System.Web.UI.Page
{
    
    protected override void InitializeCulture()
    {
        base.InitializeCulture();
        String[] userLang = Request.UserLanguages;
        String[] CI = userLang[0].Split(';');
        string CultureStr = CI[0];
        CultureInfo[] allCulture = CultureInfo.GetCultures(CultureTypes.AllCultures);
        if (allCulture.Where(w => Equals(w.Name, CultureStr)).Count() < 1) {
            CultureStr = "en-US";
        }
        if (Equals(CultureStr, "nb")) CultureStr = "nb-NO"; if (Equals(CultureStr, "sr-SC")) CultureStr = "en-US";
        if (Equals(CultureStr, "ar"))
        {
            string defaultCulture = Conversion.getStrOrNull(ConfigurationManager.AppSettings["DefaultCulture"]);
            CultureStr = string.IsNullOrEmpty(defaultCulture) ? "en-US" : (defaultCulture.IndexOf(CultureStr) > -1 ? defaultCulture : "en-US");
        }
        if (CultureStr.Length < 5)
        {
            CultureStr = (from culinf in CultureInfo.GetCultures(CultureTypes.AllCultures).AsEnumerable()
                          where culinf.Name == CultureStr
                          select new { CultureName = culinf.TextInfo.CultureName }).First().CultureName;
        }
        CultureInfo ci = new CultureInfo(CultureStr);
        Session["Culture"] = ci;
        if (Session["Culture"] != null)
        {
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo(((System.Globalization.CultureInfo)Session["Culture"]).Name, false);
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            Session.RemoveAll();
            Session["BasePageRoot"] = Global.getBasePageRoot();
            Session["BasePage"] = Global.getBasePage();
            
            String[] userLang = Request.UserLanguages;
            String[] CI = userLang[0].Split(';');
            string CultureStr = CI[0];
            CultureInfo[] allCulture = CultureInfo.GetCultures(CultureTypes.AllCultures);
            if (allCulture.Where(w => Equals(w.Name, CultureStr)).Count() < 1) {
                CultureStr = "en-US";
            }
            if (Equals(CultureStr, "nb")) CultureStr = "nb-NO"; if (Equals(CultureStr, "sr-SC")) CultureStr = "en-US";
            if (Equals(CultureStr, "ar"))
            {
                string defaultCulture = Conversion.getStrOrNull(ConfigurationManager.AppSettings["DefaultCulture"]);
                CultureStr = string.IsNullOrEmpty(defaultCulture) ? "en-US" : (defaultCulture.IndexOf(CultureStr) > -1 ? defaultCulture : "en-US");
            }
            if (CultureStr.Length < 5)
            {
                CultureStr = (from culinf in CultureInfo.GetCultures(CultureTypes.AllCultures).AsEnumerable()
                              where culinf.Name == CultureStr
                              select new { CultureName = culinf.TextInfo.CultureName }).First().CultureName;
            }
            CultureInfo ci = new CultureInfo(CultureStr);
            Session["Culture"] = ci;
            var showNewAgency = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "AdminAgencyShowNewAgency"));
            btnNewAgency.Visible = showNewAgency.HasValue && showNewAgency.Value;
        }
        if (HttpContext.Current.Session["Culture"] != null)
        {
            CultureInfo CulInfo = (CultureInfo)HttpContext.Current.Session["Culture"];
            Thread.CurrentThread.CurrentCulture = CulInfo;
            Thread.CurrentThread.CurrentUICulture = CulInfo;
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["Culture"] != null)
        {
            CultureInfo CulInfo = (CultureInfo)HttpContext.Current.Session["Culture"];
            Thread.CurrentThread.CurrentCulture = CulInfo;
            Thread.CurrentThread.CurrentUICulture = CulInfo;
        }
        if (string.IsNullOrEmpty(WebRoot.BasePageRoot))
        {           
            Response.Redirect(WebRoot.BasePage);
        }
        User UserData = new TvBo.Users().CreateUserData();

        UserData.AgencyID = txtAgencyCode.Text.TrimStart().TrimEnd().ToUpper();
        UserData.UserID = "";
        UserData.Password = txtPassword.Text;
        UserData.Ci = (CultureInfo)Session["Culture"];
        
        string errorMsg = string.Empty;
        if (string.IsNullOrEmpty(UserData.UserID))
            UserData.MasterAgency = true;
        else UserData.MasterAgency = false;

        string pageUrl = new LoginUser().toLogin(UserData,false, ref errorMsg);
        if (new Users().AgencyControl(ref UserData, new LoginUser().IpAddress(), ref errorMsg, Session.SessionID,false))
        {
            UserData.HeaderData = new UICommon().getHeaderData(UserData, ref errorMsg);
            Session["UserData"] = UserData;
            Response.Redirect("ControlPage.aspx");
        }         
    }
    protected void btnNewAgency_Click(object sender, EventArgs e)
    {
        Response.Redirect(ResolveClientUrl("~/adminagency/agencyregistration.aspx"));
    }
}
