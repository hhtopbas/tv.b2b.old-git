﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using TvBo;
using TvTools;
using System.Globalization;
using System.Threading;

public partial class AgencyAdmin_AgencyEdit : System.Web.UI.UserControl
{
    private string errorMsg = string.Empty;

    AgencyRecord oDataSource;
    public AgencyRecord ODataSource
    {
        get { return getDataSource(); }
        set { setDataSource(value); oDataSource = value; }
    }

    protected override void OnDataBinding(EventArgs e)
    {
        base.OnDataBinding(e);        
    }

    protected void setDataSource(AgencyRecord value)
    {
        bossName.Text = value.BossName;
        contactName.Text = value.ContName;
        phone1.Text = value.Phone1;
        phone2.Text = value.Phone2;
        mobile.Text = value.MobPhone;
        fax1.Text = value.Fax1;
        fax2.Text = value.Fax2;
        www.Text = value.Www;
        email1.Text = value.Email1;
        email2.Text = value.Email2;
        address.Text = value.Address;
        zip.Text = value.AddrZip;
        city.Text = value.AddrCity;
    }

    protected AgencyRecord getDataSource()
    {
        AgencyRecord rec = new AgencyRecord();
        if (oDataSource != null)
        {
            rec = oDataSource;
            if (!string.IsNullOrEmpty(contactName.Text)) rec.ContName = contactName.Text;
            if (!string.IsNullOrEmpty(phone1.Text)) rec.ContName = phone1.Text;
            if (!string.IsNullOrEmpty(phone2.Text)) rec.ContName = phone2.Text;
            if (!string.IsNullOrEmpty(mobile.Text)) rec.ContName = mobile.Text;
            if (!string.IsNullOrEmpty(fax1.Text)) rec.ContName = fax1.Text;
            if (!string.IsNullOrEmpty(fax2.Text)) rec.ContName = fax2.Text;
            if (!string.IsNullOrEmpty(www.Text)) rec.ContName = www.Text;
            if (!string.IsNullOrEmpty(email1.Text)) rec.ContName = email1.Text;
            if (!string.IsNullOrEmpty(email2.Text)) rec.ContName = email2.Text;
            if (!string.IsNullOrEmpty(address.Text)) rec.ContName = address.Text;
            if (!string.IsNullOrEmpty(zip.Text)) rec.ContName = zip.Text;
            if (!string.IsNullOrEmpty(city.Text)) rec.ContName = city.Text;                                    
        }
        return rec;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["Culture"] != null)
        {
            CultureInfo CulInfo = (CultureInfo)HttpContext.Current.Session["Culture"];
            Thread.CurrentThread.CurrentCulture = CulInfo;
            Thread.CurrentThread.CurrentUICulture = CulInfo;
        }
    }
    
}

