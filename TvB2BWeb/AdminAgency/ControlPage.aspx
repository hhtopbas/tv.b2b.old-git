﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ControlPage.aspx.cs" Inherits="AgencyAdmin_ControlPage"
    EnableEventValidation="false" %>

<%@ Register Src="~/AdminAgency/UserEdit.ascx" TagName="UserEdit" TagPrefix="uc1" %>
<%@ Register Src="~/AdminAgency/AgencyEdit.ascx" TagName="AgencyEdit" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Agency User Control</title>

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <%--<script src="../Scripts/date-functions.js" type="text/javascript"></script>--%>
    <link href="../CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/confirm.css" rel="stylesheet" type="text/css" />
    <link href="CSS/ControlPage.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function confirm(message, callback) {
            $('#confirm').modal({
                closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
                position: ["20%", ],
                overlayId: 'confirm-overlay',
                containerId: 'confirm-container',
                onShow: function(dialog) {
                    $('.message', dialog.data[0]).append(message);

                    $('.yes', dialog.data[0]).click(function() {

                        if ($.isFunction(callback)) {
                            callback.apply();
                        }

                        $.modal.close();
                    });
                }
            });
        }

        function info(message, callback) {
            $('#info').modal({
                closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
                position: ["20%", ],
                overlayId: 'info-overlay',
                containerId: 'info-container',
                onShow: function(dialog) {
                    $('.message', dialog.data[0]).append(message);

                    // if the user clicks "yes"
                    $('.yes', dialog.data[0]).click(function() {
                        // call the callback
                        if ($.isFunction(callback)) {
                            callback.apply();
                        }
                        // close the dialog
                        $.modal.close();
                    });
                }
            });
        }

        function showDialog(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function showEditUser() {
            $(function() {
                $("#dialog").dialog("destroy");
                $("#dialog-pnlEditUser").dialog({
                    modal: true,
                    height: 500,
                    width: 500,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>': function() {
                            var _recID = document.getElementById('<%= (ucEditUser.FindControl("txtRecID")).ClientID %>').value;
                            var _userID = document.getElementById('<%= (ucEditUser.FindControl("txtUserID")).ClientID %>').value;
                            var _agencyID = document.getElementById('<%= (ucEditUser.FindControl("txtAgencyID")).ClientID %>').value;
                            var _code = document.getElementById('<%= ((TextBox)ucEditUser.FindControl("txtCode")).ClientID %>').value;
                            var _name = document.getElementById('<%= ((TextBox)ucEditUser.FindControl("txtName")).ClientID %>').value;
                            var _status = document.getElementById('<%= (ucEditUser.FindControl("cbStatus")).ClientID %>');
                            var _pin = '';//document.getElementById('<%= (ucEditUser.FindControl("txtPIN")).ClientID %>').value;
                            var _phone = document.getElementById('<%= ((TextBox)ucEditUser.FindControl("txtPhone")).ClientID %>').value;
                            var _mobile = document.getElementById('<%= ((TextBox)ucEditUser.FindControl("txtMobile")).ClientID %>').value;
                            var _email = document.getElementById('<%= ((TextBox)ucEditUser.FindControl("txtEMail")).ClientID %>').value;
                            var _haddress = document.getElementById('<%= ((TextBox)ucEditUser.FindControl("txtHAddress")).ClientID %>').value;
                            var _showAllRes = document.getElementById('<%= (ucEditUser.FindControl("cbShowAllRes")).ClientID %>');
                            var _pass = document.getElementById('<%= ((TextBox)ucEditUser.FindControl("txtPassword")).ClientID %>').value;
                            var _expdate = document.getElementById('<%= ((RJS.Web.WebControl.PopCalendar)ucEditUser.FindControl("ppcExpDate")).ClientID %>');
                            var _expdatetxt = document.getElementById('<%= (ucEditUser.FindControl("txtExpDate")).ClientID %>');
                            var _expedientid = '';
                            var _format = _expdatetxt.getAttribute("Format");
                            var _PopCal = eval(_expdatetxt.getAttribute("Calendar"));
                            var _date = _PopCal.getDate(_expdatetxt.value, _format);

                            var _y = _PopCal.oYear;
                            var _m = _PopCal.oMonth + 1;
                            var _d = _PopCal.oDate;
                            var date = _y + ':' + _m + ':' + _d;
                            if (_date == null)
                                date = '';
                            var paraList = '"agency":"' + _agencyID + '"' +
                                           ',"code":"' + _code + '"' +
                                           ',"name":"' + _name + '"' +
                                           ',"status":"' + _status.checked + '"' +
                                           ',"pin":"' + _pin + '"' +
                                           ',"phone":"' + _phone + '"' +
                                           ',"mobile":"' + _mobile + '"' +
                                           ',"eMail":"' + _email + '"' +
                                           ',"hAddress":"' + _haddress + '"' +
                                           ',"showAllRes":"' + _showAllRes.checked + '"' +
                                           ',"password":"' + _pass + '"' +
                                           ',"expDate":"' + date + '"' +
                                           ',"expedintID":"' + _expedientid + '"';

                            paraList = '{' + paraList + '}';
                            var serviceUrl = '';
                            if (_recID == '' || _recID == '0')
                                serviceUrl = '../Services/AdmAgencyUsers.asmx/createAgencyUser';
                            else serviceUrl = '../Services/AdmAgencyUsers.asmx/editAgencyUser';
                            $.ajax({
                                type: "POST",
                                data: paraList,
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                url: serviceUrl,
                                success: function(msg) {
                                    info(msg.d == '1' ? '<%= GetGlobalResourceObject("AdminAgencyControlPage","Save") %>' : '<%= GetGlobalResourceObject("AdminAgencyControlPage","NotSave") %>', function() {
                                        if (msg.d == '1')
                                            __doPostBack('hdSaveBtn', '');
                                    });
                                },
                                error: function() {
                                    alert('<%= GetGlobalResourceObject("AdminAgencyControlPage","NotSave") %>');
                                }
                            });

                            $(this).dialog('close');
                        },
                        '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function showEditAgency() {
            $(function() {
                $("#dialog").dialog("destroy");
                $("#dialog-pnlEditAgency").dialog({
                    modal: true,
                    height: 500,
                    width: 500,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>': function() {
                            var _agencyID = document.getElementById('<%= txtAgencyCode.ClientID %>').value;
                            var _contactName = document.getElementById('<%= ((TextBox)ucAgencyEdit.FindControl("contactName")).ClientID %>').value;
                            var _phone1 = document.getElementById('<%= ((TextBox)ucAgencyEdit.FindControl("phone1")).ClientID %>').value;
                            var _phone2 = document.getElementById('<%= ((TextBox)ucAgencyEdit.FindControl("phone2")).ClientID %>').value;
                            var _mobile = document.getElementById('<%= ((TextBox)ucAgencyEdit.FindControl("mobile")).ClientID %>').value;
                            var _fax1 = document.getElementById('<%= ((TextBox)ucAgencyEdit.FindControl("fax1")).ClientID %>').value;
                            var _fax2 = document.getElementById('<%= ((TextBox)ucAgencyEdit.FindControl("fax2")).ClientID %>').value;
                            var _www = document.getElementById('<%= ((TextBox)ucAgencyEdit.FindControl("www")).ClientID %>').value;
                            var _email1 = document.getElementById('<%= ((TextBox)ucAgencyEdit.FindControl("email1")).ClientID %>').value;
                            var _email2 = document.getElementById('<%= ((TextBox)ucAgencyEdit.FindControl("email2")).ClientID %>').value;
                            var _address = document.getElementById('<%= ((TextBox)ucAgencyEdit.FindControl("address")).ClientID %>').value;
                            var _zip = document.getElementById('<%= ((TextBox)ucAgencyEdit.FindControl("zip")).ClientID %>').value;
                            var _city = document.getElementById('<%= ((TextBox)ucAgencyEdit.FindControl("city")).ClientID %>').value;

                            var paraList = '"agency":"' + _agencyID + '"' +
                                           ',"contactName":"' + _contactName + '"' +
                                           ',"phone1":"' + _phone1 + '"' +
                                           ',"phone2":"' + _phone2 + '"' +
                                           ',"mobile":"' + _mobile + '"' +
                                           ',"fax1":"' + _fax1 + '"' +
                                           ',"fax2":"' + _fax2 + '"' +
                                           ',"www":"' + _www + '"' +
                                           ',"email1":"' + _email1 + '"' +
                                           ',"email2":"' + _email2 + '"' +
                                           ',"address":"' + _address + '"' +
                                           ',"zip":"' + _zip + '"' +
                                           ',"city":"' + _city + '"';

                            paraList = '{' + paraList + '}';
                            var serviceUrl = '../Services/AdmAgencyUsers.asmx/editAgency';
                            $.ajax({
                                type: "POST",
                                data: paraList,
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                url: serviceUrl,
                                success: function(msg) {
                                    info(msg.d == '1' ? '<%= GetGlobalResourceObject("AdminAgencyControlPage","Save") %>' : '<%= GetGlobalResourceObject("AdminAgencyControlPage","NotSave") %>', function() {
                                        if (msg.d == '1')
                                            __doPostBack('hdAgencySaveBtn', '');
                                    });
                                },
                                error: function() {
                                    alert('<%= GetGlobalResourceObject("AdminAgencyControlPage","NotSave") %>');
                                }
                            });

                            $(this).dialog('close');
                        },
                        '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }               
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="AgencyManagementSM" runat="server" />
    <div style="font-family: Verdana;">
        <div style="text-align: center; background-color: #BCBCBC">
            <br />
            <span style="font-size: 12pt;"><b>
                <%= GetGlobalResourceObject("AdminAgencyLogin","PageTitle") %></b></span>
            <br />
            <br />
        </div>
        <br />
        <div style="width: 100%">
            <div style="width: 350px;margin:auto;">
                <asp:Repeater ID="rpAgencyList" runat="server" OnItemCommand="rpAgencyList_ItemCommand"
                    EnableViewState="true" OnItemDataBound="rpAgencyList_ItemDataBound">
                    <HeaderTemplate>
                        <table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td id="tdAgency" runat="server" style="border: solid 1px #BCBCBC;">
                                <b>
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage","AgencyName") %>:</b>
                                <%#  DataBinder.Eval(Container.DataItem, "AgencyName")%><br />
                                <b>
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage","FirmName") %>:</b>
                                <%#  DataBinder.Eval(Container.DataItem, "AgencyFirmName")%><br />
                                <asp:LinkButton ID="lnkSelect" runat="server" CommandName="ViewEdit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Code") %>'><%= GetGlobalResourceObject("AdminAgencyControlPage","Select") %></asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <div style="margin:auto;width:800px">
                <div id="agencyInfoDiv" runat="server" style="display: none;">
                    <table cellpadding="0" cellspacing="0" style="width: 600px;">
                        <tr>
                            <td class="leftTd">
                                <strong>
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage","AgencyCode") %>: </strong>
                            </td>
                            <td>
                                <asp:Label ID="agencyCode" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td class="leftTd">
                                <strong>
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage","AgencyName") %>: </strong>
                            </td>
                            <td>
                                <asp:Label ID="agencyName" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td class="leftTd">
                                <strong>
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage","FirmName") %>: </strong>
                            </td>
                            <td>
                                <asp:Label ID="agencyFirmName" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td class="leftTd">
                                <strong>
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage","OperatorOffice") %>: </strong>
                            </td>
                            <td>
                                <asp:Label ID="agencyOperatorOffice" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td class="leftTd">
                                <strong>
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage","Location") %>: </strong>
                            </td>
                            <td>
                                <asp:Label ID="agencyLocation" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td class="leftTd">
                                <strong>
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage","RegisterCode") %>: </strong>
                            </td>
                            <td>
                                <asp:Label ID="agencyRegisterCode" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td class="leftTd">
                                <strong>
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage","LicanceNo") %>: </strong>
                            </td>
                            <td>
                                <asp:Label ID="agencyLicanceNo" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td class="leftTd">
                                <strong>
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage","TaxAccountNo") %>: </strong>
                            </td>
                            <td>
                                <asp:Label ID="agencyTaxAccountNo" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td class="leftTd">
                                <strong>
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage","AccountCode") %>: </strong>
                            </td>
                            <td>
                                <asp:Label ID="agencyAccountCode" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td class="leftTd">
                                <strong>
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage","ContractNo") %>: </strong>
                            </td>
                            <td>
                                <asp:Label ID="agencyContractNo" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td class="leftTd">
                                <strong>
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage","ContractType") %>: </strong>
                            </td>
                            <td>
                                <asp:Label ID="agencyContractType" runat="server" Text="" />
                            </td>
                        </tr>
                    </table>
                    <asp:Button ID="editAgency" runat="server" Text="<%$ Resources:AdminAgencyControlPage, Edit %>"
                        OnClick="editAgency_Click" />
                    <br />
                    <hr />
                    <br />
                </div>
                <asp:Button ID="createUser" runat="server" Text="<%$ Resources:AdminAgencyControlPage, NewUser %>"
                    OnClick="createUser_Click" />
                <asp:HiddenField ID="txtAgencyCode" runat="server" />
                <asp:Repeater ID="rpAgencyUserList" runat="server" OnItemCommand="rpAgencyUserList_ItemCommand">
                    <HeaderTemplate>
                        <table cellpadding="2" cellspacing="0" style="empty-cells: inherit;">
                            <tr style="font-weight: bold;">
                                <td valign="bottom">
                                    &nbsp;
                                </td>
                                <td style="width: 75px;" class="tdHeader" valign="bottom">
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage", "UserCode")%>
                                </td>
                                <td style="width: 250px;" class="tdHeader" valign="bottom">
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage", "UserName")%>
                                </td>
                                <td style="width: 75px;" class="tdHeader" valign="bottom">
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage", "ExpireDate")%>
                                </td>
                                <td style="width: 30px;" align="center" class="tdHeader" valign="bottom">
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage", "ShowAllRes")%>
                                </td>
                                <td style="width: 30px;" align="center" class="tdHeader" valign="bottom">
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage", "Status")%>
                                </td>
                                <%if (CustomRegId != TvBo.Common.crID_SummerTour)
                                  { %>
                                <td style="width: 100px;" class="tdHeader" valign="bottom" >
                                    <%= GetGlobalResourceObject("AdminAgencyControlPage", "ExpedientID")%>
                                </td>
                                <%} %>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td id="viewEdit" runat="server">
                                <asp:LinkButton ID="lnkEdit" runat="server" CommandName="ViewEdit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Code") %>'><%= GetGlobalResourceObject("AdminAgencyControlPage", "ViewEdit")%></asp:LinkButton>
                            </td>
                            <td class="tdFirst">
                                <%# DataBinder.Eval(Container.DataItem, "Code") %>
                            </td>
                            <td class="tdSeconds">
                                <%# DataBinder.Eval(Container.DataItem, "Name") %>
                            </td>
                            <td class="tdSeconds">
                                <%# DataBinder.Eval(Container.DataItem, "ExpDate") %>
                            </td>
                            <td align="center" class="tdSeconds">
                                <asp:CheckBox ID="cbShowAllRes" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "ShowAllRes")%>'
                                    Enabled="false" />
                            </td>
                            <td align="center" class="tdSeconds">
                                <asp:CheckBox ID="cbStatus" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Status") %>'
                                    Enabled="false" />
                            </td>
                             <%if (CustomRegId != TvBo.Common.crID_SummerTour)
                                 { %>
                            <td class="tdSeconds">
                                <%# DataBinder.Eval(Container.DataItem, "ExpedientID") %>
                            </td>
                            <%} %>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    <div id="dialog-pnlEditUser" title='<%= GetGlobalResourceObject("AdminAgencyControlPage", "EditUser")%>'
        style="display: none;">
        <uc1:UserEdit ID="ucEditUser" runat="server" />
        <asp:Button ID="hdSaveBtn" runat="server" Text="Hidden" OnClick="hdSaveBtn_Click"
            Visible="false" EnableViewState="false" />
    </div>
    <div id="dialog-pnlEditAgency" title='<%= GetGlobalResourceObject("AdminAgencyControlPage", "EditAgency")%>'
        style="display: none;">
        <uc2:AgencyEdit ID="ucAgencyEdit" runat="server" />
        <asp:Button ID="hdAgencySaveBtn" runat="server" Text="Hidden" OnClick="hdAgencySaveBtn_Click"
            Visible="false" EnableViewState="false" />
    </div>
    <div id="dialog-message" title="" style="display: none;">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;">
            </span><span id="messages">Message</span>
        </p>
    </div>
    <div id="info">
        <div class="header">
            <span>
                <%= GetGlobalResourceObject("LibraryResource", "msgInfo")%></span></div>
        <p class="message">
        </p>
        <div class="buttons">
            <div class="yes">
                <%= GetGlobalResourceObject("LibraryResource", "btnOK")%></div>
        </div>
    </div>
    <div id="confirm">
        <div class="header">
            <span>
                <%= GetGlobalResourceObject("LibraryResource", "msgConfirm")%></span></div>
        <p class="message">
        </p>
        <div class="buttons">
            <div class="no simplemodal-close">
                <%= GetGlobalResourceObject("LibraryResource", "btnNo")%></div>
            <div class="yes">
                <%= GetGlobalResourceObject("LibraryResource", "btnYes")%></div>
        </div>
    </div>
    </form>
</body>
</html>
