﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgencyEdit.ascx.cs" Inherits="AgencyAdmin_AgencyEdit" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
    
<link href="CSS/ControlPage.css" rel="stylesheet" type="text/css" /> 
<link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />

<asp:HiddenField ID="txtAgencyID" runat="server" />
<table id="agencyEdit" cellpadding="2" cellspacing="0">
    <tr>
        <td class="leftTd">
            <strong><%= GetGlobalResourceObject("AdminAgencyControlPage","BossName") %>: </strong>
        </td>
        <td>
            <asp:Label ID="bossName" runat="server" CssClass="ui-widget-content ui-corner-all" />
        </td>
    </tr>
    <tr>
        <td class="leftTd">
            <strong><%= GetGlobalResourceObject("AdminAgencyControlPage","ContactName") %>: </strong>
        </td>
        <td>
            <asp:TextBox ID="contactName" runat="server" CssClass="ui-widget-content ui-corner-all"
                Width="300px" MaxLength="30" />
        </td>
    </tr>
    <tr>
        <td class="leftTd">
            <strong><%= GetGlobalResourceObject("AdminAgencyControlPage","Phone12") %>: </strong>
        </td>
        <td>
            <asp:TextBox ID="phone1" runat="server" CssClass="ui-widget-content ui-corner-all"
                Width="140px" MaxLength="20" />&nbsp;/&nbsp;
                <asp:TextBox ID="phone2" runat="server" CssClass="ui-widget-content ui-corner-all"
                Width="140px" MaxLength="20" />
        </td>
    </tr>    
    <tr>
        <td class="leftTd">
           <strong><%= GetGlobalResourceObject("AdminAgencyControlPage","Mobile") %>: </strong>
        </td>
        <td>
             <asp:TextBox ID="mobile" runat="server" CssClass="ui-widget-content ui-corner-all"
                Width="140px" MaxLength="20" />
        </td>
    </tr>
    <tr>
        <td class="leftTd">
            <strong><%= GetGlobalResourceObject("AdminAgencyControlPage","Fax12") %>: </strong>
        </td>
        <td>
            <asp:TextBox ID="fax1" runat="server" CssClass="ui-widget-content ui-corner-all"
                Width="140px" MaxLength="20" />&nbsp;/&nbsp;
                <asp:TextBox ID="fax2" runat="server" CssClass="ui-widget-content ui-corner-all"
                Width="140px" MaxLength="20" />
        </td>
    </tr>
     <tr>
        <td class="leftTd">
            <strong><%= GetGlobalResourceObject("AdminAgencyControlPage","Web") %>: </strong>
        </td>
        <td>
            <asp:TextBox ID="www" runat="server" CssClass="ui-widget-content ui-corner-all"
                Width="300px" MaxLength="128" />
        </td>
    </tr>
    <tr>
        <td class="leftTd">
            <strong><%= GetGlobalResourceObject("AdminAgencyControlPage","EMail1") %>: </strong>
        </td>
        <td>
            <asp:TextBox ID="email1" runat="server" CssClass="ui-widget-content ui-corner-all"
                Width="300px" MaxLength="128" />
        </td>
    </tr>
    <tr>
        <td class="leftTd">
            <strong><%= GetGlobalResourceObject("AdminAgencyControlPage","EMail2") %>: </strong>
        </td>
        <td>
            <asp:TextBox ID="email2" runat="server" CssClass="ui-widget-content ui-corner-all"
                Width="300px" MaxLength="128" />
        </td>
    </tr>
    <tr>
        <td valign="top" class="leftTd">
            <strong><%= GetGlobalResourceObject("AdminAgencyControlPage","Address") %>: </strong>
        </td>
        <td>
            <asp:TextBox ID="address" runat="server" CssClass="ui-widget-content ui-corner-all"
                Height="50px" MaxLength="100" TextMode="MultiLine" Width="300px" />
        </td>
    </tr>
    <tr>
        <td class="leftTd">
            <strong><%= GetGlobalResourceObject("AdminAgencyControlPage","Zip") %>: </strong>
        </td>
        <td>
            <asp:TextBox ID="zip" runat="server" CssClass="ui-widget-content ui-corner-all"
                Width="100px" MaxLength="10" />
        </td>
    </tr>
    <tr>
        <td class="leftTd">
            <strong><%= GetGlobalResourceObject("AdminAgencyControlPage","City") %>: </strong>
        </td>
        <td>
            <asp:TextBox ID="city" runat="server" CssClass="ui-widget-content ui-corner-all"
                Width="300px" MaxLength="30" />
        </td>
    </tr>
</table>
