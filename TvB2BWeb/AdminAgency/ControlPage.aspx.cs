﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Globalization;
using System.Threading;
using TvTools;

public partial class AgencyAdmin_ControlPage : System.Web.UI.Page
{
    protected string CustomRegId = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["Culture"] != null)
        {
            CultureInfo CulInfo = (CultureInfo)HttpContext.Current.Session["Culture"];
            Thread.CurrentThread.CurrentCulture = CulInfo;
            Thread.CurrentThread.CurrentUICulture = CulInfo;
        }
        editAgency.Text = HttpContext.GetGlobalResourceObject("AdminAgencyControlPage", "Edit").ToString();
        createUser.Text = HttpContext.GetGlobalResourceObject("AdminAgencyControlPage", "NewUser").ToString();
        var _editAgency = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "AdminAgencyEditAgency"));
        if (_editAgency.HasValue && !_editAgency.Value)
            editAgency.Visible = false;
        User userData = (User)Session["UserData"];
        if (!IsPostBack && userData != null)
            pageIsPosBack(sender, e, userData);
        else if (userData == null)
            Response.Redirect("Default.aspx");
                
        pageIsAfterPosBack(sender, e, userData);
    }

    protected void pageIsPosBack(object sender, EventArgs e, User userData)
    {
        if (HttpContext.Current.Session["Culture"] != null)
        {
            CultureInfo CulInfo = (CultureInfo)HttpContext.Current.Session["Culture"];
            Thread.CurrentThread.CurrentCulture = CulInfo;
            Thread.CurrentThread.CurrentUICulture = CulInfo;
        }
        
        string errorMsg = string.Empty;
        List<AdmAgencyRecord> agencyList = new AdminAgency().getAgencys(userData.AgencyID, ref errorMsg);
        rpAgencyList.DataSource = agencyList;
        rpAgencyList.DataBind();
    }

    protected void pageIsAfterPosBack(object sender, EventArgs e, User userData)
    {
        if (HttpContext.Current.Session["Culture"] != null)
        {
            CultureInfo CulInfo = (CultureInfo)HttpContext.Current.Session["Culture"];
            Thread.CurrentThread.CurrentCulture = CulInfo;
            Thread.CurrentThread.CurrentUICulture = CulInfo;
        }
        CustomRegId = userData.CustomRegID;
        if (string.IsNullOrEmpty(txtAgencyCode.Value))
            createUser.Visible = false;
        else createUser.Visible = true;
    }

    protected void rpAgencyList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string errorMsg = string.Empty;
        if (Equals(e.CommandName, "ViewEdit"))
        {
            string argument = e.CommandArgument.ToString();
            txtAgencyCode.Value = argument;

            AgencyRecord agency = new Agency().getAgency(argument, ref errorMsg);
            if (agency != null)
            {
                agencyInfoDiv.Style["display"] = "block";
                agencyCode.Text = Conversion.getStrOrNull(agency.Code);
                agencyName.Text = Conversion.getStrOrNull(!string.IsNullOrEmpty(agency.LocalName)?agency.LocalName:agency.Name);
                agencyFirmName.Text = Conversion.getStrOrNull(agency.FirmName);
                agencyOperatorOffice.Text = Conversion.getStrOrNull(agency.OprOfficeNameL);
                agencyLocation.Text = Conversion.getStrOrNull(agency.LocationLocalName);
                agencyRegisterCode.Text = Conversion.getStrOrNull(agency.RegisterCode);
                agencyLicanceNo.Text = Conversion.getStrOrNull(agency.LicenseNo);
                agencyTaxAccountNo.Text = Conversion.getStrOrNull(agency.TaxAccNo);
                agencyAccountCode.Text = Conversion.getStrOrNull(agency.AccountCode);
                List<AgencyAgreeRecord> agencyAgree = new Agency().getAgencyAgree(argument, ref errorMsg);
                if (agencyAgree != null && agencyAgree.Count > 0)
                {
                    var agree = from q in agencyAgree
                                where q.BegDate <= DateTime.Today && q.EndDate >= DateTime.Today
                                orderby q.BegDate
                                select new { q.ContractNo, q.AgreeTypeName };
                    if (agree != null && agree.Count() > 0)
                    {
                        agencyContractNo.Text = Conversion.getStrOrNull(agree.FirstOrDefault().ContractNo);
                        agencyContractType.Text = Conversion.getStrOrNull(agree.FirstOrDefault().AgreeTypeName);
                    }
                }
            }
            else agencyInfoDiv.Style["display"] = "none";

            List<AdmAgencyUserRecord> userList = new AdminAgency().getUserRecordList(argument.ToString(), ref errorMsg);
            rpAgencyUserList.DataSource = userList;
            rpAgencyUserList.DataBind();
            if (string.IsNullOrEmpty(txtAgencyCode.Value))
                createUser.Visible = false;
            else createUser.Visible = true;
        }
    }

    protected void rpAgencyUserList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string errorMsg = string.Empty;
        if (Equals(e.CommandName, "ViewEdit"))
        {
            string argument = e.CommandArgument.ToString();
            AdmAgencyUserRecord rec = new AdminAgency().getUserRecord(txtAgencyCode.Value, argument, ref errorMsg);

            ucEditUser.ODataSource = rec;
            ucEditUser.DataBind();
            runJScript("showEditUser();", "scrptEditUser");
        }
    }

    protected void hdSaveBtn_Click(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["Culture"] != null)
        {
            CultureInfo CulInfo = (CultureInfo)HttpContext.Current.Session["Culture"];
            Thread.CurrentThread.CurrentCulture = CulInfo;
            Thread.CurrentThread.CurrentUICulture = CulInfo;
        }
        string errorMsg = string.Empty;
        string argument = txtAgencyCode.Value;
        List<AdmAgencyUserRecord> userList = new AdminAgency().getUserRecordList(argument, ref errorMsg);
        rpAgencyUserList.DataSource = userList;
        rpAgencyUserList.DataBind();
    }

    protected void hdAgencySaveBtn_Click(object sender, EventArgs e)
    {
        string errorMsg = string.Empty;
        string argument = txtAgencyCode.Value;

    }

    protected void rpAgencyUserList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.DataItem != null)
        {

        }
    }

    protected void rpAgencyList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.DataItem != null && e.Item.ItemType == ListItemType.Item)
        {
            AdmAgencyRecord rec = (AdmAgencyRecord)e.Item.DataItem;
        }
    }

    internal void runJScript(string jScript, string scriptID)
    {
        if (ScriptManager.GetCurrent(this.Page) != null)
        {
            if (ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), scriptID, jScript, true);
            else Page.ClientScript.RegisterStartupScript(this.GetType(), scriptID, jScript, true);
        }
        else Page.ClientScript.RegisterStartupScript(this.GetType(), scriptID, jScript, true);
    }

    protected void createUser_Click(object sender, EventArgs e)
    {
        string errorMsg = string.Empty;

        AdmAgencyUserRecord rec = new AdmAgencyUserRecord();
        rec.Agency = txtAgencyCode.Value;
        rec.ShowAllRes = false;
        rec.Status = true;

        ucEditUser.ODataSource = rec;
        ucEditUser.DataBind();
        runJScript("showEditUser();", "scrptEditUser");

    }

    protected void editAgency_Click(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["Culture"] != null)
        {
            CultureInfo CulInfo = (CultureInfo)HttpContext.Current.Session["Culture"];
            Thread.CurrentThread.CurrentCulture = CulInfo;
            Thread.CurrentThread.CurrentUICulture = CulInfo;
        }
        string errorMsg = string.Empty;
        AgencyRecord agency = new Agency().getAgency(txtAgencyCode.Value, ref errorMsg);

        ucAgencyEdit.ODataSource = agency;
        ucAgencyEdit.DataBind();
        runJScript("showEditAgency();", "scrptEditAgency");
    }
}
