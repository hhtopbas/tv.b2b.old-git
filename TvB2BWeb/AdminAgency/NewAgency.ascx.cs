﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using TvTools;

public partial class AdminAgency_NewAgency : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            var operatorMarket = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "AdminAgencyNewAgencyMarket"));
            var operatorCountry = Conversion.getInt32OrNull(new TvBo.Common().getFormConfigValue("General", "AdminAgencyNewAgencyCountryId"));
            var operatorDefaultCulture = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "AdminAgencyNewAgencyDefaultCulture"));
            if (!string.IsNullOrEmpty(operatorDefaultCulture))
            {
                CultureInfo CulInfo = new CultureInfo(operatorDefaultCulture, true);
                Thread.CurrentThread.CurrentCulture = CulInfo;
                Thread.CurrentThread.CurrentUICulture = CulInfo;
            }
            List<Location> LocationList = CacheObjects.getLocationList(operatorMarket);
            if (operatorCountry.HasValue)
                LocationList = LocationList.Where(w => w.Country == operatorCountry.Value).ToList();
            LocationList.Where(w => w.Type == 2).OrderBy(o=>o.NameL).ToList().ForEach(f =>
                {
                    ddlLocation.Items.Add(new ListItem { Text =string.IsNullOrEmpty(f.NameL)? f.Name:f.NameL, Value = f.RecID.ToString() });
                });
        }
    }
    private bool CheckFormValidation()
    {
        if (txtBossName.Text != "" && txtContactName.Text != "" && txtEMail.Text != "" && txtFirmName.Text != "" && txtMasterPass.Text != "" && txtName.Text != "" && txtPhone.Text != "")
            return true;
        else
            return false;
    }
    protected void btnNewAgency_Click(object sender, EventArgs e)
    {
        bool isValid = GetGoogleCaptcha(hfReCaptcha.Value);
        string errorMsg = "";
        var operatorNewAgency = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "AdminAgencyNewAgencyOperator"));
        
        if (!CheckFormValidation())
        {

            ltrDescription.Text = string.Format("<p class='<p class='bg-warning'>{0}</p>", Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "AdminAgencyNewAgencyAllFieldsRequired")));
            ltrDescription.Visible = true;
        }
        if (isValid && !string.IsNullOrEmpty(operatorNewAgency) && CheckFormValidation())
        {

            AgencyRecord agency = new AgencyRecord();
            agency.OprOffice = operatorNewAgency;
            
            agency.RegisterCode = txtRegisterCode.Text;
            
            DateTime registerDate = DateTime.MinValue;
            DateTime.TryParse(txtRegisterDate.Text, out registerDate);
            if (registerDate != DateTime.MinValue)
                agency.RegisterDate = registerDate;
            agency.BossName = txtBossName.Text;
            agency.ContName = txtContactName.Text;
            agency.FirmName = txtFirmName.Text;
            agency.Name = txtName.Text;
            agency.TypeOfOwnerShip = rbTypeOfOwnerShip1.Checked ? TypeOfOwnerShip.LLC : TypeOfOwnerShip.SoleProprietor;
            agency.VATPayer = rbVatPayerA.Checked ? true : false;
            agency.Location = Convert.ToInt32(ddlLocation.SelectedValue);
            agency.Email1 = txtEMail.Text;
            agency.Phone1 = txtPhone.Text;
            agency.MasterPass = txtMasterPass.Text;
            //agency.TaxAccNo = txtTaxAccNo.Text;
            var code = new Agency().AddAgency(agency, ref errorMsg);
            if (!string.IsNullOrEmpty(code) && string.IsNullOrEmpty(errorMsg))
            {
                string description = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "AdminAgencySuccessfulSaveMessage"));
                if (description.IndexOf("{0}") >= 0)
                    description = string.Format(description, code);
                ltrDescription.Text = string.Format("<p class='bg-success' style='font-size: larger;font - weight: bold;'>{0}</p>", description);
                SendMailToAgency(code);
                SendMailToOperator(code);
                txtBossName.Text = "";
                txtContactName.Text = "";
                txtEMail.Text = "";
                txtFirmName.Text = "";
                txtMasterPass.Text = "";
                txtName.Text = "";
                txtPhone.Text = "";
            }
            else
                ltrDescription.Text = string.Format("<p class='bg-warning'>{0}</p><div class='hide'>{1}</div>", "An error occurred, please try again later.",errorMsg);
            ltrDescription.Visible = true;
        }


    }
    private void SendMailToAgency(string agencyCode)
    {
        var operatorNewAgencyMail = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "AdminAgencyNewAgencyOperatorMail"));
        if (!string.IsNullOrEmpty(operatorNewAgencyMail))
        {
            var common = new TvBo.Common();
            AsyncMailSenderParams mailParam = new AsyncMailSenderParams();
            mailParam.SenderMail = common.getFormConfigValue("General", "AdminAgencyMailSenderMail").ToString();
            mailParam.DisplayName = common.getFormConfigValue("General", "AdminAgencyMailDisplayName").ToString();
            mailParam.SenderPassword = common.getFormConfigValue("General", "AdminAgencyMailSenderPassword").ToString();
            mailParam.ToAddress = txtEMail.Text.ToString();
            mailParam.Subject = "New Agency Information";
            mailParam.Port = Convert.ToInt32(common.getFormConfigValue("General", "AdminAgencyMailPort"));
            mailParam.SMTPServer = common.getFormConfigValue("General", "AdminAgencyMailSMTPServer").ToString();
            mailParam.EnableSSL = Convert.ToBoolean(common.getFormConfigValue("General", "AdminAgencyMailEnableSSL"));
            string body = GetHtmlForAgency(agencyCode);
            mailParam.Body = body;
            new SendMail().MailSender(mailParam);
        }
    }
    private void SendMailToOperator(string agencyCode)
    {
        var operatorNewAgencyMail = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "AdminAgencyNewAgencyOperatorMail"));
        if (!string.IsNullOrEmpty(operatorNewAgencyMail))
        {
            var common = new TvBo.Common();
            AsyncMailSenderParams mailParam = new AsyncMailSenderParams();
            mailParam.SenderMail = common.getFormConfigValue("General", "AdminAgencyMailSenderMail").ToString();
            mailParam.DisplayName = common.getFormConfigValue("General", "AdminAgencyMailDisplayName").ToString();
            mailParam.SenderPassword = common.getFormConfigValue("General", "AdminAgencyMailSenderPassword").ToString();
            mailParam.ToAddress = operatorNewAgencyMail;
            mailParam.Subject = "New Agency Information";
            mailParam.Port = Convert.ToInt32(common.getFormConfigValue("General", "AdminAgencyMailPort"));
            mailParam.SMTPServer = common.getFormConfigValue("General", "AdminAgencyMailSMTPServer").ToString();
            mailParam.EnableSSL = Convert.ToBoolean(common.getFormConfigValue("General", "AdminAgencyMailEnableSSL"));
            string body = GetHtmlForOperator(agencyCode);
            mailParam.Body = body;
            new SendMail().MailSender(mailParam);
        }
    }
    private string GetHtmlForAgency(string agencyCode)
    {
        string data = @"
                        <p>Summer Tour Russia приветствует <b>{0}</b>!</p>
                        <p>&nbsp;</p>
                        <p>Поздравляем вас с успешной регистрацией на сайте компании!</p>
                        <p>&nbsp;</p>
                        <p>Ваш логин: <b>{1}</b></p>
                        <p>&nbsp;</p>
                        <p>Ваш пароль: <b>{2}</b></p>
                        <p>&nbsp;</p>
                        <p>Войти в личный кабинет можно здесь http://185.67.207.251/b2b/adminagency/</p>
                        <p>&nbsp;</p>
                        <p>Пожалуйста, дождитесь активации вашего аккаунта, активация происходит по рабочим дням с 10:00 до 19:00 по московскому времени.</p>
                        <p>Вы получите письмо об активации на данный адрес электронной почты.</p>
                        <p>&nbsp;</p>
                        <p>Summer Tour Russia</p>
                        <p>&nbsp;</p>
                        <p>sales@summer-tour.com | www.summer-tour.com</p>
                        <p>127083, Москва, ул. 8 Марта, д. 1, стр. 12</p>
";
        return string.Format(data, txtName.Text.ToString(), agencyCode, txtMasterPass.Text.ToString());
    }
    public string GetHtmlForOperator(string agencyCode)
    {
        string data = @"
                        <p>Пришла новая регистрация агента<p>

                        дата регистрации {7}<br/>
                        Имя <b>{0}</b><br/>
                        ИНН <b>{1}</b><br/>
                        почта <b>{2}</b><br/>
                        город <b>{3}</b><br/>
                        телефон <b>{4}</b><br/>
                        директор <b>{5}</b><br/>
                        контактное лицо <b>{6}</b>
";
        return string.Format(data, txtName.Text.ToString(), agencyCode, txtEMail.Text, ddlLocation.SelectedItem.Text, txtPhone.Text, txtBossName.Text, txtContactName.Text, DateTime.Now.ToShortDateString());

    }
    private bool GetGoogleCaptcha(string response)
    {

        var client = new System.Net.WebClient();
        string PrivateKey = "6LfAbiMUAAAAAKtyO1Ds6vHPvQ8CiQpktoEY_F2y";
        var GoogleReply = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", PrivateKey, response));
        var captchaResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<reCaptcha>(GoogleReply);
        if (captchaResponse != null)
        {
            return (bool)captchaResponse.success;
        }
        else
        {
            return false;
        }
    }
    public class reCaptcha
    {
        public bool success { get; set; }
    }
}