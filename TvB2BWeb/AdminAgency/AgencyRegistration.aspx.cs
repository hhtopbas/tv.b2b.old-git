﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvTools;

public partial class AdminAgency_AgencyRegistration : System.Web.UI.Page
{
    protected override void InitializeCulture()
    {
        base.InitializeCulture();
        String[] userLang = Request.UserLanguages;
        String[] CI = userLang[0].Split(';');
        string CultureStr = CI[0];
        CultureInfo[] allCulture = CultureInfo.GetCultures(CultureTypes.AllCultures);
        if (allCulture.Where(w => Equals(w.Name, CultureStr)).Count() < 1)
        {
            CultureStr = "en-US";
        }
        if (Equals(CultureStr, "nb")) CultureStr = "nb-NO"; if (Equals(CultureStr, "sr-SC")) CultureStr = "en-US";
        if (Equals(CultureStr, "ar"))
        {
            string defaultCulture = Conversion.getStrOrNull(ConfigurationManager.AppSettings["DefaultCulture"]);
            CultureStr = string.IsNullOrEmpty(defaultCulture) ? "en-US" : (defaultCulture.IndexOf(CultureStr) > -1 ? defaultCulture : "en-US");
        }
        if (CultureStr.Length < 5)
        {
            CultureStr = (from culinf in CultureInfo.GetCultures(CultureTypes.AllCultures).AsEnumerable()
                          where culinf.Name == CultureStr
                          select new { CultureName = culinf.TextInfo.CultureName }).First().CultureName;
        }
        CultureInfo ci = new CultureInfo(CultureStr);
        Session["Culture"] = ci;
        if (Session["Culture"] != null)
        {
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo(((System.Globalization.CultureInfo)Session["Culture"]).Name, false);
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}