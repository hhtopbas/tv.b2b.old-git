﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewAgency.ascx.cs" Inherits="AdminAgency_NewAgency" %>
<script type="text/javascript">
    $(function () {
        //grecaptcha.execute();
    })
    function onSubmit(response) {

            $('#<%=hfReCaptcha.ClientID%>').val(response);
            $('#<%=btnNewAgency.ClientID%>').trigger("click");
            //document.getElementById("demo-form").submit();
    }
    function checkCaptcha() {
        grecaptcha.execute();
    }
</script>
<asp:HiddenField runat="server" ID="hfReCaptcha" />
<table style="width:100%;">
    <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","FirmShortName") %>
        </td>
        <td>
            <asp:TextBox ID="txtName" runat="server" CssClass="ui-widget-content ui-corner-all" MaxLength="60" />
        </td>
    </tr>
    <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","FirmName") %>
        </td>
        <td>
            <asp:TextBox ID="txtFirmName" runat="server" CssClass="ui-widget-content ui-corner-all" Width="300px" MaxLength="250" />
        </td>
    </tr>
    
     <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","RegisterCode") %>
        </td>
        <td>
            <asp:TextBox ID="txtRegisterCode" runat="server" CssClass="ui-widget-content ui-corner-all" Width="300px" MaxLength="20" />
        </td>
    </tr>
    
    <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","RegisterDate") %>
        </td>
        <td>
            <asp:TextBox ID="txtRegisterDate" runat="server" CssClass="ui-widget-content ui-corner-all" TextMode="Date" Width="300px" MaxLength="20" />
        </td>
    </tr>
    
    <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","TypeOfOwnerShip") %>
        </td>
        <td>
            <label class="checkbox inline">
                <asp:RadioButton runat="server" Checked="true" ID="rbTypeOfOwnerShip1" GroupName="TypeOfOwnerShip" Text="<%$ Resources:AdminAgencyControlPage, TypeOfOwnerShip1 %>" />
            </label>
            <label class="checkbox inline">
                <asp:RadioButton runat="server" ID="rbTypeOfOwnerShip2" GroupName="TypeOfOwnerShip" Text="<%$ Resources:AdminAgencyControlPage, TypeOfOwnerShip2 %>" />
            </label>

        </td>
    </tr>
    <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","BossName") %>
        </td>
        <td>
            <asp:TextBox ID="txtBossName" runat="server" CssClass="ui-widget-content ui-corner-all" Width="300px" MaxLength="30" />
        </td>
    </tr>
     <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","ContactName") %>
        </td>
        <td>
            <asp:TextBox ID="txtContactName" runat="server" CssClass="ui-widget-content ui-corner-all" Width="300px" MaxLength="30" />
        </td>
    </tr>
    <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","VatPayer") %>
        </td>
        <td>
            <label class="checkbox inline">
                <asp:RadioButton runat="server" Checked="true" ID="rbVatPayerA" GroupName="VatPayer" Text="<%$ Resources:AdminAgencyControlPage, VatPayerYes %>" />
            </label>
            <label class="checkbox inline">
                <asp:RadioButton runat="server" ID="rbVatPayerB" GroupName="VatPayer" Text="<%$ Resources:AdminAgencyControlPage, VatPayerNo %>" />
            </label>
        </td>
    </tr>
    <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","Location") %>
        </td>
        <td>
            <asp:DropDownList runat="server" ID="ddlLocation"></asp:DropDownList>
        </td>
    </tr>
     <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","Phone") %>
        </td>
        <td>
            <asp:TextBox ID="txtPhone" runat="server" CssClass="ui-widget-content ui-corner-all" Width="300px" MaxLength="20" />
        </td>
    </tr>
    <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","EMail") %>
        </td>
        <td>
            <asp:TextBox ID="txtEMail" runat="server" CssClass="ui-widget-content ui-corner-all" Width="300px" MaxLength="100" />
        </td>
    </tr>
    <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","MasterPassword") %>
        </td>
        <td>
            <asp:TextBox ID="txtMasterPass" TextMode="Password" runat="server" CssClass="ui-widget-content ui-corner-all" MaxLength="20" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div id="recaptcha" class="g-recaptcha" data-sitekey="6LfAbiMUAAAAAEJ1f57ys7fbsGQwPufL89tBfWQ6" data-callback="onSubmit" data-size="invisible"></div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Literal runat="server" Visible="false" ID="ltrDescription">

            </asp:Literal>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <button type="button" onclick="checkCaptcha()" class="btn btn-success"><%= GetGlobalResourceObject("AdminAgencyControlPage","btnSave") %></button>
            <asp:Button runat="server" ID="btnNewAgency" OnClick="btnNewAgency_Click" CssClass="hide"/>
            
        </td>
    </tr>
</table>
