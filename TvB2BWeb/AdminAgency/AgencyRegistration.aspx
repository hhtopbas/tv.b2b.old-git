﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AgencyRegistration.aspx.cs" Inherits="AdminAgency_AgencyRegistration" %>

<%@ Register Src="~/AdminAgency/NewAgency.ascx" TagPrefix="uc1" TagName="NewAgency" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Agency Registration</title>
    <%--<script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <link href="../CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/confirm.css" rel="stylesheet" type="text/css" />
    <link href="CSS/ControlPage.css" rel="stylesheet" type="text/css" />--%>
    <link href="Assests/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="https://www.google.com/recaptcha/api.js?hl=ru" async="async" defer="defer"></script>

</head>
<body>
    <form id="form1" runat="server">
         <div style="text-align: center; background-color: #BCBCBC">
            <br />
            <span style="font-size: 12pt;"><b>
                <%= GetGlobalResourceObject("AdminAgencyControlPage","PageTitle") %></b></span>
            <br />
            <br />
        </div>
        <br />
        <div style="margin:auto; position:relative; width:600px;">
            <uc1:NewAgency runat="server" ID="NewAgency" />
        </div>
    </form>
</body>
</html>
