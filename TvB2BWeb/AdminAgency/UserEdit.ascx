﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserEdit.ascx.cs" Inherits="AgencyAdmin_UserEdit" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
    
<link href="CSS/ControlPage.css" rel="stylesheet" type="text/css" /> 
<link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />

<asp:HiddenField ID="txtAgencyID" runat="server" />
<asp:HiddenField ID="txtUserID" runat="server" />
<asp:HiddenField ID="txtRecID" runat="server" />
<table cellpadding="2" cellspacing="0">
    <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","UserName") %>
        </td>
        <td>
            <asp:TextBox ID="txtCode" runat="server" CssClass="ui-widget-content ui-corner-all" MaxLength="10" />
        </td>
    </tr>
    <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","Name") %>
        </td>
        <td>
            <asp:TextBox ID="txtName" runat="server" CssClass="ui-widget-content ui-corner-all"
                Width="300px" MaxLength="30" />
        </td>
    </tr>
    <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","Status") %>
        </td>
        <td>
            <asp:CheckBox ID="cbStatus" runat="server" Text="<%$ Resources:AdminAgencyControlPage, StatusEnable %>" />
        </td>
    </tr>

    <tr runat="server" id="trSummerPin">
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","PIN") %>
        </td>
        <td>
            <asp:TextBox ID="txtPIN" runat="server" CssClass="ui-widget-content ui-corner-all" />
        </td>
    </tr>
    <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","Phone") %>
        </td>
        <td>
            <asp:TextBox ID="txtPhone" runat="server" CssClass="ui-widget-content ui-corner-all" MaxLength="30" />
        </td>
    </tr>
    <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","Mobile") %>
        </td>
        <td>
            <asp:TextBox ID="txtMobile" runat="server" CssClass="ui-widget-content ui-corner-all" MaxLength="30" />
        </td>
    </tr>
    <tr>
        <td>
            <%= GetGlobalResourceObject("AdminAgencyControlPage","EMail") %>
        </td>
        <td>
            <asp:TextBox ID="txtEMail" runat="server" CssClass="ui-widget-content ui-corner-all"
                Width="300px" MaxLength="100" />
        </td>
    </tr>
    <tr>
        <td valign="top">
            <%= GetGlobalResourceObject("AdminAgencyControlPage","HomeAddress") %>
        </td>
        <td>
            <asp:TextBox ID="txtHAddress" runat="server" CssClass="ui-widget-content ui-corner-all"
                Height="50px" MaxLength="100" TextMode="MultiLine" Width="300px" />
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <asp:CheckBox ID="cbShowAllRes" runat="server" Text="<%$ Resources:AdminAgencyControlPage, ShowAllResTitle %>" />
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <fieldset title="Access Password">
                <table cellpadding="2" cellspacing="0">
                    <tr>
                        <td>
                            <%= GetGlobalResourceObject("AdminAgencyControlPage","Password") %>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPassword" runat="server" CssClass="ui-widget-content ui-corner-all"
                                TextMode="Password" MaxLength="20"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= GetGlobalResourceObject("AdminAgencyControlPage","ExpireDate") %>
                        </td>
                        <td>
                            <asp:TextBox ID="txtExpDate" runat="server" Width="75px" />
                            <rjs:PopCalendar ID="ppcExpDate" runat="server" Control="txtExpDate" AutoPostBack="False" />
                            &nbsp; &nbsp;
                        </td>
                    </tr>                   
                </table>
            </fieldset>
        </td>
    </tr>
    <%--<tr>
        <td>
            Expedient ID
        </td>
        <td>
            <asp:TextBox ID="txtExpedientID" runat="server" CssClass="ui-widget-content ui-corner-all" />
        </td>
    </tr>--%>
</table>
