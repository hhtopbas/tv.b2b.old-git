﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Threading;
using System.Text;
using System.IO;
using TvBo;
using TvTools;
using System.Globalization;

public partial class CreateOffer : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public static List<TvReport.basketLangRec> getMarketLangList(User UserData)
    {
        string Market = UserData.Market;

        string fileName = HttpContext.Current.Server.MapPath("Data\\" + new UICommon().getWebID().ToString() + "\\BasketMarketLang.xml");
        if (!File.Exists(fileName))
            return null;
        List<TvReport.basketLangRec> list = new List<TvReport.basketLangRec>();
        System.Data.DataTable dt = new System.Data.DataTable();
        dt.ReadXml(fileName);
        foreach (System.Data.DataRow row in dt.Rows)
            list.Add(new TvReport.basketLangRec { Lang = row["Market"].ToString(), Labels = row["Labels"].ToString(), Texts = row["Texts"].ToString() });
        return list;
    }

    public static string getHotelUrl(User UserData, List<SearchResult> searchResult, List<HotelMarOptRecord> hotelMarOpt, string Hotel)
    {
        string errorMsg = string.Empty;
        HotelMarOptRecord hotelUrl = hotelMarOpt.Find(f => f.Hotel == Hotel);
        string _hotelUrl = (hotelUrl != null && !string.IsNullOrEmpty(hotelUrl.InfoWeb)) ? hotelUrl.InfoWeb : "";
        return _hotelUrl;
    }

    public static string getDetailTemplate(User UserData)
    {
        string sb = string.Empty;
        string filePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + new UICommon().getWebID() + "\\OfferTemplate.tmpl";
        if (System.IO.File.Exists(filePath)) {
            System.IO.StreamReader Tex = new System.IO.StreamReader(filePath);
            sb = Tex.ReadToEnd();
            Tex.Close();
        } else {
            filePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + new UICommon().getWebID() + "\\OfferTemplate.tmpl";
            if (System.IO.File.Exists(filePath)) {
                System.IO.StreamReader Tex = new System.IO.StreamReader(filePath);
                sb = Tex.ReadToEnd();
                Tex.Close();
            }
        }
        return sb;
    }

    public static string createResultGridDetail(User UserData, List<SearchResult> offerList)
    {
        StringBuilder sb = new StringBuilder();
        string errorMsg = string.Empty;
        List<TvReport.basketLangRec> list = getMarketLangList(UserData);
        List<HotelMarOptRecord> hotelMarOpt = new Search().getHotelMarOptList(UserData, offerList, ref errorMsg);

        bool? _offerLangIsGlobal = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "offerLangIsGlobal"));
        bool isOfferLangIsGlobal = _offerLangIsGlobal.HasValue && _offerLangIsGlobal.Value ? true : false;

        string lblHotel = list != null && list.Find(f => f.Labels == "lblHotel" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblHotel" && f.Lang == UserData.Market).Texts : "Hotel";
        string lblCheckIn = list != null && list.Find(f => f.Labels == "lblCheckIn" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblCheckIn" && f.Lang == UserData.Market).Texts : "Check/in";
        string lblDuration = list != null && list.Find(f => f.Labels == "lblDuration" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblDuration" && f.Lang == UserData.Market).Texts : "Night";

        string lblRoomType = list != null && list.Find(f => f.Labels == "lblRoomType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblRoomType" && f.Lang == UserData.Market).Texts : "Room";
        string lblBoardType = list != null && list.Find(f => f.Labels == "lblBoardType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblBoardType" && f.Lang == UserData.Market).Texts : "Board";
        string lblAccom = list != null && list.Find(f => f.Labels == "lblAccom" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblAccom" && f.Lang == UserData.Market).Texts : "Accommodation";
        string lblPrice = list != null && list.Find(f => f.Labels == "lblPrice" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblPrice" && f.Lang == UserData.Market).Texts : "Price";
        string lblComment = list != null && list.Find(f => f.Labels == "lblComment" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblComment" && f.Lang == UserData.Market).Texts : "Agency comment";
        string lblTransport = list != null && list.Find(f => f.Labels == "lblTransport" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblTransport" && f.Lang == UserData.Market).Texts : "Transport";
        string lblTransportType = list != null && list.Find(f => f.Labels == "lblTransportType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblTransportType" && f.Lang == UserData.Market).Texts : "Charter flight";
        string lblTransportBusType = list != null && list.Find(f => f.Labels == "lblTransportBusType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblTransportBusType" && f.Lang == UserData.Market).Texts : "Charter bus";

        if (isOfferLangIsGlobal)
        {
            lblHotel = HttpContext.GetGlobalResourceObject("CreateOffer", "lblHotel").ToString();
            lblCheckIn = HttpContext.GetGlobalResourceObject("CreateOffer", "lblCheckIn").ToString();
            lblComment= HttpContext.GetGlobalResourceObject("CreateOffer", "lblComment").ToString();
            lblDuration = HttpContext.GetGlobalResourceObject("CreateOffer", "lblDuration").ToString();
            lblRoomType = HttpContext.GetGlobalResourceObject("CreateOffer", "lblRoomType").ToString();
            lblBoardType = HttpContext.GetGlobalResourceObject("CreateOffer", "lblBoardType").ToString();
            lblAccom = HttpContext.GetGlobalResourceObject("CreateOffer", "lblAccom").ToString();
            lblPrice = HttpContext.GetGlobalResourceObject("CreateOffer", "lblPrice").ToString();
            lblComment = HttpContext.GetGlobalResourceObject("CreateOffer", "lblComment").ToString();
            lblTransport = HttpContext.GetGlobalResourceObject("CreateOffer", "lblTransport").ToString();
            lblTransportType = HttpContext.GetGlobalResourceObject("CreateOffer", "lblTransportType").ToString();
        }
        int i = 0;
        foreach (SearchResult row in offerList) {
            i++;
            sb.Append(" <div class=\"clearDiv\"></div>");
            sb.AppendFormat("   <div id=\"RefNo_{0}\" class=\"hotelInfo\">", row.RefNo);
            sb.AppendFormat("     <img alt=\"{1}\" title=\"{1}\" src=\"Images/cancel.png\" onclick=\"removeList({0});\" class=\"pdfShow noPrint\" /><br />", row.RefNo, "Remove list");
            sb.AppendFormat("     <span class=\"font11ptBold\">{0}</span>", row.DepCityName + " - " + row.ArrCityName + " - " + row.DepCityName);
            sb.Append("     <br />");
            sb.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptBoldItalic\">{1}</span>&nbsp; <span class=\"font10ptItalic\">( {2} )</span>",
                        lblHotel,
                        row.HotelName + " ( " + row.HotCat + " ) ",
                        row.HotLocationName);
            sb.Append("     <br />");
            string hotelWWW = string.Empty;
            hotelWWW = getHotelUrl(UserData, offerList, hotelMarOpt, row.Hotel);
            if (!string.IsNullOrEmpty(hotelWWW)) {
                sb.AppendFormat("     <span class=\"font10ptItalic\"><a href='{0}' target='_blank'>{0}</a></span>", hotelWWW);
                sb.Append("     <br />");
            }
            sb.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptItalic\">{1}</span>&nbsp;",
                        lblCheckIn,
                        row.CheckIn.HasValue ? row.CheckIn.Value.ToShortDateString() : "");
            sb.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptItalic\">{1}</span>",
                        lblDuration,
                        row.Night.HasValue ? row.Night.Value.ToString() : "");
            sb.Append("     <br />");
            sb.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptItalic\">{1}</span>&nbsp;&nbsp;",
                        lblRoomType,
                        row.RoomName);
            sb.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptItalic\">{1}</span>",
                        lblBoardType,
                        row.BoardName);
            sb.Append("     <br />");
            sb.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptItalic\">{1}</span>",
                        lblAccom,
                        row.AccomFullName);
            sb.Append("     <br />");

            bool transport = !string.IsNullOrEmpty(row.DFlight) || string.Equals(row.DepServiceType, "TRANSPORT");

            sb.AppendFormat("     <div class=\"{0}\">",
                        !transport ? "display: none;" : "display: block;");
            sb.AppendFormat("       <span class=\"font9pt\">{0}</span> : <span class=\"font10ptItalic\">{1}</span>",
                        lblTransport,
                        string.Equals(row.DepServiceType, "TRANSPORT") ? lblTransportBusType : lblTransportType);
            sb.Append("       <br />");
            sb.Append("     </div>");

            sb.AppendFormat("     <span class=\"font11ptFont\">{0} : </span><span class=\"font11ptBoldItalic\">{1}</span>",
                        lblPrice,
                        row.LastPrice.HasValue ? row.LastPrice.Value.ToString("#,###.00") + " " + row.SaleCur : "");
            sb.Append("     <br />");
            sb.AppendFormat("     <span class=\"font9pt\">{0} : </span>", lblComment);
            sb.Append("     <br />");
            sb.AppendFormat("     <input id=\"agencyNote{0}\" type=\"text\" class=\"noteInput\" />", i.ToString());
            sb.Append("   </div>");
        }
        return sb.ToString();
    }

    public static object getObject(SearchResult row, string fieldName)
    {
        System.Reflection.PropertyInfo[] oProps = null;
        oProps = row.GetType().GetProperties();
        List<System.Reflection.PropertyInfo> _pi = (from q in oProps
                                                    where q.Name == fieldName
                                                    select q).ToList<System.Reflection.PropertyInfo>();
        object value = null;
        if (_pi != null && _pi.Count() > 0) {
            System.Reflection.PropertyInfo pi = _pi[0];
            value = pi.GetValue(row, null) == null ? DBNull.Value : pi.GetValue(row, null);
        }
        return value;
    }

    public static string createGridDetail(User UserData, List<SearchResult> offerList)
    {
        string errorMsg = string.Empty;
        string tmpl = getDetailTemplate(UserData);
        int lastPosition = 0;
        if (string.IsNullOrEmpty(tmpl))
            return createResultGridDetail(UserData, offerList);
        string retVal = string.Empty;
        string tmpTemplate = tmpl;

        List<TvReport.basketLangRec> list = getMarketLangList(UserData);
        List<HotelMarOptRecord> hotelMarOpt = new Search().getHotelMarOptList(UserData, offerList, ref errorMsg);
        bool? _offerLangIsGlobal = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "offerLangIsGlobal"));
        bool isOfferLangIsGlobal = _offerLangIsGlobal.HasValue && _offerLangIsGlobal.Value ? true : false;


        string lblHotel = list != null && list.Find(f => f.Labels == "lblHotel" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblHotel" && f.Lang == UserData.Market).Texts : "Hotel";
        string lblCheckIn = list != null && list.Find(f => f.Labels == "lblCheckIn" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblCheckIn" && f.Lang == UserData.Market).Texts : "Check/in";
        string lblDuration = list != null && list.Find(f => f.Labels == "lblDuration" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblDuration" && f.Lang == UserData.Market).Texts : "Night";

        string lblRoomType = list != null && list.Find(f => f.Labels == "lblRoomType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblRoomType" && f.Lang == UserData.Market).Texts : "Room";
        string lblBoardType = list != null && list.Find(f => f.Labels == "lblBoardType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblBoardType" && f.Lang == UserData.Market).Texts : "Board";
        string lblAccom = list != null && list.Find(f => f.Labels == "lblAccom" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblAccom" && f.Lang == UserData.Market).Texts : "Accommodation";
        string lblPrice = list != null && list.Find(f => f.Labels == "lblPrice" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblPrice" && f.Lang == UserData.Market).Texts : "Price";
        string lblComment = list != null && list.Find(f => f.Labels == "lblComment" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblComment" && f.Lang == UserData.Market).Texts : "Agency comment";
        string lblTransport = list != null && list.Find(f => f.Labels == "lblTransport" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblTransport" && f.Lang == UserData.Market).Texts : "Transport";
        string lblTransportType = list != null && list.Find(f => f.Labels == "lblTransportType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblTransportType" && f.Lang == UserData.Market).Texts : "Charter flight";
        string lblTransportBusType = list != null && list.Find(f => f.Labels == "lblTransportBusType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblTransportBusType" && f.Lang == UserData.Market).Texts : "Charter bus";

        string lblDateOfValidity = list != null && list.Find(f => f.Labels == "lblDateOfValidity" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblDateOfValidity" && f.Lang == UserData.Market).Texts : "Date of validity";

        if (isOfferLangIsGlobal)
        {
            lblHotel = HttpContext.GetGlobalResourceObject("CreateOffer", "lblHotel").ToString();
            lblCheckIn = HttpContext.GetGlobalResourceObject("CreateOffer", "lblCheckIn").ToString();
            lblDuration = HttpContext.GetGlobalResourceObject("CreateOffer", "lblDuration").ToString();
            lblRoomType = HttpContext.GetGlobalResourceObject("CreateOffer", "lblRoomType").ToString();
            lblBoardType = HttpContext.GetGlobalResourceObject("CreateOffer", "lblBoardType").ToString();
            lblAccom = HttpContext.GetGlobalResourceObject("CreateOffer", "lblAccom").ToString();
            lblPrice = HttpContext.GetGlobalResourceObject("CreateOffer", "lblPrice").ToString();
            lblComment = HttpContext.GetGlobalResourceObject("CreateOffer", "lblComment").ToString();
            lblTransport = HttpContext.GetGlobalResourceObject("CreateOffer", "lblTransport").ToString();
            lblTransportType = HttpContext.GetGlobalResourceObject("CreateOffer", "lblTransportType").ToString();
        }

        bool exit = true;
        while (exit) {
            string valueTemp = string.Empty;
            int first = tmpTemplate.IndexOf("[[");
            if (first > -1) {
                lastPosition = first;
                tmpTemplate = tmpTemplate.Remove(0, first + 2);
                int last = tmpTemplate.IndexOf("]]");
                valueTemp = tmpTemplate.Substring(0, last);
                tmpTemplate = tmpTemplate.Remove(0, last + 2);
                string[] local = valueTemp.Trim().Split(',');
                if (local.Length > 0) {
                    string classKey = local[0].Trim('\"');
                    string localStr = string.Empty;
                    switch (classKey) {
                        case "lblHotel":
                            localStr = lblHotel;
                            break;
                        case "lblCheckIn":
                            localStr = lblCheckIn;
                            break;
                        case "lblRoomType":
                            localStr = lblRoomType;
                            break;
                        case "lblBoardType":
                            localStr = lblBoardType;
                            break;
                        case "lblAccom":
                            localStr = lblAccom;
                            break;
                        case "lblPrice":
                            localStr = lblPrice;
                            break;
                        case "lblComment":
                            localStr = lblComment;
                            break;
                        case "lblTransport":
                            localStr = lblTransport;
                            break;
                        case "lblTransportType":
                            localStr = "[[lblTransportType]]";
                            break;
                        case "lblDateOfValidity":
                            localStr = lblDateOfValidity;
                            break;
                    }
                    tmpl = tmpl.Replace("[[" + valueTemp + "]]", localStr);
                }
            } else
                exit = false;
        }

        exit = true;
        tmpTemplate = tmpl;
        while (exit) {
            string valueTemp = string.Empty;
            int first = tmpTemplate.IndexOf("{[");
            if (first > -1) {
                lastPosition = first;
                tmpTemplate = tmpTemplate.Remove(0, first + 2);
                int last = tmpTemplate.IndexOf("]}");
                valueTemp = tmpTemplate.Substring(0, last);
                tmpTemplate = tmpTemplate.Remove(0, last + 2);
                string[] local = valueTemp.Trim().Split(',');
                if (local.Length > 1) {
                    string classKey = local[0].Trim('\"');
                    string resourceKey = local[1].Trim('\"');
                    string localStr = HttpContext.GetGlobalResourceObject(classKey, resourceKey).ToString();
                    tmpl = tmpl.Replace("{[" + valueTemp + "]}", localStr);
                }
            } else
                exit = false;
        }
        tmpTemplate = tmpl;

        string transportType = string.Empty;

        string tmpLoopSection = tmpl.Substring(tmpl.IndexOf("{loop"));
        tmpLoopSection = tmpLoopSection.Substring(5, tmpLoopSection.IndexOf("loop}") - 5);
        tmpTemplate = tmpLoopSection;
        string loopSection = string.Empty;
        Int16 cnt = 0;

        foreach (SearchResult row in offerList) {
            tmpTemplate = tmpLoopSection;
            cnt += 1;
            string _tmplA = tmpLoopSection;
            string tmplA = tmpLoopSection;
            exit = true;
            while (exit) {
                string valueTemp = string.Empty;
                string valueFormat = string.Empty;
                int first = tmpTemplate.IndexOf("{");
                if (first > -1) {
                    lastPosition = first;
                    tmpTemplate = tmpTemplate.Remove(0, first + 1);
                    int last = tmpTemplate.IndexOf("}");
                    valueTemp = tmpTemplate.Substring(0, last);
                    if (valueTemp.Split('|').Length > 1) {
                        valueFormat = valueTemp.Split('|')[1];
                        valueTemp = valueTemp.Split('|')[0];
                    }
                    valueFormat = valueFormat.Trim('\"');
                    valueTemp = valueTemp.Trim('\"');
                    tmpTemplate = tmpTemplate.Remove(0, last + 1);

                    string valueStr = string.Empty;
                    if (string.Equals(valueTemp, "InfoWeb")) {
                        string hotelWWW = getHotelUrl(UserData, offerList, hotelMarOpt, row.Hotel);
                        if (!string.IsNullOrEmpty(hotelWWW)) {
                            valueStr = hotelWWW;
                        }
                    } else if (string.Equals(valueTemp, "visableTransportType")) {
                        bool transport = (!string.IsNullOrEmpty(row.DFlight) || string.Equals(row.DepServiceType, "TRANSPORT") || string.Equals(row.DepServiceType, "FLIGHT"));
                        if (string.IsNullOrEmpty(row.DepServiceType) && string.IsNullOrEmpty(row.RetServiceType)) {
                            transport = false;
                        }
                        valueStr = transport ? "display: block;" : "display: none";
                        tmplA = tmplA.Replace("[[lblTransportType]]", string.Equals(row.DepServiceType, "TRANSPORT") ? lblTransportBusType : lblTransportType);
                    } else if (valueTemp.IndexOf('.') > -1) {
                        if (Equals(valueTemp, "UserData.Ci.LCID"))
                            valueStr = UserData.Ci.LCID.ToString();
                    } else {
                        if (valueTemp.IndexOf('-') > -1) {
                            string[] valuelist = valueTemp.Split('-');
                            List<TvTools.objectList> objectValues = new List<objectList>();
                            Type _type = typeof(System.String);
                            for (int i = 0; i < valuelist.Length; i++) {
                                object value = getObject(row, valuelist[i]);
                                objectValues.Add(new TvTools.objectList {
                                    TypeName = value.GetType().Name,
                                    Value = value
                                });
                            }
                            object obj = mathLib.returnFormulaMinus(objectValues, ref _type);
                            valueStr = Conversion.getObjectToString(obj, _type, valueFormat);
                        } else
                            if (valueTemp.IndexOf('+') > -1) {
                            string[] valuelist = valueTemp.Split('+');
                            List<TvTools.objectList> objectValues = new List<objectList>();
                            Type _type = typeof(System.String);
                            for (int i = 0; i < valuelist.Length; i++) {
                                object value = getObject(row, valuelist[i]);
                                objectValues.Add(new TvTools.objectList {
                                    TypeName = value.GetType().Name,
                                    Value = value
                                });
                            }
                            object obj = mathLib.returnFormulaPlus(objectValues, ref _type);
                            valueStr = Conversion.getObjectToString(obj, _type, valueFormat);
                        } else
                                if (valueTemp.IndexOf("DateOfValidity") > -1) {
                            DateTime? ValidDate = DateTime.MaxValue;
                            if (row.ValidDate.HasValue)
                                ValidDate = row.ValidDate.Value;
                            if (row.EBValidDate.HasValue && row.EBValidDate.Value < ValidDate.Value)
                                ValidDate = row.EBValidDate.Value;
                            if (row.SaleEndDate.HasValue && row.SaleEndDate.Value < ValidDate.Value)
                                ValidDate = row.SaleEndDate.Value;
                            if (row.FlightValidDate.HasValue && row.FlightValidDate.Value < ValidDate)
                                ValidDate = row.FlightValidDate.Value;
                            if (row.HotelValidDate.HasValue && row.HotelValidDate.Value < ValidDate)
                                ValidDate = row.HotelValidDate.Value;
                            if (row.PEB_SaleEndDate.HasValue && row.PEB_SaleEndDate.Value < ValidDate)
                                ValidDate = row.PEB_SaleEndDate.Value;
                            valueStr = ValidDate.Value.ToShortDateString();
                        } else {
                            object value = getObject(row, valueTemp);
                            valueStr = Conversion.getObjectToString(value, value.GetType(), valueFormat);
                        }
                        tmplA = tmplA.Replace("{\"" + valueTemp + (valueFormat.Length > 0 ? "\"|\"" + valueFormat : "") + "\"}", valueStr);
                    }
                    tmplA = tmplA.Replace("{\"" + valueTemp + (valueFormat.Length > 0 ? "\"|\"" + valueFormat : "") + "\"}", valueStr);
                } else {
                    exit = false;
                }
            }
            loopSection += tmplA;
        }
        tmpl = tmpl.Replace("{loop" + tmpLoopSection + "loop}", loopSection);

        return tmpl;
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData(int? removeOffer)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        List<SearchResult> offerList = new List<SearchResult>();
        if (HttpContext.Current.Session["OfferList"] == null)
            return "";
        offerList = (List<SearchResult>)HttpContext.Current.Session["OfferList"];

        if (removeOffer.HasValue) {
            offerList.Remove(offerList.Find(f => f.RefNo == removeOffer));
            HttpContext.Current.Session["OfferList"] = offerList;
        }

        bool? _offerLangIsGlobal = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "offerLangIsGlobal"));
        bool isOfferLangIsGlobal = _offerLangIsGlobal.HasValue && _offerLangIsGlobal.Value ? true : false;



        List<HotelMarOptRecord> hotelMarOpt = new Search().getHotelMarOptList(UserData, offerList, ref errorMsg);
        List<TvReport.basketLangRec> list = getMarketLangList(UserData);
        AgencyRecord agency = UserData.AgencyRec;
        StringBuilder sb = new StringBuilder();
        string operatorLogoStr = CacheObjects.getOperatorLogoUrl(UserData.Operator);
        byte[] agencyLogo = new TvReport.AllOperator().getLogo("LogoSmall", "Agency", "Code", UserData.MainAgency ? UserData.MainOffice : UserData.AgencyID, null, ref errorMsg);
        string agencyLogoStr = agencyLogo != null ? TvBo.UICommon.WriteBinaryImage(agencyLogo, UserData.AgencyID, "AG_", "", "") : "";
        string userPhone = new TvReport.AllOperator().getAgencyUserPhone(UserData.AgencyID, UserData.UserID, ref errorMsg);
        FixNotesRecord fixNote = new TvBo.Common().getFixNotes(UserData.Market, "PriceList", ref errorMsg);
        string fixNoteText = string.Empty;
        if (fixNote != null) {
            //SautinSoft.RtfToHtml rtfHtml = new SautinSoft.RtfToHtml();
            //rtfHtml.Encoding = SautinSoft.eEncoding.UTF_8;
            //rtfHtml.OutputFormat = SautinSoft.eOutputFormat.HTML_401;
            //rtfHtml.HtmlParts = SautinSoft.eHtmlParts.Html_body;
            //fixNoteText = rtfHtml.ConvertString(fixNote.Note);
            //TvTools.RTFtoHTML rtf = new RTFtoHTML();
            //rtf.rtf = fixNote.Note;
            //fixNoteText = rtf.html();
        }
        string createdBy = list != null && list.Find(f => f.Labels == "Header1" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "Header1" && f.Lang == UserData.Market).Texts : "Created ";

        string lblHotel = list != null && list.Find(f => f.Labels == "lblHotel" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblHotel" && f.Lang == UserData.Market).Texts : "Hotel";
        string lblCheckIn = list != null && list.Find(f => f.Labels == "lblCheckIn" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblCheckIn" && f.Lang == UserData.Market).Texts : "Check/in";
        string lblDuration = list != null && list.Find(f => f.Labels == "lblDuration" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblDuration" && f.Lang == UserData.Market).Texts : "Night";

        string lblRoomType = list != null && list.Find(f => f.Labels == "lblRoomType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblRoomType" && f.Lang == UserData.Market).Texts : "Room";
        string lblBoardType = list != null && list.Find(f => f.Labels == "lblBoardType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblBoardType" && f.Lang == UserData.Market).Texts : "Board";
        string lblAccom = list != null && list.Find(f => f.Labels == "lblAccom" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblAccom" && f.Lang == UserData.Market).Texts : "Accommodation";
        string lblPrice = list != null && list.Find(f => f.Labels == "lblPrice" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblPrice" && f.Lang == UserData.Market).Texts : "Price";
        string lblComment = list != null && list.Find(f => f.Labels == "lblComment" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblComment" && f.Lang == UserData.Market).Texts : "Agency comment";
        string lblTransport = list != null && list.Find(f => f.Labels == "lblTransport" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblTransport" && f.Lang == UserData.Market).Texts : "Transport";
        string lblTransportType = list != null && list.Find(f => f.Labels == "lblTransportType" && f.Lang == UserData.Market) != null ? list.Find(f => f.Labels == "lblTransportType" && f.Lang == UserData.Market).Texts : "Charter flight";
        if (isOfferLangIsGlobal)
        {
            createdBy= HttpContext.GetGlobalResourceObject("CreateOffer", "Created").ToString();
            lblHotel= HttpContext.GetGlobalResourceObject("CreateOffer", "lblHotel").ToString();
            lblCheckIn= HttpContext.GetGlobalResourceObject("CreateOffer", "lblCheckIn").ToString();
            lblDuration= HttpContext.GetGlobalResourceObject("CreateOffer", "lblDuration").ToString();
            lblRoomType = HttpContext.GetGlobalResourceObject("CreateOffer", "lblRoomType").ToString();
            lblBoardType= HttpContext.GetGlobalResourceObject("CreateOffer", "lblBoardType").ToString();
            lblAccom= HttpContext.GetGlobalResourceObject("CreateOffer", "lblAccom").ToString();
            lblPrice= HttpContext.GetGlobalResourceObject("CreateOffer", "lblPrice").ToString();
            lblComment= HttpContext.GetGlobalResourceObject("CreateOffer", "lblComment").ToString();
            lblTransport= HttpContext.GetGlobalResourceObject("CreateOffer", "lblTransport").ToString();
            lblTransportType= HttpContext.GetGlobalResourceObject("CreateOffer", "lblTransportType").ToString();
        }
        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro))
            operatorLogoStr = "";
        htmlData.Add(new TvReport.htmlCodeData { IdName = "imgOperator", TagName = "img", Data = operatorLogoStr });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "imgAgency", TagName = "img", Data = agencyLogoStr });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtCreatedBy", TagName = "span", Data = createdBy });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAgencyName", TagName = "span", Data = string.IsNullOrEmpty(agency.FirmName) ? agency.Name : agency.FirmName });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtUserName", TagName = "span", Data = UserData.UserName });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAgencyTel", TagName = "span", Data = userPhone });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtEmail", TagName = "span", Data = UserData.EMail });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtDateTime", TagName = "span", Data = DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "fixNoteText", TagName = "div", Data = fixNoteText });


        #region 2012-04-30 template design
        /*
        int i = 0;
        foreach (SearchResult row in offerList)
        {
            i++;
            sb.Append(" <div class=\"clearDiv\"></div>");
            sb.AppendFormat("   <div id=\"RefNo_{0}\" class=\"hotelInfo\">", row.RefNo);
            sb.AppendFormat("     <img alt=\"{1}\" title=\"{1}\" src=\"Images/cancel.png\" onclick=\"removeList({0});\"/><br />", row.RefNo, "Remove list");
            
            sb.AppendFormat("     <span class=\"font11ptBold\">{0}</span>", row.DepCityName + " - " + row.ArrCityName + " - " + row.DepCityName);            sb.AppendFormat("     <span class=\"font11ptBold\">{0}</span>", row.DepCityName + " - " + row.ArrCityName + " - " + row.DepCityName);
            sb.Append("     <br />");
            sb.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptBoldItalic\">{1}</span>&nbsp; <span class=\"font10ptItalic\">( {2} )</span>",
                       
        lblHotel,
                        row.HotelName + " ( " + row.HotCat + " ) ",
                        row.HotLocationName);
            sb.Append("     <br />");
            string hotelWWW = string.Empty;
            hotelWWW = getHotelUrl(UserData, offerList, hotelMarOpt, row.Hotel);
            if (!string.IsNullOrEmpty(hotelWWW))
            {
                sb.AppendFormat("     <span class=\"font10ptItalic\">{0}</span>", hotelWWW);
                sb.Append("     <br />");
            }
            sb.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptItalic\">{1}</span>&nbsp;",
                        lblCheckIn,
                        row.CheckIn.HasValue ? row.CheckIn.Value.ToShortDateString() : "");
            sb.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptItalic\">{1}</span>",
                        lblDuration,
                        row.Night.HasValue ? row.Night.Value.ToString() : "");
            sb.Append("     <br />");
            sb.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptItalic\">{1}</span>&nbsp;&nbsp;",
                        lblRoomType,
                        row.RoomName);
            sb.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptItalic\">{1}</span>",
                        lblBoardType,
                        row.BoardName);
            sb.Append("     <br />");
            sb.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptItalic\">{1}</span>",
                        lblAccom,
                        row.AccomFullName);
            sb.Append("     <br />");
            sb.AppendFormat("     <span class=\"font11ptFont\">{0} : </span><span class=\"font11ptBoldItalic\">{1}</span>",
                        lblPrice,
                        row.LastPrice.HasValue ? row.LastPrice.Value.ToString("#,###.00") + " " + row.SaleCur : "");
            sb.Append("     <br />");
            sb.AppendFormat("     <span class=\"font9pt\">{0} : </span>", lblComment);
            sb.Append("     <br />");
            sb.AppendFormat("     <input id=\"agencyNote{0}\" type=\"text\" class=\"noteInput\" />", i.ToString());
            sb.Append("   </div>");
        }

        htmlData.Add(new TvReport.htmlCodeData { IdName = "dataDiv", TagName = "div", Data = sb.ToString().Replace('"', '!') });
        */
        #endregion

        htmlData.Add(new TvReport.htmlCodeData { IdName = "dataDiv", TagName = "div", Data = createGridDetail(UserData, offerList).Replace('"', '!') });
        return Newtonsoft.Json.JsonConvert.SerializeObject(htmlData);
    }
}
