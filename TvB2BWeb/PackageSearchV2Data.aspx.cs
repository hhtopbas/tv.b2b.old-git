﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Threading;
using System.Web.Script.Serialization;
using System.Collections;
using System.Text;
using TvTools;

public partial class PackageSearchV2Data : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScriptSerializer ser = new JavaScriptSerializer();
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        int iDisplayLength = Convert.ToInt32(HttpContext.Current.Request["iDisplayLength"]);
        int iDisplayStart = Convert.ToInt32(HttpContext.Current.Request["iDisplayStart"]);
        int iEcho = Convert.ToInt32(HttpContext.Current.Request["sEcho"]);

        if (iEcho == 1) iDisplayStart = 0;

        List<SearchResult> allSearchRecord = ReadFile();
        IQueryable<SearchResult> allRecords = allSearchRecord.AsQueryable();

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        IQueryable<SearchResult> filteredRecords = allRecords;
        filterDataRecord filterData = new filterDataRecord();

        filterData.fltOprTextSelected = "";
        filterData.fltHotelSelected = "";
        filterData.fltCategorySelected = "";
        filterData.fltRoomSelected = "";
        filterData.fltLocationSelected = "";
        filterData.fltCheckInSelected = "";
        filterData.fltNightsSelected = "";
        filterData.fltBoardSelected = "";
        filterData.fltAccomSelected = "";
        filterData.fltDepartureSelected = "";
        filterData.fltReturnSelected = "";

        List<requestFormData> requestForm = new List<requestFormData>();
        for (int i = 0; i < Request.Form.Count; i++) requestForm.Add(new requestFormData { Name = Request.Form.GetKey(i), Value = Request[Request.Form.GetKey(i)] });
        var filters = from q in requestForm
                      where !string.IsNullOrEmpty(q.Value)
                      select q;

        foreach (var row in filters)
        {
            switch (row.Name)
            {
                case "OprText": filterData.fltOprTextSelected = row.Value; break;
                case "Hotel": filterData.fltHotelSelected = row.Value; break;
                case "Category": filterData.fltCategorySelected = row.Value; break;
                case "Room": filterData.fltRoomSelected = row.Value; break;
                case "Location": filterData.fltLocationSelected = row.Value; break;
                case "CheckIn": filterData.fltCheckInSelected = row.Value; break;
                case "Nights": filterData.fltNightsSelected = row.Value; break;
                case "Board": filterData.fltBoardSelected = row.Value; break;
                case "Accom": filterData.fltAccomSelected = row.Value; break;
                case "BeginPrice": filterData.fltPriceBegin = Conversion.getDecimalOrNull(row.Value); break;
                case "EndPrice": filterData.fltPriceEnd = Conversion.getDecimalOrNull(row.Value); break;
                case "Departure": filterData.fltDepartureSelected = row.Value; break;
                case "Arrival": filterData.fltReturnSelected = row.Value; break;
            }
        }
        filteredRecords = filteredRecords
                          .Where(w => string.IsNullOrEmpty(filterData.fltOprTextSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.OprText.TrimEnd(' ') : w.OprText.TrimEnd(' ')), filterData.fltOprTextSelected.TrimEnd(' ')))
                          .Where(w => string.IsNullOrEmpty(filterData.fltHotelSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotelNameL.TrimEnd(' ') : w.HotelName.TrimEnd(' ')), filterData.fltHotelSelected.TrimEnd(' ')))
                          .Where(w => string.IsNullOrEmpty(filterData.fltCategorySelected.TrimEnd(' ')) || string.Equals(w.HotCat.TrimEnd(' '), filterData.fltCategorySelected.TrimEnd(' ')))
                          .Where(w => string.IsNullOrEmpty(filterData.fltRoomSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.RoomNameL.TrimEnd(' ') : w.RoomName.TrimEnd(' ')), filterData.fltRoomSelected.TrimEnd(' ')))
                          .Where(w => string.IsNullOrEmpty(filterData.fltLocationSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotLocationNameL.TrimEnd(' ') : w.HotLocationName.TrimEnd(' ')), filterData.fltLocationSelected.TrimEnd(' ')))
                          .Where(w => string.IsNullOrEmpty(filterData.fltCheckInSelected.TrimEnd(' ')) || string.Equals(w.CheckIn, Conversion.getDateTimeOrNull(filterData.fltCheckInSelected.TrimEnd(' '))))
                          .Where(w => string.IsNullOrEmpty(filterData.fltNightsSelected.TrimEnd(' ')) || string.Equals(w.Night.ToString().TrimEnd(' '), filterData.fltNightsSelected.TrimEnd(' ')))
                          .Where(w => string.IsNullOrEmpty(filterData.fltBoardSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.BoardNameL.TrimEnd(' ') : w.BoardName.TrimEnd(' ')), filterData.fltBoardSelected.TrimEnd(' ')))
                          .Where(w => string.IsNullOrEmpty(filterData.fltAccomSelected.TrimEnd(' ')) || string.Equals(w.AccomFullName.TrimEnd(' '), filterData.fltAccomSelected.TrimEnd(' ')))
                          .Where(w => !filterData.fltPriceBegin.HasValue || (filterData.fltPriceBegin.Value <= w.LastPrice))
                          .Where(w => !filterData.fltPriceEnd.HasValue || (filterData.fltPriceEnd.Value >= w.LastPrice))
                          .Where(w => string.IsNullOrEmpty(filterData.fltDepartureSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.DepCityNameL.TrimEnd(' ') : w.DepCityName.TrimEnd(' ')), filterData.fltDepartureSelected.TrimEnd(' ')))
                          .Where(w => string.IsNullOrEmpty(filterData.fltReturnSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.ArrCityNameL.TrimEnd(' ') : w.ArrCityName.TrimEnd(' ')), filterData.fltReturnSelected.TrimEnd(' ')));


        string sortingStr = string.Empty;
        var iSortingColsQ = from q in requestForm
                            where q.Name == "iSortingCols"
                            select q.Value;
        Int16? iSortingCols = Conversion.getInt16OrNull(iSortingColsQ.FirstOrDefault());
        string sortType = "asc";
        if (iSortingCols.HasValue)
        {
            for (int i = 0; i < iSortingCols.Value; i++)
            {
                var iSortCol = (from q in requestForm
                                where q.Name == "iSortCol_" + i
                                select q.Value).FirstOrDefault();
                string sSortDir = (from q in requestForm
                                   where q.Name == "sSortDir_" + i
                                   select q.Value).FirstOrDefault();
                string bSortable = (from q in requestForm
                                    where q.Name == "bSortable_" + iSortCol
                                    select q.Value).FirstOrDefault();
                if (string.Equals(bSortable, "true"))
                {
                    sortType = sSortDir;
                    if (sortingStr.Length > 0) sortingStr += ", ";
                    switch (iSortCol)
                    {
                        case "2": sortingStr += "OprText" + " " + sSortDir; break;
                        case "3": sortingStr += (useLocalName ? "HotelNameL" : "HotelName"); break;
                        case "5": sortingStr += (useLocalName ? "HotLocationNameL" : "HotLocationName"); break;
                        case "7": sortingStr += "CheckIn"; break;
                        case "8": sortingStr += (useLocalName ? "BoardNameL" : "BoardName"); break;
                        case "10": sortingStr += "LastPrice"; break;
                        case "12": sortingStr += (useLocalName ? "DepCityNameL" : "DepCityName"); break;
                        case "13": sortingStr += (useLocalName ? "ArrCityNameL" : "ArrCityName"); break;
                    }
                }
            }
        }

        //Flight Details
        Int16 flightSeat = 10;
        if (UserData.TvParams.TvParamFlight.DispMinFlightAllotW.HasValue)
            flightSeat = UserData.TvParams.TvParamFlight.DispMinFlightAllotW.Value;
        //Flight Details

        if (sortingStr.Length > 0)
        {
            System.Reflection.PropertyInfo prop = typeof(SearchResult).GetProperty(sortingStr);
            if (sortType == "asc")
                filteredRecords = filteredRecords.OrderBy(x => prop.GetValue(x, null));
            else
                filteredRecords = filteredRecords.OrderByDescending(x => prop.GetValue(x, null));
            //filteredRecords = filteredRecords.OrderBy(sortingStr);
        }

        IQueryable<SearchResult> returnData = filteredRecords.Skip(iDisplayStart).Take(iDisplayLength);
        returnData = new Search().getRoomSeatStopSaleControl(UserData, returnData.ToList<SearchResult>(), ref errorMsg).AsQueryable();
        saveFile(allSearchRecord, returnData.AsQueryable());
        List<BrochureCheckRecord> brochureCheckList = new List<BrochureCheckRecord>();
        var query = from q in returnData
                    group q by new { q.Market, q.HolPack, q.CheckIn, q.CheckOut } into k
                    select new { k.Key.Market, k.Key.HolPack, k.Key.CheckIn, k.Key.CheckOut };
        foreach (var row in query)
        {
            List<holPackBrochureReady> getholPackBrochureReadyList = new TvBo.Common().getholPackBrochureReady(row.Market, row.CheckIn, row.CheckOut, ref errorMsg);
            if (getholPackBrochureReadyList != null)
                foreach (holPackBrochureReady r1 in getholPackBrochureReadyList)
                {
                    brochureCheckList.Add(new BrochureCheckRecord
                    {
                        Code = r1.HolPack,
                        Name = r1.HolPackName,
                        NameL = r1.HolPackNameL,
                        Brochure = r1.brochureReady
                    });
                }
        }

        var retval = from q in returnData
                     select new
                     {
                         Book = getBookButton(UserData, q, useLocalName, brochureCheckList, ref errorMsg),
                         AddOffer = (q.StopSaleStd == 2 && q.StopSaleGuar == 2) || (q.AutoStop.HasValue && q.AutoStop.Value && q.FreeRoom <= 0) ?
                                "&nbsp;"
                                :
                                string.Format("<img alt=\"{1}\" title=\"{1}\" src=\"Images/ClipbordAdd.png\" onclick=\"addOffer({0})\" class=\"bookStyle\"/>",
                                                q.RefNo,
                                                HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "Offer")),
                         OprText = getOprText(UserData, q, useLocalName),
                         Hotel = getHotel(UserData, q, useLocalName),
                         FreeRoom = getFreeRoom(UserData, q, useLocalName),
                         Location = string.Format("<span style=\"font-size: 8pt;\">{0}</span><br /><span style=\"font-weight: bold;\">{1}</span>", useLocalName ? q.HotLocationNameL : q.HotLocationName, useLocalName ? q.ArrCityNameL : q.ArrCityName),
                         CheckIn = string.Format("{0}<br /><span style=\"font-size: 7pt;\">{1}</span>", q.CheckIn.HasValue ? q.CheckIn.Value.ToShortDateString() : "", q.CheckIn.HasValue ? q.CheckIn.Value.ToString("dddd") : ""),
                         Nights = string.Format("{0}<br /><span style=\"font-size: 7pt;\">{1}</span>", q.Night.ToString(), q.CheckOut.HasValue ? q.CheckOut.Value.ToString("dd MMM") : ""),
                         Board = getBoard(UserData, q, useLocalName),
                         Accom = getAccomTable(UserData, q, useLocalName),
                         Price = getPrice(UserData, q, useLocalName),
                         Discount = getDiscount(UserData, q, useLocalName),
                         Departure = getDeparture(UserData, q, flightSeat, useLocalName),
                         Return = getArrival(UserData, q, flightSeat, useLocalName),
                         StopSale = ((q.StopSaleStd == 2 && q.StopSaleGuar == 2) || (q.AutoStop.HasValue && q.AutoStop.Value && q.FreeRoom <= 0)) ? "2" : ((q.StopSaleStd == 0 || q.StopSaleGuar == 0) ? "" : "1"),
                         StopSaleMsg = new Reservation().getStopSaleMessage(UserData.Market, q.Hotel, q.Room, q.Accom, q.Board, q.CatPackID, UserData.AgencyID, q.CheckIn, q.Night, ((q.StopSaleGuar == 1 || q.StopSaleStd == 1) && (q.StopSaleStd != 0 && q.StopSaleGuar != 0)), ref errorMsg)
                     };
        lightTable returnTable = new lightTable();
        returnTable.sEcho = iEcho;
        returnTable.iTotalRecords = allRecords.Count();
        returnTable.iTotalDisplayRecords = filteredRecords.Count();
        returnTable.aaData = retval;
        //var fltOprText = from q in allSearchRecord
        //                 group q by q.OprText into k
        //                 select k.Key;
        #region filter records
        var fltOprText = allSearchRecord
                            .Where(w => string.IsNullOrEmpty(filterData.fltOprTextSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.OprText.TrimEnd(' ') : w.OprText.TrimEnd(' ')), filterData.fltOprTextSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltHotelSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotelNameL.TrimEnd(' ') : w.HotelName.TrimEnd(' ')), filterData.fltHotelSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCategorySelected.TrimEnd(' ')) || string.Equals(w.HotCat.TrimEnd(' '), filterData.fltCategorySelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltRoomSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.RoomNameL.TrimEnd(' ') : w.RoomName.TrimEnd(' ')), filterData.fltRoomSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltLocationSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotLocationNameL.TrimEnd(' ') : w.HotLocationName.TrimEnd(' ')), filterData.fltLocationSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCheckInSelected.TrimEnd(' ')) || string.Equals(w.CheckIn, Conversion.getDateTimeOrNull(filterData.fltCheckInSelected.TrimEnd(' '))))
                            .Where(w => string.IsNullOrEmpty(filterData.fltNightsSelected.TrimEnd(' ')) || string.Equals(w.Night.ToString().TrimEnd(' '), filterData.fltNightsSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltBoardSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.BoardNameL.TrimEnd(' ') : w.BoardName.TrimEnd(' ')), filterData.fltBoardSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltAccomSelected.TrimEnd(' ')) || string.Equals(w.AccomFullName.TrimEnd(' '), filterData.fltAccomSelected.TrimEnd(' ')))
                            .Where(w => !filterData.fltPriceBegin.HasValue || (filterData.fltPriceBegin.Value <= w.LastPrice))
                            .Where(w => !filterData.fltPriceEnd.HasValue || (filterData.fltPriceEnd.Value >= w.LastPrice))
                            .Where(w => string.IsNullOrEmpty(filterData.fltDepartureSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.DepCityNameL.TrimEnd(' ') : w.DepCityName.TrimEnd(' ')), filterData.fltDepartureSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltReturnSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.ArrCityNameL.TrimEnd(' ') : w.ArrCityName.TrimEnd(' ')), filterData.fltReturnSelected.TrimEnd(' ')))
                            .GroupBy(g => g.OprText)
                            .OrderBy(o => o.Key.ToString()).Select(s => s.Key);
        var fltHotel = allSearchRecord
                            .Where(w => string.IsNullOrEmpty(filterData.fltOprTextSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.OprText.TrimEnd(' ') : w.OprText.TrimEnd(' ')), filterData.fltOprTextSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltHotelSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotelNameL.TrimEnd(' ') : w.HotelName.TrimEnd(' ')), filterData.fltHotelSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCategorySelected.TrimEnd(' ')) || string.Equals(w.HotCat.TrimEnd(' '), filterData.fltCategorySelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltRoomSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.RoomNameL.TrimEnd(' ') : w.RoomName.TrimEnd(' ')), filterData.fltRoomSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltLocationSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotLocationNameL.TrimEnd(' ') : w.HotLocationName.TrimEnd(' ')), filterData.fltLocationSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCheckInSelected.TrimEnd(' ')) || string.Equals(w.CheckIn, Conversion.getDateTimeOrNull(filterData.fltCheckInSelected.TrimEnd(' '))))
                            .Where(w => string.IsNullOrEmpty(filterData.fltNightsSelected.TrimEnd(' ')) || string.Equals(w.Night.ToString().TrimEnd(' '), filterData.fltNightsSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltBoardSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.BoardNameL.TrimEnd(' ') : w.BoardName.TrimEnd(' ')), filterData.fltBoardSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltAccomSelected.TrimEnd(' ')) || string.Equals(w.AccomFullName.TrimEnd(' '), filterData.fltAccomSelected.TrimEnd(' ')))
                            .Where(w => !filterData.fltPriceBegin.HasValue || (filterData.fltPriceBegin.Value <= w.LastPrice))
                            .Where(w => !filterData.fltPriceEnd.HasValue || (filterData.fltPriceEnd.Value >= w.LastPrice))
                            .Where(w => string.IsNullOrEmpty(filterData.fltDepartureSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.DepCityNameL.TrimEnd(' ') : w.DepCityName.TrimEnd(' ')), filterData.fltDepartureSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltReturnSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.ArrCityNameL.TrimEnd(' ') : w.ArrCityName.TrimEnd(' ')), filterData.fltReturnSelected.TrimEnd(' ')))
                            .GroupBy(g => useLocalName ? g.HotelNameL : g.HotelName)
                            .OrderBy(o => o.Key.ToString()).Select(s => s.Key);
        var fltCategory = allSearchRecord
                            .Where(w => string.IsNullOrEmpty(filterData.fltOprTextSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.OprText.TrimEnd(' ') : w.OprText.TrimEnd(' ')), filterData.fltOprTextSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltHotelSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotelNameL.TrimEnd(' ') : w.HotelName.TrimEnd(' ')), filterData.fltHotelSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCategorySelected.TrimEnd(' ')) || string.Equals(w.HotCat.TrimEnd(' '), filterData.fltCategorySelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltRoomSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.RoomNameL.TrimEnd(' ') : w.RoomName.TrimEnd(' ')), filterData.fltRoomSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltLocationSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotLocationNameL.TrimEnd(' ') : w.HotLocationName.TrimEnd(' ')), filterData.fltLocationSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCheckInSelected.TrimEnd(' ')) || string.Equals(w.CheckIn, Conversion.getDateTimeOrNull(filterData.fltCheckInSelected.TrimEnd(' '))))
                            .Where(w => string.IsNullOrEmpty(filterData.fltNightsSelected.TrimEnd(' ')) || string.Equals(w.Night.ToString().TrimEnd(' '), filterData.fltNightsSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltBoardSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.BoardNameL.TrimEnd(' ') : w.BoardName.TrimEnd(' ')), filterData.fltBoardSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltAccomSelected.TrimEnd(' ')) || string.Equals(w.AccomFullName.TrimEnd(' '), filterData.fltAccomSelected.TrimEnd(' ')))
                            .Where(w => !filterData.fltPriceBegin.HasValue || (filterData.fltPriceBegin.Value <= w.LastPrice))
                            .Where(w => !filterData.fltPriceEnd.HasValue || (filterData.fltPriceEnd.Value >= w.LastPrice))
                            .Where(w => string.IsNullOrEmpty(filterData.fltDepartureSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.DepCityNameL.TrimEnd(' ') : w.DepCityName.TrimEnd(' ')), filterData.fltDepartureSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltReturnSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.ArrCityNameL.TrimEnd(' ') : w.ArrCityName.TrimEnd(' ')), filterData.fltReturnSelected.TrimEnd(' ')))
                            .GroupBy(g => g.HotCat)
                            .OrderBy(o => o.Key.ToString()).Select(s => s.Key);
        var fltRoom = allSearchRecord
                            .Where(w => string.IsNullOrEmpty(filterData.fltOprTextSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.OprText.TrimEnd(' ') : w.OprText.TrimEnd(' ')), filterData.fltOprTextSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltHotelSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotelNameL.TrimEnd(' ') : w.HotelName.TrimEnd(' ')), filterData.fltHotelSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCategorySelected.TrimEnd(' ')) || string.Equals(w.HotCat.TrimEnd(' '), filterData.fltCategorySelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltRoomSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.RoomNameL.TrimEnd(' ') : w.RoomName.TrimEnd(' ')), filterData.fltRoomSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltLocationSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotLocationNameL.TrimEnd(' ') : w.HotLocationName.TrimEnd(' ')), filterData.fltLocationSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCheckInSelected.TrimEnd(' ')) || string.Equals(w.CheckIn, Conversion.getDateTimeOrNull(filterData.fltCheckInSelected.TrimEnd(' '))))
                            .Where(w => string.IsNullOrEmpty(filterData.fltNightsSelected.TrimEnd(' ')) || string.Equals(w.Night.ToString().TrimEnd(' '), filterData.fltNightsSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltBoardSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.BoardNameL.TrimEnd(' ') : w.BoardName.TrimEnd(' ')), filterData.fltBoardSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltAccomSelected.TrimEnd(' ')) || string.Equals(w.AccomFullName.TrimEnd(' '), filterData.fltAccomSelected.TrimEnd(' ')))
                            .Where(w => !filterData.fltPriceBegin.HasValue || (filterData.fltPriceBegin.Value <= w.LastPrice))
                            .Where(w => !filterData.fltPriceEnd.HasValue || (filterData.fltPriceEnd.Value >= w.LastPrice))
                            .Where(w => string.IsNullOrEmpty(filterData.fltDepartureSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.DepCityNameL.TrimEnd(' ') : w.DepCityName.TrimEnd(' ')), filterData.fltDepartureSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltReturnSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.ArrCityNameL.TrimEnd(' ') : w.ArrCityName.TrimEnd(' ')), filterData.fltReturnSelected.TrimEnd(' ')))
                            .GroupBy(g => useLocalName ? g.RoomNameL : g.RoomName)
                            .OrderBy(o => o.Key.ToString()).Select(s => s.Key);
        var fltLocation = allSearchRecord
                            .Where(w => string.IsNullOrEmpty(filterData.fltOprTextSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.OprText.TrimEnd(' ') : w.OprText.TrimEnd(' ')), filterData.fltOprTextSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltHotelSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotelNameL.TrimEnd(' ') : w.HotelName.TrimEnd(' ')), filterData.fltHotelSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCategorySelected.TrimEnd(' ')) || string.Equals(w.HotCat.TrimEnd(' '), filterData.fltCategorySelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltRoomSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.RoomNameL.TrimEnd(' ') : w.RoomName.TrimEnd(' ')), filterData.fltRoomSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltLocationSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotLocationNameL.TrimEnd(' ') : w.HotLocationName.TrimEnd(' ')), filterData.fltLocationSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCheckInSelected.TrimEnd(' ')) || string.Equals(w.CheckIn, Conversion.getDateTimeOrNull(filterData.fltCheckInSelected.TrimEnd(' '))))
                            .Where(w => string.IsNullOrEmpty(filterData.fltNightsSelected.TrimEnd(' ')) || string.Equals(w.Night.ToString().TrimEnd(' '), filterData.fltNightsSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltBoardSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.BoardNameL.TrimEnd(' ') : w.BoardName.TrimEnd(' ')), filterData.fltBoardSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltAccomSelected.TrimEnd(' ')) || string.Equals(w.AccomFullName.TrimEnd(' '), filterData.fltAccomSelected.TrimEnd(' ')))
                            .Where(w => !filterData.fltPriceBegin.HasValue || (filterData.fltPriceBegin.Value <= w.LastPrice))
                            .Where(w => !filterData.fltPriceEnd.HasValue || (filterData.fltPriceEnd.Value >= w.LastPrice))
                            .Where(w => string.IsNullOrEmpty(filterData.fltDepartureSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.DepCityNameL.TrimEnd(' ') : w.DepCityName.TrimEnd(' ')), filterData.fltDepartureSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltReturnSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.ArrCityNameL.TrimEnd(' ') : w.ArrCityName.TrimEnd(' ')), filterData.fltReturnSelected.TrimEnd(' ')))
                            .GroupBy(g => useLocalName ? g.HotLocationNameL : g.HotLocationName)
                            .OrderBy(o => o.Key.ToString()).Select(s => s.Key);
        var fltCheckIn = allSearchRecord
                            .Where(w => string.IsNullOrEmpty(filterData.fltOprTextSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.OprText.TrimEnd(' ') : w.OprText.TrimEnd(' ')), filterData.fltOprTextSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltHotelSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotelNameL.TrimEnd(' ') : w.HotelName.TrimEnd(' ')), filterData.fltHotelSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCategorySelected.TrimEnd(' ')) || string.Equals(w.HotCat.TrimEnd(' '), filterData.fltCategorySelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltRoomSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.RoomNameL.TrimEnd(' ') : w.RoomName.TrimEnd(' ')), filterData.fltRoomSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltLocationSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotLocationNameL.TrimEnd(' ') : w.HotLocationName.TrimEnd(' ')), filterData.fltLocationSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCheckInSelected.TrimEnd(' ')) || string.Equals(w.CheckIn, Conversion.getDateTimeOrNull(filterData.fltCheckInSelected.TrimEnd(' '))))
                            .Where(w => string.IsNullOrEmpty(filterData.fltNightsSelected.TrimEnd(' ')) || string.Equals(w.Night.ToString().TrimEnd(' '), filterData.fltNightsSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltBoardSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.BoardNameL.TrimEnd(' ') : w.BoardName.TrimEnd(' ')), filterData.fltBoardSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltAccomSelected.TrimEnd(' ')) || string.Equals(w.AccomFullName.TrimEnd(' '), filterData.fltAccomSelected.TrimEnd(' ')))
                            .Where(w => !filterData.fltPriceBegin.HasValue || (filterData.fltPriceBegin.Value <= w.LastPrice))
                            .Where(w => !filterData.fltPriceEnd.HasValue || (filterData.fltPriceEnd.Value >= w.LastPrice))
                            .Where(w => string.IsNullOrEmpty(filterData.fltDepartureSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.DepCityNameL.TrimEnd(' ') : w.DepCityName.TrimEnd(' ')), filterData.fltDepartureSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltReturnSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.ArrCityNameL.TrimEnd(' ') : w.ArrCityName.TrimEnd(' ')), filterData.fltReturnSelected.TrimEnd(' ')))
                            .GroupBy(g => g.CheckIn)
                            .OrderBy(o => o.Key)
                            .Select(s => s.Key.Value.ToShortDateString());
        var fltNight = allSearchRecord
                            .Where(w => string.IsNullOrEmpty(filterData.fltOprTextSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.OprText.TrimEnd(' ') : w.OprText.TrimEnd(' ')), filterData.fltOprTextSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltHotelSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotelNameL.TrimEnd(' ') : w.HotelName.TrimEnd(' ')), filterData.fltHotelSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCategorySelected.TrimEnd(' ')) || string.Equals(w.HotCat.TrimEnd(' '), filterData.fltCategorySelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltRoomSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.RoomNameL.TrimEnd(' ') : w.RoomName.TrimEnd(' ')), filterData.fltRoomSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltLocationSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotLocationNameL.TrimEnd(' ') : w.HotLocationName.TrimEnd(' ')), filterData.fltLocationSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCheckInSelected.TrimEnd(' ')) || string.Equals(w.CheckIn, Conversion.getDateTimeOrNull(filterData.fltCheckInSelected.TrimEnd(' '))))
                            .Where(w => string.IsNullOrEmpty(filterData.fltNightsSelected.TrimEnd(' ')) || string.Equals(w.Night.ToString().TrimEnd(' '), filterData.fltNightsSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltBoardSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.BoardNameL.TrimEnd(' ') : w.BoardName.TrimEnd(' ')), filterData.fltBoardSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltAccomSelected.TrimEnd(' ')) || string.Equals(w.AccomFullName.TrimEnd(' '), filterData.fltAccomSelected.TrimEnd(' ')))
                            .Where(w => !filterData.fltPriceBegin.HasValue || (filterData.fltPriceBegin.Value <= w.LastPrice))
                            .Where(w => !filterData.fltPriceEnd.HasValue || (filterData.fltPriceEnd.Value >= w.LastPrice))
                            .Where(w => string.IsNullOrEmpty(filterData.fltDepartureSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.DepCityNameL.TrimEnd(' ') : w.DepCityName.TrimEnd(' ')), filterData.fltDepartureSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltReturnSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.ArrCityNameL.TrimEnd(' ') : w.ArrCityName.TrimEnd(' ')), filterData.fltReturnSelected.TrimEnd(' ')))
                            .GroupBy(g => g.Night)
                            .OrderBy(o => o.Key)
                            .Select(s => s.Key);
        var fltBoard = allSearchRecord
                            .Where(w => string.IsNullOrEmpty(filterData.fltOprTextSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.OprText.TrimEnd(' ') : w.OprText.TrimEnd(' ')), filterData.fltOprTextSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltHotelSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotelNameL.TrimEnd(' ') : w.HotelName.TrimEnd(' ')), filterData.fltHotelSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCategorySelected.TrimEnd(' ')) || string.Equals(w.HotCat.TrimEnd(' '), filterData.fltCategorySelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltRoomSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.RoomNameL.TrimEnd(' ') : w.RoomName.TrimEnd(' ')), filterData.fltRoomSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltLocationSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotLocationNameL.TrimEnd(' ') : w.HotLocationName.TrimEnd(' ')), filterData.fltLocationSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCheckInSelected.TrimEnd(' ')) || string.Equals(w.CheckIn, Conversion.getDateTimeOrNull(filterData.fltCheckInSelected.TrimEnd(' '))))
                            .Where(w => string.IsNullOrEmpty(filterData.fltNightsSelected.TrimEnd(' ')) || string.Equals(w.Night.ToString().TrimEnd(' '), filterData.fltNightsSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltBoardSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.BoardNameL.TrimEnd(' ') : w.BoardName.TrimEnd(' ')), filterData.fltBoardSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltAccomSelected.TrimEnd(' ')) || string.Equals(w.AccomFullName.TrimEnd(' '), filterData.fltAccomSelected.TrimEnd(' ')))
                            .Where(w => !filterData.fltPriceBegin.HasValue || (filterData.fltPriceBegin.Value <= w.LastPrice))
                            .Where(w => !filterData.fltPriceEnd.HasValue || (filterData.fltPriceEnd.Value >= w.LastPrice))
                            .Where(w => string.IsNullOrEmpty(filterData.fltDepartureSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.DepCityNameL.TrimEnd(' ') : w.DepCityName.TrimEnd(' ')), filterData.fltDepartureSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltReturnSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.ArrCityNameL.TrimEnd(' ') : w.ArrCityName.TrimEnd(' ')), filterData.fltReturnSelected.TrimEnd(' ')))
                            .GroupBy(g => useLocalName ? g.BoardNameL : g.BoardName)
                            .OrderBy(o => o.Key.ToString())
                            .Select(s => s.Key);
        var fltAccom = allSearchRecord
                            .Where(w => string.IsNullOrEmpty(filterData.fltOprTextSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.OprText.TrimEnd(' ') : w.OprText.TrimEnd(' ')), filterData.fltOprTextSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltHotelSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotelNameL.TrimEnd(' ') : w.HotelName.TrimEnd(' ')), filterData.fltHotelSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCategorySelected.TrimEnd(' ')) || string.Equals(w.HotCat.TrimEnd(' '), filterData.fltCategorySelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltRoomSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.RoomNameL.TrimEnd(' ') : w.RoomName.TrimEnd(' ')), filterData.fltRoomSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltLocationSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotLocationNameL.TrimEnd(' ') : w.HotLocationName.TrimEnd(' ')), filterData.fltLocationSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCheckInSelected.TrimEnd(' ')) || string.Equals(w.CheckIn, Conversion.getDateTimeOrNull(filterData.fltCheckInSelected.TrimEnd(' '))))
                            .Where(w => string.IsNullOrEmpty(filterData.fltNightsSelected.TrimEnd(' ')) || string.Equals(w.Night.ToString().TrimEnd(' '), filterData.fltNightsSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltBoardSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.BoardNameL.TrimEnd(' ') : w.BoardName.TrimEnd(' ')), filterData.fltBoardSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltAccomSelected.TrimEnd(' ')) || string.Equals(w.AccomFullName.TrimEnd(' '), filterData.fltAccomSelected.TrimEnd(' ')))
                            .Where(w => !filterData.fltPriceBegin.HasValue || (filterData.fltPriceBegin.Value <= w.LastPrice))
                            .Where(w => !filterData.fltPriceEnd.HasValue || (filterData.fltPriceEnd.Value >= w.LastPrice))
                            .Where(w => string.IsNullOrEmpty(filterData.fltDepartureSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.DepCityNameL.TrimEnd(' ') : w.DepCityName.TrimEnd(' ')), filterData.fltDepartureSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltReturnSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.ArrCityNameL.TrimEnd(' ') : w.ArrCityName.TrimEnd(' ')), filterData.fltReturnSelected.TrimEnd(' ')))
                            .GroupBy(g => g.AccomFullName)
                            .OrderBy(o => o.Key.ToString())
                            .Select(s => s.Key);
        var fltPrice = allSearchRecord.GroupBy(g => g.LastPrice).OrderBy(o => o.Key.ToString()).Select(s => s.Key);
        var fltDeparture = allSearchRecord
                            .Where(w => string.IsNullOrEmpty(filterData.fltOprTextSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.OprText.TrimEnd(' ') : w.OprText.TrimEnd(' ')), filterData.fltOprTextSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltHotelSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotelNameL.TrimEnd(' ') : w.HotelName.TrimEnd(' ')), filterData.fltHotelSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCategorySelected.TrimEnd(' ')) || string.Equals(w.HotCat.TrimEnd(' '), filterData.fltCategorySelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltRoomSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.RoomNameL.TrimEnd(' ') : w.RoomName.TrimEnd(' ')), filterData.fltRoomSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltLocationSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotLocationNameL.TrimEnd(' ') : w.HotLocationName.TrimEnd(' ')), filterData.fltLocationSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCheckInSelected.TrimEnd(' ')) || string.Equals(w.CheckIn, Conversion.getDateTimeOrNull(filterData.fltCheckInSelected.TrimEnd(' '))))
                            .Where(w => string.IsNullOrEmpty(filterData.fltNightsSelected.TrimEnd(' ')) || string.Equals(w.Night.ToString().TrimEnd(' '), filterData.fltNightsSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltBoardSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.BoardNameL.TrimEnd(' ') : w.BoardName.TrimEnd(' ')), filterData.fltBoardSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltAccomSelected.TrimEnd(' ')) || string.Equals(w.AccomFullName.TrimEnd(' '), filterData.fltAccomSelected.TrimEnd(' ')))
                            .Where(w => !filterData.fltPriceBegin.HasValue || (filterData.fltPriceBegin.Value <= w.LastPrice))
                            .Where(w => !filterData.fltPriceEnd.HasValue || (filterData.fltPriceEnd.Value >= w.LastPrice))
                            .Where(w => string.IsNullOrEmpty(filterData.fltDepartureSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.DepCityNameL.TrimEnd(' ') : w.DepCityName.TrimEnd(' ')), filterData.fltDepartureSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltReturnSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.ArrCityNameL.TrimEnd(' ') : w.ArrCityName.TrimEnd(' ')), filterData.fltReturnSelected.TrimEnd(' ')))
                            .GroupBy(g => useLocalName ? g.DepCityNameL : g.DepCityName)
                            .OrderBy(o => o.Key.ToString())
                            .Select(s => s.Key);
        var fltArrival = allSearchRecord
                            .Where(w => string.IsNullOrEmpty(filterData.fltOprTextSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.OprText.TrimEnd(' ') : w.OprText.TrimEnd(' ')), filterData.fltOprTextSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltHotelSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotelNameL.TrimEnd(' ') : w.HotelName.TrimEnd(' ')), filterData.fltHotelSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCategorySelected.TrimEnd(' ')) || string.Equals(w.HotCat.TrimEnd(' '), filterData.fltCategorySelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltRoomSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.RoomNameL.TrimEnd(' ') : w.RoomName.TrimEnd(' ')), filterData.fltRoomSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltLocationSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.HotLocationNameL.TrimEnd(' ') : w.HotLocationName.TrimEnd(' ')), filterData.fltLocationSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltCheckInSelected.TrimEnd(' ')) || string.Equals(w.CheckIn, Conversion.getDateTimeOrNull(filterData.fltCheckInSelected.TrimEnd(' '))))
                            .Where(w => string.IsNullOrEmpty(filterData.fltNightsSelected.TrimEnd(' ')) || string.Equals(w.Night.ToString().TrimEnd(' '), filterData.fltNightsSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltBoardSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.BoardNameL.TrimEnd(' ') : w.BoardName.TrimEnd(' ')), filterData.fltBoardSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltAccomSelected.TrimEnd(' ')) || string.Equals(w.AccomFullName.TrimEnd(' '), filterData.fltAccomSelected.TrimEnd(' ')))
                            .Where(w => !filterData.fltPriceBegin.HasValue || (filterData.fltPriceBegin.Value <= w.LastPrice))
                            .Where(w => !filterData.fltPriceEnd.HasValue || (filterData.fltPriceEnd.Value >= w.LastPrice))
                            .Where(w => string.IsNullOrEmpty(filterData.fltDepartureSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.DepCityNameL.TrimEnd(' ') : w.DepCityName.TrimEnd(' ')), filterData.fltDepartureSelected.TrimEnd(' ')))
                            .Where(w => string.IsNullOrEmpty(filterData.fltReturnSelected.TrimEnd(' ')) || string.Equals((useLocalName ? w.ArrCityNameL.TrimEnd(' ') : w.ArrCityName.TrimEnd(' ')), filterData.fltReturnSelected.TrimEnd(' ')))
                            .GroupBy(g => useLocalName ? g.ArrCityNameL : g.ArrCityName)
                            .OrderBy(o => o.Key.ToString())
                            .Select(s => s.Key);
        #endregion
        filterData.fltBook = null;
        filterData.fltAddOffer = null;
        filterData.fltOprText = fltOprText;
        filterData.fltHotel = fltHotel;
        filterData.fltCategory = fltCategory;
        filterData.fltRoom = fltRoom;
        filterData.fltLocation = fltLocation;
        filterData.fltCheckIn = fltCheckIn;
        filterData.fltNights = fltNight;
        filterData.fltBoard = fltBoard;
        filterData.fltAccom = fltAccom;
        filterData.fltDiscount = null;
        filterData.fltDeparture = fltDeparture;
        filterData.fltReturn = fltArrival;

        returnTable.filterData = filterData;

        Response.Clear();

        Response.ContentType = ("text/html");
        Response.BufferOutput = true;
        Response.Write(ser.Serialize(returnTable));
        Response.End();
    }

    internal string getBookButton(User UserData, SearchResult row, bool useLocalName, List<BrochureCheckRecord> brochureCheckList, ref string errorMsg)
    {
        string brochure = string.Empty;
        if (brochureCheckList.Where(w => w.Code == row.HolPack && w.Brochure == true).Count() > 0)
        {
            brochure = string.Format("&nbsp;<img alt=\"{0}\" title=\"{0}\" src=\"Images/media.png\" onclick=\"showBrochure('{1}',{2},{3},'{4}')\" class=\"bookStyle\" />",
                    "Brochure",
                    row.HolPack,
                    row.CheckIn.HasValue ? row.CheckIn.Value.Ticks.ToString() : DateTime.Today.Ticks.ToString(),
                    row.CheckOut.HasValue ? row.CheckOut.Value.Ticks.ToString() : DateTime.Today.Ticks.ToString(),
                    row.Market);
        }

        string retVal = ((row.StopSaleStd == 2 && row.StopSaleGuar == 2) || !getCanBeBooked(UserData, row)) || (row.AutoStop.HasValue && row.AutoStop.Value && row.FreeRoom <= 0) ?
                                "&nbsp;"
                                :
                                string.Format("<strong onclick=\"makeRes({1},'{6}')\" class=\"bookStyle\">{0}</strong><span style=\"font-size:18pt;\"><br /><span style=\"font-size:3pt;\">&nbsp;</span><br /><span><img alt=\"{7}\" title=\"{7}\" src=\"Images/Plus_32.png\" onclick=\"addBasket({2},{3},{4},{5},'{6}')\" class=\"bookStyle\" />{8}",
                                              HttpContext.GetGlobalResourceObject("PackageSearchResult", "book"),
                                              row.RefNo,
                                              row.CatPackID,
                                              row.ARecNo,
                                              row.PRecNo,
                                              row.HAPRecId,
                                              Uri.EscapeDataString(new Reservation().getStopSaleMessage(UserData.Market, row.Hotel, row.Room, row.Accom, row.Board, row.CatPackID, UserData.AgencyID, row.CheckIn, row.Night, (row.StopSaleGuar == 1 && row.StopSaleStd == 1), ref errorMsg)),
                                              HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "AddBasket"),
                                              brochure);

        return retVal;
    }

    internal string getHotel(User UserData, SearchResult row, bool useLocalName)
    {

        string hotelName = (useLocalName ? row.HotelNameL : row.HotelName) + " (" + row.HotCat + ")";

        string hotelInfoPage = string.IsNullOrEmpty(row.InfoWeb) ? "" : "onclick=\"window.open('" + row.InfoWeb + "', '_blank')\"";
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
        {
            hotelInfoPage = string.IsNullOrEmpty(row.InfoWeb) ? "" : "onclick=\"window.open('" + row.InfoWeb + (!string.IsNullOrEmpty(UserData.AgencyRec.CIF) ? UserData.AgencyRec.CIF : "") + "', '_blank')\"";
        }
        string roomInfo = string.Format("<a class=\"roomConsept\" href=\"Controls/RoomConsept.aspx?Hotel={1}&Room={2}&Date={3}\" rel=\"Controls/RoomConsept.aspx?Hotel={1}&Room={2}&Date={3}\" title=\"{4}\">{0}</a>",
                                        useLocalName ? row.RoomNameL : row.RoomName,
                                        row.Hotel,
                                        row.Room,
                                        row.CheckIn.Value.Ticks.ToString(),
                                        useLocalName ? row.RoomNameL : row.RoomName);
        string retval = string.Format("<span style=\"font-weight: bold; {2}\" {3}>{0}</span><br />" +
                                      "<span style=\"font-size: 8pt;\">{1}</span>",
                                        hotelName,
                                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ? (useLocalName ? row.RoomNameL : row.RoomName) : roomInfo,
                                        string.IsNullOrEmpty(hotelInfoPage) ? "" : "cursor: pointer;",
                                        hotelInfoPage);
        return retval;
    }

    internal string getBoard(User UserData, SearchResult row, bool useLocalName)
    {
        string boardConcept = string.Empty;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            boardConcept = string.Format("{0}", row.Board);
        else
            boardConcept = string.Format("<a class=\"boardConsept\" href=\"Controls/BoardConsept.aspx?Hotel={1}&Board={2}&Date={3}\" rel=\"Controls/BoardConsept.aspx?Hotel={1}&Board={2}&Date={3}\" title=\"{4}\">{0}</a>",
                                    row.Board,
                                    row.Hotel,
                                    row.Board,
                                    row.CheckIn.Value.Ticks.ToString(),
                                    useLocalName ? row.BoardNameL : row.BoardName);
        string retVal = string.Format("<span title=\"{1}\" class=\"cursorDefault\">{0}</span>",
                                boardConcept,
                                useLocalName ? row.BoardNameL : row.BoardName);

        return retVal;
    }

    internal string getAccomTable(User UserData, SearchResult row, bool useLocalName)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendFormat("<span style=\"white-space:nowrap;\" class=\"cursorDefault\" title=\"{2}\"><strong>{0}</strong>&nbsp;&nbsp;&nbsp;{1}</span>",
            row.Accom,
            row.HAdult.ToString() + " " + (useLocalName ? UserData.TvParams.TvParamPrice.PLAdlCap : "Adl"),
            useLocalName ? row.AccomNameL : row.AccomName);
        if (row.HChdAgeG1 + row.HChdAgeG2 + row.HChdAgeG3 + row.HChdAgeG4 > 0)
        {
            sb.Append("<br />");
            string childAccom = string.Empty;
            if (row.HChdAgeG1 > 0)
            {
                if (childAccom.Length > 0) childAccom += "<br />";
                childAccom += row.HChdAgeG1 + " " + (useLocalName ? UserData.TvParams.TvParamPrice.PLChdCap : "Chd")
                    + "(" + (row.ChdG1Age1.HasValue ? row.ChdG1Age1.Value.ToString("#.00") : "")
                    + "-" + (row.ChdG1Age2.HasValue ? row.ChdG1Age2.Value.ToString("#.00") : "") + ")";
            }
            if (row.HChdAgeG2 > 0)
            {
                if (childAccom.Length > 0) childAccom += "<br />";
                childAccom += row.HChdAgeG2 + " " + (useLocalName ? UserData.TvParams.TvParamPrice.PLChdCap : "Chd")
                    + "(" + (row.ChdG2Age1.HasValue ? row.ChdG2Age1.Value.ToString("#.00") : "")
                    + "-" + (row.ChdG2Age2.HasValue ? row.ChdG2Age2.Value.ToString("#.00") : "") + ")";
            }
            if (row.HChdAgeG3 > 0)
            {
                if (childAccom.Length > 0) childAccom += "<br />";
                childAccom += row.HChdAgeG3 + " " + (useLocalName ? UserData.TvParams.TvParamPrice.PLChdCap : "Chd")
                    + "(" + (row.ChdG3Age1.HasValue ? row.ChdG3Age1.Value.ToString("#.00") : "")
                    + "-" + (row.ChdG3Age2.HasValue ? row.ChdG3Age2.Value.ToString("#.00") : "") + ")";
            }
            if (row.HChdAgeG4 > 0)
            {
                if (childAccom.Length > 0) childAccom += "<br />";
                childAccom += row.HChdAgeG4 + " " + (useLocalName ? UserData.TvParams.TvParamPrice.PLChdCap : "Chd")
                    + "(" + (row.ChdG4Age1.HasValue ? row.ChdG4Age1.Value.ToString("#.00") : "")
                    + "-" + (row.ChdG4Age2.HasValue ? row.ChdG4Age2.Value.ToString("#.00") : "") + ")";
            }
            sb.AppendFormat("<span style=\"font-size: 7pt; text-align:left;\">{0}</span>",
                childAccom);
        }
        return sb.ToString();
    }

    internal string getPrice(User UserData, SearchResult row, bool useLocalName)
    {
        string price = string.Empty;
        bool? showOldPrice = TvTools.Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ShowOldPrice"));
        string oldPriceStr = string.Empty;
        if (showOldPrice.HasValue && showOldPrice.Value)
        {
            if (row.LastPrice < row.CalcSalePrice)
                oldPriceStr = row.CalcSalePrice.HasValue ? "<span style=\"text-decoration:line-through;\">" + row.CalcSalePrice.Value.ToString("#,###.00") + " " + row.SaleCur + "</span>" + "<br />" : "";
        }
        if (oldPriceStr.Length > 0)
            price += oldPriceStr;

        price += string.Format("<span style=\"font-weight: bold; white-space: nowrap;\">{0}</span>", row.LastPrice.HasValue ? row.LastPrice.Value.ToString("#,###.00") + " " + row.SaleCur : "");
        if (row.AgencyEB.HasValue && row.AgencyEB.Value > 0)
            price += string.Format("<br /><span style=\"font-size: 8pt;\">{0}: {1}</span>",
                            HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "AgencyEB"),
                            row.AgencyEB.Value.ToString("#.00") + " %");
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            price += "<br /><span style=\"font-size:7pt; font-weight:bold; font-style:italic; color: #FF0000\">" + row.Description.ToString() + "</span>";
        else
        {
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Kusadasi_Ro) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Go2HolidayBg))
            {
                DateTime? ValidDate = DateTime.MaxValue;
                if (row.ValidDate.HasValue)
                    ValidDate = row.ValidDate.Value;
                if (row.EBValidDate.HasValue && row.EBValidDate.Value < ValidDate.Value)
                    ValidDate = row.EBValidDate.Value;
                if (row.SaleEndDate.HasValue && row.SaleEndDate.Value < ValidDate.Value)
                    ValidDate = row.SaleEndDate.Value;
                if (row.FlightValidDate.HasValue && row.FlightValidDate.Value < ValidDate)
                    ValidDate = row.FlightValidDate.Value;
                if (row.HotelValidDate.HasValue && row.HotelValidDate.Value < ValidDate)
                    ValidDate = row.HotelValidDate.Value;
                if (row.PEB_SaleEndDate.HasValue && row.PEB_SaleEndDate.Value < ValidDate)
                    ValidDate = row.PEB_SaleEndDate.Value;

                string priceDesc = ValidDate.HasValue ? (HttpContext.GetGlobalResourceObject("PackageSearchResult", "DateOfValidity") + " -> " + ValidDate.Value.ToShortDateString()) : "";
                if (priceDesc.Length > 0)
                    price += "<br /><span style=\"font-size:7pt; font-weight:bold; color: #FF0000\">" + priceDesc + "</span>";
            }
        }
        return price;
    }

    internal string getDiscount(User UserData, SearchResult row, bool useLocalName)
    {
        DateTime? ValidDate = DateTime.MaxValue;
        string offerTitle = string.Empty;
        decimal EBPer = row.EBPerc.HasValue ? row.EBPerc.Value : 0;
        bool _EBAmount = false;
        _EBAmount = row.PEB_AdlVal.HasValue && row.PEB_AdlVal.Value > 0 ? true : false;
        _EBAmount = row.PasEBPer.HasValue && row.PasEBPer > 0 ? true : false;
        DateTime EBValidDate = row.EBValidDate.HasValue ? row.EBValidDate.Value : DateTime.MaxValue;
        DateTime SaleEndDate = row.SaleEndDate.HasValue ? row.SaleEndDate.Value : DateTime.MaxValue;
        DateTime? PEB_SaleEndDate = row.PEB_SaleEndDate;
        List<ValidDates> validDate = new List<ValidDates>();
        bool discountImg = false;
        if ((row.SaleSPONo.HasValue && row.SaleSPONo.Value != 0) || (row.plSpoExists.HasValue && row.plSpoExists.Value))
        {
            offerTitle += GetGlobalResourceObject("PackageSearchResultV2", "msgSPO").ToString();
            discountImg = true;
        }
        if (EBPer > 0 || _EBAmount)
        {
            if (offerTitle.Length > 0) offerTitle += ", ";
            offerTitle += GetGlobalResourceObject("PackageSearchResultV2", "msgEB").ToString();
            discountImg = true;
        }
        validDate.Add(new ValidDates { Code = "SaleEndDate", ValidDate = SaleEndDate });
        validDate.Add(new ValidDates { Code = "EBValidDate", ValidDate = EBValidDate });
        validDate.Add(new ValidDates { Code = "FlightValidDate", ValidDate = (row.FlightValidDate.HasValue ? row.FlightValidDate.Value : DateTime.MinValue) });
        validDate.Add(new ValidDates { Code = "HotelValidDate", ValidDate = (row.HotelValidDate.HasValue ? row.HotelValidDate.Value : DateTime.MinValue) });
        validDate.Add(new ValidDates { Code = "PEB_SaleEndDate", ValidDate = PEB_SaleEndDate });
        var validD = from q in validDate
                     where q.ValidDate != null && q.ValidDate != DateTime.MinValue
                     orderby q.ValidDate
                     select new { date = q.ValidDate };
        if (validD != null && validD.Count() > 0)
            ValidDate = validD.FirstOrDefault().date;
        string offerTitleStr = string.Empty;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Kusadasi_Ro) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Go2HolidayBg))
        {
            ValidDate = DateTime.MaxValue;
            if (row.ValidDate.HasValue)
                ValidDate = row.ValidDate.Value;
            if (row.EBValidDate.HasValue && row.EBValidDate.Value < ValidDate.Value)
                ValidDate = row.EBValidDate.Value;
            if (row.SaleEndDate.HasValue && row.SaleEndDate.Value < ValidDate.Value)
                ValidDate = row.SaleEndDate.Value;

            if (row.FlightValidDate.HasValue && row.FlightValidDate.Value < ValidDate)
                ValidDate = row.FlightValidDate.Value;
            if (row.HotelValidDate.HasValue && row.HotelValidDate.Value < ValidDate)
                ValidDate = row.HotelValidDate.Value;
            if (row.PEB_SaleEndDate.HasValue && row.PEB_SaleEndDate.Value < ValidDate)
                ValidDate = row.PEB_SaleEndDate.Value;

            string priceDesc = ValidDate.HasValue ? ValidDate.Value.ToShortDateString() : "";
            if (priceDesc.Length > 0)
                offerTitleStr = offerTitle + (ValidDate.HasValue ? " (" + ValidDate.Value.ToString("dd MMM yyyy") + ")" : "");
        }
        else
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Go2HolidayBg))
                offerTitleStr = offerTitle;
            else offerTitleStr = offerTitle + (ValidDate.HasValue ? " (" + ValidDate.Value.ToString("dd MMM yyyy") + ")" : "");

        string offerStr = discountImg ? string.Format("<img alt=\"{0}\" title=\"{0}\" src=\"Images/discount.png\" />", offerTitleStr) : "&nbsp;";
        return offerStr;
    }

    internal bool getCanBeBooked(User UserData, SearchResult row)
    {
        int? freeRoom = row.FreeRoom;
        if (freeRoom > 0)
            return true;
        else
        {
            if (string.Equals(row.HotAllotChk, "Y") && string.Equals(row.HotAllotNotAcceptFull, "Y"))
                return false;
            else return true;
        }
    }

    internal string getFreeRoom(User UserData, SearchResult row, bool useLocalName)
    {
        int? freeRoom = row.FreeRoom;
        string retVal = string.Empty;
        if (row.FreeRoom >= 999)
            retVal = string.Format("<span style=\"font-size: 10pt;\">{0}</span>", Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "RequestInfo")));
        else
            retVal = string.Format("<span style=\"font-size: 10pt;\">{0}</span>",
                                    freeRoom.HasValue && freeRoom.Value > 0 ? (freeRoom.Value > 3 ? "3+" : freeRoom.Value.ToString()) : "0");

        return retVal;
    }

    internal string getDeparture(User UserData, SearchResult row, int? flightSeat, bool useLocalName)
    {
        string retVal = string.Empty;
        if (UserData.ShowFlight)
            retVal = string.Format("<div style=\"cursor: default; font-size: 9pt; width: 100%; height:100%; {3}\" >{0}<br /><span style=\"font-size:7pt;\">{1}</span><br /><span style=\"font-size:7pt;\">{2}</span></div>",
                                        row.DepFlight,
                                        row.DEPFlightTime.HasValue ? row.DEPFlightTime.Value.ToString("HH:mm") : "",
                                        row.DepSeat.HasValue && row.DepSeat.Value > 0 ? (row.DepSeat.Value > flightSeat ? "10+" : row.DepSeat.Value.ToString()) : "0",
                                        row.DepSeat.HasValue && row.DepSeat.Value > 0 ? "color: #006600;" : "color: #FF0000;");
        else
            retVal = string.Format("<div style=\"cursor: default; font-size: 9pt; width: 100%; height:100%; {2}\" ><strong>{0}</strong><br /><span style=\"font-size:7pt;\">{1}</span></div>",
                                row.DepSeat.HasValue && row.DepSeat.Value > 0 ? (row.DepSeat.Value > flightSeat ? "10+" : row.DepSeat.Value.ToString()) : "0",
                                useLocalName ? row.DepCityNameL : row.DepCityName,
                                row.DepSeat.HasValue && row.DepSeat.Value > 0 ? "color: #006600;" : "color: #FF0000;");
        return retVal;
    }

    internal string getArrival(User UserData, SearchResult row, int? flightSeat, bool useLocalName)
    {
        string retVal = string.Empty;
        if (UserData.ShowFlight)
            retVal = string.Format("<div style=\"cursor: default; font-size: 9pt; width: 100%; height:100%; {3}\" >{0}<br /><span style=\"font-size:7pt;\">{1}</span><br /><span style=\"font-size:7pt;\">{2}</span></div>",
                                            row.RetFlight,
                                            row.RETFlightTime.HasValue ? row.RETFlightTime.Value.ToString("HH:mm") : "",
                                            row.RetSeat.HasValue && row.RetSeat.Value > 0 ? (row.RetSeat.Value > flightSeat ? "10+" : row.RetSeat.Value.ToString()) : "0",
                                            row.RetSeat.HasValue && row.RetSeat.Value > 0 ? "color: #006600;" : "color: #FF0000;");
        else
            retVal = string.Format("<div style=\"cursor: default; font-size: 9pt; width: 100%; height:100%; {2}\"><strong>{0}</strong><br /><span style=\"font-size:7pt;\">{1}</span></div>",
                                row.RetSeat.HasValue && row.RetSeat.Value > 0 ? (row.RetSeat.Value > flightSeat ? "10+" : row.RetSeat.Value.ToString()) : "0",
                                useLocalName ? row.ArrCityNameL : row.ArrCityName,
                                row.RetSeat.HasValue && row.RetSeat.Value > 0 ? "color: #006600;" : "color: #FF0000;");
        return retVal;
    }

    internal string getOprText(User UserData, SearchResult row, bool useLocalName)
    {
        return TvTools.Conversion.getStrOrNull(row.OprText).ToString();
    }

    protected List<SearchResult> ReadFile()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\PriceSearchV2." + HttpContext.Current.Session.SessionID;
        List<SearchResult> list = new List<SearchResult>();
        if (System.IO.File.Exists(path))
        {
            System.IO.StreamReader reader = new System.IO.StreamReader(path);

            try
            {
                string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
                list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SearchResult>>(uncompressed);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                reader.Close();
            }
        }
        return list;
    }

    internal void saveFile(List<SearchResult> result, IQueryable<SearchResult> calcResult)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        foreach (SearchResult row in calcResult)
        {
            SearchResult current = result.Find(f => f.RefNo == row.RefNo);
            if (current != null)
                current = row;
        }

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\PriceSearchV2." + HttpContext.Current.Session.SessionID;
        if (System.IO.File.Exists(path))
            System.IO.File.Delete(path);
        System.IO.FileStream file = new System.IO.FileStream(path, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
        try
        {
            System.IO.StreamWriter writer = new System.IO.StreamWriter(file);
            string compress = TvTools.GZipCompres.Compress(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            writer.Write(compress);
            writer.Close();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            file.Close();
        }
    }
}
