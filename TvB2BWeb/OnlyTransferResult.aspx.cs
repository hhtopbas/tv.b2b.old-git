﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Threading;
using System.Globalization;
using TvBo;
using System.Web.Script.Services;

public partial class OnlyTransferResult : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string getGridHeaders()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<dataTableColumnsDef> aoColumns = new List<dataTableColumnsDef>();
        //96,18,30,200,30,100,80,50,50,130,100,30,28,28
        aoColumns.Add(new dataTableColumnsDef { IDNo = 1, ColName = "Book", sTitle = "&nbsp;", sClass = "center", sWidth = "10%", bSortable = false, bSearchable = false });        
        aoColumns.Add(new dataTableColumnsDef { IDNo = 2, ColName = "Name", sTitle = HttpContext.GetGlobalResourceObject("OnlyExcursion", "lblExcursionName").ToString(), sWidth = "50%", bSortable = false, bSearchable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 3, ColName = "CheckIn", sTitle = HttpContext.GetGlobalResourceObject("OnlyTransfer", "lblTransferDate").ToString(), sWidth = "15%", bSortable = false, sType = "date", bSearchable = false });       
        aoColumns.Add(new dataTableColumnsDef { IDNo = 4, ColName = "Price", sTitle = HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "Price").ToString(), sWidth = "10%", bSortable = false, bSearchable = false, sClass="price" });
        var retVal = from q in aoColumns.AsEnumerable()
                     select new { sTitle = q.sTitle, sWidth = q.sWidth, bSortable = q.bSortable, sType = q.sType, sClass = q.sClass };
        getResult();
        return Newtonsoft.Json.JsonConvert.SerializeObject(retVal);
    }

    public static string getResult()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["OnlyTransferFilter"] == null) return null;
        OnlyTransferFilter criteria = (OnlyTransferFilter)HttpContext.Current.Session["OnlyTransferFilter"];

        string errorMsg = string.Empty;

        List<OnlyTransfer_Transfers> result = new OnlyTransfers().getOnlyTransfers(UserData, criteria, ref errorMsg);
        if (result != null && result.Count > 0)
        {
            CreateAndWriteFile(result);
            return string.Empty;
        }
        else
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\OnlyTransferSearch." + HttpContext.Current.Session.SessionID;
            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);
            return errorMsg;
        }
    }

    private static void CreateAndWriteFile(List<OnlyTransfer_Transfers> result)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\OnlyTransferSearch." + HttpContext.Current.Session.SessionID;
        if (System.IO.File.Exists(path))
            System.IO.File.Delete(path);
        System.IO.FileStream f = new System.IO.FileStream(path, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
        try
        {
            System.IO.StreamWriter writer = new System.IO.StreamWriter(f);
            string compress = TvTools.GZipCompres.Compress(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            writer.Write(compress);
            writer.Close();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            f.Close();
        }
    }
}

