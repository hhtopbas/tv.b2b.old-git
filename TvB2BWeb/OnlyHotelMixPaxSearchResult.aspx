﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlyHotelMixPaxSearchResult.aspx.cs"
  Inherits="OnlyHotelMixPaxSearchResult" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    <%= GetGlobalResourceObject("PageTitle", "PackageSearchResult")%></title>

  <script src="Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery.json.js" type="text/javascript"></script>
  <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
  <script src="Scripts/jquery.textTruncate.js" type="text/javascript"></script>
  <script src="Scripts/NumberFormat154.js" type="text/javascript"></script>
  <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
  <script src="Scripts/jquery.cluetip.js" type="text/javascript"></script>
  <script src="Scripts/mustache.js" type="text/javascript"></script>
  <script src="Scripts/smartpaginator.js?v=4" type="text/javascript"></script>
  <script src="Scripts/jquery.linq.js" type="text/javascript"></script>

  <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="CSS/jquery.cluetip.css" rel="stylesheet" type="text/css" />
  <link href="CSS/OnlyHotelMixSearchResult.css?v=1" rel="stylesheet" />
  <link href="CSS/smartpaginator.css?v=3" rel="stylesheet" />

  <style type="text/css">
    #divNote { height: 30px; color: #CC0000; font-weight: bold; text-decoration: underline; cursor: help; }
  </style>

  <script language="javascript" type="text/javascript">

    var noRecordFound = '<%= GetGlobalResourceObject("PackageSearchResult", "NoRecordFound") %>';
    var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
    var btnNo = '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>';
    var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
    var sFirst = '<%= GetGlobalResourceObject("LibraryResource", "sFirst") %>';
    var sPrevious = '<%= GetGlobalResourceObject("LibraryResource", "sPrevious") %>';
    var sNext = '<%= GetGlobalResourceObject("LibraryResource", "sNext") %>';
    var sLast = '<%= GetGlobalResourceObject("LibraryResource", "sLast") %>';
    var sPage = '<%= GetGlobalResourceObject("LibraryResource", "sPage") %>';
    var sGotoPage = '<%= GetGlobalResourceObject("LibraryResource", "sGotoPage") %>';
    var CurrencyTypeNotTheSame = '<%= GetGlobalResourceObject("PackageSearchResult", "CurrencyTypeNotTheSame") %>';

    var cultureNumber;
    var cultureDate;

    function decimalPlaces(n) {
      // Make sure it is a number and use the builtin number -> string.
      var s = "" + (+n);
      // Pull out the fraction and the exponent.
      var match = /(?:\.(\d+))?(?:[eE]([+\-]?\d+))?$/.exec(s);
      // NaN or Infinity or integer.
      // We arbitrarily decide that Infinity is integral.
      if (!match) {
        match = /(?:\,(\d+))?(?:[eE]([+\-]?\d+))?$/.exec(s);
        if (!match) {
          return 0;
        }
      }
      // Count the number of digits in the fraction and subtract the
      // exponent to simulate moving the decimal point left by exponent places.
      // 1.234e+2 has 1 fraction digit and '234'.length -  2 == 1
      // 1.234e-2 has 5 fraction digit and '234'.length - -2 == 5
      return Math.max(
          0,  // lower limit.
          (match[1] == '0' ? 0 : (match[1] || '').length)  // fraction length
          - (match[2] || 0));  // exponent
    }

    //$.blockUI.defaults.message = '<h1>< %= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>';
    $(document).ajaxStart(function () {
      $.blockUI({
        message: '<h1>' + lblPleaseWait + '</h1>'
      });
    }).ajaxStop(function () {
      $.unblockUI();
    });

    if (typeof window.event != 'undefined')
      document.onkeydown = function () {
        if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
          return (event.keyCode != 8);
      }
    else
      document.onkeypress = function (e) {
        if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
          return (e.keyCode != 8);
      }


    function logout() {
      self.parent.logout();
    }

    function showHotelInfo(_hotelUrl) {
      window.open(_hotelUrl);
    }

    function tryNumberFormat(obj) {
      var nf = new NumberFormat(obj);
      if (cultureNumber != null) {
        nf.PERIOD = cultureNumber.CurrencyGroupSeparator;
        nf.COMMA = cultureNumber.CurrencyDecimalSeparator;
      }
      var rsdoS = nf.PERIOD;
      var rsdoD = nf.COMMA;
      nf.setPlaces(2);
      nf.setSeparators(true, rsdoS, rsdoD);
      obj = nf.toFormatted();
      return obj;
    }

    function showDialog(msg) {
      $("#messages").html(msg);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        modal: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog('close');
          }
        }]
      });
    }

    function showAlert(msg) {
      $("#messages").html(msg);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        modal: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog('close');
          }
        }]
      });
    }

    function changeRoomPrice(rowNumber, refNo) {

      var tablePrice = $('#tablePrice_' + rowNumber.toString());
      var hotelCheckInNightPrice = tablePrice.find("input");

      var hotelPrice = $("#HotelCheckInNightPrice_" + rowNumber);
      hotelPrice.html('');
      var priceRefNoList = '';
      var price = 0.0;
      var salecur = '';
      var first = true;
      $.each(hotelCheckInNightPrice, function (i) {
        if (this.checked) {
          var _price = $(this).attr("price");
          var _decimalPlaces = decimalPlaces(_price)

          var _salecur = $(this).attr("salecur");

          if (priceRefNoList.length > 0) priceRefNoList += ',';
          priceRefNoList += this.value;
          price += parseFloat(_price);

          if (salecur != '' && salecur != _salecur) {
            hotelPrice.html(CurrencyTypeNotTheSame);
            $("#btnBook_" + rowNumber.toString()).attr("disabled", true);
            return;
          }
          else {
            $("#btnBook_" + rowNumber.toString()).removeAttr("disabled");
            salecur = _salecur;
          }

          hotelPrice.html(formatPrice(tryNumberFormat(price)));
        }
      });
    }

    function clearOffer() {
      $.ajax({
        type: "POST",
        url: "OnlyHotelSearchResultV2.aspx/clearOffer",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {

        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showDialog(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function offerReportShow(param1, param2) {
      if (param1 == '' && (param2 != null || param2 != ''))
        showAlert(param2);
      else window.open(param1);
    }

    function createOffer() {
      self.parent.createOffer();
    }

    function addOffer(rowNumber) {
      var tablePrice = $('#tablePrice_' + rowNumber.toString());
      var hotelCheckInNightPrice = tablePrice.find("input");
      var priceRefNoList = '';
      $.each(hotelCheckInNightPrice, function (i) {
        if (this.checked) {
          if (priceRefNoList.length > 0) priceRefNoList += ',';
          priceRefNoList += this.value;
        }
      });

      $.ajax({
        type: "POST",
        url: "OnlyHotelSearchResultV2.aspx/addOffer",
        data: '{"BookList":"' + priceRefNoList + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {

        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showDialog(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function book(rowNumber) {
      var tablePrice = $('#tablePrice_' + rowNumber.toString());
      var hotelCheckInNightPrice = tablePrice.find("input");
      var priceRefNoList = '';
      var price = 0.0;
      var stopSale = false;
      var allotment = false;
      var allotmentMsg = '';
      var stopSaleMsg = '';
      $.each(hotelCheckInNightPrice, function (i) {
        if (this.checked) {
          if (priceRefNoList.length > 0) priceRefNoList += ',';
          priceRefNoList += this.value;
          price += parseFloat($(this).attr("price"));
        }
      });

      self.parent.mixBookRooms(priceRefNoList, false);
    }

    function textTrancateRun() {

      $(".resultGrid .PriceTable .PriceDetail .prices .boardConsept").textTruncate($(".resultGrid .PriceTable .PriceDetail .prices .board").width() - 5);
      $(".resultGrid .PriceTable .PriceDetail .prices .roomConsept").textTruncate($(".resultGrid .PriceTable .PriceDetail .prices .room").width() - 5);
      $(".resultGrid .PriceTable .PriceDetail .prices .accom span").textTruncate($(".resultGrid .PriceTable .PriceDetail .prices .accom").width() - 5);

      $('.roomConsept').cluetip({
        activation: 'click',
        width: 400,
        arrows: true,
        dropShadow: false,
        hoverIntent: false,
        sticky: true,
        mouseOutClose: false,
        closePosition: 'title',
        closeText: '<img src="Images/cancel.png" title="close" />'
      });
      $('.boardConsept').cluetip({
        activation: 'click',
        width: 400,
        arrows: true,
        dropShadow: false,
        hoverIntent: false,
        sticky: true,
        mouseOutClose: false,
        closePosition: 'title',
        closeText: '<img src="Images/cancel.png" title="close" />'
      });
    }

    function newFilter(fieldName, value) {
      this.FieldName = fieldName;
      this.Value = value;
    }

    function filterResult() {
      getResultGrid(1, false);
    }

    function setPage(pageNo, paged) {
      getResultGrid(pageNo, paged);
    }

    function getPageCount() {
      return parseInt('10');
    }

    function formatPrice(priceText) {
      var price = priceText;
      var decimalSperator = '.';
      if (cultureNumber != null) {
        decimalSperator = cultureNumber.CurrencyDecimalSeparator;
      }

      var pArry = priceText.toString().split(decimalSperator);
      var new_span = '';
      if (pArry.length > 0) {
        new_span = '<span class="number">' + pArry[0] + '</span>';
        if (pArry.length > 1) {
          new_span += '<span class="decimal">' + decimalSperator + pArry[1] + '</span>';
        }
      }
      return new_span;
    }

    function formatTotalPrice() {
      var totalPrice = $('span[name="totalPrice"]');
      $.each(totalPrice, function (i) {
        $(this).html(formatPrice($(this).html()));
      });
    }

    function showReadMore(_this, hotelId) {
      self.parent.showReadMore(hotelId);
    }

    function onSortChange() {
      getResultGrid(1, false);
    }

    function getResultDataAndCreate(tmpHtml, pageNo, paged, filterData) {
      var pageItemCount = getPageCount();
      var data = new Object();
      data.page = pageNo;
      data.pageItemCount = parseInt(pageItemCount);
      data.filtered = false;
      data.paged = paged;
      data.sorting = $("#fltSort").length > 0 ? $("#fltSort").val() : "price_down";
      data.filterData = filterData;
      //var tmpl = '';
      $.ajax({
        async: true,
        type: "POST",
        url: "OnlyHotelMixPaxSearchResult.aspx/getResultGrid",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var data = msg.d;
            var html = Mustache.render(tmpHtml, data);
            $("#searchResult").html(html);
            if (data.ResultNote == true) {
              $("#resultNote").html(data.SearchResultNote);
            }
            $("#fltSort").val(data.SearchResultFilter.Sort);
            cultureNumber = data.CurrentCultureNumber;
            $("#resultFilterDiv").html('');
            $("#resultFilterDiv").html(data.FilterDetails);
            $("span[name=hotelCheckIn]").html(data.SearchResultFilter.Requests.CheckIn);
            $("span[name=hotelCheckOut]").html(data.SearchResultFilter.Requests.CheckOut);
            $("span[name=hotelNight]").html(data.SearchResultFilter.Requests.Night);

            $("#searchResult").data("TotalResultCount", data.totalResultCount);
            $("#searchResult").data("PageItemCount", data.PageItemCount);
            $("#searchResult").data("CurrentPage", data.CurrentPage);

            $('#navigation').smartpaginator({
              totalrecords: data.totalResultCount,
              recordsperpage: data.PageItemCount,
              length: 4,
              next: sNext,
              prev: sPrevious,
              first: sFirst,
              last: sLast,
              go: sGotoPage,
              theme: 'gray',
              controlsalways: true,
              onchange: function (newPage) {
                getPage(newPage, true);
              },
              initval: data.CurrentPage
            });

            formatTotalPrice();
          } else {
            $("#searchResult").html('');
            $("#searchResult").html('<div style=\"width: 90%; clear:both; text-align: center;\"><span style=\"font-size: 14pt;\">' + noRecordFound + '</span><div>');
          }
          reSizeFrame();
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showDialog(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getResultGrid(pageNo, paged) {
      var params = new Object();
      var filters = [];
      var filter = { FieldName: '', Value: '' };

      filters[0] = new newFilter('rfPaxLocation', $("#rfPaxLocation").length > 0 ? $("#rfPaxLocation").val() : '');
      filters[1] = new newFilter('rfCategory', $("#rfCategory").length > 0 ? $("#rfCategory").val() : '');
      filters[2] = new newFilter('rfHotel', $("#rfHotel").length > 0 ? $("#rfHotel").val() : '');
      filters[3] = new newFilter('rfRoom', $("#rfRoom").length > 0 ? $("#rfRoom").val() : '');
      filters[4] = new newFilter('rfBoard', $("#rfBoard").length > 0 ? $("#rfBoard").val() : '');

      $("#searchResult").data("pageNo", pageNo);
      $.get($("#tmplPath").val() + 'OnlyHotelMixSearchTmpl.html?v=201510161515', function (html) {
        getResultDataAndCreate(html, pageNo, paged, filters);
      }).fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        showAlert("Request Failed: " + err);
      });
    }

    function getPage(pageNo, paged) {
      getResultGrid(pageNo, paged);
    }

    function pageChangeButtom(changeVal) {
      var pageNo = $("#pageDropDownListButtom").val();
      var _PageNo = parseInt(pageNo);
      if (changeVal != undefined && changeVal != '') {
        _PageNo = _PageNo + parseInt(changeVal);
      }
      setPage(_PageNo.toString(), true);
    }

    function pageChange(changeVal) {
      var pageNo = $("#pageDropDownListTop").val();
      var _PageNo = parseInt(pageNo);
      if (changeVal != undefined && changeVal != '') {
        _PageNo = _PageNo + parseInt(changeVal);
      }
      setPage(_PageNo.toString(), true);
    }

    function showBrochure(holPack, CheckIn, CheckOut, Market) {
      self.parent.showBrochure(holPack, CheckIn, CheckOut, Market);
    }

    function reSizeFrame() {
      self.parent.reSizeResultFrame(document.body.offsetHeight);
    }

    function prevDate(pdate) {
      self.parent.reSearch(pdate);
    }

    function nextDate(ndate) {
      self.parent.reSearch(ndate);
    }

    $(document).ready(function () {

      $(".my-navigation").on("click", function () {
        setTimeout(onClickNavigation, 500);
      });
      $(".simple-pagination-items-per-page").on("change", function () {
        onClickNavigation();
      });
      $(".simple-pagination-select-specific-page").on("change", function () {
        onClickNavigation();
      });

      getPage(1, false);
    });
  </script>

</head>
<body class="ui-helper-reset">
  <form id="formPackageSearchResult" runat="server" class="ui-helper-clearfix">
    <asp:HiddenField ID="tmplPath" runat="server" />
    <div id="searchResult" class="ui-helper-clearfix resultBody">
    </div>
    <div id="navigation">
    </div>
    <div id="dialog-message" title="" style="display: none;">
      <span id="messages" style="font-size: 80%; font-weight: normal;">Message</span>
    </div>
  </form>
</body>
</html>
