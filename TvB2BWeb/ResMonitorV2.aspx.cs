﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using TvBo;
using TvTools;

public partial class ResMonitorV2 : BasePage
{
    protected string tmpPath = "";
    protected string SearchCustomerOptions = "0";
    public static string twoLetterISOLanguageName = "en";

    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url);
        if (UserData.Ci.Name.ToLower() == "lt-lt")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "yyyy.MM.dd";
        else if (UserData.Ci.Name.ToLower() == "lv-lv")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string searchCustomerOptions = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "SearchCustomerOptions"));
        if (!string.IsNullOrEmpty(searchCustomerOptions))
            SearchCustomerOptions = searchCustomerOptions;
        List<string> lang = new List<string>() { "ar-DZ", "ar-DZ", "ar", "en-AU", "en-GB", "en-NZ", "fr-CH", "fr", "pt-BR", "pt", "sr-SR", "sr-CS", "sr", "zh-CN", "zh-HK", "zh-TW" };
        if (lang.Contains(UserData.Ci.Name))
            twoLetterISOLanguageName = UserData.Ci.Name;
        else
            twoLetterISOLanguageName = UserData.Ci.TwoLetterISOLanguageName != "en" ? UserData.Ci.TwoLetterISOLanguageName : "";

        tmpPath = WebRoot.BasePageRoot + "Data/" + new UICommon().getWebID() + "/";
    }

    public static resMonitorFilterRecordV2 createFilter()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        resMonitorFilterRecordV2 filterDef = new resMonitorFilterRecordV2();
        if (HttpContext.Current.Session["ResMonFilterDef"] != null)
            filterDef = (resMonitorFilterRecordV2)HttpContext.Current.Session["ResMonFilterDef"];
        object _endDate = new TvBo.Common().getFormConfigValue("ResMonitor", "FilterDate");
        if (_endDate != null && Conversion.getBoolOrNull(_endDate).Value)
            filterDef.EndDate1 = TvTools.Conversion.ConvertToDateString(DateTime.Today.Date);
        filterDef.DateLang = UserData.Ci.Parent.ToString();
        filterDef.AgencyUser = UserData.UserID;
        filterDef.AgencyOffice = UserData.AgencyID;
        filterDef.UserSeeAllReservation = UserData.ShowAllRes;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            filterDef.UserSeeAllReservation = true;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr))
            filterDef.ShowDraft = false;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Qasswa))
            filterDef.ShowFilterDraft = true;
        bool? showFilterOnlyPxmRes=Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "ShowFilterPxmReservation"));
        filterDef.ShowFilterOnlyPxmRes = showFilterOnlyPxmRes.HasValue ? showFilterOnlyPxmRes.Value : false;
        return filterDef;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static resMonitorFilterDataV2 CreateFilterData(string clear)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        bool? _clear = Conversion.getBoolOrNull(clear);
        if ((_clear.HasValue ? _clear.Value : true))
            HttpContext.Current.Session["ResMonFilterDef"] = null;
        bool sessionIsFull = false;

        List<resMonitorDefaultRecord> defaultData = null;

        if (HttpContext.Current.Session["ResMonDefaultData"] != null)
            defaultData = (List<resMonitorDefaultRecord>)HttpContext.Current.Session["ResMonDefaultData"];
        else
        {
            defaultData = new ReservationMonitor().getResMonDefaultData(UserData, ref errorMsg);
            HttpContext.Current.Session["ResMonDefaultData"] = defaultData;
        }
            

        resMonitorFilterDataV2 filterData = new resMonitorFilterDataV2();
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        if (HttpContext.Current.Session["ResMonFilterDef"] != null) {
            filterData.Filter = (resMonitorFilterRecordV2)HttpContext.Current.Session["ResMonFilterDef"];
            sessionIsFull = true;
        }
        else
            filterData.Filter = createFilter();
        filterData.Filter.UserSeeAllReservation = UserData.ShowAllRes;

        filterData.HolPackData = new ReservationMonitor().getHolPacks(useLocalName, defaultData);
        filterData.AgencyOfficeData = new ReservationMonitor().getAgencyOffices(useLocalName, defaultData);
        filterData.AgencyUserData = new ReservationMonitor().getAgencyUser(useLocalName, defaultData);
        filterData.DepCityData = new ReservationMonitor().getDepCity(useLocalName, defaultData);
        filterData.ArrCityData = new ReservationMonitor().getArrCity(useLocalName, defaultData);
        filterData.CurrencyData = new ReservationMonitor().getCurrency(useLocalName, UserData.Market, defaultData);
        filterData.CustomRegID = UserData.CustomRegID;
        if (filterData.AgencyUserData.Count > 0) {
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
                filterData.Filter.AgencyUser = string.Empty;
            else
                if (!sessionIsFull)
                    filterData.Filter.AgencyUser = UserData.UserID;

            if (!(filterData.Filter.UserSeeAllReservation.HasValue && filterData.Filter.UserSeeAllReservation.Value) && filterData.AgencyUserData.Where(w => w.Code != UserData.UserID).Count() > 0)
                foreach (listString row in filterData.AgencyUserData.Where(w => w.Code != UserData.UserID).Select(s => s).ToList<listString>())
                    filterData.AgencyUserData.Remove(row);
        }

        filterData.Filter.DateLang = UserData.Ci.Parent.ToString();
        filterData.OptionDateControl = Equals(UserData.CustomRegID, TvBo.Common.crID_DreamHolidays) || Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) || Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || Equals(UserData.CustomRegID, TvBo.Common.crID_Qasswa);
        filterData.ShowDraftControl = Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr);
        filterData.NoCheckOnlyActiveUsers = (Equals(UserData.CustomRegID, TvBo.Common.crID_FilipTravel) || 
                                             Equals(UserData.CustomRegID, TvBo.Common.crID_Emerald)) && UserData.ShowAllRes;

        if (string.Equals(UserData.CustomRegID, Common.crID_Qasswa))
            filterData.DetailType = 1;
        if (string.Equals(UserData.CustomRegID, Common.crID_Qasswa))
            filterData.ShowFilterDraft = true;
        bool? showFilterOnlyPxmRes = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "ShowFilterPxmReservation"));
        filterData.ShowFilterOnlyPxmRes = showFilterOnlyPxmRes.HasValue ? showFilterOnlyPxmRes.Value : false;
        return filterData;
    }

    [WebMethod(EnableSession = true)]
    public static string getGridHeaders()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<dataTableColumnsDef> aoColumns = new List<dataTableColumnsDef>();
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            aoColumns.Add(new dataTableColumnsDef { IDNo = 0, ColName = "oc", sTitle = "<img src=\"Images/CancelSorting.png\" style=\"width:30px; height:30px;\"/>", sWidth = "20px", bSortable = true });
        else
            aoColumns.Add(new dataTableColumnsDef { IDNo = 0, ColName = "oc", sTitle = "", sWidth = "20px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 1, ColName = "ResNo", sTitle = HttpContext.GetGlobalResourceObject("ResMonitor", "lblReservationnumber").ToString(), sWidth = "90px", bSortable = true, sClass = "reservationNo" });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 2, ColName = "lblInfo", sTitle = "#", sWidth = "50px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 3, ColName = "LeaderName", sTitle = HttpContext.GetGlobalResourceObject("ResMonitor", "lblLeaderName").ToString(), sWidth = "120px", bSortable = true });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 4, ColName = "HotelName", sTitle = HttpContext.GetGlobalResourceObject("ResMonitor", "lblHotel").ToString(), sWidth = "150px", bSortable = true });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 5, ColName = "Adult", sTitle = HttpContext.GetGlobalResourceObject("ResMonitor", "lblAdult").ToString(), sWidth = "35px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 6, ColName = "Child", sTitle = HttpContext.GetGlobalResourceObject("ResMonitor", "lblChild").ToString(), sWidth = "35px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 7, ColName = "BegDate", sTitle = HttpContext.GetGlobalResourceObject("ResMonitor", "lblBeginDate").ToString(), sWidth = "75px", bSortable = true, sType = "date" });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 8, ColName = "Days", sTitle = HttpContext.GetGlobalResourceObject("ResMonitor", "lblNight").ToString(), sWidth = "35px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 9, ColName = "lblStatus", sTitle = HttpContext.GetGlobalResourceObject("ResMonitor", "lblStatus").ToString(), sWidth = "60px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 10, ColName = "SPrice", sTitle = HttpContext.GetGlobalResourceObject("ResMonitor", "lblSalePrice").ToString(), sWidth = "85px", bSortable = false, sType = "numeric", sClass = "numericStyle" });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 11, ColName = "SaleCur", sTitle = HttpContext.GetGlobalResourceObject("ResMonitor", "lblCurr").ToString(), sWidth = "35px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 12, ColName = "DepCityName", sTitle = HttpContext.GetGlobalResourceObject("ResMonitor", "lblDepartureCity").ToString(), sWidth = "90px", bSortable = true });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 13, ColName = "ArrCityName", sTitle = HttpContext.GetGlobalResourceObject("ResMonitor", "lblArrivalCity").ToString(), sWidth = "90px", bSortable = true });
        var retVal = from q in aoColumns.AsEnumerable()
                     select new { sTitle = q.sTitle, sWidth = q.sWidth, bSortable = q.bSortable, sType = q.sType, sClass = q.sClass };
        return Newtonsoft.Json.JsonConvert.SerializeObject(retVal);
    }

    public static string createResultGridDetail(User UserData, resMonitorRecord row)
    {
        StringBuilder sb = new StringBuilder();
        bool? showPaymentPlan = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "ShowPaymentPlan"));
        sb.Append("<div style=\"float: left; width: 650px;\">");
        sb.Append("<table width=\"100%\"><tr>");
        #region Row 1
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblCatalogPrice"));
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0}</span></td>", row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") : "&nbsp;");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblDueToPay"));
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0}</span></td>", row.DueAgentPayment.HasValue ? row.DueAgentPayment.Value.ToString("#,###.00") : "&nbsp;");
        #endregion
        sb.Append("</tr><tr>");
        #region Row 2
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblDiscount"));
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0}</span></td>", row.Discount.HasValue ? row.Discount.Value.ToString("#,###.00") : "&nbsp;");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblActuallyPaid"));
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0}</span></td>", row.AgencyPayment.HasValue ? row.AgencyPayment.Value.ToString("#,###.00") : "&nbsp;");
        #endregion
        sb.Append("</tr><tr>");
        #region Row 3
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB"));
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0}</span></td>", row.EBAgency.HasValue ? row.EBAgency.Value.ToString("#,###.00") : "&nbsp;");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblBalance"));
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\"><b>{0}</b></span></td>", row.Balance.HasValue ? row.Balance.Value.ToString("#,###.00") : "&nbsp;");
        #endregion
        sb.Append("</tr><tr>");
        #region Row 4
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblPassangerEB"));
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0}</span></td>", row.EBPas.HasValue ? row.EBPas.Value.ToString("#,###.00") : "&nbsp;");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblPaymentDueDates"));
        if(!showPaymentPlan.HasValue || showPaymentPlan.Value)
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\" onclick=\"showPaymentPlan('{1}', '{2}')\" style=\"cursor: pointer; text-decoration: underline;\">{0}</span></td>",
                HttpContext.GetGlobalResourceObject("ResMonitor", "lblView"),
                row.ResNo,
                UserData.Ci.LCID);
        #endregion
        sb.Append("</tr><tr>");
        #region Row 5

        if (string.Equals(UserData.CustomRegID, Common.crID_SunFun)) {
            sb.Append("<td class=\"priceDetailCaption\">&nbsp;</td>");
            sb.Append("<td class=\"priceDetailValue\"><span class=\"right\">&nbsp;</span></td>");
        } else {
            sb.AppendFormat("<td class=\"priceDetailCaption\">{1}{0}</td>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgentCommision"),
            (UserData.AgencyRec.AgencyType == 0 && row.BrokerCom.HasValue) ? HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "BrokerCom").ToString() + "<br />" : "");

            sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{2}{0} {1}</span></td>",
                    row.AgencyComPer.HasValue ? "(%" + Math.Abs(row.AgencyComPer.Value).ToString("#.00") + ")" : "&nbsp;",
                    row.AgencyCom.HasValue ? row.AgencyCom.Value.ToString("#,###.00") : "&nbsp;",
                    UserData.AgencyRec.AgencyType == 0 && row.BrokerCom.HasValue ? (row.BrokerComPer.HasValue ? "(%" + Math.Abs(row.BrokerComPer.Value).ToString("#.00") + ")" : "") + row.BrokerCom.Value.ToString("#.00") + "<br />" : "");
        }

        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblInvoiceToAgency"));
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0}</span></td>", row.InvoiceNo);
        #endregion
        sb.Append("</tr><tr>");
        #region Row 6
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyDiscountFromCommision"));
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0} {1}</span></td>",
            row.AgencyDisPasPer.HasValue ? "(%" + Math.Abs(row.AgencyDisPasPer.Value).ToString("#.00") + ")" : "&nbsp;",
            row.AgencyDisPasVal.HasValue ? row.AgencyDisPasVal.Value.ToString("#.00") : "&nbsp;");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblCommisionInvoiceNumber"));
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\" onclick=\"showCommissionInvoice('{1}', '{2}');\" style=\"cursor: pointer; text-decoration: underline;\">{0}</span></td>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblShow"),
            row.ResNo,
            UserData.Ci.LCID);
        #endregion
        sb.Append("</tr></table>");

        sb.Append("</div>");
        sb.Append("<div style=\"float:left; width: 280px;\">");
        sb.Append("<table width=\"100%\">");
        sb.AppendFormat("<tr><td class=\"leftTD\"><strong>{0}</strong></td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblReservationDate"),
            row.ResDate.HasValue ? row.ResDate.Value.ToShortDateString() : "&nbsp;");
        sb.AppendFormat("<tr><td class=\"leftTD\"><strong>{0}</strong></td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyUser"),
            row.AgencyUser);
        sb.AppendFormat("<tr><td class=\"leftTD\"><strong>{0}</strong></td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblRecordChangeDate"),
            row.ChgDate);
        sb.AppendFormat("<tr><td class=\"leftTD\"><strong>{0}</strong></td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblReadByOperator"),
            row.ReadByOpr);
        sb.AppendFormat("<tr><td class=\"leftTD\"><strong>{0}</strong></td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblReadByOperatorUser"),
            row.ReadByOprUser);
        sb.AppendFormat("<tr><td class=\"leftTD\"><strong>{0}</strong></td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblReadByOperatorDate"),
            row.ReadByOprDate.HasValue ? row.ReadByOprDate.Value.ToShortDateString() : "&nbsp;");
        sb.Append("</table>");
        sb.Append("</div>");

        return sb.ToString();
    }

    public static object getObject(TvBo.resMonitorRecord row, string fieldName)
    {
        System.Reflection.PropertyInfo[] oProps = null;
        oProps = row.GetType().GetProperties();
        List<System.Reflection.PropertyInfo> _pi = (from q in oProps
                                                    where q.Name == fieldName
                                                    select q).ToList<System.Reflection.PropertyInfo>();
        object value = null;
        if (_pi != null && _pi.Count() > 0) {
            System.Reflection.PropertyInfo pi = _pi[0];
            value = pi.GetValue(row, null) == null ? DBNull.Value : pi.GetValue(row, null);
        }
        return value;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object createGridDetailType1(string resNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        resMonitorRecord row = new ReservationMonitor().getReservation(UserData, resNo, ref errorMsg);
        List<ResServiceRecord> serviceList = new ResTables().getResServiceList(UserData, resNo, null, ref errorMsg);

        var data = new {
            lblCatalogPrice = HttpContext.GetGlobalResourceObject("ResMonitor", "lblCatalogPrice").ToString(),
            lblActuallyPaid = HttpContext.GetGlobalResourceObject("ResMonitor", "lblActuallyPaid").ToString(),
            lblDiscount = HttpContext.GetGlobalResourceObject("ResMonitor", "lblDiscount").ToString(),
            lblUsedAgencyBonus = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "UsedAgencyBonus").ToString(),
            lblAgencyEB = HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB").ToString(),
            lblUsedUserBonus = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "UsedUserBonus").ToString(),
            lblPassangerEB = HttpContext.GetGlobalResourceObject("ResMonitor", "lblPassangerEB").ToString(),
            lblUsedPasBonus = HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "UsedPasBonus").ToString(),
            lblAgencyDiscountFromCommision = HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyDiscountFromCommision").ToString(),
            lblBalance = HttpContext.GetGlobalResourceObject("ResMonitor", "lblBalance").ToString(),
            lblAgentCommision = HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgentCommision").ToString(),
            lblInvoiceToAgency = HttpContext.GetGlobalResourceObject("ResMonitor", "lblInvoiceToAgency").ToString(),
            lblDueToPay = HttpContext.GetGlobalResourceObject("ResMonitor", "lblDueToPay").ToString(),
            lblPaymentDueDates = HttpContext.GetGlobalResourceObject("ResMonitor", "lblPaymentDueDates").ToString(),
            lblView = HttpContext.GetGlobalResourceObject("ResMonitor", "lblView").ToString(),
            SalePrice = row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#.00") : "&nbsp;",
            AgencyPayment = row.AgencyPayment.HasValue ? row.AgencyPayment.Value.ToString("#.00") : "&nbsp;",
            Discount = row.Discount.HasValue ? row.Discount.Value.ToString("#.00") : "&nbsp;",
            AgencyBonusAmount = row.AgencyBonusAmount.HasValue ? row.AgencyBonusAmount.Value.ToString("#.00") : "&nbsp;",
            EBAgency = row.EBAgency.HasValue ? row.EBAgency.Value.ToString("#.00") : "&nbsp;",
            UserBonusAmount = row.UserBonusAmount.HasValue ? row.UserBonusAmount.Value.ToString("#.00") : "&nbsp;",
            EBPas = row.EBPas.HasValue ? row.EBPas.Value.ToString("#.00") : "&nbsp;",
            PasBonusAmount = row.PasBonusAmount.HasValue ? row.PasBonusAmount.Value.ToString("#.00") : "&nbsp;",
            AgencyDisPasPer = row.AgencyDisPasPer.HasValue ? row.AgencyDisPasPer.Value.ToString("#.00") : "&nbsp;",
            AgencyDisPasVal = row.AgencyDisPasVal.HasValue ? row.AgencyDisPasVal.Value.ToString("#.00") : "&nbsp;",
            Balance = row.Balance.HasValue ? row.Balance.Value.ToString("#.00") : "&nbsp;",
            AgencyComPer = row.AgencyComPer.HasValue ? row.AgencyComPer.Value.ToString("#.00") : "&nbsp;",
            AgencyCom = row.AgencyCom.HasValue ? row.AgencyCom.Value.ToString("#.00") : "&nbsp;",
            InvoiceNo = string.IsNullOrEmpty(row.InvoiceNo) ? row.InvoiceNo.ToString() : "&nbsp;",
            DueAgentPayment = row.DueAgentPayment.HasValue ? row.DueAgentPayment.Value.ToString() : "&nbsp;",
            ResNo = row.ResNo.ToString(),
            CultureInfo_LCID = UserData.Ci.LCID.ToString(),
            ServiceConfList = from q in serviceList
                              select new { ServiceName = q.ServiceName, Confirmation = q.StatConfName }
        };

        string template = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + new UICommon().getWebID() + "\\" + "ResMonTemplateType1.html";

        string retVal = Nustache.Core.Render.FileToString(template, data, null);

        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string createGridDetail(string ResNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        resMonitorRecord row = new ReservationMonitor().getReservation(UserData, ResNo, ref errorMsg);
        string tmpl = getDetailTemplate(UserData);
        int lastPosition = 0;
        if (string.IsNullOrEmpty(tmpl))
            return createResultGridDetail(UserData, row);
        string retVal = string.Empty;
        string tmpTemplate = tmpl;
        bool exit = true;
        while (exit) {
            string valueTemp = string.Empty;
            int first = tmpTemplate.IndexOf("{[");
            if (first > -1) {
                lastPosition = first;
                tmpTemplate = tmpTemplate.Remove(0, first + 2);
                int last = tmpTemplate.IndexOf("]}");
                valueTemp = tmpTemplate.Substring(0, last);
                tmpTemplate = tmpTemplate.Remove(0, last + 2);
                string[] local = valueTemp.Trim().Split(',');
                if (local.Length > 1) {
                    string classKey = local[0].Trim('\"');
                    string resourceKey = local[1].Trim('\"');
                    if (classKey == "css" && resourceKey == "showBrokerCom") {
                        if (!UserData.MainAgency)
                            tmpl = tmpl.Replace("{[" + valueTemp + "]}", "ui-helper-hidden");
                        else
                            tmpl = tmpl.Replace("{[" + valueTemp + "]}", "");
                    } else {
                        string localStr = HttpContext.GetGlobalResourceObject(classKey, resourceKey).ToString();
                        tmpl = tmpl.Replace("{[" + valueTemp + "]}", localStr);
                    }
                }
            } else
                exit = false;
        }
        tmpTemplate = tmpl;
        exit = true;
        while (exit) {
            string valueTemp = string.Empty;
            string valueFormat = string.Empty;
            int first = tmpTemplate.IndexOf("{");
            if (first > -1) {
                lastPosition = first;
                tmpTemplate = tmpTemplate.Remove(0, first + 1);
                int last = tmpTemplate.IndexOf("}");
                valueTemp = tmpTemplate.Substring(0, last);
                if (valueTemp.Split('|').Length > 1) {
                    valueFormat = valueTemp.Split('|')[1];
                    valueTemp = valueTemp.Split('|')[0];
                }
                valueFormat = valueFormat.Trim('\"');
                valueTemp = valueTemp.Trim('\"');
                tmpTemplate = tmpTemplate.Remove(0, last + 1);

                string valueStr = string.Empty;
                if (valueTemp.IndexOf('.') > -1) {
                    if (Equals(valueTemp, "UserData.Ci.LCID"))
                        valueStr = UserData.Ci.LCID.ToString();
                } else {
                    if (valueTemp.IndexOf('-') > -1) {
                        string[] valuelist = valueTemp.Split('-');
                        List<TvTools.objectList> objectValues = new List<objectList>();
                        Type _type = typeof(System.String);
                        for (int i = 0; i < valuelist.Length; i++) {
                            object value = getObject(row, valuelist[i]);
                            objectValues.Add(new TvTools.objectList {
                                TypeName = value.GetType().Name,
                                Value = value
                            });
                        }
                        object obj = mathLib.returnFormulaMinus(objectValues, ref _type);
                        valueStr = Conversion.getObjectToString(obj, _type, valueFormat);
                    } else
                        if (valueTemp.IndexOf('+') > -1) {
                        string[] valuelist = valueTemp.Split('+');
                        List<TvTools.objectList> objectValues = new List<objectList>();
                        Type _type = typeof(System.String);
                        for (int i = 0; i < valuelist.Length; i++) {
                            object value = getObject(row, valuelist[i]);
                            objectValues.Add(new TvTools.objectList {
                                TypeName = value.GetType().Name,
                                Value = value
                            });
                        }
                        object obj = mathLib.returnFormulaPlus(objectValues, ref _type);
                        valueStr = Conversion.getObjectToString(obj, _type, valueFormat);
                    } else {
                        object value = getObject(row, valueTemp);
                        valueStr = Conversion.getObjectToString(value, value.GetType(), valueFormat);
                    }
                    tmpl = tmpl.Replace("{\"" + valueTemp + (valueFormat.Length > 0 ? "\"|\"" + valueFormat : "") + "\"}", valueStr);
                }
                tmpl = tmpl.Replace("{\"" + valueTemp + (valueFormat.Length > 0 ? "\"|\"" + valueFormat : "") + "\"}", valueStr);
            } else
                exit = false;
        }
        return tmpl;
    }

    public static string getDetailTemplate(User UserData)
    {
        string sb = string.Empty;
        string filePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + new UICommon().getWebID() + "\\ResMonTemplateV2.tmpl";
        if (System.IO.File.Exists(filePath)) {
            System.IO.StreamReader Tex = new System.IO.StreamReader(filePath);
            sb = Tex.ReadToEnd();
            Tex.Close();
        } else {
            filePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + new UICommon().getWebID() + "\\ResMonTemplate.tmpl";
            if (System.IO.File.Exists(filePath)) {
                System.IO.StreamReader Tex = new System.IO.StreamReader(filePath);
                sb = Tex.ReadToEnd();
                Tex.Close();
            }
        }
        return sb;
    }

    public static string createResultGrid(List<resMonitorRecordV2> data, int PageNo, int PageDataCount)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (PageDataCount == 0)
            PageDataCount = 10;
        int cnt = 0;
        int beg = (PageNo - 1) * PageDataCount;
        int end = (PageNo * PageDataCount) - 1;
        string detailTemplate = getDetailTemplate(UserData);
        StringBuilder sb = new StringBuilder();
        #region gridPager
        int? recordCount = data.Count;
        if (end > recordCount)
            end = recordCount.Value;
        decimal? _pageCount = Convert.ToDecimal(recordCount) / PageDataCount;

        int pageCount = (int)_pageCount != _pageCount ? (int)_pageCount + 1 : (int)_pageCount;
        string pageNrList = "";
        for (int i = 0; i < pageCount; i++) {
            if (PageNo == (i + 1))
                pageNrList += string.Format("<option value=\"{0}\" selected=\"selected\">{1}</option>", i + 1, i + 1);
            else
                pageNrList += string.Format("<option value=\"{0}\">{1}</option>", i + 1, i + 1);
        }
        sb.Append("<div class=\"reservationPager ui-state-hover\">");
        sb.AppendFormat("<div style= \"width: 200px; float:left\"><span>{0} :</span><select id=\"pageList\" onchange=\"pageChange();\">{1}</select>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblPage"),
            pageNrList);
        sb.AppendFormat("<input id=\"PageNo\" type=\"hidden\" value=\"{0}\" /></div>", PageNo);

        sb.Append("<div style=\"width:200px; float:left;\">");
        string pageListStr = string.Empty;

        pageListStr += PageDataCount == 10 ? "<OPTION value=\"10\" selected=\"selected\" >10</OPTION>" : "<OPTION value=\"10\">10</OPTION>";
        pageListStr += PageDataCount == 25 ? "<OPTION value=\"25\" selected=\"selected\" >25</OPTION>" : "<OPTION value=\"25\">25</OPTION>";
        pageListStr += PageDataCount == 50 ? "<OPTION value=\"50\" selected=\"selected\" >50</OPTION>" : "<OPTION value=\"50\">50</OPTION>";
        pageListStr += PageDataCount == 100 ? "<OPTION value=\"100\" selected=\"selected\" >100</OPTION>" : "<OPTION value=\"100\">100</OPTION>";

        sb.AppendFormat("{0} <SELECT id=\"pageDataList\" onchange=\"pageDataCountChange();\">{1}</SELECT> {2}",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblShow"),
            pageListStr,
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblentries"));
        sb.Append("</div>");
        sb.AppendFormat("<div style=\"width:200px; float:right; \"><span style=\"bottom: 4px;\">{0} : </span><img alt=\"\" title=\"\" src=\"Images/html_icon.gif\" height=\"22px\" onclick=\"exportHtml();\" /></div>",
                HttpContext.GetGlobalResourceObject("ResMonitor", "lblExportTo"));
        //&nbsp;&nbsp;<img alt=\"\" title=\"\" src=\"Images/txt_icon.gif\" height=\"22px\" onclick=\"exportText();\" />
        //sb.AppendFormat("<div style=\"width:200px; float:left;\">Showing {0} to {1} of {2} entries</div>", beg + 1, end + 1, recordCount);
        sb.Append("</div>");
        #endregion

        #region gridHeader
        sb.Append("<div class=\"reservationHeaderRow ui-widget-header ui-priority-secondary\">");
        sb.AppendFormat("<div class=\"resNo\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblReservationnumber"));
        sb.AppendFormat("<div class=\"info1\">{0}<br />&nbsp;</div>", "#");
        sb.AppendFormat("<div class=\"leaderName\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblLeaderName"));
        sb.AppendFormat("<div class=\"hotelName\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblHotel"));
        sb.AppendFormat("<div class=\"adult\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblAdult"));
        sb.AppendFormat("<div class=\"child\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblChild"));
        sb.AppendFormat("<div class=\"beginDate\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblBeginDate"));
        sb.AppendFormat("<div class=\"days\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblNight"));
        sb.AppendFormat("<div class=\"status\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblStatus"));
        sb.AppendFormat("<div class=\"sPrice\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblSalePrice"));
        sb.AppendFormat("<div class=\"saleCur\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblCurr"));
        sb.AppendFormat("<div class=\"depCity\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblDepartureCity"));
        sb.AppendFormat("<div class=\"arrCity\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblArrivalCity"));
        sb.Append("</div>");
        #endregion

        for (int j = beg; j < end; j++) {
            resMonitorRecordV2 row = data[j];
            bool unRead = (row.ReadByOpr.HasValue && row.ReadByOpr.Value == 0);
            cnt++;
            #region gridDataRow
            if (Convert.ToInt32(cnt / 2.0) == (cnt / 2.0))
                sb.Append("<div class=\"reservationRow Odd\">");
            else
                sb.Append("<div class=\"reservationRow Even\">");
            sb.Append("<div class=\"resNo bold\">");
            sb.AppendFormat("<img id=\"openDetail{0}\" alt=\"{1}\" src=\"Images/infoOpen.gif\" onClick=\"openRow('{0}');\">&nbsp;<span class=\"span\" onclick=\"showResView('{2}');\">{3}</span>",
                cnt,
                HttpContext.GetGlobalResourceObject("ResMonitor", "lblOpenDetail"),
                row.ResNo,
                row.ResNo);
            sb.AppendFormat("<input type=\"hidden\" id=\"ocRowId{0}\" value=\"0\" />", cnt);
            sb.Append("</div>");
            string infoStr = string.Empty;
            if (row.NewComment)
                infoStr += string.Format("<img alt=\"{0}\" src=\"Images/mail_16.gif\" onclick=\"showResComment('{1}','{2}',{3})\" />",
                        HttpContext.GetGlobalResourceObject("ResMonitor", "lblNewComment"),
                        row.ResNo,
                        UserData.UserID,
                        UserData.Ci.LCID);
            if (!string.IsNullOrEmpty(row.ResNote)) {
                if (infoStr.Length > 0)
                    infoStr += "&nbsp;";
                infoStr += string.Format("<img alt=\"{0}\" src=\"Images/edit_16.gif\" onclick=\"showResNote({1})\" />",
                    HttpContext.GetGlobalResourceObject("ResMonitor", "lblShowReservationNote"),
                    UICommon.EncodeJsString(row.ResNote));
            }
            sb.AppendFormat("<div id=\"img_{1}\" class=\"info1\">{0}</div>",
                    string.IsNullOrEmpty(infoStr) ? "&nbsp;" : infoStr, row.ResNo);
            sb.AppendFormat("<div class=\"leaderName {1}\">{0}</div>",
                    !string.IsNullOrEmpty(row.LeaderName) ? row.LeaderName : "&nbsp;",
                    unRead ? "bold" : "");
            sb.AppendFormat("<div class=\"hotelName {1}\">{0}</div>",
                    !string.IsNullOrEmpty(row.HotelName) ? row.HotelName : "&nbsp;",
                    unRead ? "bold" : "");
            sb.AppendFormat("<div class=\"adult {1}\">{0}</div>",
                    row.Adult,
                    unRead ? "bold" : "");
            sb.AppendFormat("<div class=\"child {1}\">{0}</div>",
                    row.Child,
                    unRead ? "bold" : "");
            sb.AppendFormat("<div class=\"beginDate {1}\">{0}</div>",
                    row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "&nbsp;",
                    unRead ? "bold" : "");
            sb.AppendFormat("<div class=\"days {1}\">{0}</div>",
                    row.Days,
                    unRead ? "bold" : "");
            string statusStr = string.Format("<img src=\"Images/Status/ResStatus{0}.gif\" width=\"16\" height=\"16\" alt=\"{1}\"><img src=\"Images/Status/ConfStatus{2}.gif\" width=\"16\" height=\"16\" alt=\"{3}\"><img src=\"Images/Status/PayStatus{4}.gif\" width=\"16\" height=\"16\" alt=\"{5}\">",
                    row.ResStat.ToString(),
                    HttpContext.GetGlobalResourceObject("ResMonitor", "lblResStatus").ToString() + ", " +
                        (row.ResStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + row.ResStat.ToString()).ToString() : ""),
                    row.ConfStat.ToString(),
                    HttpContext.GetGlobalResourceObject("ResMonitor", "lblConfStatus").ToString() + ", " +
                        (row.ConfStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + row.ConfStat.ToString()).ToString() : ""),
                    row.PaymentStat.ToString(),
                    HttpContext.GetGlobalResourceObject("ResMonitor", "lblPaymentStatus").ToString() + ", " +
                        (!string.IsNullOrEmpty(row.PaymentStat) ? HttpContext.GetGlobalResourceObject("LibraryResource", "PayStatus" + row.PaymentStat).ToString() : ""));
            sb.AppendFormat("<div class=\"status\">{0}</div>",
                    statusStr);
            sb.AppendFormat("<div class=\"sPrice\"><b>{0}</b></div>",
                    row.SPrice.HasValue ? row.SPrice.Value.ToString("#,###.00") : "&nbsp;");
            sb.AppendFormat("<div class=\"saleCur {1}\">{0}</div>",
                    !string.IsNullOrEmpty(row.SaleCur) ? row.SaleCur : "&nbsp;",
                    unRead ? "bold" : "");
            sb.AppendFormat("<div class=\"depCity {1}\">{0}</div>",
                    !string.IsNullOrEmpty(row.DepCityName) ? row.DepCityName : "&nbsp;",
                    unRead ? "bold" : "");
            sb.AppendFormat("<div class=\"arrCity {1}\">{0}</div>",
                    !string.IsNullOrEmpty(row.ArrCityName) ? row.ArrCityName : "&nbsp;",
                    unRead ? "bold" : "");

            #region gridDataRow Detail

            sb.Append("<div style=\"clear:both; border:none; font-weight: normal;\">");

            #region gridDataRow Detail

            //sb.Append(createResultGridDetailTemplate(UserData, row, cnt, detailTemplate));

            #endregion

            sb.Append("</div>");

            #endregion gridDataRow Detail

            sb.Append("</div>");

            #endregion gridDataRow
        }

        #region Total Row
        sb.Append("<div class=\"reservationRow\">");
        sb.AppendFormat("<div class=\"resNo\"><b>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblTotal").ToString() + ": {0}</b></div>", data.Count);
        sb.Append("<div class=\"info1\">&nbsp;</div>");
        sb.Append("<div class=\"leaderName\">&nbsp;</div>");
        sb.Append("<div class=\"hotelName\">&nbsp;</div>");
        sb.AppendFormat("<div class=\"adult\"><b>{0}</b></div>", data.Sum(s => s.Adult).ToString());
        sb.AppendFormat("<div class=\"child\"><b>{0}</b></div>", data.Sum(s => s.Child).ToString());
        sb.Append("<div class=\"beginDate\">&nbsp;</div>");
        sb.Append("<div class=\"days\">&nbsp;</div>");
        sb.Append("<div class=\"status\">&nbsp;</div>");
        //var TotalPrice = from q in data
        //                 group q by new { q.SaleCur } into k
        //                 select new { Currency = k.Key.SaleCur, SPrice = data.Sum(s => s.SPrice.HasValue ? s.SalePrice.Value : 0) };
        var TotalPrice = from q in data
                         group q by new { q.SaleCur } into k
                         select new { Currency = k.Key.SaleCur, SPrice = data.Where(w => w.SaleCur == k.Key.SaleCur).Sum(s => s.SPrice.HasValue ? s.SPrice.Value : 0) };
        string sumPrice = string.Empty;
        string sumPriceCurr = string.Empty;
        foreach (var r in TotalPrice) {
            if (sumPrice.Length > 0)
                sumPrice += "<br />";
            if (sumPriceCurr.Length > 0)
                sumPriceCurr += "<br />";
            sumPrice += string.Format("{0}", r.SPrice.ToString("#,###.00"));
            sumPriceCurr += string.Format("{0}", r.Currency);
        }
        sb.AppendFormat("<div class=\"sPrice\"><b>{0}</b></div>", sumPrice);
        sb.AppendFormat("<div class=\"saleCur\">{0}</div>", sumPriceCurr);
        sb.Append("<div class=\"depCity\">&nbsp;</div>");
        sb.Append("<div class=\"arrCity\">&nbsp;</div>");
        sb.Append("</div>");
        #endregion

        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static bool getData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return false; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        List<resMonitorRecordV2> dataResMain = new List<resMonitorRecordV2>();

        string errorMsg = string.Empty;
        resMonitorFilterRecordV2 filter = (resMonitorFilterRecordV2)HttpContext.Current.Session["ResMonFilterDef"];

        string AgencyUser = UserData.UserID;
        if (UserData.ShowAllRes)
            AgencyUser = filter.AgencyUser;
        else
            AgencyUser = UserData.UserID;

        Int16 OptionControl = 0;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) || Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || Equals(UserData.CustomRegID, TvBo.Common.crID_DreamHolidays))
            OptionControl = filter.ShowOptionRes.HasValue ? filter.ShowOptionRes.Value : Convert.ToInt16(0);
        else
            OptionControl = Convert.ToInt16(0);

        try {
            List<resMonitorRecordV2> _dataResMain = new ReservationMonitor().getResMonitorData_V2(UserData, AgencyUser, filter.AgencyOffice,
                        filter.ResNo1, filter.ResNo2, Conversion.getInt32OrNull(filter.DepCity), Conversion.getInt32OrNull(filter.ArrCity),
                        TvTools.Conversion.ConvertFromString(filter.BegDate1), TvTools.Conversion.ConvertFromString(filter.BegDate2),
                        TvTools.Conversion.ConvertFromString(filter.EndDate1), TvTools.Conversion.ConvertFromString(filter.EndDate2),
                        TvTools.Conversion.ConvertFromString(filter.ResDate1), TvTools.Conversion.ConvertFromString(filter.ResDate2),
                        filter.HolPack, filter.LeaderSurname, filter.LeaderName, filter.ResStatus, filter.ConfStatus, filter.PayStatus,
                        OptionControl, (filter.ShowDraft.HasValue && filter.ShowDraft.Value), filter.Currency, filter.ShowOnlyActiveUsers, filter.ShowOnlyPxmRes, ref errorMsg);
            if (_dataResMain.Count > 0) {
                CreateAndWriteFile(_dataResMain, false);
                return true;
            } else {
                CreateAndWriteFile(null, true);
                return false;
            }
        }
        catch {
            CreateAndWriteFile(null, true);
            return false;
        }
    }

    private static void CreateAndWriteFile(List<resMonitorRecordV2> result, bool clearData)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\ResPaymentMonitor." + HttpContext.Current.Session.SessionID;
        if (clearData) {
            if (File.Exists(path))
                File.Delete(path);
            return;
        }

        if (File.Exists(path))
            File.Delete(path);
        FileStream f = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
        try {
            StreamWriter writer = new StreamWriter(f);
            string compress = TvTools.GZipCompres.Compress(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            writer.Write(compress);
            writer.Close();
        }
        catch (Exception) {
            throw;
        }
        finally {
            f.Close();
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static bool SaveFilter(string data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return false; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResMonFilterDef"] == null) {
            resMonitorFilterRecordV2 filterDef = createFilter();
            HttpContext.Current.Session["ResMonFilterDef"] = filterDef;
        }

        //string data = Newtonsoft.Json.JsonConvert.SerializeObject(datedata);

        JObject jObj = (JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(data.Replace('|', '"').Replace('<', '{').Replace('>', '}'));
        resMonitorFilterRecordV2 _data = new resMonitorFilterRecordV2();

        foreach (JToken row in ((JObject)jObj).AsJEnumerable()) {
            JProperty obj = (JProperty)row;
            switch (obj.Name) {
                case "AgencyOffice":
                    _data.AgencyOffice = ((JValue)obj.Value).Value<string>();
                    break;
                case "AgencyUser":
                    _data.AgencyUser = ((JValue)obj.Value).Value<string>();
                    break;
                case "ArrCity":
                    _data.ArrCity = Conversion.getInt32OrNull(((JValue)obj.Value).Value<string>());
                    break;
                case "BegDate1":
                    _data.BegDate1 = ((JValue)obj.Value).Value<string>();
                    break;
                case "BegDate2":
                    _data.BegDate2 = ((JValue)obj.Value).Value<string>();
                    break;
                case "ConfStatus":
                    _data.ConfStatus = ((JValue)obj.Value).Value<string>();
                    break;
                case "Currency":
                    _data.Currency = ((JValue)obj.Value).Value<string>();
                    break;
                case "DateLang":
                    _data.DateLang = ((JValue)obj.Value).Value<string>();
                    break;
                case "DepCity":
                    _data.DepCity = Conversion.getInt32OrNull(((JValue)obj.Value).Value<string>());
                    break;
                case "EndDate1":
                    _data.EndDate1 = ((JValue)obj.Value).Value<string>();
                    break;
                case "EndDate2":
                    _data.EndDate2 = ((JValue)obj.Value).Value<string>();
                    break;
                case "HolPack":
                    _data.HolPack = ((JValue)obj.Value).Value<string>();
                    break;
                case "LeaderName":
                    _data.LeaderName = ((JValue)obj.Value).Value<string>();
                    break;
                case "LeaderSurname":
                    _data.LeaderSurname = ((JValue)obj.Value).Value<string>();
                    break;
                case "PayDate1":
                    _data.PayDate1 = ((JValue)obj.Value).Value<long?>();
                    break;
                case "PayDate2":
                    _data.PayDate2 = ((JValue)obj.Value).Value<long?>();
                    break;
                case "PayStatus":
                    _data.PayStatus = ((JValue)obj.Value).Value<string>();
                    break;
                case "ResDate1":
                    _data.ResDate1 = ((JValue)obj.Value).Value<string>();
                    break;
                case "ResDate2":
                    _data.ResDate2 = ((JValue)obj.Value).Value<string>();
                    break;
                case "ResNo1":
                    _data.ResNo1 = ((JValue)obj.Value).Value<string>();
                    break;
                case "ResNo2":
                    _data.ResNo2 = ((JValue)obj.Value).Value<string>();
                    break;
                case "ResStatus":
                    _data.ResStatus = ((JValue)obj.Value).Value<string>();
                    break;
                case "ShowDraft":
                    _data.ShowDraft = ((JValue)obj.Value).Value<bool?>();
                    break;
                case "ShowOptionRes":
                    _data.ShowOptionRes = ((JValue)obj.Value).Value<Int16?>();
                    break;
                case "UserSeeAllReservation":
                    _data.UserSeeAllReservation = ((JValue)obj.Value).Value<bool?>();
                    break;
                case "showOnlyActiveUsers":
                    _data.ShowOnlyActiveUsers = ((JValue)obj.Value).Value<bool>();
                    break;
                case "ShowPxmRes":
                    _data.ShowOnlyPxmRes = ((JValue)obj.Value).Value<bool>();
                    break;
            }
        }
        //resMonitorFilterRecordV2 _data = Newtonsoft.Json.JsonConvert.DeserializeObject<resMonitorFilterRecordV2>(data.Replace('|', '"').Replace('<', '{').Replace('>', '}'));
        HttpContext.Current.Session["ResMonFilterDef"] = _data;
        return true;
    }

    [WebMethod(EnableSession = true)]
    public static string setReadCommentData(string ResNo, string UserID, int? RecID, int? lcID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string retVal = string.Empty;
        retVal = new TvBo.ReservationMonitor().setReadComment(UserID, ResNo, RecID.HasValue ? RecID.Value : -1, lcID.HasValue ? lcID.Value : -1);
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string getCommentData(string ResNo, string UserID, int? lcID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        return new TvBo.ReservationMonitor().getCommentDataString(ResNo, UserID, lcID.HasValue ? lcID.Value : -1);
    }

    [WebMethod(EnableSession = true)]
    public static string setCommentData(string ResNo, string Comment, string UserID, int? lcID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string retVal = string.Empty;
        retVal = new TvBo.ReservationMonitor().setComment(UserData, ResNo, Comment, lcID.HasValue ? lcID.Value : -1);
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string getCommisionInvoices(string ResNo, int? lcID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();
        List<CommisionInvoiceNumberRecord> list = new ReservationMonitor().getCommisionInvoiceNumber(ResNo, ref errorMsg);
        if (list.Count > 0) {
            sb.Append("<br />");
            sb.Append("<table style=\"font-family: Tahoma; font-size: small\">");
            sb.Append(" <tr>");
            sb.Append("     <td align=\"center\" style=\"font-weight:bold\">" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblInvSerie").ToString() + "</td>");
            sb.Append("     <td align=\"center\" style=\"font-weight:bold\">" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblInvNo").ToString() + "</td>");
            sb.Append(" </tr>");
            foreach (CommisionInvoiceNumberRecord row in list) {
                sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>",
                    row.InvSerial,
                    row.InvNo);
            }
            sb.Append("</table>");
        }
        return list.Count > 0 ? sb.ToString() : HttpContext.GetGlobalResourceObject("ResMonitor", "msgNoCommisionInvoice").ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getTotalBonus()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        UserBonusTotalRecord agencyBonus = new Bonus().getBonus(DateTime.Today, UserData.SaleCur, "A", UserData.AgencyID, ref errorMsg);
        UserBonusTotalRecord userBonus = new Bonus().getBonus(DateTime.Today, UserData.SaleCur, "U", UserData.PIN, ref errorMsg);
        StringBuilder sb = new StringBuilder();

        if (agencyBonus == null && userBonus == null)
            return "";
        if ((!UserData.Bonus.BonusUserSeeOwnW && !UserData.Bonus.BonusAgencySeeOwnW) || (agencyBonus.ErrCode != 0 && userBonus.ErrCode != 0))
            return "";

        sb.Append("<table>");
        sb.Append(" <tr>");
        sb.AppendFormat(" <td>{0}</td>", "&nbsp;");
        if (UserData.Bonus.BonusAgencySeeOwnW)
            sb.AppendFormat(" <td>{0} (<span class=\"link\" onclick=\"showBonusList('agency');\">{1}</span>)</td>", HttpContext.GetGlobalResourceObject("LibraryResource", "bonuslblAgency"),
                                                  HttpContext.GetGlobalResourceObject("Bonus", "BLBonusList"));
        sb.AppendFormat(" <td>{0} (<span class=\"link\" onclick=\"showBonusList('user');\">{1}</span>)</td>", HttpContext.GetGlobalResourceObject("LibraryResource", "bonuslblUser"),
                                              HttpContext.GetGlobalResourceObject("Bonus", "BLBonusList"));
        sb.Append(" </tr>");

        sb.Append(" <tr>");
        sb.AppendFormat(" <td class=\"header\">{0}</td>", HttpContext.GetGlobalResourceObject("LibraryResource", "bonuslblTotalbonus"));
        if (UserData.Bonus.BonusAgencySeeOwnW && UserData.Bonus.BonusUserSeeAgencyW)
            sb.AppendFormat(" <td class=\"agency\">{0}</td>", agencyBonus.TotBonus.HasValue ? agencyBonus.TotBonus.Value.ToString() : "&nbsp;");
        else
            sb.Append(" <td>&nbsp;</td>");
        if (UserData.Bonus.BonusUserSeeOwnW)
            sb.AppendFormat(" <td class=\"user\">{0}</td>", userBonus.TotBonus.HasValue ? userBonus.TotBonus.Value.ToString() : "&nbsp;");
        else
            sb.Append(" <td>&nbsp;</td>");
        sb.Append(" </tr>");
        sb.Append(" <tr>");
        sb.AppendFormat(" <td class=\"header\">{0}</td>", HttpContext.GetGlobalResourceObject("LibraryResource", "bonuslblTotalUsedBonus"));
        if (UserData.Bonus.BonusUserSeeOwnW && UserData.Bonus.BonusUserSeeAgencyW || string.IsNullOrEmpty(UserData.UserID))
            sb.AppendFormat(" <td class=\"agency\">{0}</td>", agencyBonus.UsedBonus.HasValue ? agencyBonus.UsedBonus.Value.ToString() : "&nbsp;");
        else
            sb.Append(" <td>&nbsp;</td>");
        if (UserData.Bonus.BonusUserSeeOwnW)
            sb.AppendFormat(" <td class=\"user\">{0}</td>", userBonus.UsedBonus.HasValue ? userBonus.UsedBonus.Value.ToString() : "&nbsp;");
        else
            sb.Append(" <td>&nbsp;</td>");
        sb.Append(" </tr>");
        sb.Append(" <tr>");
        sb.AppendFormat(" <td class=\"header\">{0}</td>", HttpContext.GetGlobalResourceObject("LibraryResource", "bonuslblTotalUseableBonus"));
        if (UserData.Bonus.BonusAgencySeeOwnW && UserData.Bonus.BonusUserSeeAgencyW)
            sb.AppendFormat(" <td class=\"agency\">{0}</td>", agencyBonus.UseableBonus.HasValue ? agencyBonus.UseableBonus.Value.ToString() : "&nbsp;");
        else
            sb.Append(" <td>&nbsp;</td>");
        if (UserData.Bonus.BonusUserSeeOwnW)
            sb.AppendFormat(" <td class=\"user\">{0}</td>", userBonus.UseableBonus.HasValue ? userBonus.UseableBonus.Value.ToString() : "&nbsp;");
        else
            sb.Append(" <td>&nbsp;</td>");
        sb.Append(" </tr>");
        sb.Append("</table>");
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getBonusList(string listType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        string _html = new Bonus().getBonusHtml(UserData, Equals(listType, "user") ? "U" : "A", Equals(listType, "user") ? UserData.PIN : UserData.AgencyID);
        return _html;
    }

    public static List<ResMainRecord> getReservationList(User UserData, List<resMonitorRecordV2> ResNoList, ref string errorMsg)
    {
        List<string> resNoList = ResNoList.Select(s => s.ResNo).ToList();
        string xmlResList = string.Empty;
        xmlResList += "<ResList>";
        foreach (var q in resNoList) {
            xmlResList += string.Format("<Reservation ResNo=\"{0}\" />", q);
        }
        xmlResList += "</ResList>";
        return null;
    }

    [WebMethod(EnableSession = true)]
    public static string exportExcel()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];

        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        resMonitorFilterRecordV2 filter = (resMonitorFilterRecordV2)HttpContext.Current.Session["ResMonFilterDef"];

        string AgencyUser = UserData.UserID;
        if (UserData.ShowAllRes)
            AgencyUser = filter.AgencyUser;
        else
            AgencyUser = UserData.UserID;

        Int16 OptionControl = 0;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) || Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            OptionControl = filter.ShowOptionRes.HasValue ? filter.ShowOptionRes.Value : Convert.ToInt16(0);
        else
            OptionControl = Convert.ToInt16(0);

        List<resMonitorExcelExportRecord> resList = new ReservationMonitor().getExcelExportData(UserData, AgencyUser, filter.AgencyOffice,
                        filter.ResNo1, filter.ResNo2, Conversion.getInt32OrNull(filter.DepCity), Conversion.getInt32OrNull(filter.ArrCity),
                        TvTools.Conversion.ConvertFromString(filter.BegDate1), TvTools.Conversion.ConvertFromString(filter.BegDate2),
                        TvTools.Conversion.ConvertFromString(filter.EndDate1), TvTools.Conversion.ConvertFromString(filter.EndDate2),
                        TvTools.Conversion.ConvertFromString(filter.ResDate1), TvTools.Conversion.ConvertFromString(filter.ResDate2),
                        filter.HolPack, filter.LeaderSurname, filter.LeaderName, filter.ResStatus, filter.ConfStatus, filter.PayStatus,
                        OptionControl, (filter.ShowDraft.HasValue && filter.ShowDraft.Value), filter.Currency, filter.ShowOnlyActiveUsers, ref errorMsg);

        //List<ResMainRecord> resList = getReservationList(UserData, resMonData, ref errorMsg);

        if (resList == null || resList.Count < 1) {
            return string.Empty;
        }
        //List<resMonitorRecord> dataResMain = (List<resMonitorRecord>)HttpContext.Current.Session["ResMonData"];
        StringBuilder sb = new StringBuilder();
        string strRow = string.Empty;
        strRow += "<meta http-equiv='Content-Type' content='charset=utf-8' />";
        strRow += "<font style='font-size:10.0pt; font-family:Calibri;'>";
        strRow += "<BR><BR><BR>";
        //strRow += "<table border='1' bgColor='#ffffff' borderColor='#000000' cellSpacing='0' cellPadding='0' style='font-size:10.0pt; font-family:Calibri; background:white;'>";
        strRow += "<TABLE border='1' bgColor='#ffffff' borderColor='#000000' cellSpacing='0' cellPadding='0'>";
        strRow += "<TR>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblReservationnumber") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblLeaderName") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblPackageName") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblHotel") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAdult") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblChild") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblBeginDate") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblNight") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblStatus") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblConfStatus") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblCurr") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblCatalogPrice") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblDiscount") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB") + " %" + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblPassangerEB") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "UsedAgencyBonus") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "UsedUserBonus") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "UsedPasBonus") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblSalePrice") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgentCommision") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgentCommision") + " %" + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyDiscountFromCommision") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyDiscountFromCommision") + " %" + "</B></TD>";

        if (UserData.MasterAgency) {
            strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "BrokerCom") + "</B></TD>";
            strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "BrokerCom") + " %" + "</B></TD>";
        }

        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentAgencyAmountToPay") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblActuallyPaid") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblBalance") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblPaymentStatus") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblRecordChangeDate") + "</B></TD>";
        if (UserData.MasterAgency) {
            strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResView", "lblAgency") + "</B></TD>";
        }
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyUser") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblDepartureCity") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblArrivalCity") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblReservationDate") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblReadByOperatorUser") + "</B></TD>";
        strRow += "<TD><B>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblReadByOperatorDate") + "</B></TD>";
        strRow += "</TR>";
        sb.Append(strRow);

        string decimalFormat = "";//"#.00"; //UserData.Ci.NumberFormat.CurrencyDecimalSeparator

        foreach (resMonitorExcelExportRecord row in resList) {
            strRow = string.Empty;
            strRow += "<TR>";
            strRow += "<TD>" + row.ResNo + "</TD>";

            strRow += "<TD>" + row.LeaderName + "</TD>";
            strRow += "<TD>" + row.HolPackName + "</TD>";
            if (!string.IsNullOrEmpty(row.HotelName))
                strRow += "<TD>" + row.HotelName + "</TD>";
            else
                strRow += "<TD>" + "</TD>";
            strRow += "<TD>" + row.Adult.ToString() + "</TD>";
            strRow += "<TD>" + row.Child.ToString() + "</TD>";
            strRow += "<TD>" + (row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "") + "</TD>";
            strRow += "<TD>" + row.Days.ToString() + "</TD>";
            strRow += "<TD>" + (row.ResStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + row.ResStat.ToString()).ToString() : "") + "</TD>";
            strRow += "<TD>" + (row.ConfStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + row.ConfStat.ToString()).ToString() : "") + "</TD>";
            strRow += "<TD>" + row.SaleCur + "</TD>";
            strRow += "<TD>" + (row.SalePrice.HasValue ? row.SalePrice.Value.ToString(decimalFormat).Replace(".", ",") : "") + "</TD>";
            strRow += "<TD>" + (row.Discount.HasValue ? row.Discount.Value.ToString(decimalFormat).Replace(".", ",") : "") + "</TD>";
            strRow += "<TD>" + (row.EBAgency.HasValue ? row.EBAgency.Value.ToString(decimalFormat).Replace(".", ",") : "") + "</TD>";
            strRow += "<TD>" + (row.EBAgencyPer.HasValue ? row.EBAgencyPer.Value.ToString(decimalFormat).Replace(".", ",") : "") + "</TD>";
            strRow += "<TD>" + (row.EBPas.HasValue ? row.EBPas.Value.ToString(decimalFormat).Replace(".", ",") : "") + "</TD>";
            strRow += "<TD>" + (row.AgencyBonusAmount.HasValue ? row.AgencyBonusAmount.Value.ToString(decimalFormat).Replace(".", ",") : "") + "</TD>";
            strRow += "<TD>" + (row.UserBonusAmount.HasValue ? row.UserBonusAmount.Value.ToString(decimalFormat).Replace(".", ",") : "") + "</TD>";
            strRow += "<TD>" + (row.PasBonusAmount.HasValue ? row.PasBonusAmount.Value.ToString(decimalFormat).Replace(".", ",") : "") + "</TD>";
            strRow += "<TD>" + (row.PasAmount.HasValue ? row.PasAmount.Value.ToString(decimalFormat).Replace(".", ",") : "") + "</TD>";
            decimal agencyCom = (row.AgencyCom.HasValue ? row.AgencyCom.Value : 0) + (row.AgencyComSup.HasValue ? row.AgencyComSup.Value : 0) - (row.AgencyDisPasVal.HasValue ? row.AgencyDisPasVal.Value : 0);
            decimal agencyComPer = (row.AgencyComPer.HasValue ? row.AgencyComPer.Value : 0) + (row.AgencyComSupPer.HasValue ? row.AgencyComSupPer.Value : 0) - (row.AgencyDisPasPer.HasValue ? row.AgencyDisPasPer.Value : 0);
            strRow += "<TD>" + (agencyCom.ToString(decimalFormat).Replace(".", ",")) + "</TD>";
            strRow += "<TD>" + (agencyComPer.ToString(decimalFormat).Replace(".", ",")) + "</TD>";
            strRow += "<TD>" + (row.AgencyDisPasVal.HasValue ? row.AgencyDisPasVal.Value.ToString(decimalFormat).Replace(".", ",") : "") + "</TD>";
            strRow += "<TD>" + (row.AgencyDisPasPer.HasValue ? row.AgencyDisPasPer.Value.ToString(decimalFormat).Replace(".", ",") : "") + "</TD>";

            if (UserData.MasterAgency) {
                strRow += "<TD>" + (row.BrokerCom.HasValue ? row.BrokerCom.Value.ToString(decimalFormat).Replace(".", ",") : "") + "</TD>";
                strRow += "<TD>" + (row.BrokerComPer.HasValue ? row.BrokerComPer.Value.ToString(decimalFormat).Replace(".", ",") : "") + "</TD>";
            }
            if (UserData.AgencyRec.AgencyType == 2)
                strRow += "<TD>" + (row.AgencyPayable.HasValue ? (row.AgencyPayable.Value + (row.BrokerCom.HasValue ? row.BrokerCom.Value : 0)).ToString(decimalFormat).Replace(".", ",") : "") + "</TD>";
            else
                strRow += "<TD>" + (row.AgencyPayable.HasValue ? row.AgencyPayable.Value.ToString(decimalFormat).Replace(".", ",") : "") + "</TD>";

            strRow += "<TD>" + (row.AgencyPayment.HasValue ? row.AgencyPayment.Value.ToString(decimalFormat).Replace(".", ",") : "") + "</TD>";

            if (UserData.AgencyRec.AgencyType == 2)
                strRow += "<TD>" + (row.Balance.HasValue ? (row.Balance.Value + (row.BrokerCom.HasValue ? row.BrokerCom.Value : 0)).ToString(decimalFormat) : "").Replace(".", ",") + "</TD>";
            else
                strRow += "<TD>" + (row.Balance.HasValue ? row.Balance.Value.ToString(decimalFormat) : "").Replace(".", ",") + "</TD>";

            strRow += "<TD>" + (!string.IsNullOrEmpty(row.PaymentStat) ? HttpContext.GetGlobalResourceObject("LibraryResource", "PayStatus" + row.PaymentStat).ToString() : "") + "</TD>";
            strRow += "<TD>" + (row.ChgDate.HasValue ? row.ChgDate.Value.ToShortDateString() : "") + "</TD>";
            if (UserData.MasterAgency) {
                strRow += "<TD>" + row.AgencyName + "</TD>";
            }
            strRow += "<TD>" + row.AgencyUser + "</TD>";
            strRow += "<TD>" + row.DepCityName + "</TD>";
            strRow += "<TD>" + row.ArrCityName + "</TD>";
            strRow += "<TD>" + (row.ResDate.HasValue ? row.ResDate.Value.ToShortDateString() : "") + "</TD>";
            strRow += "<TD>" + row.ReadByOprUser + "</TD>";
            strRow += "<TD>" + (row.ReadByOprDate.HasValue ? row.ReadByOprDate.Value.ToShortDateString() : "") + "</TD>";
            strRow += "</TR>";
            sb.Append(strRow);
        }
        sb.Append("</TABLE>");
        string filePath = AppDomain.CurrentDomain.BaseDirectory + "Cache\\" + UserData.AgencyID + "_" + UserData.UserID + ".xls";
        try {
            if (File.Exists(filePath))
                File.Delete(filePath);
            using (StreamWriter outfile = new StreamWriter(filePath, true, Encoding.Unicode)) {
                outfile.Write(TvTools.GZipCompres.Compress(sb.ToString()));
            }
            return WebRoot.BasePageRoot + "ViewXLS.aspx?FileName=" + UserData.AgencyID + "_" + UserData.UserID + ".xls";
        }
        catch (Exception Ex) {
            errorMsg = Ex.Message;
            return "";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string exportExcelCsv()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        List<resMonitorRecordV2> resMonData = new List<resMonitorRecordV2>();
        resMonData = ReadFile();

        if (resMonData == null || resMonData.Count < 1) {
            return string.Empty;
        }
        //List<resMonitorRecord> dataResMain = (List<resMonitorRecord>)HttpContext.Current.Session["ResMonData"];
        StringBuilder sb = new StringBuilder();
        string strRow = string.Empty;
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblReservationnumber") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblLeaderName") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblPackageName") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblHotel") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblAdult") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblChild") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblBeginDate") + ";";
        //strRow += (row.EndDate.HasValue ? row.EndDate.Value.ToShortDateString() : "") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblNight") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblStatus") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblConfStatus") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblCurr") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblCatalogPrice") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblDiscount") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB") + " %" + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblPassangerEB") + ";";
        strRow += HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "UsedAgencyBonus") + ";";
        strRow += HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "UsedUserBonus") + ";";
        strRow += HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "UsedPasBonus") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblSalePrice") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgentCommision") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgentCommision") + " %" + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyDiscountFromCommision") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyDiscountFromCommision") + " %" + ";";
        //strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblDueToPay") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblActuallyPaid") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblBalance") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblPaymentStatus") + ";";
        //strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblInvoiceToAgency") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblRecordChangeDate") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyUser") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblDepartureCity") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblArrivalCity") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblReservationDate") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblReadByOperatorUser") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblReadByOperatorDate");
        sb.Append(strRow + "\n");
        foreach (resMonitorRecordV2 rowS in resMonData) {
            ResMainRecord row = new ResTables().getResMain(UserData, rowS.ResNo, null, ref errorMsg);
            List<ResServiceRecord> resSerList = new ResTables().getResServiceList(UserData, rowS.ResNo, null, ref errorMsg);
            strRow = string.Empty;
            strRow += rowS.ResNo + ";";
            //strRow += row.ResNote.Replace(";", " ") + ";";
            strRow += rowS.LeaderName.Replace(";", " ") + ";";
            strRow += row.HolPackName.Replace(";", " ") + ";";
            if (resSerList.Where(w => string.Equals(w.ServiceType, "HOTEL") && w.StatConf <= 1 && w.StatSer <= 1).Count() > 0)
                strRow += resSerList.Where(w => string.Equals(w.ServiceType, "HOTEL") && w.StatConf <= 1 && w.StatSer <= 1).FirstOrDefault().ServiceName.Replace(";", " ") + ";";
            else
                strRow += ";";
            strRow += row.Adult.ToString() + ";";
            strRow += row.Child.ToString() + ";";
            strRow += (row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "") + ";";
            strRow += row.Days.ToString() + ";";
            strRow += (row.ResStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + row.ResStat.ToString()).ToString() : "") + ";";
            strRow += (row.ConfStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + row.ConfStat.ToString()).ToString() : "") + ";";
            strRow += row.SaleCur + ";";
            strRow += (row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.Discount.HasValue ? row.Discount.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.EBAgency.HasValue ? row.EBAgency.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.EBAgencyPer.HasValue ? row.EBAgencyPer.Value.ToString("###.00") : "") + ";";
            strRow += (row.EBPas.HasValue ? row.EBPas.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.AgencyBonusAmount.HasValue ? row.AgencyBonusAmount.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.UserBonusAmount.HasValue ? row.UserBonusAmount.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.PasBonusAmount.HasValue ? row.PasBonusAmount.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.PasAmount.HasValue ? row.PasAmount.Value.ToString("#,###.00") : "") + ";";
            decimal agencyCom = (row.AgencyCom.HasValue ? row.AgencyCom.Value : 0) + (row.AgencyComSup.HasValue ? row.AgencyComSup.Value : 0) - (row.AgencyDisPasVal.HasValue ? row.AgencyDisPasVal.Value : 0);
            decimal agencyComPer = (row.AgencyComPer.HasValue ? row.AgencyComPer.Value : 0) + (row.AgencyComSupPer.HasValue ? row.AgencyComSupPer.Value : 0) - (row.AgencyDisPasPer.HasValue ? row.AgencyDisPasPer.Value : 0);
            strRow += agencyCom.ToString("#,###.00") + ";";
            strRow += agencyComPer.ToString("#,###.00") + ";";
            strRow += (row.AgencyDisPasVal.HasValue ? row.AgencyDisPasVal.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.AgencyDisPasPer.HasValue ? row.AgencyDisPasPer.Value.ToString("#,###.00") : "") + ";";
            //strRow += (row.HasValue ? row.DueAgentPayment.Value.ToString("#,###.00") : "") + ";";

            if (UserData.AgencyRec.AgencyType == 2)
                strRow += (row.AgencyPayment.HasValue ? (row.AgencyPayment.Value + (row.BrokerCom.HasValue ? row.BrokerCom.Value : 0)).ToString("#,###.00") : "") + ";";
            else
                strRow += (row.AgencyPayment.HasValue ? row.AgencyPayment.Value.ToString("#,###.00") : "") + ";";

            if (UserData.AgencyRec.AgencyType == 2)
                strRow += (row.Balance.HasValue ? (row.Balance.Value + (row.BrokerCom.HasValue ? row.BrokerCom.Value : 0)).ToString("#,###.00") : "") + ";";
            else
                strRow += (row.Balance.HasValue ? row.Balance.Value.ToString("#,###.00") : "") + ";";

            strRow += (!string.IsNullOrEmpty(row.PaymentStat) ? HttpContext.GetGlobalResourceObject("LibraryResource", "PayStatus" + row.PaymentStat).ToString() : "") + ";";
            //strRow += row.InvoiceNo + ";";
            strRow += (row.ChgDate.HasValue ? row.ChgDate.Value.ToShortDateString() : "") + ";";
            strRow += row.AgencyUser + ";";
            strRow += row.DepCityName + ";";
            strRow += row.ArrCityName + ";";
            strRow += (row.ResDate.HasValue ? row.ResDate.Value.ToShortDateString() : "") + ";";
            strRow += row.ReadByOprUser + ";";
            strRow += row.ReadByOprDate.HasValue ? row.ReadByOprDate.Value.ToShortDateString() : "";
            sb.Append(strRow + "\n");
        }
        string filePath = AppDomain.CurrentDomain.BaseDirectory + "Cache\\" + UserData.AgencyID + "_" + UserData.UserID + ".csv";
        try {
            if (File.Exists(filePath))
                File.Delete(filePath);
            using (StreamWriter outfile = new StreamWriter(filePath, true, Encoding.Unicode)) {
                outfile.Write(sb.ToString());
            }
            return WebRoot.BasePageRoot + "Cache/" + UserData.AgencyID + "_" + UserData.UserID + ".csv";
        }
        catch (Exception Ex) {
            errorMsg = Ex.Message;
            return "";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string exportHtml()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        resMonitorFilterRecordV2 filter = (resMonitorFilterRecordV2)HttpContext.Current.Session["ResMonFilterDef"];

        string AgencyUser = UserData.UserID;
        if (UserData.ShowAllRes)
            AgencyUser = filter.AgencyUser;
        else
            AgencyUser = UserData.UserID;

        Int16 OptionControl = 0;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) || Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            OptionControl = filter.ShowOptionRes.HasValue ? filter.ShowOptionRes.Value : Convert.ToInt16(0);
        else
            OptionControl = Convert.ToInt16(0);

        List<resMonitorExcelExportRecord> resList = new ReservationMonitor().getExcelExportData(UserData, AgencyUser, filter.AgencyOffice,
                        filter.ResNo1, filter.ResNo2, Conversion.getInt32OrNull(filter.DepCity), Conversion.getInt32OrNull(filter.ArrCity),
                        TvTools.Conversion.ConvertFromString(filter.BegDate1), TvTools.Conversion.ConvertFromString(filter.BegDate2),
                        TvTools.Conversion.ConvertFromString(filter.EndDate1), TvTools.Conversion.ConvertFromString(filter.EndDate2),
                        TvTools.Conversion.ConvertFromString(filter.ResDate1), TvTools.Conversion.ConvertFromString(filter.ResDate2),
                        filter.HolPack, filter.LeaderSurname, filter.LeaderName, filter.ResStatus, filter.ConfStatus, filter.PayStatus,
                        OptionControl, (filter.ShowDraft.HasValue && filter.ShowDraft.Value), filter.Currency, filter.ShowOnlyActiveUsers, ref errorMsg);

        //List<ResMainRecord> resList = getReservationList(UserData, resMonData, ref errorMsg);

        if (resList == null || resList.Count < 1) {
            return string.Empty;
        }

        //List<resMonitorRecord> dataResMain = (List<resMonitorRecord>)HttpContext.Current.Session["ResMonData"];
        StringBuilder sb = new StringBuilder();
        sb.Append("<table width=\"100%\" cellpadding=\"5\" cellspacing=\"5\">");
        sb.Append("<tr>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblReservationnumber") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblLeaderName") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblPackageName") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblHotel") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAdult") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblChild") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblBeginDate") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblNight") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblStatus") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblConfStatus") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblCurr") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblCatalogPrice") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblDiscount") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB") + " %" + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblPassangerEB") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "UsedAgencyBonus") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "UsedUserBonus") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "UsedPasBonus") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblSalePrice") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgentCommision") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgentCommision") + " %" + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyDiscountFromCommision") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyDiscountFromCommision") + " %" + "</th>");
        if (UserData.MasterAgency) {
            sb.Append("<th>" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "BrokerCom") + "</th>");
            sb.Append("<th>" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "BrokerCom") + " %" + "</th>");
        }
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentAgencyAmountToPay") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblActuallyPaid") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblBalance") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblPaymentStatus") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblRecordChangeDate") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyUser") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblDepartureCity") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblArrivalCity") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblReservationDate") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblReadByOperatorUser") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblReadByOperatorDate") + "</th>");
        sb.Append("</tr>");
        foreach (resMonitorExcelExportRecord row in resList) {
            //ResMainRecord row = new ResTables().getResMain(UserData, rowS.ResNo, null, ref errorMsg);
            //List<ResServiceRecord> resSerList = new ResTables().getResServiceList(UserData, rowS.ResNo, null, ref errorMsg);
            sb.Append("<tr>");
            sb.Append("<td>" + row.ResNo + "</td>");
            sb.Append("<td>" + row.LeaderName.Replace(";", " ") + "</td>");
            sb.Append("<td>" + row.HolPackName.Replace(";", " ") + "</td>");
            sb.Append("<td>" + row.HotelName.Replace(";", " ") + "</td>");
            sb.Append("<td class=\"alignCenter\">" + row.Adult.ToString() + "</td>");
            sb.Append("<td class=\"alignCenter\">" + row.Child.ToString() + "</td>");
            sb.Append("<td>" + (row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "") + "</td>");
            sb.Append("<td class=\"alignCenter\">" + row.Days.ToString() + "</td>");
            sb.Append("<td class=\"alignCenter\">" + (row.ResStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + row.ResStat.ToString()).ToString() : "") + "</td>");
            sb.Append("<td class=\"alignCenter\">" + (row.ConfStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + row.ConfStat.ToString()).ToString() : "") + "</td>");
            sb.Append("<td>" + row.SaleCur + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.SalePrice.HasValue && row.SalePrice.Value > 0 ? row.SalePrice.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.Discount.HasValue && row.Discount.Value != 0 ? row.Discount.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.EBAgency.HasValue && row.EBAgency.Value != 0 ? row.EBAgency.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.EBAgencyPer.HasValue && row.EBAgencyPer.Value != 0 ? row.EBAgencyPer.Value.ToString("###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.EBPas.HasValue && row.EBPas.Value != 0 ? row.EBPas.Value.ToString("#,###.00") : "") + "</td>");

            sb.Append("<td class=\"alignRight\">" + (row.AgencyBonusAmount.HasValue ? row.AgencyBonusAmount.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.UserBonusAmount.HasValue ? row.UserBonusAmount.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.PasBonusAmount.HasValue ? row.PasBonusAmount.Value.ToString("#,###.00") : "") + "</td>");

            sb.Append("<td class=\"alignRight\">" + (row.PasAmount.HasValue && row.PasAmount.Value != 0 ? row.PasAmount.Value.ToString("#,###.00") : "") + "</td>");

            decimal agencyCom = (row.AgencyCom.HasValue ? row.AgencyCom.Value : 0) + (row.AgencyComSup.HasValue ? row.AgencyComSup.Value : 0) - (row.AgencyDisPasVal.HasValue ? row.AgencyDisPasVal.Value : 0);
            decimal agencyComPer = (row.AgencyComPer.HasValue ? row.AgencyComPer.Value : 0) + (row.AgencyComSupPer.HasValue ? row.AgencyComSupPer.Value : 0) - (row.AgencyDisPasPer.HasValue ? row.AgencyDisPasPer.Value : 0);

            sb.Append("<td class=\"alignRight\">" + agencyCom.ToString("#,###.00") + "</td>");
            sb.Append("<td class=\"alignRight\">" + agencyComPer.ToString("#,###.00") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.AgencyDisPasVal.HasValue && row.AgencyDisPasVal.Value != 0 ? row.AgencyDisPasVal.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.AgencyDisPasPer.HasValue && row.AgencyDisPasPer.Value != 0 ? row.AgencyDisPasPer.Value.ToString("#,###.00") : "") + "</td>");
            if (UserData.MasterAgency) {
                sb.Append("<td class=\"alignRight\">" + (row.BrokerCom.HasValue && row.BrokerCom.Value != 0 ? row.BrokerCom.Value.ToString("#,###.00") : "") + "</td>");
                sb.Append("<td class=\"alignRight\">" + (row.BrokerComPer.HasValue && row.BrokerComPer.Value != 0 ? row.BrokerComPer.Value.ToString("#,###.00") : "") + "</td>");
            }

            if (UserData.AgencyRec.AgencyType == 2)
                sb.Append("<td class=\"alignRight\">" + (row.AgencyPayable.HasValue && row.AgencyPayable.Value != 0 ? (row.AgencyPayable.Value + (row.BrokerCom.HasValue ? row.BrokerCom.Value : 0)).ToString("#,###.00") : "") + "</td>");
            else
                sb.Append("<td class=\"alignRight\">" + (row.AgencyPayable.HasValue && row.AgencyPayable.Value != 0 ? (row.AgencyPayable.Value).ToString("#,###.00") : "") + "</td>");

            sb.Append("<td class=\"alignRight\">" + (row.AgencyPayment.HasValue && row.AgencyPayment.Value != 0 ? row.AgencyPayment.Value.ToString("#,###.00") : "") + "</td>");

            if (UserData.AgencyRec.AgencyType == 2)
                sb.Append("<td class=\"alignRight\">" + (row.Balance.HasValue && row.Balance.Value != 0 ? (row.Balance.Value + (row.BrokerCom.HasValue ? row.BrokerCom.Value : 0)).ToString("#,###.00") : "") + "</td>");
            else
                sb.Append("<td class=\"alignRight\">" + (row.Balance.HasValue && row.Balance.Value != 0 ? row.Balance.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignCenter\">" + (!string.IsNullOrEmpty(row.PaymentStat) ? HttpContext.GetGlobalResourceObject("LibraryResource", "PayStatus" + row.PaymentStat).ToString() : "") + "</td>");
            sb.Append("<td>" + (row.ChgDate.HasValue ? row.ChgDate.Value.ToShortDateString() : "") + "</td>");
            sb.Append("<td>" + row.AgencyUser + "</td>");
            sb.Append("<td>" + row.DepCityName + "</td>");
            sb.Append("<td>" + row.ArrCityName + "</td>");
            sb.Append("<td>" + (row.ResDate.HasValue ? row.ResDate.Value.ToShortDateString() : "") + "</td>");
            sb.Append("<td>" + row.ReadByOprUser + "</td>");
            sb.Append("<td>" + (row.ReadByOprDate.HasValue ? row.ReadByOprDate.Value.ToShortDateString() : "") + "</td>");
            sb.Append("</tr>");
        }
        sb.Append("</table>");
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string reservationIsBusy(string ResNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        bool? _resIsBusy = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "reservationIsBusyControl"));
        if (!_resIsBusy.HasValue || (_resIsBusy.HasValue && _resIsBusy.Value))
        {
            bool resIsBusy = new Reservation().reservationIsBusy(ResNo);
            if (resIsBusy)
                return HttpContext.GetGlobalResourceObject("LibraryResource", "msgReservationIsBusy").ToString();
            else return string.Empty;
        }
        else return string.Empty;
    }

    protected static List<resMonitorRecordV2> ReadFile()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\ResPaymentMonitor." + HttpContext.Current.Session.SessionID;
        List<resMonitorRecordV2> list = new List<resMonitorRecordV2>();
        if (System.IO.File.Exists(path)) {
            System.IO.StreamReader reader = new System.IO.StreamReader(path);

            try {
                string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
                list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<resMonitorRecordV2>>(uncompressed);
            }
            catch (Exception) {
                throw;
            }
            finally {
                reader.Close();
            }
        }
        return list;
    }
    internal static List<SearchResult> readBasketData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\PriceSearchBasketV2." + HttpContext.Current.Session.SessionID;
        List<SearchResult> list = new List<SearchResult>();
        if (System.IO.File.Exists(path))
        {
            System.IO.StreamReader reader = new System.IO.StreamReader(path);

            try
            {
                string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
                list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SearchResult>>(uncompressed);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                reader.Close();
            }
        }
        return list;
    }
    internal static List<SearchResult> readSearchData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\PriceSearchV2." + HttpContext.Current.Session.SessionID;
        List<SearchResult> list = new List<SearchResult>();
        if (System.IO.File.Exists(path))
        {
            System.IO.StreamReader reader = new System.IO.StreamReader(path);

            try
            {
                string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
                list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SearchResult>>(uncompressed);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                reader.Close();
            }
        }
        return list;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static PackageSearchMakeRes getBookReservation(int? refNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        object extAllotControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ExtAllotControl");
        bool ExtAllotCont = Conversion.getBoolOrNull(extAllotControl).HasValue ? Conversion.getBoolOrNull(extAllotControl).Value : false;
        object _checkAvailableFlightSeat = new TvBo.Common().getFormConfigValue("SearchPanel", "CheckAvailableFlightSeat");
        bool checkAvailableFlightSeat = Conversion.getBoolOrNull(_checkAvailableFlightSeat).HasValue ? Conversion.getBoolOrNull(_checkAvailableFlightSeat).Value : false;
        object _checkAvailableRoom = new TvBo.Common().getFormConfigValue("SearchPanel", "CheckAvailableRoom");
        bool checkAvailableRoom = Conversion.getBoolOrNull(_checkAvailableRoom).HasValue ? Conversion.getBoolOrNull(_checkAvailableRoom).Value : false;

        HttpContext.Current.Session["ResData"] = null;
        if (HttpContext.Current.Session["Criteria"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.SearchCriteria criteria = (TvBo.SearchCriteria)HttpContext.Current.Session["Criteria"];

        ResDataRecord ResData = new ResDataRecord();
        ResData.SelectBook = new List<SearchResult>();
        if (!refNo.HasValue)
        {
            List<SearchResult> bookSelected = readBasketData();
            if (bookSelected == null || bookSelected.Count < 1)
                return new PackageSearchMakeRes { resOK = false, errMsg = "No", version = string.Empty };

            foreach (SearchResult row in bookSelected)
            {
                SearchResult sr = new SearchResult();
                SearchCriteriaRooms sc = new SearchCriteriaRooms();
                sr = row;
                sc = criteria.RoomsInfo.FirstOrDefault();
                sr.Child1Age = sc.Chd1Age;
                sr.Child2Age = sc.Chd2Age;
                sr.Child3Age = sc.Chd3Age;
                sr.Child4Age = sc.Chd4Age;
                ResData.SelectBook.Add(sr);
            }
        }
        else
        {
            SearchResult sr = new SearchResult();
            SearchCriteriaRooms sc = new SearchCriteriaRooms();
            List<SearchResult> searchData = readSearchData();
            if (searchData == null) return new PackageSearchMakeRes { resOK = false, errMsg = "No", version = string.Empty };
            sr = searchData.Find(f => f.RefNo == refNo);
            sc = criteria.RoomsInfo.FirstOrDefault();
            sr.Child1Age = sc.Chd1Age;
            sr.Child2Age = sc.Chd2Age;
            sr.Child3Age = sc.Chd3Age;
            sr.Child4Age = sc.Chd4Age;
            ResData.SelectBook.Add(sr);
        }
        string errorMsg = string.Empty;

        Guid? logID = null;
        String useLog = System.Configuration.ConfigurationManager.AppSettings["useLog"];
        if (!string.IsNullOrEmpty(useLog) && string.Equals(useLog, "1"))
        {
            if (ResData.SelectBook.FirstOrDefault().LogID.HasValue)
                logID = new WEBLog().saveWEBBookLog(ResData.SelectBook.FirstOrDefault().LogID.Value, DateTime.Now, null, ref errorMsg);
        }

        int totalPax = ResData.SelectBook.Sum(s => (s.HAdult + s.HChdAgeG2 + s.HChdAgeG3 + s.HChdAgeG4 + (s.ChdG1Age2.HasValue && s.ChdG1Age2.Value > (decimal)(199 / 100) ? s.HChdAgeG1 : 0)));
        int maxTotalPax = UserData.AgencyRec.MaxPaxCnt.HasValue ? UserData.AgencyRec.MaxPaxCnt.Value : (UserData.TvParams.TvParamReser.MaxPaxCnt.HasValue ? UserData.TvParams.TvParamReser.MaxPaxCnt.Value : 10);
        if (maxTotalPax < totalPax)
        {
            string msg = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "MaxPaxReservation").ToString(), maxTotalPax.ToString());
            return new PackageSearchMakeRes { resOK = false, errMsg = msg, version = string.Empty };
        }
        if (!checkAvailableFlightSeat && !ExtAllotCont)
            if (!(new UIReservation().bookFlightAllotControl(UserData, ResData.SelectBook, ResData.SelectBook.FirstOrDefault(), false, ref errorMsg)))
                return new PackageSearchMakeRes { resOK = false, errMsg = errorMsg, version = string.Empty };

        if (!ExtAllotCont)
            if (!(new Hotels().AllotmentControlForMultipleRoom(UserData, ResData.SelectBook, "H", ref errorMsg)))
                return new PackageSearchMakeRes { resOK = false, errMsg = errorMsg, version = string.Empty };

        ResData = new Reservation().getResData(UserData, ResData, criteria, SearchType.PackageSearch, false, ref errorMsg);
        if (!string.IsNullOrEmpty(errorMsg))
            return new PackageSearchMakeRes { resOK = false, errMsg = errorMsg, version = string.Empty };

        ResData.FirstData = new ResTables().getFirstResData(ResData);
        ResData.FirstData.ReservationType = SearchType.PackageSearch;

        string version = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "Version"));
        if (string.Equals(version, "V2"))
        {
            ResData = new ReservationV2().getResDataExtras(UserData, ResData, ref errorMsg);
        }
        if (ResData.ExtrasData == null)
        {
            ResData.ExtrasData = ResData;
        }

        bool CreateChildAge = false;
        if (UserData.WebService)
            CreateChildAge = true;
        else
        {
            object _createChildAge = new TvBo.Common().getFormConfigValue("MakeReservation", "CreateChildAge");
            CreateChildAge = _createChildAge != null ? (bool)_createChildAge : false;
        }

        if (!CreateChildAge)
        {
            foreach (ResCustRecord row in ResData.ResCust)
            {
                row.Age = null;
                row.Birtday = null;
            }
            foreach (ResCustRecord row in ResData.ExtrasData.ResCust)
            {
                row.Age = null;
                row.Birtday = null;
            }
        }

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_SunFun) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka))
        {
            ResMainRecord resMain = ResData.ResMain;
            resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
        }

        ResData.LogID = logID;

        HttpContext.Current.Session["ResData"] = ResData;
        return new PackageSearchMakeRes { resOK = true, errMsg = string.Empty, version = version };
    }
}

[Serializable()]
public class jSonDataRecord
{
    public jSonDataRecord()
    {
    }

    public string Name { get; set; }
    public object Value { get; set; }
}
