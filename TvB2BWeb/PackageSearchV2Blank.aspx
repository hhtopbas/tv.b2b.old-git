﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PackageSearchV2Blank.aspx.cs"
    Inherits="PackageSearchV2Blank" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="cache-control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "Blank") %></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function logout() {
            $('<div>' + '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>' + '</div>').dialog({
                autoOpen: true,
                position: {
                    my: 'center',
                    at: 'center'
                },
                modal: true,
                resizable: true,
                autoResize: true,
                bigframe: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                        $(this).dialog("close");
                        $(this).dialog("destroy");
                        window.location = 'Default.aspx';
                    }
                }
            });
        }

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
        
    </script>

</head>
<body>
    <form id="form1" runat="server">   
    <div>
    </div>
    </form>
</body>
</html>
