﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Threading;
using System.Web.Script.Serialization;
using System.Collections;
using System.Text;
using TvTools;

public partial class OnlyTransferSearchData : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScriptSerializer ser = new JavaScriptSerializer();
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        int iDisplayLength = Convert.ToInt32(HttpContext.Current.Request["iDisplayLength"]);
        int iDisplayStart = Convert.ToInt32(HttpContext.Current.Request["iDisplayStart"]);
        int iEcho = Convert.ToInt32(HttpContext.Current.Request["sEcho"]);

        if (iEcho == 1) iDisplayStart = 0;

        List<OnlyTransfer_Transfers> allSearchRecord = ReadFile();
        IQueryable<OnlyTransfer_Transfers> allRecords = allSearchRecord.AsQueryable();

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        IQueryable<OnlyTransfer_Transfers> filteredRecords = allRecords;

        IQueryable<OnlyTransfer_Transfers> returnData = filteredRecords.Skip(iDisplayStart).Take(iDisplayLength);

        saveFile(allSearchRecord, returnData.AsQueryable());
        var retval = from q in returnData
                     select new
                     {
                         Book = string.Format("<strong onclick=\"makeRes({1})\" class=\"bookStyle\">{0}</strong><span style=\"font-size:18pt;\"><br /><span style=\"font-size:3pt;\">&nbsp;</span>",
                                              HttpContext.GetGlobalResourceObject("PackageSearchResult", "book"),
                                              q.RefNo),
                         Name = getTransferName(UserData, q, useLocalName),                         
                         CheckIn = getTransferDate(UserData, q, useLocalName),
                         Price = getPrice(UserData, q, useLocalName)
                     };
        lightTable returnTable = new lightTable();
        returnTable.sEcho = iEcho;
        returnTable.iTotalRecords = allRecords.Count();
        returnTable.iTotalDisplayRecords = filteredRecords.Count();
        returnTable.aaData = retval;

        Response.Clear();

        Response.ContentType = ("text/html");
        Response.BufferOutput = true;
        Response.Write(ser.Serialize(returnTable));
        Response.End();
    }

    internal string getTransferDate(User UserData, OnlyTransfer_Transfers row, bool useLocalName)
    {
        string date = string.Format("{0}<br /><span style=\"font-size: 7pt;\">{1}</span>", row.DepDate.HasValue ? row.DepDate.Value.ToShortDateString() : "", row.DepDate.HasValue ? row.DepDate.Value.ToString("dddd") : "");
        if (row.retTransferRecord != null && row.RetPriceCalculated)
        {
            date += "<br />";
            date += string.Format("{0}<br /><span style=\"font-size: 7pt;\">{1}</span>", row.RetDate.HasValue ? row.RetDate.Value.ToShortDateString() : "", row.RetDate.HasValue ? row.RetDate.Value.ToString("dddd") : "");
        }
        else
        {
            date += "<br />";
            date += string.Format("{0}<br /><span style=\"font-size: 7pt;\">{1}</span>", row.RetDate.HasValue ? row.RetDate.Value.ToShortDateString() : "", row.RetDate.HasValue ? row.RetDate.Value.ToString("dddd") : "");
        }
        return date;
    }

    internal string getPrice(User UserData, OnlyTransfer_Transfers row, bool useLocalName)
    {
        string price = string.Empty;
        price = row.TotalPrice.HasValue ? row.TotalPrice.Value.ToString("#,###.00") + " " + row.DepSaleCur : "";
        return price;
    }
   
    internal string getTransferName(User UserData, OnlyTransfer_Transfers row, bool useLocalName)
    {
        string name = string.Empty;
        if (useLocalName)
        {
            name += row.depTransferRecord.LocalName;
            name += "<br />";
            name += " (" + row.DepTrfFromNameL + " -> " + row.DepTrfToNameL + (string.Equals(row.DepDirection, "R") ? " -> " + row.DepTrfFromNameL : string.Empty) + ")";
            if (!string.IsNullOrEmpty(row.DepVehicleCatName))
                name += " (" + row.DepVehicleCatName + ")";
        }
        else
        {
            name += row.depTransferRecord.Name;
            name += "<br />";
            name += " (" + row.DepTrfFromName + " -> " + row.DepTrfToName + (string.Equals(row.DepDirection, "R") ? " -> " + row.DepTrfFromName : string.Empty) + ")";
            if (!string.IsNullOrEmpty(row.DepVehicleCatName))
                name += " (" + row.DepVehicleCatName + ")";
        }
        if (row.retTransferRecord != null && row.RetPriceCalculated)
        {
            name += "<br />";
            if (useLocalName)
            {
                name += row.retTransferRecord.LocalName;
                name += "<br />";
                name += " (" + row.RetTrfFromNameL + " -> " + row.RetTrfToNameL + ")";
                if (!string.IsNullOrEmpty(row.RetVehicleCatName))
                    name += " (" + row.RetVehicleCatName + ")";
            }
            else
            {
                name += row.retTransferRecord.Name;
                name += "<br />";
                name += " (" + row.RetTrfFromName + " -> " + row.RetTrfToName + ")";
                if (!string.IsNullOrEmpty(row.RetVehicleCatName))
                    name += " (" + row.RetVehicleCatName + ")";
            }
        }
        return name;
    }

    protected List<OnlyTransfer_Transfers> ReadFile()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\OnlyTransferSearch." + HttpContext.Current.Session.SessionID;
        List<OnlyTransfer_Transfers> list = new List<OnlyTransfer_Transfers>();
        if (System.IO.File.Exists(path))
        {
            System.IO.StreamReader reader = new System.IO.StreamReader(path);

            try
            {
                string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
                list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OnlyTransfer_Transfers>>(uncompressed);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                reader.Close();
            }
        }
        return list;
    }

    internal void saveFile(List<OnlyTransfer_Transfers> result, IQueryable<OnlyTransfer_Transfers> calcResult)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        foreach (OnlyTransfer_Transfers row in calcResult)
        {
            OnlyTransfer_Transfers current = result.Find(f => f.RefNo == row.RefNo);
            if (current != null)
                current = row;
        }

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\OnlyTransferSearch." + HttpContext.Current.Session.SessionID;
        if (System.IO.File.Exists(path))
            System.IO.File.Delete(path);
        System.IO.FileStream file = new System.IO.FileStream(path, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
        try
        {
            System.IO.StreamWriter writer = new System.IO.StreamWriter(file);
            string compress = TvTools.GZipCompres.Compress(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            writer.Write(compress);
            writer.Close();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            file.Close();
        }
    }
}
