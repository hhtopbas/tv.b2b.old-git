﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvTools;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using TvBo;

public partial class _3thParty_Paximum_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["UserData"] == null) { return; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        PaximumRecord paximumSetting = new TvSystem().getPaximumConfig(UserData, ref errorMsg);
        if (paximumSetting.Pxm_Use.HasValue && paximumSetting.Pxm_Use.Value && !string.IsNullOrEmpty(paximumSetting.Pxm_AutMainCode))
        {
            string paximumApiUrl = paximumSetting.Pxm_APIAddr + (paximumSetting.Pxm_APIAddr.Substring(paximumSetting.Pxm_APIAddr.Length - 1, 1) == "/" ? "" : "/");
            string paximumB2BUrl = paximumSetting.Pxm_RedictionUrl + (paximumSetting.Pxm_RedictionUrl.Substring(paximumSetting.Pxm_RedictionUrl.Length - 1, 1) == "/" ? "" : "/");
            string paximumAccess_Token = paximumSetting.Pxm_AutMainCode;
            string baseUrl = string.Format("{0}TourVisio/GenerateTokenForTourvisio?access_token={4}&TvCustomerNo={1}&UserId={2}&AccountId={3}",
                paximumApiUrl,
                UserData.CustomRegID,
                UserData.UserID,
                UserData.AgencyID,
                paximumAccess_Token);
            try
            {
                WebClient client = new WebClient();
                client.BaseAddress = baseUrl;
                client.Headers.Add("content-type", "application/json");
                var response = client.DownloadString(baseUrl);
                
                apiResponse b2bToken = JsonConvert.DeserializeObject<apiResponse>(response);
                if (b2bToken.Error == null)
                    Response.Redirect(paximumB2BUrl + b2bToken.token);
                else
                {
                    errMsg.Visible = true;
                    errMsg.Text = "<h1>" + b2bToken.Error.Message + "</h1>";
                }
            }
            catch (Exception ex)
            {
                errMsg.Visible = true;
                errMsg.Text = "<h1>" + ex.Message + "</h1><br/>Request Url For Authorization";

            }

        }
        else
        {
            errMsg.Visible = true;
            errMsg.Text = "<h1>You do not have autorisation For Paximum</h1>";
        }
    }
}

public class apiResponse
{
    public string token { get; set; }
    public paxError Error { get; set; }

}
public class paxError
{
    public string Message { get; set; }
}