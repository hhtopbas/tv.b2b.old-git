﻿using System;
using System.Configuration;
using System.Threading;
using System.Web;
using TvTools;

public partial class _3thParty_SummaryPackPrice_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["UserData"] == null) { return; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        string qString = string.Empty;                
        qString += "?ac=" + SANCyrpto.Encyrpt(UserData.CustomRegID, UserData.AgencyID, true);

        string url = Conversion.getStrOrNull(ConfigurationManager.AppSettings["ExternalPage"]);

        Response.Redirect(url + qString);
    }
}