﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_3thParty_SummaryPackPrice_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>

    <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.url.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            //getParameters();
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Summary Pack Price List
        </div>
        <asp:Literal ID="errMsg" runat="server" Visible="false"></asp:Literal>
    </form>
</body>
</html>
