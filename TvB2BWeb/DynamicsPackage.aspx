﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DynamicsPackage.aspx.cs"
    Inherits="TvSearch.DynamicsPackage" EnableEventValidation="false" %>

<%@ Register Src="Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle","OnlyHotelSearch") %></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

    <script src="Scripts/jquery.dialogextend.js" type="text/javascript"></script>

    <script src="Scripts/jquery.printPage.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        if (typeof window.event != 'undefined')
            document.onkeydown = function() {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function(e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function logout() {
            $('<div>' + '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>' + '</div>').dialog({
                autoOpen: true,
                position: {
                    my: 'center',
                    at: 'center'
                },
                modal: true,
                resizable: true,
                autoResize: true,
                bigframe: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                        $(this).dialog("close");
                        $(this).dialog("destroy");
                        window.location = 'Default.aspx';
                    }
                }
            });
            window.setTimeout(function () { window.location = "<%=VirtualPathUtility.ToAbsolute("~/Default.aspx")%>"; }, 10000);
        }

        var myWidth = 0, myHeight = 0;

        function showDialog(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    autoOpen: true,
                    position: {
                        my: 'center',
                        at: 'center'
                    },
                    modal: true,
                    resizable: true,
                    autoResize: false,
                    bigframe: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        var NS = document.all;

        function maximize() {
            var myWidth = screen.availWidth;
            var myHeight = screen.availHeight;
            if (location.href.indexOf('pic') == -1) {
                if (window.opera) { } else {
                    top.window.moveTo(0, 0);
                    if (document.all) { top.window.resizeTo(myWidth, myHeight); }
                    else
                        if (document.layers || document.getElementById) {
                        if ((top.window.outerHeight < myHeight) || (top.window.outerWidth < myWidth)) {
                            top.window.outerHeight = myWidth + 'px';
                            top.window.outerWidth = myHeight + 'px';
                        }
                    }
                }
            }
        }

        function bookReservation() {
            obj = new Object();
            obj.Adult = parseInt($("#fltAdult").val());
            obj.Inf = parseInt($("#fltInf").val());
            obj.Child = parseInt($("#fltChild").val());
            obj.ArrCity = parseInt($("#fltArrival").val());
            obj.Curr = $("#cbCurrency").val();
            $.ajax({
                type: "POST",
                data: $.json.encode(obj),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: "DynamicsPackage.aspx/getBookReservation",
                success: function(msg) {
                    if (msg.d == "") {
                        window.scrollTo(0, 0);
                        $('html').css('overflow', 'hidden');
                        $("#MakeReservation").attr("src", 'MakeReservation.aspx');
                        $("#dialog").dialog("destroy");
                        $("#dialog-MakeReservation").dialog(
                            {
                                autoOpen: true,
                                modal: true,
                                minWidth: 990,
                                minHeight: 700,
                                resizable: true,
                                autoResize: true,
                                close: function(event, ui) { $('html').css('overflow', 'auto'); }
                            })
                            .dialogExtend({
                                "maximize": true,
                                "icons": {
                                    "maximize": "ui-icon-circle-plus",
                                    "restore": "ui-icon-pause"
                                }
                            });
                        $("#dialog-MakeReservation").dialogExtend("maximize");
                        return true;
                    }
                    else {
                        showDialog(msg.d);
                        return false;
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                    return false;
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });

        }

        function cancelMakeRes() {
            $("#MakeReservation").attr("src", "");
            $("#dialog-MakeReservation").dialog("close");
        }
        function gotoResViewPage(ResNo) {
            $("#MakeReservation").attr("src", "");
            $("#dialog-MakeReservation").dialog("close");
            $('html').css('overflow', 'auto');
            window.location = 'ResView.aspx?ResNo=' + ResNo;
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "DynamicsPackage.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        if (data == null || data == undefined) {
                            self.location = self.location;
                            return false;
                        }

                        $("#fltArrival").html('');
                        //$("#fltArrival").append("<option value='' >" + '< %=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                        $.each(data.Arrival, function(i) {
                            $("#fltArrival").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
                        });
                        var currOpt = data.Currency;
                        $("#cbCurrency").html('');
                        if (currOpt.curOption == 2) {
                            $("#divCurrency").show();
                            $.each(currOpt.currencyList, function(i) {
                                if (this.toString() == currOpt.currency)
                                    $("#cbCurrency").append("<option value='" + this.toString() + "' selected='selected'>" + this.toString() + "</option>");
                                else $("#cbCurrency").append("<option value='" + this.toString() + "'>" + this.toString() + "</option>");
                            });
                            $("#cbCurrency").removeAttr('disabled');
                        }
                        else if (currOpt.curOption == 1) {
                            $("#cbCurrency").append("<option value=''></option>");
                            $("#divCurrency").hide();
                        }
                        else {
                            $("#cbCurrency").append("<option value='" + currOpt.currency + "' selected='selected'>" + currOpt.currency + "</option>");
                            $("#divCurrency").hide();
                        }
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(function() {
            maximize();
            getFormData();
        });
    </script>

</head>
<body>
    <form id="PackageSearchForm" runat="server">
    <div class="Page">
        <tv1:Header ID="tvHeader" runat="server" />
        <tv1:MainMenu ID="tvMenu" runat="server" />
        <br />
        <div <%--class="Content"--%>>
            <div class="ui-helper-clearfix" style="margin: 4px 4px 4px 4px; width: 100%; height: 100%;
                text-align: center;">
                <div style="border: solid 1px #AAA;">
                    <div id="divRoutes" style="width: 600px; height: 45px;">
                        <div id="divDeparture" style="float: left; width: 195px; display: none;">
                            <strong>
                                <%= GetGlobalResourceObject("PackageSearchFilter","lblDepCity") %></strong>
                            <br />
                            <asp:DropDownList ID="fltDeparture" runat="server" AutoPostBack="True" Width="155px" />
                        </div>
                        <div id="divArrival" style="float: left; width: 195px; text-align: left;">
                            <strong>
                                <%= GetGlobalResourceObject("Controls", "lblRouteTo")%></strong>
                            <br />
                            <select id="fltArrival" style="width: 155px;">
                            </select>
                        </div>
                    </div>
                    <div id="divAdultChild" style="width: 600px;">
                        <div style="float: left; width: 198px; text-align: left;">
                            <strong>
                                <%= GetGlobalResourceObject("LibraryResource","lblAdult") %></strong>
                            <br />
                            <select id="fltAdult" style="width: 50px;">
                                <option value="1">1</option>
                                <option value="2" selected="selected">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                        <div style="float: left; width: 198px; text-align: left;">
                            <strong>
                                <%= GetGlobalResourceObject("LibraryResource","lblChild") %>
                                (0.00-1.99)</strong>
                            <br />
                            <select id="fltInf" style="width: 50px;">
                                <option value="0" selected="selected">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                            </select>
                        </div>
                        <div style="float: left; width: 198px; text-align: left;">
                            <strong>
                                <%= GetGlobalResourceObject("LibraryResource","lblChild") %>
                                (2.00-15.99)</strong>
                            <br />
                            <select id="fltChild" style="width: 50px;">
                                <option value="0" selected="selected">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                            </select>
                        </div>
                    </div>
                    <div id="divCurrency" style="text-align: left; width: 600px;">
                        <div style="float: left; width: 198px;">
                            <strong>
                                <%= GetGlobalResourceObject("ResView", "lblSaleCur")%></strong>
                            <br />
                            <select id="cbCurrency" style="width: 50px;">
                            </select>
                            <input id="CurrOption" type="hidden" value="0" />
                        </div>
                    </div>
                    <div id="divNext" style="clear: both; width: 400; text-align: center;">
                        <br />
                        <input id="nextStep" type="button" value='<%= GetGlobalResourceObject("LibraryResource","btnDynamicNext") %>'
                            class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
                            style="width: 140px;" onclick="bookReservation();" />
                        <br />
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
        <div class="Footer">
            <tv1:Footer ID="tvfooter" runat="server" />
        </div>
    </div>
    <div id="dialog-MakeReservation" title='<%= GetGlobalResourceObject("MakeReservation", "lblMakeReservation") %>'
        style="display: none; text-align: center;">
        <iframe id="MakeReservation" runat="server" height="100%" width="960px" frameborder="0"
            style="clear: both; text-align: left;"></iframe>
    </div>
    <%--Message, Confirm Area--%>
    <div id="dialog-message" title="" style="display: none;">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;">
            </span><span id="messages">Message</span>
        </p>
    </div>
    <div id="waitMessage" style="display: none;">
        <img alt="" title="" src="Images/Wait.gif" />
    </div>
    <%--Message, Confirm Area--%>
    <div id="dialog-viewReport" title='' style="display: none; text-align: center;">
        <iframe id="viewReport" runat="server" height="100%" width="700px" frameborder="0"
            style="clear: both; text-align: left;"></iframe>
    </div>
    </form>
</body>
</html>
