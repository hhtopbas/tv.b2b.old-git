﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Threading;
using System.Globalization;

public partial class ViewXLS : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["UserData"] == null) { return ; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (!IsPostBack)
            if (!string.IsNullOrEmpty(Request.Params["FileName"]))
            {
                string fileName = (string)Request.Params["FileName"];
                string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\" + fileName;
                if (System.IO.File.Exists(path))
                {
                    string uncompressed = string.Empty;
                    System.IO.StreamReader reader = new System.IO.StreamReader(path);
                    try
                    {
                        uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());                                                
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        reader.Close();                        
                    }
                    ExporttoExcel(fileName, uncompressed, UserData.Ci);
                }
            }
    }

    private void ExporttoExcel(string reportName, string reportData, CultureInfo Ci)
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.ClearHeaders();
        HttpContext.Current.Response.Buffer = true;
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + reportName);

       // HttpContext.Current.Response.Charset = "utf-8";
       // HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding(Ci.TextInfo.OEMCodePage);
        //Write data "xls"
        HttpContext.Current.Response.Write(reportData);
        //Write data "xls"
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();
    }	

}
