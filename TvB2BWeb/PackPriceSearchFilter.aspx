﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PackPriceSearchFilter.aspx.cs" Inherits="PackPriceSearchFilter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <title>
        <%= GetGlobalResourceObject("PageTitle", "PackageSearchFilter")%></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.datepick/jquery-ui-i18n.js" type="text/javascript"></script>
    <script src="Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>
    <script src="Scripts/jquery.textTruncate.js" type="text/javascript"></script>
    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>
    <script src="Scripts/Tv.PackageSearchFilterV2.js?ver=201507010930" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/PriceSearchFilter.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .special_day a { background: #00CC00 url('Images/FlightDay.gif') no-repeat !important; color: #222222 !important; }
        /*for Detur*/
        /*.special_day a { background: #00CC00 no-repeat !important; color: #222222 !important; }*/
    </style>

    <script language="javascript" type="text/javascript">

        var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
        var ComboSelect = '<%= GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
        var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
        var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
        var btnClear = '<%= GetGlobalResourceObject("LibraryResource", "btnClear") %>';
        var addPleaseSelectDeparture = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectDeparture") %>';
        var lblDay = '<%= GetGlobalResourceObject("LibraryResource", "lblDay") %>';

        var myWidth = 0, myHeight = 0;

        var dayColor = [];

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '</h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        function reSizeFrame() {
            self.parent.reSizeFilterFrame($('body').height());
        }

        function showDialogYesNo(msg) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                        return true;
                    }
                }, {
                    text: btnCancel,
                    click: function () {
                        $(this).dialog('close');
                        return false;
                    }
                }]
            });
        }

        function showDialog(msg) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                    }
                }]
            });
        }

        function showMessage(msg, transfer, trfUrl) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                    }
                }, {
                    text: btnCancel,
                    click: function () {
                        $(this).dialog('close');
                    }
                }]
            });
        }

        function getInf(adult, whois) {
            var Adult = parseInt(adult);
            $("#fltInfant" + whois).html("");
            for (i = 0; i < Adult + 1; i++) {
                $("#fltInfant" + whois).append("<option value='" + i.toString() + "'>" + i.toString() + "</option>");
            }
        }

        function SetChild(childCount, whois) {
            if (childCount > 0) $("#divRoomInfoChd1" + whois).show(); else $("#divRoomInfoChd1" + whois).hide();
            if (childCount > 1) $("#divRoomInfoChd2" + whois).show(); else $("#divRoomInfoChd2" + whois).hide();
            if (childCount > 2) $("#divRoomInfoChd3" + whois).show(); else $("#divRoomInfoChd3" + whois).hide();
            if (childCount > 3) $("#divRoomInfoChd4" + whois).show(); else $("#divRoomInfoChd4" + whois).hide();
        }

        function getChild(child, whois) {
            if ($("#divRoomInfoChd" + whois).length > 0) {
                var _divChild = $("#divRoomInfoChd" + whois);
                if (parseInt(child) == 0) {
                    SetChild(parseInt(child), whois);
                    _divChild.hide();
                }
                else {
                    _divChild.show();
                    SetChild(parseInt(child), whois);
                }
            }
        }

        function onChildChange(whois) {
            getChild($("#fltChild" + whois).val(), whois);
            reSizeFrame();
        }


        function getRoomPaxs() {
            var roomCount = parseInt($("#fltRoomCount").val());
            var dataList = [];
            var result = '';
            for (i = 1; i < roomCount + 1; i++) {
                var obj = new Object();

                var adult = parseInt($("#fltAdult" + i.toString()).val());
                var child = parseInt($("#fltChild" + i.toString()).val());
                var chd1Age = parseInt($("#fltRoomInfoChd1" + i.toString()).val());
                var chd2Age = parseInt($("#fltRoomInfoChd2" + i.toString()).val());
                var chd3Age = parseInt($("#fltRoomInfoChd3" + i.toString()).val());
                var chd4Age = parseInt($("#fltRoomInfoChd4" + i.toString()).val());
                obj.Adult = adult;
                obj.Child = child;
                obj.Chd1Age = (child > 0 ? chd1Age : -1);
                obj.Chd2Age = (child > 1 ? chd2Age : -1);
                obj.Chd3Age = (child > 2 ? chd3Age : -1);
                obj.Chd4Age = (child > 3 ? chd4Age : -1);
                dataList.push(obj);

                if (i > 1)
                    result += ',(';
                else result += '(';
                result += '|Adult|:|' + adult.toString() + '|';
                result += ',|Child|:|' + child.toString() + '|';
                result += ',|Chd1Age|:|' + (child > 0 ? chd1Age : -1) + '|';
                result += ',|Chd2Age|:|' + (child > 1 ? chd2Age : -1) + '|';
                result += ',|Chd3Age|:|' + (child > 2 ? chd3Age : -1) + '|';
                result += ',|Chd4Age|:|' + (child > 3 ? chd4Age : -1) + '|';
                result += ')';
            }
            var hfRoomInfo = $("#hfRoomInfo");
            hfRoomInfo.val(result);
            reSizeFrame();
        }

        function getFirstFlight() {
            $.ajax({
                async: false,
                type: "POST",
                url: "PackPriceSearchFilter.aspx/getFirstFlight",
                data: '{"DepCity":"' + $("#fltDepCity").val() + '","ArrCity":"' + $("#fltArrCity").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var firstDay = $.json.decode(msg.d);
                        if (firstDay != null) {
                            $("#fltDepCity").val(firstDay.DepCity);
                            $("#hfDate").val('');
                            $("#hfDate").val(firstDay.CheckIn.toString());
                            var _date = new Date(Date(eval(firstDay.CheckIn)).toString());
                            $("#iCheckIn").datepicker('setDate', _date);
                            setFlightDays();
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getArrival(_first) {
            $.ajax({
                type: "POST",
                url: "PackPriceSearchFilter.aspx/getArrival",
                data: '{"DepCity":"' + $("#fltDepCity").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#fltArrCity").html("");
                    $("#fltArrCity").append("<option value='' >" + ComboSelect + "</option>");
                    var country = '';
                    var first = true;
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        $.each(msg.d, function (i) {
                            if (this.Country != country) {
                                if (!($("#CustomRegID").val() == "0855101" || $("#CustomRegID").val() == "0970301")) {
                                    if (!first) {
                                        $("#fltArrCity").append("</optgroup>");
                                    }
                                    $("#fltArrCity").append("<optgroup label='" + this.Country + "'>");
                                    first = false;
                                    country = this.Country;
                                }
                                else {
                                    $("#fltArrCity").append('<option value="' + this.CountryCode + '" style="background: #DBDBDB; color: #000; font-weight: bold;">' + this.Country + '</option>');
                                    country = this.Country;
                                }
                            }
                            $("#fltArrCity").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");                            
                        });
                        if (!($("#CustomRegID").val() == "0855101" || $("#CustomRegID").val() == "0970301")) {
                            if ($("#fltArrCity").html() != '') {
                                $("#fltArrCity").append("</optgroup>");
                            }
                        }
                        if (_first == true) {
                            getFirstFlight();
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        alert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getHolpack() {
            $.ajax({
                type: "POST",
                url: "PackPriceSearchFilter.aspx/getHolpack",
                data: '{"DepCity":"' + $("#fltDepCity").val() + '","ArrCity":"' + $("#fltArrCity").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#fltHolpack").html("");
                    $("#fltHolpack").append("<option value=';;' >" + ComboSelect + "</option>");
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        $.each($.json.decode(msg.d), function (i) {
                            $("#fltHolpack").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        alert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changeDepCity() {
            $("#txtResort").html(''); $("#hfResort").val('');
            $("#txtCategory").html(''); $("#hfCategory").val('');
            $("#txtHotel").html(''); $("#hfHotel").val('');
            $("#txtRoom").html(''); $("#hfRoom").val('');
            $("#txtBoard").html(''); $("#hfBoard").val('');

            $("#divListPopup").html('');
            getArrival(false);            
            setFlightDays();
            getFirstFlight();
        }

        function changeHolpack() {
            var holpackCode = $("#fltHolpack").val();
            $("#fltDepCity").val(holpackCode.split(';')[1]);
            $("#fltArrCity").val(holpackCode.split(';')[2]);

            $("#txtHotel").html(''); $("#hfHotel").val('');
            $("#txtRoom").html(''); $("#hfRoom").val('');
            $("#txtBoard").html(''); $("#hfBoard").val('');
            $("#hfHolPack").val(holpackCode.split(';')[0]);
            setFlightDays();
            getFirstFlight();
        }

        function setFlightDays() {
            dayColor = [];
            var data = new Object();
            data.DepCity = $("#fltDepCity").val();
            data.ArrCity = $("#fltArrCity").val();
            data.B2BMenuCat = $("#b2bMenuCat").val();

            $.ajax({
                async: false,
                type: "POST",
                url: "PackPriceSearchFilter.aspx/getFlightDays",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = msg.d;
                        dayColor = [];
                        dayColor = data;
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        alert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changeArrCity() {
            $("#txtResort").html(''); $("#hfResort").val('');
            $("#txtCategory").html(''); $("#hfCategory").val('');
            $("#txtHotel").html(''); $("#hfHotel").val('');
            $("#txtRoom").html(''); $("#hfRoom").val('');
            $("#txtBoard").html(''); $("#hfBoard").val('');

            $("#divListPopup").html('');
            setFlightDays();
            getFirstFlight();
            getHolpack();
        }

        function getDeparture() {
            $.ajax({
                async: false,
                type: "POST",
                url: "PackPriceSearchFilter.aspx/getDeparture",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#fltDepCity").html("");
                    $("#fltDepCity").append("<option value='' >" + ComboSelect + "</option>");
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        if (data != null) {
                            $.each(data, function (i) {
                                $("#fltDepCity").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
                            });
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        alert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        String.prototype.ltrim = function () {
            return this.replace(/^\s+/, "");
        }

        function autoCompletteChange(key) {
            var autoCList = $('div[name$="listPopupRow"]');
            $.each(autoCList, function (i) {
                if (key != '') {
                    if ($.browser.msie) {
                        if (this.innerText.ltrim().toLowerCase().indexOf(key.toLowerCase()) > -1) {//.substring(0, key.toString().length).toLowerCase() == key.toLowerCase())
                            $(this).show();
                        } else {
                            $(this).hide();
                        }
                    }
                    else {
                        if (this.textContent.ltrim().toLowerCase().indexOf(key.toLowerCase()) > -1) {//.substring(0, key.toString().length).toLowerCase() == key.toLowerCase())
                            $(this).show();
                        } else {
                            $(this).hide();
                        }
                    }
                } else {
                    $(this).show();
                }
            });
        }

        function listPopupFillData(sourceBtn, clear) {
            var depCity = $("#fltDepCity").val();
            var arrCity = $("#fltArrCity").val();
            var resort = $("#hfResort").val();
            var category = $("#hfCategory").val();
            var hotel = $("#hfHotel").val();
            var room = $("#hfRoom").val();
            var board = $("#hfBoard").val();
            var holpack = $("#fltHolpack").val().split(';')[0];
            var paraList = '';
            var serviceUrl = '';
            switch (sourceBtn) {
                case 'resort':
                    paraList = '{"ArrCity":"' + arrCity + '"}';
                    serviceUrl = 'PackPriceSearchFilter.aspx/getResortData';
                    $.ajax({
                        async: false,
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: serviceUrl,
                        success: function (msg) {
                            if (clear == true) $("#hfResort").val('');
                            $("#txtCategory").html(''); $("#hfCategory").val('');
                            $("#txtHotel").html(''); $("#hfHotel").val('');
                            $("#txtRoom").html(''); $("#hfRoom").val('');
                            $("#txtBoard").html(''); $("#hfBoard").val('');
                            if (msg.hasOwnProperty('d') && msg.d != '') {
                                var inputDataArray = msg.d;
                                var listChk = $("#hfResort");
                                var _input = '';
                                var mydata = $.json.decode(inputDataArray);
                                if (mydata != null)
                                    $.each(mydata, function (index, inputData) {
                                        var checked = '';
                                        if (listChk.val().length > 0) {
                                            var chkListStr = listChk.val();
                                            var chkList = chkListStr.split('|');
                                            var _chkList = $.json.encode(chkList);

                                            if (chkList.length > 0 && _chkList.indexOf(inputData.RecID.toString()) > 0)
                                                checked = 'checked=\'checked\'';
                                            else checked = '';
                                        } else checked = '';
                                        _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.RecID + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
                                    });
                                $("#divListPopup").html('');
                                $("#divListPopup").html(_input);
                            }
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                alert(xhr.responseText);
                        }

                    });
                    break;
                case 'category':
                    paraList = '{"ArrCity":"' + arrCity + '","Resort":"' + resort + '"}';
                    serviceUrl = 'PackPriceSearchFilter.aspx/getCategoryData';
                    $.ajax({
                        async: false,
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: serviceUrl,
                        success: function (msg) {
                            if (clear == true) $("#hfCategory").val('');
                            $("#txtHotel").html(''); $("#hfHotel").val('');
                            $("#txtRoom").html(''); $("#hfRoom").val('');
                            $("#txtBoard").html(''); $("#hfBoard").val('');
                            if (msg.hasOwnProperty('d') && msg.d != '') {
                                var inputDataArray = msg.d;
                                var listChk = $("#hfCategory");
                                var _input = '';
                                var mydata = $.json.decode(inputDataArray);
                                if (mydata != null)
                                    $.each(mydata, function (index, inputData) {
                                        var checked = '';
                                        if (listChk.val().length > 0) {
                                            var chkListStr = listChk.val();
                                            var chkList = chkListStr.split('|');
                                            if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                                                checked = 'checked=\'checked\'';
                                            else checked = '';
                                        } else checked = '';
                                        _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
                                    });
                                $("#divListPopup").html('');
                                $("#divListPopup").html(_input);
                            }
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                alert(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                    break;
                case 'hotel':
                    paraList = '{"ArrCity":"' + arrCity + '","Resort":"' + resort + '","Category":"' + category + '","Holpack":"' + holpack + '"}';
                    serviceUrl = 'PackPriceSearchFilter.aspx/getHotelData';
                    $.ajax({
                        async: false,
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: serviceUrl,
                        success: function (msg) {
                            if (clear == true) $("#hfHotel").val('');
                            $("#txtRoom").html(''); $("#hfRoom").val('');
                            $("#txtBoard").html(''); $("#hfBoard").val('');
                            if (msg.hasOwnProperty('d') && msg.d != '') {
                                var inputDataArray = msg.d;
                                var listChk = $("#hfHotel");
                                var _input = '';
                                var mydata = $.json.decode(inputDataArray);
                                if (mydata != null) {
                                    _input += '<input type="text" id="hotelAutoComplette"  onkeyup="autoCompletteChange(this.value);" >';
                                    $.each(mydata, function (index, inputData) {
                                        var checked = '';
                                        if (listChk.val().length > 0) {
                                            var chkListStr = listChk.val();
                                            var chkList = chkListStr.split('|');
                                            if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                                                checked = 'checked=\'checked\'';
                                            else checked = '';
                                        } else checked = '';

                                        if (index % 2)
                                            _input += '<div class="divListPopupRow" name="listPopupRow" id="divListPopupRow_' + index.toString() + '">';
                                        else _input += '<div class="divListPopupRowAlter" name="listPopupRow" id="divListPopupRow_' + index.toString() + '">';
                                        _input += '<div class="divListPopupleftDiv">';
                                        _input += " <input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " hotelName=\"" + inputData.Name + "(" + inputData.Category + ")" + "\" />";
                                        _input += '</div>';
                                        _input += '<div class="divListPopuprightDiv">';
                                        _input += '<div class=\"tranCss\">';
                                        _input += " <label for='" + sourceBtn + "_" + index.toString() + "'><b>" + inputData.Name + "(" + inputData.Category + ")" + "</b></label>";
                                        _input += "<br />" + inputData.LocationName;
                                        _input += '</div>';
                                        _input += '</div>';
                                        _input += '</div>';
                                    });
                                }
                                $("#divListPopup").html('');
                                $("#divListPopup").html(_input);
                                if (mydata != null) { $("#hotelAutoComplette").focus(); }
                            }
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                alert(xhr.responseText);
                        }
                    });
                    break;
                case 'room':
                    paraList = '{"DepCity":"' + depCity + '","ArrCity":"' + arrCity + '","Resort":"' + resort + '","Hotel":"' + hotel + '"}';
                    serviceUrl = 'PackPriceSearchFilter.aspx/getRoomData';
                    $.ajax({
                        async: false,
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: serviceUrl,
                        success: function (msg) {
                            if (clear == true) $("#hfRoom").val('');
                            $("#txtBoard").html(''); $("#hfBoard").val('');
                            if (msg.hasOwnProperty('d') && msg.d != '') {
                                var inputDataArray = msg.d;
                                var listChk = $("#hfRoom");
                                var _input = '';
                                var mydata = $.json.decode(inputDataArray);
                                if (mydata != null)
                                    $.each(mydata, function (index, inputData) {
                                        var checked = '';
                                        if (listChk.val().length > 0) {
                                            var chkListStr = listChk.val();
                                            var chkList = chkListStr.split('|');
                                            if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                                                checked = 'checked=\'checked\'';
                                            else checked = '';
                                        } else checked = '';

                                        _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
                                    });
                                $("#divListPopup").html('');
                                $("#divListPopup").html(_input);
                            }
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                alert(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                    break;
                case 'board':
                    paraList = '{"DepCity":"' + depCity + '","ArrCity":"' + arrCity + '","Resort":"' + resort + '","Hotel":"' + hotel + '"}';
                    serviceUrl = 'PackPriceSearchFilter.aspx/getBoardData';
                    $.ajax({
                        async: false,
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: serviceUrl,
                        success: function (msg) {
                            if (clear == true) $("#hfBoard").val('');
                            if (msg.hasOwnProperty('d') && msg.d != '') {
                                var inputDataArray = msg.d;
                                var listChk = $("#hfBoard");
                                var _input = '';
                                var mydata = $.json.decode(inputDataArray);
                                if (mydata != null)
                                    $.each(mydata, function (index, inputData) {
                                        var checked = '';
                                        if (listChk.val().length > 0) {
                                            var chkListStr = listChk.val();
                                            var chkList = chkListStr.split('|');
                                            if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                                                checked = 'checked=\'checked\'';
                                            else checked = '';
                                        } else checked = '';

                                        _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
                                    });
                                $("#divListPopup").html('');
                                $("#divListPopup").html(_input);
                            }
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                                alert(xhr.responseText);
                            }
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                    break;
                default: break;
            }
            reSizeFrame();
        }

        function listPopupGetSelected(sourceBtn) {
            switch (sourceBtn) {
                case 'resort':
                    var hfResort = $("#hfResort");
                    var iResort = $("#txtResort");
                    var listChk = '';
                    var listTxt = '';
                    iResort.text('');
                    $(":checked", "#divListPopup").each(function () {
                        if (listChk.length > 0) {
                            listChk += '|';
                            listTxt += '<br />';
                        }
                        listChk += $(this).val();
                        listTxt += $(this).next().text();
                    });
                    hfResort.val(listChk);
                    iResort.html(listTxt);
                    break;
                case 'category':
                    var hfCategory = $("#hfCategory");
                    var iCategory = $("#txtCategory");
                    var listChk = '';
                    var listTxt = '';
                    iCategory.text('');
                    $(":checked", "#divListPopup").each(function () {
                        if (listChk.length > 0) {
                            listChk += '|';
                            listTxt += '<br />';
                        }
                        listChk += $(this).val();
                        listTxt += $(this).next().text();
                    });

                    hfCategory.val(listChk);
                    iCategory.html(listTxt);
                    break;
                case 'hotel':
                    var hfHotel = $("#hfHotel");
                    var iHotel = $("#txtHotel");
                    var listChk = '';
                    var listTxt = '';
                    iHotel.text('');
                    $(":checked", "#divListPopup").each(function () {
                        if (listChk.length > 0) {
                            listChk += '|';
                            listTxt += '<br />';
                        }
                        listChk += $(this).val();
                        listTxt += $(this).attr("hotelName");
                    });

                    hfHotel.val(listChk);
                    iHotel.html(listTxt);
                    break;
                case 'room':
                    var hfRoom = $("#hfRoom");
                    var iRoom = $("#txtRoom");
                    var listChk = '';
                    var listTxt = '';
                    iRoom.text('');
                    $(":checked", "#divListPopup").each(function () {
                        if (listChk.length > 0) {
                            listChk += '|';
                            listTxt += '<br />';
                        }
                        listChk += $(this).val();
                        listTxt += $(this).next().text();
                    });

                    hfRoom.val(listChk);
                    iRoom.html(listTxt);
                    break;
                case 'board':
                    var hfBoard = $("#hfBoard");
                    var iBoard = $("#txtBoard");
                    var listChk = '';
                    var listTxt = '';
                    iBoard.text('');
                    $(":checked", "#divListPopup").each(function () {
                        if (listChk.length > 0) {
                            listChk += '|';
                            listTxt += '<br />';
                        }
                        listChk += $(this).val();
                        listTxt += $(this).next().text();
                    });
                    hfBoard.val(listChk);
                    iBoard.html(listTxt);
                    break;
                default: break;
            }
            reSizeFrame();
        }

        $.extend($.ui.dialog.prototype, {
            'addbutton': function (buttonName, func) {
                var buttons = this.element.dialog('option', 'buttons');
                buttons.push({ text: buttonName, click: func });
                //buttons[buttonName] = func; //2014-09-06 Nova bug
                this.element.dialog('option', 'buttons', buttons);
            }
        });

        function showListPopupAddButton(btnName, func) {
            //$.extend($.ui.dialog.prototype, {
            //    'addbutton': function (buttonName, func) {
            //        var buttons = this.element.dialog('option', 'buttons');
            //        buttons[buttonName] = func;
            //        this.element.dialog('option', 'buttons', buttons);
            //    }
            //});
            $("#dialog-listPopup").dialog('addbutton', btnName, func);
        }

        function showListPopup(_title, sourceBtn) {
            $("#divListPopup").html('');
            listPopupFillData(sourceBtn, false);

            $("#dialog").dialog("destroy");
            $("#dialog-listPopup").dialog({
                title: _title,
                modal: true,
                height: 400,
                width: 240,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        listPopupGetSelected(sourceBtn);
                        $(this).dialog('close');
                        return true;
                    }
                }, {
                    text: btnCancel,
                    click: function () {
                        $(this).dialog('close');
                        return false;
                    }
                }]
            }).css('font-size', '7px');

            if ($("#CustomRegID").val() != '0970301') {
                showListPopupAddButton(btnClear, function () {
                    $("#divListPopup").html('');
                    listPopupFillData(sourceBtn, true);
                    return false;
                });
            }

            reSizeFrame();
        }

        function getRoomInfo(setCrt) {
            var roomCount = $("#fltRoomCount").val();
            $.ajax({
                type: "POST",
                url: "PackPriceSearchFilter.aspx/getRoomInfo",
                data: '{"RoomCount":"' + roomCount + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#fltRoomInfo").html('');
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        $("#fltRoomInfo").html(msg.d);
                    }
                    reSizeFrame();
                    if (setCrt == true) {
                        setCriteria();
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        alert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function filterclear() {
            self.parent.pageReLoad();
        }

        function setCriteria() {
            getRoomPaxs();
            var data = new Object();
            data.CheckIn = $("#iCheckIn").datepicker('getDate').getTime();
            data.ExpandDate = $("#fltCheckInDay").val();
            data.NightFrom = $("#fltNight1").val();
            data.NightTo = $("#fltNight2").val();
            data.RoomCount = $("#fltRoomCount").val();
            data.roomsInfo = $("#hfRoomInfo").val();
            data.Board = $("#hfBoard").val();
            data.Room = $("#hfRoom").val();
            data.DepCity = $("#fltDepCity").val();
            data.ArrCity = $("#fltArrCity").val();
            data.Resort = $("#hfResort").val();
            data.Category = $("#hfCategory").val();
            data.Hotel = $("#hfHotel").val();
            data.CurControl = ($("#CurrencyConvert")[0].checked ? 'Y' : 'N');
            data.CurrentCur = $("#currentCur").val();
            data.ShowStopSaleHotels = ($("#showStopSale")[0].checked ? 'Y' : 'N');
            data.HolPack = $("#hfHolPack").val();
            //data.Facility = $("#hfFacility").val();
            data.B2BMenuCatStr = $("#b2bMenuCat").val();
            $.ajax({
                type: "POST",
                url: "PackPriceSearchFilter.aspx/SetCriterias",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function search() {
            if ($("#CustomRegID").val() == "0302501") {
                if ($("#fltHolpack").val() == "") {
                    alert('Please select package.');
                    return false;
                }
            }
            if ($("#hfDepCityCompulsory").val() == 'true') {
                if ($("#fltDepCity").val() == '') {
                    alert(addPleaseSelectDeparture);
                    return false;
                }
            }

            getRoomPaxs();
            var params = new Object();
            params.CheckIn = $("#iCheckIn").datepicker('getDate') != null ? $("#iCheckIn").datepicker('getDate').getTime() : null;
            params.ExpandDate = $("#fltCheckInDay").val();
            params.NightFrom = $("#fltNight1").val();
            params.NightTo = $("#fltNight2").val();
            params.RoomCount = $("#fltRoomCount").val();
            params.roomsInfo = $("#hfRoomInfo").val();
            params.Board = $("#hfBoard").val();
            params.Room = $("#hfRoom").val();
            params.DepCity = $("#fltDepCity").val();
            params.ArrCity = $("#fltArrCity").val();
            params.Resort = $("#hfResort").val();
            params.Category = $("#hfCategory").val();
            params.Hotel = $("#hfHotel").val();
            params.CurControl = ($("#CurrencyConvert")[0].checked ? 'Y' : 'N');
            params.CurrentCur = $("#currentCur").val();
            params.ShowStopSaleHotels = ($("#showStopSale")[0].checked ? 'Y' : 'N');
            params.HolPack = $("#hfHolPack").val();
            params.B2BMenuCatStr = $("#b2bMenuCat").val();

            $.ajax({
                type: "POST",
                url: "PackPriceSearchFilter.aspx/SetCriterias",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    self.parent.searchPrice();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
            reSizeFrame();
        }

        function getStopSaleHotels(showStopSale) {
            if (showStopSale == true)
                $("#showStopSale").attr("checked", "checked");
        }

        function getFormData() {
            var data = new Object();
            data.B2BMenuCatStr = $("#b2bMenuCat").val();
            $.ajax({
                async: false,
                type: "POST",
                url: "PackPriceSearchFilter.aspx/getFormData",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null && msg.d != '') {
                        var data = msg.d;//$.json.decode(msg.d);
                        if (data == null || data == undefined) {
                            self.location = self.location;
                            return false;
                        }
                        if ((data.CustomRegID == '1195901' || data.CustomRegID == '0970301') || data.ShowFilterPackage == false) {
                            $("#holpackDiv").hide();
                        }
                        else {
                            $("#holpackDiv").show();
                        }
                        $("#CustomRegID").val(data.CustomRegID);
                        $("#Market").val(data.Market);

                        if (data.CustomRegID == '0970301' && data.Market == 'SWEMAR') {
                            $("#divNight2").hide();
                        }

                        if (data.CustomRegID != '0970301') {
                            $("#hfDepCityCompulsory").val('true');
                        }
                        else {
                            $("#hfDepCityCompulsory").val('false');
                        }
                        $("#fltCheckInDay").html(packageSearchFilterV2_fltCheckInDay(data.fltCheckInDay, lblDay));

                        $("#fltNight1").html(packageSearchFilterV2_fltNight(data.fltNight));
                        $("#fltNight2").html(packageSearchFilterV2_fltNight(data.fltNight2));
                        $("#fltRoomCount").html(packageSearchFilterV2_fltRoomCount(data.fltRoomCount));
                        $("#divCurrency").html('');
                        $("#divCurrency").html(data.MarketCur);
                        $("#hfDate").val('');
                        $("#hfDate").val(data.CheckIn);

                        var _date1 = new Date(Date(eval(data.CheckIn)).toString());
                        $("#iCheckIn").datepicker('setDate', _date1);

                        if (data.ShowExpandDay == false) $("#divExpandDate").hide();
                        if (data.ShowSecondDay == false) $("#divNight2").hide();

                        if (data.ShowFirstDay != true)
                        {
                            $("#divDays").hide();
                        }
                        if (data.ShowDepCity != true) {
                            $("#divDepCity").hide();
                        }
                        if (data.ShowArrCity != true) {
                            $("#divArrCity").hide();
                        }
                        getDeparture();
                        getArrival(true);
                        getRoomInfo(true);
                        if (data.CustomRegID == '0970301' || data.CustomRegID == '1076601') {
                            $("#divStopSaleHotels").hide();
                        }
                        else {
                            getStopSaleHotels(data.ShowStopSaleHotels);
                        }
                        if (data.ShowOfferBtn) {
                            $("#divOffers").show();
                        }
                        if (data.DisableRoomFilter) {
                            $("#roomFilter").hide();
                        } else {
                            $("#roomFilter").show();
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changedNight1() {
            $("#fltNight2").val($("#fltNight1").val());
        }

        function changedNight2() {
            var night1 = parseInt($("#fltNight1").val());
            var night2 = parseInt($("#fltNight2").val());
            if (night1 > night2)
                $("#fltNight2").val($("#fltNight1").val());
        }

        function reSearch(date) {
            $.ajax({
                type: "POST",
                url: "PackPriceSearchFilter.aspx/reSearch",
                data: '{"date":' + date + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        $("#hfDate").val('');
                        $("#hfDate").val(msg.d.CheckIn.toString());                        

                        var _date1 = new Date(Date(eval(msg.d.CheckIn)).toString());
                        $("#iCheckIn").datepicker('setDate', _date1);

                        self.parent.searchPrice();
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
            reSizeFrame();
        }

        function createOffer() {
            self.parent.createOfferV2();
        }

        function flightDays(date) {
            var noWeekend = $.datepicker.noWeekends(date);
            if (dayColor != null && dayColor.length > 0) {
                for (i = 0; i < dayColor.length; i++) {
                    var year = date.getFullYear();
                    var month = date.getMonth();
                    var day = date.getDate();
                    if (month == (dayColor[i].Month - 1) && day == dayColor[i].Day && year == dayColor[i].Year) {
                        return [true, 'special_day', dayColor[i].Text];
                    }
                }
                return [true, ''];
            }
            return [true, ''];
        }

        $(document).ready(function () {

            $.datepicker.setDefaults($.datepicker.regional['<%= twoLetterISOLanguageName %>' != 'en' ? '<%= twoLetterISOLanguageName %>' : '']);

            $("#iCheckIn").datepicker({
                showOn: "button",
                buttonImage: "Images/Calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                onSelect: function (dateText, inst) {
                    if (dateText != '') {
                        var date2 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
                        var iDate = $("#hfDate");
                        if (date2) {
                            iDate.val(Date.parse(date2));
                        }
                        else {
                            iDate.val(Date.parse(new Date()));
                        }
                    }
                },
                minDate: new Date(),
                beforeShowDay: flightDays
            });
            $.query = $.query.load(location.href);
            var b2bMenuCat = '';
            b2bMenuCat = $.query.get('B2BMenuCat');
            if (b2bMenuCat == undefined && b2bMenuCat.length == 0) b2bMenuCat == '';
            $("#b2bMenuCat").val(b2bMenuCat);
            getFormData();
        });
    </script>

</head>
<body>
    <form id="SearchFilterForm" runat="server">
        <input id="CustomRegID" type="hidden" value="" />
        <input id="Market" type="hidden" value="" />
        <input id="b2bMenuCat" type="hidden" value="" />
        <div style="width: 250px;">
            <table cellpadding="2" cellspacing="0" style="text-align: left; width: 250px;">
                <tr id="divDepCity">
                    <td valign="top" style="width: 100%;">
                        <strong>
                            <%= GetGlobalResourceObject("PackageSearchFilter", "lblDepCity") %></strong>
                        <br />
                        <select id="fltDepCity" onchange="changeDepCity();">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr id="divArrCity">
                    <td>
                        <strong>
                            <%= GetGlobalResourceObject("PackageSearchFilter", "lblArrCity") %></strong>
                        <br />
                        <select id="fltArrCity" onchange="changeArrCity();">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="holpackDiv" style="display: none;">
                            <strong>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblHolpack")%></strong>
                            <br />
                            <select id="fltHolpack" onchange="changeHolpack();">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="divCheckIn" style="width: 148px; float: left;">
                            <strong>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblCheckIn") %></strong>
                            <br />
                            <input id="iCheckIn" type="text" style="width: 95px;" />
                            <%--<asp:TextBox ID="iCheckIn" runat="server" Width="100px" />
                                <rjs:PopCalendar ID="ppcCheckIn" runat="server" Control="iCheckIn" ClientScriptOnDateChanged="dateChange" />--%>
                        </div>
                        <div id="divExpandDate" style="width: 95px; float: left;">
                            <strong>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblExpandDate") %></strong>
                            <br />
                            <select id="fltCheckInDay">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr id="divDays">
                    <td>
                        <div id="divNight1" style="width: 122px; float: left;">
                            <strong>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblNight1") %></strong>
                            <br />
                            <select id="fltNight1" onchange="changedNight1();">
                            </select>
                        </div>
                        <div id="divNight2" style="width: 122px; float: left;">
                            <strong>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblNight2") %></strong>
                            <br />
                            <select id="fltNight2" onchange="changedNight2();">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <%= GetGlobalResourceObject("PackageSearchFilter", "lblRoomCount") %></strong>
                        <br />
                        <select id="fltRoomCount" onchange="getRoomInfo(false);">
                        </select>
                        <div id="divRoomInfo" style="width: 100%;">
                            <div id="fltRoomInfo">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="detailSearch" valign="middle">
                        <strong>
                            <%= GetGlobalResourceObject("PackageSearchFilter", "lblDetailedSearch") %></strong>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%; height: 22px; position: relative; vertical-align: bottom;">
                            <span style="position: absolute; bottom: 2px;"><b>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblResort") %></b></span>
                            <img alt="" src="Images/search.png" height="22px" style="position: absolute; right: 2px;"
                                onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleResortList") %>', 'resort');" />
                        </div>
                        <div id="txtResort" class="multiField">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%; height: 22px; position: relative; vertical-align: bottom;">
                            <span style="position: absolute; bottom: 2px;"><b>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblCategory") %></b></span>
                            <img alt="" src="Images/search.png" height="22px" style="position: absolute; right: 2px;"
                                onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleCategoryList") %>', 'category');" />
                        </div>
                        <div id="txtCategory" class="multiField">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%; height: 22px; position: relative; vertical-align: bottom;">
                            <span style="position: absolute; bottom: 2px;"><b>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblHotel") %></b></span>
                            <img alt="" src="Images/search.png" height="22px" style="position: absolute; right: 2px;"
                                onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleHotelList") %>', 'hotel');" />
                        </div>
                        <div id="txtHotel" class="multiField">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr id="roomFilter">
                    <td>
                        <div style="width: 100%; height: 22px; position: relative; vertical-align: bottom;">
                            <span style="position: absolute; bottom: 2px;"><b>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblRoom") %></b></span>
                            <img alt="" src="Images/search.png" height="22px" style="position: absolute; right: 2px;"
                                onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleRoomList") %>', 'room');" />
                        </div>
                        <div id="txtRoom" class="multiField">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%; height: 22px; position: relative; vertical-align: bottom;">
                            <span style="position: absolute; bottom: 2px;"><b>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblBoard") %></b></span>
                            <img alt="" src="Images/search.png" height="22px" style="position: absolute; right: 2px;"
                                onclick="showListPopup('<%= GetGlobalResourceObject("PackageSearchFilter", "titleBoardList") %>', 'board');" />
                        </div>
                        <div id="txtBoard" class="multiField">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="divCurrency" style="clear: both; width: 100%;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="divStopSaleHotels" style="clear: both; width: 100%;">
                            <br />
                            <input id="showStopSale" type="checkbox" /><label for="showStopSale"><%= GetGlobalResourceObject("PackageSearchFilter", "lblShowStopSaleHotels") %></label>
                            <br />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="z-index: 1; text-align: center;">
                        <input type="button" name="btnSearch" value='<%= GetGlobalResourceObject("LibraryResource", "btnSearch") %>'
                            style="width: 100px;" onclick="search()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />&nbsp;
                    <input type="button" name="btnClear" value='<%= GetGlobalResourceObject("LibraryResource", "btnClear") %>'
                        style="width: 100px;" onclick="filterclear()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td id="divOffers" style="z-index: 1; text-align: center; display: none;">
                        <input type="button" name="btnCreateOffer" value='<%= GetGlobalResourceObject("PackageSearchResultV2","btnCreateOffer") %>'
                            style="width: 80%;" onclick="createOffer()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />&nbsp;                    
                    </td>
                </tr>
            </table>
        </div>
        <div id="hiddenControls" style="display: none; visibility: hidden;">
            <input id="hfDate" type="hidden" />
            <input id="hfResort" type="hidden" />
            <input id="hfCategory" type="hidden" />
            <input id="hfHotel" type="hidden" />
            <input id="hfRoomInfo" type="hidden" />
            <input id="hfRoom" type="hidden" />
            <input id="hfBoard" type="hidden" />
            <input id="hfHolPack" type="hidden" />
            <input id="hfDepCityCompulsory" type="hidden" />
        </div>
        <div id="dialog-listPopup" style="display: none;">
            <div id="divListPopup">
            </div>
        </div>
        <div id="dialog-message" title="" style="display: none;">
            <span id="messages">Message</span>
        </div>
    </form>
</body>
</html>
