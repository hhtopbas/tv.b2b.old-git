﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Data.SqlTypes;
using System.Data;
using System.Web.Services;
using System.Text;
using TvTools;
using System.IO;
using Winnovative;


public partial class AgencyPaymentMonitor : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        IsPostBackBefore(UserData);
        if (!IsPostBack)
            IsPostBackInside(sender, e, UserData);
        IsPostBackAfter(UserData);

    }

    void IsPostBackBefore(User UserData)
    {
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        string datePatern = new TvBo.Common().getDateFormat(UserData.Ci);

        ppcBegDate1.Culture = ci.Name + " " + ci.EnglishName;
        ppcBegDate1.Format = datePatern.Replace('/', ' ');
        ppcBegDate2.Culture = ci.Name + " " + ci.EnglishName;
        ppcBegDate2.Format = datePatern.Replace('/', ' ');
        ppcDueDate.Culture = ci.Name + " " + ci.EnglishName;
        ppcDueDate.Format = datePatern.Replace('/', ' ');
        ppcSaleDate1.Culture = ci.Name + " " + ci.EnglishName;
        ppcSaleDate2.Format = datePatern.Replace('/', ' ');
        ppcDueDate.DateValue = DateTime.Today;
    }

    void IsPostBackInside(object sender, EventArgs e, User UserData)
    {
        string errorMsg = string.Empty;
        Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url);//"AgencyPaymentMonitor.aspx";
    }

    void IsPostBackAfter(User UserData)
    {

    }

    public static paymentMonitorFilterRecord createFilter()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;

        paymentMonitorFilterRecord filterDef = new paymentMonitorFilterRecord();
        if (HttpContext.Current.Session["FilterDefPayment"] != null)
            filterDef = (paymentMonitorFilterRecord)HttpContext.Current.Session["FilterDefPayment"];
        string datePatern = new TvBo.Common().getDateFormat(UserData.Ci);
        datePatern = datePatern.Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], '-');
        filterDef.ResStatus = "0;1;3";
        filterDef.ConfStatus = "0;1;2;3";
        filterDef.PayStatus = "2;3";
        filterDef.DateFormat = datePatern;
        filterDef.ShowAllRes = UserData.ShowAllRes;
        return filterDef;
    }

    [WebMethod(EnableSession = true)]
    public static string CreateFilterData(string clear)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        bool? _clear = Conversion.getBoolOrNull(clear);
        if ((_clear.HasValue ? _clear.Value : true))
            HttpContext.Current.Session["FilterDefPayment"] = null;

        List<paymentMonitorDefaultRecord> defaultData = new AgencyPaymentMonitors().getResMonDefaultData(UserData, ref errorMsg);
        paymentMonitorFilterData filterData = new paymentMonitorFilterData();
        //if (createFilter() != "OK") Newtonsoft.Json.JsonConvert.SerializeObject(filterData);
        filterData.Filter = createFilter();
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            filterData.AgencyOfficeData = new AgencyPaymentMonitors().getAgencyOffices(defaultData).Where(w => w.Code == UserData.AgencyID).ToList<listString>();
        else filterData.AgencyOfficeData = new AgencyPaymentMonitors().getAgencyOffices(defaultData);
        filterData.AgencyUserData = new AgencyPaymentMonitors().getAgencyUser(defaultData);
        filterData.DepCityData = new AgencyPaymentMonitors().getDepCity(defaultData);
        filterData.ArrCityData = new AgencyPaymentMonitors().getArrCity(defaultData);

        if (filterData.AgencyUserData.Count > 0)
        {
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
                filterData.ShowAllRes = true;
            else filterData.Filter.AgencyUser = UserData.UserID;
            filterData.ShowAllRes = UserData.ShowAllRes;
            if (!filterData.ShowAllRes && filterData.AgencyUserData.Where(w => w.Code != UserData.UserID).Count() > 0)
                foreach (listString row in filterData.AgencyUserData.Where(w => w.Code != UserData.UserID).Select(s => s).ToList<listString>())
                    filterData.AgencyUserData.Remove(row);
        }
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr))
            filterData.ShowDraft = true;

        return Newtonsoft.Json.JsonConvert.SerializeObject(filterData);
    }

    public static paymentMonitorFilterRecord convertFilterRecord(paymentMonitorFilterJSonRecord _data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        paymentMonitorFilterRecord retVal = new paymentMonitorFilterRecord();
        retVal.AgencyOffice = _data.AgencyOffice;
        retVal.AgencyUser = _data.AgencyUser;
        retVal.ArrCity = Conversion.getInt32OrNull(_data.ArrCity);
        retVal.DueDate = Conversion.getDateTimeOrNull(_data.DueDate);
        retVal.BegDate1 = Conversion.getDateTimeOrNull(_data.BegDate1);
        retVal.BegDate2 = Conversion.getDateTimeOrNull(_data.BegDate2);
        retVal.ResDate1 = Conversion.getDateTimeOrNull(_data.ResDate1);
        retVal.ResDate2 = Conversion.getDateTimeOrNull(_data.ResDate2);
        retVal.ConfStatus = _data.ConfStatus;
        retVal.DateFormat = _data.DateFormat;
        retVal.DepCity = Conversion.getInt32OrNull(_data.DepCity);
        retVal.LeaderNameF = _data.LeaderNameF;
        retVal.LeaderNameS = _data.LeaderNameS;
        retVal.PageNo = Conversion.getInt32OrNull(_data.PageNo);
        retVal.PayStatus = _data.PayStatus;
        retVal.ResDate1 = Conversion.getDateTimeOrNull(_data.ResDate1);
        retVal.ResDate2 = Conversion.getDateTimeOrNull(_data.ResDate2);
        retVal.ResStatus = _data.ResStatus;
        retVal.IncludeDueDate = Equals(_data.IncludeDueDate, "1") ? true : false;
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string GetSummary()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        StringBuilder sb = new StringBuilder();
        List<AgencyPaymentMonitorFields> list = ReadFile();

        var sums = from s in list.AsEnumerable()
                   group s by new { Cur = s.SaleCur } into g
                   select new
                   {
                       Cur = g.Key.Cur,
                       Payable = list.AsEnumerable().Where(w => w.SaleCur == g.Key.Cur.ToString()).Sum(s => s.AgencyPayable),
                       Payable2 = list.AsEnumerable().Where(w => w.SaleCur == g.Key.Cur.ToString()).Sum(s => s.AgencyPayable2),
                       Payment = list.AsEnumerable().Where(w => w.SaleCur == g.Key.Cur.ToString()).Sum(s => s.AgencyPayment),
                       Balance = list.AsEnumerable().Where(w => w.SaleCur == g.Key.Cur.ToString()).Sum(s => s.Balance)
                   };

        sb.Append("<div id=\"divSummation\">");
        sb.Append("<div class=\"reservationHeaderRow\">");
        sb.Append("<div class=\"divSumCurHeader\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Cur") + "</div>");
        sb.Append("<div class=\"divSumAgencyPayableHeader\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyPayable") + "</div>");
        sb.Append("<div class=\"divSumAgencyPayableAccordingHeader\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyPayableAccording") + "</div>");
        sb.Append("<div class=\"divSumAgencyPaymentHeader\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyPayment") + "</div>");
        sb.Append("<div class=\"divSumDueDateBalanceHeader\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "DueDateBalance") + "</div>");
        sb.Append("</div>");
        int cnt = 0;
        foreach (var record in sums)
        {
            if (Convert.ToInt32(cnt / 2.0) == (cnt / 2.0))
                sb.Append("<div class=\"reservationRow\" style=\"background-color: #FFF;\">");
            else sb.Append("<div class=\"reservationRow\" style=\"background-color: #CCC;\">");
            sb.Append("<div class=\"divSumCur\"><span>" + record.Cur + "</span></div>");
            sb.Append("<div class=\"divSumAgencyPayable\"><span>" + record.Payable + "</span></div>");
            sb.Append("<div class=\"divSumAgencyPayableAccording\"><span>" + record.Payable2 + "</span></div>");
            sb.Append("<div class=\"divSumAgencyPayment\"><span>" + record.Payment + "</span></div>");
            sb.Append("<div class=\"divSumDueDateBalance\"><span>" + record.Balance + "</span></div>");
            sb.Append("</div>");
            cnt++;
        }
        sb.Append("</div>");
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string GetPaymentMonitorGrid(string data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string error = "";
        string _dataJson = data.Replace('<', '{').Replace('>', '}').Replace('|', '"');
        paymentMonitorFilterJSonRecord dt = Newtonsoft.Json.JsonConvert.DeserializeObject<paymentMonitorFilterJSonRecord>(_dataJson);
        paymentMonitorFilterRecord _data = convertFilterRecord(dt);
        List<AgencyPaymentMonitorFields> list = new AgencyPaymentMonitors().getAgencyPaymentMonitorData((User)HttpContext.Current.Session["UserData"],
                                                        _data.AgencyUser, _data.AgencyOffice, _data.ArrCity,
                                                        _data.DepCity, _data.BegDate1, _data.BegDate2,
                                                        _data.ResDate1, _data.ResDate2, _data.DueDate,
                                                        _data.PayStatus, _data.ResStatus, _data.ConfStatus,
                                                        _data.IncludeDueDate, _data.LeaderNameS, _data.LeaderNameF, 
                                                        _data.ResNo1, _data.ResNo2, ref error);

        CreateAndWriteFile(list);
        HttpContext.Current.Session.Add("pageNo", 1);
        return ListToTable(list);
    }

    private static void CreateAndWriteFile(List<AgencyPaymentMonitorFields> result)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string yol = AppDomain.CurrentDomain.BaseDirectory + "Cache\\AgencyPaymentMonitor." + HttpContext.Current.Session.SessionID;
        if (File.Exists(yol))
            File.Delete(yol);
        FileStream f = new FileStream(yol, FileMode.OpenOrCreate, FileAccess.Write);
        StreamWriter writer = new StreamWriter(f);
        writer.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
        writer.Close();
        f.Close();
    }

    private static string ListToTable(List<AgencyPaymentMonitorFields> list)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        StringBuilder sb = new StringBuilder();
        sb.Append("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" id=\"resTable\" class=\"display\" style=\"margin-left: 0pt; width: 1000px;\">");
        #region Header
        /*
        sb.Append("<thead>");
        sb.Append("<tr>");
        sb.Append("<th class=\"sorting_asc\" style=\"width:75px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "ResNo") + "</th>");
        sb.Append("<th class=\"sorting\" style=\"width:125px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "DepCity") + "</th>");
        sb.Append("<th class=\"sorting\" style=\"width:125px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "ArrCity") + "</th>");
        sb.Append("<th class=\"sorting date\" style=\"width:60px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "BegDate") + "</th>");
        sb.Append("<th class=\"sorting date\" style=\"width:60px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "EndDate") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:30px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Nights") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:50px;\">" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblStatus") + "</th>");
        sb.Append("<th class=\"sorting\" style=\"width:60px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "SaleDate") + "</th>");
        sb.Append("<th class=\"sorting date\" style=\"width:125px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Leader") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:30px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Adult") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:30px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Child") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:30px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Currency") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:70px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "SalePrice") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:70px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyCom") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:70px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Discount") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:70px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "BrokerCom") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:70px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencySupDis") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:70px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyComSup") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:70px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyEB") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:70px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "PassengerEB") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:70px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyPayable") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:70px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyPayableAccording") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:70px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyPayment") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:70px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "DueDateBalance") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:70px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "UserBonus") + "</th>");
        sb.Append("<th class=\"sorting price\" style=\"width:70px;\">" + HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "AgencyBonus") + "</th>");
        sb.Append("</tr>");
        sb.Append("</thead>");
        */
        #endregion
        int cnt = 0;
        sb.Append("<tbody>");
        foreach (AgencyPaymentMonitorFields record in list)
        {
            if (Convert.ToInt32(cnt / 2.0) == (cnt / 2.0))
                sb.Append("<tr class=\"gradeU odd\">");
            else sb.Append("<tr  class=\"gradeU even\">");
            string resNoDiv = string.Empty;
            resNoDiv += string.Format("<strong>{0}</strong>", record.ResNo);
            resNoDiv += "<br />";
            resNoDiv += string.Format("<span style=\"text-decoration: underline;cursor: pointer;\" onclick=\"showResView('{0}');\">{1}</span>",
                            record.ResNo, HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "View"));
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            {
                resNoDiv += "&nbsp;&nbsp;|&nbsp;&nbsp;";
                resNoDiv += string.Format("<span style=\"text-decoration: underline;cursor: pointer;\" onclick=\"showResInv('{0}');\">{1}</span>",
                                record.ResNo,
                                HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "Invoice"));
            }            
            sb.Append("<td style=\"width:75px;\">" + resNoDiv + "</td>");
            sb.Append("<td style=\"width:125px;\" NOWRAP>" + record.DepCityName + "</td>");
            sb.Append("<td style=\"width:125px;\" NOWRAP>" + record.ArrCityName + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:60px;\">" + record.BegDate.Value.ToShortDateString() + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:60px;\">" + record.EndDate.Value.ToShortDateString() + "</td>");
            sb.Append("<td class=\"centeraligned\" style=\"width:30px;\">" + record.Days + "</td>");
            string statusStr = string.Format("<img src=\"Images/Status/ResStatus{0}.gif\" width=\"16\" height=\"16\" title=\"{1}\">"
                + "<img src=\"Images/Status/ConfStatus{2}.gif\" width=\"16\" height=\"16\" title=\"{3}\">",
                    record.ResStat,
                    HttpContext.GetGlobalResourceObject("ResMonitor", "lblResStatus").ToString() + ", " +
                        HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + record.ResStat.ToString()).ToString(),
                    record.ConfStat,
                    HttpContext.GetGlobalResourceObject("ResMonitor", "lblConfStatus").ToString() + ", " +
                        HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + record.ConfStat.ToString()).ToString());
            sb.Append("<td class=\"centeraligned\"style=\"width:50px;\">" + statusStr + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:60px;\">" + record.ResDate.Value.ToShortDateString() + "</td>");
            sb.Append("<td style=\"width:125px;\" NOWRAP>" + record.Leader + "</td>");
            sb.Append("<td class=\"centeraligned\" style=\"width:30px;\">" + record.Adult + "</td>");
            sb.Append("<td class=\"centeraligned\" style=\"width:30px;\">" + record.Child + "</td>");
            sb.Append("<td class=\"centeraligned\" style=\"width:30px;\">" + record.SaleCur + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:70px;\">" + record.SalePrice + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:70px;\">" + record.AgencyCom + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:70px;\">" + record.Discount + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:70px;\">" + record.BrokerCom + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:70px;\">" + record.AgencySupDis + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:70px;\">" + record.AgencyComSup + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:70px;\">" + record.EBAgency + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:70px;\">" + record.EBPas + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:70px;\">" + record.AgencyPayable + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:70px;\">" + record.AgencyPayable2 + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:70px;\">" + record.AgencyPayment + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:70px;\">" + record.Balance + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:70px;\">" + record.UserBonus + "</td>");
            sb.Append("<td class=\"rightaligned\" style=\"width:70px;\">" + record.AgencyBonus + "</td>");
            sb.Append("</tr>");
            cnt++;
        }
        sb.Append("</tbody>");
        sb.Append("</table>");
        sb.Append("</div>");
        return sb.ToString();
    }

    private static List<AgencyPaymentMonitorFields> ReadFile()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string yol = AppDomain.CurrentDomain.BaseDirectory + "Cache\\AgencyPaymentMonitor." + HttpContext.Current.Session.SessionID;
        StreamReader reader = new StreamReader(yol);
        List<AgencyPaymentMonitorFields> list = new List<AgencyPaymentMonitorFields>();
        try
        {
            list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgencyPaymentMonitorFields>>(reader.ReadToEnd());
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            reader.Close();
        }
        return list;
    }

    [WebMethod(EnableSession = true)]
    public static string getInvoice(string ResNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        ResDataRecord ResData = new ResTables().getReservationData(UserData, ResNo, string.Empty, ref errorMsg);
        HttpContext.Current.Session["ResData"] = ResData;
        List<ResViewMenu> subMenuList = CacheObjects.getResViewMenuData();
        var invoiceM = subMenuList.Where(w => w.Code == "Invoice");
        bool invoiceMenu = false;
        if (invoiceM.Count() > 0)
            invoiceMenu = true;
        else invoiceMenu = false;

        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        string docFolder = string.Empty;
        if (invoiceM.FirstOrDefault().SubFolder)
            docFolder = basePageUrl + (invoiceM.FirstOrDefault().SubFolder ? (DocumentFolder + "/" + UserData.Market + "/") : "");
        else docFolder = "";
        string DocUrl = string.Empty;
        if (invoiceMenu)
            DocUrl = docFolder + invoiceM.FirstOrDefault().Url;
        string docFile = docFolder + DocUrl;
        string retValM = "\"htmlUrl\":\"{0}\",\"DocName\":\"{1}\",\"docNameResource\":\"{2}\"";
        string retVal = string.Format(retValM,
                        DocUrl,
                        "Invoice",
                        new UICommon().GetSubMenuGlobalResourceObject(UserData, invoiceM.FirstOrDefault().ResourceName).ToString());
        return "{" + retVal + "}";
    }


    [WebMethod(EnableSession = true)]
    public static string viewPDFReport(string reportType, string _html, string pageWidth, string ResNo, string urlBase, string pdfConvSerial)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        _html = _html.Replace('|', '"');
        _html = _html.Replace("<div id=\"noPrintDiv\">", "<div id=\"noPrintDiv\" style=\"display: none; visibility: hidden;\">");
        string errorMsg = string.Empty;
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        string pdfFileName = reportType + "_" + ResNo + ".pdf";
        StringBuilder html = new StringBuilder(_html);
        int PageWidth = Conversion.getInt32OrNull(pageWidth).HasValue ? Conversion.getInt32OrNull(pageWidth).Value : 700;

        //initialize the PdfConvert object
        PdfConverter pdfConverter = new PdfConverter();
        
        pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
        pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.NoCompression;
        pdfConverter.PdfDocumentOptions.ShowHeader = false;
        pdfConverter.PdfDocumentOptions.ShowFooter = false;
        pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;        
        pdfConverter.PdfDocumentOptions.LeftMargin = 10;
        pdfConverter.PdfDocumentOptions.TopMargin = 10;
        // set the demo license key
        pdfConverter.LicenseKey = pdfConvSerial; // "RG92ZHVkdXF0cGRxanRkd3VqdXZqfX19fQ==";

        // get the base url for string conversion which is the url from where the html code was retrieved
        // the base url is a hint for the converter to find the external CSS and images referenced by relative URLs
        string thisPageURL = HttpContext.Current.Request.Url.AbsoluteUri;
        string baseUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/";

        try
        {
            pdfConverter.SavePdfFromHtmlStringToFile(html.ToString(), siteFolderISS + "ACE\\" + pdfFileName, urlBase);
            string returnUrl = basePageUrl + "ACE/" + pdfFileName;
            return returnUrl;
        }
        catch
        {
            return "";
        }
    }
}