﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangePLFlight.aspx.cs" Inherits="ChangePLFlight" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "ChangePLFlight")%></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/ChangePLFlight.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">

        if (typeof window.event != 'undefined')
            document.onkeydown = function() {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function(e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function logout() {
            self.parent.logout();
        }

        function showMsg(msg) {
            $(function() {
                $("#messages").html('');
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    autoResize: true,
                    resizable: true,
                    resizeStop: function(event, ui) {
                        $(this).dialog({ position: 'center' });
                    },
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function selectFlight(depFlightNo, retFlightNo, refNo) {
            var depClass = $("#flightClassD_" + refNo).val();
            var retClass = $("#flightClassR_" + refNo).val();
            $.ajax({
                type: "POST",
                url: "ChangePLFlight.aspx/changeFlights",
                data: '{"DepFlightNo":"' + depFlightNo + '","DepClass":"' + depClass + '","RetFlightNo":"' + retFlightNo + '","RetClass":"' + retClass + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != '')
                        showMsg(msg.d);
                    else {
                        window.close;
                        self.parent.retutnChangeFlight();
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getFormData() {
            $.ajax({
                type: "POST",
                url: "ChangePLFlight.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#flightListDiv").html('');
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        $("#flightListDiv").html(msg.d);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                getFormData();
            });
    </script>

</head>
<body>
    <form id="ChangePLFlightForm" runat="server">
    <div id="flightListDiv">
    </div>
    <div id="dialog-message" title="" style="display: none;">
        <div id="messages">
        </div>
    </div>
    </form>
</body>
</html>
