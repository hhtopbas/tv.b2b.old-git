﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using TvTools;

public partial class ComplaintImageUpload : BasePage
{
    string ResNo = string.Empty;
    int? RecID = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        if (!string.IsNullOrEmpty(Request.Params["ResNo"])) {
            ResNo = (string)Request.Params["ResNo"];
            RecID = Conversion.getInt32OrNull(Request.Params["RecID"]);
        }
    }
    protected void Upload(object sender, EventArgs e)
    {        
        User UserData = (User)Session["UserData"];                
        if (fileUpld.HasFile) {
            string errorMsg = string.Empty;
            string fileName = Path.GetFileName(fileUpld.PostedFile.FileName);
            fileName = ResNo + "_" + UserData.SID + "_" + fileName;
            fileUpld.PostedFile.SaveAs(Server.MapPath("~/Cache/") + fileName);
            if (new Complaints().saveComplaintPicture(UserData, Server.MapPath("~/Cache/") + fileName, RecID, ref errorMsg))
                FileName.Value = fileName;
        }
    }
}