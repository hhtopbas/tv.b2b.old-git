﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlyExcursionSearchFilter.aspx.cs" Inherits="OnlyExcursionSearchFilter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Excursion</title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>
    <script src="Scripts/Tv.PackageSearchFilterV2.js" type="text/javascript"></script>
    <script src="Scripts/jquery.timepicker.min.js" type="text/javascript"></script>    

    <link href="CSS/jquery.timepicker.css" rel="stylesheet" />
    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/PriceSearchFilter.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function logout() {
            self.parent.logout();
        }

        var myWidth = 0, myHeight = 0;

        $.blockUI.defaults.message = '<h1>' + '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>' + '</h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function reSizeFrame() {
            self.parent.reSizeFilterFrame($('body').height());
        }

        function showDialogYesNo(msg) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                        $(this).dialog('close');
                        return true;
                    }
                        ,
                    '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>': function () {
                        $(this).dialog('close');
                        return false;
                    }
                }
            });
        }


        function showDialog(msg) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                        $(this).dialog('close');
                    }
                }
            });
        }

        function showMessage(msg, transfer, trfUrl) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                            $(this).dialog('close');
                        }
                        ,
                        '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>': function () {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function getInf(adult, whois) {
            var Adult = parseInt(adult);
            $("#fltInfant" + whois).html("");
            for (i = 0; i < Adult + 1; i++) {
                $("#fltInfant" + whois).append("<option value='" + i.toString() + "'>" + i.toString() + "</option>");
            }
        }

        function SetChild(childCount, whois) {
            if (childCount > 0) $("#divRoomInfoChd1" + whois).show(); else $("#divRoomInfoChd1" + whois).hide();
            if (childCount > 1) $("#divRoomInfoChd2" + whois).show(); else $("#divRoomInfoChd2" + whois).hide();
            if (childCount > 2) $("#divRoomInfoChd3" + whois).show(); else $("#divRoomInfoChd3" + whois).hide();
            if (childCount > 3) $("#divRoomInfoChd4" + whois).show(); else $("#divRoomInfoChd4" + whois).hide();
        }

        function getChild(child, whois) {
            if ($("#divRoomInfoChd" + whois).length > 0) {
                var _divChild = $("#divRoomInfoChd" + whois);
                if (parseInt(child) == 0) {
                    SetChild(parseInt(child), whois);
                    _divChild.hide();
                }
                else {
                    _divChild.show();
                    SetChild(parseInt(child), whois);
                }
            }
        }

        function onChildChange(whois) {
            getChild($("#fltChild" + whois).val(), whois);
            reSizeFrame();
        }

        function getRoomPaxs() {
            var roomCount = parseInt($("#fltRoomCount").val());
            var result = '';
            for (i = 1; i < roomCount + 1; i++) {
                var adult = parseInt($("#fltAdult" + i.toString()).val());
                var child = parseInt($("#fltChild" + i.toString()).val());
                var chd1Age = parseInt($("#fltRoomInfoChd1" + i.toString()).val());
                var chd2Age = parseInt($("#fltRoomInfoChd2" + i.toString()).val());
                var chd3Age = parseInt($("#fltRoomInfoChd3" + i.toString()).val());
                var chd4Age = parseInt($("#fltRoomInfoChd4" + i.toString()).val());
                if (i > 1)
                    result += ',(';
                else result += '(';
                result += '|Adult|:|' + adult.toString() + '|';
                result += ',|Child|:|' + child.toString() + '|';
                result += ',|Chd1Age|:|' + (child > 0 ? chd1Age : -1) + '|';
                result += ',|Chd2Age|:|' + (child > 1 ? chd2Age : -1) + '|';
                result += ',|Chd3Age|:|' + (child > 2 ? chd3Age : -1) + '|';
                result += ',|Chd4Age|:|' + (child > 3 ? chd4Age : -1) + '|';
                result += ')';
            }
            var hfRoomInfo = $("#hfRoomInfo");
            hfRoomInfo.val(result);
            reSizeFrame();
        }

        function filterclear() {
            self.parent.pageReLoad();
        }

        function setCriteria() {
            getRoomPaxs();
            var data = new Object();
            data.Type = $("#fltType").val();
            data.CheckIn = $("#iCheckIn").datepicker('getDate').getTime();
            data.ExpandDate = $("#fltCheckInDay").val();
            data.Hour = $("#fltHour").val();
            data.HourExpand = $("#fltHourExpand").val();
            data.Duration = $("#fltDuration").val();
            data.RoomCount = $("#fltRoomCount").val();
            data.roomsInfo = $("#hfRoomInfo").val();
            data.ArrCity = $("#fltArrCity").val();
            data.Category = $("#hfCategory").val();
            data.CurControl = ($("#CurrencyConvert")[0].checked ? 'Y' : 'N');
            data.CurrentCur = $("#currentCur").val();
            data.FlightNo = $("#iFlightNo").val();
            data.Airport = $("#iAirport").val();
            $.ajax({
                type: "POST",
                url: "OnlyExcursionSearchFilter.aspx/SetCriterias",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getRoomInfo(setCrt) {
            var roomCount = $("#fltRoomCount").val();
            $.ajax({
                type: "POST",
                url: "OnlyExcursionSearchFilter.aspx/getRoomInfo",
                data: '{"RoomCount":"' + roomCount + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#fltRoomInfo").html('');
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        $("#fltRoomInfo").html(msg.d);
                    }
                    reSizeFrame();
                    if (setCrt == true)
                        setCriteria();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getAirports() {
            var data = new Object();
            data.city = parseInt($("#fltArrCity").val());
            $.ajax({
                type: "POST",
                url: "OnlyExcursionSearchFilter.aspx/getAirports",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#iAirport").html('');
                    $("#iAirport").append("<option value='' >" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        $.each(msg.d, function (i) {
                            $("#iAirport").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changeArrCity() {
            $("#txtCategory").html('');
            $("#hfCategory").val('');
            $("#divListPopup").html('');
            if ($("#fltType").val() == '1')
                getAirports();
        }

        function getArrival(type) {
            var data = new Object();
            $.ajax({
                type: "POST",
                url: "OnlyExcursionSearchFilter.aspx/getArrival",
                data: '{"Type":"' + type + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#fltArrCity").html('');
                    $("#fltArrCity").append("<option value='' >" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        $.each(msg.d, function (i) {
                            $("#fltArrCity").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getFormData() {
            $.ajax({
                type: "POST",
                url: "OnlyExcursionSearchFilter.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var data = msg.d;
                        if (data == null || data == undefined) {
                            self.location = self.location;
                            return false;
                        }

                        $("#CustomRegID").val(data.CustomRegID);

                        $("#fltCheckInDay").html(''); $("#fltCheckInDay").html(packageSearchFilterV2_fltCheckInDay(data.fltCheckInDay, '<%= GetGlobalResourceObject("LibraryResource", "lblDay") %>'));
                        $("#fltRoomCount").html(''); $("#fltRoomCount").html(packageSearchFilterV2_fltRoomCount(data.fltRoomCount));
                        $("#fltHourExpand").html(''); $("#fltHourExpand").html(packageSearchFilterV2_fltRoomCount(data.fltHourExpand));
                        $("#fltDuration").html(''); $("#fltDuration").html(packageSearchFilterV2_fltRoomCount(data.fltDuration));
                        $("#divCurrency").html('');
                        $("#divCurrency").html(data.MarketCur);
                        $("#hfDate").val('');
                        $("#hfDate").val(data.CheckIn);

                        $.datepicker.setDefaults($.datepicker.regional[data.DateFormat != 'en' ? data.DateFormat : '']);
                        $("#fltCheckInDay").html(packageSearchFilterV2_fltCheckInDay(data.fltCheckInDay, '<%= GetGlobalResourceObject("LibraryResource", "lblDay") %>'));

                        var _date1 = new Date(Date(eval(data.CheckIn)).toString());
                        $("#iCheckIn").datepicker('setDate', _date1);

                        //if (!data.ShowExpandDay) $("#divExpandDate").hide();

                        getArrival();
                        getRoomInfo(true);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function onChange_fltType() {
            var value = $("#fltType").val();
            switch (value) {
                case '0': // Regular                    
                    $("#spHour").hide();
                    $("#trHour").hide();
                    $("#spDuration").hide();
                    $("#trDuration").hide();
                    $("#spCategory").show();
                    $("#trCategory").show();
                    $("#divExpandDate").hide();
                    $("#trFlightNo").hide();
                    $("#spFlightNo").hide();
                    $("#trAirport").hide();
                    $("#spAirport").hide();
                    break;
                case '1': // Transit
                    $("#spHour").show();
                    $("#trHour").show();
                    $("#spDuration").show();
                    $("#trDuration").show();
                    $("#spCategory").hide();
                    $("#trCategory").hide();
                    $("#divExpandDate").hide();
                    $("#trFlightNo").show();
                    $("#spFlightNo").show();
                    $("#trAirport").hide();
                    $("#spAirport").hide();
                    break;
                case '2': // Private
                    $("#spHour").hide();
                    $("#trHour").hide();
                    $("#spDuration").hide();
                    $("#trDuration").hide();
                    $("#spCategory").show();
                    $("#trCategory").hide();
                    $("#divExpandDate").hide();
                    $("#trFlightNo").hide();
                    $("#spFlightNo").hide();
                    $("#trAirport").hide();
                    $("#spAirport").hide();
                    break;
            }
            getArrival(value);
        }

        function listPopupFillData(sourceBtn, clear) {
            var arrCity = $("#fltArrCity").val();
            var category = $("#hfCategory").val();
            var paraList = '';
            var serviceUrl = '';
            switch (sourceBtn) {
                case 'category':
                    paraList = '{"ArrCity":"' + arrCity + '"}';
                    serviceUrl = 'OnlyExcursionSearchFilter.aspx/getCategoryData';
                    $.ajax({
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: serviceUrl,
                        success: function (msg) {
                            if (clear == true) $("#hfCategory").val('');
                            if (msg.hasOwnProperty('d') && msg.d != null) {
                                var inputDataArray = msg.d;
                                var listChk = $("#hfCategory");
                                var _input = '';
                                var mydata = inputDataArray;
                                if (mydata != null)
                                    $.each(mydata, function (index, inputData) {
                                        var checked = '';
                                        if (listChk.val().length > 0) {
                                            var chkListStr = listChk.val();
                                            var chkList = chkListStr.split('|');
                                            if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                                                checked = 'checked=\'checked\'';
                                            else checked = '';
                                        } else checked = '';
                                        _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
                                    });
                                $("#divListPopup").html('');
                                $("#divListPopup").html(_input);
                            }
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                alert(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                    break;
                default: break;
            }
            reSizeFrame();
        }

        function listPopupGetSelected(sourceBtn) {
            switch (sourceBtn) {
                case 'category':
                    var hfCategory = $("#hfCategory");
                    var iCategory = $("#txtCategory");
                    var listChk = '';
                    var listTxt = '';
                    iCategory.text('');
                    $(":checked", "#divListPopup").each(function () {
                        if (listChk.length > 0) {
                            listChk += '|';
                            listTxt += '<br />';
                        }
                        listChk += $(this).val();
                        listTxt += $(this).next().text();
                    });

                    hfCategory.val(listChk);
                    iCategory.html(listTxt);
                    break;
                default: break;
            }
            reSizeFrame();
        }

        function showListPopupAddButton(btnName, func) {
            $.extend($.ui.dialog.prototype, {
                'addbutton': function (buttonName, func) {
                    var buttons = this.element.dialog('option', 'buttons');
                    buttons[buttonName] = func;
                    this.element.dialog('option', 'buttons', buttons);
                }
            });
            $("#dialog-listPopup").dialog('addbutton', btnName, func);
        }

        function clearBtnAdd(sourceBtn) {
            $("#divListPopup").html('');
            listPopupFillData(sourceBtn, true);
            return false;
        }

        function showListPopup(_title, sourceBtn) {
            $("#divListPopup").html('');
            listPopupFillData(sourceBtn, false);

            $("#dialog").dialog("destroy");
            $("#dialog-listPopup")
            .dialog({
                title: _title,
                modal: true,
                height: 400,
                width: 240,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                        listPopupGetSelected(sourceBtn);
                        $(this).dialog('close');
                        return true;
                    }
                        ,
                    '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>': function () {
                        $(this).dialog('close');
                        return false;
                    }
                }
            })
            .css('font-size', '7px');

            if ($("#CustomRegID").val() != '0970301') {
                showListPopupAddButton('<%= GetGlobalResourceObject("LibraryResource", "btnClear") %>',
        function () {
            $("#divListPopup").html('');
            listPopupFillData(sourceBtn, true);
            return false;
        });
        }

        reSizeFrame();
    }

    function search() {

        getRoomPaxs();
        var data = new Object();
        data.Type = $("#fltType").val();
        data.CheckIn = $("#iCheckIn").datepicker('getDate').getTime();
        data.ExpandDate = $("#fltCheckInDay").length > 0 ? $("#fltCheckInDay").val() : "0";
        data.Hour = $("#fltHour").val();
        data.HourExpand = $("#fltHourExpand").length > 0 ? $("#fltHourExpand").val() : "0";
        data.Duration = $("#fltDuration").val();
        data.RoomCount = $("#fltRoomCount").val();
        data.roomsInfo = $("#hfRoomInfo").val();
        data.ArrCity = $("#fltArrCity").val();
        data.Category = $("#hfCategory").val();
        data.CurControl = ($("#CurrencyConvert")[0].checked ? 'Y' : 'N');
        data.CurrentCur = $("#currentCur").val();
        data.FlightNo = $("#iFlightNo").val();
        data.Airport = $("#iAirport").val();        
        $.ajax({
            type: "POST",
            url: "OnlyExcursionSearchFilter.aspx/SetCriterias",
            data: $.json.encode(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                self.parent.searchPrice();
            },
            error: function (xhr, msg, e) {
                if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                    alert(xhr.responseText);
            },
            statusCode: {
                408: function () {
                    logout();
                }
            }
        });
        reSizeFrame();
    }

    $(document).ready(function () {

        $.datepicker.setDefaults($.datepicker.regional['<%= twoLetterISOLanguageName %>' != 'en' ? '<%= twoLetterISOLanguageName %>' : '']);
        
        $("#iCheckIn").datepicker({
            showOn: "button",
            buttonImage: "Images/Calendar.gif",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            onSelect: function (dateText, inst) {
                if (dateText != '') {
                    var date2 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
                    var iDate = $("#hfDate");
                    if (date2) {
                        iDate.val(Date.parse(date2));
                    }
                    else {
                        iDate.val(Date.parse(new Date()));
                    }
                }
            },
            minDate: new Date()
            //,beforeShowDay: flightDays
        });
        $("#fltHour").timepicker({ 'timeFormat': 'H:i' });
        getFormData();
        onChange_fltType();
    });
    </script>

</head>
<body>
    <form id="SearchFilterForm" runat="server">
        <input id="CustomRegID" type="hidden" value="" />
        <div style="width: 250px;">
            <table cellpadding="2" cellspacing="0" style="text-align: left; width: 250px;">
                <tr>
                    <td>
                        <strong>
                            <%= GetGlobalResourceObject("OnlyExcursion", "ExcursionType") %></strong>
                        <br />
                        <select id="fltType" onchange="onChange_fltType()">
                            <option value="0"><%= GetGlobalResourceObject("OnlyExcursion", "ExcursionType0") %></option>
                            <option value="1"><%= GetGlobalResourceObject("OnlyExcursion", "ExcursionType1") %></option>
                            <option value="2"><%= GetGlobalResourceObject("OnlyExcursion", "ExcursionType2") %></option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <%= GetGlobalResourceObject("PackageSearchFilter", "lblArrCity") %></strong>
                        <br />
                        <select id="fltArrCity" onchange="changeArrCity();">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="divCheckIn" style="width: 148px; float: left;">
                            <strong>
                                <%= GetGlobalResourceObject("OnlyExcursion", "lblTourDate") %></strong>
                            <br />
                            <input id="iCheckIn" style="width: 100px;" />
                        </div>
                        <div id="divExpandDate" style="width: 95px; float: left; display: none;">
                            <strong>&nbsp;&nbsp;</strong>
                            <br />
                            <div style="font-size: 12pt; font-weight: bold; float: left;">±</div>
                            <select id="fltCheckInDay" style="width: 80px;">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr id="spAirport" class="ui-helper-hidden">
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr id="trAirport" class="ui-helper-hidden">
                    <td class="ui-helper-hidden">
                        <div style="width: 148px; float: left;"  class="ui-helper-hidden">
                            <strong>
                                <%= GetGlobalResourceObject("Controls", "trfViewAirportLocation") %></strong>
                            <br />
                            <select id="iAirport">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr id="spFlightNo">
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr id="trFlightNo">
                    <td>
                        <div style="width: 148px; float: left;">
                            <strong>
                                <%= GetGlobalResourceObject("Controls", "viewFlight") %></strong>
                            <br />
                            <input id="iFlightNo" type="text" style="width: 75px;" />
                        </div>
                    </td>
                </tr>
                <tr id="spHour">
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr id="trHour">
                    <td>
                        <div style="width: 148px; float: left;">
                            <strong>
                                <%= GetGlobalResourceObject("OnlyExcursion", "lblHour") %></strong>
                            <br />
                            <input id="fltHour" class="time ui-timepicker-input" type="text" style="width: 75px;" />
                        </div>
                        <div style="width: 95px; float: left; display: none;">
                            <strong>&nbsp;&nbsp;</strong>
                            <br />
                            <div style="font-size: 12pt; font-weight: bold; float: left;">±</div>
                            <select id="fltHourExpand" style="width: 50px;">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr id="spDuration">
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr id="trDuration">
                    <td>
                        <strong>
                            <%= GetGlobalResourceObject("OnlyExcursion", "lblDuration") %></strong>
                        <br />
                        <select id="fltDuration" style="width: 50px;">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="display: none;">
                            <strong>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblRoomCount") %></strong>
                            <br />
                            <select id="fltRoomCount" onchange="getRoomInfo(false);">
                            </select>
                        </div>
                        <div id="divRoomInfo" style="width: 100%;">
                            <div id="fltRoomInfo">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr id="spCategory">
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr id="trCategory">
                    <td>
                        <div style="width: 100%; height: 22px; position: relative; vertical-align: bottom;">
                            <span style="position: absolute; bottom: 2px;"><b>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblCategory") %></b> </span>
                            <img alt="" title="" src="Images/search.png" height="22px" style="position: absolute; right: 2px;"
                                onclick="showListPopup('Category List', 'category');" />
                        </div>
                        <div id="txtCategory" class="multiField">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="divCurrency" style="clear: both; width: 100%;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="z-index: 1; text-align: center;">
                        <input type="button" name="btnSearch" value='<%= GetGlobalResourceObject("LibraryResource", "btnSearch") %>'
                            style="width: 100px;" onclick="search()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />&nbsp;
                    <input type="button" name="btnClear" value='<%= GetGlobalResourceObject("LibraryResource", "btnClear") %>'
                        style="width: 100px;" onclick="filterclear()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="hiddenControls" style="display: none; visibility: hidden;">
            <input id="hfDate" type="hidden" />
            <input id="hfCategory" type="hidden" />
            <input id="hfRoomInfo" type="hidden" />
        </div>
        <div id="dialog-listPopup" style="display: none;">
            <div id="divListPopup">
            </div>
        </div>
        <div id="dialog-message" title="" style="display: none;">
            <span id="messages">Message</span>
        </div>
    </form>
</body>
</html>
