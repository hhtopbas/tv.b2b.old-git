﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CreateOffer.aspx.cs" Inherits="CreateOffer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" id="htmlCreateOffer">
<head runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    <%= GetGlobalResourceObject("PageTitle", "CreateOffer") %></title>

  <script src="Scripts/jquery.min.js" type="text/javascript"></script>

  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

  <script src="Scripts/jquery.json.js" type="text/javascript"></script>

  <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

  <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

  <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <style type="text/css">
    html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
    html { overflow: auto; background-color: Transparent; }
    ol, ul { list-style: none; }
    blockquote, q { quotes: none; }
    :focus, a:focus, a:active { outline: 0; }
    input { outline: 0; }
    ins { text-decoration: none; }
    del { text-decoration: line-through; }
    table { border-collapse: collapse; border-spacing: 0; }
    body { width: 650px; font-family: Tahoma Times New Roman; font-size: 10pt; }
    .mainPage { width: 650px; }
    .logoDiv { width: 100%; height: 138px; position: relative; border-bottom: solid 1px #000; }
    .oprLogoDiv { width: 150px; height: 135px; float: left; }
      .oprLogoDiv img { bottom: 25px; position: absolute; }
    .agencyLogoDiv { width: 500px; height: 135px; float: right; }
    .agencyImgDiv { width: 245px; height: 135px; float: left; position: relative; text-align: right; font-family: Tahoma Times New Roman; font-size: 9pt; }
      .agencyImgDiv img { bottom: 25px; position: absolute; right: 5px; }
      .agencyImgDiv span { bottom: 3px; position: absolute; right: 5px; }
    .agencyInfoDiv { width: 245px; height: 100%; float: right; position: relative; font-family: Tahoma Times New Roman; font-size: 9pt; }
      .agencyInfoDiv span { bottom: 3px; left: 5px; position: absolute; width: 4px; }
    .dataDiv { width: 100%; border: solid 1px #000; font-family: Tahoma Times New Roman; }
    .font8pt { font-size: 8pt; }
    .font8ptBold { font-size: 8pt; font-weight: bold; }
    .font8ptItalic { font-size: 8pt; font-style: italic; }
    .font9pt { font-size: 9pt; }
    .font9ptBold { font-size: 9pt; font-weight: bold; }
    .font10pt { font-size: 10pt; }
    .font10ptBold { font-size: 10pt; font-weight: bold; }
    .font10ptItalic { font-size: 10pt; font-style: italic; }
    .font10ptBoldItalic { font-size: 10pt; font-weight: bold; font-style: italic; }
    .font11ptFont { font-size: 11pt; }
    .font11ptBold { font-size: 11pt; font-weight: bold; }
    .font11ptItalic { font-size: 11pt; font-style: italic; }
    .font11ptBoldItalic { font-size: 11pt; font-weight: bold; font-style: italic; }
    .hotelInfo { width: 100%; }
    .noteInput { border: solid 1px #c8c8c8; width: 90%; }
    .clearDiv { clear: both; border-bottom: solid 1pt #000; }
    .noPDF { display: none; visibility: hidden; }

    @media print {
      #noPrintDiv { display: none; visibility: hidden; }
      .noPrint { display: none; visibility: hidden; }
    }
  </style>

  <script type="text/javascript">

    if (typeof window.event != 'undefined')
      document.onkeydown = function () {
        if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
          return (event.keyCode != 8);
      }
    else
      document.onkeypress = function (e) {
        if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
          return (e.keyCode != 8);
      }

    function logout() {
      self.parent.logout();
    }

    function getFormData(removeOffer) {
      $.ajax({
        async: false,
        type: "POST",
        url: "CreateOffer.aspx/getFormData",
        data: '{"removeOffer":' + removeOffer + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {

          if (msg.hasOwnProperty('d') && msg.d != '') {
            var data = $.json.decode(msg.d);
            $.each(data, function (i) {
              switch (this.TagName) {
                case 'span':
                  $("#" + this.IdName).text('');
                  $("#" + this.IdName).text(this.Data);
                  break;
                case 'div':
                  $("#" + this.IdName).html('');
                  $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                  break;
                case 'img':
                  $("#" + this.IdName).removeAttr("src");
                  $("#" + this.IdName).attr("src", this.Data);
                  if (this.Data == '')
                    $("#" + this.IdName).hide();
                  break;
                default: $("#" + this.IdName).val('');
                  $("#" + this.IdName).val(this.Data);
              }

            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });


      $.query = $.query.load(location.href);


      if ($.query.get('print')) {
        $('input:checkbox').removeAttr('checked');

        $.each($.query.keys, function (i) {
          if (i != 'ResNo' && i != 'docName' && i != 'print' && i != '__VIEWSTATE') {
            var elm = $("#" + i);
            if (this == 'on' || this == 'off') {
              if (this == 'on')
                elm.attr('checked', 'checked');
              else
                elm.removeAttr('checked');
            }
            else if (!(this == true || this == false)) {
              elm.val(this);
            }
          }
        });
        window.print();
        window.close();
      }
    }

    function reCalc() {
      getFormData();
    }

    function removeList(RefNo) {
      getFormData(RefNo);
    }

    $(document).ready(function () {
      getFormData(null);
    });
  </script>

</head>
<body>
  <form id="form1" runat="server">
    <input id="param1" type="hidden" value="" />
    <div class="mainPage">
      <%--mainDiv--%>
      <table width="100%" style="border-bottom: solid 2px #000000;">
        <tr>
          <td style="width: 130px; text-align: center;">
            <img alt="" title="" id="imgOperator" style="width: 120px;" />
          </td>
          <td align="right" valign="bottom" style="min-width: 120px;">
            <table>
              <tr>
                <td align="right" valign="bottom">
                  <img alt="" title="" id="imgAgency" style="width: 120px;" /><br />
                  <span id="txtCreatedBy"></span>
                </td>
                <td valign="bottom" style="font-size: 9pt;">
                  <span id="txtAgencyName"></span>
                  <br />
                  <span id="txtUserName"></span>
                  <br />
                  <span id="txtAgencyTel"></span>
                  <br />
                  <span id="txtEmail"></span>
                  <br />
                  <span id="txtDateTime"></span>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <div class="font8pt">
        <div id="fixNoteText">
        </div>
      </div>
      <div class="clearDiv">
      </div>
      <div id="dataDiv">
      </div>
    </div>
    <%--mainDiv--%>
  </form>
</body>
</html>
