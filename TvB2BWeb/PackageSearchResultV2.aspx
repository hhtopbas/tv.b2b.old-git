﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PackageSearchResultV2.aspx.cs"
    Inherits="PackageSearchResultV2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "PackageSearchResult")%></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

    <script src="Scripts/jquery.cluetip.js" type="text/javascript"></script>

    <script src="Scripts/datatable/jquery.dataTables.min.js" type="text/javascript"></script>

    <script src="Scripts/datatable/jquery.dataTables.Pagination.ListBox.js" type="text/javascript"></script>

    <script src="Scripts/datatable/ColVis.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/PackageSearchResultV2.css" rel="stylesheet" type="text/css" />
    <link href="CSS/jquery.table.jui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/datatable/ColVis.css" rel="stylesheet" type="text/css" />
    <link href="CSS/jquery.cluetip.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #divNote { height: 30px; color: #CC0000; font-weight: bold; text-decoration: underline; cursor: help; }
        .center { text-align: center; }
        .stopSale { color: #FF0000; text-decoration: line-through !important; }
            .stopSale td { color: #FF0000; background-color: #FAB7B7; text-decoration: line-through !important; }
        .stopSaleOk { color: #FF0000; }
            .stopSaleOk td { color: #FF0000; }
        #mydiv { position: absolute; top: 0; left: 0; width: 100%; height: 100%; z-index: 1000; background-color: grey; opacity: .8; }

        .ajax-loader { position: absolute; left: 50%; top: 50%; margin-left: -32px; /* -1 * image width / 2 */ margin-top: -32px; /* -1 * image height / 2 */ display: block; }
    </style>

    <script language="javascript" type="text/javascript">

        var cultureNumber;
        var cultureDate;

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function logout() {
            self.parent.logout();
        }

        function showDialog(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function reSizeFrame() {
            self.parent.reSizeResultFrame(document.body.offsetHeight);
        }

        function makeRes(refNo, stopSaleMsg) {
            if (stopSaleMsg != '') {
                $(function () {
                    var msg = '<%= GetGlobalResourceObject("PackageSearchResult", "StopSaleWarningMessage") %>';
                    $("#messages").html(msg.replace('{0}', unescape(stopSaleMsg)));
                    $("#dialog").dialog("destroy");
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                                $(this).dialog('close');
                                self.parent.bookReservation(refNo);
                            },
                            '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>': function () {
                                $(this).dialog('close');
                            }
                        }
                    });
                });
            }
            else self.parent.bookReservation(refNo);
        }

        function showBrochure(holPack, CheckIn, CheckOut, Market) {
            self.parent.showBrochure(holPack, CheckIn, CheckOut, Market);
        }

        function clearOffer() {
            $.ajax({
                type: "POST",
                url: "PackageSearchResultV2.aspx/clearOffer",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function addOffer(refNo) {

            $.ajax({
                type: "POST",
                url: "PackageSearchResultV2.aspx/addOffer",
                data: '{"BookList":"' + refNo + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function createOffer() {
            self.parent.createOffer();
        }

        function createOfferV2() {
            self.parent.createOfferV2();
        }

        function basketMakeRes() {
            self.parent.bookReservation(null);
        }

        function clearBasketList() {
            $.ajax({
                async: false,
                type: "POST",
                data: '{}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'PackageSearchResultV2.aspx/removeBasketList',
                success: function (msg) {
                    $("#basketTableDiv").html('');
                    $("#basketTableDiv").show();
                    if (msg.d != null && msg.d.basketListOK != false) {
                        $("#basketTableDiv").html(msg.d.basketDiv);
                        reSizeFrame();
                        if (msg.d.message != '')
                            showDialog(msg.d.message);
                    }
                    else $("#basketTableDiv").hide();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function removeBasket(refNo) {
            $.ajax({
                async: false,
                type: "POST",
                data: '{"rowID":' + parseInt(refNo) + '}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'PackageSearchResultV2.aspx/removeBasket',
                success: function (msg) {
                    $("#basketTableDiv").html('');
                    $("#basketTableDiv").show();
                    if (msg.d != null && msg.d.basketListOK != false) {
                        $("#basketTableDiv").html(msg.d.basketDiv);
                        reSizeFrame();
                        if (msg.d.message != '')
                            showDialog(msg.d.message);
                    }
                    else $("#basketTableDiv").hide();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function addBasketFunc(CatPackID, ARecNo, PRecNo, HapRecId) {
            var data = new Object();
            data.CatPackId = parseInt(CatPackID);
            data.ARecNo = parseInt(ARecNo);
            data.PRecNo = parseInt(PRecNo);
            data.HapRecId = parseInt(HapRecId);
            $.ajax({
                async: false,
                type: "POST",
                data: $.json.encode(data),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'PackageSearchResultV2.aspx/setBasket',
                success: function (msg) {
                    $("#basketTableDiv").html('');
                    $("#basketTableDiv").show();
                    if (msg.d != null && msg.d.basketListOK != false) {
                        $("#basketTableDiv").html(msg.d.basketDiv);
                        reSizeFrame();
                        if (msg.d.message != '')
                            showDialog(msg.d.message);
                    }
                    else $("#basketTableDiv").hide();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function addBasket(CatPackID, ARecNo, PRecNo, HapRecId, stopSaleMsg) {
            if (stopSaleMsg != '') {
                $(function () {
                    var msg = '<%= GetGlobalResourceObject("PackageSearchResult", "StopSaleWarningMessage") %>';
                    $("#messages").html(msg.replace('{0}', unescape(stopSaleMsg)));
                    $("#dialog").dialog("destroy");
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                                $(this).dialog('close');
                                addBasketFunc(CatPackID, ARecNo, PRecNo, HapRecId)
                            },
                            '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>': function () {
                                $(this).dialog('close');
                            }
                        }
                    });
                });
            } else {
                addBasketFunc(CatPackID, ARecNo, PRecNo, HapRecId)
            }

        }

        function getBasket() {
            $.ajax({
                async: false,
                type: "POST",
                data: '{}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'PackageSearchResultV2.aspx/getBasket',
                success: function (msg) {
                    $("#basketTableDiv").html('');
                    $("#basketTableDiv").show();
                    if (msg.d != null && msg.d.basketListOK != false) {
                        $("#basketTableDiv").html(msg.d.basketDiv);
                        reSizeFrame();
                        if (msg.d.message != '')
                            showDialog(msg.d.message);
                    }
                    else $("#basketTableDiv").hide();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }
        var oTable = null;
        var oFltField = { OprText: '', Hotel: '', Category: '', Room: '', Location: '', CheckIn: '', Nights: '', Board: '', Accom: '', BeginPrice: '', EndPrice: '', Departure: '', Arrival: '' };

        function fnSelectedFilter(value, i) {
            //oTable.fnFilter(value, i);
            switch (i) {
                case 2: oFltField.OprText = value; break;
                case 3: oFltField.Hotel = value; break;
                case 31: oFltField.Category = value; break;
                case 32: oFltField.Room = value; break;
                case 5: oFltField.Location = value; break;
                case 6: oFltField.CheckIn = value; break;
                case 7: oFltField.Nights = value; break;
                case 8: oFltField.Board = value; break;
                case 9: oFltField.Accom = value; break;
                case 91: oFltField.BeginPrice = value; break;
                case 92: oFltField.EndPrice = value; break;
                case 12: oFltField.Departure = value; break;
                case 13: oFltField.Arrival = value; break;
            }
            oTable.fnDraw();
        }

        function fnCreateSelect(aData, selected, header, i, filterName) {
            if (aData == null) return '';
            var style = '';
            if (header.sWidth != null) {
                switch (filterName) {
                    case 'Hotel': style = 'style="width: 75%' + /*header.sWidth +*/';"'; break;
                    case 'Category': style = 'style="width: 24%' + /*header.sWidth +*/';"'; break;
                    default: style = 'style="width: 100%' + /*header.sWidth +*/';"'; break;
                }
            }

            var r = '<select ' + style + ' onchange="fnSelectedFilter(this.value, ' + i.toString() + ');" ><option value=""></option>', i, iLen = aData.length;

            for (i1 = 0; i1 < iLen; i1++) {
                if (aData[i1] == selected)
                    r += '<option value="' + aData[i1] + '" selected="selected">' + aData[i1] + '</option>';
                else r += '<option value="' + aData[i1] + '">' + aData[i1] + '</option>';
            }
            return r + '</select>';
        }

        function fnCreatePriceSelect(value1, value2) {
            var r = '';
            r += '<input type="text" value="' + (value1 != null ? value1 : "") + '" onblur="fnSelectedFilter(this.value, 91);" style="width: 100%; text-align:right;"/>' +
                 '<br />' +
                 '<input type="text" value="' + (value2 != null ? value2 : "") + '" onblur="fnSelectedFilter(this.value, 92);" style="width: 100%; text-align:right;"/>';
            return r;
        }

        function blockUIWait() {
            $('#mydiv').show();
        }

        function unBlockUIWait() {
            $('#mydiv').hide();
        }

        function showResult() {
            var _aoColumns = [];
            //  self.parent.startWait();
            $.ajax({
                async: false,
                type: "POST",
                data: '{}',
                timeout: (120 * 1000),
                cache: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'PackageSearchResultV2.aspx/getGridHeaders',
                success: function (msg) {
                    if (msg != null && msg.d != null) {
                        var data = msg.d.data;
                        $.each(data, function (i) {
                            obj = new Object();
                            if (this.sTitle != undefined && this.sTitle != null) obj.sTitle = this.sTitle;
                            if (this.sWidth != undefined && this.sWidth != null) obj.sWidth = this.sWidth;
                            if (this.bSortable != undefined && this.bSortable != null) obj.bSortable = this.bSortable;
                            if (this.sType != undefined && this.sType != null) obj.sType = this.sType;
                            if (this.sClass != undefined && this.sClass != null) obj.sClass = this.sClass;
                            if (this.bSearchable != undefined && this.bSearchable != null) obj.bSearchable = this.bSearchable;
                            _aoColumns.push(obj);
                        });

                        if (msg.d.searchNote != undefined && msg.d.searchNote != '') {
                            $("#searchNote").show();
                            $("#searchNote").html(msg.d.searchNote);
                        } else {
                            $("#searchNote").hide();
                        }

                        if (msg.d.newOffer) {
                            $("#clientOffer").html('');
                            $("#clientOffer").html(msg.d.newOfferHtml);
                        }
                    }

                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
            // self.parent.stopWait();

            var oLanguage = {
                "sProcessing": '<%= GetGlobalResourceObject("LibraryResource", "sProcessing") %>',
                "sLengthMenu": '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>',
                "sZeroRecords": '<%= GetGlobalResourceObject("LibraryResource", "sZeroRecords") %>',
                "sEmptyTable": '<%= GetGlobalResourceObject("LibraryResource", "sEmptyTable") %>',
                "sInfo": '<%= GetGlobalResourceObject("LibraryResource", "sInfo") %>',
                "sInfoEmpty": '<%= GetGlobalResourceObject("LibraryResource", "sInfoEmpty") %>',
                "sInfoFiltered": '<%= GetGlobalResourceObject("LibraryResource", "sInfoFiltered") %>',
                "sInfoPostFix": '<%= GetGlobalResourceObject("LibraryResource", "sInfoPostFix") %>',
                "sSearch": '<%= GetGlobalResourceObject("LibraryResource", "sSearch") %>',
                "sUrl": '<%= GetGlobalResourceObject("LibraryResource", "sUrl") %>',
                "oPaginate": {
                    "sFirst": '<%= GetGlobalResourceObject("LibraryResource", "sFirst") %>',
                    "sPrevious": '<%= GetGlobalResourceObject("LibraryResource", "sPrevious") %>',
                    "sNext": '<%= GetGlobalResourceObject("LibraryResource", "sNext") %>',
                    "sLast": '<%= GetGlobalResourceObject("LibraryResource", "sLast") %>',
                    "sPage": '<%= GetGlobalResourceObject("LibraryResource", "sPage") %>'
                },
                "fnInfoCallback": null
            };
            var first = true;

            oTable = $('#resTable').dataTable({
                "bDestory": false,
                "bRetrieve": true,
                "bStateSave": true,
                "aaSorting": [],
                "fnStateLoadParams": function (oSettings, oData) {
                    if (first) {
                        oData.iStart = 0;
                        oSettings._iDisplayStart = 0;
                        if (oData.oFilter != null && oData.oFilter != undefined && oData.oFilter.sSearch != null && oData.oFilter.sSearch != undefined)
                            oData.oFilter.sSearch = "";
                    }
                    first = false;
                },
                "sPaginationType": "listbox",
                "bFilter": true,
                "bJQueryUI": true,
                "oLanguage": oLanguage,
                "aoColumns": _aoColumns,
                "bProcessing": false,
                "bServerSide": true,
                "sServerMethod": "POST",
                "sAjaxSource": "PackageSearchV2Data.aspx",
                "fnDrawCallback": function (oSettings) {
                    for (var i = 0, iLen = oSettings.aoData.length; i < iLen; i++) {
                        var status = oSettings.aoData[i]._aData[14];
                        var colorCSS = '';
                        if (status == '2') {
                            colorCSS = 'stopSale';
                        }
                        else if (status == '1') {
                            colorCSS = 'stopSaleOk';
                        }
                        oSettings.aoData[i].nTr.className += " " + colorCSS;
                        if (oSettings.aoData[i]._aData[15] != '' && oSettings.aoData[i]._aData[15] != undefined) {
                            $(oSettings.aoData[i].nTr).attr('title', oSettings.aoData[i]._aData[15]);
                        }
                    }
                },
                "fnServerData": function (sSource, aoData, fnCallback) {
                    var obj = new Object();
                    if (oFltField.OprText != '') { obj = new Object(); obj.name = 'OprText'; obj.value = oFltField.OprText; aoData.push(obj); }
                    if (oFltField.Hotel != '') { obj = new Object(); obj.name = 'Hotel'; obj.value = oFltField.Hotel; aoData.push(obj); }
                    if (oFltField.Category != '') { obj = new Object(); obj.name = 'Category'; obj.value = oFltField.Category; aoData.push(obj); }
                    if (oFltField.Room != '') { obj = new Object(); obj.name = 'Room'; obj.value = oFltField.Room; aoData.push(obj); }
                    if (oFltField.Location != '') { obj = new Object(); obj.name = 'Location'; obj.value = oFltField.Location; aoData.push(obj); }
                    if (oFltField.CheckIn != '') { obj = new Object(); obj.name = 'CheckIn'; obj.value = oFltField.CheckIn; aoData.push(obj); }
                    if (oFltField.Nights != '') { obj = new Object(); obj.name = 'Nights'; obj.value = oFltField.Nights; aoData.push(obj); }
                    if (oFltField.Board != '') { obj = new Object(); obj.name = 'Board'; obj.value = oFltField.Board; aoData.push(obj); }
                    if (oFltField.Accom != '') { obj = new Object(); obj.name = 'Accom'; obj.value = oFltField.Accom; aoData.push(obj); }
                    if (oFltField.BeginPrice != '') { obj = new Object(); obj.name = 'BeginPrice'; obj.value = oFltField.BeginPrice; aoData.push(obj); }
                    if (oFltField.EndPrice != '') { obj = new Object(); obj.name = 'EndPrice'; obj.value = oFltField.EndPrice; aoData.push(obj); }
                    if (oFltField.Departure != '') { obj = new Object(); obj.name = 'Departure'; obj.value = oFltField.Departure; aoData.push(obj); }
                    if (oFltField.Arrival != '') { obj = new Object(); obj.name = 'Arrival'; obj.value = oFltField.Arrival; aoData.push(obj); }
                    if (aoData[0].value == 1)
                        aoData[3].value = 0;

                    $.ajax({
                        "async": false,
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (data, textStatus, xmlHttpRequest) {
                            var jData = $(data);
                            var json = { "sEcho": aoData[0].value, "aaData": [] };
                            json.iTotalRecords = data.iTotalRecords;
                            json.iTotalDisplayRecords = data.iTotalDisplayRecords;
                            $.each(data.aaData, function (i) {
                                var obj = [];
                                if (this.Book != undefined) obj.push(this.Book);
                                if (this.AddOffer != undefined) obj.push(this.AddOffer);
                                if (this.OprText != undefined) obj.push(this.OprText);
                                if (this.Hotel != undefined) obj.push(this.Hotel);
                                if (this.FreeRoom != undefined) obj.push(this.FreeRoom);
                                if (this.Location != undefined) obj.push(this.Location);
                                if (this.CheckIn != undefined) obj.push(this.CheckIn);
                                if (this.Nights != undefined) obj.push(this.Nights);
                                if (this.Board != undefined) obj.push(this.Board);
                                if (this.Accom != undefined) obj.push(this.Accom);
                                if (this.Price != undefined) obj.push(this.Price);
                                if (this.Discount != undefined) obj.push(this.Discount);
                                if (this.Departure != undefined) obj.push(this.Departure);
                                if (this.Return != undefined) obj.push(this.Return);
                                if (this.StopSale != undefined) obj.push(this.StopSale);
                                if (this.StopSaleMsg != undefined) obj.push(this.StopSaleMsg);
                                json.aaData.push(obj);
                            });

                            fnCallback(json);

                            var footerHTML = '';
                            $("thead th").each(function (i) {
                                switch (i) {
                                    case 0: footerHTML += '<th>' + '&nbsp;' + '</th>'; break;
                                    case 1: footerHTML += '<th>' + '&nbsp;' + '</th>'; break;
                                    case 2: footerHTML += '<th>' + fnCreateSelect(data.filterData.fltOprText, data.filterData.fltOprTextSelected, _aoColumns[i], i, 'OprText') + '</th>'; break;
                                    case 3: footerHTML += '<th>' +
                                                          fnCreateSelect(data.filterData.fltHotel, data.filterData.fltHotelSelected, _aoColumns[i], i, 'Hotel') +
                                                          fnCreateSelect(data.filterData.fltCategory, data.filterData.fltCategorySelected, _aoColumns[i], 31, 'Category') + '<br />' +
                                                          fnCreateSelect(data.filterData.fltRoom, data.filterData.fltRoomSelected, _aoColumns[i], 32, 'Room') +
                                                          '</th>';
                                        break;
                                    case 4: footerHTML += '<th>' + '&nbsp;' + '</th>'; break;
                                    case 5: footerHTML += '<th>' + fnCreateSelect(data.filterData.fltLocation, data.filterData.fltLocationSelected, _aoColumns[i], i, 'Location') + '</th>'; break;
                                    case 6: footerHTML += '<th>' + fnCreateSelect(data.filterData.fltCheckIn, data.filterData.fltCheckInSelected, _aoColumns[i], i, 'CheckIn') + '</th>'; break;
                                    case 7: footerHTML += '<th>' + fnCreateSelect(data.filterData.fltNights, data.filterData.fltNightsSelected, _aoColumns[i], i, 'Night') + '</th>'; break;
                                    case 8: footerHTML += '<th>' + fnCreateSelect(data.filterData.fltBoard, data.filterData.fltBoardSelected, _aoColumns[i], i, 'Board') + '</th>'; break;
                                    case 9: footerHTML += '<th>' + fnCreateSelect(data.filterData.fltAccom, data.filterData.fltAccomSelected, _aoColumns[i], i, 'Accom') + '</th>'; break;
                                    case 10: footerHTML += '<th>' + fnCreatePriceSelect(data.filterData.fltPriceBegin, data.filterData.fltPriceEnd) + '</th>'; break;
                                    case 11: footerHTML += '<th>' + '&nbsp;' + '</th>'; break;
                                    case 12: footerHTML += '<th>' + fnCreateSelect(data.filterData.fltDeparture, data.filterData.fltDepartureSelected, _aoColumns[i], i, 'Departure') + '</th>'; break;
                                    case 13: footerHTML += '<th>' + fnCreateSelect(data.filterData.fltReturn, data.filterData.fltReturnSelected, _aoColumns[i], i, 'Return') + '</th>'; break;
                                }
                            });
                            $("#resTable tbody tr:first").before('<tr>' + footerHTML + '</tr>');

                            reSizeFrame();
                            $('.roomConsept').cluetip({ activation: 'click', width: 400, arrows: true, dropShadow: false, hoverIntent: false, sticky: true, mouseOutClose: false, closePosition: 'title', closeText: '<img src="Images/cancel.png" title="close" />' });
                            $('.boardConsept').cluetip({ activation: 'click', width: 400, arrows: true, dropShadow: false, hoverIntent: false, sticky: true, mouseOutClose: false, closePosition: 'title', closeText: '<img src="Images/cancel.png" title="close" />' });
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                showDialog(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                    $("#resTable_filter").hide();

                }
            });
            getBasket();
        }

        $(document).ready(function () {

            $.ajaxSetup({
                beforeSend: function () {
                    self.parent.startWait();
                },
                complete: function () {
                    self.parent.stopWait();
                }
            });

            $("#resultTableDiv").remove("#resTable");
            var tableElm = $(document.createElement('table'));
            tableElm.attr({ 'id': 'resTable', 'class': 'display' }).css({ 'width': '970px' });
            $("#resultTableDiv").append(tableElm);
            //self.parent.startWait();
            showResult();
            //self.parent.stopWait();
        });
    </script>

</head>
<body>
    <form id="formPackageSearchResultV2" runat="server">
        <div id="gridResult" style="width: 970px;">
            <div id="clientOffer" class="offer">
                <input id="btnCreateOffer" type="button" value="<%=GetGlobalResourceObject("PackageSearchResultV2","btnCreateOffer") %>"
                    class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
                    onclick="createOffer();" />&nbsp;&nbsp;
                <input id="btnClearOffer" type="button" value="<%=GetGlobalResourceObject("PackageSearchResultV2","btnClearOffer") %>"
                    class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
                    onclick="clearOffer();" />
            </div>
            <div id="basketTableDiv">
            </div>
            <div id="searchNote" style="display: none;"></div>
            <div id="resultTableDiv">
            </div>
        </div>
        <div id="dialog-message" title="" style="display: none;">
            <span id="messages">Message</span>
        </div>
        <div id="dialog-CreateReservation" title=''
            style="display: none; text-align: center;">
            <h3>
                <%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h3>
        </div>
    </form>
</body>
</html>
