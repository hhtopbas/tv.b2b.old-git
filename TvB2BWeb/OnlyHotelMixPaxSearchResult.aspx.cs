﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Text;
using System.Web.Services;
using System.Threading;
using System.Globalization;
using TvBo;
using TvTools;
using System.Collections;
using System.Web.Script.Services;

public partial class OnlyHotelMixPaxSearchResult : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        if (!Page.IsPostBack) {
            HttpContext.Current.Session["MixSearchResultPaximum"] = null;
        }
        string _tmpPath = WebRoot.BasePageRoot + "Data/";
        tmplPath.Value = _tmpPath;
    }

    public static Hotel[] setResultFilter(SearchHotelsResponse searchResult, mixResultFilter filter)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int orderNo = 1;
        Hotel[] pagedHotel = (from q in searchResult.Hotels
                              where (string.IsNullOrEmpty(filter.Category) || Convert.ToDouble(filter.Category) == q.Stars) &&
                                    (string.IsNullOrEmpty(filter.Hotel) || filter.Hotel == q.Id) &&
                                    (string.IsNullOrEmpty(filter.PaxLocation) || filter.PaxLocation == q.City.id)
                              select new Hotel {
                                  City = q.City,
                                  CityName = q.CityName,
                                  Country = q.Country,
                                  Description = q.Description,
                                  Facilities = q.Facilities,
                                  Geolocation = q.Geolocation,
                                  Id = q.Id,
                                  IsFavorite = q.IsFavorite,
                                  Name = q.Name,
                                  Offers = whereHotelOffers(UserData, q.Offers, filter.Board, filter.Room),
                                  OrderNo = orderNo++, //q.OrderNo,
                                  Rating = q.Rating,
                                  RatingStr = q.RatingStr,
                                  ReviewUrl = q.ReviewUrl,
                                  SelectedPrice = q.SelectedPrice,
                                  Stars = q.Stars,
                                  StarStr = q.StarStr,
                                  Themes = q.Themes,
                                  Thumbnail = q.Thumbnail
                              }).ToArray();

        return pagedHotel.Where(w => w.Offers.Count() > 0).ToArray();
    }

    public static HotelOffer[] whereHotelOffers(User UserData, HotelOffer[] offers, string board, string room)
    {
        List<HotelOffer> newOffers = new List<HotelOffer>();
        newOffers = (from q in offers
                     where (string.IsNullOrEmpty(board) || q.Board == board) &&
                           (string.IsNullOrEmpty(room) || q.RoomName == room)
                     select q).ToList<HotelOffer>();
        return newOffers.ToArray();
    }

    public static string createResultFilter(User UserData, SearchHotelsResponse result, mixResultFilter filteredData)
    {
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        List<resultFilterMix> filterList = new List<resultFilterMix>();
        if (result.FilterResult == null)
            result.FilterResult = new mixResultFilter();
        mixResultFilter filter = result.FilterResult;
        List<Hotel> filterData = new List<Hotel>();

        filterData = result.Hotels.ToList();
        var PaxLocation = from q in filterData
                          group q by new { CityID = q.City.id } into k
                          select new {
                              Code = k.Key.CityID,
                              Name = filterData.Where(w => w.City.id == k.Key.CityID).FirstOrDefault().CityName
                          };
        if ((PaxLocation != null && PaxLocation.Count() > 1) || !string.IsNullOrEmpty(filteredData.PaxLocation)) {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in PaxLocation.OrderBy(o => o.Name))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, row.Name, string.Equals(filteredData.PaxLocation, row.Code) ? "selected=\"selected\"" : "");

            filterValue = string.Format("<div style=\"width:205px; float: left;\"><strong>{0}</strong><br /><select id=\"rfPaxLocation\" style=\"width:95%;\" onchange=\"filterResult();\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblArrCity"),
                            selectValue);
            filterList.Add(new resultFilterMix { ID = "rfPaxLocation", Type = "string", FieldName = "PaxLocation", Value = filterValue, Width = 205 });
        }

        var Category = from q in filterData
                       group q by new { q.Stars } into k
                       select new { k.Key.Stars };
        if ((Category != null && Category.Count() > 1) || !string.IsNullOrEmpty(filteredData.Category)) {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Category.OrderBy(o => o.Stars))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Stars, row.Stars, filteredData.Category == row.Stars.ToString() ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:205px; float: left;\"><strong>{0}</strong><br /><select id=\"rfCategory\" style=\"width:97%;\" onchange=\"filterResult('HotCat', this.value);\">{1}</select></div>",
                                        HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblCategory"),
                                        selectValue);
            filterList.Add(new resultFilterMix { ID = "rfCategory", Type = "string", FieldName = "HotCat", Value = filterValue, Width = 205 });
        }

        var Hotel = from q in filterData
                    group q by new { q.Id } into k
                    select new {
                        Code = k.Key.Id,
                        Name = filterData.Where(w => w.Id == k.Key.Id).FirstOrDefault().Name
                    };
        if ((Hotel != null && Hotel.Count() > 1) || !string.IsNullOrEmpty(filteredData.Hotel)) {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Hotel.OrderBy(o => o.Name))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, row.Name, filteredData.Hotel == row.Code ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:305px; float: left;\"><strong>{0}</strong><br /><select id=\"rfHotel\" style=\"width:97%;\" onchange=\"filterResult('Hotel', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblHotel"),
                            selectValue);
            filterList.Add(new resultFilterMix { ID = "rfHotel", Type = "string", FieldName = "Hotel", Value = filterValue, Width = 305 });
        }
        List<string> mixRoomList = new List<string>();
        List<string> mixBoardList = new List<string>();
        foreach (Hotel rowH in filterData) {
            foreach (HotelOffer rowO in rowH.Offers) {
                mixRoomList.Add(rowO.RoomName);
                mixBoardList.Add(rowO.Board);
            }
        }
        //var q11 = filterData.First().Offers.First().Rooms.First();
        
        var Room = from q in mixRoomList
                   group q by new { q }  into k
                   select new {
                       Code = k.Key.q,
                       Name = k.Key.q
                   };
        if ((Room != null && Room.Count() > 1) || !string.IsNullOrEmpty(filteredData.Room)) {
            StringBuilder sbSelectValue = new StringBuilder();
            string filterValue = string.Empty;
            //string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            sbSelectValue.Append(string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll")));
            foreach (var row in Room.OrderBy(o => o.Name))
                //selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, row.Name, "");
                sbSelectValue.Append(string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, row.Name, filteredData.Room == row.Name ? "selected=\"selected\"" : ""));
                filterValue = string.Format("<div style=\"width:205px; float: left;\"><strong>{0}</strong><br /><select id=\"rfRoom\" style=\"width:97%;\" onchange=\"filterResult('Room', this.value);\">{1}</select></div>",
                                HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblRoom"),
                                sbSelectValue.ToString());
            filterList.Add(new resultFilterMix { ID = "rfRoom", Type = "string", FieldName = "Room", Value = filterValue, Width = 205 });
        }

        var Board = from q in mixBoardList
                    group q by new { q } into k
                    select new {
                        Code = k.Key.q,
                        Name = k.Key.q
                    };
        if ((Board != null && Board.Count() > 1) || !string.IsNullOrEmpty(filteredData.Board)) {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Board.OrderBy(o => o.Name))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, row.Name, filteredData.Board == row.Code ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:205px; float: left;\"><strong>{0}</strong><br /><select id=\"rfBoard\" style=\"width:97%;\" onchange=\"filterResult('Board', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblBoard"),
                            selectValue);
            filterList.Add(new resultFilterMix { ID = "rfBoard", Type = "string", FieldName = "Board", Value = filterValue, Width = 205 });
        }

        StringBuilder sb = new StringBuilder();
        int len = 0;
        string divRow = string.Empty;
        string divRowOuther = "<div class=\"clearfix\">";
        foreach (resultFilterMix row in filterList) {
            len += row.Width.Value;
            if (len > 728) {
                len = 0;
                len += row.Width.Value;
                sb.Append(divRowOuther);
                sb.Append(divRow);
                sb.Append("</div>");
                divRow = string.Empty;
            }
            divRow += row.Value;
        }

        sb.Append(divRowOuther);
        sb.Append(divRow);
        sb.Append("</div>");

        sb.Insert(0, "<div style=\"width:730px;\">");
        sb.Append("</div>");
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static object getResultGrid(int page, int? pageItemCount, bool? filtered, bool? paged, string sorting, List<mixFilterSelectedList> filterData)
    {
        //var sw = new System.Diagnostics.Stopwatch();
        //sw.Start();
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        if (HttpContext.Current.Session["MixCriteria"] == null) return null;
        MixSearchCriteria criteria = (MixSearchCriteria)HttpContext.Current.Session["MixCriteria"];
        SearchHotelsResponse priceList = new SearchHotelsResponse();
        SearchHotelsResponse pagedPriceList = new SearchHotelsResponse();

        string sortValue = "SelectedPrice.Amount";
        string sortDirection = "down";
        if (!string.IsNullOrEmpty(sorting) && sorting.Split('_').Length > 1) {
            sortValue = sorting.Split('_')[0];
            sortDirection = sorting.Split('_')[1];
            switch (sortValue) {
                case "name":
                    sortValue = "Name";
                    break;
                case "price":
                    sortValue = "SelectedPrice.Amount";
                    break;
                case "raiting":
                    sortValue = "Rating";
                    break;
                case "stars":
                    sortValue = "Stars";
                    break;
                default:
                    sortValue = "SelectedPrice.Amount";
                    break;
            }

        }

        mixResultFilter filter = new mixResultFilter();
        foreach (mixFilterSelectedList row in filterData) {
            switch (row.FieldName) {
                case "rfPaxLocation": filter.PaxLocation = Conversion.getStrOrNull(row.Value); break;
                case "rfCategory": filter.Category = Conversion.getStrOrNull(row.Value); break;
                case "rfHotel": filter.Hotel = Conversion.getStrOrNull(row.Value); break;
                case "rfRoom": filter.Room = Conversion.getStrOrNull(row.Value); break;
                case "rfBoard": filter.Board = Conversion.getStrOrNull(row.Value); break;
            }
        }
        filter.filtered = !string.IsNullOrEmpty(filter.PaxLocation) ||
                          !string.IsNullOrEmpty(filter.Board) ||
                          !string.IsNullOrEmpty(filter.Category) ||
                          !string.IsNullOrEmpty(filter.Hotel) ||
                          filter.Night.HasValue ||
                          !string.IsNullOrEmpty(filter.Room);
        //sw.Stop();
        //var saat1 = sw.ElapsedMilliseconds;
        //Console.WriteLine("Saat 1:" + saat1.ToString());


        int totalHotelCount = 0;
        pageItemCount = pageItemCount.HasValue ? pageItemCount.Value : 10;
        if (HttpContext.Current.Session["MixSearchResultPaximum"] == null) {
            //sw.Start();

            string pLocation = criteria.PaxLocation.FirstOrDefault();
            int orderNo = 0;
            int pNight = criteria.NightFrom;
            List<DestinationSearchCriteria> destination = new List<DestinationSearchCriteria>();
            destination.Add(new DestinationSearchCriteria { Id = pLocation, Type = "city" });
            if (!string.IsNullOrEmpty(criteria.Hotel)) {
                foreach (string row in criteria.Hotel.Split('|')) {
                    destination.Add(new DestinationSearchCriteria { Id = row, Type = "hotel" });
                }
            }

            //sw.Stop(); var saat2 = sw.ElapsedMilliseconds;
            //sw.Reset();
            //Console.WriteLine("Saat 2:" + saat2.ToString());
            //sw.Start();

            priceList = new MixSearchPaximum().searchHotel(UserData, destination.ToArray(), criteria.RoomInfoList, criteria.CheckIn, pNight, criteria.CurrentCurrency, criteria.PaxNationality, criteria.PaxAvailable);
            if (priceList == null || priceList.Hotels == null || priceList.Hotels.Count() == 0) {
                return null;
            }

            //sw.Stop(); var saat3 = sw.ElapsedMilliseconds; sw.Reset();
            //Console.WriteLine("Saat 3:" + saat3.ToString());
            //sw.Start();

            priceList.Requests.CheckOut = priceList.Requests.CheckoutDate.ToShortDateString();
            priceList.Requests.CheckIn = priceList.Requests.CheckinDate.ToShortDateString();
            priceList.Requests.Night = (priceList.Requests.CheckoutDate - priceList.Requests.CheckinDate).Days.ToString();
            foreach (Hotel row in priceList.Hotels) {
                ++orderNo;
                row.OrderNo = orderNo;
                row.SelectedPrice = row.Offers.Count() > 0 ? row.Offers.FirstOrDefault().Price : new Money();
                row.CityName = row.City.name;
                row.RatingStr = row.Rating > 0 ? row.Rating.ToString("#.00") : "0" + UserData.Ci.NumberFormat.CurrencyDecimalSeparator + "00";
                row.Description = row.Description.Replace("'", "&#39;").Replace(":", "&#58;").Replace("&", "&amp;");
                row.ShowDescription = !(row.Description.Replace(" ", "").Length > 0);
                row.StarStr = row.Stars.ToString().Replace(",", "").Replace(".", "");
                row.IsSpecial = !(row.Offers.Where(w => w.IsSpecial).Count() > 0);
                row.IsPriced = !(row.Offers.Count() > 0);
                if (string.IsNullOrEmpty(row.Thumbnail)) {
                    row.ReviewUrl = Global.getBasePageRoot() + "Images/";
                    row.Thumbnail = "noimage.png";
                } else {
                    row.ReviewUrl = "http://media.dev.paximum.com/hotelimages_125x125/";
                }
                bool selectedPrice = true;
                foreach (HotelOffer r1 in row.Offers) {
                    r1.Selected = selectedPrice;
                    r1.RoomName = r1.Rooms.FirstOrDefault().Type.ToString().Split(':').Count() > 1 ? r1.Rooms.FirstOrDefault().Type.ToString().Split(':')[1] : r1.Rooms.FirstOrDefault().Type.ToString();
                    int seqNo = 0;
                    foreach (Room rRoom in r1.Rooms) {
                        rRoom.Type = rRoom.Type.ToString().Split(':').Count() > 1 ? rRoom.Type.ToString().Split(':')[1] : rRoom.Type.ToString();
                        seqNo++;
                        rRoom.SeqNo = seqNo;
                    }
                    r1.AvailableMsg = r1.IsAvailable ? "Available" : " On Request";
                    r1.Price.AmountStr = r1.Price.Amount.ToString("#,###.00");
                    selectedPrice = false;
                }
                if (row.Offers.Count() > 0)
                    row.Offers.OrderBy(o => o.Price.Amount).FirstOrDefault().Selected = true;
            }
            HttpContext.Current.Session["MixSearchResultPaximum"] = priceList;
            priceList = (SearchHotelsResponse)Common.DeepClone(HttpContext.Current.Session["MixSearchResultPaximum"]);
            Hotel[] pagedHotel = priceList.Hotels;

            pagedPriceList = priceList;
            pagedPriceList.Hotels = pagedHotel;

            var tmp = sortValue.IndexOf("Price") > -1 ?
                (string.Equals(sortDirection, "down") ? pagedHotel.AsQueryable().Where(w => w.Offers.Count() > 0).OrderBy(sortValue) : pagedHotel.AsQueryable().Where(w => w.Offers.Count() > 0).OrderByDescending(sortValue)).Concat(pagedHotel.AsQueryable().Where(w => w.Offers.Count() == 0))
                :
                string.Equals(sortDirection, "down") ? pagedHotel.AsQueryable().OrderBy(sortValue) : pagedHotel.AsQueryable().OrderByDescending(sortValue);
            orderNo = -1;
            foreach (Hotel row in tmp) {
                orderNo++;
                row.OrderNo = orderNo;
            }
            totalHotelCount = tmp.Count();

            pagedPriceList.FilterHotels = tmp.Where(w => w.OrderNo >= ((page) * pageItemCount - pageItemCount) && w.OrderNo < (page) * pageItemCount).ToArray();
            pagedPriceList.Sort = sorting;
            HttpContext.Current.Session["MixSearchResultPaximum"] = pagedPriceList;

            //sw.Stop(); var saat4 = sw.ElapsedMilliseconds;
            //sw.Reset();
            //Console.WriteLine("Saat 4:" + saat4.ToString());

        } else {
            priceList = (SearchHotelsResponse)Common.DeepClone(HttpContext.Current.Session["MixSearchResultPaximum"]);

            Hotel[] pagedHotel = setResultFilter(priceList, filter);
            pagedPriceList.FilterResult = filter;
            pagedPriceList = priceList;

            var tmp = sortValue.IndexOf("Price") > -1 ?
                (string.Equals(sortDirection, "down") ? pagedHotel.AsQueryable().Where(w => w.Offers.Count() > 0).OrderBy(sortValue) : pagedHotel.AsQueryable().Where(w => w.Offers.Count() > 0).OrderByDescending(sortValue)).Concat(pagedHotel.AsQueryable().Where(w => w.Offers.Count() == 0))
                :
                string.Equals(sortDirection, "down") ? pagedHotel.AsQueryable().OrderBy(sortValue) : pagedHotel.AsQueryable().OrderByDescending(sortValue);
            int orderNo = -1;
            foreach (Hotel row in tmp) {
                orderNo++;
                row.OrderNo = orderNo;
            }

            totalHotelCount = tmp.Count();

            foreach (Hotel row in tmp) {
                row.SelectedPrice = row.Offers.Count() > 0 ? row.Offers.FirstOrDefault().Price : new Money();

                bool selectedPrice = true;
                foreach (HotelOffer r1 in row.Offers) {
                    r1.Selected = selectedPrice;
                    selectedPrice = false;
                }
                if (row.Offers.Count() > 0)
                    row.Offers.OrderBy(o => o.Price.Amount).FirstOrDefault().Selected = true;
            }

            pagedPriceList.FilterHotels = tmp.Where(w => w.OrderNo >= ((page) * pageItemCount - pageItemCount) && w.OrderNo < (page) * pageItemCount).ToArray();
            pagedPriceList.Sort = sorting;
            HttpContext.Current.Session["MixSearchResultPaximum"] = priceList;
        }
        //sw.Start();

        SearchHotelsResponse priceListTmp = (SearchHotelsResponse)HttpContext.Current.Session["MixSearchResultPaximum"];
        string filterDetails = createResultFilter(UserData, priceListTmp, filter);

        //sw.Stop(); var saat5 = sw.ElapsedMilliseconds; sw.Reset();
        //Console.WriteLine("Saat 5:" + saat5.ToString());
        //sw.Start();

        var result = new {
            ResultFilter = true,
            SearchResultFilter = pagedPriceList,
            ResultOffer = true,
            HotelGroup = 1,
            FilterDetails = filterDetails,
            CurrentCultureNumber = UserData.Ci.NumberFormat,
            totalResultCount = totalHotelCount,
            PageItemCount = pageItemCount,
            CurrentPage = page,
            lblTotalResult = string.Format(HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblTotalResultCount").ToString(), totalHotelCount),
            lblSortBy = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblSortBy"),
            lblSortByNameF = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblSortByNameF"),
            lblSortByNameL = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblSortByNameL"),
            lblSortByPriceF = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblSortByPriceF"),
            lblSortByPriceL = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblSortByPriceL"),
            lblSortByRaitingF = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblSortByRaitingF"),
            lblSortByRaitingL = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblSortByRaitingL"),
            lblSortByStarsF = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblSortByStarsF"),
            lblSortByStarsL = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblSortByStarsL"),
            lblReadMore = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblReadMore"),
            lblNights = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblNights"),
            lblBook = HttpContext.GetGlobalResourceObject("PackageSearchResult", "book"),
            lblSelectRoom = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblSelectRoom"),
            lblRoomNr = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblRoomNr"),
            lblRoomType = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblRoomType"),
            lblBoard = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblBoard"),
            lblStatus = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblStatus"),
            lblPrice = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblPrice"),
            lblContactPersonInformation = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblContactPersonInformation")
        };

        //sw.Stop(); var saat6 = sw.ElapsedMilliseconds;
        //sw.Reset();
        //Console.WriteLine("Saat 6:" + saat6.ToString());
        return result;
    }

    [WebMethod(EnableSession = true)]
    public static string getAllotmentControl(string RefNoList)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<SearchResultOH> searchResult = (List<SearchResultOH>)HttpContext.Current.Session["SearchResultOH"];
        List<SearchResultOH> _bookList = new List<SearchResultOH>();
        SearchCriteria _criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];

        for (int i = 0; i < RefNoList.Split(',').Length; i++) {
            int refNo = Convert.ToInt32(RefNoList.Split(',')[i]);
            SearchResultOH bookRow = searchResult.Find(f => f.RefNo == refNo);
            _bookList.Add(bookRow);
        }

        string errorMsg = string.Empty;

        if (new Reservation().priceSearchOHAllotControl(UserData, _bookList, ref errorMsg))
            return string.Empty;
        else return errorMsg;
    }

    [WebMethod(EnableSession = true)]
    public static string addOffer(string BookList)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<SearchResultOH> searchResult = (List<SearchResultOH>)HttpContext.Current.Session["SearchResultOH"];
        List<SearchResultOH> _bookList = new List<SearchResultOH>();
        for (int i = 0; i < BookList.Split(',').Length; i++) {
            int refNo = Convert.ToInt32(BookList.Split(',')[i]);
            SearchResultOH bookRow = searchResult.Find(f => f.PriceID == refNo);

            _bookList.Add(bookRow);
        }
        List<SearchResultOH> offerList = new List<SearchResultOH>();
        if (HttpContext.Current.Session["OfferList"] != null)
            offerList = (List<SearchResultOH>)HttpContext.Current.Session["OfferList"];
        foreach (SearchResultOH row in _bookList)
            offerList.Add(row);
        HttpContext.Current.Session["OfferList"] = offerList;
        return "";
    }
}