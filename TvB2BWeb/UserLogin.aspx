﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserLogin.aspx.cs" Inherits="UserLogin"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><%= GetGlobalResourceObject("PageTitle", "Default") %></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="Scripts/jquery.url.js" type="text/javascript"></script>

    <script type="text/javascript">

        function gotoStandartLogin() {
            var url = $.url(window.location.href);
            $.ajax({
                async: false,
                type: "POST",
                url: "UserLogin.aspx/getFormData",
                data: '{"queryString":"' + url.data.attr.query + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    window.open(msg.d, "_self", "toolbar=no,location=no,scrollbars=yes");
                },
                error: function (XMLHttpRequest, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(XMLHttpRequest.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        $(document).ready(function () {
            var url = $.url(window.location.href);
            $.query = $.query.load(location.href);
            token = $.query.get('Token');
            if (token) {

                $.ajax({
                    async: false,
                    type: "POST",
                    url: "UserLogin.aspx/extLogin",
                    data: '{"token":"' + token + '","queryString":"' + url.data.attr.query + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != null && msg.d != '') {
                            window.location.href = msg.d;
                        }
                        else {
                            gotoStandartLogin();
                        }
                    },
                    error: function (xhr, msg, e) {
                        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                            alert(xhr.responseText);
                    },
                    statusCode: {
                        408: function () {
                            logout();
                        }
                    }
                });
            } else {
                gotoStandartLogin();
            }
        });

    </script>

</head>
<body>
    <form id="UserLoginForm" runat="server">
    </form>
</body>
</html>
