﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MakeMixReservation.aspx.cs"
    Inherits="MakeMixReservation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "MakeReservation") %></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script src="Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>
    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="Scripts/mustache.js" type="text/javascript"></script>
    <script src="Scripts/checkDate.js" type="text/javascript"></script>
    <script src="Scripts/linq.js" type="text/javascript"></script>
    <script src="Scripts/jquery.linq.js" type="text/javascript"></script>
    <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="CSS/MakeMixReservation.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script type="text/javascript">

        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(email)) {
                return false;
            } else {
                return true;
            }
        }

        var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
        var InvalidBirthDate = '<%= GetGlobalResourceObject("LibraryResource", "InvalidBirthDate") %>';
        var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
        var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
        var btnGotoResCard = '<%= GetGlobalResourceObject("LibraryResource", "btnGotoResCard") %>';
        var btnPaymentPage = '<%= GetGlobalResourceObject("LibraryResource", "btnPaymentPage") %>';
        var btnYes = '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>';
        var btnNo = '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>';
        var WhereInvoiceTo = '<%= GetGlobalResourceObject("LibraryResource", "WhereInvoiceTo") %>';
        var lblAlreadyRes = '<%= GetGlobalResourceObject("BookTicket", "lblAlreadyRes") %>';
        var PickupPointNotSelected = '<%= GetGlobalResourceObject("MakeReservation", "PickupPointNotSelected") %>';
        var ComboSelect = '<%= GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
        var ForAgency = '<%= GetGlobalResourceObject("LibraryResource", "ForAgency") %>';
        var ForPassenger = '<%= GetGlobalResourceObject("LibraryResource", "ForPassenger") %>';
        var EnterChildOrInfantBirtDay = '<%= GetGlobalResourceObject("LibraryResource", "EnterChildOrInfantBirtDay") %>';
        var lblNoResult = '<%= GetGlobalResourceObject("BookTicket", "lblNoResult") %>';
        var viewOldResSalePrice = '<%= GetGlobalResourceObject("Controls", "viewOldResSalePrice") %>';
        var viewNewResSalePrice = '<%= GetGlobalResourceObject("Controls", "viewNewResSalePrice") %>';
        var cancelService = '<%= GetGlobalResourceObject("LibraryResource", "cancelService") %>';

        var reservationSaved = false;

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }


        function logout() {
            self.parent.logout();
        }


        $(document).ajaxStart(function () {
            $.blockUI({
                message: '<h1>' + lblPleaseWait + '</h1>'
            });
        }).ajaxStop(function () {
            $.unblockUI();
        });

        var _dateMask = '';
        var _dateFormat = '';

        function SetAge(Id, cinDate, _formatDate) {
            try {
                var birthDate = dateValue(_formatDate, $("#iBirthDay" + Id).val());
                if (birthDate == null) {
                    $("#iBirthDay" + Id).val('');
                    $("#iAge" + Id).val('');
                    if ($("#customRegID").val() == '0969801') {
                        $("#cCardBtn_" + Id).hide();
                    }
                    return;
                }
                var checkIN = dateValue(_formatDate, cinDate);
                if (checkIN == null) return;
                var _minBirthDate = new Date(1, 1, 1910);
                var _birthDate = birthDate;
                var _cinDate = checkIN;
                if (_birthDate < _minBirthDate) {
                    $("#iAge" + Id).val('');
                    birthDate = '';
                    if ($("#customRegID").val() == '0969801') {
                        $("#cCardBtn_" + Id).hide();
                    }
                    showMsg(InvalidBirthDate);
                    return;
                }
                var age = Age(_birthDate, _cinDate);
                $("#iAge" + Id).val(age);
                if ($("#customRegID").val() == '0969801') { //for Sunrise
                    var realAge = Age(_birthDate, new Date());
                    if (((parseInt($("#iTitle" + Id).val()) == 1 || parseInt($("#iTitle" + Id).val()) == 2) && parseInt(realAge) >= 20) ||
                        ((parseInt($("#iTitle" + Id).val()) == 3 || parseInt($("#iTitle" + Id).val()) == 4) && parseInt(realAge) >= 18))
                        $("#cCardBtn_" + Id).show();
                    else $("#cCardBtn_" + Id).hide();
                }
            }
            catch (err) {
                $("#iAge" + Id).val('');
                if ($("#customRegID").val() == '0969801') {
                    $("#cCardBtn_" + Id).hide();
                }
            }
        }

        function showDialogYesNo(msg) {
            $("#messages").html(msg);
            var _maxWidth = 880;
            var _maxHeight = 500;
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                maxWidth: _maxWidth,
                maxHeight: _maxHeight,
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                        return true;
                    }
                }, {
                    text: btnCancel,
                    click: function () {
                        $(this).dialog('close');
                        return false;
                    }
                }]
            });
        }

        function showMsg(msg) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                autoResize: true,
                resizable: true,
                resizeStop: function (event, ui) {
                    $(this).dialog({ position: 'center' });
                },
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                    }
                }]
            });
        }

        function showDialog(msg, transfer, trfUrl) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                maxWidth: 880,
                maxHeight: 500,
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                        if (transfer == true) {
                            var url = trfUrl;
                            $(location).attr('href', url);
                        }
                    }
                }]
            });
        }

        function showDialogEndGotoPaymentPage(msg, ResNo) {
            $('<div>' + msg + '</div>').dialog({
                buttons: [{
                    text: btnGotoResCard,
                    click: function () {
                        $(this).dialog('close');
                        window.close;
                        self.parent.gotoResViewPage(ResNo);
                    }
                }, {
                    text: btnPaymentPage,
                    click: function () {
                        $(this).dialog('close');
                        window.close;
                        self.parent.paymentPage(ResNo);
                    }
                }]
            });
        }


        function showDialogEndExit(msg, ResNo) {
            $("#messages").html('');
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                resizable: true,
                autoResize: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                        window.close;
                        if (ResNo != '')
                            self.parent.gotoResViewPage(ResNo, true);
                        else self.parent.cancelMakeRes();
                    }
                }]
            });
        }

        function showMessage(msg, transfer, trfUrl) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    maxWidth: 880,
                    maxHeight: 500,
                    modal: true,
                    buttons: [{
                        text: btnOK,
                        click: function () {
                            $(this).dialog('close');
                        }
                    }, {
                        text: btnCancel,
                        click: function () {
                            $(this).dialog('close');
                        }
                    }]
                });
            });
        }

        function editResService(serviceUrl) {
            setCustomers();
            $("#dialog-resServiceEdit").dialog("open");
            $("#resServiceEdit").attr("src", serviceUrl);
            return false;
        }

        function returnEditResServices(save) {
            if (save == true) {
                $("#dialog-resServiceEdit").dialog("close");
                getResMainDiv();
            }
            else {
                $("#dialog-resServiceEdit").dialog("close");
            }
        }

        function getCustomers() {
            var custCount = $("input[id^='iSeqNo_']").length;
            var custList = [];
            for (var i = 0; i < custCount; i++) {
                var custId = $("input[id^='iSeqNo_']")[i].id.split('_')[1];
                var cust = new Object();
                cust.SeqNo = $("#iSeqNo" + custId).text();
                cust.TravellerNo = custId;
                cust.Title = $("#iTitle" + custId).length > 0 ? $("#iTitle" + custId).val() : '';
                cust.Surname = $("#iSurname" + custId).length > 0 ? $("#iSurname" + custId).val() : '';
                cust.Name = $("#iName" + custId).length > 0 ? $("#iName" + custId).val() : '';
                cust.BirtDate = $("#iBirthDate" + custId).length > 0 ? $("#iBirthDate" + custId).val() : '';
                cust.Phone = $("#iPhone" + custId).length > 0 ? $("#iPhone" + custId).val() : '';
                cust.Nationality = $("#iNationality" + custId).length > 0 ? $("#iNationality" + custId).val() : '';
                cust.Leader = $('input[name=Leader]:checked').val() == custId;
                cust.Email = $("#iEmail" + custId).length > 0 ? $("#iEmail" + custId).val() : '';
                cust.Mobile = $("#iMobile" + custId).length > 0 ? $("#iMobile" + custId).val() : '';
                cust.isLead = $("#iLeader" + custId).length > 0 ? $("#iLeader" + custId).attr('checked') == 'checked' : false;
                custList.push(cust);
            }
            return $.json.encode(custList);
        }

        function checkUsers(custs) {
            var retVal = '';
            var CustList = $.json.decode(custs);
            try {
                var i = 0;
                for (i = 0; i < CustList.length; i++) {
                    if (CustList[i].Name == '' || CustList[i].Surname == '' || (CustList[i].Leader && (CustList[i].Email == '' || CustList[i].Mobile == ''))) {
                        var rowMsg = '';
                        if (retVal.length > 0) retVal += '<br />';
                        if (CustList[i].Name == '') {
                            if (rowMsg.length > 0) rowMsg += ', ';
                            rowMsg += 'Name';
                        }
                        if (CustList[i].Surname == '') {
                            if (rowMsg.length > 0) rowMsg += ', ';
                            rowMsg += 'Surname';
                        }
                        if (CustList[i].Leader && CustList[i].Email == '') {
                            if (rowMsg.length > 0) rowMsg += ', ';
                            rowMsg += 'E-Mail';
                        }
                        if (CustList[i].Leader && CustList[i].Mobile == '') {
                            if (rowMsg.length > 0) rowMsg += ', ';
                            rowMsg += 'Phone';
                        }
                        retVal += 'Customer ' + (i + 1) + ': Please enter ' + rowMsg;
                    }
                    else {
                        if (CustList[i].Leader && !IsEmail(CustList[i].Email)) {
                            return "Please type correct email address.";
                        }
                    }

                }
            }
            catch (e) {
                retVal = 'Please enter Name, Surname, Leader Phone, Leader Email';
            }
            return retVal;
        }

        function saveMixMakeRes() {
            var data = new Object();
            var customers = getCustomers();
            data.data = $.json.decode(customers);
            var userCheck = checkUsers(customers);
            if (userCheck != '') {
                showMsg(userCheck);
                return;
            }
            data.pResNote = $("#iNote").val();

            if (reservationSaved) {
                showMsg(lblAlreadyRes);
                return;
            } else {
                reservationSaved = true;
            }

            $.ajax({
                //async: false,
                type: "POST",
                url: "MakeMixReservation.aspx/saveReservation",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var retVal = $.json.decode(msg.d)[0];
                    if (retVal.ControlOK == true) {
                        if (retVal.GotoPaymentPage == true)
                            showDialogEndGotoPaymentPage(retVal.Message, retVal.ResNo);
                        else if (retVal.GotoReservation)
                            showDialogEndExit(retVal.Message, retVal.ResNo);
                        else showDialogEndExit(retVal.Message, '');
                    }
                    else {
                        showMsg(retVal.Message);
                        reservationSaved = false;
                        $("#goAhead").val('0');
                    }

                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                    reservationSaved = false;
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });

        }

        function setCustomers() {
            var data = new Object();
            data.data = $.json.decode(getCustomers());
            $.ajax({
                //async: false,
                type: "POST",
                url: "MakeReservation.aspx/setCustomers",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    return true;
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showMsg(xhr.responseText);
                    }
                    return false;
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function cancelMixMakeRes() {
            window.close;
            self.parent.cancelMakeRes();
        }

        function clickMsgAccept() {
            if ($('#msgAccept').attr('checked'))
                $("#btnSave").show();
            else $("#btnSave").hide();
        }

        function leaderChange(idNo) {
            $("div[id^='custInfo']").hide();
            $("#custInfo" + idNo).show();
        }

        function resNoteOptChange() {
            $("#iNote").val('');
            var valueHtml = '';
            $("input[type='checkbox'][name='resNoteOpt']").each(function () {
                var id = $(this).attr('id');
                var value = $(this).attr('value');
                if ($(this).attr('checked') == 'checked') {
                    valueHtml += (valueHtml.length > 0 ? ', ' : '') + value;
                }
            });
            $("#iNote").val(valueHtml);
        }

        function getReservation() {
            $.get($("#tmplPath").val() + 'OnlyHotelMixMakeReservation.html?v=201511021140', function (html) {
                getResMainDiv(html);
            }).fail(function (jqxhr, textStatus, error) {
                var err = textStatus + ', ' + error;
                showAlert("Request Failed: " + err);
            });
        }

        function getResMainDiv(tmpHtml) {
            $.ajax({
                //async: false,
                type: "POST",
                url: "MakeMixReservation.aspx/getReservation",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {

                        var data = msg.d;
                        if (data.ok == false) {
                            $("#messages").html(data.Message);
                            $("#dialog").dialog("destroy");
                            $("#dialog-message").dialog({
                                modal: true,
                                autoResize: true,
                                resizable: true,
                                buttons: [{
                                    text: btnOK,
                                    click: function () {
                                        window.close;
                                        self.parent.cancelMakeRes();
                                        $(this).dialog('close');
                                    }
                                }],
                                close: function (event, ui) {
                                    window.close;
                                    self.parent.cancelMakeRes();
                                    $(this).dialog('close');
                                }
                            });
                            return false;
                        }

                        var html = Mustache.render(tmpHtml, data);
                        $("#divReservation").html(html);
                        if (data.CancelRoles != '') {
                            $("#divCancelRoles").show();
                            $("#CancellationRoles").html(data.CancelRoles);
                            if (data.ShowCancelMessage == true) {
                                $(".cancellationPolicyAgree").show();
                            }
                            else {
                                $(".cancellationPolicyAgree").hide();
                                $("#msgAccept").attr("checked", "checked");
                            }
                        } else {
                            $("#divCancelRoles").hide();
                            $("#msgAccept").attr("checked", "checked");
                        }
                        leaderChange(data.Leader);
                        $.each(data.Rooms, function (i) {
                            $.each(this.ResCust, function (c) {
                                $("#iTitle" + this.SeqNo).val(this.Title);
                            });
                        });

                        $(".formatDate").mask(data.dateMask);
                        clickMsgAccept();
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }


        function searchSelectCust(CustNo) {
            $("#dialog-searchCust").dialog("close");
            var Custs = $.json.decode(getCustomers());
            var data = new Object();
            data.CustNo = CustNo;
            data.Custs = Custs;
            $.ajax({
                type: "POST",
                url: "MakeReservation.aspx/copyCustomer",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 'OK') {
                        getResMainDiv();
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function dialogInitialize() {
            $("#dialog-Ssrc").dialog({ autoOpen: false, modal: true, width: 640, height: 550, resizable: true, autoResize: true });
            $("#dialog-resServiceEdit").dialog({ autoOpen: false, modal: true, width: 675, height: 600, resizable: true, autoResize: true });
            $("#dialog-promoSelect").dialog({ autoOpen: false, modal: true, width: 820, height: 440, resizable: true, autoResize: true });
            $("#dialog-resCustOtherInfo").dialog({ autoOpen: false, modal: true, width: 440, height: 470, resizable: true, autoResize: true });
            $("#dialog-resCustomerAddress").dialog({ autoOpen: false, modal: true, width: 600, height: 535, resizable: false, autoResize: true });
            $("#dialog-resServiceAdd").dialog({ autoOpen: false, modal: true, width: 675, height: 600, resizable: true, autoResize: true });
            $("#dialog-resServiceExtAdd").dialog({ autoOpen: false, modal: true, width: 870, height: 525, resizable: true, autoResize: true });
        }

        function arrivalInformationElmChange() {
            if ($("#arrivalInformation").show()) {

                if (($("#airPort").val() != undefined && $("#airPort").val() != '' &&
                    $("#flightNo").val() != '' &&
                    $("#arrivalTimeHH").val() != '' &&
                    $("#arrivalTimeMM").val() != '') ||
                    $("#noTransfer").attr("checked")) {
                    $("#btnSave").show();
                } else {
                    $("#btnSave").hide();
                }

            }
        }

        $(document).ready(function () {
            getReservation();
            dialogInitialize();
        });

    </script>

</head>
<body>
    <form id="MakeReservationForm" runat="server">
        <asp:HiddenField ID="tmplPath" runat="server" />
        <input id="hfCustCount" type="hidden" value="" />

        <div style="width: 900px" class="ui-widget">
            <div id="divReservation" class="reservationDiv ui-helper-clearfix ui-widget">
            </div>
            <br />

            <div id="divCancelRoles" class="ui-helper-clearfix ui-widget">
                <div class="cancellationPolicyHeader ui-widget-header"><%= GetGlobalResourceObject("OnlyHotelMix", "lblCancellationPolicy") %></div>
                <div id="CancellationRoles" class="cancellationPolicy"></div>
                <div class="cancellationPolicyAgree">
                    <strong><%= GetGlobalResourceObject("OnlyHotelMix", "msgAttention") %></strong><br />
                    <input id="msgAccept" type="checkbox" onclick="clickMsgAccept()" /><label for="msgAccept"><%= GetGlobalResourceObject("OnlyHotelMix", "msgAccept") %></label>
                </div>
            </div>
            <br />
            <div style="text-align: center;">
                <div id="divNote" style="text-align: left;">
                    <strong>
                        <%= GetGlobalResourceObject("BookTicket", "saveDivNote") %>: </strong>
                    <br />
                    <input id="iNote" type="text" style="width: 95%; height: 30px;" maxlength="200" />
                    <span style="color: #9ed7d7; font-size: 10pt;"><%= GetGlobalResourceObject("OnlyHotelMix", "lblMaximum200characters") %></span>
                </div>
            </div>
            <div class="divButtons">
                <div style="width: 100%; height: 5px;">
                    &nbsp;
                </div>
                <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource","btnSaveReservation").ToString() %>'
                    onclick="saveMixMakeRes('');" style="min-width: 100px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover ui-helper-hidden" />
                &nbsp;&nbsp;
                <input id="btnCancel" type="button" value='<%= GetGlobalResourceObject("LibraryResource","btnCancel").ToString() %>'
                    onclick="cancelMixMakeRes();" style="min-width: 100px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
            </div>
            <br />
        </div>
        <div id="dialog-message" title="" style="display: none;">
            <span id="messages">Message</span>
        </div>
        <div id="dialog-resCustomerAddress" title='<%= GetGlobalResourceObject("MakeReservation", "lblCustomerAddress") %>'
            style="display: none;">
            <iframe id="resCustomerAddress" runat="server" height="490px" width="570px" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-resCustOtherInfo" title='<%= GetGlobalResourceObject("MakeReservation", "lblCustomerOtherInfo") %>'
            style="display: none;">
            <iframe id="resCustOtherInfo" runat="server" height="430px" width="410px" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-promoSelect" title='<%= GetGlobalResourceObject("MakeReservation", "lblSelectPromotion") %>'
            style="display: none;">
            <iframe id="promoSelect" runat="server" height="400px" width="795px" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-resServiceAdd" title='<%= GetGlobalResourceObject("MakeReservation", "lblService") %>'
            style="display: none;">
            <iframe id="resServiceAdd" runat="server" height="100%" width="100%" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-resServiceExtAdd" title='<%= GetGlobalResourceObject("MakeReservation", "lblExtService") %>'
            style="display: none;">
            <iframe id="resServiceExtAdd" runat="server" height="100%" width="835px" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-Ssrc" title='<%= GetGlobalResourceObject("MakeReservation", "lblSpecialServiceReqCode") %>'
            style="display: none;">
            <iframe id="Ssrc" runat="server" height="100%" width="100%" frameborder="0" style="clear: both;"></iframe>
        </div>
        <div id="dialog-searchCust" title='<%= GetGlobalResourceObject("MakeReservation", "searchCustLabel") %>'
            style="display: none;">
            <iframe id="searchCust" runat="server" height="100%" width="100%" frameborder="0"
                style="clear: both;"></iframe>
            <%--<div id="searchCust">
            </div--%>>
        </div>
    </form>
</body>
</html>
