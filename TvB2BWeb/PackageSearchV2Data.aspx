﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PackageSearchV2Data.aspx.cs"
    Inherits="PackageSearchV2Data" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="cache-control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>


    <script language="javascript" type="text/javascript">

        $(document).ajaxStart(function () {
            $.blockUI({
                message: '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>'
            });
        }).ajaxStop(function () {
            $.unblockUI();
            return false;
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
    </form>
</body>
</html>
