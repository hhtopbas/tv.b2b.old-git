﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StopSale.aspx.cs" Inherits="StopSale" %>

<%@ Register Src="Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "StopSale") %></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

    <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <script src="Scripts/jquery.textTruncate.js" type="text/javascript"></script>

    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

    <script src="Scripts/datatable/jquery.dataTables.min.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="CSS/StopSale.css" rel="stylesheet" type="text/css" />
    <link href="CSS/jquery.table.jui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        if (typeof window.event != 'undefined')
            document.onkeydown = function() {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function(e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function logout() {
            window.location = 'Default.aspx'; $('<div>' + '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>' + '</div>').dialog({
                autoOpen: true,
                position: {
                    my: 'center',
                    at: 'center'
                },
                modal: true,
                resizable: true,
                autoResize: true,
                bigframe: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                        $(this).dialog("close");
                        $(this).dialog("destroy");
                        window.location = 'Default.aspx';
                    }
                }
            });
        }

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        $(document).ready(function() {
            setOptions();
        });

        function stopSaleFlightSeatWithOpt() {
            $.ajax({
                type: "POST",
                data: '{}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'StopSale.aspx/GetFirstDataFlightSeatWithOpt',
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $("#dateFormat").val(data.DateFormat);
                        if (data.BegDate != null && data.BegDate != '') {
                            var _date1 = new Date(Date(eval(data.BegDate)).toString());
                            $("#fltDate1").val($.format.date(_date1.toString(), data.DateFormat));
                        }
                        else $("#fltDate2").val('');
                        if (data.EndDate != null && data.EndDate != '') {
                            var _date2 = new Date(Date(eval(data.EndDate)).toString());
                            $("#fltDate2").val($.format.date(_date2.toString(), data.DateFormat));
                        }
                        else $("#fltDate2").val('');
                        setDepCity(data);
                        setFlight(data);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function stopSaleStandartDefaults() {
            $.ajax({
                type: "POST",
                data: '{}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'StopSale.aspx/GetFirstDataStandart',
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $("#dateFormat").val(data.DateFormat);
                        if (data.BegDate != null && data.BegDate != '') {
                            var _date1 = new Date(Date(eval(data.BegDate)).toString());
                            $("#fltDate1").val($.format.date(_date1.toString(), data.DateFormat));
                        }
                        else $("#fltDate2").val('');
                        if (data.EndDate != null && data.EndDate != '') {
                            var _date2 = new Date(Date(eval(data.EndDate)).toString());
                            $("#fltDate2").val($.format.date(_date2.toString(), data.DateFormat));
                        }
                        else $("#fltDate2").val('');
                        setHotels(data);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function setDepCity(data) {
            $("#fltDepCity").html('');
            $("#fltDepCity").append("<option value=''>" + '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>' + "</option>");
            $.each(data.DepCity, function(i) {
                $("#fltDepCity").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
            });
        }

        function setArrCity(data) {
            $("#fltArrCity").html('');
            $("#fltArrCity").append("<option value=''>" + '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>' + "</option>");
            if (data == undefined || data == null)
                return;
            else {
                $.each(data, function(i) {
                    $("#fltArrCity").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
                });

            }
        }

        function setFlight(data) {
            $("#fltFlightCls").html('');
            $("#fltFlightCls").append("<option value=''>" + '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>' + "</option>");
            $.each(data.FlgCls, function(i) {
                $("#fltFlightCls").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
        }

        function setHotels(data) {
            $("#fltHotel").html('');
            $("#fltHotel").append("<option value=''>" + '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>' + "</option>");
            $.each(data.HotelList, function(i) {
                $("#fltHotel").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
        }

        function setArrCityHotels(data) {
            $("#fltHotel").html('');
            $("#fltHotel").append("<option value=''>" + '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>' + "</option>");
            $.each(data, function(i) {
                $("#fltHotel").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
        }

        function setOptions() {
            var value = $('input:radio[name=rbSelectListType]:checked').val();
            var oldValue = $("#hfOptionType").val();
            if (oldValue != value) {
                if (value == '0') {
                    $("#fltDepCity").html('');
                    $("#fltArrCity").html('');
                    $("#fltFlightCls").html('');
                    $("#fltHotel").html('');
                    $("#depCityDiv").hide();
                    $("#arrCityDiv").hide();
                    $("#flgClassDiv").hide();
                    $("#StopSaleGrid").html('');
                    stopSaleStandartDefaults();
                }
                else {
                    $("#depCityDiv").show();
                    $("#arrCityDiv").show();
                    $("#flgClassDiv").show();
                    $("#fltHotel").html('');
                    $("#StopSaleGrid").html('');
                    stopSaleFlightSeatWithOpt()
                }
            }
            $("#hfOptionType").val(value);
        }

        function ChangeDepCity() {
            var depCityID = $("#fltDepCity").val();
            $.ajax({
                type: "POST",
                data: '{"depCityID":' + depCityID + '}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'StopSale.aspx/GetArrCityByDepCity',
                success: function(msg) {
                    var data = $.json.decode(msg.d);
                    setArrCity(data);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function ChangeArrCity() {
            var arrCityID = $("#fltArrCity").val();
            $.ajax({
                type: "POST",
                data: '{"begDate":"' + $("#fltDate1").val() + '","endDate":"' + $("#fltDate2").val() + '","arrCityID":' + arrCityID + '}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'StopSale.aspx/GetArrCityHotel',
                success: function(msg) {
                    var data = $.json.decode(msg.d);
                    setArrCityHotels(data);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function standartStopSale() {
            var oLanguage = {
                "sProcessing": '<%= GetGlobalResourceObject("LibraryResource", "sProcessing") %>',
                "sLengthMenu": '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>',
                "sZeroRecords": '<%= GetGlobalResourceObject("LibraryResource", "sZeroRecords") %>',
                "sEmptyTable": '<%= GetGlobalResourceObject("LibraryResource", "sEmptyTable") %>',
                "sInfo": '<%= GetGlobalResourceObject("LibraryResource", "sInfo") %>',
                "sInfoEmpty": '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>',
                "sInfoEmpty": '<%= GetGlobalResourceObject("LibraryResource", "sInfoEmpty") %>',
                "sInfoFiltered": '<%= GetGlobalResourceObject("LibraryResource", "sInfoFiltered") %>',
                "sInfoPostFix": '<%= GetGlobalResourceObject("LibraryResource", "sInfoPostFix") %>',
                "sSearch": '<%= GetGlobalResourceObject("LibraryResource", "sSearch") %>',
                "sUrl": '<%= GetGlobalResourceObject("LibraryResource", "sUrl") %>',
                "oPaginate": {
                    "sFirst": '<%= GetGlobalResourceObject("LibraryResource", "sFirst") %>',
                    "sPrevious": '<%= GetGlobalResourceObject("LibraryResource", "sPrevious") %>',
                    "sNext": '<%= GetGlobalResourceObject("LibraryResource", "sNext") %>',
                    "sLast": '<%= GetGlobalResourceObject("LibraryResource", "sLast") %>'
                },
                "fnInfoCallback": null
            };

            var dataString = '{"begDate":"' + $("#fltDate1").val() + '","endDate":"' + $("#fltDate2").val() + '","Hotel":"' + $("#fltHotel").val() + '"}';
            $.ajax({
                type: "POST",
                data: dataString,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'StopSale.aspx/GetResult',
                success: function(msg) {
                    $("#StopSaleGrid").html('');
                    $("#StopSaleGrid").html(msg.d);
                    $('#stopSaleTable').dataTable({
                        "sScrollX": "1000px",
                        "sScrollXInner": "125%",
                        "bScrollCollapse": true,
                        "sPaginationType": "full_numbers",
                        "sScrollY": "600",
                        "bFilter": false,
                        "bJQueryUI": true,
                        "bStateSave": true,
                        "aaSorting": [],
                        "oLanguage": oLanguage
                    });
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function optionalStopSale() {
            var oLanguage = {
                "sProcessing": '<%= GetGlobalResourceObject("LibraryResource", "sProcessing") %>',
                "sLengthMenu": '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>',
                "sZeroRecords": '<%= GetGlobalResourceObject("LibraryResource", "sZeroRecords") %>',
                "sEmptyTable": '<%= GetGlobalResourceObject("LibraryResource", "sEmptyTable") %>',
                "sInfo": '<%= GetGlobalResourceObject("LibraryResource", "sInfo") %>',
                "sInfoEmpty": '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>',
                "sInfoEmpty": '<%= GetGlobalResourceObject("LibraryResource", "sInfoEmpty") %>',
                "sInfoFiltered": '<%= GetGlobalResourceObject("LibraryResource", "sInfoFiltered") %>',
                "sInfoPostFix": '<%= GetGlobalResourceObject("LibraryResource", "sInfoPostFix") %>',
                "sSearch": '<%= GetGlobalResourceObject("LibraryResource", "sSearch") %>',
                "sUrl": '<%= GetGlobalResourceObject("LibraryResource", "sUrl") %>',
                "oPaginate": {
                    "sFirst": '<%= GetGlobalResourceObject("LibraryResource", "sFirst") %>',
                    "sPrevious": '<%= GetGlobalResourceObject("LibraryResource", "sPrevious") %>',
                    "sNext": '<%= GetGlobalResourceObject("LibraryResource", "sNext") %>',
                    "sLast": '<%= GetGlobalResourceObject("LibraryResource", "sLast") %>'
                },
                "fnInfoCallback": null
            };
            var dataString = '{"begDate":"' + $("#fltDate1").val() + '","endDate":"' + $("#fltDate2").val() + '","Hotel":"' + $("#fltHotel").val() + '","depCity":' + $("#fltDepCity").val() + ',"arrCity":' + $("#fltArrCity").val() + ',"flgClass":"' + $("#fltFlightCls").val() + '"}';
            $.ajax({
                type: "POST",
                data: dataString,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'StopSale.aspx/getFlightSeatWithOptResult',
                success: function(msg) {
                    $("#StopSaleGrid").html('');
                    $("#StopSaleGrid").html(msg.d);
                    if (msg.d != '')
                        $("#stopSaleTable").dataTable({
                            "sScrollX": "1000px",
                            "sScrollXInner": "150%",
                            "bScrollCollapse": true,
                            "sPaginationType": "full_numbers",
                            "sScrollY": "600px",
                            "bInfo": true,
                            "bFilter": false,
                            "bSort": false,
                            "bJQueryUI": true,
                            "bStateSave": true,
                            "aaSorting": [],
                            "bAutoWidth": false
                        });
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function btnFilter_onclick() {
            var value = $('input:radio[name=rbSelectListType]:checked').val();
            if (value == '0') {
                standartStopSale();
            }
            else if (value == '1') {
                optionalStopSale();
            }
        }

        function btnUnFilter_onclick() {
            window.location = "StopSale.aspx";
        }

        function DateChanged(_TextDay1, _PopCal) {
            if (_TextDay1.value == '') return
            var _TextDay2 = document.getElementById('<%= fltDate2.ClientID %>');
            if ((!_TextDay1) || (!_PopCal)) return
            if (_TextDay2.value != '') {
                var _format = _TextDay1.getAttribute("Format");
                var _Day1 = _PopCal.getDate(_TextDay1.value, _format);
                var _Day2 = _PopCal.getDate(_TextDay2.value, _format);
                if (_Day1 > _Day2) {
                    showAlert('<%= GetGlobalResourceObject("LibraryResource", "LastDateSmall") %>');
                    _TextDay2.value = _TextDay1.value;
                }
            }
        }

        function DateChangedTo(_TextDay2, _PopCal) {
            if (_TextDay2.value == '') return
            var _TextDay1 = document.getElementById('<%= fltDate1.ClientID %>');
            if ((!_TextDay2) || (!_PopCal)) return
            if (_TextDay1.value != '') {
                var _format = _TextDay2.getAttribute("Format");
                var _Day2 = _PopCal.getDate(_TextDay2.value, _format);
                var _Day1 = _PopCal.getDate(_TextDay1.value, _format);
                if (_Day1 > _Day2) {
                    showAlert('<%= GetGlobalResourceObject("LibraryResource", "LastDateSmall") %>');
                    _TextDay2.value = _TextDay1.value;
                }
            }
        }

        function showAlert(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }                        
    </script>
</head>
<body>
    <form id="StopSaleForm" runat="server">
    <div class="Page">
        <input id="dateFormat" type="hidden" value='' />
        <tv1:Header ID="tvHeader" runat="server" />
        <tv1:MainMenu ID="tvMenu" runat="server" />
        <div class="divFilter">
            <div class="ssInputBlockDiv">
                <div class="labelDiv">
                    <%= GetGlobalResourceObject("StopSale", "lblFirstDate")%>
                    :
                </div>
                <div class="ssInputDiv">
                    <asp:TextBox ID="fltDate1" runat="server" Width="110px" Text="" ReadOnly="True" />
                    <rjs:PopCalendar ID="ppcDate1" runat="server" Control="fltDate1" ClientScriptOnDateChanged="DateChanged" />
                </div>
            </div>
            <div class="ssInputBlockDiv">
                <div class="labelDiv">
                    <%= GetGlobalResourceObject("StopSale", "lblLastDate")%>
                    :
                </div>
                <div class="ssInputDiv">
                    <asp:TextBox ID="fltDate2" runat="server" Width="110px" ReadOnly="True" />
                    <rjs:PopCalendar ID="ppcDate2" runat="server" Control="fltDate2" ClientScriptOnDateChanged="DateChangedTo" />
                </div>
            </div>
            <div id="depCityDiv" class="ssInputBlockDiv" style="display: none;">
                <div class="labelDiv">
                    <%= GetGlobalResourceObject("ResMonitor", "lblDepartureCity")%>
                    :
                </div>
                <div class="ssInputDiv">
                    <select id="fltDepCity" style="width: 95%;" onchange="ChangeDepCity();">
                    </select>
                </div>
            </div>
            <div id="arrCityDiv" class="ssInputBlockDiv" style="display: none;">
                <div class="labelDiv">
                    <%= GetGlobalResourceObject("ResMonitor", "lblArrivalCity")%>
                    :
                </div>
                <div class="ssInputDiv">
                    <select id="fltArrCity" style="width: 95%;" onchange="ChangeArrCity();">
                    </select>
                </div>
            </div>
            <div id="flgClassDiv" class="ssInputBlockDiv" style="display: none;">
                <div class="labelDiv">
                    <%= GetGlobalResourceObject("BookTicket", "flightInfoClass")%>
                    :
                </div>
                <div class="ssInputDiv">
                    <select id="fltFlightCls" style="width: 95%;">
                    </select>
                </div>
            </div>
            <div class="ssInputBlockDiv">
                <div class="labelDiv">
                    <%= GetGlobalResourceObject("StopSale", "lblHotel")%>
                    :
                </div>
                <div class="ssInputDiv">
                    <select id="fltHotel" style="width: 95%;">
                    </select>
                </div>
            </div>
            <div class="ssInputBlockDiv" style="text-align: center;">
                <div>
                    <input id="rbSelectListType0" name="rbSelectListType" type="radio" value="0" checked="checked"
                        onclick="setOptions();" />
                    <label for="rbSelectListType0">
                        <%= GetGlobalResourceObject("StopSale", "cbStandartStopSaleList")%></label>
                    <input id="rbSelectListType1" name="rbSelectListType" type="radio" value="1" onclick="setOptions();" />
                    <label for="rbSelectListType1">
                        <%= GetGlobalResourceObject("StopSale", "cbStopSaleListWithFlightOptimization")%></label>
                    <input id="hfOptionType" type="hidden" value="1" />
                </div>
            </div>
            <br />
            <div class="ssInputBlockDiv" style="width: 100%; height: 35px; text-align: center;">
                <table cellpadding="0" cellspacing="0" style="width: 305px;">
                    <tr>
                        <td style="width: 150px; text-align: center;">
                            <input visible="true" type="button" id="btnFilter" value='<%= GetGlobalResourceObject("StopSale", "btnFilter")%>'
                                style="min-width: 120px; height: 29px;" onclick="return btnFilter_onclick()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                        </td>
                        <td style="text-align: center;">
                            <input type="button" visible="true" id="btnUnFilter" value='<%= GetGlobalResourceObject("StopSale", "btnClearFilter")%>'
                                style="min-width: 120px; height: 29px;" onclick="return btnUnFilter_onclick()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="Content">
            <div class="dataTables_scroll">
                <div id="StopSaleGrid" class="dataTables_scrollBody">
                </div>
            </div>
        </div>
        <div class="Footer">
            <tv1:Footer ID="tvfooter" runat="server" />
        </div>
    </div>
    </form>
</body>
</html>
