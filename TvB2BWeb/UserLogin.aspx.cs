﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using TvBo;
using TvTools;
using System.Web.Services.Discovery;
public partial class UserLogin : BasePage
{
    protected System.Collections.Specialized.NameValueCollection queryString;

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["ResMonFilterDef"] = null;
        if (!IsPostBack) {
            String[] userLang = Request.UserLanguages;
            String[] CI = userLang[0].Split(';');
            string CultureStr = CI[0];
            if (Equals(CultureStr, "nb"))
                CultureStr = "nb-NO";
            if (Equals(CultureStr, "sr-SC"))
                CultureStr = "en-US";

            CultureInfo[] allCulture = CultureInfo.GetCultures(CultureTypes.AllCultures);
            if (allCulture.Where(w => Equals(w.Name, CultureStr)).Count() < 1) {
                CultureStr = "en-US";
            }

            if (Equals(CultureStr, "ar")) {
                string defaultCulture = Conversion.getStrOrNull(ConfigurationManager.AppSettings["DefaultCulture"]);
                CultureStr = string.IsNullOrEmpty(defaultCulture) ? "en-US" : (defaultCulture.IndexOf(CultureStr) > -1 ? defaultCulture : "en-US");
            }
            if (CultureStr.Length < 5) {
                CultureStr = (from culinf in CultureInfo.GetCultures(CultureTypes.AllCultures).AsEnumerable()
                              where culinf.Name == CultureStr
                              select new { CultureName = culinf.TextInfo.CultureName }).First().CultureName;
            }
            CultureInfo ci = new CultureInfo(CultureStr);
            Session["Culture"] = ci;
            queryString = Request.QueryString;
            string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\APMSelectList." + Session.SessionID;
            if (File.Exists(path))
                File.Delete(path);
        }
    }

    public static string IpAddress()
    {
        string strIpAddress;
        strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (strIpAddress == null) {
            strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }
        return strIpAddress;
    }

    public static bool IpControl(User UserData)
    {
        string errorMsg = string.Empty;
        if (UserData.MasterAgency)
            return true;
        else {
            System.Collections.Generic.List<AgencyIPList> Ips = new TvBo.Common().AllowIps(UserData.AgencyID, ref errorMsg);
            if (Ips == null || Ips.Count < 1)
                return true;
            else {
                string myIpStr = IpAddress();
                if (string.Equals(myIpStr.Trim(), "127.0.0.1"))
                    return true;
                var q1 = from ip in Ips.AsEnumerable()
                         where ip.ServiceControl == true
                         select new { IpNo = ip.IpNo };

                var q = from ip in q1
                        where ip.IpNo == myIpStr
                        select ip;
                if (q1.Count() > 0) {
                    if (q.Count() > 0)
                        return true;
                    else
                        return false;
                } else
                    return true;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData(string queryString)
    {
        string basePageRoot = TvTools.Conversion.getStrOrNull(HttpContext.Current.Session["BasePageRoot"]);
        string basePage = TvTools.Conversion.getStrOrNull(HttpContext.Current.Session["BasePage"]);
        string tempResData = string.Empty;
        if (HttpContext.Current.Session["tempResData"] != null)
            tempResData = (string)HttpContext.Current.Session["tempResData"];
        HttpContext.Current.Session.RemoveAll();
        HttpContext.Current.Session["tempResData"] = tempResData;
        HttpContext.Current.Session["BasePageRoot"] = basePageRoot;
        HttpContext.Current.Session["BasePage"] = basePage;
        string loginVersion = Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["LoginVersion"]);
        if (string.Equals(loginVersion, "V2")) {
            return basePageRoot + "Logins/UserLogin.aspx" + (!string.IsNullOrEmpty(queryString) ? "?" + queryString : "");
        } else {
            string webID = new UICommon().getWebID();

            var host =HttpContext.Current.Request.Url.Host;
            var serverIP = HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
            object _sslControl = new TvBo.Common().getFormConfigValue("General", "UseSSL");
            bool useSSL = TvTools.Conversion.getBoolOrNull(_sslControl).HasValue ? TvTools.Conversion.getBoolOrNull(_sslControl).Value : false;
            string absolutePath = VirtualPathUtility.ToAbsolute("~/");
            string sslPath = string.Format("http://{0}/{1}", HttpContext.Current.Request.ServerVariables["HTTP_HOST"],absolutePath);
            
            if (useSSL && HttpContext.Current.Request.IsLocal.Equals(false) & host != serverIP)
            {
                sslPath = string.Format("https://{0}/{1}",HttpContext.Current.Request.ServerVariables["HTTP_HOST"],absolutePath);
            }
            if (File.Exists(HttpContext.Current.Server.MapPath("") + "\\Login\\" + webID + "\\UserLogin.aspx")) {
                return sslPath + "Login/" + webID + "/" + "UserLogin.aspx" + (!string.IsNullOrEmpty(queryString) ? "?" + queryString : "");
            } else {
                return sslPath + "Login/Default/" + "UserLogin.aspx" + (!string.IsNullOrEmpty(queryString) ? "?" + queryString : "");
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public static string extLogin(string token, string queryString)
    {
        string basePageRoot = TvTools.Conversion.getStrOrNull(HttpContext.Current.Session["BasePageRoot"]);
        string basePage = TvTools.Conversion.getStrOrNull(HttpContext.Current.Session["BasePage"]);
        string tempResData = string.Empty;
        if (HttpContext.Current.Session["tempResData"] != null)
            tempResData = (string)HttpContext.Current.Session["tempResData"];
        HttpContext.Current.Session.RemoveAll();
        HttpContext.Current.Session["tempResData"] = tempResData;
        HttpContext.Current.Session["BasePageRoot"] = basePageRoot;
        HttpContext.Current.Session["BasePage"] = basePage;

        #region
        if (HttpContext.Current.Session["Culture"] != null)
        {
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo(((System.Globalization.CultureInfo)HttpContext.Current.Session["Culture"]).Name, false);
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
            HttpContext.Current.Session["Culture"] = culture;
        }
        else
        {
            String[] userLang = HttpContext.Current.Request.UserLanguages;
            String[] CI = userLang[0].Split(';');
            string CultureStr = CI[0];

            CultureInfo[] allCulture = CultureInfo.GetCultures(CultureTypes.AllCultures);
            if (allCulture.Where(w => Equals(w.Name, CultureStr)).Count() < 1)
            {
                CultureStr = "en-US";
            }

            if (Equals(CultureStr, "nb"))
                CultureStr = "nb-NO";
            if (Equals(CultureStr, "sr-SC"))
                CultureStr = "en-US";
            if (Equals(CultureStr, "ar"))
            {
                string defaultCulture = Conversion.getStrOrNull(ConfigurationManager.AppSettings["DefaultCulture"]);
                CultureStr = string.IsNullOrEmpty(defaultCulture) ? "en-US" : (defaultCulture.IndexOf(CultureStr) > -1 ? defaultCulture : "en-US");
            }
            if (CultureStr.Length < 5)
            {
                CultureStr = (from culinf in CultureInfo.GetCultures(CultureTypes.AllCultures).AsEnumerable()
                              where culinf.Name == CultureStr
                              select new { CultureName = culinf.TextInfo.CultureName }).First().CultureName;
            }
            CultureInfo ci = new CultureInfo(CultureStr);
            HttpContext.Current.Session["Culture"] = ci;
        }
        System.Collections.Specialized.NameValueCollection _queryString = new System.Collections.Specialized.NameValueCollection();
        if (!string.IsNullOrEmpty(queryString))
        {
            var q1 = queryString.Split('&').AsEnumerable();
            foreach (var row in q1)
                _queryString.Set(row.Split('=').Length > 0 ? row.Split('=')[0] : row, row.Split('=').Length > 0 ? row.Split('=')[1] : "");
        }
        #endregion

        string errorMsg = string.Empty;
        string webID = new UICommon().getWebID();
        User UserData = new Users().CreateUserData();
        string version = TvTools.Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "Version"));
        ExtLogin.Login _extLogin = new ExtLogin.Login();

        _extLogin.Url = Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["ExtLogin.login"]);
        DataTable _extLoginReturn = _extLogin.getLogin(token);
        if (_extLoginReturn != null && _extLoginReturn.Rows.Count > 0)
        {
            UserData.AgencyID = Conversion.getStrOrNull(_extLoginReturn.Rows[0]["AgencyID"]);
            UserData.UserID = Conversion.getStrOrNull(_extLoginReturn.Rows[0]["UserID"]);

            if (!IpControl(UserData))
            {
                errorMsg = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "UnauthorizedIpNumber").ToString(), IpAddress());
                if (errorMsg != "")
                    return WebRoot.BasePageRoot + "Default.aspx?Message=" + TvBo.UICommon.EncodeJsString(errorMsg);
                else
                    return WebRoot.BasePageRoot + "Default.aspx";
            }

            if (new Users().getUser(ref UserData, IpAddress(), HttpContext.Current.Session.SessionID, ref errorMsg))
            {

                List<TvBo.MainMenu> mainMenuData = CacheObjects.getMainMenuData(UserData, ref errorMsg);
                string firstMenu = string.Empty;
                try
                {
                    object value = new TvBo.Common().getFormConfigValue("General", "FirstPage");
                    string currentUrl = (value != null ? (string)value : "ResMonitor" + version + ".aspx");
                    HttpContext.Current.Session["Menu"] = mainMenuData.Find(f => string.Equals(f.Url, currentUrl)) != null ?
                                                                mainMenuData.Find(f => string.Equals(f.Url, currentUrl)).SubMenuItem.ToString() : "ResMonitor";
                }
                catch
                {
                    HttpContext.Current.Session["Menu"] = "ResMonitor";
                }

                TvBo.MainMenu currentMenu = mainMenuData.Find(f => string.Equals(f.SubMenuItem, HttpContext.Current.Session["Menu"].ToString()));
                firstMenu = currentMenu.Url + currentMenu.PageExt;

                if (new Users().UserControl(ref UserData, IpAddress(), ref errorMsg, HttpContext.Current.Session.SessionID))
                {
                    UserData.HeaderData = new UICommon().getHeaderData(UserData, ref errorMsg);

                    UserData.CheckMarketLang = Equals(ConfigurationManager.AppSettings["CheckMarketLang"], "1");
                    UserData.EqMarketLang = UserData.MarketLang == UserData.Ci.LCID;
                    HttpContext.Current.Session["UserData"] = UserData;
                    string BasePageUrl = string.Empty;
                    BasePageUrl = WebRoot.BasePageRoot;

                    HttpContext.Current.Session["UserData"] = UserData;
                    if (queryString != null)
                    {
                        Int16? b2bToB2CSource = Conversion.getInt16OrNull(!string.IsNullOrEmpty(_queryString.Get("B2BToB2CSource")) ? _queryString.Get("B2BToB2CSource").ToString() : "");
                        if (!b2bToB2CSource.HasValue || (B2CToB2BSource)b2bToB2CSource.Value == B2CToB2BSource.PaketSearch)
                        {
                            if (!string.IsNullOrEmpty(_queryString.Get("CatPackId")) && !string.IsNullOrEmpty(_queryString.Get("ARecNo")) &&
                                !string.IsNullOrEmpty(_queryString.Get("PRecNo")) && !string.IsNullOrEmpty(_queryString.Get("HAPRecNo")) &&
                                !string.IsNullOrEmpty(_queryString.Get("Chd1")) && !string.IsNullOrEmpty(_queryString.Get("Chd2")) &&
                                !string.IsNullOrEmpty(_queryString.Get("Chd3")) && !string.IsNullOrEmpty(_queryString.Get("Chd4")))
                            {
                                if (new Reservation().makeB2CReservation(UserData, _queryString, ref errorMsg))
                                    return basePageRoot + "/ResMonitor" + version + ".aspx?FromB2C=1&First=ok&ExLogV=2";
                                else
                                {
                                    string UrlQueryStr = HttpUtility.UrlEncode(errorMsg, System.Text.Encoding.UTF8);
                                    return basePageRoot + "/Default.aspx?Message=" + UrlQueryStr.Replace("+", "%20");
                                }
                            }
                            else
                            {
                                return WebRoot.BasePageRoot + firstMenu + (firstMenu.IndexOf('?') > 0 ? "&First=ok" : "?First=ok");
                            }
                        }
                        else
                        {
                            if (b2bToB2CSource.HasValue && (B2CToB2BSource)b2bToB2CSource.Value != B2CToB2BSource.PaketSearch)
                            {
                                if (new Reservation().makeB2CIndividualReservation(UserData, _queryString, (B2CToB2BSource)b2bToB2CSource.Value, ref errorMsg))
                                {
                                    return BasePageUrl + "/ResMonitor" + version + ".aspx?FromB2C=1&First=ok";
                                }
                                else
                                {
                                    string UrlQueryStr = HttpUtility.UrlEncode(errorMsg, System.Text.Encoding.UTF8);
                                    return BasePageUrl + "/Default.aspx?Message=" + UrlQueryStr.Replace("+", "%20");
                                }
                            }
                            else
                            {
                                return WebRoot.BasePageRoot + firstMenu + (firstMenu.IndexOf('?') > 0 ? "&First=ok" : "?First=ok");
                            }
                        }
                    }
                    else
                    {
                        return WebRoot.BasePageRoot + firstMenu + (firstMenu.IndexOf('?') > 0 ? "&First=ok" : "?First=ok");
                    }
                }
                else
                {
                    HttpContext.Current.Session["UserData"] = UserData;
                    if (errorMsg != "")
                        return WebRoot.BasePageRoot + "Default.aspx?Message=" + TvBo.UICommon.EncodeJsString(errorMsg);
                    else
                        return WebRoot.BasePageRoot + "Default.aspx";
                }
            }
            else
            {
                return WebRoot.BasePageRoot;
            }
        }
        else
        {
            return WebRoot.BasePageRoot;
        }
    }
}
