﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MainMenu.ascx.cs" Inherits="MainMenu" %>

<script type="text/javascript">    
    $(document).ready(function () {
        //$("#mnMainMenu").tooltip();
        $("#tabs").tabs("option", "selected", "<%= mainID %>");
        $('#tabs').tabs({
            load: function(event, ui) {
                $('a', ui.panel).click(function() {
                    $(ui.panel).load(this.href);
                    return false;
                });
            }
        });
    });
</script>
<asp:Literal ID="mnMainMenu" runat="server" />
