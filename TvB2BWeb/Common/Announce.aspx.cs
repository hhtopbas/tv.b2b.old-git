﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Text;
using TvBo;
using System.Threading;

public partial class Common_Announce : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [WebMethod(EnableSession = true)]
    public static string getAnnounce()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();        
        List<TvBo.AnnounceRecord> allAnnounce = new TvBo.Announces().getAllAnnounce(UserData, ref errorMsg);
        if (allAnnounce != null && allAnnounce.Count > 0)
        {
            foreach (TvBo.AnnounceRecord row in allAnnounce)
            {
                sb.Append("<div class=\"announceRowDiv\">");
                sb.Append("<div class=\"annonceRow\">");
                sb.Append(row.Message);
                sb.Append("</div>");
                sb.Append("</div>");
            }
        }
        else
        {
            sb.AppendFormat("<b>{0}</b>", HttpContext.GetGlobalResourceObject("LibraryResource", "msgAnnounce"));
        }
        return sb.ToString();
    }
}
