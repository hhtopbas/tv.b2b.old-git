﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Footer.ascx.cs" Inherits="Comman_Footer" %>
<script type="text/javascript">
    $(document).ready(function () {
        $("#tvLogo").attr("src", $("#basePageUrl").val() + "Images/tv_kc_logo.gif");
    });
</script>
<div id="footer" class="ui-widget ui-widget-content ui-widget-header">
    <div id="footerMenu" class="ui-widget ui-widget-content ui-widget-header">
        <table id="footerMenuTable" border="0" cellspacing="0" cellpadding="0">
            <tr>                
                <td valign="middle" align="center">
                    <img id="tvLogo" alt="" title="" src="Images/tv_kc_logo.gif" width="40px" height="40px" />
                </td>
                <td valign="top">
                    <a href="http://www.tourvisio.com/" class="LogoutLink" target="_blank" title="San Bilgisayar">
                        <span><strong>TourVisio<sup>TM</sup></strong>
                        <br />
                        <br />
                        Tour Operator Solutions</span>
                    </a>
                </td>
                <td style="background:none;">
                    &nbsp;&nbsp;<strong>Version :</strong><asp:Label ID="txtVersion" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</div>
