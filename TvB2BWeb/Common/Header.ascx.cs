﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using TvBo;
using TvTools;


public partial class Header : System.Web.UI.UserControl
{
    protected string scrollSpeed = "1";
    protected string usingPaximumButtons = "0";
    protected string paximumPopupUrl = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        string errorMsg = string.Empty;
        termsCondation.Visible = false;
        currenyRates.Visible = false;
        User UserData = (User)Session["UserData"];
        bool FirstPage = false;
        if (!IsPostBack)
        {
            //VirtualPathUtility.ToAbsolute("~/");

            string imgUrl = string.Equals(System.Configuration.ConfigurationManager.AppSettings["CachingMethod"], "V2_") ?
                            VirtualPathUtility.ToAbsolute("~/") + GlobalLib.getOprLogoUrl(UserData.Operator) : CacheObjects.getOperatorLogoUrl(UserData.Operator);
            imageOperator.ImageUrl = imgUrl;
            string _clickTo = TvTools.Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "ClickTo"));
            if (!string.IsNullOrEmpty(_clickTo))
            {
                try
                {
                    List<LogoClickTo> clickToList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LogoClickTo>>(_clickTo);
                    LogoClickTo clickTo = clickToList.Find(f => string.Equals(f.Market, UserData.Market));
                    if (clickTo != null)
                    {
                        imageOperator.Attributes.Add("onclick", "window.open('" + clickTo.Url + "','_blank')");
                        imageOperator.Style.Add("cursor", "pointer");
                    }
                }
                catch
                {

                }
            }
            txtOperator.Text = UserData.EqMarketLang ? (UserData.HeaderData.OperatorL + " / " + UserData.HeaderData.OfficeL) : (UserData.HeaderData.Operator + " / " + UserData.HeaderData.Office);
            txtMarket.Text = UserData.EqMarketLang ? UserData.HeaderData.MarketL : UserData.HeaderData.Market;
            txtAgency.Text = (UserData.EqMarketLang ? UserData.HeaderData.AgencyNameL : UserData.HeaderData.AgencyName) + (string.IsNullOrEmpty((UserData.EqMarketLang ? UserData.HeaderData.MainAgencyL : UserData.HeaderData.MainAgency)) ? "" : " / " + (UserData.EqMarketLang ? UserData.HeaderData.MainAgencyL : UserData.HeaderData.MainAgency));
            txtUser.Text = UserData.EqMarketLang ? UserData.HeaderData.UserNameL : UserData.HeaderData.UserName;
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
                trAgencyRating.Visible = true;
            else
                trAgencyRating.Visible = false;

            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            {
                txtAgencyRaiting.Text = string.Format(HttpContext.GetGlobalResourceObject("HeaderResource", "lblAgencyRaiting").ToString(),
                                                        DateTime.Today.Year);
                txtRaiting.Text = string.Format(HttpContext.GetGlobalResourceObject("HeaderResource", "lblAgencyRaitingNo").ToString(),
                                                        UserData.AgencyRating);
            }
            if (ViewState["FirstPage"] == null && !string.IsNullOrEmpty(Request.Params["First"]))
            {
                FirstPage = Request.Params["First"].ToString() == "ok" ? true : false;
            }

            CultureInfo ci = UserData.Ci;
            mesajTxt.Text = ci.EnglishName.ToString();

            flag.ImageUrl = "~/Images/flag/" + ci.Name.Split('-')[ci.Name.Split('-').Count() - 1] + ".gif";

            if (!(Request.Url.ToString().IndexOf("BeginPayment.aspx") > -1 || Request.Url.ToString().IndexOf("ShowPaymentFinal") > -1 || Request.Url.ToString().IndexOf("noPaymentResult") > -1))
                topMenuCreate(UserData);

            string _scrollSpeed = TvTools.Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "scrollSpeed"));
            if (!string.IsNullOrEmpty(_scrollSpeed))
                scrollSpeed = _scrollSpeed;

            AgencyRisk agencyRisk = new AgencyPaymentMonitors().getAgencyRisk(UserData, ref errorMsg);
            bool? isHeaderRiskVisibility = TvTools.Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "headerRiskVisibility"));


            if (Common.crID_DreamHolidays!=UserData.CustomRegID &&  Common.crID_ViyaTurizm!=UserData.CustomRegID && Common.crID_CelexTravel != UserData.CustomRegID && Common.crID_Novaturas_Lt != UserData.CustomRegID && agencyRisk != null && agencyRisk.CreditLimit.HasValue && agencyRisk.Risk.HasValue && agencyRisk.CreditLimit.Value > 0)
            {
                string riskHtml = string.Empty;

                riskHtml += "<table style=\"width: 100%;\">";
                riskHtml += " <tr>";
                riskHtml += "  <td align=\"right\" style=\"width: 43%;\">";
                riskHtml += "    <b>Credit Limit</b>";
                riskHtml += "  </td>";
                riskHtml += "  <td align=\"right\" style=\"width: 43%;\">";
                riskHtml += "    <b>Risk</b>";
                riskHtml += "  </td>";
                riskHtml += "  <td rowspan=\"2\" style=\"text-align: center; cursor: pointer;\">";
                riskHtml += string.Format("<img src=\"{8}\" alt=\"\" onclick=\"showRisk({0}, {1}, {2}, '{3}', '{4}', '{5}', '{6}', '{7}');\">",
                    agencyRisk.Adl.ToString(),
                    agencyRisk.Chd.ToString(),
                    agencyRisk.Pax.ToString(),
                    agencyRisk.CreditLimit.HasValue ? agencyRisk.CreditLimit.Value.ToString("#,###.00") : "",
                    agencyRisk.Sale.HasValue ? agencyRisk.Sale.Value.ToString("#,###.00") : "",
                    agencyRisk.Payment.HasValue ? agencyRisk.Payment.Value.ToString("#,###.00") : "",
                    agencyRisk.Balance.HasValue ? agencyRisk.Balance.Value.ToString("#,###.00") : "",
                    agencyRisk.Risk.HasValue ? agencyRisk.Risk.Value.ToString("#,###.00") : "",
                    Global.getBasePageRoot() + "Images/info.gif");
                riskHtml += "  </td>";
                riskHtml += " </tr>";
                riskHtml += "<tr>";
                riskHtml += " <td align=\"right\">";
                riskHtml += agencyRisk.CreditLimit.Value.ToString("#,###.00");
                riskHtml += "  </td>";
                riskHtml += "  <td align=\"right\">";
                riskHtml += agencyRisk.Risk.Value.ToString("#,###.00");
                riskHtml += "  </td>";
                riskHtml += " </tr>";

                riskHtml += "</table>";
                creditLimitDiv.Text = riskHtml;
                creditLimit.Visible = true;
            }
            else
            {
                creditLimit.Visible = false;
            }
        }

        if (string.Equals(System.Configuration.ConfigurationManager.AppSettings["ShowVPosDocument"], "1"))
        {
            var common = new Common();
            bool? showPrivacyPolicy = Conversion.getBoolOrNull(common.getFormConfigValue("General", "ShowPrivacyPolicyForVPos"));
            bool? showAboutUs = Conversion.getBoolOrNull(common.getFormConfigValue("General", "ShowAboutUsForVPos"));
            bool? showCancellationPolicy = Conversion.getBoolOrNull(common.getFormConfigValue("General", "ShowCancellationAndRefundForVPos"));
            string vposLinks = "";
            if (showPrivacyPolicy.HasValue && showPrivacyPolicy.Value)
                vposLinks = string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", Global.getBasePageRoot() + "Data/" + new UICommon().getWebID() + "/PrivacyPolicy.html",
                "Privacy Policy");
            if (showCancellationPolicy.HasValue && showCancellationPolicy.Value)
                vposLinks= string.Concat( vposLinks, "&nbsp;", string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", Global.getBasePageRoot() + "Data/" + new UICommon().getWebID() + "/CancellationAndRefund.html",
                "Cancellation and Refund"));
            if (showAboutUs.HasValue && showAboutUs.Value)
                vposLinks = string.Concat(vposLinks, "&nbsp;", string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", Global.getBasePageRoot() + "Data/" + new UICommon().getWebID() + "/ContactUs.html",
                "Contact Us"));
            TermsAndConditions.Text = vposLinks;
            termsCondation.Visible = true;
        }

        if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["ShowExchangeRate"]))
        {
            string exchangeRates = string.Empty;
            string[] currList = System.Configuration.ConfigurationManager.AppSettings["ShowExchangeRate"].ToString().Split(';');
            List<ReadyCurrencyRecord> rateList = new Common().getReadyCurrency(UserData.Market, UserData.SaleCur, ref errorMsg);
            foreach (var r in currList)
            {
                ReadyCurrencyRecord rate = rateList.Find(f => f.Code == r);
                if (rate != null)
                {
                    if (exchangeRates.Length > 0)
                        exchangeRates += "<br />";
                    exchangeRates += string.Format("<b>{0}</b> : {1} {2}", rate.Code, rate.RateIsReady.Value.ToString("#,###.0000"), UserData.SaleCur);
                }
            }
            if (exchangeRates.Length > 0)
            {
                rates.Text = exchangeRates;
                currenyRates.Visible = true;
            }
        }

        if (VersionControl.getTableField("ParamSystem", "pxm_use") && Conversion.getStrOrNull(ConfigurationManager.AppSettings["ShowPaxPopup"]) == "1")
        {
            bool? showPaximumBtn = Convert.ToBoolean(new Common().getValueForQuery(string.Empty, "Select pxm_use=isnull(pxm_use,0) From ParamSystem (NOLOCK) Where Market=''", ref errorMsg));
            if (showPaximumBtn.HasValue && showPaximumBtn.Value && (!UserData.AgencyRec.Pxm_Use.HasValue || (UserData.AgencyRec.Pxm_Use.HasValue && UserData.AgencyRec.Pxm_Use.Value)))
            {
                usingPaximumButtons = "1";
                paximumPopupUrl = string.Format("http://media.paximum.com/tourvisio/{0}.jpg", UserData.CustomRegID.ToString());
            }
        }

    }

    protected void topMenuCreate(User UserData)
    {
        StringBuilder tMenu = new StringBuilder();
        bool changePassLink = true;
        try
        {
            object changePass = new TvBo.Common().getFormConfigValue("General", "noShowChangePass");
            if (changePass != null)
            {
                string[] noShowMarket = changePass.ToString().ToUpper().Split('|');
                if (noShowMarket.Length > 0)
                {
                    if (noShowMarket.Contains(UserData.Market))
                        changePassLink = false;
                }
            }
        }
        catch { }
        if (changePassLink)
        {
            if (string.IsNullOrEmpty(UserData.UserID))
                changePassLink = false;
        }
        tMenu.Append("<div id=\"tabsTopMenu\" class=\"ui-tabs ui-widget ui-widget-content ui-corner-all\">");
        tMenu.Append("<ul class=\"ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all\">");
        if (changePassLink)
            tMenu.Append("<li class=\"ui-state-default ui-corner-top\"><a href=\"#\" onclick=\"showChangePassword('" + WebRoot.BasePageRoot + "Common/ChangePassword.aspx" + "')\">" + GetGlobalResourceObject("HeaderResource", "changePassResource").ToString() + "</a></li>");
        //if (tMenu.Length > 0) tMenu.Append("&nbsp;");        
        tMenu.Append("<li class=\"ui-state-default ui-corner-top\"><a href=\"#\" onclick=\"showAllAnnounce('" + WebRoot.BasePageRoot + "Common/Announce.aspx" + "')\">" + GetGlobalResourceObject("HeaderResource", "AnounceResource").ToString() + "</a></li>");
        //if (tMenu.Length > 0) tMenu.Append("&nbsp;");        
        string logoutToUrljSon = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "LogourToUrl"));
        if (string.IsNullOrEmpty(logoutToUrljSon))
        {
            tMenu.Append("<li class=\"ui-state-default ui-corner-top\"><a id=\"alogout\" href=\"#\" goUrl=\"" + WebRoot.BasePageRoot + "Default.aspx" + "\">" + GetGlobalResourceObject("HeaderResource", "btnLogoutResource").ToString() + "</a></li>");
        }
        else
        {
            List<LogoutToUrlRecord> logoutToUrlList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LogoutToUrlRecord>>(logoutToUrljSon);
            LogoutToUrlRecord logoutToUrl = logoutToUrlList.Find(f => f.Market == UserData.Market);
            if (logoutToUrl != null && !string.IsNullOrEmpty(logoutToUrl.GoToUrl))
            {
                tMenu.Append("<li class=\"ui-state-default ui-corner-top\"><a id=\"alogout\" href=\"#\" goUrl=\"" + logoutToUrl.GoToUrl + "\">" + GetGlobalResourceObject("HeaderResource", "btnLogoutResource").ToString() + "</a></li>");
            }
            else
            {
                tMenu.Append("<li class=\"ui-state-default ui-corner-top\"><a id=\"alogout\" href=\"#\" goUrl=\"" + WebRoot.BasePageRoot + "Default.aspx" + "\">" + GetGlobalResourceObject("HeaderResource", "btnLogoutResource").ToString() + "</a></li>");
            }
        }
        try
        {
            object helpVisible = new TvBo.Common().getFormConfigValue("General", "HelpButtonVisable");
            if (helpVisible != null && (bool)helpVisible)
            {
                //if (tMenu.Length > 0) tMenu.Append("&nbsp;");  
                if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\Help\\" + UserData.Market))
                {
                    tMenu.Append("<li class=\"ui-state-default ui-corner-top\"><a class=\"LogoutLink\" href=\"" + WebRoot.BasePageRoot + "/Help/" + UserData.Market + "/Default.aspx\" target=\"_blank\"" + "\">" + GetGlobalResourceObject("HeaderResource", "HelpResource").ToString() + "</a></li>");
                }
                else
                {
                    string externalUrl = Conversion.getStrOrNull(new Common().getFormConfigValue("General", "ExtHelpUrl"));
                    tMenu.AppendFormat("<li class=\"ui-state-default ui-corner-top\"><a class=\"LogoutLink\" href=\"" + WebRoot.BasePageRoot + "/Help/Default.aspx{0}\" target=\"_blank\"" + "\">" + GetGlobalResourceObject("HeaderResource", "HelpResource").ToString() + "</a></li>",
                        string.IsNullOrEmpty(externalUrl) ? string.Empty : "?ExternalUrl=" + externalUrl);
                }
            }
        }
        catch { }
        tMenu.Append("</ul>");
        tMenu.Append("</div>");
        tMenu.AppendFormat("<input type=\"hidden\" id=\"basePageUrl\" value=\"{0}\" />", WebRoot.BasePageRoot);
        topMenu.Text = tMenu.ToString();
    }

    protected void AnnounceGridShow(bool first)
    {

    }

    protected void ShowAnnounce(User UserData)
    {
        if (Session["validAnnounces"] == null)
            return;
        StringBuilder sb = new StringBuilder();
        DataTable ATable = (DataTable)Session["validAnnounces"];
        if (ATable.Rows.Count == 0)
            return;
        foreach (DataRow row in ATable.Rows)
        {
            sb.Append(row["Message"].ToString() + "<br /><br />");
        }
        //CommonAnnounce.Text = sb.ToString();
    }

    protected void btnRead_Click(object sender, EventArgs e)
    {
    }
}

public class LogoClickTo
{
    public string Market { get; set; }
    public string Url { get; set; }
}