﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using TvBo;
using System.Threading;
using TvTools;

public partial class MainMenu : System.Web.UI.UserControl
{
    protected int mainID = 0;
    protected int subMenuID = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserData"] == null)
            return;
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (!IsPostBack)
        {
            string script = string.Empty;
            string errorMsg = string.Empty;
            List<TvBo.MainMenu> mainMenuData = CacheObjects.getMainMenuData(UserData, ref errorMsg);
            if ((UserData.AgencyRec.LockSale.HasValue && UserData.AgencyRec.LockSale.Value) || (UserData.AgencyRec.LockFinance.HasValue && UserData.AgencyRec.LockFinance.Value))
            {
                TvBo.MainMenu disableMenu = mainMenuData.Find(f => string.Equals(f.MenuItem, f.SubMenuItem) && string.Equals(f.MenuItem.ToString().ToLower(), "pricesearch"));
                disableMenu.Disable = false;
            }
            List<AgencySaleSer> agencySaleService = new Agency().getAgencySaleRestriction(UserData.AgencyID, ref errorMsg);
            if (UserData.AgencyRec.UseRoles.HasValue && UserData.AgencyRec.UseRoles.Value && UserData.AgencyRec.Roles != null && UserData.AgencyRec.Roles.Any())
            {
                List<AgencyRoles> agencyRoles = UserData.AgencyRec.Roles;
                foreach (TvBo.MainMenu row in mainMenuData)
                {
                    AgencyRoles role = agencyRoles.Find(f => f.RoleID == row.AuthorityID);
                    if (role != null && !role.Authority)
                    {
                        row.Disable = false;
                    }
                    if (agencySaleService.Find(f => f.ServiceType == row.ServiceType) != null)
                    {
                        row.Disable = false;
                    }
                }
            }
            if (VersionControl.getTableField("ParamSystem", "pxm_use"))
            {
                bool? showPaximumBtn = Convert.ToBoolean(new Common().getValueForQuery(string.Empty, "Select pxm_use=isnull(pxm_use,0) From ParamSystem (NOLOCK) Where Market=''", ref errorMsg));
                if (UserData.PaxSetting != null && showPaximumBtn.HasValue && showPaximumBtn.Value && (!UserData.AgencyRec.Pxm_Use.HasValue || (UserData.AgencyRec.Pxm_Use.HasValue && UserData.AgencyRec.Pxm_Use.Value)))
                {
                    if (UserData.PaxSetting.Pxm_WorkType == PxmWorkType.All || UserData.PaxSetting.Pxm_WorkType == PxmWorkType.UsePaximumButton)
                        if (mainMenuData.Where(w => w.MenuItem == "Paximum").Count() < 1)
                        {
                            mainMenuData.Add(new TvBo.MainMenu
                            {
                                SubMenu = false,
                                MenuItem = "Paximum",
                                SubMenuItem = "Paximum",
                                ResourceName = "mnPaximum",
                                MenuResource = "Paximum",
                                Disable = true,
                                Url = "3thParty/Paximum/Default",
                                ResNo = false,
                                ShowAllUser = true,
                                OnlyMainAgency = false,
                                PageExt = ".aspx",
                                NoShow = "",
                                HolPackCategory = null,
                                PackageType = "",
                                B2BMenuCat = "",
                                External = true,
                                NewWindow = true//,
                                //Style = "background-color: #005195; color: #f07b05; width: auto; height: auto;"
                            });
                        }
                        else
                        {
                            TvBo.MainMenu mainMenu = mainMenuData.Find(w => w.MenuItem == "Paximum" && w.Disable == false);
                            if (mainMenu != null)
                                mainMenuData.Remove(mainMenu);
                        }
                }
                else
                {
                    TvBo.MainMenu mainMenu = mainMenuData.Find(w => w.MenuItem == "Paximum");
                    mainMenuData.Remove(mainMenu);
                }
            }
            string BasePageUrl = WebRoot.BasePageRoot;
            StringBuilder mnMenu = new StringBuilder();



            mnMenu.Append("<div id=\"topMenu\">");
            mnMenu.Append(getMenu(mainMenuData));
            mnMenu.Append("</div>");

            mnMainMenu.Text = mnMenu.ToString();

        }
    }

    protected string getMenu(List<TvBo.MainMenu> mainMenuData)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string script = string.Empty;

        string errorMsg = string.Empty;

        string BasePageUrl = WebRoot.BasePageRoot;

        StringBuilder mnMenu = new StringBuilder();
        mnMenu.Append("   <div class=\"ui-tabs ui-widget ui-widget-content ui-corner-all\" id=\"tabs\">");
        mnMenu.Append(createMenuItems(UserData, BasePageUrl, mainMenuData));
        mnMenu.Append("   </div>");
        return mnMenu.ToString();
    }

    protected string createMenuItems(User UserData, string BasePageUrl, List<TvBo.MainMenu> mainMenuData)
    {
        StringBuilder sb = new StringBuilder();
        string topMenu = string.Empty;
        var menuQ = from q in mainMenuData.AsEnumerable()
                    where q.SubMenu == false &&
                        q.Disable &&
                        !q.NoShow.Split('|').Contains(UserData.Market) &&
                        ((!string.Equals(q.MenuItem, "PriceSearch") && UserData.MainAgency) || !UserData.MainAgency) &&
                        //((!string.Equals(q.MenuItem, "PriceSearch") && UserData.AgencyRec.WorkType == 2) || UserData.AgencyRec.WorkType != 2) &&
                        (string.IsNullOrEmpty(q.ShowMarket) || (!string.IsNullOrEmpty(q.ShowMarket) && q.ShowMarket == UserData.Market)) &&
                        (q.ShowAllUser || (q.ShowAllUser == false && UserData.ShowAllRes)) &&
                        (q.OnlyMainAgency == false || (q.OnlyMainAgency && string.Equals(UserData.MainOffice, UserData.AgencyID) && UserData.ShowAllRes)) == true
                    select new { q.Disable, q.IDNo, q.MenuItem, q.MenuResource, q.NoShow, q.ResNo, q.ResourceName, q.Selected, q.SubMenu, q.SubMenuItem, q.Url, q.OnlyMainAgency, q.PageExt, q.External, q.NewWindow, q.Style };
        var activeMenuQuery = from w in mainMenuData.AsEnumerable()
                              where Equals(w.SubMenuItem, HttpContext.Current.Session["Menu"].ToString())
                              group w by new { activeMenu = w.MenuItem } into k
                              select new { activeMenu = k.Key.activeMenu };

        string activeMenu = activeMenuQuery != null && activeMenuQuery.Count() > 0 ? activeMenuQuery.FirstOrDefault().activeMenu : string.Empty;

        topMenu += "<ul class=\"ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all\">";
        int i = -1;
        foreach (var r in menuQ)
        {
            if (r.Disable)
            {
                var subMenuQ = from q in mainMenuData.AsEnumerable()
                               where q.MenuItem == r.MenuItem && q.Disable && q.SubMenu && !q.NoShow.Split('|').Contains(UserData.Market) &&
                                    (q.ShowAllUser || (q.ShowAllUser == false && UserData.ShowAllRes)) &&
                                    (q.OnlyMainAgency == false || (q.OnlyMainAgency && string.Equals(UserData.MainOffice, UserData.AgencyID) && UserData.ShowAllRes)) == true
                               select q;

                bool selectSubMenu = (from q in mainMenuData.AsEnumerable()
                                      where q.MenuItem == r.MenuItem && q.Disable && q.SubMenu && q.SubMenuItem == Session["Menu"].ToString()
                                      select new { idNo = q.IDNo }).Count() > 0 ? true : false;

                string subMenuStr = string.Empty;
                if (subMenuQ.Count() > 0)
                {
                    foreach (var row in subMenuQ)
                    {
                        string queryString = string.Empty;
                        if (row.HolPackCategory.HasValue)
                        {
                            if (queryString.Length > 0)
                                queryString += "&";
                            queryString += "HolPackCat=" + row.HolPackCategory.Value.ToString();
                        }
                        if (!string.IsNullOrEmpty(row.PackageType))
                        {
                            if (queryString.Length > 0)
                                queryString += "&";
                            queryString += "PackType=" + row.PackageType;
                        }
                        if (!string.IsNullOrEmpty(row.B2BMenuCat))
                        {
                            if (queryString.Length > 0)
                                queryString += "&";
                            queryString += "B2BMenuCat=" + row.B2BMenuCat;
                        }

                        /*
                        string qString = string.Empty;
                        string url = BasePageUrl + r.Url + r.PageExt;
                        if (r.SubMenuItem == "SummaryPackPriceList") {
                            qString += "?ac=" + SANCyrpto.Encyrpt(UserData.CustomRegID, UserData.AgencyID, true);
                            url = r.Url;
                        }
                        */
                        //string ac = TvBo.SANCyrpto.Decrypt(UserData.CustomRegID, "mkqoF9Eqbt%2fZdX5qCbGTUw%3d%3d", true);


                        if (queryString.Length > 0)
                            queryString = (!string.IsNullOrEmpty(row.QueryStr) ? "&" : "?") + queryString;
                        string urlStr = BasePageUrl + row.Url + row.PageExt + (string.IsNullOrEmpty(row.QueryStr) ? row.QueryStr : "?" + row.QueryStr) + queryString;

                        if (row.NewWindow && row.External && !string.IsNullOrEmpty(row.Url))
                            subMenuStr += "<input type=\"button\" class=\"ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all\" onclick=\"javascript:window.open('"+row.Url+"','_blank')\" value='" + row.MenuResource + "'>";
                        else
                            if (row.SubMenuItem == Session["Menu"].ToString())

                                subMenuStr += "<input type=\"button\" class=\"ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all ui-state-active\" style=\""+(!string.IsNullOrEmpty(row.Style)?row.Style:"")+"\" onclick=\"javasript:window.location='" + urlStr + "'\" value='" + row.MenuResource + "'>";
                            else
                                subMenuStr += "<input type=\"button\" class=\"ui-button ui-button-text-only ui-widget ui-state-default ui-corner-all\" style=\"" + (!string.IsNullOrEmpty(row.Style) ? row.Style : "") + "\" onclick=\"javascript:window.location='" + urlStr + "'\" value='" + row.MenuResource + "'>";

                    }
                }


                if (Equals(r.MenuItem, activeMenu))
                {
                    sb.AppendFormat("<div id=\"{0}\" class=\"ui-tabs-panel ui-widget-content ui-corner-bottom\">{1}</div>", r.MenuItem, subMenuStr);
                }
                else
                {
                    sb.AppendFormat("<div id=\"{0}\" class=\"ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide\">{1}</div>", r.MenuItem, subMenuStr);
                }

                if (r.External == true && !string.IsNullOrEmpty(r.Url) && r.NewWindow == true)
                {
                    if (r.MenuItem == "Paximum")
                        topMenu += string.Format("<li class=\"ui-state-default ui-corner-top ui-tabs-selected ui-state-active\" onclick=\"javascript:window.open('{0}','_blank')\"><span title=\"{3}\" style=\"cursor: pointer; line-height: 2em; height:2em; padding: 0em; {2}\">{1}</span></li>",
                                                 BasePageUrl + r.Url + r.PageExt,
                                                 "<img src=\"" + WebRoot.BasePageRoot + (Global.usePaxBtnTV ? "LogoCache/paximumBtn.gif\"" : "Common/paximum.gif\"") + "/>",
                                                 "",
                                                 HttpContext.GetGlobalResourceObject("mainMenu", "lblPaximumButtonHint"));
                    else
                    {
                        if (r.MenuItem == "B2CPriceList")
                            topMenu += string.Format("<li class=\"ui-state-default ui-corner-top\" onclick=\"javascript:window.open('{0}','_blank')\"><a href=\"#{0}\">{1}</a></li>",
                                                    string.Format(r.Url, SANCyrpto.Encyrpt(UserData.CustomRegID, UserData.AgencyID, true)),
                                                    r.MenuResource, r.Style);
                        else
                            topMenu += string.Format("<li class=\"ui-state-default ui-corner-top\" onclick=\"javascript:window.open('{0}','_blank')\"><span style=\"cursor: pointer; line-height: 2em; height:2em; padding: .5em 1em; {2}\">{1}</span></li>",
                                                        r.Url,
                                                        r.MenuResource,
                                                        r.Style);
                    }
                }
                else
                {
                    if (Equals(r.MenuItem, activeMenu))
                        topMenu += string.Format("<li class=\"ui-state-default ui-corner-top ui-tabs-selected ui-state-active\"><a href=\"#{0}\">{1}</a></li>",
                                                    r.MenuItem,
                                                    r.MenuResource);
                    else
                        topMenu += string.Format("<li class=\"ui-state-default ui-corner-top\"><a href=\"#{0}\">{1}</a></li>",
                                                    r.MenuItem,
                                                    r.MenuResource);
                }
            }
        }
        topMenu += "</ul>";

        return topMenu + sb.ToString();
    }
}