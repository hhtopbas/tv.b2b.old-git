﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Announce.aspx.cs" Inherits="Common_Announce" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
    <title><%= GetGlobalResourceObject("PageTitle", "Announce")%></title>
    <link href="Announce.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        function logout() {
            self.parent.logout();
        }
        
        function pageLoad() {
            $.ajax({
                type: "POST",
                url: "../Common/Announce.aspx/getAnnounce",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#gridAnnounce").html('');
                    $("#gridAnnounce").html(msg.d);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }
    </script>

</head>
<body onload="javascript:pageLoad();">
    <form id="AnnounceShowForm" runat="server">
    <div id="divAnnounce">
        <span id="gridAnnounce"></span>
    </div>
    </form>
</body>
</html>
