﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="Header" %>

<script language="javascript" type="text/javascript">

  var btnClose = '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>';
  var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
  var lblSessionEnd = '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>';

  function showNewAnnounce() {
    var url = $("#basePageUrl").val() + 'Common/NewAnnounce.aspx';
    $("#dialog-newAnnounce").dialog({
      autoOpen: true,
      width: 760,
      height: 480,
      modal: true,
      resizable: true,
      autoResize: true,
      buttons: [{
        text: btnClose,
        click: function () {
          $(this).dialog('close');
        }
      }]
    });
    $('#newAnnounce').attr("src", url);
  }

  function checkAnnounce() {
    var url = $("#basePageUrl").val() + 'Services/ResMonitorServices.asmx/getAnnounce';
    $.ajax({
      type: "POST",
      url: url,
      data: '{}',
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function (msg) {
        if (msg.d == 'OK') {
          showNewAnnounce();
        }
        else if (msg.d == 'YES') {
        }
      },
      error: function (xhr, msg, e) {
        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
          alert(xhr.responseText);
      },
      statusCode: {
        408: function () {
          logout('');
        }
      }
    });

    var url = $("#basePageUrl").val() + 'Services/ResMonitorServices.asmx/getAllAnnounce';
    $.ajax({
      type: "POST",
      url: url,
      data: '{}',
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function (msg) {
        $("#CommonAnnounce").html('');
        if (msg.d != '')
          $("#CommonAnnounce").html(msg.d);
      },
      error: function (xhr, msg, e) {
        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
          alert(xhr.responseText);
      },
      statusCode: {
        408: function () {
          logout('');
        }
      }
    });
  }

  function showAllAnnounce(url) {
    $("#dialog-allAnnounce").dialog({
      autoOpen: true,
      width: 760,
      height: 480,
      modal: true,
      resizable: true,
      autoResize: true,
      buttons: [{
        text: btnOK,
        click: function () {
          $(this).dialog('close');
        }
      }]
    });
    $('#allAnnounce').attr("src", url);
  }

  function showRisk(adl, chd, pax, creditLimit, sale, payment, balance, risk) {
    var html = '';
    html += '<div>';
    html += '<table style="width:100%;">';

    html += ' <tr><td align="right"><b>Adult :</b></td><td align="right">' + adl + '</td></tr>';
    html += ' <tr><td align="right"><b>Child :</b></td><td align="right">' + chd + '</td></tr>';
    html += ' <tr><td align="right"><b>Pax :</b></td><td align="right">' + pax + '</td></tr>';
    html += ' <tr><td align="right"><b>Credit Limit :</b></td><td align="right">' + creditLimit + '</td></tr>';
    html += ' <tr><td align="right"><b>Sale :</b></td><td align="right">' + sale + '</td></tr>';
    html += ' <tr><td align="right"><b>Payment :</b></td><td align="right">' + payment + '</td></tr>';
    html += ' <tr><td align="right"><b>Balance :</b></td><td align="right">' + balance + '</td></tr>';
    html += ' <tr><td align="right"><b>Risk :</b></td><td align="right">' + risk + '</td></tr>';

    html += '</table>';
    html += '</div>';

    $(html).dialog({
      autoOpen: true,
      position: {
        my: 'center',
        at: 'center'
      },
      modal: true,
      resizable: true,
      autoResize: true,
      bigframe: true,
      buttons: [{
        text: btnClose,
        click: function () {
          $(this).dialog("close");
          $(this).dialog("destroy");          
        }
      }]
    });
  }

  function showDialogIFrame(changePassUrl) {
    $("#dialog").dialog("destroy");
    $("#dialog-changePassword").dialog({
      autoOpen: true,
      width: 530,
      height: 250,
      modal: true,
      resizable: true,
      autoResize: true
    });
    $('#changePassword').attr("src", changePassUrl);
  }

  function showChangePassword(url) {
    showDialogIFrame(url);
  }

  function returnChangePassword(_cancel) {
    $('#dialog-changePassword').dialog("close");
  }

  function returnAnnounce(_cancel) {
    $('#dialog-newAnnounce').dialog("close");
  }

  function logoutDefault(_url) {
    if (_url == '' || _url == 'undefined' || _url == undefined) _url = 'Default.aspx';
    window.location = _url;
  }

  function logout(_url) {
    $('<div>' + lblSessionEnd + '</div>').dialog({
      autoOpen: true,
      position: {
        my: 'center',
        at: 'center'
      },
      modal: true,
      resizable: true,
      autoResize: true,
      bigframe: true,
      buttons: [{
        text: btnOK,
        click: function () {
          $(this).dialog("close");
          $(this).dialog("destroy");
          if (_url == '' || _url == 'undefined' || _url == undefined) _url = 'Default.aspx';
          window.location = _url;
        }
      }]
    });
  }

  $(document).ready(function () {

    if (jQuery().cookies != undefined) {
      $.getScript($("#basePageUrl").val() + '/Scripts/jquery.cookies.2.2.0.js', function () { });
    }

    if ($("#btnLogoutClick").val() == 'No')
      window.location = $("#basePageUrl").val() + 'Default.aspx';

    if ($.cookies) {
      var _time = Date.parse($.cookies.get('BeginTime'));
      if (_time) {
        var _now = Date.parse(Date());
        if ((_now - _time) > 1800000) {
          checkAnnounce();
          $.cookies.set('BeginTime', Date());
        }
      }
      else {
        checkAnnounce();
        $.cookies.set('BeginTime', Date());
        _time = Date.parse($.cookies.get('BeginTime'));
      }
    }

    $("#alogout").click(function (e) {
      if (e.ctrlKey || e.metaKey) {
        var aBetterEventObject = jQuery.Event(e);
        // Now you can do what you want: (Cross-browser)
        aBetterEventObject.preventDefault();
        aBetterEventObject.isDefaultPrevented();
        aBetterEventObject.stopPropagation();
        aBetterEventObject.isPropagationStopped();
        aBetterEventObject.stopImmediatePropagation();
        aBetterEventObject.isImmediatePropagationStopped();
        $.cookies.del('autoLogin');
        $.cookies.del('rememberMe');
        logoutDefault($("#alogout").attr('goUrl'));
        return false;
      }
      else logoutDefault($("#alogout").attr('goUrl'));
    });
    if ($.cookies) {
      //$.cookies.del('PaximumPopup');
      if (!$.cookies.get('PaximumPopup') && '<%= usingPaximumButtons %>' == '1') {
            $.cookies.set('PaximumPopup', '0');
            $("#paximumPopupImage").attr("src", '<%= paximumPopupUrl %>');
                $('#dialog-paximumPopup').dialog({
                  autoOpen: true,
                  position: {
                    my: 'center',
                    at: 'center'
                  },
                  modal: true,
                  height: 700,
                  width: 990,
                  resizable: true,
                  autoResize: true,
                  bigframe: true,
                  close: function () {
                    $.cookies.set('PaximumPopup', '1');
                  }
                });
              }
            }
    });
</script>

<input id="btnLogoutClick" type="hidden" value="Yes" />
<div id="LogoutMenu" class="ui-widget ui-widget-content ui-widget-header">
  <table id="LogoutMenuTable" border="0" align="right" cellpadding="0" cellspacing="0">
    <tr>
      <td id="logoutLeft" style="width: 25px"></td>
      <td id="smallMenuTd" align="center">
        <asp:Literal ID="topMenu" runat="server" />
      </td>
      <td id="logoutRight" style="width: 25px"></td>
      <td id="flagTd" class="ui-dialog-title">
        <asp:Image ID="flag" runat="server" />
        <asp:Label ID="mesajTxt" runat="server" />
      </td>
    </tr>
  </table>
</div>
<div id="LogoAlan">
  <div id="UstTablo">
    <table cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td class="commonLeftTd">
          <div class="divCommonLeft">
            <asp:Image ID="imageOperator" runat="server" Height="100px" Width="200px" />
          </div>
        </td>
        <td id="commonCenterTd">
          <div id="divCommonCenter">
            <marquee direction="up" scrollamount="<%= scrollSpeed %>" loop="true" height="105px;"
              id="maque">
                            <div id="CommonAnnounce"></div></marquee>
          </div>
        </td>
        <td>
          <div id="divCommonRight" style="text-align: left;">
            <table id="OperatorBilgi" cellpadding="5" cellspacing="2">

              <tr>
                <td style="white-space: nowrap; text-align: right;">
                  <strong>
                    <%= GetGlobalResourceObject("HeaderResource", "lblOperator")%>:&nbsp;</strong>
                </td>
                <td style="white-space: nowrap;">
                  <asp:Label ID="txtOperator" runat="server" />
                </td>
              </tr>
              <tr>
                <td style="white-space: nowrap; text-align: right;">
                  <strong>
                    <%= GetGlobalResourceObject("HeaderResource", "lblMarket")%>:&nbsp;</strong>
                </td>
                <td align="left" style="white-space: nowrap;">
                  <asp:Label ID="txtMarket" runat="server" />
                </td>
              </tr>
              <tr>
                <td style="white-space: nowrap; text-align: right;">
                  <strong>
                    <%= GetGlobalResourceObject("HeaderResource", "lblAgency")%>:&nbsp;</strong>
                </td>
                <td style="white-space: nowrap;">
                  <asp:Label ID="txtAgency" runat="server" />
                </td>
              </tr>
              <tr>
                <td style="white-space: nowrap; text-align: right;">
                  <strong>
                    <%= GetGlobalResourceObject("HeaderResource", "lblUser")%>:&nbsp;</strong>
                </td>
                <td style="white-space: nowrap;">
                  <asp:Label ID="txtUser" runat="server" />
                </td>
              </tr>
              <tr id="trAgencyRating" runat="server">
                <td style="white-space: nowrap; text-align: right;">
                  <strong>
                    <asp:Label ID="txtAgencyRaiting" runat="server" />:&nbsp;</strong>
                </td>
                <td>
                  <asp:Label ID="txtRaiting" runat="server" />
                </td>
              </tr>
              <tr id="termsCondation" runat="server">
                <td colspan="2" align="right">
                  <asp:Literal ID="TermsAndConditions" runat="server"></asp:Literal>
                </td>
              </tr>
              <tr id="currenyRates" runat="server">
                <td colspan="2" align="left">
                  <asp:Literal ID="rates" runat="server"></asp:Literal>
                </td>
              </tr>
              <tr id="creditLimit" runat="server">
                <td colspan="2" align="left">
                  <asp:Literal ID="creditLimitDiv" runat="server"></asp:Literal>
                </td>
              </tr>
            </table>
          </div>
        </td>
      </tr>
    </table>
  </div>
  <div id="divAnounce" runat="server" style="height: 400px; display: none; visibility: hidden; background-color: White; border: solid 2px #000000; text-align: center; z-index: 999999;">
    <div style="text-align: center; width: 100%;">
      <asp:Label ID="lblAnounce" runat="server" Text="Label" Font-Bold="True" Font-Names="Verdana"
        Font-Size="Medium"></asp:Label>
    </div>
    <div>
    </div>
    <div style="text-align: center;">
      <asp:Button ID="btnRead" runat="server" Text='<%= GetGlobalResourceObject("HeaderResource", "lblCloseMsg")%>'
        Width="241px" OnClick="btnRead_Click" UseSubmitBehavior="False" />
    </div>
  </div>
</div>
<div id="dialog-changePassword" title='<%= GetGlobalResourceObject("HeaderResource", "lblChangeUserPassword")%>'
  style="display: none;">
  <iframe id="changePassword" frameborder="0" style="clear: both; width: 502px; height: 190px;"></iframe>
</div>
<div id="dialog-allAnnounce" title='<%= GetGlobalResourceObject("HeaderResource", "AnounceResource")%>'
  style="display: none;">
  <iframe id="allAnnounce" frameborder="0" style="clear: both; height: 380px; width: 730px;"></iframe>
</div>
<div id="dialog-newAnnounce" title='<%= GetGlobalResourceObject("HeaderResource", "lblNewAnnounce")%>'
  style="display: none;">
  <iframe id="newAnnounce" frameborder="0" style="clear: both; height: 380px; width: 730px;"></iframe>
</div>
<div id="dialog-paximumPopup" title='' style="display: none; width: 1000px; height: 650px;">
  <img alt="" id="paximumPopupImage" src="" onclick="javascript:window.open('<%= Global.getBasePageRoot() %>3thParty/Paximum','_blank')" />
</div>
