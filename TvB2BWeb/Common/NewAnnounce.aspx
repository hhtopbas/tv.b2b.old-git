﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewAnnounce.aspx.cs" Inherits="Common_NewAnnounce" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%= GetGlobalResourceObject("PageTitle", "NewAnnounce")%></title>
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="NewAnnounce.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function logout() {
            self.parent.logout();
        }
        
        function readAnnounce(_recID) {
            $.ajax({
                type: "POST",
                url: "../Common/NewAnnounce.aspx/setAnnounce",
                data: '{"RecID":"' + _recID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d == '') {
                        pageLoad();
                    }
                    else {
                        $("#gridAnnounce").html('');
                        $("#gridAnnounce").html(msg.d);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function pageLoad() {
            $("#gridAnnounce").html('new announce not found.');
            $.ajax({
                type: "POST",
                url: "../Common/NewAnnounce.aspx/getAnnounce",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != '') {
                        $("#gridAnnounce").html('');
                        $("#gridAnnounce").html(msg.d);
                    }
                    else {
                        window.close;
                        self.parent.returnAnnounce(false);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }
    </script>

</head>
<body onload="javascript:pageLoad();">
    <form id="AnnounceShowForm" runat="server">
    <div id="divAnnounce">
        <span id="gridAnnounce"></span>
    </div>
    </form>
</body>
</html>
