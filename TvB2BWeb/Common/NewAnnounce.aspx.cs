﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Text;
using TvBo;
using System.Threading;
using TvTools;

public partial class Common_NewAnnounce : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [WebMethod(EnableSession = true)]
    public static string getAnnounce()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();        
        List<TvBo.AnnounceForUser> allAnnounce = new TvBo.Announces().getAnnounce(UserData, ref errorMsg);
        if (allAnnounce != null && allAnnounce.Count > 0)
        {
            foreach (TvBo.AnnounceForUser row in allAnnounce)
            {
                sb.Append("<div class=\"announceRowDiv\">");
                sb.Append("<div class=\"annonceRowCheck\">");
                sb.AppendFormat("<input id=\"btnRead\" type=\"button\" value=\"{1}\" {2} onclick=\"readAnnounce({0});\"/>", 
                            row.RecID,
                            HttpContext.GetGlobalResourceObject("LibraryResource", "Read"),
                            "class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\"");
                sb.Append("</div>");
                //sb.Append("<div style=\"float:left; width:10px;\">&nbsp;&nbsp;</div>");
                sb.Append("<div class=\"annonceRow\">");
                sb.Append(row.Message);
                sb.Append("</div>");
                sb.Append("</div>");
            }
        }        
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string setAnnounce(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        int? recID = Conversion.getInt32OrNull(RecID);                
        if (new TvBo.Announces().readAnnounce(UserData, recID, ref errorMsg))
            return "";
        else return errorMsg;
    }
}
