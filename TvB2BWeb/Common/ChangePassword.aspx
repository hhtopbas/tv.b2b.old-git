﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="Common_ChangePassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "ChangePassword") %></title>
    <link href="ChangePassword.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        function logout() {
            self.parent.logout();
        }
        
        function cancelChange() {
            window.close;
            self.parent.returnChangePassword(false);
        }

        function saveChange() {
            if ($("#txtOldPassword").val() == '') {
                alert('<%= GetGlobalResourceObject("LibraryResource", "CheckOldPassword") %>');
                return;
            }
            var newPass0 = $("#txtNewPassword0");
            var newPass1 = $("#txtNewPassword1");
            if ((newPass0.val() != newPass1.val()) || newPass0.val() == '' || newPass1.val() == '') {
                alert('<%= GetGlobalResourceObject("LibraryResource", "CheckNewPassword") %>');
                return;
            }

            $.ajax({
                type: "POST",
                url: "../Common/ChangePassword.aspx/setPassword",
                data: '{"Pass":"' + $("#txtNewPassword0").val() + '","OldPass":"' + $("#txtOldPassword").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d == '') {
                        window.close;
                        self.parent.returnChangePassword(true);
                    }
                    else {
                        alert(msg.d);
                        return;
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }
    </script>

</head>
<body>
    <form id="ChangePasswordForm" runat="server">
    <div class="PasswordDiv">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" class="header">
                    <%= GetGlobalResourceObject("HeaderResource", "changePassResource") %>
                </td>
            </tr>
            <tr>
                <td class="tdLeft">
                    <%= GetGlobalResourceObject("HeaderResource", "oldPassResource") %>
                </td>
                <td class="tdRight">
                    <input id="txtOldPassword" type="password" class="input" />
                </td>
            </tr>
            <tr>
                <td class="tdLeft">
                    <%= GetGlobalResourceObject("HeaderResource", "newPassResource") %>
                </td>
                <td class="tdRight">
                    <input id="txtNewPassword0" type="password" class="input" />
                </td>
            </tr>
            <tr>
                <td class="tdLeft">
                    <%= GetGlobalResourceObject("HeaderResource", "confirmPassResource") %>
                </td>
                <td class="tdRight">
                    <input id="txtNewPassword1" type="password" class="input" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <input id="CancelButton" type="button" value='<%= GetGlobalResourceObject("HeaderResource", "btnCancel") %>'
                        style="width: 100px;" onclick="cancelChange();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />&nbsp;&nbsp;&nbsp;
                    <input id="ChangeButton" type="button" value='<%= GetGlobalResourceObject("HeaderResource", "btnChange") %>'
                        style="width: 100px;" onclick="saveChange();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
