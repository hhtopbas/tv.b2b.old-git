﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Web.Services;
using System.Threading;

public partial class Common_ChangePassword : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {  
      
    }

    [WebMethod]
    public static string setPassword(string Pass, string OldPass)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (!Equals(UserData.Password, OldPass)) return HttpContext.GetGlobalResourceObject("LibraryREsource", "CheckOldPassword").ToString();
        if (new Users().changePassword(UserData, Pass))
            return "";
        else return "Error";
    }
}
