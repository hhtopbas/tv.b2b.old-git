﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;

public partial class Comman_Footer : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        { 
            string tvVersion = ((User)Session["UserData"]).TvVersion;
            string webVersion = ((User)Session["UserData"]).WebVersion;
            txtVersion.Text = Convert.ToInt32(tvVersion.Substring(0, 2)).ToString() + "." + Convert.ToInt32(tvVersion.Substring(2, 2)).ToString() + "." + Convert.ToInt32(tvVersion.Substring(4, 2)).ToString() + " (Build:" + Convert.ToInt32(tvVersion.Substring(6, 3)).ToString() + ") - (Web:" + Convert.ToInt32(webVersion).ToString() + ")";
        }
    }
}
