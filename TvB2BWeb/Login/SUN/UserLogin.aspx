﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserLogin.aspx.cs" Inherits="UserLogin"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("Login", "PageTitle") %></title>
    <link rel="stylesheet" type="text/css" href="login.css" />
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>   
    <script src="../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.url.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.json.js" type="text/javascript"></script>

    <link href="../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/JavaScript">
        
        function sahakontrol() {
            if ($("#txtAgency").val() == "" || $("#txtPassword").val() == "") {
                if ($("#txtPassword").val() == "") { alert(document.forms[0].HiddenAgency.value); }
                if ($("#txtPassword").val() == "") { alert(document.forms[0].HiddenPassword.value); }
                return false;
            }
            return true;
        }
        
        $(document).ready(function() {
            $.query = $.query.load(location.href);
            Message = $.query.get('Message');
            if (Message != undefined && Message != '') {
                if (Message == "'AAA'")
                    showAlert('Внимание! Для бронирования необходимо перезаключение агентского договора, доступного на сайте «Санрайз тур» в разделе «Сотрудничество». <br />Для срочного заключения договора его подписанную версию необходимо отправить на адрес Registration@sunrise-tour.ru с пометкой «Срочно», и новые пароли будут выданы в течение 1 часа. По всем вопросам просим Вас обращаться в отдел продаж «Санрайз тур» к Вашим кураторам.');
                else showAlert(Message.replace(/\+/g, " "));
            }
            maximize();            
            if ($.cookies) {
                $("#remember").attr('checked', 'checked');
                var rememMe = $.cookies.get('rememberMe');
                if (rememMe != null) {
                    var rememberMe = $.browser.msie ? $.json.decode(rememMe) : rememMe;
                    $("#txtAgency").val(rememberMe.agencyCode);
                    $("#txtUserName").val(rememberMe.userCode);
                    $("#txtPassword").val(rememberMe.pass);
                } else $("#remember").removeAttr('checked');
            }            
        });

        function tryLogin() {
            if (sahakontrol()) {
                if ($.cookies) {
                    if ($("#remember").attr('checked')) {
                        var rememberMe = { agencyCode: $("#txtAgency").val(), userCode: $("#txtUserName").val(), pass: $("#txtPassword").val() };
                        CookieExpires = new Date();
                        CookieExpires.setTime(CookieExpires.getTime() + (1000 * 60 * 60 * 24 * 365));
                        $.cookies.set('rememberMe', $.browser.msie ? $.json.encode(rememberMe) : rememberMe, { expiresAt: CookieExpires });
                    }
                    else {
                        $.cookies.del('rememberMe');
                    }
                }
                var qS = $.url(location.href);

                var loginData = '{"agencyCode":"' + $("#txtAgency").val() + '",' +
                            '"userName":"' + $("#txtUserName").val() + '",' +
                            '"pass":"' + $("#txtPassword").val() + '",' +
                            '"queryString":"' + qS.data.attr.query + '"}';

                $.ajax({
                    async: false,
                    type: "POST",
                    url: "UserLogin.aspx/tryLogin",
                    data: loginData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(msg) {
                        window.location = msg.d;
                    },
                    error: function(xhr, msg, e) {
                        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                            showAlert(xhr.responseText);
                    },
                    statusCode: {
                        408: function() {
                            logout();
                        }
                    }
                });
            }
            //sahakontrol(); string agencyCode, string userName, string pass, string queryString
        }

        function showAlert(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                            return true;
                        }
                    }
                });
            });
        }

        var NS = document.all;
        
        function LoginCenter() {
            var divL = document.getElementById('divLogin');
            var myWidth = 0, myHeight = 0;
            if (typeof (window.innerWidth) == 'number') {
                //Non-IE
                myWidth = window.innerWidth;
                myHeight = window.innerHeight;
            } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
                //IE 6+ in 'standards compliant mode'
                myWidth = document.documentElement.clientWidth;
                myHeight = document.documentElement.clientHeight;
            } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
                //IE 4 compatible
                myWidth = document.body.clientWidth;
                myHeight = document.body.clientHeight;
            }

            var x = (myWidth / 2) - 417;
            var y = (myHeight / 2) - 200;

            divL.style.position = 'absolute';
            divL.style.top = y + 'px';
            divL.style.left = x + 'px';
        }

        function maximize() {
            //var W = 500, H = 400;
            var maxW = screen.availWidth;
            var maxH = screen.availHeight;
            if (location.href.indexOf('pic') == -1) {
                if (window.opera) { } else {
                    top.window.moveTo(0, 0);
                    if (document.all) { top.window.resizeTo(maxW, maxH); }
                    else
                        if (document.layers || document.getElementById) {
                        if ((top.window.outerHeight < maxH) || (top.window.outerWidth < maxW)) {
                            top.window.outerHeight = maxH; top.window.outerWidth = maxW;
                        }
                    }
                }
            }
            LoginCenter();
        }
  
    </script>

</head>
<body>
    <form id="UserLoginForm" runat="server">
    <div id="divLogin">
        <div style="margin-left: 240px; width: 357px;">
            <div id="divHelp" runat="server" style="text-align: right">
            </div>
            <div id="tvlogo">
            </div>
            <p align="center">
                <font size="1">Если Вы хотите войти в личный кабинет со старым дизайном, перейдите по
                    <a href="/b2bold">ссылке</a></font>
            </p>
           
            <div class="Loginalt">
                <div class="login_lg_sol">
                    <span>
                        <h1>
                            <span style="height: 21px; white-space: nowrap;">
                                <%= GetGlobalResourceObject("Login", "lblBaslik") %></span>
                        </h1>
                    </span>
                </div>
            </div>
            <div class="LoginOrta">
                <table width="80%" border="0" align="center" cellpadding="3" cellspacing="0">
                    <tr>
                        <td width="130" align="right" class="noWarp">
                            <span class="yazi"><b>
                                <%= GetGlobalResourceObject("Login", "lblAgency") %></b></span>
                        </td>
                        <td width="209">
                            <asp:TextBox ID="txtAgency" runat="server" CssClass="login_input" AutoCompleteType="Company" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="noWarp">
                            <span class="yazi"><b>
                                <%= GetGlobalResourceObject("Login", "lblUserName") %></b></span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtUserName" runat="server" CssClass="login_input" AutoCompleteType="FirstName" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="noWarp">
                            <span class="yazi"><b>
                                <%= GetGlobalResourceObject("Login", "lblPassword") %></b></span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPassword" runat="server" CssClass="login_input" TextMode="Password" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right">
                            <asp:Image ID="flag" runat="server" />
                            <asp:Label ID="mesajTxt" runat="server" CssClass="LangMarRight" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                        </td>
                        <td style="text-align: right">
                        </td>
                    </tr>
                </table>
            </div>
            <div class="Loginaltalt">
                <div class="LoginBTAltSag">
                    <div class="login_bt">
                        <span>
                        <input id="Button1" type="button" value='<%= GetGlobalResourceObject("Login", "loginButton") %>'
                                onclick="tryLogin();" style="width: 100px; height: 25px; font-weight: bold;"
                                class="login_button" />                                
                            <%--<asp:Button ID="loginButton" runat="server" CssClass="login_button" Text="<%$ Resources:Login, loginButton %>"
                                Width="100px" OnClientClick="return sahakontrol();" Font-Bold="True" OnClick="loginButton_Click"
                                Height="25px"></asp:Button>--%>
                        </span>
                    </div>
                </div>
                <div class="LoginBTAltSol">
                </div>
            </div>
            <div style="text-align: right;">
                <input id="remember" type="checkbox" /><label for="remember"><%= GetGlobalResourceObject("Login", "rememberMe")%></label>
            </div>
            <div align="center" width="350px" style="margin-left: 0px;">
                <table width="350">
                    <tr>
                        <td width="350">
                            <br>
                            <p style="text-align: justify;">
                                <span style="color: #ee4325;">Внимание!</span> При возникновении сложностей с входом
                                в систему бронирования просим Вас проверить актуальность действующего договора в
                                отделе продаж:</p>
                            <p align="center">
                                <font size="3">(495) 935-70-26</font><br>
                                info@sunrise-tour.ru
                            </p>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="PoweredBy">
                Powered by <a href="http://www.sansejour.com" target="_blank">
                    <img alt="" title="" src="TVLogoSmall.png" width="100" border="0" height="30" /></a>
            </div>            
        </div>        
        <div class="Loginaltalt1">
            &nbsp;
        </div>
    </div>
    <br />
    <asp:HiddenField ID="HiddenAgency" runat="server" Value="Enter Agency Code" />
    <asp:HiddenField ID="HiddenUserName" runat="server" Value="Enter User Code" />
    <asp:HiddenField ID="HiddenPassword" runat="server" Value="Enter Password" />
    <div id="dialog-message" title='<%= GetGlobalResourceObject("ResView", "lblMessage") %>'
        style="display: none;">
        <div id="messages">
            Message</div>
    </div>    
    </form>
</body>
</html>
