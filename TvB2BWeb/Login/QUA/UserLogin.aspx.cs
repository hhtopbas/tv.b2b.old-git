﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Globalization;
using System.Threading;
using TvBo;


public partial class UserLogin : BasePage
{
    protected System.Collections.Specialized.NameValueCollection queryString;

    protected override void InitializeCulture()
    {
        base.InitializeCulture();
        String[] userLang = Request.UserLanguages;
        String[] CI = userLang[0].Split(';');
        string CultureStr = CI[0];
        CultureInfo[] allCulture = CultureInfo.GetCultures(CultureTypes.AllCultures);
        if (allCulture.Where(w => Equals(w.Name, CultureStr)).Count() < 1) {
            CultureStr = "en-US";
        }
        if (Equals(CultureStr, "nb")) CultureStr = "nb-NO"; if (Equals(CultureStr, "sr-SC")) CultureStr = "en-US";
        if (Equals(CultureStr, "ar") || Equals(CultureStr, "ar-SA")) CultureStr = "en-US";
        if (CultureStr.Length < 5)
        {
            CultureStr = (from culinf in CultureInfo.GetCultures(CultureTypes.AllCultures).AsEnumerable()
                          where culinf.Name == CultureStr
                          select new { CultureName = culinf.TextInfo.CultureName }).First().CultureName;
        }
        CultureInfo ci = new CultureInfo(CultureStr);
        Session["Culture"] = ci;
        if (Session["Culture"] != null)
        {
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo(((System.Globalization.CultureInfo)Session["Culture"]).Name, false);
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(WebRoot.BasePageRoot)) Response.Redirect("~/Default.aspx");
        if (!IsPostBack)
        {
            String[] userLang = Request.UserLanguages;
            String[] CI = userLang[0].Split(';');
            string CultureStr = CI[0];
            CultureInfo[] allCulture = CultureInfo.GetCultures(CultureTypes.AllCultures);
            if (allCulture.Where(w => Equals(w.Name, CultureStr)).Count() < 1) {
                CultureStr = "en-US";
            }
            if (Equals(CultureStr, "nb")) CultureStr = "nb-NO"; if (Equals(CultureStr, "sr-SC")) CultureStr = "en-US";
            if (Equals(CultureStr, "ar") || Equals(CultureStr, "ar-SA")) CultureStr = "en-US";
            if (CultureStr.Length < 5)
            {
                CultureStr = (from culinf in CultureInfo.GetCultures(CultureTypes.AllCultures).AsEnumerable()
                              where culinf.Name == CultureStr
                              select new { CultureName = culinf.TextInfo.CultureName }).First().CultureName;
            }
            CultureInfo ci = new CultureInfo(CultureStr);
            Session["Culture"] = ci;

            mesajTxt.Text = ci.EnglishName.ToString();
            Thread.CurrentThread.CurrentCulture = ci;
            flag.ImageUrl = "../../Images/flag/" + ci.Name.Split('-')[ci.Name.Split('-').Count() - 1] + ".gif";
            txtAgency.Focus();            
        }
        queryString = Request.QueryString;
    }

    protected void loginButton_Click(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["Culture"] != null)
        {
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo(((System.Globalization.CultureInfo)HttpContext.Current.Session["Culture"]).Name, false);
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
            HttpContext.Current.Session["Culture"] = culture;
        }
        else
        {
            String[] userLang = HttpContext.Current.Request.UserLanguages;
            String[] CI = userLang[0].Split(';');
            string CultureStr = CI[0];
            CultureInfo[] allCulture = CultureInfo.GetCultures(CultureTypes.AllCultures);
            if (allCulture.Where(w => Equals(w.Name, CultureStr)).Count() < 1) {
                CultureStr = "en-US";
            }
            if (Equals(CultureStr, "nb")) CultureStr = "nb-NO"; if (Equals(CultureStr, "sr-SC")) CultureStr = "en-US";
            if (Equals(CultureStr, "ar") || Equals(CultureStr, "ar-SA")) CultureStr = "en-US";
            if (CultureStr.Length < 5)
            {
                CultureStr = (from culinf in CultureInfo.GetCultures(CultureTypes.AllCultures).AsEnumerable()
                              where culinf.Name == CultureStr
                              select new { CultureName = culinf.TextInfo.CultureName }).First().CultureName;
            }
            CultureInfo ci = new CultureInfo(CultureStr);
            HttpContext.Current.Session["Culture"] = ci;
        }
        queryString = Request.QueryString;
        string BasePageUrl = string.Empty;
        if (string.IsNullOrEmpty(WebRoot.BasePageRoot))
        {
            //BasePageUrl = Request.Url.ToString().Replace("Login/" + new UICommon().getWebID() + "/UserLogin.aspx", "");
            //Session["BasePageUrl"] = BasePageUrl;
            Response.Redirect(ResolveUrl("~/Default.aspx"));
        }
        else BasePageUrl = WebRoot.BasePageRoot;

        User UserData = new TvBo.Users().CreateUserData();

        UserData.AgencyID = txtAgency.Text.TrimStart().TrimEnd().ToUpper();
        UserData.UserID = txtUserName.Text.TrimStart().TrimEnd().ToUpper();
        UserData.Password = txtPassword.Text;
        UserData.Ci = (CultureInfo)Session["Culture"];
        queryString = Request.QueryString;
        Session["UserData"] = UserData;
        string version = TvTools.Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "Version"));
        string errorMsg = string.Empty;
        string pageUrl = new LoginUser().toLogin(UserData, ref errorMsg);
        if (pageUrl.IndexOf("Default.aspx") > 0)
        {
            Response.Redirect(pageUrl);
        }
        else
        {
            if (queryString != null && !string.IsNullOrEmpty(queryString.Get("CatPackId")) && !string.IsNullOrEmpty(queryString.Get("ARecNo")) &&
                !string.IsNullOrEmpty(queryString.Get("PRecNo")) && !string.IsNullOrEmpty(queryString.Get("HAPRecNo")) &&
                !string.IsNullOrEmpty(queryString.Get("Chd1")) && !string.IsNullOrEmpty(queryString.Get("Chd2")) &&
                !string.IsNullOrEmpty(queryString.Get("Chd3")) && !string.IsNullOrEmpty(queryString.Get("Chd4")))
            {
                if (new Reservation().makeB2CReservation(UserData, queryString, ref errorMsg))
                    Response.Redirect("~/ResMonitor" + version + ".aspx?FromB2C=1&First=ok");
                else
                {
                    string UrlQueryStr = HttpUtility.UrlEncode(errorMsg, System.Text.Encoding.UTF8);
                    Response.Redirect(BasePageUrl + "/Default.aspx?Message=" + UrlQueryStr.Replace("+", "%20"));
                }
            }
            else
            {
                
                Response.Redirect(pageUrl);
            }
        }
    }
}
