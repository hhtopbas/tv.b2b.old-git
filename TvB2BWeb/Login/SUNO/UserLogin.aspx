﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserLogin.aspx.cs" Inherits="UserLogin" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <meta http-equiv="X-UA-Compatible" content="IE=9" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    <%= GetGlobalResourceObject("Login", "PageTitle") %></title>
  <link rel="stylesheet" type="text/css" href="login.css" />
  <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico">

  <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery.url.js" type="text/javascript"></script>
  <script src="../../Scripts/urlEncode.js" type="text/javascript"></script>

  <link href="../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="login.css?v=1" rel="stylesheet" />
  <script language="JavaScript" type="text/JavaScript">

        var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';

        function addPlaceHolder(input) {
            input.focus(function () {
                if ($(this).val() == $(this).attr('placeholder')) {
                    $(this).val('');
                    $(this).removeClass('placeholder');
                }
            }).blur(function () {
                if ($(this).val() == '' || $(this).val() == $(this).attr('placeholder')) {
                    $(this).addClass('placeholder');
                    $(this).val($(this).attr('placeholder'));
                }
            }).blur();
            $('.loginBtn').click(function () {
                $(this).find(input).each(function () {
                    if ($(this).val() == $(this).attr('placeholder')) {
                        $(this).val('');
                    }
                })
            });
        }

        function sahakontrol() {
            if ($("#txtAgency").val() == "" || $("#txtPassword").val() == "") {
                if ($("#txtPassword").val() == "") { alert(document.forms[0].HiddenAgency.value); }
                if ($("#txtPassword").val() == "") { alert(document.forms[0].HiddenPassword.value); }
                return false;
            }
            return true;
        }

        $(document).ready(function () {
            if ($.browser.msie && parseInt($.browser.version.split('.')[0]) < 10) {
                addPlaceHolder($(':text'));
                addPlaceHolder($('textarea'));
                addPlaceHolder($('#txtPassword'));
            }
            $.query = $.query.load(location.href);
            Message = $.query.get('Message');
            if (Message != undefined && Message != '') {
                showAlert(Message);
            }
            maximize();
            document.UserLoginForm.txtAgency.focus();
            if ($.cookies) {
                $("#remember").attr('checked', 'checked');
                var rememMe = $.cookies.get('rememberMe');
                if (rememMe != null) {
                    var rememberMe = $.browser.msie ? $.json.decode(rememMe) : rememMe;
                    $("#txtAgency").val(rememberMe.agencyCode);
                    $("#txtUserName").val(rememberMe.userCode);
                    $("#txtPassword").val(rememberMe.pass);
                } else $("#remember").removeAttr('checked');
            }
        });

        function tryLogin() {
            if (sahakontrol()) {
                if ($.cookies) {
                    if ($("#remember").attr('checked')) {
                        var rememberMe = { agencyCode: $("#txtAgency").val(), userCode: $("#txtUserName").val(), pass: $("#txtPassword").val() };
                        CookieExpires = new Date();
                        CookieExpires.setTime(CookieExpires.getTime() + (1000 * 60 * 60 * 24 * 365));
                        $.cookies.set('rememberMe', $.browser.msie ? $.json.encode(rememberMe) : rememberMe, { expiresAt: CookieExpires });
                    }
                    else {
                        $.cookies.del('rememberMe');
                    }
                }
                var qS = $.url(location.href);

                var loginData = '{"agencyCode":"' + $("#txtAgency").val() + '",' +
                            '"userName":"' + $("#txtUserName").val() + '",' +
                            '"pass":"' + $("#txtPassword").val() + '",' +
                            '"queryString":"' + qS.data.attr.query + '"}';

                $.ajax({
                    async: false,
                    type: "POST",
                    url: "UserLogin.aspx/tryLogin",
                    data: loginData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.hasOwnProperty('d') && msg.d != '')
                            window.location = msg.d;
                    },
                    error: function (xhr, msg, e) {
                        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                            showAlert(xhr.responseText);
                    },
                    statusCode: {
                        408: function () {
                            alert('statusCode=408');
                            logout();
                        }
                    }
                });
            }
        }

        function showAlert(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: [{
                        text: btnOK,
                        click: function () {
                            $(this).dialog('close');
                            return true;
                        }
                    }]
                });
            });
        }

        var NS = document.all;

        function maximize() {
            //var W = 500, H = 400;
            var maxW = screen.availWidth;
            var maxH = screen.availHeight;
            if (location.href.indexOf('pic') == -1) {
                if (window.opera) { } else {
                    top.window.moveTo(0, 0);
                    if (document.all) { top.window.resizeTo(maxW, maxH); }
                    else
                        if (document.layers || document.getElementById) {
                            if ((top.window.outerHeight < maxH) || (top.window.outerWidth < maxW)) {
                                top.window.outerHeight = maxH; top.window.outerWidth = maxW;
                            }
                        }
                }
            }
        }

  </script>

</head>
<body>
  <form id="UserLoginForm" runat="server">
    <div class="wrapper">
      <div class="back">
        <div class="background"></div>
      </div>
      <div class="logo">
      </div>
      <div class="ui-helper-clearfix loginPanel">

        <div class="inputArea">
          <span class="header">Login to your account
          </span>
          <div class="agency">
            <span>&nbsp;</span><br />
            <input id="txtAgency" type="text" placeholder="Agency" class="agency" />
          </div>
          <div class="agency">
            <span>&nbsp;</span><br />
            <input id="txtUserName" type="text" placeholder="User" class="user" />
          </div>
          <div class="agency">
            <span>&nbsp;</span><br />
            <input id="txtPassword" type="password" placeholder="Password" class="password" />
          </div>
          <div class="login">
            <div class="loginBtn" onclick="tryLogin()">
              <a href="#">Sign-In</a>
            </div>
          </div>
          <div class="PoweredBy">
            Powered by <a href="http://www.santsg.com" target="_blank">
              <img alt="" title="" src="TVLogoSmall.png" width="100" border="0" height="30" /></a>
          </div>
        </div>

      </div>

      <br />
      <asp:HiddenField ID="HiddenAgency" runat="server" Value="Enter Agency Code" />
      <asp:HiddenField ID="HiddenUserName" runat="server" Value="Enter User Code" />
      <asp:HiddenField ID="HiddenPassword" runat="server" Value="Enter Password" />
      <div id="dialog-message" title='<%= GetGlobalResourceObject("ResView", "lblMessage") %>'
        style="display: none;">
        <span id="messages">Message</span>
      </div>
    </div>
  </form>


</body>
</html>
