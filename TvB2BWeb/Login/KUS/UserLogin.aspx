﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserLogin.aspx.cs" Inherits="Kusadasi_UserLogin"  ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("Login", "PageTitle") %></title>
    <link rel="stylesheet" type="text/css" href="login.css" />
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico">

    <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../../Scripts/urlEncode.js" type="text/javascript"></script>        
    
    <link href="../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />    
    
    <script type="text/JavaScript">

        $(document).ready(function() {
            $.query = $.query.load(location.href);
            Message = $.query.get('Message');
            if (Message != undefined && Message != '') {
                showAlert(Message.replace(/\+/g, ' '));
            }
            maximize();
            document.UserLoginForm.txtAgency.focus();
        });

        function showAlert(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                            return true;
                        }
                    }
                });
            });
        }
        
        var NS = document.all;
                
        function sahakontrol() {
            if (document.forms[0].txtAgency.value == "" || document.forms[0].txtPassword.value == "") {
                if (document.forms[0].txtAgency.value == "") { alert(document.forms[0].HiddenAgency.value); }
                if (document.forms[0].txtPassword.value == "") { alert(document.forms[0].HiddenPassword.value); }
                return false;
            }
            return true;
        }

        function LoginCenter() {
            var divL = document.getElementById('divLogin');
            var myWidth = 0, myHeight = 0;
            if (typeof (window.innerWidth) == 'number') {
                //Non-IE
                myWidth = window.innerWidth;
                myHeight = window.innerHeight;
            } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
                //IE 6+ in 'standards compliant mode'
                myWidth = document.documentElement.clientWidth;
                myHeight = document.documentElement.clientHeight;
            } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
                //IE 4 compatible
                myWidth = document.body.clientWidth;
                myHeight = document.body.clientHeight;
            }

            var x = (myWidth / 2) - 417;
            var y = 10;

            divL.style.position = 'absolute';
            divL.style.top = y + 'px';
            divL.style.left = x + 'px';
        }

        function maximize() {
            //var W = 500, H = 400;
            var maxW = screen.availWidth;
            var maxH = screen.availHeight;
            if (location.href.indexOf('pic') == -1) {
                if (window.opera) { } else {
                    top.window.moveTo(0, 0);
                    if (document.all) { top.window.resizeTo(maxW, maxH); }
                    else
                        if (document.layers || document.getElementById) {
                        if ((top.window.outerHeight < maxH) || (top.window.outerWidth < maxW)) {
                            top.window.outerHeight = maxH; top.window.outerWidth = maxW;
                        }
                    }
                }
            }
            LoginCenter();                       
        }
  
    </script>

</head>
<body>
    <form id="UserLoginForm" runat="server">
    <div id="divLogin">
        <div style="width: 250px; margin-top: 170px; margin-left: 470px;">
            <div style="height: 87px; padding-right: 10px; text-align: right; color: #222853;
                font: bold;">
                <span>
                    <%= GetGlobalResourceObject("Login", "lblBaslik") %></span></div>
            <div class="Acenta">
                <span class="yazi"><b>
                    <%= GetGlobalResourceObject("Login", "lblAgency") %></b></span>
            </div>
            <div class="AcentaInput">
                <asp:TextBox ID="txtAgency" runat="server" CssClass="login_input" AutoCompleteType="Company" />
            </div>
            <div style="height: 15px; width: 170px; margin-left: 32px;">
                <span class="yazi"><b>
                    <%= GetGlobalResourceObject("Login", "lblUserName") %></b></span>
            </div>
            <div class="LoginInput">
                <asp:TextBox ID="txtUserName" runat="server" CssClass="login_input" AutoCompleteType="FirstName" />
            </div>
            <div style="height: 15px; width: 170px; margin-left: 22px;">
                <span class="yazi"><b>
                    <%= GetGlobalResourceObject("Login", "lblPassword") %></b></span>
            </div>
            <div class="PasswordInput">
                <asp:TextBox ID="txtPassword" runat="server" CssClass="login_input" TextMode="Password" />
            </div>
            <div class="LoginBt">
                <asp:Button ID="loginButton" runat="server" CssClass="login_button" Text="<%$ Resources:Login, loginButton %>"
                    Width="188px" OnClientClick="return sahakontrol()" Font-Bold="True" OnClick="loginButton_Click"
                    Height="25px" />
            </div>
            <div style="width: 190px; margin-top: 15px; text-align: right; color: #00a7e7;">
                <asp:Image ID="flag" runat="server" />
                <asp:Label ID="mesajTxt" runat="server" />
            </div>
            <br />
        </div>
    </div>
    <br />
    <asp:HiddenField ID="HiddenAgency" runat="server" Value="Enter Agency Code" />
    <asp:HiddenField ID="HiddenUserName" runat="server" Value="Enter User Code" />
    <asp:HiddenField ID="HiddenPassword" runat="server" Value="Enter Password" />    
    <div id="dialog-message" title='<%= GetGlobalResourceObject("ResView", "lblMessage") %>'
        style="display: none;">
        <span id="messages">Message</span>
    </div>
    </form>
</body>
</html>
