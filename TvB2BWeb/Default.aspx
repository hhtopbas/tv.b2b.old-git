﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%=GetGlobalResourceObject("PageTitle", "Default") %></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="Scripts/jquery.url.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">       

        $(document).ready(function () {            
            var frame = $(this).frame;
            window.name = "TVB2B";
            $.query = $.query.load(location.href);
            Message = $.query.get('Message');
            EndSession = $.query.get('EndSession');
            CloseIFrame = $.query.get('CloseIFrame');
            var url = $.url(location.href);
            if (CloseIFrame == 'true') {
                window.close();
                self.parent.logout();
                return;
            }
            if (EndSession == 'true') {
                $('<div>' + '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>' + '</div>').dialog({
                    autoOpen: true,
                    position: {
                        my: 'center',
                        at: 'center'
                    },
                    modal: true,
                    resizable: true,
                    autoResize: true,
                    bigframe: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                            $(this).dialog("close");
                            $(this).dialog("destroy");
                            var queryStr = '';
                            if ($.query) queryStr = $.query.toString();
                            var _url = url.data.attr.base + url.data.attr.directory + "UserLogin.aspx" + queryStr;
                            window.open(_url, "TVB2B", "toolbar=no,location=no,scrollbars=yes,width=1000,height=700");
                        }
                    }
                });
            }
            else {
                var queryStr = '';
                if ($.query) queryStr = $.query.toString();
                var _url = url.data.attr.base + url.data.attr.directory + "UserLogin.aspx" + queryStr;
                window.open(_url, "TVB2B", "toolbar=no,location=no,scrollbars=yes,width=1000,height=700");
            }
        });

    </script>

</head>
<body>
    <form id="form1" runat="server">
    </form>
</body>
</html>
