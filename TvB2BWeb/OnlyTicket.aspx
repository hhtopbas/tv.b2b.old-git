﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlyTicket.aspx.cs" Inherits="OnlyTicket"
  EnableEventValidation="false" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
  TagPrefix="rjs" %>
<%@ Register Src="Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    <%= GetGlobalResourceObject("PageTitle", "OnlyTicket") %></title>

  <script src="Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery.datepick/jquery-ui-i18n.js" type="text/javascript"></script>
  <script src="Scripts/jquery.json.js" type="text/javascript"></script>
  <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
  <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>
  <script src="Scripts/jquery.textTruncate.js" type="text/javascript"></script>
  <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
  <script src="Scripts/jquery.dialogextend.js" type="text/javascript"></script>
  <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>
  <script src="Scripts/Tv.PackageSearchFilterV2.js" type="text/javascript"></script>

  <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="CSS/main.css" rel="stylesheet" type="text/css" />
  <link href="CSS/OnlyTicket.css" rel="stylesheet" type="text/css" />

  <style type="text/css">
    .special_day a { background: #ABABAB url('Images/FlightDay.gif') no-repeat !important; color: #222222 !important; }
  </style>

  <script language="javascript" type="text/javascript">
    var twoLetterISOLanguageName = '<%= twoLetterISOLanguageName %>';
    var FirstNightGreatSecondNight = '<%= GetGlobalResourceObject("OnlyTicket","FirstNightGreatSecondNight") %>';
    var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
    var lblSessionEnd = '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>';
    var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
    var PleaseSelectDepature = '<%= GetGlobalResourceObject("OnlyTicket", "PleaseSelectDepature")%>';
    var PleaseSelectArrival = '<%= GetGlobalResourceObject("OnlyTicket", "PleaseSelectArrival")%>';
    var ComboSelect = '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
    var ComboAll = '<%=GetGlobalResourceObject("LibraryResource", "ComboAll") %>';
    var titleBookTicket = '<%= GetGlobalResourceObject("BookTicket","titleBookTicket")%>';

    if (typeof window.event != 'undefined')
      document.onkeydown = function () {
        if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
          return (event.keyCode != 8);
      }
    else
      document.onkeypress = function (e) {
        if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
          return (e.keyCode != 8);
      }

    $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '</h1>';
    $(document)
        .ajaxStart($.blockUI)
        .ajaxStop($.unblockUI);

    var dayColor = [];

    function logout() {
      window.location = 'Default.aspx'; $('<div>' + lblSessionEnd + '</div>').dialog({
        autoOpen: true,
        position: {
          my: 'center',
          at: 'center'
        },
        modal: true,
        resizable: true,
        autoResize: true,
        bigframe: true,
        buttons: [{
          text: btnOK,
          text: function () {
            $(this).dialog("close");
            $(this).dialog("destroy");
            window.location = 'Default.aspx';
          }
        }]
      });
      window.setTimeout(function () { window.location = "<%=VirtualPathUtility.ToAbsolute("~/Default.aspx")%>"; }, 10000);
    }

    var NS = document.all;

    function maximize() {
      var maxW = screen.availWidth;
      var maxH = screen.availHeight;
      if (location.href.indexOf('pic') == -1) {
        if (window.opera) { } else {
          top.window.moveTo(0, 0);
          if (document.all) {
            top.window.resizeTo(maxW, maxH);
          }
          else {
            if (document.layers || document.getElementById) {
              if ((top.window.outerHeight < maxH) || (top.window.outerWidth < maxW)) {
                top.window.outerHeight = maxH; top.window.outerWidth = maxW;
              }
            }
          }
        }
      }
    }

    function setCreateria() {

      var flyDate = $("#cbFlightDay").val();
      var _flyDateFormat = $("#cbFlightDay").attr("Format");

      var params = new Object();
      params.Date = $("#cbFlightDay").datepicker('getDate') != null ? $("#cbFlightDay").datepicker('getDate').getTime() : null;
      params.OWRT = $("#fltOWRT").val();
      params.CurrCheck = $("#CurrCheck:checked").is(':checked');
      params.Departure = $("#fltDeparture").val();
      params.Arrival = $("#fltArrival").val();
      params.Days = $("#cbDays").val();
      params.ReturnDays1 = $("#cbReturnDay1").val();
      params.ReturnDays2 = $("#cbReturnDay2").val();
      params.FlgClass = $("#fltFlgClass").val();
      params.CurOption = $("#fltFlgClass").val();
      params.cur = $("#cbCurrency").val();
      params.AllFlight = $("#cbAllFlight:checked").is(':checked');
      params.FlightType = $("#cbFlyType").val();
      $.ajax({
        async: false,
        type: "POST",
        url: "OnlyTicket.aspx/setCriteria",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {

        },
        error: function (XMLHttpRequest, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(XMLHttpRequest.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function preSearch() {
      if ($("#fltDeparture").val() == '') {
        alert(PleaseSelectDepature);
        return;
      }
      if ($("#fltArrival").val() == '') {
        alert(PleaseSelectArrival);
        return;
      }

      var params = new Object();
      params.Date = $("#cbFlightDay").datepicker('getDate') != null ? $("#cbFlightDay").datepicker('getDate').getTime() : null;
      params.OWRT = $("#fltOWRT").val();
      params.CurrCheck = $("#CurrCheck:checked").is(':checked');
      params.Departure = $("#fltDeparture").val();
      params.Arrival = $("#fltArrival").val();
      params.Days = $("#cbDays").val();
      params.ReturnDays1 = $("#cbReturnDay1").val();
      params.ReturnDays2 = $("#cbReturnDay2").val();
      params.FlgClass = $("#fltFlgClass").val();
      params.CurOption = $("#fltFlgClass").val();
      params.cur = $("#cbCurrency").val();
      params.AllFlight = $("#cbAllFlight:checked").is(':checked');
      params.FlightType = $("#cbFlyType").val();
      $.ajax({
        async: false,
        type: "POST",
        url: "OnlyTicket.aspx/getSearchResult",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#gridSearch").html('');
          if (msg.hasOwnProperty('d') && msg.d != '') {
            $("#gridSearch").html(msg.d);
          }
        },
        error: function (XMLHttpRequest, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(XMLHttpRequest.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getFlightClass() {
      $.ajax({
        async: false,
        type: "POST",
        url: "OnlyTicket.aspx/getFlightClass",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#fltFlgClass").html('');
          if (msg.hasOwnProperty('d')) {
            $("#fltFlgClass").append("<option value='' >" + ComboAll + "</option>");
            $.each(msg.d, function (i) {
              $("#fltFlgClass").append("<option value='" + this.Class + "'>" + this.Name + "</option>");
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function setFlightDays() {
      $.ajax({
        async: false,
        type: "POST",
        url: "OnlyTicket.aspx/getFlightDays",
        data: '{"DepCity":"' + $("#fltDeparture").val() + '","ArrCity":"' + $("#fltArrival").val() + '","ORT":"' + $("#fltOWRT").val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var data = msg.d;
            dayColor = [];
            dayColor = data;
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getFirstFlight() {
      $.ajax({
        async: false,
        type: "POST",
        url: "OnlyTicket.aspx/getFirstFlight",
        data: '{"DepCity":"' + $("#fltDeparture").val() + '","ArrCity":"' + $("#fltArrival").val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != '') {
            var firstDay = $.json.decode(msg.d);
            if (firstDay != null) {
              $("#hfDate").val('');
              $("#hfDate").val(firstDay.CheckIn.toString());
              //var _date = new Date(Date(eval(firstDay.CheckIn)).toString());
              //$("#cbFlightDay").datepicker('setDate', _date);
                //edit for detur
              dateParts = firstDay.FlyDate.toString().match(/(\d+)/g);
              realDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
              //$('#datePicker').datepicker({ dateFormat: 'yy-mm-dd' }); // format to show
              $('#cbFlightDay').datepicker('setDate', realDate);
                //edit
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function changeArrivalCity() {
      setFlightDays();
      getFirstFlight();
    }

    function getArrival() {

      $.ajax({
        async: false,
        type: "POST",
        url: "OnlyTicket.aspx/getArrivalCitys",
        data: '{"DepCity":"' + $("#fltDeparture").val() + '","ORT":"' + $("#fltOWRT").val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#fltArrival").html('');
          if (msg.hasOwnProperty('d')) {
            $("#fltArrival").append("<option value='' >" + ComboSelect + "</option>");
            $.each(msg.d, function (i) {
              $("#fltArrival").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function changeDepartureCity() {
      getArrival();
      setFlightDays();
      getFirstFlight();
    }

    function getDeparture() {
      $.ajax({
        async: false,
        type: "POST",
        url: "OnlyTicket.aspx/getDepartureCitys",
        data: '{"ORT":"' + $("#fltOWRT").val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#fltArrival").html('');
          $("#fltDeparture").html('');
          if (msg.hasOwnProperty('d')) {
            $("#fltDeparture").append('<option value="" >' + ComboSelect + '</option>');
            $.each(msg.d, function (i) {
              $("#fltDeparture").append('<option value="' + this.RecID + '">' + this.Name + '</option>');
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function changeOWRT() {
      var oWRT = $("#fltOWRT").val();
      if (oWRT == 'OW') {
        $("#divDays").hide();
      }
      else {
        $("#divDays").show();
      }

      setFlightDays();
      getFirstFlight();
      getDeparture();
    }

    function clearFilter() {
      $("#gridSearch").html('');

      getCriterias();
      getFlightClass();
      changeOWRT();
    }

    function bookFlight(recID, ver) {
      window.scrollTo(0, 0);
      $('html').css('overflow', 'hidden');
      $("#MakeReservation").attr("src", "BookTicket" + ver + ".aspx?BookNr=" + recID);
      $("#dialog").dialog("destroy");
      $("#dialog-MakeReservation").dialog(
          {
            title: titleBookTicket,
            autoOpen: true,
            modal: true,
            width: 998,
            height: 700,
            resizable: true,
            autoResize: false,
            bigframe: true,
            close: function (event, ui) { $('html').css('overflow', 'auto'); }
          }).dialogExtend({
            "maximize": true,
            "icons": {
              "maximize": "ui-icon-circle-plus",
              "restore": "ui-icon-pause"
            }
          });
      $("#dialog-MakeReservation").dialogExtend("maximize");
      return false;
    }

    function ChangeNight1() {
      var Night1 = $("#cbReturnDay1");
      var Night2 = $("#cbReturnDay2");
      Night2.val(Night1.val());
    }

    function ChangeNight2() {
      var Night1 = $("#cbReturnDay1");
      var Night2 = $("#cbReturnDay2");
      if (parseInt(Night1.val()) > parseInt(Night2.val())) {
        alert(FirstNightGreatSecondNight);
        Night2.val(Night1.val());
      }
    }

    function cancelMakeRes() {
      $("#MakeReservation").attr("src", "");
      $("#dialog-MakeReservation").dialog("close");
    }

    function gotoResViewPage(ResNo) {
      $("#MakeReservation").attr("src", "");
      $("#dialog-MakeReservation").dialog("close");
      $('html').css('overflow', 'auto');
      window.location = 'ResView.aspx?ResNo=' + ResNo;
    }

    function paymentPage(ResNo, PaymentPageUrl) {
      $("#MakeReservation").attr("src", "");
      $("#dialog-MakeReservation").dialog("close");
      window.open(PaymentPageUrl + '?ResNo=' + ResNo, '_blank');
    }

    function getCriterias() {
      $.ajax({
        async: false,
        type: "POST",
        url: "OnlyTicket.aspx/getTicketTypeLock",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var retVal = msg.d;

            var _date1 = new Date();
            $("#cbFlightDay").datepicker('setDate', _date1);

            $("#CurOption").val(retVal.curOption);
            if (retVal.SearchAllFlight == true){
                $("#allFlight").show();
                if(retVal.SearchAllFlightDefaultValue!=undefined)
                    $("#cbAllFlight").attr("checked", retVal.SearchAllFlightDefaultValue);
            }
              
            else $("#allFlight").hide();
            if (retVal.days != null) {
              $("#cbReturnDay1").html('');
              $("#cbReturnDay2").html('');

              $.each(retVal.days, function (i) {
                if (this.toString() == "7") {
                  $("#cbReturnDay1").append("<option value='" + this.toString() + "' selected='selected'>" + this.toString() + "</option>");
                  $("#cbReturnDay2").append("<option value='" + this.toString() + "' selected='selected'>" + this.toString() + "</option>");
                }
                else {
                  $("#cbReturnDay1").append("<option value='" + this.toString() + "'>" + this.toString() + "</option>");
                  $("#cbReturnDay2").append("<option value='" + this.toString() + "'>" + this.toString() + "</option>");
                }
              });
            }

            if (retVal.expandDay != null) {
              $("#cbDays").html('');
              $.each(retVal.expandDay, function (i) {
                if (this.toString() == "1") {
                  $("#cbDays").append("<option value='" + this.toString() + "' selected='selected'>" + '(+) ' + this.toString() + "</option>");
                }
                else {
                  $("#cbDays").append("<option value='" + this.toString() + "'>" + '(+) ' + this.toString() + "</option>");
                }
              });
            }

            if (retVal.showFlyType && retVal.flyTypeList.length > 0) {
              $("#divFlyType").show();
              $("#cbFlyType").html('');
              $("#cbFlyType").append('<option value="">' + ComboAll + '</option>');
              $.each(retVal.flyTypeList, function (i) {
                $("#cbFlyType").append('<option value="' + this.RecID + '">' + this.Name + '</option>');
              });
            } else {
              $("#divFlyType").hide();
            }

            if (retVal.typeLock == true) {
              $("#cbCurrency").html('');
              $("#cbCurrency").append("<option value='" + retVal.currency + "' selected='selected'>" + retVal.currency + "</option>");
              $("#CurrCheck").attr('checked', 'checked');
              $("#divCurrency").hide();
              $("#divOWRTLabel").hide();
              $("#divOWRT").hide();
              $("#divCurrency").hide();
            }
            else {
              $("#divOWRTLabel").show();
              $("#divOWRT").show();
              $("#divCurrency").hide();
              $("#CurrCheck").hide();
              $("#cbCurrency").html('');

              if (retVal.curOption == 2) {
                $("#divCurrency").show();
                $("#CurrCheck").show();
                $.each(retVal.currencyList, function (i) {
                  if (this.toString() == retVal.currency)
                    $("#cbCurrency").append("<option value='" + this.toString() + "' selected='selected'>" + this.toString() + "</option>");
                  else $("#cbCurrency").append("<option value='" + this.toString() + "'>" + this.toString() + "</option>");
                });
                $("#cbCurrency").removeAttr('disabled');
              }
              else if (retVal.curOption == 1) {
                $("#cbCurrency").append("<option value=''></option>");
                $("#CurrCheck").removeAttr('checked');
                $("#divCurrency").hide();
              }
              else {
                $("#cbCurrency").append("<option value='" + retVal.currency + "' selected='selected'>" + retVal.currency + "</option>");
                $("#CurrCheck").attr('checked', 'checked');
                $("#divCurrency").hide();
              }
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
      getDeparture();
    }

    function reSearch(date) {
      $.ajax({
        type: "POST",
        url: "OnlyTicket.aspx/reSearch",
        data: '{"date":' + date + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {

            var _date1 = new Date(Date(eval(msg.d.CheckIn)).toString());
            $("#cbFlightDay").datepicker('setDate', _date1);

            preSearch();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function prevDate(pdate) {
      reSearch(pdate);
    }

    function nextDate(ndate) {
      reSearch(ndate);
    }

    function flightDays(date) {
      var noWeekend = $.datepicker.noWeekends(date);
      if (dayColor != null && dayColor.length > 0) {
        for (i = 0; i < dayColor.length; i++) {
          var year = date.getFullYear();
          var month = date.getMonth();
          var day = date.getDate();
          if (month == (dayColor[i].Month - 1) && day == dayColor[i].Day && year == dayColor[i].Year) {
            return [true, 'special_day', dayColor[i].Text];
          }
        }
        return [true, ''];
      }
      return [true, ''];
    }

    $(document).ready(function () {
      $.datepicker.setDefaults($.datepicker.regional[twoLetterISOLanguageName != 'en' ? twoLetterISOLanguageName : '']);

      $("#cbFlightDay").datepicker({
        showOn: "button",
        buttonImage: "Images/Calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onSelect: function (dateText, inst) {
          if (dateText != '') {
            var date2 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
            var iDate = $("#hfDate");
            if (date2) {
              iDate.val(Date.parse(date2));
            }
            else {
              iDate.val(Date.parse(new Date()));
            }
          }
        },
        minDate: new Date(),
        beforeShowDay: flightDays
      });
      maximize();
      getCriterias();
      getFlightClass();
      changeOWRT();
      setCreateria();
    });
  </script>

</head>
<body>
  <form id="form1" runat="server">
    <div class="Page">
      <tv1:Header ID="tvHeader" runat="server" />
      <tv1:MainMenu ID="tvMenu" runat="server" />
      <input id="hfDate" type="hidden" />
      <div class="Page">
        <div class="FilterArea">
          <div style="border: 1px solid #808080; max-width: 1000px; margin: 2px; text-align: left;">
            <table cellpadding="0" cellspacing="0" width="1000px">
              <tr>
                <td>
                  <div id="divOWRTAndCurr" style="width: 100%; height: 25px; vertical-align: middle; margin-top: 3px;">
                    <div id="divOWRTLabel" style="width: 150px; float: left; text-align: right; height: 25px; display: none;">
                      <span>
                        <%= GetGlobalResourceObject("OnlyTicket", "lblTicketType")%>
                                            : </span>
                    </div>
                    <div id="divOWRT" style="width: 130px; float: left; vertical-align: middle; height: 25px; display: none;">
                      <select id="fltOWRT" onchange="changeOWRT();">
                        <option value="RT" selected="selected">
                          <%= GetGlobalResourceObject("OnlyTicket", "RoundTrip")%></option>
                        <option value="OW">
                          <%= GetGlobalResourceObject("OnlyTicket", "OneWay")%></option>
                      </select>
                    </div>
                    <div id="divCurrency" style="width: 400px; float: left; vertical-align: middle; height: 25px; display: none;">
                      <input id="CurrCheck" type="checkbox" checked="checked" /><label for="CurrCheck"><%= GetGlobalResourceObject("ResView", "lblSaleCur")%>:</label>
                      <select id="cbCurrency">
                      </select>
                      <input id="CurOption" type="hidden" value="0" />
                    </div>
                    <div id="allFlight" style="width: 300px; float: left;">
                      <input id="cbAllFlight" type="checkbox" /><label for="cbAllFlight"><%= GetGlobalResourceObject("OnlyTicket", "ShowAllPossibleFlight")%></label>
                    </div>
                  </div>
                  <div id="divRoutes" style="width: 800px; height: 25px;">
                    <div id="divRouteLabel" style="width: 150px; float: left; text-align: right; vertical-align: middle; height: 25px;">
                      <span>
                        <%= GetGlobalResourceObject("OnlyTicket", "lblFlyRoute")%>
                                            : </span>
                    </div>
                    <div id="divRouteFrom" style="width: 125px; float: left; vertical-align: middle; height: 25px;">
                      <select id="fltDeparture" style="width: 120px;" onchange="changeDepartureCity();">
                      </select>
                    </div>
                    <div style="width: 30px; float: left; text-align: center; vertical-align: middle; height: 25px;">
                      -&gt;
                    </div>
                    <div id="divRouteTo" style="width: 125px; float: left; vertical-align: middle; height: 25px;">
                      <select id="fltArrival" style="width: 120px;" onchange="changeArrivalCity();">
                      </select>
                    </div>
                  </div>
                  <div id="divDate" style="width: 500px; height: 25px; clear: both;">
                    <div id="divDateLabel" style="width: 150px; float: left; text-align: right; vertical-align: middle; height: 25px;">
                      <span>
                        <%= GetGlobalResourceObject("OnlyTicket", "lblFlyDate")%>
                                            : </span>
                    </div>
                    <div id="divFlyDate1" style="width: 130px; float: left; vertical-align: middle; height: 25px;">
                      <input id="cbFlightDay" style="width: 85px;" />
                    </div>
                    <div style="width: 60px; float: left; text-align: center; vertical-align: middle; height: 25px;">
                      <select id="cbDays">
                        <option value="0">(+) 0</option>
                        <option value="1" selected="selected">(+) 1</option>
                        <option value="2">(+) 2</option>
                        <option value="3">(+) 3</option>
                      </select>
                    </div>
                  </div>
                  <div id="divFlyType" style="width: 500px; height: 25px; clear: both; display: none;">
                    <div id="divFlyTypeLabel" style="width: 150px; float: left; text-align: right; vertical-align: middle; height: 25px;">
                      <span>Flight Type : </span>
                    </div>
                    <div style="width: 250px; float: left; text-align: left; vertical-align: middle; height: 25px;">
                      <select id="cbFlyType" style="width: 99%;">
                      </select>
                    </div>
                  </div>
                  <div id="divDays" style="width: 500px; height: 25px; clear: both;">
                    <div id="divDayLabel" style="width: 150px; float: left; text-align: right; vertical-align: middle; height: 25px;">
                      <span>
                        <%= GetGlobalResourceObject("OnlyTicket", "lblReturnDay")%>
                                            : </span>
                    </div>
                    <div id="divReturnDay1" style="width: 50px; float: left; height: 22px; vertical-align: middle; height: 25px;">
                      <select id="cbReturnDay1" onchange="ChangeNight1();">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7" selected="selected">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                      </select>
                    </div>
                    <div style="width: 25px; float: left; text-align: center; vertical-align: middle; height: 25px;">
                      -&gt;
                    </div>
                    <div id="divReturnDay2" style="width: 50px; float: left; vertical-align: middle;">
                      <select id="cbReturnDay2" onchange="ChangeNight2();">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7" selected="selected">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                      </select>
                    </div>
                  </div>
                  <div id="divCls" style="width: 500px; height: 25px;">
                    <div id="divClassLabel" style="width: 150px; float: left; text-align: right; vertical-align: middle; height: 25px;">
                      <span>
                        <%= GetGlobalResourceObject("OnlyTicket", "lblSClass")%>
                                            : </span>
                    </div>
                    <div id="divClass" style="width: 120px; float: left; vertical-align: middle; height: 25px;">
                      <select id="fltFlgClass">
                      </select>
                    </div>
                  </div>
                </td>
              </tr>
            </table>
            <asp:HiddenField ID="hfFlyDate" runat="server" />
            <table style="font-family: Tahoma; font-size: small;">
              <tr>
                <td>
                  <input type="button" name="btnSearch" value='<%= GetGlobalResourceObject("LibraryResource", "btnSearch") %>'
                    style="min-width: 100px;" onclick="preSearch()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />&nbsp;
                  <input type="button" name="btnClear" value='<%= GetGlobalResourceObject("LibraryResource", "btnClear") %>'
                    style="min-width: 100px;" onclick="clearFilter()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                </td>
              </tr>
            </table>
          </div>
        </div>
        <div class="Content">
          <span id="gridSearch"></span>
        </div>
      </div>
      <div class="Footer">
        <tv1:Footer ID="tvfooter" runat="server" />
      </div>
    </div>
    <div id="dialog-MakeReservation" title="Book Ticket" style="display: none; text-align: center;">
      <iframe id="MakeReservation" runat="server" height="100%" width="966px" frameborder="0"
        style="clear: both; text-align: center;"></iframe>
    </div>
  </form>
</body>
</html>
