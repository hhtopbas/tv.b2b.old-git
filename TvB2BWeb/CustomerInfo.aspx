﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CustomerInfo.aspx.cs" Inherits="CustomerInfo" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
  TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    <%= GetGlobalResourceObject("PageTitle", "CustomerInfo")%></title>

  <script src="Scripts/jquery.min.js" type="text/javascript"></script>

  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

  <script src="Scripts/jquery.json.js" type="text/javascript"></script>

  <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

  <script src="Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>

  <script src="Scripts/linq.js" type="text/javascript"></script>

  <script src="Scripts/jquery.linq.js" type="text/javascript"></script>

  <script src="Scripts/checkDate.js" type="text/javascript"></script>

  <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>

  <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="CSS/confirm.css" rel="stylesheet" type="text/css" />
  <link href="CSS/ResCustInfo.css" rel="stylesheet" type="text/css" />
  <link href="CSS/ResCustInfoMem.css" rel="stylesheet" type="text/css" />

  <script language="javascript" type="text/javascript">

    var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';

    if (typeof window.event != 'undefined')
      document.onkeydown = function () {
        if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
          return (event.keyCode != 8);
      }
    else
      document.onkeypress = function (e) {
        if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
          return (e.keyCode != 8);
      }

    function logout() {
      self.parent.logout();
    }

    function showAlert(msg) {
      $(function () {
        $("#messages").html(msg);
        $("#dialog").dialog("destroy");
        $("#dialog-message").dialog({
          modal: true,
          buttons: [{
            text: btnOK,
            click: function () {
              $(this).dialog('close');
              return true;
            }
          }]
        });
      });
    }

    $(function () {
      $("#Tabs").tabs({ remote: true, selected: 1 });
    });


    function exit(source) {
      var data = '';
      data += '<';
      data += '|EdtTitleCode|:|' + $("#edtTitleCode").val() + '|';
      data += ',|EdtSurname|:|' + $("#edtSurname").val() + '|';
      data += ',|EdtSurnameL|:|' + ($("#edtSurnameL").length > 0 ? $("#edtSurnameL").val() : '') + '|';
      data += ',|EdtName|:|' + $("#edtName").val() + '|';
      data += ',|EdtNameL|:|' + ($("#edtNameL").length > 0 ? $("#edtNameL").val() : '') + '|';
      data += ',|EdtBirtday|:|' + $("#edtBirtday").val() + '|';
      data += ',|EdtIDNo|:|' + $("#edtIDNo").val() + '|';
      data += ',|EdtPassSerie|:|' + $("#edtPassSerie").val() + '|';
      data += ',|EdtPassNo|:|' + $("#edtPassNo").val() + '|';
      data += ',|EdtPassIssueDate|:|' + $("#edtPassIssueDate").val() + '|';
      data += ',|EdtPassExpDate|:|' + $("#edtPassExpDate").val() + '|';
      data += ',|EdtPassGiven|:|' + $("#edtPassGiven").val() + '|';

      data += ',|CTitle|:|' + $("#CTitle").val() + '|';
      data += ',|CName|:|' + $("#CName").val() + '|';
      data += ',|CSurName|:|' + $("#CSurName").val() + '|';
      data += ',|MobTel|:|' + ($("#MobTel").val() != '' ? $("#MobTelCountryCode").text() + $("#MobTel").val() : $("#MobTel").val()) + '|';
      data += ',|ContactAddr|:|' + $("#ContactAddr").val() + '|';
      data += ',|InvoiceAddr|:|' + $("#InvoiceAddr").val() + '|';
      data += ',|AddrHome|:|' + $("#AddrHome").val() + '|';
      data += ',|AddrHomeCity|:|' + $("#AddrHomeCity").val() + '|';
      data += ',|AddrHomeCountry|:|' + $("#AddrHomeCountry").val() + '|';
      data += ',|AddrHomeCountryCode|:|' + $("#AddrHomeCountryCode").val() + '|';
      data += ',|AddrHomeZip|:|' + $("#AddrHomeZip").val() + '|';
      data += ',|AddrHomeTel|:|' + ($("#AddrHomeTel").val() != '' ? $("#AddrHomeTelCountryCode").text() + $("#AddrHomeTel").val() : $("#AddrHomeTel").val()) + '|';
      data += ',|AddrHomeFax|:|' + ($("#AddrHomeFax").val() != '' ? $("#AddrHomeFaxCountryCode").text() + $("#AddrHomeFax").val() : $("#AddrHomeFax").val()) + '|';
      data += ',|AddrHomeEmail|:|' + $("#AddrHomeEmail").val() + '|';
      data += ',|HomeTaxOffice|:|' + $("#HomeTaxOffice").val() + '|';
      data += ',|HomeTaxAccNo|:|' + $("#HomeTaxAccNo").val() + '|';
      data += ',|WorkFirmName|:|' + $("#WorkFirmName").val() + '|';
      data += ',|AddrWork|:|' + $("#AddrWork").val() + '|';
      data += ',|AddrWorkCity|:|' + $("#AddrWorkCity").val() + '|';
      data += ',|AddrWorkCountry|:|' + $("#AddrWorkCountry").val() + '|';
      data += ',|AddrWorkCountryCode|:|' + $("#AddrWorkCountryCode").val() + '|';
      data += ',|AddrWorkZip|:|' + $("#AddrWorkZip").val() + '|';
      data += ',|AddrWorkTel|:|' + ($("#AddrWorkTel").val() != '' ? $("#AddrWorkTelCountryCode").text() + $("#AddrWorkTel").val() : $("#AddrWorkTel").val()) + '|';
      data += ',|AddrWorkFax|:|' + ($("#AddrWorkFax").val() != '' ? $("#AddrWorkFaxCountryCode").text() + $("#AddrWorkFax").val() : $("#AddrWorkFax").val()) + '|';
      data += ',|AddrWorkEMail|:|' + $("#AddrWorkEMail").val() + '|';
      data += ',|WorkTaxOffice|:|' + $("#WorkTaxOffice").val() + '|';
      data += ',|WorkTaxAccNo|:|' + $("#WorkTaxAccNo").val() + '|';
      data += ',|Bank|:|' + ($("#Bank").length > 0 ? $("#Bank").val() : '') + '|';
      data += ',|BankAccNo|:|' + ($("#BankAccNo").length > 0 ? $("#BankAccNo").val() : '') + '|';
      data += ',|BankIBAN|:|' + ($("#BankIBAN").length > 0 ? $("#BankIBAN").val() : '') + '|';
      data += ',|CustNo|:|' + $("#CustNo").val() + '|';
      data += ',|LCID|:|' + $("#hfLCID").val() + '|';
      data += '>';

      window.close;
      self.parent.returnCustInfoEdit(data.replace(/\\/g, '/'), source);

    }

    function getFormData(custNo, onlyView) {
      $.ajax({
        type: "POST",
        url: "CustomerInfo.aspx/getFormData",
        data: '{"custNo":' + custNo + ',"OnlyView":' + onlyView + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var formData = $.json.decode(msg.d);
          var dateFormat = formData.DateFormat;
          $("#divResCust").html('');
          var divResCustHtml = formData.ResCust.replace(/!/g, '"').replace(/~/g, '\'');
          $("#divResCust").html(divResCustHtml);
          $(".formatDate").mask(formData.DateMask);
          var resCustInfo = $.json.decode('{' + formData.ResCustInfo.replace(/!/g, '"') + '}');
          $("#dateFormat").val(dateFormat);

          $("#divContact").html('');
          $("#divContact").html(resCustInfo.Contact.replace(/#/g, '"').replace(/~/g, '\''));
          $("#divHomeAddress").html('');
          $("#divHomeAddress").html(resCustInfo.HomeAddr.replace(/#/g, '"').replace(/~/g, '\''));
          $("#divWorkAddress").html('');
          $("#divWorkAddress").html(resCustInfo.WorkAddr.replace(/#/g, '"').replace(/~/g, '\''));
          $("#divBankInfo").html('');
          if (resCustInfo.Bank != '')
            $("#divBankInfo").html(resCustInfo.Bank.replace(/#/g, '"').replace(/~/g, '\''));
          else
            $("#liBankInfo").hide();
          if ($("#mob_phone_Mask").val() != '') {
            $("#MobTel").mask($("#mob_phone_Mask").val());
          }
          if ($("#phone_Mask").val() != '') {
            $("#AddrHomeTel").mask($("#phone_Mask").val());
            $("#AddrHomeFax").mask($("#phone_Mask").val());
            $("#AddrWorkTel").mask($("#phone_Mask").val());
            $("#AddrWorkFax").mask($("#phone_Mask").val());
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showAlert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function homeCountryChanged() {
      var country = $("#AddrHomeCountry").val();
      var countryList = $("#countryList").val();

      var queryResult = $.Enumerable.From($.json.decode(countryList))
                         .Where(function (x) { return x.CountryNameL == country })
                         .Select(function (x) { return { 'CountryPhoneCode': x.CountryPhoneCode, 'PhoneMask': x.PhoneMask, 'PhoneMaskMob': x.PhoneMaskMob, 'IntCode': x.IntCode } })
                         .First();
      if (queryResult != '') {
        $("#MobTelCountryCode").text('+' + queryResult.CountryPhoneCode);
        $("#AddrHomeTelCountryCode").text('+' + queryResult.CountryPhoneCode);
        $("#AddrHomeFaxCountryCode").text('+' + queryResult.CountryPhoneCode);
        $("#phone_Mask").val(queryResult.PhoneMask);
        $("#mob_phone_Mask").val(queryResult.PhoneMaskMob);

        $("#AddrHomeCountryCode").val(queryResult.IntCode)
      }
    }

    function workCountryChanged() {
      var country = $("#AddrWorkCountry").val();
      var countryList = $("#countryList").val();

      var queryResult = $.Enumerable.From($.json.decode(countryList))
                         .Where(function (x) { return x.CountryName == country })
                         .Select(function (x) { return { 'CountryPhoneCode': x.CountryPhoneCode, 'PhoneMask': x.PhoneMask, 'PhoneMaskMob': x.PhoneMaskMob, 'IntCode': x.IntCode } })
                         .First();
      if (queryResult != '') {
        $("#AddrWorkTelCountryCode").text('+' + queryResult.CountryPhoneCode);
        $("#AddrWorkFaxCountryCode").text('+' + queryResult.CountryPhoneCode);
        $("#AddrWorkCountryCode").val(queryResult.IntCode)
      }
    }

    function onLoad() {
      $.query = $.query.load(location.href);
      var CustNo = $.query.get('CustNo');
      $("#CustNo").val(CustNo);
      var onlyView = $.query.get('onlyView');
      getFormData(CustNo, onlyView != "undefined" ? onlyView : false);
    }

  </script>

  <style type="text/css">
        </style>
</head>
<body onload="javascript:onLoad();">
  <form id="form1" runat="server">
    <input id="CustNo" type="hidden" />
    <input id="dateFormat" type="hidden" />
    <br />
    <div id="divCustomersInfo" style="width: 740px;">
      <div class="divCustomers">
        <div id="divResCust">
        </div>
      </div>
      <div id="divInfo">
        <div id="Tabs">
          <ul>
            <li id="liContact"><a href="#divContact">
              <%= GetGlobalResourceObject("LibraryResource", "lblContactPerson") %></a></li>
            <li id="liHomeAddress"><a href="#divHomeAddress">
              <%= GetGlobalResourceObject("LibraryResource", "lblHomeAddress")%></a></li>
            <li id="liWorkAddress"><a href="#divWorkAddress">
              <%= GetGlobalResourceObject("LibraryResource", "lblWorkAddress")%></a></li>
            <li id="liBankInfo"><a href="#divBankInfo">
              <%= GetGlobalResourceObject("LibraryResource", "lblBankInfo")%></a></li>
          </ul>
          <div id="divContact">
          </div>
          <div id="divHomeAddress">
          </div>
          <div id="divWorkAddress">
          </div>
          <div id="divBankInfo">
          </div>
        </div>
        <div id="divButtons" style="width: 740px; text-align: center;">
          <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>'
            onclick="exit('save');" style="width: 110px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
          &nbsp;&nbsp;
                <input id="btnCancel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>'
                  onclick="exit('cancel');" style="width: 110px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
        </div>
      </div>
    </div>
    <div id="dialog-message" title='<%= GetGlobalResourceObject("ResView", "lblMessage") %>'
      style="display: none;">
      <span id="messages">Message</span>
    </div>
  </form>
</body>
</html>
