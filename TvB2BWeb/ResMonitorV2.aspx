﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResMonitorV2.aspx.cs" Inherits="ResMonitorV2"
    EnableEventValidation="false" %>

<%@ Register Src="~/Comment.ascx" TagName="ResComment" TagPrefix="resCom1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "ResMonitor") %></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.datepick/jquery-ui-i18n.js" type="text/javascript"></script>
    <script src="Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="Scripts/jquery.simplemodal.js" type="text/javascript"></script>
    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
    <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>
    <script src="Scripts/NumberFormat154.js" type="text/javascript"></script>
    <script src="Scripts/jquery.textTruncate.js" type="text/javascript"></script>
    <script src="Scripts/jquery.dialogextend.js" type="text/javascript"></script>
    <script src="Scripts/datatable/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="Scripts/datatable/jquery.dataTables.Pagination.ListBox.js" type="text/javascript"></script>
    <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>
    <script src="Scripts/stylesheetToggle.js" type="text/javascript"></script>
    <script src="Scripts/mustache.js" type="text/javascript"></script>
    <script type="text/javascript" src="Scripts/moment.min.js"></script>
    <link id="jQueryUIPath" href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="CSS/ResMonitor.css" rel="stylesheet" type="text/css" />
    <link href="CSS/ResMonitorV2.css?v=1" rel="stylesheet" type="text/css" />
    <link href="CSS/datatable/jquery.dataTables_themeroller.css" rel="stylesheet" />
    <style type="text/css">
        
    </style>

    <script language="javascript" type="text/javascript">

        var twoLetterISOLanguageName = '<%= twoLetterISOLanguageName %>';
        var tmpPath = '<%= tmpPath %>';
        var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
        var comboAll = '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>';
        var lblSessionEnd = '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>';
        var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
        var btnClear = '<%= GetGlobalResourceObject("LibraryResource", "btnClear") %>';
        var btnSend = '<%= GetGlobalResourceObject("LibraryResource", "btnSend") %>';
        var lblNotSaved = '<%= GetGlobalResourceObject("LibraryResource","lblNotSaved").ToString() %>';
        var btnRefresh = '<%= GetGlobalResourceObject("LibraryResource", "btnRefresh") %>';
        var btnClose = '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>';

        var sProcessing = '<%= GetGlobalResourceObject("LibraryResource", "sProcessing") %>';
        var sLengthMenu = '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>';
        var sZeroRecords = '<%= GetGlobalResourceObject("LibraryResource", "sZeroRecords") %>';
        var sEmptyTable = '<%= GetGlobalResourceObject("LibraryResource", "sEmptyTable") %>';
        var sInfo = '<%= GetGlobalResourceObject("LibraryResource", "sInfo") %>';
        var sInfoEmpty = '<%= GetGlobalResourceObject("LibraryResource", "sInfoEmpty") %>';
        var sInfoFiltered = '<%= GetGlobalResourceObject("LibraryResource", "sInfoFiltered") %>';
        var sInfoPostFix = '<%= GetGlobalResourceObject("LibraryResource", "sInfoPostFix") %>';
        var sSearch = '<%= GetGlobalResourceObject("LibraryResource", "sSearch") %>';
        var sUrl = '<%= GetGlobalResourceObject("LibraryResource", "sUrl") %>';
        var sFirst = '<%= GetGlobalResourceObject("LibraryResource", "sFirst") %>';
        var sPrevious = '<%= GetGlobalResourceObject("LibraryResource", "sPrevious") %>';
        var sNext = '<%= GetGlobalResourceObject("LibraryResource", "sNext") %>';
        var sLast = '<%= GetGlobalResourceObject("LibraryResource", "sLast") %>';
        var sPage = '<%= GetGlobalResourceObject("LibraryResource", "sPage") %>';

        var lblCustomerSurname = '<%= GetGlobalResourceObject("ResMonitor", "lblCustomerSurname")%>';
        var lblSurname = '<%= GetGlobalResourceObject("ResMonitor", "lblSurname")%>';

        var txtMessage = '<%= (resComment.FindControl("txtMessage")).ClientID %>';
        var searchCustomerOptions = '<%= SearchCustomerOptions%>';

        $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '<\/h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);


        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function logout() {
            $('<div>' + lblSessionEnd + '</div>').dialog({
                autoOpen: true,
                position: {
                    my: 'center',
                    at: 'center'
                },
                modal: true,
                resizable: true,
                autoResize: true,
                bigframe: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog("close");
                        $(this).dialog("destroy");
                        window.location = 'Default.aspx';
                    }
                }]
            });
            window.setTimeout(function () { window.location = "<%=VirtualPathUtility.ToAbsolute("~/Default.aspx")%>"; }, 10000);
        }

        function toJSONDate(date) {
            if (date == null) return null;
            else return '/Date(' + date.getTime() + ')/';
        }

        function showAlert(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: [{
                        text: btnOK,
                        click: function () {
                            $(this).dialog('close');
                        }
                    }]
                });
            });
        }

        var NS = document.all;

        function maximize() {
            var maxW = screen.availWidth;
            var maxH = screen.availHeight;
            if (location.href.indexOf('pic') == -1) {
                if (window.opera) { } else {
                    top.window.moveTo(0, 0);
                    if (document.all) {
                        top.window.resizeTo(maxW, maxH);
                    }
                    else {
                        if (document.layers || document.getElementById) {
                            if ((top.window.outerHeight < maxH) || (top.window.outerWidth < maxW)) {
                                top.window.outerHeight = maxH; top.window.outerWidth = maxW;
                            }
                        }
                    }
                }
            }
        }

        function changeOnlyActiveUsers() {
            if ($("#fltOnlyActiveUsers").attr("checked") == 'checked') {
                $("#fltAgencyUser option[stat='0']").attr('disabled', 'disabled');
            }
            else {
                $("#fltAgencyUser option").removeAttr('disabled');
            }
        }


        function getBonus() {
            $.ajax({
                type: "POST",
                data: '{}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResMonitorV2.aspx/getTotalBonus',
                success: function (msg) {
                    $("#divBonus").html('');
                    $("#divBonus").html(msg.d);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function openRow(rowID) {
            var ocRow = $("#ocRowId" + rowID).val();
            if (ocRow == '1') {
                $("#ocRowId" + rowID).val('0');
                $("#openDetail" + rowID).attr("src", "Images/infoOpen.gif");
                $("#rowOtherInfo" + rowID).hide();
            }
            else {
                $("#ocRowId" + rowID).val('1');
                $("#openDetail" + rowID).attr("src", "Images/infoClose.gif");
                $("#rowOtherInfo" + rowID).show();
            }
        }

        function ShowDraftClick() {
            if ($("#cbShowDraft").attr("checked") == "checked") {
                $("#rbOptionControlDiv").hide();
            }
            else {
                $("#rbOptionControlDiv").show();
            }
        }

        function ShowPxmResClick() {
            if ($("#cbShowPxmRes").attr("checked") == "checked") {

            }
            else {

            }
        }

        function showResNote(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: [{
                        text: btnOK,
                        click: function () {
                            $(this).dialog('close');
                        }
                    }]
                });
            });
        }

        function showCommentDiv(resNo, userID, ci) {
            $(function () {
                $("#dialog").dialog("destroy");
                $("#dialog-resComment").dialog({
                    modal: true,
                    height: 500,
                    width: 820,
                    buttons: [{
                        text: btnClear,
                        click: function () {
                            var comment = document.getElementById(txtMessage);
                            comment.value = "";
                            return true;
                        }
                    }, {
                        text: btnSend,
                        click: function () {
                            var comment = document.getElementById(txtMessage);
                            if (comment.value != '') {
                                var paraList = '{"ResNo":"' + resNo + '","Comment":"' + comment.value + '","UserID":"' + userID + '","lcID":' + ci + '}';
                                $.ajax({
                                    type: "POST",
                                    data: paraList,
                                    dataType: "json",
                                    contentType: "application/json; charset=utf-8",
                                    url: 'ResMonitorV2.aspx/setCommentData',
                                    success: function (msg) {
                                        $('#lblCommentHeader').html(resNo);
                                        $('#gridComment').html('');
                                        $('#gridComment').html(msg.d);
                                        comment.value = '';
                                    },
                                    error: function (xhr, msg, e) {
                                        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                            showAlert(lblNotSaved);
                                    },
                                    statusCode: {
                                        408: function () {
                                            logout();
                                        }
                                    }
                                });
                            }
                            return true;
                        }
                    }, {
                        text: btnRefresh,
                        click: function () {
                            var paraList = '{"ResNo":"' + resNo + '","UserID":"' + userID + '","lcID":' + ci + '}';
                            $.ajax({
                                type: "POST",
                                data: paraList,
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                url: 'ResMonitorV2.aspx/getCommentData',
                                success: function (msg) {
                                    $('#lblCommentHeader').html(resNo);
                                    $('#gridComment').html('');
                                    $('#gridComment').html(msg.d);
                                },
                                error: function (xhr, msg, e) {
                                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                        showAlert(xhr.responseText);
                                },
                                statusCode: {
                                    408: function () {
                                        logout();
                                    }
                                }
                            });
                            return true;
                        }
                    }, {
                        text: btnClose,
                        click: function () {
                            $(this).dialog('close');
                            return false;
                        }
                    }]
                });
                $('#dialog-resComment').live("dialogclose", function () {
                    if ($(this).find("#commnetCount").val() == '0') {
                        $("#img_" + resNo).html('');
                        $("#img_" + resNo).html('&nbsp;');
                    }
                });
            });
        }

        function readAllComment(resNo, userID, ci) {
            var paraList = '{"ResNo":"' + resNo + '","UserID":"' + userID + '","RecID":-1,"lcID":' + ci + '}';
            $.ajax({
                type: "POST",
                data: paraList,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResMonitorV2.aspx/setReadCommentData',
                success: function (msg) {
                    var paraList = '{"ResNo":"' + resNo + '","UserID":"' + userID + '","lcID":' + ci + '}';
                    $.ajax({
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: 'ResMonitorV2.aspx/getCommentData',
                        success: function (msg) {
                            $('#lblCommentHeader').html(resNo);
                            $('#gridComment').html(msg.d);
                            showCommentDiv(resNo, userID, ci);
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                showAlert(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function showResComment(resNo, userID, ci) {
            if ($("#hfCustomRegID").val() == '0855101') {
                readAllComment(resNo, userID, ci);
            }
            else {
                var paraList = '{"ResNo":"' + resNo + '","UserID":"' + userID + '","lcID":' + ci + '}';
                $.ajax({
                    type: "POST",
                    data: paraList,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    url: 'ResMonitorV2.aspx/getCommentData',
                    success: function (msg) {
                        $('#lblCommentHeader').html(resNo);
                        $('#gridComment').html(msg.d);
                        showCommentDiv(resNo, userID, ci);
                    },
                    error: function (xhr, msg, e) {
                        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                            showAlert(xhr.responseText);
                    },
                    statusCode: {
                        408: function () {
                            logout();
                        }
                    }
                });
            }
        }

        function readComment(resNo, userID, recID, ci) {
            var paraList = '{"ResNo":"' + resNo + '","UserID":"' + userID + '","RecID":' + recID + ',"lcID":' + ci + '}';
            $.ajax({
                type: "POST",
                data: paraList,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResMonitorV2.aspx/setReadCommentData',
                success: function (msg) {
                    $('#lblCommentHeader').html(resNo);
                    $('#gridComment').html('');
                    $('#gridComment').html(msg.d);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(lblNotSaved);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function showCommissionInvoiceDiv(msg) {
            $('#commissionInvoice').html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-commissionInvoice").dialog({
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                        return false;
                    }
                }]
            });
        }

        function showCommissionInvoice(resNo, ci) {
            //paymentPlan dialog-resComment
            var paraList = '{"ResNo":"' + resNo + '", "lcID":' + ci + '}';
            $.ajax({
                type: "POST",
                data: paraList,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResMonitorV2.aspx/getCommisionInvoices',
                success: function (msg) {
                    showCommissionInvoiceDiv(msg.d);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showAlert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function showPaymentPlanDiv(msg) {
            $(function () {
                $('#paymentPlan').html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-paymentPlan").dialog({
                    modal: true,
                    buttons: [{
                        text: btnOK,
                        click: function () {
                            $(this).dialog('close');
                            return false;
                        }
                    }]
                });
            });
        }

        function showPaymentPlan(resNo, ci) {
            var paraList = '{"ResNo":"' + resNo + '", "lcID":' + ci + '}';
            $.ajax({
                type: "POST",
                data: paraList,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'Services/ResMonitorServices.asmx/getPaymentPlanData',
                success: function (msg) {
                    showPaymentPlanDiv(msg.d);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showAlert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function showResView(ResNo) {
            $.ajax({
                type: "POST",
                data: '{"ResNo":"' + ResNo + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResMonitorV2.aspx/reservationIsBusy',
                success: function (msg) {
                    if (msg.d != '') {
                        showAlert(msg.d);
                    } else {
                        window.location = $("#basePageUrl").val() + "ResView.aspx?ResNo=" + ResNo;
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function b2cRes() {
            $("#forB2CRes").val('2');
        }

        function makeReservation(isV2) {
            isV2 = isV2 == undefined ? "" : "V2";
            $('html').css('overflow', 'hidden');
            $("#MakeReservation").attr("src", 'MakeReservation' + isV2 + '.aspx' + ($("#forB2CRes").val() == '1' ? '?forb2c=1' : ''));
            $("#dialog").dialog("destroy");
            $("#dialog-MakeReservation").dialog(
                {
                    autoOpen: true,
                    modal: true,
                    minWidth: 1000,
                    minHeight: 600,
                    resizable: true,
                    autoResize: false,
                    bigframe: true,
                    close: function (event, ui) {
                        $('html').css('overflow', 'auto');
                        createFilterData(false);
                        btnFilterClick();
                    }
                })
                .dialogExtend({
                    "maximize": true,
                    "icons": {
                        "maximize": "ui-icon-circle-plus",
                        "restore": "ui-icon-pause"
                    }
                });
            $("#dialog-MakeReservation").dialogExtend("maximize");
        }

        function cancelMakeRes() {
            $("#dialog-MakeReservation").dialog("close");
            createFilterData(false);
        }

        function gotoResViewPage(ResNo) {
            $("#MakeReservation").attr("src", "");
            $("#dialog-MakeReservation").dialog("close");
            $('html').css('overflow', 'auto');
            window.location = 'ResView.aspx?ResNo=' + ResNo;
        }

        function paymentPage(ResNo) {
            $("#MakeReservation").attr("src", "");
            $("#dialog-MakeReservation").dialog("close");
            window.open('Payments/BeginPayment.aspx?ResNo=' + ResNo, '_blank');
        }

        function showListBonus(_html) {
            $("#divBonusList").html('');
            $("#divBonusList").html(_html);
            $(function () {
                $("#dialog").dialog("destroy");
                $("#dialog-bonusList").dialog({
                    modal: true,
                    height: 500,
                    width: 820,
                    buttons: [{
                        text: btnOK,
                        click: function () {
                            $("#dialog-bonusList").dialog("close");
                            return false;
                        }
                    }]
                });
            });
        }

        function showBonusList(list) {
            $.ajax({
                type: "POST",
                data: '{"listType":"' + list + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResMonitorV2.aspx/getBonusList',
                success: function (msg) {
                    showListBonus(msg.d);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showAlert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function exportExcel() {
            $.ajax({
                type: "POST",
                data: '{}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResMonitorV2.aspx/exportExcel',
                success: function (msg) {
                    if (msg.d != '') {
                        window.open(msg.d);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showAlert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function showExportHtml(_html) {
            window.scrollTo(0, 0);
            $("#exportDiv").html('');
            $("#exportDiv").html(_html);
            $("#dialog").dialog("destroy");
            $("#dialog-export").dialog({
                modal: true,
                buttons: [{
                    text: btnClose,
                    click: function () {
                        $("#dialog-export").dialog("close");
                        return false;
                    }
                }]
            })
            .dialogExtend({
                "maximize": true,
                "icons": {
                    "maximize": "ui-icon-circle-plus",
                    "restore": "ui-icon-pause"
                }
            });
            $("#dialog-export").dialogExtend("maximize");
        }

        function exportHtml() {
            $.ajax({
                type: "POST",
                data: '{}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResMonitorV2.aspx/exportHtml',
                success: function (msg) {
                    if (msg.d != '') {
                        showExportHtml(msg.d);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showAlert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function setDepCity(data) {
            $("#fltDepCity").html('');
            var comboData = '';
            comboData += '<option value="">' + comboAll + '<\/option>';
            var depCityData = data.DepCityData;
            $.each(depCityData, function (i) {
                comboData += '<option value="' + this.Code + '">' + this.Name + '<\/option>';
            });
            $("#fltDepCity").html(comboData);
            if (data.Filter.DepCity != null && data.Filter.DepCity != '') $("#fltDepCity").val(data.Filter.DepCity);
        }

        function setArrCity(data) {
            $("#fltArrCity").html('');
            $("#fltArrCity").append("<option value=''>" + comboAll + "<\/option>");
            $.each(data.ArrCityData, function (i) {
                $("#fltArrCity").append("<option value='" + this.Code + "'>" + this.Name + "<\/option>");
            });
            if (data.Filter.ArrCity != null && data.Filter.ArrCity != '') $("#fltArrCity").val(data.Filter.ArrCity);
        }

        function setHolpack(data) {
            $("#fltHolPack").html('');
            $("#fltHolPack").append("<option value=''>" + comboAll + "<\/option>");
            $.each(data.HolPackData, function (i) {
                $("#fltHolPack").append("<option value='" + this.Code + "'>" + this.Name + "<\/option>");
            });
            if (data.Filter.HolPack != null && data.Filter.HolPack != '') $("#fltHolPack").val(data.Filter.HolPack)
        }

        function setAgencyOffice(data) {
            $("#fltAgencyOffice").html('');
            $("#fltAgencyOffice").data('UserList', data.AgencyUserData);
            $("#fltAgencyOffice").data('User', data.Filter.AgencyUser);
            $("#fltAgencyOffice").data('ShowAllRes', data.Filter.ShowAllRes);

            $("#fltAgencyOffice").append("<option value=''>" + comboAll + "<\/option>");
            $.each(data.AgencyOfficeData, function (i) {
                $("#fltAgencyOffice").append("<option value='" + this.Code + "'>" + this.Name + "<\/option>");
            });
            if (data.Filter.AgencyOffice != null && data.Filter.AgencyOffice != '') {
                $("#fltAgencyOffice").val(data.Filter.AgencyOffice);
            }
        }

        function changeAgencyOffice(agencyOffice) {
            var userList = $("#fltAgencyOffice").data('UserList');
            var user = $("#fltAgencyOffice").data('User');
            var showAllRes = $("#fltAgencyOffice").data('ShowAllRes');
            var activeUserList = [];
            if (agencyOffice != '') {
                $.each(userList, function (i) {
                    if (this.ParentCode == agencyOffice)
                        activeUserList.push(this);
                });
            }
            else {
                activeUserList = userList;
            }

            if (agencyOffice == '') {
                setAgencyUser(activeUserList, '', '', showAllRes);
            } else {
                setAgencyUser(activeUserList, user, agencyOffice, showAllRes);
            }
        }

        function changeAgencyUser() {
            var userCode = $("#fltAgencyUser :selected").val();
            var agencyCode = $("#fltAgencyUser :selected").attr("agencyCode");
            if (userCode != '') {
                $("#fltAgencyOffice").val(agencyCode);
            }
        }

        function setAgencyUser(data, AgencyUser, AgencyID, ShowAllRes) {
            $("#fltAgencyUser").html('');
            $("#fltAgencyUser").append("<option value='' stat='' agencyCode=''>" + comboAll + "<\/option>");
            $.each(data, function (i) {
                if (AgencyID == '') {
                    $("#fltAgencyUser").append("<option value='" + this.Code + "' stat='" + (this.status ? '1' : '0') + "' agencyCode='" + this.ParentCode + "'>" + this.Name + (this.status ? "" : " (disabled)") + "<\/option>");
                } else {
                    if (AgencyUser == this.Code && AgencyID == '') {
                        $("#fltAgencyUser").append("<option value='" + this.Code + "' stat='" + (this.status ? '1' : '0') + "' agencyCode='" + this.ParentCode + "' selected='selected'>" + this.Name + (this.status ? "" : " (disabled)") + "<\/option>");
                    } else {
                        if (AgencyUser == this.Code && AgencyID == this.ParentCode) {
                            $("#fltAgencyUser").append("<option value='" + this.Code + "' stat='" + (this.status ? '1' : '0') + "' agencyCode='" + this.ParentCode + "' selected='selected'>" + this.Name + (this.status ? "" : " (disabled)") + "<\/option>");
                        } else {
                            $("#fltAgencyUser").append("<option value='" + this.Code + "' stat='" + (this.status ? '1' : '0') + "' agencyCode='" + this.ParentCode + "'>" + this.Name + (this.status ? "" : " (disabled)") + "<\/option>");
                        }
                    }
                }
            });

            if (!ShowAllRes)
                $("#fltAgencyUser").removeAttr("disabled");
            else $("#fltAgencyUser").attr("disabled", "disabled");

            changeOnlyActiveUsers();
            changeAgencyUser();
        }

        function setCurrency(data) {
            $("#fltCurrency").html('');
            var comboData = '<option value="">' + comboAll + '<\/option>';
            $.each(data.CurrencyData, function (i) {
                comboData += '<option value="' + this.Code + '">' + this.Name + '<\/option>';
            });
            $("#fltCurrency").html(comboData);

            if (data.Filter.Currency != null && data.Filter.Currency != '') {
                $("#fltCurrency").val(data.Filter.Currency);
            }
        }

        var oTable;

        function showResDetailsType1(tmpHtml, resNo) {
            var sOut = '';
            $.ajax({
                async: false,
                type: "POST",
                data: '{"resNo":"' + resNo + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResMonitorV2.aspx/createGridDetailType1',
                success: function (msg) {
                    if (msg.d != null && msg.d != '') {
                        data = msg.d;
                        sOut = Mustache.render(tmpHtml, data);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showAlert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
            return sOut;
        }

        function fnFormatDetailsType1(nTr, oTable) {
            var aData = oTable.fnGetData(nTr);
            var _resno = $(aData[0]);
            var resNo = _resno.attr("resno");
            var sOut = '';
            $.ajax({
                async: false,
                type: "POST",
                data: '{"resNo":"' + resNo + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResMonitorV2.aspx/createGridDetailType1',
                success: function (msg) {
                    if (msg.d != null && msg.d != '') {
                        sOut = msg.d;
                    }
                    /*
                    if (msg.d != null && msg.d != '') {
          
                      data = msg.d;
                      sOut = Mustache.render(tmpHtml, data);
                    }
                    */
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showAlert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
            return sOut;
            /*
            $.get(tmpPath + 'ResMonTemplateType1.html?v=201512011410', function (html) {
              sOut = showResDetailsType1(html, resNo);
              return sOut;
            }).fail(function (jqxhr, textStatus, error) {
              var err = textStatus + ', ' + error;
              showAlert("Request Failed: " + err);
              return sOut;
            });
            */
        }

        function fnFormatDetails(nTr, oTable) {
            var aData = oTable.fnGetData(nTr);
            var _resno = $(aData[0]);
            var resNo = _resno.attr("resno");
            var sOut = '';
            $.ajax({
                async: false,
                type: "POST",
                data: '{"ResNo":"' + resNo + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResMonitorV2.aspx/createGridDetail',
                success: function (msg) {
                    if (msg.d != null && msg.d != '') {
                        sOut = msg.d;
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showAlert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
            return sOut;
        }

        function fnCreateSelect(header) {
            var style = '';
            if (header.sWidth != null) {
                style = 'style="width: 100%' + /*header.sWidth +*/';"';
            }
            var r = '<input ' + style + ' id="' + '" />';
            return r;
        }

        function tryNumberFormat(obj) {
            var nf = new NumberFormat(obj);
            var rsdoS = nf.PERIOD;
            var rsdoD = nf.COMMA;
            nf.setPlaces(2);
            nf.setSeparators(true, rsdoS, rsdoD);
            obj = nf.toFormatted();
            return obj;
        }

        function showSearchData() {
            $("#resMonitorGrid").remove("#resTable");
            var tableElm = $(document.createElement('table'));
            tableElm.attr({ 'id': 'resTable', 'class': 'display' }).css({ 'css': 'width: 1000px' });
            $("#resMonitorGrid").append(tableElm);

            var _aoColumns = [];
            $.ajax({
                async: false,
                type: "POST",
                data: '{}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResMonitorV2.aspx/getGridHeaders',
                success: function (msg) {
                    if (msg.d != null && msg.d != '') {

                        $.each($.json.decode(msg.d), function (i) {
                            obj = new Object();
                            if (this.sTitle != undefined && this.sTitle != null) obj.sTitle = this.sTitle;
                            if (this.sWidth != undefined && this.sWidth != null) obj.sWidth = this.sWidth;
                            if (this.bSortable != undefined && this.bSortable != null) obj.bSortable = this.bSortable;
                            if (this.sType != undefined && this.sType != null) obj.sType = this.sType;
                            if (this.sClass != undefined && this.sClass != null) obj.sClass = this.sClass;
                            _aoColumns.push(obj);
                        });

                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showAlert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });

            var oLanguage = {
                "sProcessing": sProcessing,
                "sLengthMenu": sLengthMenu,
                "sZeroRecords": sZeroRecords,
                "sEmptyTable": sEmptyTable,
                "sInfo": sInfo,
                "sInfoEmpty": sInfoEmpty,
                "sInfoFiltered": sInfoFiltered,
                "sInfoPostFix": sInfoPostFix,
                "sSearch": sSearch,
                "sUrl": sUrl,
                "oPaginate": { "sFirst": sFirst, "sPrevious": sPrevious, "sNext": sNext, "sLast": sLast, "sPage": sPage },
                "fnInfoCallback": null
            };
            oTable = $('#resTable').dataTable({
                "bDestory": false,
                "bRetrieve": true,
                "bScrollCollapse": true,
                "sPaginationType": "listbox",
                "bFilter": false,
                "bJQueryUI": true,
                "oLanguage": oLanguage,
                "aoColumns": _aoColumns,
                "bScrollAutoCss": false,
                "bProcessing": false,
                "bServerSide": true,
                "bStateSave": true,
                "sServerMethod": "POST",
                "sAjaxSource": "ResMonitorV2Data.aspx",
                "fnRowCallback": function (nRow, aaData, iDisplayIndex) {
                    if (aaData[0].indexOf('input type="hidden" value="2"/>') > 0) {
                        $('tr', nRow).addClass('totalrow');
                    }
                    else if (aaData[0].indexOf('<input type="hidden" value="0"/>') > 0) {
                        $('td:eq(3)', nRow).addClass('boldrow');
                        $('td:eq(4)', nRow).addClass('boldrow');
                        $('td:eq(5)', nRow).addClass('boldrow');
                        $('td:eq(6)', nRow).addClass('boldrow');
                        $('td:eq(7)', nRow).addClass('boldrow');
                        $('td:eq(8)', nRow).addClass('boldrow');
                        $('td:eq(10)', nRow).addClass('boldrow');
                        $('td:eq(11)', nRow).addClass('boldrow');
                        $('td:eq(12)', nRow).addClass('boldrow');
                        $('td:eq(13)', nRow).addClass('boldrow');
                    }
                    else if (aaData[0].indexOf('input type="hidden" value="9"/>') > 0) {
                        $('td:eq(3)', nRow).addClass('optionrow');
                        $('td:eq(4)', nRow).addClass('optionrow');
                        $('td:eq(5)', nRow).addClass('optionrow');
                        $('td:eq(6)', nRow).addClass('optionrow');
                        $('td:eq(7)', nRow).addClass('optionrow');
                        $('td:eq(8)', nRow).addClass('optionrow');
                        $('td:eq(10)', nRow).addClass('optionrow');
                        $('td:eq(11)', nRow).addClass('optionrow');
                        $('td:eq(12)', nRow).addClass('optionrow');
                        $('td:eq(13)', nRow).addClass('optionrow');
                    }
                    else if (aaData[0].indexOf('input type="hidden" value="pxm"/>') > 0) {
                        $('td:eq(3)', nRow).addClass('pxmrow');
                        $('td:eq(4)', nRow).addClass('pxmrow');
                        $('td:eq(5)', nRow).addClass('pxmrow');
                        $('td:eq(6)', nRow).addClass('pxmrow');
                        $('td:eq(7)', nRow).addClass('pxmrow');
                        $('td:eq(8)', nRow).addClass('pxmrow');
                        $('td:eq(10)', nRow).addClass('pxmrow');
                        $('td:eq(11)', nRow).addClass('pxmrow');
                        $('td:eq(12)', nRow).addClass('pxmrow');
                        $('td:eq(13)', nRow).addClass('pxmrow');
                    }
                    else {
                        $('td:eq(3)', nRow).addClass('normalrow');
                        $('td:eq(4)', nRow).addClass('normalrow');
                        $('td:eq(5)', nRow).addClass('normalrow');
                        $('td:eq(6)', nRow).addClass('normalrow');
                        $('td:eq(7)', nRow).addClass('normalrow');
                        $('td:eq(8)', nRow).addClass('normalrow');
                        $('td:eq(10)', nRow).addClass('normalrow');
                        $('td:eq(11)', nRow).addClass('normalrow');
                        $('td:eq(12)', nRow).addClass('normalrow');
                        $('td:eq(13)', nRow).addClass('normalrow');
                    }
                    return nRow;
                },
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (data, textStatus, xmlHttpRequest) {

                            var jData = $(data);
                            var json = { "sEcho": aoData[0].value, "aaData": [] };
                            json.iTotalRecords = data.iTotalRecords;
                            json.iTotalDisplayRecords = data.iTotalDisplayRecords;
                            var objSum = [];
                            var tableSum = data.tableSum;
                            $.each(data.aaData, function (i) {
                                var obj = [];
                                if (this.oc != undefined) { obj.push(this.oc); }
                                if (this.ResNo != undefined) { obj.push(this.ResNo); }
                                if (this.Info1 != undefined) { obj.push(this.Info1); }
                                if (this.LeaderName != undefined) { obj.push(this.LeaderName); }
                                if (this.HotelName != undefined) { obj.push(this.HotelName); }
                                if (this.Adult != undefined) { obj.push(this.Adult); }
                                if (this.Child != undefined) { obj.push(this.Child); }
                                if (this.BegDate != undefined) { obj.push(this.BegDate); }
                                if (this.Days != undefined) { obj.push(this.Days); }
                                if (this.statusStr != undefined) { obj.push(this.statusStr); }
                                if (this.SalePrice != undefined) { obj.push(this.SalePrice); }
                                if (this.SaleCur != undefined) { obj.push(this.SaleCur); }
                                if (this.DepCityName != undefined) { obj.push(this.DepCityName); }
                                if (this.ArrCityName != undefined) { obj.push(this.ArrCityName); }
                                json.aaData.push(obj);
                            });
                            if (data.aaData.length > 0) {
                                var objSum = [];
                                var _tableSum = data.tableSum;
                                var _this = data.aaData[0];
                                if (_this.oc != undefined) { objSum.push('<input type="hidden" value="2"/>'); }
                                if (_this.ResNo != undefined) { objSum.push('<b>' + _tableSum.totalRecord + '</b>'); }
                                if (_this.Info1 != undefined) { objSum.push("&nbsp"); }
                                if (_this.LeaderName != undefined) { objSum.push("&nbsp"); }
                                if (_this.HotelName != undefined) { objSum.push("&nbsp"); }
                                if (_this.Adult != undefined) { objSum.push('<b>' + _tableSum.totalAdult + '</b>'); }
                                if (_this.Child != undefined) { objSum.push('<b>' + _tableSum.totalChild + '</b>'); }
                                if (_this.BegDate != undefined) { objSum.push("&nbsp"); }
                                if (_this.Days != undefined) { objSum.push("&nbsp"); }
                                if (_this.statusStr != undefined) { objSum.push("&nbsp"); }
                                if (_this.SalePrice != undefined && _this.SaleCur != undefined) {
                                    var saleCur = '';
                                    var salePrice = '';
                                    $.each(_tableSum.totalPrice, function (i) {
                                        if (saleCur.length > 0) saleCur += '<br />';
                                        saleCur += this.SaleCur;
                                        if (salePrice.length > 0) salePrice += '<br />';
                                        salePrice += tryNumberFormat(this.SalePrice);
                                    });
                                    objSum.push('<b>' + salePrice + '</b>');
                                    objSum.push('<b>' + saleCur + '</b>');
                                }
                                if (_this.DepCityName != undefined) { objSum.push("&nbsp"); }
                                if (_this.ArrCityName != undefined) { objSum.push("&nbsp"); }
                                json.aaData.push(objSum);
                            }
                            fnCallback(json);
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                showAlert(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                }
            });

            $('#resTable tbody td #detayImg ').live('click', function () {
                var nTr = this.parentNode.parentNode;
                if (this.src.match('infoClose')) {
                    /* This row is already open - close it */
                    this.src = "Images/infoOpen.gif";
                    oTable.fnClose(nTr);
                }
                else {
                    /* Open this row */
                    this.src = "Images/infoClose.gif";
                    if ($("#hfDetailType").val() != '' && $("#hfDetailType").val() == '1') {
                        oTable.fnOpen(nTr, fnFormatDetailsType1(nTr, oTable), 'details');
                    } else {
                        oTable.fnOpen(nTr, fnFormatDetails(nTr, oTable), 'details');
                    }
                }
            });
        }

        function getSearchData() {
            $.ajax({
                async: false,
                type: "POST",
                data: '{}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResMonitorV2.aspx/getData',
                success: function (msg) {
                    var oSettings = oTable.fnSettings();
                    oSettings._iDisplayStart = 0;
                    oTable.fnDraw(false);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showAlert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }
        function dateToFormattedDate(date) {
            return moment(date).format("YYYY-MM-DD");
        }
        function formattedDateToDate(date) {
            return moment(date,"YYYY-MM-DD").toDate();
        }

        function btnFilterClick() {
            var _resStatus = '';
            var _confStatus = '';
            var _payStatus = '';

            var chkBox = $("input:checked");
            for (i = 0; i < chkBox.length; i++) {
                if (chkBox[i].name == 'fltResStatus') {
                    if (_resStatus.length > 0) _resStatus += ';';
                    _resStatus += chkBox[i].value;
                }
                if (chkBox[i].name == 'fltConfirmStatus') {
                    if (_confStatus.length > 0) _confStatus += ';';
                    _confStatus += chkBox[i].value;
                }
                if (chkBox[i].name == 'fltPaymentStatus') {
                    if (_payStatus.length > 0) _payStatus += ';';
                    _payStatus += chkBox[i].value;
                }
            }

            var _showDraft = $("#cbShowDraft:checked").is(':checked') ? true : false;

            var _showOptionRes = $("input:radio[name='rbOptionControl']:checked").val();

            _filterData = new Object();
            _filterData.ResNo1 = $("#fltResNo1").val();
            _filterData.ResNo2 = $("#fltResNo2").val();
            _filterData.BegDate1 = $("#fltBeginDate1").val() != '' ? dateToFormattedDate($("#fltBeginDate1").datepicker("getDate")) : null;
            _filterData.BegDate2 = $("#fltBeginDate2").val() != '' ? dateToFormattedDate($("#fltBeginDate2").datepicker("getDate")) : null;
            _filterData.EndDate1 = $("#fltEndDate1").val() != '' ? dateToFormattedDate($("#fltEndDate1").datepicker("getDate")) : null;
            _filterData.EndDate2 = $("#fltEndDate2").val() != '' ? dateToFormattedDate($("#fltEndDate2").datepicker("getDate")) : null;
            _filterData.ResDate1 = $("#fltResDate1").val() != '' ? dateToFormattedDate($("#fltResDate1").datepicker("getDate")) : null;
            _filterData.ResDate2 = $("#fltResDate2").val() != '' ? dateToFormattedDate($("#fltResDate2").datepicker("getDate")) : null;
            _filterData.PayDate1 = null;
            _filterData.PayDate2 = null;
            _filterData.ArrCity = $("#fltArrCity").val();
            _filterData.DepCity = $("#fltDepCity").val();
            _filterData.AgencyOffice = $("#fltAgencyOffice").val();
            _filterData.AgencyUser = $("#fltAgencyUser").val();
            _filterData.LeaderSurname = $("#fltSurname").val();
            _filterData.LeaderName = $("#fltName").val();
            _filterData.HolPack = $("#fltHolPack").val();
            _filterData.ResStatus = _resStatus;
            _filterData.ConfStatus = _confStatus;
            _filterData.PayStatus = _payStatus;
            _filterData.UserSeeAllReservation = null;
            _filterData.ShowDraft = _showDraft;
            _filterData.ShowOptionRes = _showOptionRes;
            _filterData.DateLang = '';
            _filterData.Currency = $("#fltCurrency").val();
            _filterData.showOnlyActiveUsers = $("#fltOnlyActiveUsers").attr('checked') == 'checked';
            _filterData.ShowPxmRes = $("#cbShowPxmRes").attr("checked") == "checked";
            var _data = $.json.encode(_filterData).replace(/"/g, '|').replace(/{/g, '<').replace(/}/g, '>');
            $.ajax({
                async: false,
                type: "POST",
                data: $.json.encode({ data: _data }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResMonitorV2.aspx/SaveFilter',
                success: function (msg) {
                    if (msg.d == true) {
                        getSearchData();
                    }
                    getBonus();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showAlert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function setOtherFields(data) {
            $('input[name=fltResStatus]').each(function (index) { this.checked = false; });
            if (data.Filter.ResStatus != null && data.Filter.ResStatus != '') {
                for (i = 0; i < data.Filter.ResStatus.split(';').length; i++) {
                    $("#fltResStatus" + data.Filter.ResStatus.split(';')[i]).attr("checked", "checked");
                }
            }
            $('input[name=fltConfirmStatus]').each(function (index) { this.checked = false; });
            if (data.Filter.ConfStatus != null && data.Filter.ConfStatus != '') {
                for (i = 0; i < data.Filter.ConfStatus.split(';').length; i++) {
                    $("#fltConfirmStatus" + data.Filter.ConfStatus.split(';')[i]).attr("checked", "checked");
                }
            }
            $('input[name=fltPaymentStatus]').each(function (index) { this.checked = false; });
            if (data.Filter.PayStatus != null && data.Filter.PayStatus != '') {
                for (i = 0; i < data.Filter.PayStatus.split(';').length; i++) {
                    $("#fltPaymentStatus" + data.Filter.PayStatus.split(';')[i]).attr("checked", "checked");
                }
            }

            $("#fltSurname").val(data.Filter.LeaderSurname != null ? data.Filter.LeaderSurname : "");
            $("#fltName").val(data.Filter.LeaderName != null ? data.Filter.LeaderName : "");
            $("#fltResNo1").val(data.Filter.ResNo1 != null ? data.Filter.ResNo1 : "");
            $("#fltResNo2").val(data.Filter.ResNo2 != null ? data.Filter.ResNo2 : "");
            var _date = '';
            if (data.Filter.BegDate1 != null && data.Filter.BegDate1 != '') {
                var _date = formattedDateToDate(data.Filter.BegDate1); //new Date(Date(eval(data.Filter.BegDate1)).toString());
                $('#fltBeginDate1').datepicker('setDate', _date);
            } else {
                $("#fltBeginDate1").val('');
            }

            if (data.Filter.BegDate2 != null && data.Filter.BegDate2 != '') {
                var _date = formattedDateToDate(data.Filter.BegDate2); //new Date(Date(eval(data.Filter.BegDate2)).toString());
                $("#fltBeginDate2").datepicker('setDate', _date);
            } else {
                $("#fltBeginDate2").val('');
            }

            if (data.Filter.EndDate1 != null && data.Filter.EndDate1 != '') {
                var _date = formattedDateToDate(data.Filter.EndDate1); //new Date(Date(eval(data.Filter.EndDate1)).toString());
                $("#fltEndDate1").datepicker('setDate', _date);
            } else {
                $("#fltEndDate1").val('');
            }

            if (data.Filter.EndDate2 != null && data.Filter.EndDate2 != '') {
                var _date = formattedDateToDate(data.Filter.EndDate2); //new Date(Date(eval(data.Filter.EndDate2)).toString());
                $("#fltEndDate2").datepicker('setDate', _date);
            } else {
                $("#fltEndDate2").val('');
            }

            if (data.Filter.ResDate1 != null && data.Filter.ResDate1 != '') {
                var _date = formattedDateToDate(data.Filter.ResDate1); //new Date(Date(eval(data.Filter.ResDate1)).toString());
                $("#fltResDate1").datepicker('setDate', _date);
            } else {
                $("#fltResDate1").val('');
            }

            if (data.Filter.ResDate2 != null && data.Filter.ResDate2 != '') {
                var _date = formattedDateToDate(data.Filter.ResDate2); //new Date(Date(eval(data.Filter.ResDate2)).toString());
                $("#fltResDate2").datepicker('setDate', _date);
            } else {
                $("#fltResDate2").val('');
            }
        }

        function createFilterData(clear) {
            clearFilterData(clear);
        }

        function clearFilterData(clear) {
            var defaultData = getDefaultData(clear);
            $("#fltBeginDate1").datepicker("setDate", null);
            $("#fltBeginDate2").datepicker("setDate", null);
            $("#fltEndDate1").datepicker("setDate", null);
            $("#fltEndDate2").datepicker("setDate", null);
            $("#fltResDate1").datepicker("setDate", null);
            $("#fltResDate2").datepicker("setDate", null);
            showFilterData(defaultData);
        }

        function showFilterData(data) {
            setDepCity(data);
            setArrCity(data);
            setHolpack(data);
            setAgencyOffice(data);
            setAgencyUser(data.AgencyUserData, data.Filter.AgencyUser, data.Filter.AgencyOffice, data.Filter.ShowAllRes);
            if (data.Filter.AgencyUser != '') {
                var agency = $("#fltAgencyUser :selected").attr("agencyCode");
                changeAgencyOffice(agency);
            }
            setCurrency(data);
            if (data.OptionDateControl == true) {
                $(".divSelect").show();
                if (data.ShowFilterDraft == true) {
                    $("#fltResStatus9").show();
                    $('#fltResStatus9, label[for="fltResStatus9"]').show();
                }
                else {
                    $("#fltResStatus9").hide();
                    $('#fltResStatus9, label[for="fltResStatus9"]').hide();
                }
                if (data.ShowDraftControl == true) {
                    $("#cbShowDraftDiv").show();
                    ShowDraftClick();
                }
                else {
                    $("#cbShowDraftDiv").hide();
                }
            }
            else {
                $(".divSelect").hide();
            }
            //$.datepicker.setDefaults($.datepicker.regional[data.Filter.DateLang != 'en' ? data.Filter.DateLang : '']);
            setOtherFields(data);
            $("#hfDetailType").val(data.DetailType);
        }

        function getDefaultData(clear) {
            var data;
            $.ajax({
                async: false,
                type: "POST",
                data: '{"clear":"' + clear + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResMonitorV2.aspx/CreateFilterData',
                success: function (msg) {
                    if (msg.hasOwnProperty('d')) {
                        data = msg.d;
                        $("#hfCustomRegID").val(data.CustomRegID);
                        if (data.NoCheckOnlyActiveUsers == true) {
                            $("#fltOnlyActiveUsers").removeAttr('checked');
                        }
                        if (data.ShowFilterOnlyPxmRes) {
                            $("#showPxmRes").show();
                        } else {
                            $("#showPxmRes").hide();
                        }
                        return data;
                    } else {
                        return null;
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showAlert(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
            return data;
        }

        $(document).ready(function () {
            maximize();
            if (searchCustomerOptions == "1") {
                $("#lblSurname").html(lblCustomerSurname + ':&nbsp;');
            } else {
                $("#lblSurname").html(lblSurname + ':&nbsp;');
            }
            var defaultData = getDefaultData(false);
            $.datepicker.setDefaults($.datepicker.regional[twoLetterISOLanguageName != 'en' ? twoLetterISOLanguageName : '']);
            $("#fltBeginDate1").datepicker({
                showOn: "button",
                buttonImage: "Images/Calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                onSelect: function (dateText, inst) {
                    if (dateText != '') {
                        var date1 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
                        if ($("#fltBeginDate2").datepicker("getDate") != null) {
                            var date2 = $("#fltBeginDate2").datepicker("getDate");
                            if (date2.getTime() < date1.getTime()) {
                                $("#fltBeginDate2").datepicker("setDate", date1);
                            }
                        }
                    }
                }
            });
            $("#fltBeginDate2").datepicker({
                showOn: "button",
                buttonImage: "Images/Calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                onSelect: function (dateText, inst) {
                    if (dateText != '') {
                        var date2 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
                        if ($("#fltBeginDate1").datepicker("getDate") != null) {
                            var date1 = $("#fltBeginDate1").datepicker("getDate");
                            if (date2.getTime() < date1.getTime()) {
                                $("#fltBeginDate1").datepicker("setDate", date2);
                            }
                        }
                    }
                }
            });
            $("#fltEndDate1").datepicker({
                showOn: "button",
                buttonImage: "Images/Calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                onSelect: function (dateText, inst) {
                    if (dateText != '') {
                        var date1 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
                        if ($("#fltEndDate2").datepicker("getDate") != null) {
                            var date2 = $("#fltEndDate2").datepicker("getDate");
                            if (date2.getTime() < date1.getTime()) {
                                $("#fltEndDate2").datepicker("setDate", date1);
                            }
                        }
                    }
                }
            });
            $("#fltEndDate2").datepicker({
                showOn: "button",
                buttonImage: "Images/Calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                onSelect: function (dateText, inst) {
                    if (dateText != '') {
                        var date2 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
                        if ($("#fltEndDate1").datepicker("getDate") != null) {
                            var date1 = $("#fltEndDate1").datepicker("getDate");
                            if (date2.getTime() < date1.getTime()) {
                                $("#fltEndDate1").datepicker("setDate", date2);
                            }
                        }
                    }
                }
            });
            $("#fltResDate1").datepicker({
                showOn: "button",
                buttonImage: "Images/Calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                onSelect: function (dateText, inst) {
                    if (dateText != '') {
                        var date1 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
                        if ($("#fltResDate2").datepicker("getDate") != null) {
                            var date2 = $("#fltResDate2").datepicker("getDate");
                            if (date2.getTime() < date1.getTime()) {
                                $("#fltResDate2").datepicker("setDate", date1);
                            }
                        }
                    }
                }
            });
            $("#fltResDate2").datepicker({
                showOn: "button",
                buttonImage: "Images/Calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                onSelect: function (dateText, inst) {
                    if (dateText != '') {
                        var date2 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
                        if ($("#fltResDate1").datepicker("getDate") != null) {
                            var date1 = $("#fltResDate1").datepicker("getDate");
                            if (date2.getTime() < date1.getTime()) {
                                $("#fltResDate1").datepicker("setDate", date2);
                            }
                        }
                    }
                }
            });
            showFilterData(defaultData);
            $.query = $.query.load(location.href);
            fromB2C = $.query.get('FromB2C');
            showSearchData();
            btnFilterClick();
            extLoginVer = $.query.get("ExLogV");

            if (fromB2C == '1') {
                if ($("#forB2CRes").val() != '2') {
                    $("#forB2CRes").val('1');
                    if (extLoginVer == "2")
                        setTimeout('makeReservation("V2")', 1000);
                    else
                        setTimeout('makeReservation()', 1000);
                }
            }
        });
        var extLogin = "0";
    </script>

</head>
<body>
    <form id="ResMonitorForm" runat="server">
        <div class="Page">
            <tv1:Header ID="tvHeader" runat="server" />
            <tv1:MainMenu ID="tvMenu" runat="server" />
            <div class="ui-helper-clearfix FilterArea">
                <div>
                    <div class="LeftDiv border ui-helper-clearfix">
                        <div class="divResNo">
                            <span class="Caption"><strong>
                                <%= GetGlobalResourceObject("ResMonitor", "lblResNo")%>:&nbsp;</strong></span>
                            <div class="dateDivBlock">
                                <input id="fltResNo1" />
                            </div>
                            <div class="dateDivBlockSeparator">
                                <strong>&gt;&gt;</strong>
                            </div>
                            <div class="dateDivBlock">
                                <input id="fltResNo2" />
                            </div>
                        </div>
                        <div class="divResBegDate">
                            <span class="Caption"><strong>
                                <%= GetGlobalResourceObject("ResMonitor", "lblBeginDate")%>:&nbsp;</strong></span>
                            <div class="dateDivBlock">
                                <input id="fltBeginDate1" type="text" class="dateStyle" />
                            </div>
                            <div class="dateDivBlockSeparator">
                                <strong>&gt;&gt;</strong>
                            </div>
                            <div class="dateDivBlock">
                                <input id="fltBeginDate2" type="text" class="dateStyle" />
                            </div>
                        </div>
                        <div class="divResEndDate">
                            <span class="Caption"><strong>
                                <%= GetGlobalResourceObject("ResMonitor", "lblEndDate")%>:&nbsp;</strong></span>
                            <div class="dateDivBlock">
                                <input id="fltEndDate1" type="text" class="dateStyle" />
                            </div>
                            <div class="dateDivBlockSeparator">
                                <strong>&gt;&gt;</strong>
                            </div>
                            <div class="dateDivBlock">
                                <input id="fltEndDate2" type="text" class="dateStyle" />
                            </div>
                        </div>
                        <div class="divResRegisterDate">
                            <span class="Caption"><strong>
                                <%= GetGlobalResourceObject("ResMonitor", "lblResDate")%>:&nbsp;</strong></span>
                            <div class="dateDivBlock">
                                <input id="fltResDate1" type="text" class="dateStyle" />
                            </div>
                            <div class="dateDivBlockSeparator">
                                <strong>&gt;&gt;</strong>
                            </div>
                            <div class="dateDivBlock">
                                <input id="fltResDate2" type="text" class="dateStyle" />
                            </div>
                        </div>
                        <div class="divHolpack">
                            <span class="Caption"><strong>
                                <%= GetGlobalResourceObject("ResMonitor", "lblHolpack")%>:&nbsp;</strong></span>
                            <div class="inputDiv300px">
                                <select id="fltHolPack" class="width95Per">
                                </select>
                            </div>
                        </div>
                        <div class="divDepCity">
                            <span class="Caption"><strong>
                                <%= GetGlobalResourceObject("ResMonitor", "lblDepCity")%>:&nbsp;</strong></span>
                            <div class="inputDiv300px">
                                <select id="fltDepCity" class="width95Per">
                                </select>
                            </div>
                        </div>
                        <div class="divArrCity">
                            <span class="Caption"><strong>
                                <%= GetGlobalResourceObject("ResMonitor", "lblArrCity")%>:&nbsp;</strong></span>
                            <div class="inputDiv300px">
                                <select id="fltArrCity" class="width95Per">
                                </select>
                            </div>
                        </div>
                        <div class="divArrCity">
                            <span class="Caption"><strong>
                                <%= GetGlobalResourceObject("ResView", "lblCurrency")%>:&nbsp;</strong></span>
                            <div class="inputDiv300px">
                                <select id="fltCurrency" class="width95Per">
                                </select>
                            </div>
                        </div>
                        <div class="divSelect">
                            <div class="divBlock">
                                <div id="cbShowDraftDiv">
                                    <input id="cbShowDraft" type="checkbox" onclick="ShowDraftClick();" /><label for="cbShowDraft"><%= GetGlobalResourceObject("LibraryResource", "cbShowDraft")%></label>
                                </div>
                                <br />
                                <div id="showPxmRes">
                                    <input id="cbShowPxmRes" type="checkbox" onclick="ShowPxmResClick();" /><label for="cbShowDraft">Show only Paximum reservation.</label>
                                </div>
                                <br />
                                <div id="rbOptionControlDiv">
                                    <input id="rbOptionControl0" type="radio" name="rbOptionControl" value="0" checked="checked" />
                                    <label for="rbOptionControl0">
                                        <%= GetGlobalResourceObject("LibraryResource", "ReservationAll")%></label>&nbsp;&nbsp;
                                <input id="rbOptionControl1" type="radio" name="rbOptionControl" value="1" />
                                    <label for="rbOptionControl1">
                                        <%= GetGlobalResourceObject("LibraryResource", "ReservationOptional")%></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="RightDiv border ui-helper-clearfix">
                        <div class="divAgencyOfficeAgencyUser">
                            <div class="divAgencyOffice">
                                <strong>
                                    <%= GetGlobalResourceObject("ResMonitor", "lblAgencyOffice")%>:&nbsp;</strong>
                                <br />
                                <select id="fltAgencyOffice" onchange="changeAgencyOffice(this.value);">
                                </select>
                            </div>
                            <div class="divAgencyUser">
                                <strong>
                                    <%= GetGlobalResourceObject("ResMonitor", "lblAgencyUser")%>:&nbsp;</strong>
                                <br />
                                <select id="fltAgencyUser" onchange="changeAgencyUser();">
                                </select>
                            </div>
                        </div>
                        <div class="divOnlyActiveUser">
                            <div class="divLeftShowOnlyUsers">
                                &nbsp;
                            </div>
                            <div class="divShowOnlyUsers">
                                <input id="fltOnlyActiveUsers" type="checkbox" checked="checked" onclick="changeOnlyActiveUsers();" />
                                <label for="fltOnlyActiveUsers">
                                    <%= GetGlobalResourceObject("ResMonitor", "lblShowActiveUser")%></label>
                            </div>
                        </div>
                        <div class="divLeader">
                            <div class="divSurname">
                                <strong id="lblSurname"></strong>
                                <br />
                                <input id="fltSurname" />
                            </div>
                            <div class="divName">
                                <strong>
                                    <%= GetGlobalResourceObject("ResMonitor", "lblName")%>:&nbsp;</strong>
                                <br />
                                <input id="fltName" />
                            </div>
                        </div>
                        <div class="divStatus">
                            <table>
                                <tr>
                                    <td>
                                        <strong>
                                            <%= GetGlobalResourceObject("ResMonitor", "lblResStatus")%>:&nbsp;</strong>
                                    </td>
                                    <td>
                                        <strong>
                                            <%= GetGlobalResourceObject("ResMonitor", "lblConfStatus")%>:&nbsp;</strong>
                                    </td>
                                    <td>
                                        <strong>
                                            <%= GetGlobalResourceObject("ResMonitor", "lblPayStatus")%>:&nbsp;</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <input id="fltResStatus0" name="fltResStatus" type="checkbox" value="0" /><label
                                            for="fltResStatus0"><%= GetGlobalResourceObject("LibraryResource", "ResStatus0")%></label><br />
                                        <input id="fltResStatus1" name="fltResStatus" type="checkbox" value="1" /><label
                                            for="fltResStatus1"><%= GetGlobalResourceObject("LibraryResource", "ResStatus1")%></label><br />
                                        <input id="fltResStatus2" name="fltResStatus" type="checkbox" value="2" /><label
                                            for="fltResStatus2"><%= GetGlobalResourceObject("LibraryResource", "ResStatus2")%></label><br />
                                        <input id="fltResStatus3" name="fltResStatus" type="checkbox" value="3" /><label
                                            for="fltResStatus3"><%= GetGlobalResourceObject("LibraryResource", "ResStatus3")%></label><br />
                                        <input id="fltResStatus9" name="fltResStatus" type="checkbox" value="9" style="display: none;" /><label
                                            for="fltResStatus9" style="display: none;"><%= GetGlobalResourceObject("LibraryResource", "ResStatus9")%></label>
                                    </td>
                                    <td valign="top">
                                        <input id="fltConfirmStatus0" name="fltConfirmStatus" type="checkbox" value="0" /><label
                                            for="fltConfirmStatus0"><%= GetGlobalResourceObject("LibraryResource", "ConfStatus0")%></label><br />
                                        <input id="fltConfirmStatus1" name="fltConfirmStatus" type="checkbox" value="1" /><label
                                            for="fltConfirmStatus1"><%= GetGlobalResourceObject("LibraryResource", "ConfStatus1")%></label><br />
                                        <input id="fltConfirmStatus2" name="fltConfirmStatus" type="checkbox" value="2" /><label
                                            for="fltConfirmStatus2"><%= GetGlobalResourceObject("LibraryResource", "ConfStatus2")%></label><br />
                                        <input id="fltConfirmStatus3" name="fltConfirmStatus" type="checkbox" value="3" /><label
                                            for="fltConfirmStatus3"><%= GetGlobalResourceObject("LibraryResource", "ConfStatus3")%></label>
                                    </td>
                                    <td valign="top">
                                        <input id="fltPaymentStatusU" name="fltPaymentStatus" type="checkbox" value="U" /><label
                                            for="fltPaymentStatusU"><%= GetGlobalResourceObject("LibraryResource", "PayStatusU")%></label><br />
                                        <input id="fltPaymentStatusP" name="fltPaymentStatus" type="checkbox" value="P" /><label
                                            for="fltPaymentStatusU"><%= GetGlobalResourceObject("LibraryResource", "PayStatusP")%></label><br />
                                        <input id="fltPaymentStatusO" name="fltPaymentStatus" type="checkbox" value="O" /><label
                                            for="fltPaymentStatusU"><%= GetGlobalResourceObject("LibraryResource", "PayStatusO")%></label><br />
                                        <input id="fltPaymentStatusV" name="fltPaymentStatus" type="checkbox" value="V" /><label
                                            for="fltPaymentStatusU"><%= GetGlobalResourceObject("LibraryResource", "PayStatusV")%></label><br />
                                        <input id="fltPaymentStatusN" name="fltPaymentStatus" type="checkbox" value="N" /><label
                                            for="fltPaymentStatusN"><%= GetGlobalResourceObject("LibraryResource", "PayStatusN")%></label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="divFilterButton">
                    <table>
                        <tr>
                            <td>
                                <input type="button" id="btnFilter" value='<%= GetGlobalResourceObject("ResMonitor", "btnFilter")%>'
                                    onclick="btnFilterClick();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                            </td>
                            <td>
                                <input type="button" id="btnfilterclear" value='<%= GetGlobalResourceObject("ResMonitor", "btnClear")%>'
                                    onclick="clearFilterData(true);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="divBonus">
            </div>
            <div class="Content">
                <div class="exportDiv">
                    <input type="button" value='<%= GetGlobalResourceObject("ResMonitor", "lblExportToExcel")%>' onclick="exportExcel();"
                        class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                    &nbsp;
          <input type="button" value='<%= GetGlobalResourceObject("ResMonitor", "lblExportToHtml")%>' onclick="exportHtml();"
              class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                </div>
                <div id="resMonitorGrid">
                </div>
            </div>
            <div class="Footer">
                <tv1:Footer ID="tvfooter" runat="server" />
            </div>
        </div>
        <div id="dialog-paymentPlan" title='<%= GetGlobalResourceObject("ResMonitor", "titlePaymentPlan")%>'>
            <span id="paymentPlan"></span>
        </div>
        <div id="dialog-commissionInvoice" title='<%= GetGlobalResourceObject("ResMonitor", "lblCommisionInvoiceNumber")%>'>
            <span id="commissionInvoice"></span>
        </div>
        <div id="dialog-resComment" title='<%= GetGlobalResourceObject("ResMonitor", "titleComment")%>'>
            <resCom1:ResComment ID="resComment" runat="server" />
        </div>
        <div id="dialog-bonusList" title="">
            <div id="divBonusList">
            </div>
        </div>
        <div id="dialog-message" title="">
            <span id="messages">Message</span>
        </div>
        <div id="dialog-export" title="">
            <div id="exportDiv">
            </div>
        </div>
        <div id="waitMessage">
            <img alt="" src="Images/Wait.gif" />
        </div>
        <div id="dialog-MakeReservation" title='<%= GetGlobalResourceObject("MakeReservation", "lblMakeReservation") %>' style="display: none;text-align:center;">
            <iframe id="MakeReservation"></iframe>
        </div>
        <input id="hfCustomRegID" type="hidden" value="" />
        <input id="hfDetailType" type="hidden" value="0" />
        <input id="forB2CRes" type="hidden" value="0" />
    </form>
</body>
</html>
