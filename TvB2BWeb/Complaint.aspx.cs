﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using TvBo;

public partial class Complaint : BasePage
{
    public static string twoLetterISOLanguageName = "en";

    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url);

        List<string> lang = new List<string>() { "ar-DZ", "ar-DZ", "ar", "en-AU", "en-GB", "en-NZ", "fr-CH", "fr", "pt-BR", "pt", "sr-SR", "sr-CS", "sr", "zh-CN", "zh-HK", "zh-TW" };
        if (lang.Contains(UserData.Ci.Name))
            twoLetterISOLanguageName = UserData.Ci.Name;
        else
            twoLetterISOLanguageName = UserData.Ci.TwoLetterISOLanguageName != "en" ? UserData.Ci.TwoLetterISOLanguageName : "";
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        string errorMsg = string.Empty;
        //List<ComplaintList> complaintList = new Complaints().getComplaintList(UserData, ref errorMsg);
        List<CodeName> hotelList = new Complaints().getComplaintHotelList(UserData, useLocalName, ref errorMsg);
        List<CompDepartment> departmentL = new Complaints().getComplaintDepartment(UserData, ref errorMsg);
        var departmentList = from q in departmentL
                             select new { Code = q.RecID, Name = useLocalName ? q.NameL : q.Name };
        return new {
            HotelList = hotelList,
            Department = departmentList
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getComplaintData(DateTime? BegDate, DateTime? EndDate, Int16? Type, bool? Active, int? Department, string Hotel)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        string errorMsg = string.Empty;

        List<ComplaintList> complaintList = new Complaints().getComplaintList(UserData, BegDate, EndDate, Type, Active, Department, Hotel, ref errorMsg);
        if (string.IsNullOrEmpty(errorMsg)) {
            CreateAndWriteFile(complaintList);
            return true;
        } else {
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getGridHeaders()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        List<dataTableColumnsDef> aoColumns = new List<dataTableColumnsDef>();

        aoColumns.Add(new dataTableColumnsDef { IDNo = 0, ColName = "ViewEdit", sTitle = "&nbsp;", sWidth = "50px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 1, ColName = "Attach", sTitle = "Attach", sWidth = "50px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 2, ColName = "Department", sTitle = "Department", sWidth = "180px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 3, ColName = "Hotel", sTitle = "Hotel", sWidth = "225px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 4, ColName = "Source", sTitle = "Source", sWidth = "125px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 5, ColName = "Status", sTitle = "Status", sWidth = "75px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 6, ColName = "Category", sTitle = "Category", sWidth = "75px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 7, ColName = "ReceivedDate", sTitle = "Received Date", sWidth = "75px", bSortable = false, sType = "date" });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 8, ColName = "Active", sTitle = "Active", sWidth = "60px", bSortable = false });
        aoColumns.Add(new dataTableColumnsDef { IDNo = 9, ColName = "ComplaintType", sTitle = "Complaint Type", sWidth = "55px", bSortable = false });
        
        var retVal = from q in aoColumns.AsEnumerable()
                     select new { sTitle = q.sTitle, sWidth = q.sWidth, bSortable = q.bSortable, sType = q.sType, sClass = q.sClass, q.SeqNo };
        return retVal;
    }

    private static void CreateAndWriteFile(List<ComplaintList> result)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        User UserData = (User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\ComplaintMonitor." + HttpContext.Current.Session.SessionID;
        if (File.Exists(path))
            File.Delete(path);
        FileStream f = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
        try {
            StreamWriter writer = new StreamWriter(f);
            string compress = TvTools.GZipCompres.Compress(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            writer.Write(compress);
            writer.Close();
        }
        catch (Exception) {
            throw;
        }
        finally {
            f.Close();
        }
    }
}
