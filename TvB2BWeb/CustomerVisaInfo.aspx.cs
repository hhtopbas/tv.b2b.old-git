﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Data;
using TvTools;
using System.Web.Services;
using System.Text;
using System.Web.Script.Services;

public partial class CustomerVisaInfo : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<TvReport.htmlCodeData> createResCustDiv(int? custNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == custNo);

        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();

        bool _custEdit = true;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) || Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
            _custEdit = false;
        bool passEdit = true;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && (ResData.ResMain.BegDate.Value - DateTime.Today).TotalDays < 8)
            passEdit = false;
        bool? showPIN = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "showPIN"));
        bool? showPassportInfo = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ShowPassportInfo"));

        htmlData.Add(new TvReport.htmlCodeData { IdName = "hfCustNo", TagName = "input", Data = custNo.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "lblResNo", TagName = "span", Data = ResData.ResMain.ResNo.ToString() });
        string customerStr = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ContactInfo").ToString(), resCust.Surname + " " + resCust.Name);
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtCustInfo", TagName = "span", Data = customerStr, Disabled = true });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtTitleCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "lblTitle").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtTitle", TagName = "input", Data = resCust.TitleStr.ToString(), Disabled = true });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtTitleCode", TagName = "input", Data = resCust.Title.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtSurnameCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "lblSurname").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtSurname", TagName = "input", Data = resCust.Surname.ToString(), Css = new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.surnameWrittingRules), Disabled = true });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtNameCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "lblName").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtName", TagName = "input", Data = resCust.Name.ToString(), Css = new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.nameWrittingRules), Disabled = true });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtBirtdayCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "lblBirtday").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtBirtday", TagName = "date", Data = resCust.Birtday.HasValue ? Newtonsoft.Json.JsonConvert.SerializeObject(resCust.Birtday.Value.ToUniversalTime()) : "", Disabled = true });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "editIDNoCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "lblIDNo").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "editIDNo", TagName = "input", Data = resCust.IDNo.ToString(), Css = !showPIN.HasValue || showPIN.Value ? "display: block;" : "display: none;", Disabled = true });

        htmlData.Add(new TvReport.htmlCodeData { IdName = "divPassSerie", TagName = "css", Data = "", Css = !showPassportInfo.HasValue || showPassportInfo.Value ? "display: block;" : "display: none;" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "editPassSerieCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "lblPassSerieNo").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtPassSerie", TagName = "input", Data = resCust.PassSerie.ToString(), Disabled = true });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtPassNo", TagName = "input", Data = resCust.PassNo.ToString(), Disabled = true });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "divPassIssueDateExpDateGiven", TagName = "css", Data = "", Css = !showPassportInfo.HasValue || showPassportInfo.Value ? "display: block;" : "display: none;" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtPassIssueDateCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "lblPassIssueDate").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtPassIssueDate", TagName = "date", Data = resCust.PassIssueDate.HasValue ? Newtonsoft.Json.JsonConvert.SerializeObject(resCust.PassIssueDate.Value.ToUniversalTime()) : "", Disabled = true });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtPassExpDateCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "lblPassExpDate").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtPassExpDate", TagName = "date", Data = resCust.PassExpDate.HasValue ? Newtonsoft.Json.JsonConvert.SerializeObject(resCust.PassExpDate.Value.ToUniversalTime()) : "", Disabled = true });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtPassGivenCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "lblPassGiven").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtPassGiven", TagName = "input", Data = resCust.PassGiven, Disabled = true });

        ResCustVisaRecord resCustVisa = ResData.ResCustVisa.Find(f => f.CustNo == custNo);
        if (resCustVisa == null)
            resCustVisa = new ResCustVisaRecord();

        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtBirthSurnameCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblBirthSurname").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtBirthSurname", TagName = "input", Data = Conversion.getStrOrNull(resCustVisa.BirthSurname), Disabled = !string.IsNullOrEmpty(resCustVisa.BirthSurname) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtPlaceOfBirthCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblPlaceOfBirth").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtPlaceOfBirth", TagName = "input", Data = Conversion.getStrOrNull(resCustVisa.PlaceOfBirth), Disabled = !string.IsNullOrEmpty(resCustVisa.PlaceOfBirth) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtCountryOfBirthCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblCountryOfBirth").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtCountryOfBirth", TagName = "input", Data = Conversion.getStrOrNull(resCustVisa.CountryOfBirth), Disabled = !string.IsNullOrEmpty(resCustVisa.CountryOfBirth) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtRemarkForChildCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblCountryOfBirth").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtRemarkForChild", TagName = "text", Data = Conversion.getStrOrNull(resCustVisa.RemarkForChild), Disabled = !string.IsNullOrEmpty(resCustVisa.RemarkForChild) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtGivenByCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblGivenBy").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtGivenBy", TagName = "input", Data = Conversion.getStrOrNull(resCustVisa.GivenBy), Disabled = !string.IsNullOrEmpty(resCustVisa.GivenBy) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtJobCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblJob").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtJob", TagName = "input", Data = Conversion.getStrOrNull(resCustVisa.Job), Disabled = !string.IsNullOrEmpty(resCustVisa.Job) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtEmployerCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblEmployer").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtEmployer", TagName = "input", Data = Conversion.getStrOrNull(resCustVisa.Employer), Disabled = !string.IsNullOrEmpty(resCustVisa.Employer) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtEmpAddressCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblEmpAddress").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtEmpAddress", TagName = "input", Data = Conversion.getStrOrNull(resCustVisa.EmpAddress), Disabled = !string.IsNullOrEmpty(resCustVisa.EmpAddress) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtEmpTelCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblEmpTel").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtEmpTel", TagName = "input", Data = Conversion.getStrOrNull(resCustVisa.EmpTel), Disabled = !string.IsNullOrEmpty(resCustVisa.EmpTel) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtInvitePersonCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblInvitePerson").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtInvitePerson", TagName = "input", Data = Conversion.getStrOrNull(resCustVisa.InvitePerson), Disabled = !string.IsNullOrEmpty(resCustVisa.InvitePerson) });
        string sexList = "[{" + string.Format("\"Code\":\"{0}\",\"Name\":\"{1}\"", "M", HttpContext.GetGlobalResourceObject("LibraryResource", "sexM").ToString()) + "},";
        sexList += "{" + string.Format("\"Code\":\"{0}\",\"Name\":\"{1}\"", "M", HttpContext.GetGlobalResourceObject("LibraryResource", "sexM").ToString()) + "}]";
        List<TitleRecord> titles = new TvBo.Common().getTitle(ref errorMsg);
        TitleRecord title = titles.Find(f => f.TitleNo == resCust.Title);
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtSexCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblSex").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtSex", TagName = "combo", Data = sexList, Value = title != null ? title.Sex : "" });

        string maritalStatus = string.Empty;
        maritalStatus += "{" + string.Format("\"Code\":\"{0}\",\"Name\":\"{1}\"", "0", HttpContext.GetGlobalResourceObject("LibraryResource", "maritial0").ToString()) + "},";
        maritalStatus += "{" + string.Format("\"Code\":\"{0}\",\"Name\":\"{1}\"", "1", HttpContext.GetGlobalResourceObject("LibraryResource", "maritial1").ToString()) + "},";
        maritalStatus += "{" + string.Format("\"Code\":\"{0}\",\"Name\":\"{1}\"", "2", HttpContext.GetGlobalResourceObject("LibraryResource", "maritial2").ToString()) + "},";
        maritalStatus += "{" + string.Format("\"Code\":\"{0}\",\"Name\":\"{1}\"", "3", HttpContext.GetGlobalResourceObject("LibraryResource", "maritial3").ToString()) + "},";
        maritalStatus += "{" + string.Format("\"Code\":\"{0}\",\"Name\":\"{1}\"", "4", HttpContext.GetGlobalResourceObject("LibraryResource", "maritial4").ToString()) + "},";
        maritalStatus += "{" + string.Format("\"Code\":\"{0}\",\"Name\":\"{1}\"", "5", HttpContext.GetGlobalResourceObject("LibraryResource", "maritial5").ToString()) + "}";
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtMaritalCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblMarital").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtMarital", TagName = "combo", Data = "[" + maritalStatus + "]", Value = Conversion.getStrOrNull(resCustVisa.Marital) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtMaritalRemarkCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblMaritalRemark").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtMaritalRemark", TagName = "input", Data = Conversion.getStrOrNull(resCustVisa.MaritalRemark) });

        string visaCount = string.Empty;
        visaCount += "{" + string.Format("\"Code\":\"{0}\",\"Name\":\"{1}\"", "0", "0") + "},";
        visaCount += "{" + string.Format("\"Code\":\"{0}\",\"Name\":\"{1}\"", "1", "1 " + HttpContext.GetGlobalResourceObject("LibraryResource", "lbltime").ToString()) + "},";
        visaCount += "{" + string.Format("\"Code\":\"{0}\",\"Name\":\"{1}\"", "2", "2 " + HttpContext.GetGlobalResourceObject("LibraryResource", "lbltimes").ToString()) + "},";
        visaCount += "{" + string.Format("\"Code\":\"{0}\",\"Name\":\"{1}\"", "3", "3 " + HttpContext.GetGlobalResourceObject("LibraryResource", "lbltimes").ToString()) + "},";
        visaCount += "{" + string.Format("\"Code\":\"{0}\",\"Name\":\"{1}\"", "4", "4 " + HttpContext.GetGlobalResourceObject("LibraryResource", "lbltimes").ToString()) + "}";
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtVisaCountCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblVisaCount").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtVisaCount", TagName = "combo", Data = "[" + visaCount + "]", Value = resCustVisa.VisaCount.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtValidFromCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblValidFrom").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtValidFrom", TagName = "date", Data = resCustVisa.ValidFrom.HasValue ? Newtonsoft.Json.JsonConvert.SerializeObject(resCustVisa.ValidFrom.Value) : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtValidTillCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblValidTill").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtValidTill", TagName = "date", Data = resCustVisa.ValidTill.HasValue ? Newtonsoft.Json.JsonConvert.SerializeObject(resCustVisa.ValidTill.Value) : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtValidFrom1Cap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblValidFrom").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtValidFrom1", TagName = "date", Data = resCustVisa.ValidFrom2.HasValue ? Newtonsoft.Json.JsonConvert.SerializeObject(resCustVisa.ValidFrom2.Value) : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtValidTill1Cap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblValidTill").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtValidTill1", TagName = "date", Data = resCustVisa.ValidTill2.HasValue ? Newtonsoft.Json.JsonConvert.SerializeObject(resCustVisa.ValidTill2.Value) : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtValidFrom2Cap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblValidFrom").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtValidFrom2", TagName = "date", Data = resCustVisa.ValidFrom3.HasValue ? Newtonsoft.Json.JsonConvert.SerializeObject(resCustVisa.ValidFrom3.Value) : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtValidTill2Cap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblValidTill").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtValidTill2", TagName = "date", Data = resCustVisa.ValidTill3.HasValue ? Newtonsoft.Json.JsonConvert.SerializeObject(resCustVisa.ValidTill3.Value) : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtValidFrom3Cap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblValidFrom").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtValidFrom3", TagName = "date", Data = resCustVisa.ValidFrom4.HasValue ? Newtonsoft.Json.JsonConvert.SerializeObject(resCustVisa.ValidFrom4.Value) : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtValidTill3Cap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblValidTill").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtValidTill3", TagName = "date", Data = resCustVisa.ValidTill3.HasValue ? Newtonsoft.Json.JsonConvert.SerializeObject(resCustVisa.ValidTill4.Value) : "" });

        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtAddressCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblAddress").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtAddress", TagName = "text", Data = Conversion.getStrOrNull(resCustVisa.Address) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtTelCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblTel").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtTel", TagName = "input", Data = Conversion.getStrOrNull(resCustVisa.Tel) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtFaxCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblFax").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtFax", TagName = "input", Data = Conversion.getStrOrNull(resCustVisa.Fax) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtVisaNoCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblVisaNo").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtVisaNo", TagName = "input", Data = Conversion.getStrOrNull(resCustVisa.VisaNo) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtDepCityCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblDeparture").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtDepCity", TagName = "input", Data = string.IsNullOrEmpty(resCustVisa.DepCity) ? ResData.ResMain.DepCityName : Conversion.getStrOrNull(resCustVisa.DepCity), Disabled = !string.IsNullOrEmpty(resCustVisa.DepCity) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtArrCityCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblArrival").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtArrCity", TagName = "input", Data = string.IsNullOrEmpty(resCustVisa.ArrCity) ? ResData.ResMain.ArrCityName : Conversion.getStrOrNull(resCustVisa.ArrCity), Disabled = !string.IsNullOrEmpty(resCustVisa.ArrCity) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtAgencyNameCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblAgencyName").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtAgencyName", TagName = "input", Data = string.IsNullOrEmpty(resCustVisa.AgencyName) ? UserData.AgencyName.ToString() : Conversion.getStrOrNull(resCustVisa.AgencyName), Disabled = !string.IsNullOrEmpty(resCustVisa.AgencyName) });

        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtAgencyAddressCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblAgencyAddress").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtAgencyAddress", TagName = "input", Data = string.IsNullOrEmpty(resCustVisa.AgencyAddress) ? UserData.AgencyRec.Address.ToString() : Conversion.getStrOrNull(resCustVisa.AgencyAddress), Disabled = !string.IsNullOrEmpty(resCustVisa.AgencyAddress) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtAgencyTelCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblAgencyTel").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtAgencyTel", TagName = "input", Data = string.IsNullOrEmpty(resCustVisa.AgencyTel) ? UserData.AgencyRec.Phone1.ToString() : Conversion.getStrOrNull(resCustVisa.AgencyTel), Disabled = !string.IsNullOrEmpty(resCustVisa.AgencyTel) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtAgencyFaxCap", TagName = "span", Data = HttpContext.GetGlobalResourceObject("CustomerInfo", "vlblAgencyFax").ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "edtAgencyFax", TagName = "input", Data = string.IsNullOrEmpty(resCustVisa.AgencyFax) ? UserData.AgencyRec.Fax1.ToString() : Conversion.getStrOrNull(resCustVisa.AgencyFax), Disabled = !string.IsNullOrEmpty(resCustVisa.AgencyFax) });

        //htmlData.Add(new TvReport.htmlCodeData { IdName = "", TagName = "span", Data = "" });

        return htmlData;
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string retVal = UserData.Ci.TwoLetterISOLanguageName;
        return retVal;
    }

}
