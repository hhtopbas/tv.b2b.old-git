﻿using System;
using System.Collections.Generic;
using System.Linq;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Text;
using System.Web.Services;
using TvTools;
using System.IO;
using BankIntegration;
using System.Web.Script.Services;
using Winnovative;
using System.Configuration;

public partial class HotelSale : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        if (Session["SearchMixData"] == null)
        {
            bool GroupSearch = false;
            object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
            if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
            MixSearchFilterCacheData filterData = MixSearch.getMixPackageSearchData(UserData, GroupSearch, SearchType.HotelSale);
            Session["SearchMixData"] = filterData;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static bool UserHasAuth()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return false; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        return UserData.BlackList ? false : true;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static PackageSearchMakeRes getBookReservation(string BookList)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        object extAllotControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ExtAllotControl");
        bool ExtAllotCont = Conversion.getBoolOrNull(extAllotControl).HasValue ? Conversion.getBoolOrNull(extAllotControl).Value : false;

        object _checkAvailableRoom = new TvBo.Common().getFormConfigValue("SearchPanel", "CheckAvailableRoom");
        bool checkAvailableRoom = Conversion.getBoolOrNull(_checkAvailableRoom).HasValue ? Conversion.getBoolOrNull(_checkAvailableRoom).Value : false;

        string version = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "Version"));
        string onlyHotelVersion = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "OnlyHotelVersion"));

        string errorMsg = string.Empty;

        HttpContext.Current.Session["ResData"] = null;
        TvBo.MixSearchCriteria paxCriteria = (TvBo.MixSearchCriteria)HttpContext.Current.Session["MixCriteria"];

        List<SearchResultOH> searchResult = (List<SearchResultOH>)HttpContext.Current.Session["SearchResultOH"];

        List<int> _bookList = new List<int>();
        for (int i = 0; i < BookList.Split(',').Length; i++)
            _bookList.Add(Convert.ToInt32(BookList.Split(',')[i]));
        List<SearchResultOH> selectedRoom = (from q in searchResult
                                             join q1 in _bookList.AsEnumerable() on q.RefNo equals q1
                                             select q).ToList<SearchResultOH>();

        ResDataRecord ResData = new ResDataRecord();
        ResData.SelectBookOH = new List<SearchResultOH>();
        foreach (int row in _bookList)
        {
            SearchResultOH sr = new SearchResultOH();
            MixSearchCriteriaRoom sc = new MixSearchCriteriaRoom();
            sr = searchResult.Find(f => f.RefNo == row);
            sc = paxCriteria.RoomInfoList.Find(f => f.RoomNr == sr.RoomNr);
            ResData.SelectBookOH.Add(sr);
        }

        ResData = new MakeSearch2Res().getResDataHotelSale(UserData, ResData, paxCriteria, ref errorMsg);
        if (!string.IsNullOrEmpty(errorMsg)) {
            return new PackageSearchMakeRes { resOK = false, errMsg = errorMsg, version = version };
        }
        ResData.FirstData = new ResTables().getFirstResData(ResData);
        ResData.FirstData.ReservationType = SearchType.HotelSale;

        List<ResServiceRecord> resServiceList = ResData.ResService;
        foreach (ResServiceRecord row in resServiceList)
            row.ChkCelMakeRes2 = false;
        List<ResServiceExtRecord> resServiceExtList = ResData.ResServiceExt;
        foreach (ResServiceExtRecord row in resServiceExtList)
            row.ChkCelMakeRes2 = false;

        if (string.Equals(version, "V2"))
        {
            ResData = new ReservationV2().getResDataExtras(UserData, ResData, ref errorMsg);
        }
        if (ResData.ExtrasData == null)
        {
            ResData.ExtrasData = ResData;
        }

        bool CreateChildAge = false;
        if (UserData.WebService)
            CreateChildAge = true;
        else
        {
            object _createChildAge = new TvBo.Common().getFormConfigValue("MakeReservation", "CreateChildAge");
            CreateChildAge = _createChildAge != null ? (bool)_createChildAge : false;
        }

        if (!CreateChildAge)
        {
            foreach (ResCustRecord row in ResData.ResCust)
            {
                row.Age = null;
                row.Birtday = null;
            }
            if (ResData.ExtrasData == null)
            {
                ResData.ExtrasData = new ResDataRecord();
                ResData.ExtrasData = ResData;
            }
            foreach (ResCustRecord row in ResData.ExtrasData.ResCust)
            {
                row.Age = null;
                row.Birtday = null;
            }
        }
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka))
        {
            ResMainRecord resMain = ResData.ResMain;
            resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
        }
        HttpContext.Current.Session["ResData"] = ResData;
        return new PackageSearchMakeRes { resOK = true, errMsg = string.Empty, version = version };
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static PackageSearchMakeRes getMixBookReservation(string BookList)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        HttpContext.Current.Session["ResData"] = null;
        TvBo.MixSearchCriteria criteria = (TvBo.MixSearchCriteria)HttpContext.Current.Session["MixCriteria"];
        List<string> _bookList = new List<string>();
        for (int i = 0; i < BookList.Split(',').Length; i++)
            _bookList.Add(BookList.Split(',')[i]);
        MixResData ResData = new MixResData();

        GetHotelOfferDetailsResponse offerDetails = new MixSearchPaximum().getOfferDetails(UserData, BookList, ref errorMsg);
        if (offerDetails == null)
        {
            switch (errorMsg)
            {
                case "StopSale":
                    errorMsg = "Your offer's availability has been changed. Please search another criteries";
                    break;
                default:
                    errorMsg = "Error.(Internal)";
                    break;
            }
            return new PackageSearchMakeRes { resOK = false, errMsg = errorMsg, version = "MIX" };
        }
        CreateOrder pParams = new CreateOrder();

        //pParams.UserId = "ee98baab-8207-4bee-b792-a2ed0479d19f";//paximumUserToken.sub;
        pParams.Travellers = new Traveller[0];
        Room[] rooms = offerDetails.Rooms;
        List<BookRoom> bookRoom = new List<BookRoom>();
        List<Traveller> customers = new List<Traveller>();
        bool leader = true;
        foreach (Room row in rooms)
        {
            BookRoom bookR = new BookRoom();
            foreach (var cRow in row.Travellers)
            {
                cRow.TravellerNo = Guid.NewGuid().ToString();
                cRow.isLead = leader;
                leader = false;
                customers.Add(cRow);
            }
            bookR.RoomId = row.Id;
            bookR.Travellers = row.Travellers.Select(s => s.TravellerNo).ToArray();
            bookRoom.Add(bookR);
        }

        pParams.Travellers = customers.ToArray();
        pParams.HotelBookings = new HotelBooking[] {new HotelBooking(){
                    OfferId=offerDetails.Id,                    
                    ReservationNote="",                    
                    Rooms= bookRoom.ToArray()
                }
            };

        ResData.offerDetail = offerDetails;
        ResData.order = pParams;

        HttpContext.Current.Session["ResData"] = ResData;

        return new PackageSearchMakeRes { resOK = true, errMsg = string.Empty, version = "MIX" };
    }

    private static BookRoom[] BookRoomMap(Room[] room)
    {
        var bookRoom = new List<BookRoom>();
        bookRoom = room.Select(x =>
        {
            List<string> cust = new List<string>();
            foreach (Traveller r1 in x.Travellers)
                cust.Add(Guid.NewGuid().ToString());
            return new BookRoom
            {
                RoomId = x.Id,
                Travellers = cust.ToArray()
            };
        }).ToList();
        return bookRoom.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string viewPDFReport(string reportType, string _html, string pageWidth, string urlBase)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        _html = _html.Replace('|', '"');
        _html = _html.Replace("<div id=\"noPrintDiv\">", "<div id=\"noPrintDiv\" style=\"display: none; visibility: hidden;\">");
        string errorMsg = string.Empty;
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        string pdfFileName = reportType + "_" + UserData.AgencyID + ".pdf";
        StringBuilder html = new StringBuilder(_html);
        int PageWidth = Conversion.getInt32OrNull(pageWidth).HasValue ? Conversion.getInt32OrNull(pageWidth).Value : 700;
        //initialize the PdfConvert object
        PdfConverter pdfConverter = new PdfConverter();
        pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
        pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.NoCompression;
        pdfConverter.PdfDocumentOptions.ShowHeader = false;
        pdfConverter.PdfDocumentOptions.ShowFooter = false;
        pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
        pdfConverter.PdfDocumentOptions.LeftMargin = 10;
        pdfConverter.PdfDocumentOptions.TopMargin = 10;
        // set the demo license key
        pdfConverter.LicenseKey = ConfigurationManager.AppSettings["Winnovative"]; ;

        // get the base url for string conversion which is the url from where the html code was retrieved
        // the base url is a hint for the converter to find the external CSS and images referenced by relative URLs
        string thisPageURL = HttpContext.Current.Request.Url.AbsoluteUri;
        string baseUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/";
        try
        {
            pdfConverter.SavePdfFromHtmlStringToFile(html.ToString(), siteFolderISS + "ACE\\" + pdfFileName, urlBase);
            string returnUrl = basePageUrl + "ACE/" + pdfFileName;
            return returnUrl;
        }
        catch
        {
            return "";
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string getBrochure(string holPack, long? CheckIn, long? CheckOut, string Market)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        DateTime begDate = CheckIn.HasValue ? new DateTime(CheckIn.Value) : DateTime.Today;
        DateTime endDate = CheckOut.HasValue ? new DateTime(CheckOut.Value) : DateTime.Today;

        string errorMsg = string.Empty;
        HolPackBrochureRecord holPackBrochure = new TvBo.Common().getHolPackBrochure(Market, holPack, begDate, endDate, ref errorMsg);
        if (holPackBrochure != null)
        {
            string fileName = holPack + "_" + CheckIn.ToString() + "_" + CheckOut.ToString() + ".PDF";
            if (File.Exists(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName))
                File.Delete(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName);
            FileStream fileStream = new FileStream(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName, FileMode.Create, FileAccess.ReadWrite);
            fileStream.Write((byte[])holPackBrochure.FileImage, 0, ((byte[])holPackBrochure.FileImage).Length);
            fileStream.Close();

            return WebRoot.BasePageRoot + "/ACE/" + fileName;
        }
        else return "";

    }
    [WebMethod(EnableSession = true)]

    public static string getOffersUrl()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        object _ppd = new TvBo.Common().getFormConfigValue("General", "b2cOffersUrl");
        List<BIPaymentPageDefination> ppd = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BIPaymentPageDefination>>(Conversion.getStrOrNull(_ppd));
        if (ppd != null)
        {
            BIPaymentPageDefination offersUrl = ppd.Find(w => w.Market == UserData.Market && string.Equals(w.Code, "PACK"));
            return offersUrl != null ? offersUrl.B2CUrl : string.Empty;
        }
        else
            return string.Empty;

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getHotelDetais(string hotelId)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        HotelDetailsResponse hotelDetails = new MixSearchPaximum().getHotelDetais(UserData, hotelId, ref errorMsg);

        return new
        {
            Raiting = hotelDetails.Rating.ToString("#.00"),
            HotelImageUrl = "http://media.dev.paximum.com/hotelimages_125x125/" + hotelDetails.Photos.FirstOrDefault(),
            HotelName = hotelDetails.Name,
            StarStr = hotelDetails.Stars.ToString().Replace(",", "").Replace(".", ""),
            HotelLocation = hotelDetails.City.name + " / " + hotelDetails.Country.name,
            Description = hotelDetails.Description,
            Facilities = hotelDetails.Facilities.Select(s => s.Description).ToArray(),
            HotelDescription = hotelDetails.Content.FirstOrDefault().Description,
            Photos = hotelDetails.Photos
        };
    }

}
