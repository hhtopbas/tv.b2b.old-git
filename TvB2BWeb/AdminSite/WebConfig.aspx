﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebConfig.aspx.cs" Inherits="AdminSite_WebConfig"
    EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <link href="CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/confirm.css" rel="stylesheet" type="text/css" />
    <link href="CSS/WebConfig.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function getFormData() {
            $.ajax({
                type: "POST",
                url: "WebConfig.aspx/getWebConfigData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#divWebConfig").html('');
                    $("#divWebConfig").html(msg.d);
                },
                error: function(xhr, msg, e) {
                    if (xhr.status == 408) {
                        self.parent.logout();
                    }
                    else
                        alert(xhr.responseText);
                }
            });
        }

        function pageLoad() {
            getFormData();
        }
    </script>

</head>
<body onload="javascript:pageLoad();">
    <form id="webConfigForm" runat="server">
    <div id="divWebConfig">
    </div>
    </form>
</body>
</html>
