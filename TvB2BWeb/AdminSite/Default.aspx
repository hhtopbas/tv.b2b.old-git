﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Admin_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Agency Management Page</title>

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <link href="../CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">        
	</script>

</head>
<body>
    <form id="form1" runat="server">
    <div style="font-family: Verdana;">
        <div style="text-align: center; background-color:#BCBCBC">
            <br />
            <span style="font-size: 12pt;"><b>Web Management Page</b></span>
            <br />
            <br />
        </div>
        <br />
        <div style="text-align: center">
            <table style="border: solid 1px #000000;">
                <tr>
                    <td>
                        <asp:Label ID="lblAgencyCode" runat="server" Text="Code:" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtAgencyCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMasterPass" runat="server" Text="Password:" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <asp:Button ID="btnLogin" runat="server" Text="Login" 
                            onclick="btnLogin_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
