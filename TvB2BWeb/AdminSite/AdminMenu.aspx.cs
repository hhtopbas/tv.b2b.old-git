﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TvBo;

public partial class Admin_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["WebAdmin"] == null)
        {
            Response.Redirect("Default.aspx");
            return;
        }
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(WebRoot.BasePageRoot))
            {
                //string BasePageUrl = string.Empty;

                //BasePageUrl = Request.Url.ToString().Replace("/AdminMenu.aspx", "");
                //Session["BasePageUrl"] = BasePageUrl;
                Response.Redirect(WebRoot.BasePage);
            }
        }
    }

    protected void btnWebConfig_Click(object sender, EventArgs e)
    {
        string controlName = Request.Params.Get("__EVENTTARGET");
        string passedArgument = Request.Params.Get("__EVENTARGUMENT");
        string errorMsg = string.Empty;
        List<MarketRecord> markets = new Parameters().getMarkets(ref errorMsg);
        List<OperatorRecord> operators = new Parameters().getOperators("", ref errorMsg);
        if (Equals(controlName, "btnWebConfig"))
        {
            switch (passedArgument)
            {
                case "0":
                    CacheHelper.Clear("WebConfig"); //1
                    CacheHelper.Clear("OprLogo");//2
                    CacheHelper.Clear("AgencyLogo");//2
                    CacheHelper.Clear("Nationality");//2
                    var keysToClear3All = (from System.Collections.DictionaryEntry dict in HttpContext.Current.Cache
                                           let key = dict.Key.ToString()
                                           where key.StartsWith("Package_") ||
                                                 key.StartsWith("OnlyHotel_") ||
                                                 key.StartsWith("OnlyHotelMix_") ||
                                                 key.StartsWith("HotelSale_") ||
                                                 key.StartsWith("Other_") ||
                                                 key.StartsWith("CultureTour_") ||
                                                 key.StartsWith("None_") ||
                                                 key.IndexOf("OnlyHotel_") > 0
                                           select key).ToList();
                    foreach (var row in keysToClear3All)
                        CacheHelper.Clear(row.ToString());//3

                    foreach (TvBo.MarketRecord rowAll in markets)
                    {
                        CacheHelper.Clear(string.Format("FlightDays_{0}", rowAll.Code));//4
                        CacheHelper.Clear(string.Format("Location_{0}", rowAll.Code));//8
                        CacheHelper.Clear(string.Format("Market_{0}", rowAll.Code));//9
                        CacheHelper.Clear(string.Format("Operator_{0}", rowAll.Code));//10
                        CacheHelper.Clear(string.Format("Nation_{0}", rowAll.Code));//11
                        CacheHelper.Clear(string.Format("HotelHandikap_{0}", rowAll.Code));//14
                    }

                    var keysToClear5All = (from System.Collections.DictionaryEntry dict in HttpContext.Current.Cache
                                           let key = dict.Key.ToString()
                                           where key.StartsWith("MainMenu_")
                                           select key).ToList();
                    foreach (var row in keysToClear5All)
                        CacheHelper.Clear(row.ToString());//5
                    CacheHelper.Clear("ResViewMenu");//6
                    CacheHelper.Clear("MakeResMenu");//7
                    break;
                case "1": CacheHelper.Clear("WebConfig"); break;
                case "2":
                    CacheHelper.Clear("OprLogo");
                    CacheHelper.Clear("AgencyLogo");
                    break;
                case "3":
                    var keysToClear3 = (from System.Collections.DictionaryEntry dict in HttpContext.Current.Cache
                                        let key = dict.Key.ToString()
                                        where key.StartsWith("Package_") ||
                                              key.StartsWith("OnlyHotel_") ||
                                              key.StartsWith("OnlyHotelMix_") ||
                                              key.StartsWith("HotelSale_") ||
                                              key.StartsWith("Other_") ||
                                              key.StartsWith("CultureTour_") ||
                                              key.StartsWith("None_") ||
                                              key.IndexOf("OnlyHotel_") > 0
                                        select key).ToList();
                    foreach (var row in keysToClear3)
                        CacheHelper.Clear(row.ToString());
                    break;
                case "4":
                    foreach (TvBo.MarketRecord rowM in markets)
                    {
                        CacheHelper.Clear(string.Format("FlightDays_{0}", rowM.Code));
                    }
                    break;
                case "5":
                    var keysToClear5 = (from System.Collections.DictionaryEntry dict in HttpContext.Current.Cache
                                        let key = dict.Key.ToString()
                                        where key.StartsWith("MainMenu_")
                                        select key).ToList();
                    foreach (var row in keysToClear5)
                        CacheHelper.Clear(row.ToString());
                    break;
                case "6":
                    CacheHelper.Clear("ResViewMenu");
                    break;
                case "7":
                    CacheHelper.Clear("MakeResMenu");
                    break;
                case "8":
                    foreach (TvBo.MarketRecord rowL in markets)
                    {
                        CacheHelper.Clear(string.Format("Location_{0}", rowL.Code));
                    }
                    break;
                case "9":
                    foreach (TvBo.MarketRecord rowM in markets)
                    {
                        CacheHelper.Clear(string.Format("Market_{0}", rowM.Code));
                    }
                    break;
                case "10":
                    foreach (TvBo.MarketRecord rowO in markets)
                    {
                        CacheHelper.Clear(string.Format("Operator_{0}", rowO.Code));
                    }
                    break;
                case "11":
                    foreach (TvBo.MarketRecord rowN in markets)
                    {
                        CacheHelper.Clear(string.Format("Nation_{0}", rowN.Code));
                    }
                    break;
                case "12":
                    var keysToClear6 = (from System.Collections.DictionaryEntry dict in HttpContext.Current.Cache
                                        let key = dict.Key.ToString()
                                        where key.StartsWith("MarketPriceListFixNote_")
                                        select key).ToList();
                    foreach (var row in keysToClear6)
                        CacheHelper.Clear(row.ToString());
                    break;
                case "13":
                    var keysToClear7 = (from System.Collections.DictionaryEntry dict in HttpContext.Current.Cache
                                        let key = dict.Key.ToString()
                                        where key.StartsWith("GetAllAnnounce_")
                                        select key).ToList();

                    foreach (var row in keysToClear7)
                        CacheHelper.Clear(row.ToString());

                    keysToClear7 = (from System.Collections.DictionaryEntry dict in HttpContext.Current.Cache
                                    let key = dict.Key.ToString()
                                    where key.StartsWith("GetAnnounce_")
                                    select key).ToList();

                    foreach (var row in keysToClear7)
                        CacheHelper.Clear(row.ToString());
                    break;
                case "14":
                    var keysToClear8 = (from System.Collections.DictionaryEntry dict in HttpContext.Current.Cache
                                        let key = dict.Key.ToString()
                                        where key.StartsWith("HotelHandikap_")
                                        select key).ToList();
                    foreach (var row in keysToClear8)
                        CacheHelper.Clear(row.ToString());
                    break;
            }
        }
    }
}
