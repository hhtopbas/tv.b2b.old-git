﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using TvBo;
using System.Text;

public partial class AdminSite_WebConfig : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public static string createPhoneMask(string value)
    {
        List<PhoneMaskRecord> list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PhoneMaskRecord>>(value);
        if (list.Count > 0)
        {
            string table = string.Empty;
            string tableRows = string.Empty;
            table += "<table><tr>";
            table += string.Format("<td>{0}</td><td>{1}</td><td>{2}</td>", 
                "Market",
                "Phone country code",
                "Phone number mask");
            table += "</tr>";
            foreach (PhoneMaskRecord row in list)
            {
                tableRows += string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>",
                    row.Market,
                    row.CountryCode,
                    row.PhoneMask);
            }
            table += tableRows;
            table += "</table>";
            return table;
        }
        else return "";
    }

    [WebMethod(EnableSession = true)]
    public static string getWebConfigData()
    {
        List<WebConfig> webConfigRecordList = new UICommon().getWebConfig();
        StringBuilder sb = new StringBuilder();
        foreach (WebConfig row in webConfigRecordList)
        {
            switch (row.TYPES)
            { 
                case "jSon":
                    row.FIELDVALUE = row.FIELDVALUE.Replace('\n',' ').Replace('\r',' ').TrimStart(' ').TrimEnd(' ');
                    switch (row.FIELDTYPE)
                    {
                        case "PhoneMaskRecord":
                            sb.Append(createPhoneMask(row.FIELDVALUE));
                            break;
                        default: break;
                    }
                    break;
                case "Default":
                    switch (row.FIELDTYPE)
                    {
                        case "":
                            break;
                        default:
                            break;
                    }
                    break;
                default :
                    break;
            }
        }
        return sb.ToString();
    }
}
