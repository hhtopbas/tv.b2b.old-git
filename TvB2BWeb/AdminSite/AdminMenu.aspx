﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdminMenu.aspx.cs" Inherits="Admin_Default"
    EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Web Management Page</title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/stuHover.js" type="text/javascript"></script>

    <script src="Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <link href="CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/pro_dropdown_3.css" rel="stylesheet" type="text/css" />
    <link href="CSS/confirm.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function confirm(message, callback) {
            $('#confirm').modal({
                closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
                position: ["20%", ],
                overlayId: 'confirm-overlay',
                containerId: 'confirm-container',
                onShow: function(dialog) {
                    $('.message', dialog.data[0]).append(message);

                    // if the user clicks "yes"
                    $('.yes', dialog.data[0]).click(function() {
                        // call the callback
                        if ($.isFunction(callback)) {
                            callback.apply();
                        }
                        // close the dialog
                        $.modal.close();
                    });
                }
            });
        }

        function runCacheClear(whereToRun) {
            confirm('Clear Cache',
                    function() {
                        __doPostBack('btnWebConfig', whereToRun, '');
                    }
            );
        }

        function webConfigEdit() {
            var _url = 'WebConfig.aspx';
            $("#WebConfig").attr("src", _url);
            $("#dialog").dialog("destroy");
            $("#dialog-WebConfig").dialog(
                {
                    autoOpen: true,
                    modal: true,
                    width: 1000,
                    height: 700,
                    resizable: true,
                    autoResize: false,
                    bigframe: true
                });
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="smSiteAdmin" runat="server" />
    <asp:UpdatePanel ID="upSiteAdmin" runat="server">
        <ContentTemplate>
            <div style="font-family: Verdana;">
                <div style="text-align: center; background-color: #BCBCBC">
                    <br />
                    <span style="font-size: 12pt;"><b>Web Management Page</b></span>
                    <br />
                    <br />
                </div>
                <span class="preload1"></span><span class="preload2"></span>
                <ul id="nav">
                    <li class="top"><a href="#" class="top_link"><span>Home</span></a></li>
                    <li class="top"><a href="#" id="cacheClear" class="top_link"><span class="down">Cache
                        Clear</span></a>
                        <ul class="sub">
                            <%--<li><a href="#" onclick="runCacheClear(-1);">ReStart Application</a></li>--%>
                            <li><a href="#" onclick="runCacheClear(0);">Clear All</a></li>
                            <li><a href="#" onclick="runCacheClear(1);">Web Configuration</a></li>
                            <li><a href="#" onclick="runCacheClear(2);">Logo</a></li>
                            <li><a href="#" onclick="runCacheClear(3);">Pricelist Filters</a></li>
                            <li><a href="#" onclick="runCacheClear(4);">Flight Days</a></li>
                            <li><a href="#" onclick="runCacheClear(5);">Main Menu</a></li>
                            <li><a href="#" onclick="runCacheClear(6);">Reservation View Menu</a></li>
                            <li><a href="#" onclick="runCacheClear(7);">Make Reservation Menu</a></li>
                            <li><a href="#" onclick="runCacheClear(8);">Location List</a></li>
                            <li><a href="#" onclick="runCacheClear(9);">Market List</a></li>
                            <li><a href="#" onclick="runCacheClear(10);">Operator List</a></li>
                            <li><a href="#" onclick="runCacheClear(11);">Nation List</a></li>
                            <li><a href="#" onclick="runCacheClear(12);">Price List Fix Notes</a></li>
                            <li><a href="#" onclick="runCacheClear(13);">All Announcements</a></li>
                            <li><a href="#" onclick="runCacheClear(14);">Hotel Handikaps</a></li>
                            
                        </ul>
                    </li>
                    <%--<li class="top"><a href="#" onclick="webConfigEdit();" class="top_link"><span>Web Config</span></a></li>--%>
                </ul>
            </div>
            <div style="display: none;">
                <asp:Button ID="btnWebConfig" runat="server" OnClick="btnWebConfig_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="confirm">
        <div class="header">
            <span>
                <%= GetGlobalResourceObject("LibraryResource", "msgConfirm")%></span></div>
        <p class="message">
        </p>
        <div class="buttons">
            <div class="no simplemodal-close">
                <%= GetGlobalResourceObject("LibraryResource", "btnNo")%></div>
            <div class="yes">
                <%= GetGlobalResourceObject("LibraryResource", "btnYes")%></div>
        </div>
    </div>
    <div id="dialog-WebConfig" title="Web Config" style="display: none;">
        <iframe id="WebConfig" runat="server" height="100%" width="960px" frameborder="0"
            style="clear: both;"></iframe>
    </div>
    </form>
</body>
</html>
