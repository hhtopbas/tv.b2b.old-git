﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using TvBo;

public partial class Admin_Default : System.Web.UI.Page
{
    protected override void InitializeCulture()
    {
        base.InitializeCulture();
        String[] userLang = Request.UserLanguages;
        String[] CI = userLang[0].Split(';');
        string CultureStr = CI[0];
        CultureInfo[] allCulture = CultureInfo.GetCultures(CultureTypes.AllCultures);
        if (allCulture.Where(w => Equals(w.Name, CultureStr)).Count() < 1) {
            CultureStr = "en-US";
        }
        if (Equals(CultureStr, "nb")) CultureStr = "nb-NO"; if (Equals(CultureStr, "sr-SC")) CultureStr = "en-US";
        if (Equals(CultureStr, "ar") || Equals(CultureStr, "ar-SA")) CultureStr = "en-US";
        if (CultureStr.Length < 5)
        {
            CultureStr = (from culinf in CultureInfo.GetCultures(CultureTypes.AllCultures).AsEnumerable()
                          where culinf.Name == CultureStr
                          select new { CultureName = culinf.TextInfo.CultureName }).First().CultureName;
        }
        CultureInfo ci = new CultureInfo(CultureStr);
        Session["Culture"] = ci;
        if (Session["Culture"] != null)
        {
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo(((System.Globalization.CultureInfo)Session["Culture"]).Name, false);
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {            
            String[] userLang = Request.UserLanguages;
            String[] CI = userLang[0].Split(';');
            string CultureStr = CI[0];
            CultureInfo[] allCulture = CultureInfo.GetCultures(CultureTypes.AllCultures);
            if (allCulture.Where(w => Equals(w.Name, CultureStr)).Count() < 1) {
                CultureStr = "en-US";
            }
            if (Equals(CultureStr, "nb")) CultureStr = "nb-NO"; if (Equals(CultureStr, "sr-SC")) CultureStr = "en-US";
            if (Equals(CultureStr, "ar") || Equals(CultureStr, "ar-SA")) CultureStr = "en-US";
            if (CultureStr.Length < 5)
            {
                CultureStr = (from culinf in CultureInfo.GetCultures(CultureTypes.AllCultures).AsEnumerable()
                              where culinf.Name == CultureStr
                              select new { CultureName = culinf.TextInfo.CultureName }).First().CultureName;
            }
            CultureInfo ci = new CultureInfo(CultureStr);
            Session["Culture"] = ci;            
        } 
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(WebRoot.BasePageRoot))
        {
            Global.getBasePage();
            Global.getBasePageRoot();
            Response.Redirect(ResolveUrl("~/Default.aspx"));
        }        
        WebAdminUser WebAdmin = new WebAdminUser();

        WebAdmin.Code = txtAgencyCode.Text.ToUpper().Trim();
        WebAdmin.Password = txtPassword.Text;
        WebAdmin.Ci = (CultureInfo)Session["Culture"];
        
        string errorMsg = string.Empty;

        if (new AdmUser().tryLogin(ref WebAdmin, ref errorMsg))
        {
            Session["WebAdmin"] = WebAdmin;
            Response.Redirect("AdminMenu.aspx");
        }         
    }
}
