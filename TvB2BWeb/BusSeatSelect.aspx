﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BusSeatSelect.aspx.cs" Inherits="BusSeatSelect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "SeatSelect") %></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/BusSelectSeat.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        $(function() {
            var rx = /INPUT|TEXTAREA/i;
            var rxT = /RADIO|CHECKBOX|SUBMIT/i;

            $(document).bind("keydown keypress", function(e) {
                var preventKeyPress;
                if (e.keyCode == 8) {
                    var d = e.srcElement || e.target;
                    if (rx.test(e.target.tagName)) {
                        var preventPressBasedOnType = false;
                        if (d.attributes["type"]) {
                            preventPressBasedOnType = rxT.test(d.attributes["type"].value);
                        }
                        preventKeyPress = d.readOnly || d.disabled || preventPressBasedOnType;
                    } else { preventKeyPress = true; }
                } else { preventKeyPress = false; }

                if (preventKeyPress) e.preventDefault();
            });
        });

        function showAlert(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    position: 'center',
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                            return true;
                        }
                    }
                });
            });
        }

        function logout() {
            self.parent.logout();
        }

        function saveSeat(seatNo, custNo, service, busDate, bus) {
            var extraSeat = null;
            if ($("#extraSeat").length > 0)
            {
                if ($("#extraSeat").attr('checked') == 'checked')
                    extraSeat = $("#extraSeat").val();
            }
            $.ajax({
                type: "POST",
                url: "BusSeatSelect.aspx/saveSeat",
                data: '{"seatNo":' + seatNo + ',"custNo":' + custNo + ',"Service":"' + service + '","BusDate":' + busDate + ',"Bus":"' + bus + '","extraSeat":' + extraSeat + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d == 'OK') {
                        getCustomers();
                        getSeatPlan(service, bus, busDate, custNo);
                        $("#busSelectSeatDiv").html('');
                    } else {
                      showAlert(msg.d);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function selectSeat(row, col, service, bus, date, custNo) {            
            $.ajax({
                type: "POST",
                url: "BusSeatSelect.aspx/getSeatCust",
                data: '{"Row":' + row + ',"Col":' + col + ',"CustNo":' + custNo + ',"Bus":"' + bus + '","Service":"' + service + '","BusDate":' + date + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != '') {
                        $("#busSelectSeatDiv").html('');
                        $("#busSelectSeatDiv").html(msg.d);                        
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getSeatPlan(service, bus, transDate, custNo) {
            $.ajax({
                type: "POST",
                url: "BusSeatSelect.aspx/getSeatPlan",
                data: '{"Service":"' + service + '","Bus":"' + bus + '","Date":' + transDate + ',"CustNo":' + custNo + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != '') {
                        $("#busSeatPlanDiv").html('');
                        $("#busSeatPlanDiv").html(msg.d);
                        $("#busSelectSeatDiv").html('');
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getCustomers() {
            $.ajax({
                async: false,
                type: "POST",
                url: "BusSeatSelect.aspx/getCustomers",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != '') {
                        $("#customerDiv").html('');
                        $("#customerDiv").html(msg.d);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function selectCustumer(custNo, transDate, service, bus, elm) {
            var custs = $('span[name="custs"]');
            for (i = 0; i < custs.length; i++) {
                $(custs[i]).find('img').attr('src', 'Images/nochecked_16.gif');
            }
            var sel = $(elm);
            var img = sel.find('img');
            img.attr('src', 'Images/checked_16.gif');
            getSeatPlan(service, bus, transDate, custNo);
        }

        $(document).ready(function() {
            $.query = $.query.load(location.href);
            var recID = $.query.get('ServiceID');
            $("#hfServiceID").val(recID);
            getCustomers();
        });
    </script>

</head>
<body>
    <form id="BusSeatSelectForm" runat="server">
    <input id="hfServiceID" type="hidden" value="" />
    <div>
        <div class="leftDiv">
            <div id="customerDiv">
            </div>
            <br />
            &nbsp;
            <br />
            <div id="busSelectSeatDiv" class="saveDiv">
            </div>
        </div>
        <div id="busSeatPlanDiv">
        </div>
    </div>
    <div id="dialog-message" title='<%= GetGlobalResourceObject("ResView", "lblMessage") %>'
        style="display: none;">
        <span id="messages"></span>
    </div>
    </form>
</body>
</html>
