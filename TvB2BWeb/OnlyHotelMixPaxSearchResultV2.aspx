﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlyHotelMixPaxSearchResultV2.aspx.cs" Inherits="OnlyHotelMixPaxSearchResultV2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title><%= GetGlobalResourceObject("PageTitle", "PackageSearchResult")%></title>

  <script src="Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery.json.js" type="text/javascript"></script>
  <script src="Scripts/json2.js" type="text/javascript"></script>
  <script src="Scripts/lodash.min.js" type="text/javascript"></script>
  <script src="Scripts/smartpaginator.js" type="text/javascript"></script>
  <script src="Scripts/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
  <script src="Scripts/PaximumListing.js?v=6" type="text/javascript"></script>
  <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

  <link href="CSS/PaximumListing.css?v=3" rel="stylesheet" />
  <link href="CSS/jquery.mCustomScrollbar.min.css" rel="stylesheet" />
  <link href="CSS/smartpaginator_new.css?v=1" rel="stylesheet" />

  <script type="text/javascript">    
    var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';

    function gotoReservation(hotelId, offerId, hotelDetail) {      
      self.parent.mixNewBookRooms(hotelId, offerId, hotelDetail);
    }

    $(document).ajaxStart(function () {
      $.blockUI({
        message: '<h1>' + lblPleaseWait + '</h1>'
      });
    }).ajaxStop(function () {
      $.unblockUI();
    });

    function getHotels(filterData) {
      var pl = new paxList({
        token: filterData.paxList.token,
        apiAddress: filterData.paxList.apiAddress,
        mediaAddress: filterData.paxList.mediaAddress,
        rowCountPerPage: filterData.paxList.rowCountPerPage
      }).init(false);

      var l = filterData.localisation;

      pl.setLocalisation({
        room: l.room,
        board: l.board,
        status: l.status,
        price: l.price,
        reservation: l.reservation,
        offers: l.offers,
        available: l.available,
        notAvailable: l.notAvailable,
        first: l.first,
        previous: l.previous,
        next: l.next,
        last: l.last,
        specialOffer: l.specialOffer,
        conditions: l.conditions,
        conditionTitle: l.conditionTitle,
        hotelNotFoundMessage: l.hotelNotFoundMessage,
        checkStatus: l.checkStatus
      }).getHotels(filterData.request);
    }

    function getFormData() {
      $.ajax({
        type: "POST",
        url: "OnlyHotelMixPaxSearchResultV2.aspx/getFormData",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            getHotels(msg.d);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showDialog(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    $(document).ready(function () {      
      getFormData();
    });

  </script>

</head>
<body>
  <form id="form1" runat="server">
    <div class="paximum-listing" style="display: none;">
      <div class="div-filter-area clearfix">
        <div class="div-filter div-arrival">
          <label class="lbl-arrival">Arrival</label>
          <select id="cmbArrival"></select>
        </div>
        <div class="div-filter div-category">
          <label class="lbl-category">Category</label>
          <select id="cmbCategory"></select>
        </div>
        <div class="div-filter div-hotel">
          <label class="lbl-hotel">Hotel</label>
          <select id="cmbHotel"></select>
        </div>
        <div class="div-filter div-room">
          <label class="lbl-room">Room</label>
          <select id="cmbRoom"></select>
        </div>
        <div class="div-filter div-board">
          <label class="lbl-board">Board</label>
          <select id="cmbBoard"></select>
        </div>
      </div>


      <div class="response">
        <div class="response-header clearfix">
          <div class="pull-left record-list">
            <span id="spnRecordCount">0</span> Record Listed
          </div>
          <div class="pull-right sort-by">
            <label class="lbl-sort">Sort by</label>
            <select id="cmbSort">
              <option value="asc">Price Ascending</option>
              <option value="desc">Price Descending</option>
            </select>
          </div>
        </div>
        <div class="response-body" id="response"></div>
        <div class="response-footer" id="divPaginator"></div>
      </div>
    </div>
  </form>
</body>
</html>
