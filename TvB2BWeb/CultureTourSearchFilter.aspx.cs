﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Text;
using System.Web.Services;
using TvTools;
using System.Web.Script.Services;

public partial class CultureTourSearchFilter : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;

        string datePatern = new TvBo.Common().getDateFormat(UserData.Ci);
        ppcCheckIn.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcCheckIn.Format = datePatern.Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');
        ppcCheckIn.From.Date = DateTime.Today;

        if (!IsPostBack)
            ppcCheckIn.DateValue = DateTime.Today.Date;

    }

    [WebMethod(EnableSession = true)]
    public static string CreateCriteria()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null)
            GroupSearch = (bool)groupSeacrh;

        bool CurControl = false;
        object curControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowMarketCur");
        if (curControl != null)
            CurControl = (bool)curControl;

        bool ExtAllotCont = false;
        object extAllotControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ExtAllotControl");
        if (extAllotControl != null)
            ExtAllotCont = (bool)extAllotControl;

        bool ShowExpandDay = true;
        object showExpandDay = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowExpandDay");
        if (showExpandDay != null)
            ShowExpandDay = (bool)showExpandDay;

        bool ShowSecondDay = true;
        object showSecondDay = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowSecondDay");
        if (showSecondDay != null)
            ShowSecondDay = (bool)showSecondDay;

        SearchCriteria criteria = new SearchCriteria
        {
            SType = SearchType.TourPackageSearch,
            FromRecord = 1,
            ToRecord = 25,
            ShowAvailable = true,
            CurrentCur = UserData.SaleCur,
            ExpandDate = 0,
            NightFrom = 7,
            NightTo = 7,
            RoomCount = 1,
            RoomsInfo = new List<SearchCriteriaRooms>(),
            CheckIn = DateTime.Today,
            UseGroup = GroupSearch,
            CurControl = CurControl,
            ExtAllotControl = ExtAllotCont,
            ShowExpandDay = ShowExpandDay,
            ShowSecondDay = ShowSecondDay
        };
        HttpContext.Current.Session["Criteria"] = criteria;
        return "";
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        CreateCriteria();

        SearchCriteria criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];


        TourCultureFilterDefaults tourCultureDefaults = new TourCultureFilterDefaults();
        try
        {
            tourCultureDefaults = Newtonsoft.Json.JsonConvert.DeserializeObject<TourCultureFilterDefaults>(new Common().getFormConfigValue("SearchPanel", "TourCultureDefaults").ToString());
        }
        catch
        {
            tourCultureDefaults = null;
        }
        Int16? defaultNight = Conversion.getInt16OrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "DefaultDays"));
        Int16 DefaultNight = defaultNight.HasValue ? defaultNight.Value : Convert.ToInt16(7);
        Int16 DefaultNight2 = DefaultNight;
        Int16 DefaultMaxNight = 28;
        Int16 DefaultExpandDay = 0;
        Int16 DefaultMaxExpandDay = 7;

        if (tourCultureDefaults != null)
        {
            DefaultNight = tourCultureDefaults.DefaultNight1.HasValue ? tourCultureDefaults.DefaultNight1.Value : DefaultNight;
            DefaultNight2 = tourCultureDefaults.DefaultNight2.HasValue ? tourCultureDefaults.DefaultNight2.Value : DefaultNight;
            DefaultExpandDay = tourCultureDefaults.DefaultExpandDay.HasValue ? tourCultureDefaults.DefaultExpandDay.Value : DefaultExpandDay;
            DefaultMaxExpandDay = tourCultureDefaults.DefaultMaxExpandDay.HasValue ? tourCultureDefaults.DefaultMaxExpandDay.Value : DefaultMaxExpandDay;
        }

        string fltCheckInDay = string.Empty;
        string lblDay = Conversion.getStrOrNull(HttpContext.GetGlobalResourceObject("LibraryResource", "lblDay"));
        for (int i = 0; i < DefaultMaxExpandDay + 1; i++)
            fltCheckInDay += string.Format("<option value='{0}' {3}>{1} {2}</option>", i, "+" + i, lblDay, i == DefaultExpandDay ? "selected='selected'" : string.Empty);

        string fltNight = string.Empty;
        string fltNight2 = string.Empty;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
        {
            List<Int16> nights = new TvBo.Common().getPLNights(UserData.Market, SearchType.TourPackageSearch, ref errorMsg);
            if (nights == null || nights.Count < 1)
            {
                fltNight += string.Format("<option value='{0}' {1}>{2}</option>", "7", "selected='selected'", "7");
            }
            else
            {
                foreach (Int16 row in nights)
                    fltNight += string.Format("<option value='{0}' {1}>{2}</option>", row, row == DefaultNight ? "selected='selected'" : "", row);
            }
        }
        else
        {
            for (int i = 1; i < DefaultMaxNight + 1; i++)
            {
                fltNight += string.Format("<option value='{0}' {1}>{2}</option>", i, i == DefaultNight ? "selected='selected'" : "", i);
                fltNight2 += string.Format("<option value='{0}' {1}>{2}</option>", i, i == DefaultNight2 ? "selected='selected'" : "", i);
            }
        }

        string fltRoomCount = string.Empty;
        for (int i = 1; i <= UserData.MaxRoomCount; i++)
            fltRoomCount += string.Format("<option value='{0}'>{1}</option>", i, i);

        bool? currencyConvert = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "ShowMarketCur"));

        packageSearchFilterFormData data = new packageSearchFilterFormData();
        data.ShowStopSaleHotels = (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) && !string.Equals(UserData.CustomRegID, TvBo.Common.ctID_TandemMayak)); // true;
        data.Market = UserData.Market;
        data.fltCheckInDay = fltCheckInDay;
        data.fltNight = fltNight;
        data.fltNight2 = fltNight2;
        data.fltRoomCount = fltRoomCount;
        data.CurrencyConvert = currencyConvert.HasValue ? currencyConvert.Value : true;

        List<ReadyCurrencyRecord> getCurrencyList = new TvBo.Common().getReadyCurrency(UserData.Market, UserData.SaleCur, ref errorMsg);

        string curList = string.Empty;
        curList += "<select id=\"currentCur\">";
        foreach (ReadyCurrencyRecord row in getCurrencyList)
        {
            curList += string.Format("<option value=\"{0}\" {2} >{1}</option>",
                                        row.Code,
                                        Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && Equals(UserData.Market, "SWEMAR") ? row.Code : row.NameL,
                                        Equals(UserData.SaleCur, row.Code) ? "selected=\"selected\"" : "");
        }
        curList += "</select>";
        data.MarketCur = string.Format("<div {3}><input id=\"CurrencyConvert\" type=\"checkbox\" {0} /><label for=\"CurrencyConvert\">{1}</label>&nbsp;{2}</div>",
                data.CurrencyConvert ? "checked='checked'" : "",
                HttpContext.GetGlobalResourceObject("LibraryResource", "lblShowInPrice2"),
                curList,
                Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && Equals(UserData.Market, "FINMAR") ? "style=\"display: none;\"" : "");

        DateTime ppcCheckIn = DateTime.Today.Date;
        Int64 days = (ppcCheckIn - (new DateTime(1970, 1, 1))).Days - 1;
        data.CheckIn = (days * 86400000).ToString();
        data.ShowStopSaleHotels = true;
        data.CustomRegID = UserData.CustomRegID;
        data.ShowExpandDay = criteria.ShowExpandDay;
        data.ShowSecondDay = criteria.ShowSecondDay;
        data.DateFormat = new TvBo.Common().getDateFormat(UserData.Ci);
        data.DisableRoomFilter = true;
        return Newtonsoft.Json.JsonConvert.SerializeObject(data);
    }

    public static string getSearchFilterData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["SearchPLData"] == null)
        {
            bool GroupSearch = false;
            object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
            if (groupSeacrh != null)
                GroupSearch = (bool)groupSeacrh;
            SearchPLData filterData = CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.TourPackageSearch, null, string.Empty);
            HttpContext.Current.Session["SearchPLData"] = filterData;
        }
        return "OK";
    }

    [WebMethod(EnableSession = true)]
    public static string getHolPackCat(string Direction)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["SearchPLData"] == null)
            getSearchFilterData();
        if (HttpContext.Current.Session["SearchPLData"] == null)
            return "";
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        SearchPLData data = (SearchPLData)HttpContext.Current.Session["SearchPLData"];
        List<CodeName> holPackCat = new Search().getSearchHolPackCat(useLocalName, data, Conversion.getInt16OrNull(Direction), ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(holPackCat);
    }

    [WebMethod(EnableSession = true)]
    public static string getDeparture(string Direction, string HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["SearchPLData"] == null)
            getSearchFilterData();
        if (HttpContext.Current.Session["SearchPLData"] == null)
            return "";

        SearchPLData data = (SearchPLData)HttpContext.Current.Session["SearchPLData"];
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<LocationIDName> depCityData = new Search().getSearchDepCitys(useLocalName, data, Conversion.getInt16OrNull(Direction), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(depCityData);
    }

    [WebMethod(EnableSession = true)]
    public static string getArrival(string DepCity, string Direction, string HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        Int32? depCity = Conversion.getInt32OrNull(DepCity);
        if (HttpContext.Current.Session["SearchPLData"] == null)
            return "";
        SearchPLData data = (SearchPLData)HttpContext.Current.Session["SearchPLData"];
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<plLocationData> arrCityData = new Search().getSearchArrCitys(useLocalName, data, depCity, Conversion.getInt16OrNull(Direction), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        var retVal = from q in arrCityData
                     orderby (useLocalName ? q.ArrCountryNameL : q.ArrCountryName), (useLocalName ? q.ArrCityNameL : q.ArrCityName)
                     group q by new
                     {
                         country = useLocalName ? q.ArrCountryNameL : q.ArrCountryName,
                         RecID = q.ArrCity,
                         Name = useLocalName ? q.ArrCityNameL : q.ArrCityName
                     } into k
                     select new { Country = k.Key.country, RecID = k.Key.RecID, Name = k.Key.Name };
        return Newtonsoft.Json.JsonConvert.SerializeObject(retVal);
    }

    [WebMethod(EnableSession = true)]
    public static string getHolpack(string DepCity, string ArrCity, string Direction, string HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        Int32? depCity = Conversion.getInt32OrNull(DepCity);
        Int32? arrCity = Conversion.getInt32OrNull(ArrCity);
        if (HttpContext.Current.Session["SearchPLData"] == null)
            return "";
        SearchPLData data = (SearchPLData)HttpContext.Current.Session["SearchPLData"];
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<CodeName> holpackData = new Search().getSearchHolPack(useLocalName, data, depCity, arrCity, Conversion.getInt16OrNull(Direction), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        var retVal = from q in holpackData
                     orderby q.Name, q.Code
                     group q by new
                     {
                         Name = q.Name,
                         Code = q.Code + ';' + data.plHolPacks.Find(f => f.HolPack == q.Code).DepCity.ToString() + ';' + data.plHolPacks.Find(f => f.HolPack == q.Code).ArrCity.ToString()
                     } into k
                     select new { Code = k.Key.Code, Name = k.Key.Name };
        return Newtonsoft.Json.JsonConvert.SerializeObject(retVal);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<TvBo.LocationIDName> getResortData(string ArrCity, string Direction, string HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null)
            GroupSearch = (bool)groupSeacrh;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        TvBo.SearchPLData data = TvBo.CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.TourPackageSearch, null, string.Empty);
        List<TvBo.LocationIDName> resortData = new TvBo.Search().getSearchResorts(useLocalName, data, Conversion.getInt32OrNull(ArrCity), Conversion.getInt16OrNull(Direction), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        return resortData;
    }

    [WebMethod(EnableSession = true)]
    public static string getRoomInfo(string RoomCount)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        Int16 maxChildAge = UserData.TvParams.TvParamReser.MaxChdAgeB2Bsrc.HasValue ? Convert.ToInt16(Math.Floor(UserData.TvParams.TvParamReser.MaxChdAgeB2Bsrc.Value)) : (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ? Convert.ToInt16(18) : Convert.ToInt16(12));
        StringBuilder html = new StringBuilder();
        plMaxPaxCounts maxPaxData = new plMaxPaxCounts();
        object _paxData = new TvBo.Common().getFormConfigValue("SearchPanel", "PaxData");
        if (_paxData != null)
        {
            plMaxPaxCounts paxData = Newtonsoft.Json.JsonConvert.DeserializeObject<plMaxPaxCounts>(_paxData.ToString());
            maxPaxData = paxData;
        }
        if (HttpContext.Current.Session["SearchPLData"] != null && (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Elsenal)))
            if (((SearchPLData)HttpContext.Current.Session["SearchPLData"]).PlMaxPaxCount != null)
                maxPaxData = ((SearchPLData)HttpContext.Current.Session["SearchPLData"]).PlMaxPaxCount;

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Safiran) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_CelexTravel))
            maxPaxData.maxAdult = 10;
        //TVB2B-4008
        if (string.Equals(UserData.CustomRegID, Common.crID_Rezeda) || string.Equals(UserData.CustomRegID, Common.crID_KidyTour))
            maxPaxData.maxAdult = 12;
        /* TICKET ID : 14489
        if (string.Equals(UserData.CustomRegID, Common.crID_Dallas))
        {
            maxPaxData.maxAdult = 3;
            maxPaxData.maxChd = 2;
        }
        */
        Int32 roomCount = Conversion.getInt32OrNull(RoomCount).HasValue ? Conversion.getInt32OrNull(RoomCount).Value : 1;
        Int32 j = 0;
        for (int i = 1; i < roomCount + 1; i++)
        {
            html.AppendFormat("<div style=\"width:250px; clear:both;\"><b>{0} {1}</b></div>",
                    HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblRoom"),
                    i.ToString());
            html.AppendFormat("<div id=\"divRoomInfo{0}\" style=\"width:250px; clear:both; border-top: solid 1px #000000; \">", i.ToString());
            html.AppendFormat("  <div id=\"divAdult{0}\" style=\"float: left; width:100px;\">", i.ToString());
            html.AppendFormat("  <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblAdult").ToString());
            html.AppendFormat("  <select id=\"fltAdult{0}\" style=\"width: 50px;\">", i.ToString(), i.ToString());
            for (j = 1; j <= maxPaxData.maxAdult; j++)
            {
                if (j == 2)
                    html.AppendFormat("     <option value='{0}' selected=\"selected\">{1}</option>", j.ToString(), j.ToString());
                else
                    html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            }
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");

            html.AppendFormat("  <div id=\"divChild{0}\" style=\"float: left; width:50px;\">", i.ToString());
            html.AppendFormat("  <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChild").ToString());
            html.AppendFormat("  <select id=\"fltChild{0}\" onchange=\"onChildChange('{1}')\" style=\"width: 50px;\">", i.ToString(), i.ToString());
            for (j = 0; j < (maxPaxData.maxChd.HasValue ? maxPaxData.maxChd.Value + 1 : 5); j++)
                html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");
            html.AppendFormat("</div>");

            html.AppendFormat("<div id=\"divRoomInfoChd{0}\" style=\"width:250px; clear:both; display:none;\">", i.ToString());
            html.AppendFormat(" <div id=\"divRoomInfoChd1{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
            string childAgeStr = string.Empty;
            if (HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString().IndexOf('{') > 0)
                childAgeStr = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString(), "1");
            else
                childAgeStr = HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 1";

            html.AppendFormat("   <span>{0}</span><br />", childAgeStr);
            html.AppendFormat("   <select id=\"fltRoomInfoChd1{0}\" style=\"width: 100%;\">", i.ToString());
            for (j = 0; j <= maxChildAge; j++)
                html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");
            html.AppendFormat(" <div id=\"divRoomInfoChd2{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
            if (HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString().IndexOf('{') > 0)
                childAgeStr = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString(), "2");
            else
                childAgeStr = HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 2";
            html.AppendFormat("   <span>{0}</span><br />", childAgeStr);
            html.AppendFormat("   <select id=\"fltRoomInfoChd2{0}\" style=\"width: 100%;\">", i.ToString());
            for (j = 0; j <= maxChildAge; j++)
                html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");

            html.AppendFormat(" <div id=\"divRoomInfoChd3{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
            if (HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString().IndexOf('{') > 0)
                childAgeStr = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString(), "3");
            else
                childAgeStr = HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 3";
            html.AppendFormat("   <span>{0}</span><br />", childAgeStr);
            html.AppendFormat("   <select id=\"fltRoomInfoChd3{0}\" style=\"width: 100%;\">", i.ToString());
            for (j = 0; j <= maxChildAge; j++)
                html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");

            html.AppendFormat(" <div id=\"divRoomInfoChd4{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
            if (HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString().IndexOf('{') > 0)
                childAgeStr = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString(), "4");
            else
                childAgeStr = HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 4";
            html.AppendFormat("   <span>{0}</span><br />", childAgeStr);
            html.AppendFormat("   <select id=\"fltRoomInfoChd4{0}\" style=\"width: 100%;\">", i.ToString());
            for (j = 0; j <= maxChildAge; j++)
                html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");

            html.AppendFormat("</div>");
        }
        return html.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getCategoryData(string ArrCity, string Resort, string Direction, string HolPackCat)
    {
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        int? _resort = null;
        if (Resort.Split('|').Length == 1)
            _resort = Conversion.getInt32OrNull(Resort.Split('|')[0]);
        List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);
        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null)
            GroupSearch = (bool)groupSeacrh;
        Location filterResort = locations.Find(f => f.RecID == _resort);
        List<TvBo.CodeName> categoryData = new List<CodeName>();
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        TvBo.SearchPLData data = TvBo.CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.TourPackageSearch, null, string.Empty);
        if (filterResort != null && _resort.HasValue)
        {
            if (filterResort.Type == 4)
                categoryData = new TvBo.Search().getSearchCategorysCulture(useLocalName, data, Conversion.getInt32OrNull(ArrCity), null, _resort, Conversion.getInt16OrNull(Direction), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
            else
                categoryData = new TvBo.Search().getSearchCategorysCulture(useLocalName, data, Conversion.getInt32OrNull(ArrCity), _resort, null, Conversion.getInt16OrNull(Direction), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        }
        else
            categoryData = new TvBo.Search().getSearchCategorysCulture(useLocalName, data, Conversion.getInt32OrNull(ArrCity), Resort, Conversion.getInt16OrNull(Direction), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(categoryData);
    }

    [WebMethod(EnableSession = true)]
    public static string getHotelData(string ArrCity, string Resort, string Category, string Holpack, string Direction, string HolPackCat)
    {
        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        int? _resort = null;
        if (Resort.Split('|').Length == 1)
            _resort = Conversion.getInt32OrNull(Resort.Split('|')[0]);
        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null)
            GroupSearch = (bool)groupSeacrh;
        TvBo.SearchPLData data = TvBo.CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.TourPackageSearch, null, string.Empty);
        List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);
        Location filterResort = locations.Find(f => f.RecID == _resort);
        List<TvBo.CodeHotelName> hotelData = new List<CodeHotelName>();
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        if (filterResort != null && _resort.HasValue)
        {
            if (filterResort.Type == 4)
                hotelData = new TvBo.Search().getSearchHotelsCulture(useLocalName, data, _resort, Conversion.getInt32OrNull(ArrCity), Resort, Category, Holpack, Conversion.getInt16OrNull(Direction), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
            else
                hotelData = new TvBo.Search().getSearchHotelsCulture(useLocalName, data, null, Conversion.getInt32OrNull(ArrCity), Resort, Category, Holpack, Conversion.getInt16OrNull(Direction), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        }
        else
            hotelData = new TvBo.Search().getSearchHotelsCulture(useLocalName, data, null, Conversion.getInt32OrNull(ArrCity), Resort, Category, Holpack, Conversion.getInt16OrNull(Direction), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(hotelData);
    }

    [WebMethod(EnableSession = true)]
    public static string getRoomData(string DepCity, string ArrCity, string Resort, string Hotel, string Direction, string HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? _resort = null;
        if (Resort.Split('|').Length == 1)
            _resort = Conversion.getInt32OrNull(Resort.Split('|')[0]);
        List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);

        string errorMsg = string.Empty;
        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null)
            GroupSearch = (bool)groupSeacrh;
        Location filterResort = locations.Find(f => f.RecID == _resort);
        TvBo.SearchPLData data = TvBo.CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.TourPackageSearch, null, string.Empty);
        List<TvBo.CodeName> hotelData = new List<CodeName>();
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        if (filterResort != null && _resort.HasValue)
        {
            if (filterResort.Type == 4)
                hotelData = new TvBo.Search().getSearchRoomsCulture(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity == "null" ? "" : ArrCity), Resort, _resort, Hotel, Conversion.getInt16OrNull(Direction), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
            else
                hotelData = new TvBo.Search().getSearchRoomsCulture(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity == "null" ? "" : ArrCity), Resort, null, Hotel, Conversion.getInt16OrNull(Direction), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        }
        else
            hotelData = new TvBo.Search().getSearchRoomsCulture(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity == "null" ? "" : ArrCity), Resort, null, Hotel, Conversion.getInt16OrNull(Direction), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(hotelData);
    }

    [WebMethod(EnableSession = true)]
    public static string getBoardData(string DepCity, string ArrCity, string Resort, string Hotel, string Direction, string HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? _resort = null;
        if (Resort.Split('|').Length == 1)
            _resort = Conversion.getInt32OrNull(Resort.Split('|')[0]);
        List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);

        string errorMsg = string.Empty;
        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null)
            GroupSearch = (bool)groupSeacrh;
        Location filterResort = locations.Find(f => f.RecID == _resort);
        List<TvBo.CodeName> hotelData = new List<CodeName>();
        TvBo.SearchPLData data = TvBo.CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.TourPackageSearch, null, string.Empty);
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        if (filterResort != null && _resort.HasValue)
        {
            if (filterResort.Type == 4)
                hotelData = new TvBo.Search().getSearchBoardsCulture(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity), Resort, _resort, Hotel, Conversion.getInt16OrNull(Direction), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
            else
                hotelData = new TvBo.Search().getSearchBoardsCulture(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity), Resort, null, Hotel, Conversion.getInt16OrNull(Direction), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        }
        else
            hotelData = new TvBo.Search().getSearchBoardsCulture(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity), Resort, null, Hotel, Conversion.getInt16OrNull(Direction), Conversion.getInt16OrNull(HolPackCat), ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(hotelData);
    }

    [WebMethod(EnableSession = true)]
    public static string SetCriterias(string CheckIn, string ExpandDate, string NightFrom, string NightTo,
            string RoomCount, string roomsInfo, string Board, string Room, string DepCity, string ArrCity,
            string Resort, string Category, string Hotel, string CurControl, string CurrentCur,
            string ShowStopSaleHotels, string HolPack, string Direction, string HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["Criteria"] == null)
            CreateCriteria();
        SearchCriteria criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];
        DateTime checkIn = new DateTime(1970, 1, 1);
        checkIn = checkIn.AddDays((Convert.ToInt64(CheckIn) / 86400000) + 1);
        criteria.CheckIn = checkIn.Date;
        criteria.ExpandDate = Convert.ToInt16(ExpandDate);
        criteria.NightFrom = Convert.ToInt16(NightFrom);
        criteria.NightTo = Convert.ToInt16(NightTo);
        criteria.RoomCount = Convert.ToInt16(RoomCount);
        string _room = "[" + roomsInfo.Replace("|", "\"").Replace(')', '}').Replace('(', '{') + "]";
        List<SearchCriteriaRooms> rooms = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SearchCriteriaRooms>>(_room);
        criteria.RoomsInfo.Clear();
        int i = 0;
        foreach (SearchCriteriaRooms row in rooms)
        {
            ++i;
            criteria.RoomsInfo.Add(new SearchCriteriaRooms
            {
                RoomNr = i,
                Adult = row.Adult,
                Child = row.Child,
                Chd1Age = row.Chd1Age >= 0 ? row.Chd1Age : null,
                Chd2Age = row.Chd2Age >= 0 ? row.Chd2Age : null,
                Chd3Age = row.Chd3Age >= 0 ? row.Chd3Age : null,
                Chd4Age = row.Chd4Age >= 0 ? row.Chd4Age : null
            });
        }
        criteria.Board = Conversion.getStrOrNull(Board);
        criteria.Room = Conversion.getStrOrNull(Room);
        criteria.DepCity = Conversion.getInt32OrNull(DepCity);
        criteria.ArrCity = Conversion.getInt32OrNull(!Equals(ArrCity, "null") ? ArrCity : "");
        criteria.Resort = Conversion.getStrOrNull(Resort);
        criteria.Category = Conversion.getStrOrNull(Category);
        criteria.Hotel = Conversion.getStrOrNull(Hotel);
        criteria.Package = Conversion.getStrOrNull(HolPack);
        criteria.FromRecord = 1;
        criteria.ToRecord = 25;
        criteria.CurControl = Equals(CurControl, "Y");
        criteria.CurrentCur = CurrentCur;
        criteria.SType = SearchType.TourPackageSearch;
        criteria.ShowStopSaleHotels = Equals(ShowStopSaleHotels, "Y");
        criteria.HolPackCat = Conversion.getInt16OrNull(HolPackCat);
        criteria.Direction = Direction;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        criteria.UseGroup = groupSeacrh != null ? (bool)groupSeacrh : false;
        HttpContext.Current.Session["Criteria"] = criteria;
        return "OK";
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<calendarColor> getFlightDays(string DepCity, string ArrCity, string Direction, string HolPackCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        int? depCity = Conversion.getInt32OrNull(DepCity);
        int? arrCity = Conversion.getInt32OrNull(ArrCity);
        //string holPack = string.IsNullOrEmpty(HolPackCat) ? string.Empty : (HolPackCat.IndexOf(';') > -1 ? HolPackCat.Split(';')[0] : HolPackCat);
        List<HolPackFixDate> flightDays = new HolPacks().getCultureTourCheckInDays(UserData, depCity, arrCity, Direction, HolPackCat, ref errorMsg);
        List<Location> location = CacheObjects.getLocationList(UserData.Market);
        var flights = from q in flightDays
                      group q by new { q.CheckIn } into k
                      select new { k.Key.CheckIn };

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        List<calendarColor> retVal = new List<calendarColor>();
        foreach (var row in flights)
        {
            var query1 = from q in flightDays
                         where q.CheckIn == row.CheckIn
                         select q;
            string text = string.Empty;
            foreach (var row2 in query1)
            {
                if (text.Length > 0)
                    text += "\\n\\r";
                text += string.Format("{0}", useLocalName ? row2.HolPackNameL : row2.HolPackName);
                Int16 colorType = -1;
                retVal.Add(new calendarColor
                {
                    Year = row.CheckIn.Value.Year,
                    Month = row.CheckIn.Value.Month,
                    Day = row.CheckIn.Value.Day,
                    Text = text,
                    ColorType = colorType
                });
            }
        }
        return retVal;
    }
}

