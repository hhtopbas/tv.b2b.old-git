﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using TvBo;
using TvTools;

public partial class ReservationRFWizard : BasePage
{
    public static string twoLetterISOLanguageName = "en";

    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<string> lang = new List<string>() { "ar-DZ", "ar-DZ", "ar", "en-AU", "en-GB", "en-NZ", "fr-CH", "fr", "pt-BR", "pt", "sr-SR", "sr-CS", "sr", "zh-CN", "zh-HK", "zh-TW" };
        if (lang.Contains(UserData.Ci.Name))
            twoLetterISOLanguageName = UserData.Ci.Name;
        else
            twoLetterISOLanguageName = UserData.Ci.TwoLetterISOLanguageName != "en" ? UserData.Ci.TwoLetterISOLanguageName : "";
        string _tmpPath = WebRoot.BasePageRoot + "Data/";
        tmplPath.Value = _tmpPath;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        Int16 seqNo = 0;
        var hotelList = from q in ResData.ResService
                        where string.Equals(q.ServiceType, "HOTEL") &&
                              string.IsNullOrEmpty(q.Room) &&
                              string.IsNullOrEmpty(q.Accom) &&
                              string.IsNullOrEmpty(q.Board)
                        orderby q.StepNo
                        group q by new { q.RecID, q.Service, q.ServiceName, q.Unit, q.BegDate, q.EndDate, q.Duration } into g
                        select new {
                            SeqNo = ++seqNo,
                            RecID = g.Key.RecID,
                            Service = g.Key.Service,
                            Description = g.Key.ServiceName,
                            Unit = g.Key.Unit,
                            CheckIn = g.Key.BegDate.Value.ToShortDateString(),
                            CheckOut = g.Key.EndDate.Value.ToShortDateString(),
                            Night = g.Key.Duration.ToString(),
                            IsPackage = string.IsNullOrEmpty(ResData.ResMain.HolPack) == false
                        };

        return new {
            ResBegDate = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToShortDateString() : "",
            ResEndDate = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.ToShortDateString() : "",
            ResNight = ResData.ResMain.Days.HasValue ? ResData.ResMain.Days.Value.ToString() : "",
            ResPax = (ResData.ResMain.Adult.HasValue ? ResData.ResMain.Adult.Value : 0) + (ResData.ResMain.Child.HasValue ? ResData.ResMain.Child.Value : 0),
            DateTimeFormat = new Common().getDateFormatRegion(UserData.Ci),
            DateMaskFormat = (new Common().getDateFormatRegion(UserData.Ci)).ToLower().Replace('y', '9').Replace('m', '9').Replace('d', '9'),
            Hotels = hotelList
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getHotelRooms(string Hotel, int? RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord selectMaster = ResData.ResService.Find(f => f.RecID == RecID);
        Int16 seqNo = 0;
        var hotelRoomList = from q in ResData.ResService
                            where string.Equals(q.ServiceType, "HOTEL") &&
                                  q.Service == selectMaster.Service &&
                                  DateTimeFunc.CompareDate(q.BegDate, selectMaster.BegDate) &&
                                  DateTimeFunc.CompareDate(q.EndDate, selectMaster.EndDate) &&
                                  !string.IsNullOrEmpty(q.Accom) &&
                                  !string.IsNullOrEmpty(q.Room) &&
                                  !string.IsNullOrEmpty(q.Board)
                            group q by new { q.Room, q.RoomName, q.RoomNameL, q.Accom, q.AccomName, q.AccomNameL, q.Board, q.BoardName, q.BoardNameL } into k
                            select new {
                                SeqNo = seqNo++,
                                RecId = selectMaster.RecID,
                                Hotel = selectMaster.Service,
                                Room = k.Key.Room,
                                RoomName = k.Key.RoomName,
                                Accom = k.Key.Accom,
                                AccomName = k.Key.AccomName,
                                Board = k.Key.Board,
                                BoardName = k.Key.BoardName,
                                IsPackage = string.IsNullOrEmpty(ResData.ResMain.HolPack) == false,
                                Unit = ResData.ResService.Where(
                                                                 q =>
                                                                 q.Service == selectMaster.Service &&
                                                                 DateTimeFunc.CompareDate(q.BegDate, selectMaster.BegDate) &&
                                                                 DateTimeFunc.CompareDate(q.EndDate, selectMaster.EndDate) &&
                                                                 q.Accom == k.Key.Accom &&
                                                                 q.Room == k.Key.Room &&
                                                                 q.Board == k.Key.Board
                                                               ).Count()
                            };
        List<HotelRoomRecord> hotelRooms = new Hotels().getHotelRoomList(UserData.Market, Hotel, ref errorMsg);
        List<HotelAccomRecord> hotelAccoms = new Hotels().getHotelAccom(UserData.Market, Hotel, ref errorMsg);
        List<HotelBoardRecord> hotelBoards = new Hotels().getHotelBoardList(UserData.Market, Hotel, ref errorMsg);
        return new {
            HotelRoomList = hotelRooms,
            HotelAccomList = hotelAccoms,
            HotelBoardList = hotelBoards,
            Rooms = new {
                MasterRecId = RecID,
                HotelCode = Hotel,
                HotelRooms = hotelRoomList
            }
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object addHotelRooms(string Hotel, int? RecID, string Room, string Accom, string Board, int? Unit)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        List<ResServiceRecord> hotelServices = ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL") && string.Equals(w.Service, Hotel) && string.IsNullOrEmpty(w.Room) && string.IsNullOrEmpty(w.Accom) && string.IsNullOrEmpty(w.Board)).ToList<ResServiceRecord>();
        ResServiceRecord hotel = hotelServices.Find(f => f.RecID == RecID);
        if (hotel != null) {
            List<HotelAccomRecord> HAP = new Hotels().getHotelAccom(ResData.ResMain.PLMarket, hotel.Service, ref errorMsg);
            Debug.Print(Newtonsoft.Json.JsonConvert.SerializeObject(hotel));
            Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(hotel));
            ResData = new ReservationRequestForm().AddHotelServiceFromWizard(UserData, ResData, hotel.RecID, Room, Accom, Board, HAP, Unit, ref errorMsg);
            Debug.Print(Newtonsoft.Json.JsonConvert.SerializeObject(ResData));
            Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(ResData));
            HttpContext.Current.Session["ResData"] = ResData;
            return true;
        } else {
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getHotelRoomCustomers(int? RecID, string Hotel, string Room, string Board, string Accom)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord thisHotel = ResData.ResService.Find(f => f.RecID == RecID);
        if (thisHotel == null) {
            return null;
        }
        var q = from q1 in ResData.ResCust
                join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                join q3 in ResData.ResService on q2.ServiceID equals q3.RecID
                where q3.ServiceType == "HOTEL" &&
                      q3.Service == thisHotel.Service &&
                      DateTimeFunc.CompareDate(q3.BegDate, thisHotel.BegDate) &&
                      DateTimeFunc.CompareDate(q3.EndDate, thisHotel.EndDate) &&
                      !string.IsNullOrEmpty(q3.Accom) &&
                      !string.IsNullOrEmpty(q3.Room) &&
                      !string.IsNullOrEmpty(q3.Board)
                group q3 by new { ServiceID = q3.RecID } into k
                select new { ServiceID = k.Key.ServiceID };

        List<CodeName> selectCust = new List<CodeName>();
        int roomNr = 0;
        int totalCust = 0;
        foreach (var row in q) {
            ++roomNr;
            var roomPassengers = from q1 in ResData.ResCust
                                 join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                                 join q3 in ResData.ResService on q2.ServiceID equals q3.RecID
                                 where q3.Service == Hotel &&
                                       q3.Room == Room &&
                                       q3.Board == Board &&
                                       q3.Accom == Accom &&
                                       q3.RecID == row.ServiceID
                                 select new { Code = roomNr.ToString(), Name = q1.TitleStr + ". " + q1.Surname + ", " + q1.Name, Title = q1.Title };
            totalCust += roomPassengers.Count();
            foreach (var r1 in roomPassengers)
                selectCust.Add(new CodeName() {
                    Code = r1.Code,
                    Name = r1.Name
                });
        }

        return new {
            Selected = selectCust,
            TotalCust = totalCust
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object setHotelRoomCustomers(int? RecID, List<int?> SelectedCust)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord thisHotel = ResData.ResService.Find(f => f.RecID == RecID);
        if (thisHotel == null || !RecID.HasValue) {
            return null;
        }

        List<ResCustRecord> q = (from q1 in ResData.ResCust
                                 join q2 in SelectedCust on q1.CustNo equals q2.Value
                                 select q1).ToList<ResCustRecord>();
        List<ResConRecord> resConList = ResData.ResCon;
        foreach (ResCustRecord row in q) {
            if (resConList.Find(f => f.CustNo == row.CustNo && f.ServiceID == RecID) == null) {
                resConList.Add(new ResConRecord() {
                    RecID = resConList.Count > 0 ? resConList.Last().RecID + 1 : 1,
                    RecordID = resConList.Count > 0 ? resConList.Last().RecID + 1 : 1,
                    RecStatus = RecordStatus.New,
                    ChkSel = true,
                    CustNo = row.CustNo,
                    CustNoT = row.CustNoT,
                    ResNo = ResData.ResMain.ResNo,
                    ServiceID = RecID.Value,
                    ServiceIDT = RecID,
                    UnitNo = 1
                });
            }
        }
        HttpContext.Current.Session["ResData"] = ResData;

        List<bool> roomOk = new List<bool>();

        var rooms = from s in ResData.ResService
                    where string.Equals(s.ServiceType, "HOTEL") &&
                          string.IsNullOrEmpty(s.Room) &&
                          string.IsNullOrEmpty(s.Board) &&
                          string.IsNullOrEmpty(s.Accom)
                    select s;
        foreach (var row in rooms) {
            var rsc = from rs in ResData.ResService
                      join rc in ResData.ResCon on rs.RecID equals rc.ServiceID
                      join c in ResData.ResCust on rc.CustNo equals c.CustNo
                      where string.Equals(rs.ServiceType, "HOTEL") &&
                            !string.IsNullOrEmpty(rs.Room) &&
                            !string.IsNullOrEmpty(rs.Board) &&
                            !string.IsNullOrEmpty(rs.Accom) &&
                            string.Equals(row.Service, rs.Service) &&
                            DateTimeFunc.CompareDate(row.BegDate, rs.BegDate) &&
                            DateTimeFunc.CompareDate(row.EndDate, rs.EndDate)
                      group c by new { c.CustNo } into g
                      select new { g.Key.CustNo };
            if (rsc.Count() == ResData.ResCust.Count) {
                roomOk.Add(true);
            } else {
                roomOk.Add(false);
            }
        }

        return new {
            setCust = true,
            roomOKList = roomOk,
            roomOK = roomOk.GroupBy(g => g).Count() == 1
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object wizardAddHotel(string DateTimeFormat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        ResServiceRecord[] hotelServices = ResData.ResService.Where(w => w.ServiceType == "HOTEL").OrderBy(o => o.EndDate).ToArray();
        DateTime? lastCheckOut = hotelServices != null && hotelServices.Count() > 0 ? hotelServices.LastOrDefault().EndDate : ResData.ResMain.BegDate;
        List<HotelRecord> hotelList = new Hotels().getHotelDetail(UserData, null, null, ref errorMsg);

        return new {
            HotelList = from q in hotelList
                        orderby q.Name
                        select new { Code = q.Code, Name = q.Name + " (" + q.LocationName + ")" },
            BegDate = lastCheckOut.HasValue ? (!string.IsNullOrEmpty(DateTimeFormat) ? lastCheckOut.Value.ToString(DateTimeFormat.Replace("m", "M")) : lastCheckOut.Value.ToShortDateString()) : "",
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object AddHotel(string Hotel, string BegDate, string EndDate, Int16? Night, string DateTimeFormat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        //DateTime? begDate = Conversion.convertDateTime(BegDate, DateTimeFormat);
        //DateTime? endDate = Conversion.convertDateTime(EndDate, DateTimeFormat);
        DateTime? begDate = null;
        DateTime? endDate = null;
        if (!string.IsNullOrEmpty(BegDate)) {
            DateTime? _begDate = DateTime.ParseExact(BegDate, DateTimeFormat.Replace('m', 'M'), Thread.CurrentThread.CurrentCulture);
            begDate = _begDate;
        }
        if (!string.IsNullOrEmpty(EndDate)) {
            DateTime? _endDate = DateTime.ParseExact(EndDate, DateTimeFormat.Replace('m', 'M'), Thread.CurrentThread.CurrentCulture);
            endDate = _endDate;
        }
        if (string.IsNullOrEmpty(Hotel) || !begDate.HasValue || !endDate.HasValue || !Night.HasValue) {
            return false;
        }

        ServiceRecord mainService = new TvBo.Common().getMainService(UserData.Market, "HOTEL");
        HotelRecord hotelDetail = new Hotels().getHotelDetail(UserData, Hotel, ref errorMsg);

        List<ResServiceRecord> serviceList = ResData.ResService;
        ResServiceRecord newHotel = new ResServiceRecord();
        newHotel.RecID = serviceList.Count > 0 ? (int)(serviceList.OrderBy(o => o.RecID).LastOrDefault().RecID + 1) : (int)1;
        newHotel.RecordID = newHotel.RecID;
        newHotel.SeqNo = serviceList.Count > 0 ? (short?)(serviceList.OrderBy(o => o.SeqNo).LastOrDefault().SeqNo + 1) : (short?)1;
        newHotel.ServiceType = "HOTEL";
        newHotel.ServiceTypeName = mainService.Name;
        newHotel.ServiceTypeNameL = mainService.NameL;
        newHotel.Service = Hotel;
        newHotel.ServiceName = hotelDetail.Name;
        newHotel.ServiceNameL = hotelDetail.LocalName;
        newHotel.DepLocation = hotelDetail.Location;
        newHotel.DepLocationName = hotelDetail.LocationName;
        newHotel.DepLocationNameL = hotelDetail.LocationLocalName;
        newHotel.BegDate = DateTime.SpecifyKind(begDate.Value.Date, DateTimeKind.Utc);
        newHotel.EndDate = DateTime.SpecifyKind(endDate.Value.Date, DateTimeKind.Utc);
        newHotel.Duration = Night;
        newHotel.Unit = 1;
        newHotel.NetCur = ResData.ResMain.NetCur;
        newHotel.SaleCur = ResData.ResMain.SaleCur;
        newHotel.Supplier = hotelDetail.DefSupplier;
        newHotel.Compulsory = false;
        newHotel.IncPack = "N";
        serviceList.Add(newHotel);
        HttpContext.Current.Session["ResData"] = ResData;
        return new {
            OK = true,
            BegDate = begDate,
            EndDate = endDate,
            Hotel = newHotel
        };
    }
}
