﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using TvTools;
using System.Threading;
using System.Web.Script.Serialization;
using System.Collections;
using System.Text;

public partial class ResMonitorV2Data : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScriptSerializer ser = new JavaScriptSerializer();
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        if (UserData.Ci.Name.ToLower() == "lt-lt")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "yyyy.MM.dd";
        else if (UserData.Ci.Name.ToLower() == "lv-lv")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int iDisplayLength = Convert.ToInt32(HttpContext.Current.Request["iDisplayLength"]);
        int iDisplayStart = Convert.ToInt32(HttpContext.Current.Request["iDisplayStart"]);
        int iEcho = Convert.ToInt32(HttpContext.Current.Request["sEcho"]);
        IQueryable<resMonitorRecordV2> allRecords = ReadFile().AsQueryable();

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        List<requestFormData> requestForm = new List<requestFormData>();
        for (int i = 0; i < Request.Form.Count; i++) requestForm.Add(new requestFormData { Name = Request.Form.GetKey(i), Value = Request[Request.Form.GetKey(i)] });
        string sortingStr = string.Empty;
        var iSortingColsQ = from q in requestForm
                            where q.Name == "iSortingCols"
                            select q.Value;
        Int16? iSortingCols = Conversion.getInt16OrNull(iSortingColsQ.FirstOrDefault());
        string sort = string.Empty;
        if (iSortingCols.HasValue)
        {
            for (int i = 0; i < iSortingCols.Value; i++)
            {
                var iSortCol = (from q in requestForm
                                where q.Name == "iSortCol_" + i
                                select q.Value).FirstOrDefault();
                string sSortDir = (from q in requestForm
                                   where q.Name == "sSortDir_" + i
                                   select q.Value).FirstOrDefault();
                string bSortable = (from q in requestForm
                                    where q.Name == "bSortable_" + iSortCol
                                    select q.Value).FirstOrDefault();
                if (string.Equals(bSortable, "true"))
                {
                    if (sortingStr.Length > 0) sortingStr += ", ";
                    switch (iSortCol)
                    {
                        case "0": 
                            sortingStr = "";
                            sort = sSortDir;
                            break;
                        case "1": 
                            sortingStr = "ResNo";
                            sort = sSortDir;
                            break;
                        case "3": 
                            sortingStr = "LeaderName";
                            sort = sSortDir;
                            break;
                        case "4": 
                            sortingStr = "HotelName";
                            sort = sSortDir;
                            break;
                        case "7": 
                            sortingStr = "BegDate" ;
                            sort = sSortDir;
                            break;
                        case "12": 
                            sortingStr = "DepCityName";
                            sort = sSortDir;
                            break;
                        case "13": 
                            sortingStr = "ArrCityName";
                            sort = sSortDir;
                            break;
                    }
                }
            }
        }

        int totalRecords = allRecords.Count();
        if (sortingStr.Length > 0)
            if (sort.ToLower() == "asc")
                allRecords = allRecords.OrderBy(sortingStr);
            else
                allRecords = allRecords.OrderByDescending(sortingStr);
       
        Int32 totalRecord = allRecords.Count();
        Int32? totalAdult = allRecords.Sum(s => s.Adult);
        Int32? totalChild = allRecords.Sum(s => s.Child);
        var totalPrice = from q in allRecords
                         group q by new { SaleCur = q.SaleCur } into k
                         select new
                         {
                             SaleCur = k.Key.SaleCur,
                             SalePrice = allRecords.Where(w => w.SaleCur == k.Key.SaleCur).Sum(s => (s.SPrice.HasValue ? s.SPrice.Value : 0))
                         };
        IQueryable<resMonitorRecordV2> filteredRecords = allRecords;
        filteredRecords = filteredRecords.Skip(iDisplayStart).Take(iDisplayLength);
        var retval = from q in filteredRecords
                     select new
                     {
                         oc = getOc(q),
                         ResNo = getResNo(q, UserData),
                         Info1 = getInfo1(UserData, q),
                         LeaderName = Conversion.getStrOrNull(q.LeaderName),
                         HotelName = getHotelName(useLocalName, q),
                         Adult = Conversion.getStrOrNull(q.Adult),
                         Child = Conversion.getStrOrNull(q.Child),
                         BegDate = q.BegDate.HasValue ? q.BegDate.Value.ToShortDateString() : "&nbsp;",
                         Days = Conversion.getStrOrNull(q.Days),
                         statusStr = getStatusStr(UserData, q),
                         SalePrice = Conversion.getStrOrNull(q.SPrice),
                         SaleCur = Conversion.getStrOrNull(q.SaleCur),
                         DepCityName = (useLocalName ? Conversion.getStrOrNull(q.DepCityNameL) : Conversion.getStrOrNull(q.DepCityName)),
                         ArrCityName = (useLocalName ? Conversion.getStrOrNull(q.ArrCityNameL) : Conversion.getStrOrNull(q.ArrCityName))
                     };
        tableSumResMonitor sum = new tableSumResMonitor
        {
            totalRecord = totalRecord,
            totalAdult = totalAdult.HasValue ? totalAdult.Value : 0,
            totalChild = totalChild.HasValue ? totalChild.Value : 0,
            totalPrice = totalPrice.AsEnumerable()
        };

        lightTable returnTable = new lightTable();
        returnTable.sEcho = iEcho;
        returnTable.iTotalRecords = totalRecords;
        returnTable.iTotalDisplayRecords = totalRecords;
        returnTable.aaData = retval;
        returnTable.tableSum = sum;

        Response.Clear();

        Response.ContentType = ("text/html");
        Response.BufferOutput = true;
        Response.Write(ser.Serialize(returnTable));
        Response.End();
    }

    protected string getOc(resMonitorRecordV2 row)
    {
        return string.Format("<img id=\"detayImg\" alt=\"\" src=\"Images/infoOpen.gif\" resno=\"{0}\" /><input type=\"hidden\" value=\"{1}\"/>",
            row.ResNo,
            row.isPxmReservation ? "pxm" : (row.OptDate.HasValue ? "9" : (row.ReadByOpr.HasValue && row.ReadByOpr.Value == 1 ? "1" : "0")));
    }

    protected string getResNo(resMonitorRecordV2 row, User UserData)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendFormat("<strong><a href=\"#\" data-ci='"+UserData.Ci.Name.ToString()+"-"+UserData.Ci.DateTimeFormat.ShortDatePattern.ToString()+"' onclick=\"showResView('{0}');\">{1}</a></strong>",
            row.ResNo,
            row.ResNo);
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Azur) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Kusadasi_Ro) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Safiran) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Qualitum) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_RoyalPersia) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_SunFun) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_KidyTour) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Dallas) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt)||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_DreamHolidays) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_ViyaTurizm)||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_SeaTravel)||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_TahaVoyages)||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_BalkanHolidays) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.ctID_BTBTour) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.ctID_Zershan) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.ctID_NeedTour))
        {
            if (row.OptDate.HasValue)
                sb.AppendFormat("<br /><span style=\"width:100%;font-weight:normal; font-size:7pt;text-align:center;\">{0}</span>",
                    row.OptDate.Value.ToShortDateString() + " " + row.OptDate.Value.ToString("HH:mm"));
        }
        return sb.ToString();
    }

    protected string getInfo1(User UserData, resMonitorRecordV2 row)
    {
        string comment = string.Empty;
        string resNote = string.Empty;

        if (row.NewComment)
            comment = string.Format("<img alt=\"{0}\" src=\"Images/mail_16.gif\" onclick=\"showResComment('{1}','{2}',{3})\" />",
                                                                    HttpContext.GetGlobalResourceObject("ResMonitor", "lblNewComment"),
                                                                    row.ResNo,
                                                                    UserData.UserID,
                                                                    UserData.Ci.LCID);

        if (!string.IsNullOrEmpty(row.ResNote))
        {
            resNote = string.Format("<img alt=\"{0}\" src=\"Images/edit_16.gif\" onclick=\"showResNote({1})\" />",
                                                                    HttpContext.GetGlobalResourceObject("ResMonitor", "lblShowReservationNote"),
                                                                    UICommon.EncodeJsString(row.ResNote));
            if (row.NewComment) resNote = "&nbsp;" + resNote;
        }

        return comment + resNote;
    }

    protected string getHotelName(bool useLocalName, resMonitorRecordV2 row)
    {
        if (useLocalName)
            return Conversion.getStrOrNull(row.HotelNameL);
        else return Conversion.getStrOrNull(row.HotelName);
    }

    protected string getStatusStr(User UserData, resMonitorRecordV2 row)
    {
        try
        {
            string resStatus = string.Empty;
            string confStat = string.Empty;
            string payStat = string.Empty;
            if (row.ResStat.HasValue)
                resStatus = string.Format("<img src=\"Images/Status/ResStatus{0}.gif\" width=\"16\" height=\"16\" alt=\"{1}\">",
                                row.ResStat,
                                HttpContext.GetGlobalResourceObject("ResMonitor", "lblResStatus").ToString() + ", " + (row.ResStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + row.ResStat.ToString()).ToString() : ""));
            if (row.ConfStat.HasValue)
                confStat = string.Format("<img src=\"Images/Status/ConfStatus{0}.gif\" width=\"16\" height=\"16\" alt=\"{1}\">",
                            row.ConfStat,
                            HttpContext.GetGlobalResourceObject("ResMonitor", "lblConfStatus").ToString() + ", " + (row.ConfStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + row.ConfStat.ToString()).ToString() : ""));
            if (!string.IsNullOrEmpty(row.PaymentStat))
                payStat = string.Format("<img src=\"Images/Status/PayStatus{0}.gif\" width=\"16\" height=\"16\" alt=\"{1}\">",
                            Conversion.getStrOrNull(row.PaymentStat),
                            HttpContext.GetGlobalResourceObject("ResMonitor", "lblPaymentStatus").ToString() + ", " + (!string.IsNullOrEmpty(row.PaymentStat) ? HttpContext.GetGlobalResourceObject("LibraryResource", "PayStatus" + row.PaymentStat).ToString() : ""));
            return resStatus + confStat + payStat;
        }
        catch
        {
            return string.Empty;
        }
    }

    protected List<resMonitorRecordV2> ReadFile()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\ResPaymentMonitor." + HttpContext.Current.Session.SessionID;
        List<resMonitorRecordV2> list = new List<resMonitorRecordV2>();
        if (System.IO.File.Exists(path))
        {
            System.IO.StreamReader reader = new System.IO.StreamReader(path);

            try
            {
                string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
                list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<resMonitorRecordV2>>(uncompressed);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                reader.Close();
            }
        }
        return list;
    }
}

