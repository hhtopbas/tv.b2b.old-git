﻿using BankIntegration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;

public partial class Data_ZER_MesafeliSatisSozlesmesi : System.Web.UI.Page
{
    protected User UserData;
    protected BIResMainRecord resMain;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            FormLoad();
    }
    private void FormLoad()
    {
        UserData = (User)Session["UserData"];
        string ResNo = Request.QueryString["resno"];
        string errorMsg = string.Empty;
        BIUser BIUserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);
        resMain = new BILib().getResMain(BIUserData, ResNo, ref errorMsg);
    }
    
}