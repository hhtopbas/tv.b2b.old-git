﻿using System;
using System.Collections.Generic;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Text;
using System.Web.Services;

namespace TvSearch
{
    public partial class OnlyExcursionSearch : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User UserData = (User)Session["UserData"];

            CultureInfo ci = UserData.Ci;
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
        }

        [WebMethod(EnableSession = true)]
        public static string UserHasAuth()
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            return UserData.BlackList ? "N" : "Y";
        }

        [WebMethod(EnableSession = true)]
        public static string getBookReservation(int? BookID)
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            if (HttpContext.Current.Session["ExcuresionCriteria"] == null)
            { HttpContext.Current.Response.StatusCode = 408; return null; }

            HttpContext.Current.Session["ResData"] = null;

            ExcursionSearchCriteria criteria = (ExcursionSearchCriteria)HttpContext.Current.Session["ExcuresionCriteria"];
            string errorMsg = string.Empty;
            bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
            string airportName = string.Empty;
            AirportRecord airport = new Flights().getAirport(UserData.Market, criteria.AirPort, ref errorMsg);
            if (airport != null)
                airportName = useLocalName ? airport.LocalName : airport.Name;
            StringBuilder sb = new StringBuilder();


            List<ExcursionSearchResult> searchData = ReadFile();
            ExcursionSearchResult excursion = searchData.Find(f => f.RefNo == BookID);
            if (excursion != null)
            {
                ResDataRecord ResData = new ResDataRecord();
                ResData = new ExcursionSearch().getExcursionReservation(UserData, criteria, excursion, ref errorMsg);
                if (string.IsNullOrEmpty(errorMsg))
                {
                    ResMainRecord resMain = ResData.ResMain;
                    resMain.ResNote = "Airport : " + airportName + ", Flight: " + criteria.FlightNo;
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka))
                    {
                        resMain = ResData.ResMain;
                        resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
                    }
                    HttpContext.Current.Session["ResData"] = ResData;
                    return "";
                }
                else return errorMsg;
            }
            else return "NO";
        }

        internal static List<ExcursionSearchResult> ReadFile()
        {
            if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci;
            Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\ExcursionSearch." + HttpContext.Current.Session.SessionID;
            List<ExcursionSearchResult> list = new List<ExcursionSearchResult>();
            if (System.IO.File.Exists(path))
            {
                System.IO.StreamReader reader = new System.IO.StreamReader(path);

                try
                {
                    string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
                    list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ExcursionSearchResult>>(uncompressed);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    reader.Close();
                }
            }
            return list;
        }

    }
}