﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Threading;
using System.Web.Script.Serialization;
using System.Collections;
using System.Text;
using TvTools;

public partial class OnlyExcursionSearchData : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScriptSerializer ser = new JavaScriptSerializer();
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        int iDisplayLength = Convert.ToInt32(HttpContext.Current.Request["iDisplayLength"]);
        int iDisplayStart = Convert.ToInt32(HttpContext.Current.Request["iDisplayStart"]);
        int iEcho = Convert.ToInt32(HttpContext.Current.Request["sEcho"]);

        if (iEcho == 1) iDisplayStart = 0;

        List<ExcursionSearchResult> allSearchRecord = ReadFile();
        IQueryable<ExcursionSearchResult> allRecords = allSearchRecord.AsQueryable();

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        IQueryable<ExcursionSearchResult> filteredRecords = allRecords;

        IQueryable<ExcursionSearchResult> returnData = filteredRecords.Skip(iDisplayStart).Take(iDisplayLength);

        saveFile(allSearchRecord, returnData.AsQueryable());
        var retval = from q in returnData
                     select new
                     {
                         Book = string.Format("<strong onclick=\"makeRes({1})\" class=\"bookStyle\">{0}</strong><span style=\"font-size:18pt;\"><br /><span style=\"font-size:3pt;\">&nbsp;</span>",
                                              HttpContext.GetGlobalResourceObject("PackageSearchResult", "book"),
                                              q.RefNo),
                         Name = string.IsNullOrEmpty(q.Name) ? getExcursionPackageName(UserData, q, useLocalName) : getExcurtionName(UserData, q, useLocalName),
                         Location = string.Format("<span style=\"font-size: 8pt;\">{0}</span>", useLocalName ? q.LocationNameL : q.LocationName),
                         CheckIn = string.Format("{0}<br /><span style=\"font-size: 7pt;\">{1}</span>", q.CheckIn.HasValue ? q.CheckIn.Value.ToShortDateString() : "", q.CheckIn.HasValue ? q.CheckIn.Value.ToString("dddd") : ""),
                         Price = getPrice(UserData, q, useLocalName)
                     };
        lightTable returnTable = new lightTable();
        returnTable.sEcho = iEcho;
        returnTable.iTotalRecords = allRecords.Count();
        returnTable.iTotalDisplayRecords = filteredRecords.Count();
        returnTable.aaData = retval;


        Response.Clear();

        Response.ContentType = ("text/html");
        Response.BufferOutput = true;
        Response.Write(ser.Serialize(returnTable));
        Response.End();
    }

    internal string getPrice(User UserData, ExcursionSearchResult row, bool useLocalName)
    {
        string price = string.Empty;
        price = row.TotalPrice.HasValue ? row.TotalPrice.Value.ToString("#,###.00") + " " + row.SaleCur : "";
        return price;
    }

    internal string getExcursionPackageName(User UserData, ExcursionSearchResult row, bool useLocalName)
    {
        string name = string.Empty;
        name = useLocalName ? row.PackageNameL : row.PackageName;
        if (row.PackageDetails != null && row.PackageDetails.Count > 0)
        {
            name += "<br /><span style='font-size: 7pt;'>Package Detail";
            foreach (ExcursionSearchResult r in row.PackageDetails)
            {
                name += "<br />";
                name += useLocalName ? r.NameL : r.Name; 
            }
            name += "</span>";
        }
        return name;
    }

    internal string getExcurtionName(User UserData, ExcursionSearchResult row, bool useLocalName)
    {
        string name = string.Empty;

        name = string.Format("<a class=\"cssExcursionName\" href=\"Controls/ExcursionInfo.aspx?Code={1}&Date={2}\" rel=\"Controls/ExcursionInfo.aspx?Code={1}&Date={2}\" title=\"{3}\">{0}</a>",
                    (useLocalName ? row.NameL : row.Name),
                    row.Code,
                    row.CheckIn.Value.Ticks,
                    HttpContext.GetGlobalResourceObject("Controls", "viewExcursion"));
        if (row.TransFromTime.HasValue || row.TransToTime.HasValue)
        {
            name += " (";
            if (row.TransFromTime.HasValue)
                name += row.TransFromTime.Value.ToString("HH:mm");
            name += "<->";
            if (row.TransToTime.HasValue)
                name += row.TransToTime.Value.ToString("HH:mm");
            name += ")";
        }
        if (row.FreeFromTime.HasValue || row.FreeToTime.HasValue)
        {
            name += "<br /><span style='font-size: 7pt;'>Free time:";
            if (row.FreeFromTime.HasValue)
                name += row.FreeFromTime.Value.ToString();
            name += "-";
            if (row.FreeToTime.HasValue)
                name += row.FreeToTime.Value.ToString();
            name += "</span>";
        }
        return name + (!string.IsNullOrEmpty(row.VehicleCatName) ? " (" + row.VehicleCatName + ")" : "");
    }

    protected List<ExcursionSearchResult> ReadFile()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\ExcursionSearch." + HttpContext.Current.Session.SessionID;
        List<ExcursionSearchResult> list = new List<ExcursionSearchResult>();
        if (System.IO.File.Exists(path))
        {
            System.IO.StreamReader reader = new System.IO.StreamReader(path);

            try
            {
                string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
                list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ExcursionSearchResult>>(uncompressed);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                reader.Close();
            }
        }
        return list;
    }

    internal void saveFile(List<ExcursionSearchResult> result, IQueryable<ExcursionSearchResult> calcResult)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        foreach (ExcursionSearchResult row in calcResult)
        {
            ExcursionSearchResult current = result.Find(f => f.RefNo == row.RefNo);
            if (current != null)
                current = row;
        }

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\ExcursionSearch." + HttpContext.Current.Session.SessionID;
        if (System.IO.File.Exists(path))
            System.IO.File.Delete(path);
        System.IO.FileStream file = new System.IO.FileStream(path, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
        try
        {
            System.IO.StreamWriter writer = new System.IO.StreamWriter(file);
            string compress = TvTools.GZipCompres.Compress(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            writer.Write(compress);
            writer.Close();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            file.Close();
        }
    }
}
