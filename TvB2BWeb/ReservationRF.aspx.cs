﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using TvBo;
using TvTools;

public partial class ReservationRF : BasePage
{
    public static string twoLetterISOLanguageName = "en";
    public static string pageRoot = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<string> lang = new List<string>() { "ar-DZ", "ar-DZ", "ar", "en-AU", "en-GB", "en-NZ", "fr-CH", "fr", "pt-BR", "pt", "sr-SR", "sr-CS", "sr", "zh-CN", "zh-HK", "zh-TW" };
        if (lang.Contains(UserData.Ci.Name))
            twoLetterISOLanguageName = UserData.Ci.Name;
        else
            twoLetterISOLanguageName = UserData.Ci.TwoLetterISOLanguageName != "en" ? UserData.Ci.TwoLetterISOLanguageName : "";
        pageRoot = WebRoot.BasePageRoot;
        string _tmpPath = WebRoot.BasePageRoot + "Data/";
        tmplPath.Value = _tmpPath;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getHolpackList(long? BegDate, Int16? Night, Int16? ResType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        string errorMsg = string.Empty;
        DateTime? begDate = null;
        if (BegDate.HasValue)
            begDate = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(BegDate.Value);
        List<RRFHolPack> holpackList = new HolPacks().getRRFHolPack(UserData, begDate, Night, ResType, ref errorMsg);

        return holpackList.Select(s => new {
            Code = s.Code,
            Name = useLocalName ? s.NameL : s.Name,
            DepCity = s.DepCity,
            ArrCity = s.ArrCity
        }).ToArray();
    }

    public static object getOtherServiceMenu()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        List<SaleServiceRecord> saleRecords = new ReservationCommon().getSaleServices(UserData, ResData, ref errorMsg);
        //MenuStr.Append("<div id=\"divAddResServiceMenu\"  class=\"ui-widget-header\"><div style=\"min-width: 100px;\">");
        List<SaleServiceRecord> saleService = new List<SaleServiceRecord>();
        if (ResData == null || ResData.ResMain == null || string.IsNullOrEmpty(ResData.ResMain.PackType))
            saleService = saleRecords.Where(w => w.B2BIndPack.HasValue && w.B2BIndPack.Value == true).ToList<SaleServiceRecord>();
        else
            saleService = saleRecords.Where(w => string.Equals(w.WebSale, "Y")).ToList<SaleServiceRecord>();

        List<AgencySaleSer> agencySaleService = new Agency().getAgencySaleRestriction(UserData.AgencyID, ref errorMsg);

        List<object> menu = new List<object>();
        foreach (SaleServiceRecord row in saleService) {
            if (agencySaleService.Find(f => f.ServiceType == row.Service) == null) {
                switch (row.Service) {
                    case "HOTEL":
                        menu.Add(new { controlUrl = "Controls/RSAdd_Hotel.aspx", imageUrl = "Images/Services/hotel.gif", description = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceHotel").ToString() });
                        break;
                    case "FLIGHT":
                        menu.Add(new { controlUrl = "Controls/RSAdd_Flight.aspx", imageUrl = "Images/Services/flight.gif", description = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceFlight").ToString() });
                        break;
                    case "TRANSPORT":
                        menu.Add(new { controlUrl = "Controls/RSAdd_Transport.aspx", imageUrl = "Images/Services/transport.gif", description = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceTransport").ToString() });
                        break;
                    case "RENTING":
                        menu.Add(new { controlUrl = "Controls/RSAdd_Renting.aspx", imageUrl = "Images/Services/renting.gif", description = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceRenting").ToString() });
                        break;
                    case "EXCURSION":
                        menu.Add(new { controlUrl = "Controls/RSAdd_Excursion.aspx", imageUrl = "Images/Services/excursion.gif", description = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceExcursion").ToString() });
                        break;
                    case "INSURANCE":
                        menu.Add(new { controlUrl = "Controls/RSAdd_Insurance.aspx", imageUrl = "Images/Services/insurance.gif", description = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceInsurance").ToString() });
                        break;
                    case "VISA":
                        menu.Add(new { controlUrl = "Controls/RSAdd_Visa.aspx", imageUrl = "Images/Services/visa.gif", description = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceVisa").ToString() });
                        break;
                    case "TRANSFER":
                        menu.Add(new { controlUrl = "Controls/RSAdd_TransferV2.aspx", imageUrl = "Images/Services/transfer.gif", description = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceTransfer").ToString() });
                        break;
                    case "HANDFEE":
                        menu.Add(new { controlUrl = "Controls/RSAdd_Flight.aspx", imageUrl = "Images/Services/handfee.gif", description = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceHandfee").ToString() });
                        break;
                    default:
                        menu.Add(new { controlUrl = "Controls/RSAdd_Other.aspx", imageUrl = "Images/Services/other.gif", description = string.IsNullOrEmpty(row.NameL) ? row.Name : row.NameL });
                        break;
                }
            }
        }
        menu.Add(new { controlUrl = "Controls/ExtraService_Add.aspx", imageUrl = "Images/Services/extraservice.gif", description = HttpContext.GetGlobalResourceObject("LibraryResource", "AddExtraService").ToString() });

        return menu;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        List<ResGroup> groupList = new ReservationRequestForm().getResGroupList(UserData, ref errorMsg);
        groupList.Insert(0, new ResGroup() { Agency = UserData.AgencyID, CheckIn = null, Days = null, FullName = "&lt;New Group&gt;", GroupNo = -1 });
        groupList.Insert(0, new ResGroup() { Agency = UserData.AgencyID, CheckIn = null, Days = null, FullName = "&lt;n/a&gt;", GroupNo = -2 });

        int? defaultNation = UserData.Country != null ? UserData.Country.Value : 1;
        Nationality nationality = new Locations().getDefaultNationality(UserData, defaultNation, ref errorMsg);

        List<Nationality> nationalList = CacheObjects.getNationality(UserData);

        List<Location> locationList = new Locations().getLocationList(UserData, LocationType.None, null, null, null, null, ref errorMsg);
        var lList = from q in locationList
                    where q.Parent != q.RecID
                    orderby q.Country, q.City, q.Town, q.Village
                    select new {
                        RecID = q.RecID,
                        Name = q.Name,
                        FullName = locationList.Find(f => f.RecID == q.Country).Name +
                                   (q.City.HasValue ? " -> " + locationList.Find(f => f.RecID == q.City).Name : "") +
                                   (q.Town.HasValue ? " -> " + locationList.Find(f => f.RecID == q.Town).Name : "") +
                                   (q.Village.HasValue ? " -> " + locationList.Find(f => f.RecID == q.Village).Name : ""),
                        FullNameL = locationList.Find(f => f.RecID == q.Country).NameL +
                                    (q.City.HasValue ? " -> " + locationList.Find(f => f.RecID == q.City).NameL : "") +
                                    (q.Town.HasValue ? " -> " + locationList.Find(f => f.RecID == q.Town).NameL : "") +
                                    (q.Village.HasValue ? " -> " + locationList.Find(f => f.RecID == q.Village).NameL : "")
                    };

        List<ShortSupplierRecord> supplierList = new Suppliers().getSupplierList(UserData, ref errorMsg);

        List<EntrancePortRecord> entrancePortList = new Visas().getEntrancePortList(UserData, ref errorMsg);

        List<CodeName> resType = new List<CodeName>();
        if (!UserData.AgencyRec.SaleIndRes.HasValue || (UserData.AgencyRec.SaleIndRes.HasValue && UserData.AgencyRec.SaleIndRes.Value))
            resType.Add(new CodeName { Code = "0", Name = "&lt;Individual&gt;" });
        if (!string.IsNullOrEmpty(UserData.AgencyRec.IATA)) {
            if (!UserData.AgencyRec.SaleUmrah.HasValue || (UserData.AgencyRec.SaleUmrah.HasValue && UserData.AgencyRec.SaleUmrah.Value))
                resType.Add(new CodeName { Code = "1", Name = "Umrah" });
            if (!UserData.AgencyRec.SaleHajj.HasValue || (UserData.AgencyRec.SaleHajj.HasValue && UserData.AgencyRec.SaleHajj.Value))
                resType.Add(new CodeName { Code = "2", Name = "Hajj" });
        }


        return new {
            GroupList = groupList,
            CurrentNational = string.IsNullOrEmpty(UserData.Nationality) ? nationality.Code3 : UserData.Nationality,
            Nationality = nationalList,
            LocationList = lList,
            SupplierList = supplierList,
            EntrancePortList = entrancePortList,
            OtherServiceButtons = getOtherServiceMenu(),
            ResTypeList = resType
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getPackageWizard(RRFCriteria data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();
        ResData = new ReservationRequestForm().getResData(UserData, ResData, data, ref errorMsg);
        if (string.IsNullOrEmpty(errorMsg)) {
            HttpContext.Current.Session["ResData"] = ResData;
            return true;
        } else {
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getCustomers()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        int? defaultNation = UserData.Country != null ? UserData.Country.Value : 1;
        Nationality nationality = new Locations().getDefaultNationality(UserData, defaultNation, ref errorMsg);

        List<Nationality> nationalityList = new Locations().getNationalityList(ref errorMsg);

        int cnt = 0;
        var resCust = from c in ResData.ResCust
                      select new {
                          oddEven = cnt++ % 2 == 0,
                          SeqNo = c.SeqNo,
                          TitleList = from t in ResData.Title
                                      where t.Enable
                                      select new {
                                          t.TitleNo,
                                          t.Code,
                                          thisTitle = c.Title == t.TitleNo
                                      },
                          Surname = c.Surname,
                          Name = c.Name,
                          BirthDate = c.Birtday.HasValue ? c.Birtday.Value.ToShortDateString() : string.Empty,
                          Age = c.Age,
                          NationalityList = from n in nationalityList
                                            select new {
                                                n.Code3,
                                                n.Name,
                                                thisNationality = c.Nationality == n.Code3
                                            },
                          HasPassport = c.HasPassport.HasValue ? c.HasPassport.Value : false,
                          Leader = string.Equals(c.Leader, "Y"),
                          Phone = c.Phone
                      };

        string dateFormat = new TvBo.Common().getDateFormat(UserData.Ci).Replace('/', UserData.Ci.DateTimeFormat.DateSeparator[0]);
        string dateMaskA = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ');
        string[] dateMaskB = dateMaskA.Split(UserData.Ci.DateTimeFormat.DateSeparator[0]);
        string _dateMask = string.Empty;
        string _dateFormat = string.Empty;
        foreach (string row in dateMaskB)
            if (!string.IsNullOrEmpty(row.ToString()))
                switch (row[0].ToString().ToLower()) {
                    case "d":
                        _dateMask += UserData.Ci.DateTimeFormat.DateSeparator[0] + "99";
                        _dateFormat += UserData.Ci.DateTimeFormat.DateSeparator[0] + "dd";
                        break;
                    case "m":
                        _dateMask += UserData.Ci.DateTimeFormat.DateSeparator[0] + "99";
                        _dateFormat += UserData.Ci.DateTimeFormat.DateSeparator[0] + "MM";
                        break;
                    case "y":
                        _dateMask += UserData.Ci.DateTimeFormat.DateSeparator[0] + "9999";
                        _dateFormat += UserData.Ci.DateTimeFormat.DateSeparator[0] + "yyyy";
                        break;
                    default:
                        break;
                }
        _dateMask = _dateMask.Remove(0, 1);
        var returnVal = new {
            lblTitle = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustTitle").ToString(),
            lblSurname = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustSurname").ToString(),
            lblName = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustName").ToString(),
            lblBirthDate = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustBirthDate").ToString(),
            lblAge = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustAge").ToString(),
            lblNationality = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustCitizenship").ToString(),
            lblPassport = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPass").ToString(),
            lblLeader = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustLeader").ToString(),
            lblPhone = HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPhone").ToString(),
            CheckIn = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToString(dateFormat) : "",
            formatDate = dateFormat,
            dateMask = _dateMask,
            Customers = resCust
        };
        return returnVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getOtherService()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        useLocalName = false;

        foreach (TvBo.ResServiceRecord row in ResData.ResService) {
            string ServiceDesc = string.Empty;
            #region Description
            switch (row.ServiceType) {
                case "HOTEL":
                    ServiceDesc = (useLocalName ? row.RoomNameL : row.RoomName) + "," + row.Board + " , " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "FLIGHT":
                    FlightDayRecord flg = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                    ServiceDesc += flg != null && flg.TDepDate.HasValue ? flg.TDepDate.Value.ToShortDateString() : row.BegDate.Value.ToShortDateString();
                    break;
                case "TRANSPORT":
                    ServiceDesc += row.BegDate.Value.ToShortDateString();
                    break;
                case "TRANSFER":
                    TransferRecord trf = new Transfers().getTransfer(UserData.Market, row.Service, ref errorMsg);
                    if (trf != null && string.Equals(trf.Direction, "R"))
                        ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    else
                        ServiceDesc += row.BegDate.Value.ToShortDateString();
                    break;
                case "RENTING":
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "EXCURSION":
                    ServiceDesc += row.BegDate.Value.ToShortDateString();
                    break;
                case "INSURANCE":
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "VISA":
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "HANDFEE":
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                default:
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
            }
            #endregion
            row.Description = (useLocalName ? row.ServiceNameL : row.ServiceName) + (string.Equals(row.ServiceType, "HOTEL") ? (useLocalName ? row.RoomNameL : row.RoomName) + "," + row.Board : "");
        }

        object _showIncSrvPrice = new TvBo.Common().getFormConfigValue("MakeRes", "ShowIncSrvPrice");
        bool? ShowIncSrvPrice = Conversion.getBoolOrNull(_showIncSrvPrice);

        List<VehicleCategoryRecord> vehicleCategoryList = new List<VehicleCategoryRecord>();
        if (VersionControl.tableIsExists("VehicleCategory", ref errorMsg))
            vehicleCategoryList = new ReservationRequestForm().getVehicleCategory(UserData, ref errorMsg);

        List<string> vehicleCategoryServices = new List<string> { /*"EXCURSION", */"TRANSFER" };

        var resService = from s in ResData.ResService
                         where !string.Equals(s.ServiceType, "HOTEL")
                         select new {
                             RecId = s.RecID,
                             ServiceType = s.ServiceTypeName,
                             ServiceDesc = s.ServiceName,
                             Compulsory = s.Compulsory.HasValue ? s.Compulsory.Value : false,
                             IncPack = string.Equals(s.IncPack, "Y"),
                             Unit = s.Unit,
                             Price = string.Equals(s.IncPack, "Y") || s.Compulsory == true ? HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString() : (s.SalePrice.HasValue ? s.SalePrice.Value.ToString("#,###.00") + " " + s.SaleCur : ""),
                             editServiceUrl = getOtherServiceButton(UserData, s),
                             hideEditService = true,
                             hideCancelService = true,
                             notShowVehicleCatID = !vehicleCategoryServices.Contains(s.ServiceType),
                             VehicleCatIDList = from vc in vehicleCategoryList
                                                select new {
                                                    ID = vc.RecID,
                                                    Name = vc.Name,
                                                    selected = vc.RecID == s.VehicleCatID
                                                },
                             VehicleUnit = s.VehicleUnit
                         };

        return new {
            lblServiceType = HttpContext.GetGlobalResourceObject("MakeReservation", "resSerServiceType").ToString(),
            lblServiceDesc = HttpContext.GetGlobalResourceObject("MakeReservation", "resSerServiceDesc").ToString(),
            lblCompulsory = HttpContext.GetGlobalResourceObject("MakeReservation", "resSerComp").ToString(),
            lblIncPack = HttpContext.GetGlobalResourceObject("MakeReservation", "resSerIncPack").ToString(),
            lblUnit = HttpContext.GetGlobalResourceObject("MakeReservation", "resSerUnit").ToString(),
            lblPrice = HttpContext.GetGlobalResourceObject("MakeReservation", "resSerPrice").ToString(),
            lblVehicleUnit = HttpContext.GetGlobalResourceObject("Controls", "viewVehicleUnit").ToString(),
            lblEditService = HttpContext.GetGlobalResourceObject("ResView", "lblEditService"),
            lblCancelService = HttpContext.GetGlobalResourceObject("ResView", "lblCancelService"),
            OtherService = resService
        };
    }

    public static string getOtherServiceButton(User UserData, ResServiceRecord row)
    {
        string editPageUrl = string.Empty;
        #region url
        switch (row.ServiceType) {
            case "HOTEL": editPageUrl = "RSEdit_Hotel.aspx"; break;
            case "FLIGHT": editPageUrl = "RSEdit_Flight.aspx"; break;
            case "TRANSPORT": editPageUrl = "RSEdit_Transport.aspx"; break;
            case "TRANSFER": editPageUrl = "RSEdit_TransferV2.aspx"; break;
            case "HANDFEE": editPageUrl = "RSEdit_Handfee.aspx"; break;
            case "EXCURSION": editPageUrl = "RSEdit_Excursion.aspx"; break;
            case "INSURANCE": editPageUrl = "RSEdit_Insurance.aspx"; break;
            case "RENTING": editPageUrl = "RSEdit_Renting.aspx"; break;
            case "VISA": editPageUrl = "RSEdit_Visa.aspx"; break;
            default: editPageUrl = "RSEdit_Other.aspx"; break;
        }
        string editServiceUrl = string.Format("Controls/{0}?RecID={1}&NewRes=1",
                        editPageUrl,
                        row.RecID.ToString());
        #endregion
        return editServiceUrl;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getHotelService()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        ResDataRecord ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        useLocalName = false;

        var hotelServiceGroup = from q in ResData.ResService
                                where string.Equals(q.ServiceType, "HOTEL")
                                group q by new {
                                    q.Service,
                                    q.ServiceName,
                                    q.ServiceNameL,
                                    q.BegDate,
                                    q.EndDate,
                                    q.Duration,
                                    q.Room,
                                    q.RoomName,
                                    q.RoomNameL,
                                    q.Board,
                                    q.BoardName,
                                    q.BoardNameL,
                                    q.Accom,
                                    q.AccomName,
                                    q.AccomNameL
                                } into k
                                select new {
                                    k.Key.Service,
                                    k.Key.ServiceName,
                                    k.Key.ServiceNameL,
                                    k.Key.BegDate,
                                    k.Key.EndDate,
                                    k.Key.Duration,
                                    k.Key.Room,
                                    k.Key.RoomName,
                                    k.Key.RoomNameL,
                                    k.Key.Board,
                                    k.Key.BoardName,
                                    k.Key.BoardNameL,
                                    k.Key.Accom,
                                    k.Key.AccomName,
                                    k.Key.AccomNameL,
                                    Description = (useLocalName ? k.Key.ServiceNameL : k.Key.ServiceName) + ", " + (useLocalName ? k.Key.RoomNameL : k.Key.RoomName) + ", " + k.Key.Board + ", " + (useLocalName ? k.Key.AccomNameL : k.Key.AccomName)
                                };

        object _showIncSrvPrice = new TvBo.Common().getFormConfigValue("MakeRes", "ShowIncSrvPrice");
        bool? ShowIncSrvPrice = Conversion.getBoolOrNull(_showIncSrvPrice);

        var resService = from s in hotelServiceGroup
                         where !string.IsNullOrEmpty(s.Room) && !string.IsNullOrEmpty(s.Accom) && !string.IsNullOrEmpty(s.Board)
                         select new {
                             Description = s.Description,
                             Unit = ResData.ResService.Where(w => w.Service == s.Service && 
                                                                  w.BegDate == s.BegDate && 
                                                                  w.EndDate == s.EndDate && 
                                                                  w.Room == s.Room && 
                                                                  w.Board == s.Board && 
                                                                  w.Accom == s.Accom && 
                                                                  !string.IsNullOrEmpty(w.Room) && 
                                                                  !string.IsNullOrEmpty(w.Accom) && 
                                                                  !string.IsNullOrEmpty(w.Board)
                                                            ).Count(),
                             CheckIn = s.BegDate.HasValue ? s.BegDate.Value.ToShortDateString() : string.Empty,
                             CheckOut = s.EndDate.HasValue ? s.EndDate.Value.ToShortDateString() : string.Empty,
                             Night = s.Duration                             
                         };

        return new {            
            Hotels = resService
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string setCustomers(List<resCustjSonData> Customers)
    {
        if (HttpContext.Current.Session["UserData"] == null || /*string.IsNullOrEmpty(data)*/ Customers == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ResData"] == null) {
            return "NO";
        }

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();

        ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);

        try {
            resCustjSonData cust = new resCustjSonData();
            //data = data.Replace('|', '"').Replace('<', '{').Replace('>', '}');
            //string[] _data = data.Split('@');
            for (int i = 0; i < Customers.Count; i++) {
                cust = Customers[i];//Newtonsoft.Json.JsonConvert.DeserializeObject<resCustjSonData>(_data[i]);
                ResCustRecord _cust = ResData.ResCust.Find(f => f.SeqNo == cust.SeqNo);
                TitleRecord _title = ResData.Title.Find(f => f.TitleNo == cust.Title);
                _cust.Title = cust.Title;
                _cust.TitleStr = _title != null ? _title.Code : "";
                _cust.Surname = cust.Surname;
                _cust.Name = cust.Name;
                _cust.Birtday = Conversion.getDateTimeOrNull(cust.Birtday);
                _cust.Age = TvBo.Common.getAge(_cust.Birtday, UserData.TvParams.TvParamReser.AgeCalcType > 2 ? ResData.ResMain.EndDate : ResData.ResMain.BegDate);
                _cust.IDNo = cust.IDNo;
                _cust.PassSerie = cust.PassSerie;
                _cust.PassNo = cust.PassNo;
                _cust.PassExpDate = Conversion.getDateTimeOrNull(cust.PassExpDate);
                _cust.PassIssueDate = Conversion.getDateTimeOrNull(cust.PassIssueDate);
                _cust.PassGiven = cust.PassGiven;
                _cust.Phone = cust.Phone;
                _cust.Nation = cust.Nation;
                _cust.Nationality = cust.Nationality;
                _cust.HasPassport = cust.Passport;
                _cust.Leader = cust.Leader.HasValue ? (cust.Leader.Value ? "Y" : "N") : "N";
            }

            HttpContext.Current.Session["ResData"] = ResData;
            return "OK";
        }
        catch {
            return "NO";
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object saveReservation(List<resCustjSonData> Customers, bool goAhead, Int16? ResType, List<ServiceVehicleID> VehicleCatIDList, int? GroupNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        setCustomers(Customers);

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        ResDataRecord ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);

        List<ResServiceRecord> resServiceList = new List<ResServiceRecord>();
        List<ResConRecord> resConList = new List<ResConRecord>();
        List<ResServiceExtRecord> resServiceExtList = new List<ResServiceExtRecord>();
        List<ResConExtRecord> resConExtList = new List<ResConExtRecord>();

        foreach (ResServiceRecord rowRs in ResData.ResService) {
            ServiceVehicleID serviceVehicleID = VehicleCatIDList.Find(f => f.ServiceID == rowRs.RecID);
            if (serviceVehicleID != null) {
                rowRs.VehicleCatID = serviceVehicleID.VehicleCatID;
                rowRs.VehicleUnit = serviceVehicleID.VehicleUnit;
            }
            if ((!string.IsNullOrEmpty(rowRs.Accom) && !string.IsNullOrEmpty(rowRs.Board) && !string.IsNullOrEmpty(rowRs.Room) && string.Equals(rowRs.ServiceType, "HOTEL")) || !string.Equals(rowRs.ServiceType, "HOTEL")) {
                resServiceList.Add(rowRs);
                foreach (ResConRecord rowRc in ResData.ResCon.Where(w => w.ServiceID == rowRs.RecID))
                    resConList.Add(rowRc);

                foreach (ResServiceExtRecord rowRsE in ResData.ResServiceExt.Where(w => w.ServiceID == rowRs.RecID)) {
                    resServiceExtList.Add(rowRsE);
                    foreach (ResConExtRecord rowRcE in ResData.ResConExt.Where(w => w.ServiceID == rowRsE.RecID))
                        resConExtList.Add(rowRcE);
                }
            }
        }

        ResData.ResService = resServiceList;
        ResData.ResCon = resConList;
        ResData.ResServiceExt = resServiceExtList;
        ResData.ResConExt = resConExtList;

        List<ReservastionSaveErrorRecord> returnData = new List<ReservastionSaveErrorRecord>();

        ResMainRecord resMain = ResData.ResMain;



        resMain.OptDate = null;
        if (ResType.HasValue)
            resMain.ResType = ResType;

        bool saveAsDraft = true;
        bool? setOptionTime = null;

        #region Credit control
        Int16 CreditCont = new TvBo.Reservation().CreditControl(UserData.AgencyID, UserData.Market, ref errorMsg);

        if (CreditCont == 2) {
            returnData.Add(new ReservastionSaveErrorRecord {
                ControlOK = false,
                Message = HttpContext.GetGlobalResourceObject("LibraryResource", "NoCreditLimit").ToString()
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }
        #endregion

        #region No servis error
        if (ResData.ResService.Count < 1) {
            returnData.Add(new ReservastionSaveErrorRecord {
                ControlOK = false,
                Message = HttpContext.GetGlobalResourceObject("LibraryResource", "NoResServiceMsg").ToString()
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }
        #endregion

        #region Check flight time
        if (!(new Flights().CheckFlightTime(UserData, ResData.ResService, ref errorMsg))) {
            returnData.Add(new ReservastionSaveErrorRecord {
                ControlOK = false,
                Message = HttpContext.GetGlobalResourceObject("LibraryResource", "CheckInTimeOver").ToString()
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }
        #endregion

        List<CustControlErrorRecord> custControl = new Reservation().CustControl(UserData, ResData, ref errorMsg);
        if ((custControl == null || custControl.Count == 1) && (custControl.Where(w => w.ErrorCode == 0).Count() == 1 || custControl.Where(w => w.ErrorCode == 30).Count() == 1)) {
            resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
            if (setOptionTime.HasValue && setOptionTime.Value == false)
                resMain.OptDate = null;

            returnData = new Reservation().SaveReservation(UserData, ref ResData, saveAsDraft);

            if (returnData == null || returnData.Count < 1) {
                returnData.Add(new ReservastionSaveErrorRecord {
                    ControlOK = false,
                    Message = HttpContext.GetGlobalResourceObject("LibraryResource", "ResNotSaved").ToString(),
                    OtherReturnValue = custControl.FirstOrDefault().ErrorCode.ToString()
                });
                return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
            } else {
                bool? saveResAfterResView = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "SaveResAfterResView"));
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK) {
                    if (UserData.AgencyRec != null && UserData.AgencyRec.AceExport) {
                        string AceFileName = string.Empty;
                        if (!(new Aces().SendAceTo(UserData, ResData, AppDomain.CurrentDomain.BaseDirectory + "ACE//", ref AceFileName, ref errorMsg))) {
                        }
                    }

                    returnData.Clear();
                    ReservastionSaveErrorRecord retVal = new ReservastionSaveErrorRecord();
                    if (custControl.Where(w => w.ErrorCode == 30).Count() == 1) {
                        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
                        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
                        string msg = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString(), ResData.ResMain.ResNo);
                        msg += "<br />";
                        msg += HttpContext.GetGlobalResourceObject("MakeReservation", "parentAuthorizationMsg").ToString();
                        msg += "<br />";
                        msg += string.Format("<a href=\"#\" onClick=\"window.open('{0}','mywindow')\" style=\"font-style:italic; font-weight:bold;\">{1}</a>",
                                             WebRoot.BasePageRoot + DocumentFolder + "/" + UserData.Market + "/authorization.pdf",
                                             HttpContext.GetGlobalResourceObject("MakeReservation", "parentAuthorizationLnk"));
                        retVal.ControlOK = true;
                        retVal.Message = msg;
                        retVal.OtherReturnValue = custControl != null && custControl.Count > 0 ? custControl.FirstOrDefault().ErrorCode.ToString() : string.Empty;
                        retVal.GotoReservation = !saveResAfterResView.HasValue || (saveResAfterResView.HasValue && saveResAfterResView.Value);
                        returnData.Add(retVal);
                    } else
                        if (custControl.Where(w => w.ErrorCode == 32).Count() > 1 && goAhead != true) {
                        retVal.ControlOK = false;
                        retVal.Message = custControl.Where(w => w.ErrorCode == 32).FirstOrDefault().ErrorMessage;
                        retVal.OtherReturnValue = custControl != null && custControl.Count > 0 ? custControl.FirstOrDefault().ErrorCode.ToString() : string.Empty;
                        retVal.GotoReservation = false;
                        returnData.Add(retVal);
                    } else {
                        List<resPayPlanRecord> resPayPlan = new ReservationMonitor().getPaymentPlan(ResData.ResMain.ResNo, ref errorMsg);
                        string paymentPlanStr = new TvBo.ReservationMonitor().getPaymentPlanHTML(ResData.ResMain.ResNo, UserData.Ci.LCID);
                        retVal.ControlOK = true;
                        if (resPayPlan != null && resPayPlan.Count > 0) {
                            resPayPlanRecord firstPayment = resPayPlan.OrderBy(o => o.DueDate).FirstOrDefault();
                            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                                retVal.Message = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString() + "<br />" + paymentPlanStr,
                                                                        ResData.ResMain.ResNo);
                            else {
                                string paidMessage = string.Empty;
                                if (firstPayment.Amount.HasValue)
                                    paidMessage = string.Format(HttpContext.GetGlobalResourceObject("MakeReservation", "lblAmount").ToString(),
                                                                           firstPayment.Amount.Value.ToString("#,###.00") + " " + firstPayment.Cur,
                                                                           firstPayment.DueDate.ToShortDateString());
                                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Orex))
                                    paidMessage = string.Empty;
                                retVal.Message = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString() + "<br />{1}",
                                                                        ResData.ResMain.ResNo,
                                                                        paidMessage);
                            }
                        } else
                            retVal.Message = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString(),
                                ResData.ResMain.ResNo);

                        retVal.OtherReturnValue = custControl != null && custControl.Count > 0 ? custControl.FirstOrDefault().ErrorCode.ToString() : string.Empty;
                        //retVal.SendEmailB2C = getEmailUrl(UserData, ResData);
                        retVal.GotoPaymentPage = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur);
                        retVal.GotoReservation = !saveResAfterResView.HasValue || (saveResAfterResView.HasValue && saveResAfterResView.Value);
                        retVal.ResNo = ResData.ResMain.ResNo;
                        returnData.Add(retVal);
                    }

                    return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
                } else {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
                }
            }
        } else {
            returnData.Add(new ReservastionSaveErrorRecord {
                ControlOK = false,
                Message = new Reservation().getCustControlErrorMessage(custControl),
                OtherReturnValue = custControl != null && custControl.Count > 0 ? custControl.FirstOrDefault().ErrorCode.ToString() : string.Empty
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object createCustomers(Int16? ResType, int? GroupNo, DateTime? CheckIn, Int16? Night, Int16? Adult, Int16? Child, Int16? Infant, string LeaderSurname, string LeaderName, int? DepCity, int? ArrCity)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();
        RRFCriteria criteria = new RRFCriteria();
        criteria.ResType = ResType;
        criteria.Adult = Adult;
        criteria.Child = Child;
        criteria.Infant = Infant;
        criteria.CheckIn = CheckIn;
        criteria.Night = Night;
        criteria.LeaderSurname = LeaderSurname;
        criteria.LeaderName = LeaderName;
        criteria.HolPack = string.Empty;
        criteria.ArrCity = ArrCity.HasValue ? ArrCity.Value : UserData.AgencyRec.Location;
        criteria.DepCity = DepCity.HasValue ? DepCity.Value : criteria.ArrCity;

        ResData = new ReservationRequestForm().getResData(UserData, ResData, criteria, ref errorMsg);
        if (string.IsNullOrEmpty(errorMsg)) {
            HttpContext.Current.Session["ResData"] = ResData;
            return true;
        } else {
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object updateCustomerData(int? SeqNo, PassportReaderData_ComboSmart PassportData)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ResData"] == null) {
            return "NO";
        }

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();
        string Passport_Type = string.Empty;
        string Passport_StateCode = string.Empty;
        string Passport_SurName = string.Empty;
        string Passport_Name = string.Empty;
        string Passport_Serie = string.Empty;
        string Passport_No = string.Empty;
        string Passport_Nationality = string.Empty;
        string Passport_DateOfBirth = string.Empty;
        string Passport_Sex = string.Empty;
        string Passport_ExpireDate = string.Empty;
        string Passport_IDNo = string.Empty;

        ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);
        try {
            if (PassportData != null &&
                !string.IsNullOrEmpty(PassportData.Field1) && PassportData.Field1.Length == 44 &&
                !string.IsNullOrEmpty(PassportData.Field2) && PassportData.Field2.Length == 44) {

                Passport_Type = PassportData.Field1.Substring(0, 5).Split('<')[0];
                Passport_StateCode = PassportData.Field1.Substring(0, 5).Split('<')[1];
                Passport_SurName = PassportData.Field1.Substring(5).Substring(0, PassportData.Field1.Substring(6).IndexOf("<<") + 1);
                Passport_Name = PassportData.Field1.Substring(PassportData.Field1.IndexOf("<<") + 1).Trim('<');
                Passport_Serie = PassportData.Field2.Substring(0, 1);
                Passport_No = PassportData.Field2.Substring(1, 8);
                Passport_Nationality = PassportData.Field2.Substring(10, 3);
                Passport_DateOfBirth = PassportData.Field2.Substring(13, 6);
                Passport_Sex = PassportData.Field2.Substring(20, 1);
                Passport_ExpireDate = PassportData.Field2.Substring(21, 6);
                Passport_IDNo = PassportData.Field2.Substring(28, 14).Trim('<');

            } else {
                return false;
            }
        }
        catch (Exception Ex) {
            errorMsg = Ex.Message;
            return false;
        }
        List<ResCustRecord> resCustList = ResData.ResCust;
        ResCustRecord resCust = resCustList.Find(f => f.SeqNo == SeqNo);

        if (resCust != null) {
            resCust.Surname = Passport_SurName;
            resCust.Name = Passport_Name;
            resCust.PassSerie = Passport_Serie;
            resCust.PassNo = Passport_No;
            resCust.Nationality = Passport_Nationality;
            resCust.Birtday = new DateTime(Convert.ToInt32(Passport_DateOfBirth.Substring(0, 2)) > 20 ? Convert.ToInt32("19" + Passport_DateOfBirth.Substring(0, 2)) : Convert.ToInt32("20" + Passport_DateOfBirth.Substring(0, 2)), Convert.ToInt32(Passport_DateOfBirth.Substring(2, 2)), Convert.ToInt32(Passport_DateOfBirth.Substring(4, 2)));
            resCust.PassExpDate = new DateTime(Convert.ToInt32("20" + Passport_ExpireDate.Substring(0, 2)), Convert.ToInt32(Passport_ExpireDate.Substring(2, 2)), Convert.ToInt32(Passport_ExpireDate.Substring(4, 2)));
            resCust.IDNo = Passport_IDNo;

            HttpContext.Current.Session["ResData"] = ResData;
            return true;
        } else {
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object removeService(int? RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResDataRecord OldResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);
        List<ResServiceRecord> resServiceList = ResData.ResService;
        ResServiceRecord resService = resServiceList.Find(f => f.RecID == RecID);
        if (resService == null) return "Unknown error";
        ResData = new Reservation().DeleteResServiceFromResData(UserData, ResData, RecID, false, ref errorMsg);
        if (string.IsNullOrEmpty(errorMsg)) {
            HttpContext.Current.Session["ResData"] = ResData;
            return true;
        } else {
            return false;
        }
    }
}
