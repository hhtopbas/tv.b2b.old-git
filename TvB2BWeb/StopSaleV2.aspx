﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StopSaleV2.aspx.cs" Inherits="StopSaleV2" %>

<%@ Register Src="Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "StopSale") %></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>    

    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

    <script src="Scripts/datatable/jquery.dataTables.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.datepick/jquery-ui-i18n.js" type="text/javascript"></script>

    <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="CSS/StopSaleV2.css" rel="stylesheet" type="text/css" />
    <link href="CSS/jquery.table.jui.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #StopSaleListGrid { overflow: auto; min-height: 550px; }
        #PageNumbersContent a { width: 20px !important; height: 20px; margin: 10px 10px 0 0; }
        .headerTh { font-weight: normal !important; color: #555555; }
        .sorting_1_odd { background-color: #D3D6FF; }
        .sorting_1_even { background-color: #EAEBFF; }
    </style>

    <script type="text/javascript">

        if (typeof window.event != 'undefined')
            document.onkeydown = function() {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function(e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function logout() {
            window.location = 'Default.aspx'; $('<div>' + '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>' + '</div>').dialog({
                autoOpen: true,
                position: {
                    my: 'center',
                    at: 'center'
                },
                modal: true,
                resizable: true,
                autoResize: true,
                bigframe: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                        $(this).dialog("close");
                        $(this).dialog("destroy");
                        window.location = 'Default.aspx';
                    }
                }
            });
        }

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function stopSaleFlightSeatWithOpt() {
            $.ajax({
                type: "POST",
                data: '{}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'StopSaleV2.aspx/GetFirstDataFlightSeatWithOpt',
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $("#dateFormat").val(data.DateFormat);
                        if (data.BegDate != null && data.BegDate != '') {
                            var _date1 = new Date(Date(eval(data.BegDate)).toString());
                            $("#fltDate1").val($.format.date(_date1.toString(), data.DateFormat));
                        }
                        else $("#fltDate2").val('');
                        if (data.EndDate != null && data.EndDate != '') {
                            var _date2 = new Date(Date(eval(data.EndDate)).toString());
                            $("#fltDate2").val($.format.date(_date2.toString(), data.DateFormat));
                        }
                        else $("#fltDate2").val('');
                        setDepCity(data);
                        setFlight(data);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function stopSaleStandartDefaults() {
            $.ajax({
                type: "POST",
                data: '{}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'StopSaleV2.aspx/GetFirstDataStandart',
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $("#dateFormat").val(data.DateFormat);
                        if (data.BegDate != null && data.BegDate != '') {
                            var _date1 = new Date(Date(eval(data.BegDate)).toString());
                            $("#fltDate1").val($.format.date(_date1.toString(), data.DateFormat));
                        }
                        else $("#fltDate2").val('');
                        if (data.EndDate != null && data.EndDate != '') {
                            var _date2 = new Date(Date(eval(data.EndDate)).toString());
                            $("#fltDate2").val($.format.date(_date2.toString(), data.DateFormat));
                        }
                        else $("#fltDate2").val('');
                        setHotels(data);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function setDepCity(data) {
            $("#fltDepCity").html('');
            $("#fltDepCity").append("<option value=''>" + '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>' + "</option>");
            $.each(data.DepCity, function(i) {
                $("#fltDepCity").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
            });
        }

        function setArrCity(data) {
            $("#fltArrCity").html('');
            $("#fltArrCity").append("<option value=''>" + '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>' + "</option>");
            if (data == undefined || data == null)
                return;
            else {
                $.each(data, function(i) {
                    $("#fltArrCity").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
                });
            }
        }

        function setFlight(data) {
            $("#fltFlightCls").html('');
            $("#fltFlightCls").append("<option value=''>" + '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>' + "</option>");
            $.each(data.FlgCls, function(i) {
                $("#fltFlightCls").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
        }

        function setHotels(data) {
            $("#fltHotel").html('');
            $("#fltHotel").append("<option value=''>" + '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>' + "</option>");
            $.each(data.HotelList, function(i) {
                $("#fltHotel").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
        }

        function setArrCityHotels(data) {
            $("#fltHotel").html('');
            $("#fltHotel").append("<option value=''>" + '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>' + "</option>");
            $.each(data, function(i) {
                $("#fltHotel").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
        }

        function ChangeDepCity() {
            var depCityID = $("#fltDepCity").val();
            $.ajax({
                type: "POST",
                data: '{"depCityID":' + depCityID + '}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'StopSaleV2.aspx/GetArrCityByDepCity',
                success: function(msg) {
                    var data = $.json.decode(msg.d);
                    setArrCity(data);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function ChangeArrCity() {
            var arrCityID = $("#fltArrCity").val();
            $.ajax({
                type: "POST",
                data: '{"begDate":"' + $("#fltDate1").datepicker({ dateFormat: $("#dateFormat").val() }).val() + '","endDate":"' + $("#fltDate2").datepicker({ dateFormat: $("#dateFormat").val() }).val() + '","arrCityID":' + arrCityID + '}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'StopSaleV2.aspx/GetArrCityHotel',
                success: function(msg) {
                    var data = $.json.decode(msg.d);
                    setArrCityHotels(data);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function standartStopSale() {
            var oLanguage = {
                "sProcessing": '<%= GetGlobalResourceObject("LibraryResource", "sProcessing") %>',
                "sLengthMenu": '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>',
                "sZeroRecords": '<%= GetGlobalResourceObject("LibraryResource", "sZeroRecords") %>',
                "sEmptyTable": '<%= GetGlobalResourceObject("LibraryResource", "sEmptyTable") %>',
                "sInfo": '<%= GetGlobalResourceObject("LibraryResource", "sInfo") %>',
                "sInfoEmpty": '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>',
                "sInfoEmpty": '<%= GetGlobalResourceObject("LibraryResource", "sInfoEmpty") %>',
                "sInfoFiltered": '<%= GetGlobalResourceObject("LibraryResource", "sInfoFiltered") %>',
                "sInfoPostFix": '<%= GetGlobalResourceObject("LibraryResource", "sInfoPostFix") %>',
                "sSearch": '<%= GetGlobalResourceObject("LibraryResource", "sSearch") %>',
                "sUrl": '<%= GetGlobalResourceObject("LibraryResource", "sUrl") %>',
                "oPaginate": {
                    "sFirst": '<%= GetGlobalResourceObject("LibraryResource", "sFirst") %>',
                    "sPrevious": '<%= GetGlobalResourceObject("LibraryResource", "sPrevious") %>',
                    "sNext": '<%= GetGlobalResourceObject("LibraryResource", "sNext") %>',
                    "sLast": '<%= GetGlobalResourceObject("LibraryResource", "sLast") %>'
                },
                "fnInfoCallback": null
            };

            var dataString = '{"begDate":"' + $("#fltDate1").datepicker({ dateFormat: $("#dateFormat").val() }).val() + '","endDate":"' + $("#fltDate2").datepicker({ dateFormat: $("#dateFormat").val() }).val() + '","Hotel":"' + $("#fltHotel").val() + '"}';
            $.ajax({
                type: "POST",
                data: dataString,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'StopSaleV2.aspx/GetResult',
                success: function(msg) {
                    $("#StopSaleListGrid").html('');
                    $("#StopSaleGrid").html('');
                    $("#StopSaleGrid").html(msg.d);
                    $('#stopSaleTable').dataTable({
                        "sScrollX": "1000px",
                        "sScrollXInner": "125%",
                        "bScrollCollapse": true,
                        "sPaginationType": "full_numbers",
                        "sScrollY": "600",
                        "bFilter": false,
                        "bJQueryUI": true,
                        "bStateSave": true,
                        "aaSorting": [],
                        "oLanguage": oLanguage
                    });
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }
        
        function changeSlctPagingListCount(opt) {
            $("#hfPagingListCount").val(opt.toString());
            stopSaleListPagination();
        }

        function changeSlctPageNumbers(index) {
            var pagingListCount = $("#hfPagingListCount").val();
            var gt = pagingListCount * index;
            $('#stopSaleListTable tbody tr').hide();
            for (var i = gt - pagingListCount; i < gt; i++) {
                $("#stopSaleListTable tbody tr:eq(" + i + ")").show();
            }

            var totalTr = $("#stopSaleListTable tbody tr").size();
            var start = gt - pagingListCount;
            if ($("#slctPageNumbers option").size() != index) {
                if (start == 0)
                { start = 1; }
                var label = '<%= GetGlobalResourceObject("LibraryResource", "sInfo") %>';
                var label2 = label.replace('_TOTAL_', totalTr).replace('_START_', start).replace('_END_', gt);
            }
            else {
                var label = '<%= GetGlobalResourceObject("LibraryResource", "sInfo") %>';
                var label2 = label.replace('_TOTAL_', totalTr).replace('_START_', start).replace('_END_', totalTr);

            }
            $("#PageNumbersLabel").html('');
            $("#PageNumbersLabel").html(label2);

        }

        function stopSaleListPagination() {
            var totalTr = $("#stopSaleListTable tbody tr").size();
            var pagingListCount = $("#hfPagingListCount").val();
            $("#stopSaleListTable tbody tr:even").addClass("sorting_1_even");
            $("#stopSaleListTable tbody tr:odd").addClass("sorting_1_odd");
            $("#stopSaleTable tbody tr:even").addClass("sorting_1_even");
            $("#stopSaleTable tbody tr:odd").addClass("sorting_1_odd");
            $("#stopSaleListTable tbody tr").hide();
            $("#stopSaleListTable tbody tr:lt(" + pagingListCount + ")").show();

            var pageCount = Math.ceil(totalTr / pagingListCount);

            $("#slctPageNumbers").html('');
            for (var i = 1; i <= pageCount; i++) {

                $("#slctPageNumbers").append('<option value=' + i + ' >' + i + '</option>');
            }
            if (pagingListCount <= totalTr) {
                var label = '<%= GetGlobalResourceObject("LibraryResource", "sInfo") %>';
                var label2 = label.replace('_TOTAL_', totalTr).replace('_START_', 1).replace('_END_', pagingListCount);
            }
            else {
                var label = '<%= GetGlobalResourceObject("LibraryResource", "sInfo") %>';
                var label2 = label.replace('_TOTAL_', totalTr).replace('_START_', 1).replace('_END_', totalTr);
            }
            $("#PageNumbersLabel").html('');
            $("#PageNumbersLabel").html(label2);
            $("#PagingListCountLabel").html('');
            var label3 = '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>';
            var label4 = label3.replace('_MENU_', '');
            $("#PagingListCountLabel").html(label4);
        }

        function optionalStopSale() {
            var oLanguage = {
                "sProcessing": '<%= GetGlobalResourceObject("LibraryResource", "sProcessing") %>',
                "sLengthMenu": '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>',
                "sZeroRecords": '<%= GetGlobalResourceObject("LibraryResource", "sZeroRecords") %>',
                "sEmptyTable": '<%= GetGlobalResourceObject("LibraryResource", "sEmptyTable") %>',
                "sInfo": '<%= GetGlobalResourceObject("LibraryResource", "sInfo") %>',
                "sInfoEmpty": '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>',
                "sInfoEmpty": '<%= GetGlobalResourceObject("LibraryResource", "sInfoEmpty") %>',
                "sInfoFiltered": '<%= GetGlobalResourceObject("LibraryResource", "sInfoFiltered") %>',
                "sInfoPostFix": '<%= GetGlobalResourceObject("LibraryResource", "sInfoPostFix") %>',
                "sSearch": '<%= GetGlobalResourceObject("LibraryResource", "sSearch") %>',
                "sUrl": '<%= GetGlobalResourceObject("LibraryResource", "sUrl") %>',
                "oPaginate": {
                    "sFirst": '<%= GetGlobalResourceObject("LibraryResource", "sFirst") %>',
                    "sPrevious": '<%= GetGlobalResourceObject("LibraryResource", "sPrevious") %>',
                    "sNext": '<%= GetGlobalResourceObject("LibraryResource", "sNext") %>',
                    "sLast": '<%= GetGlobalResourceObject("LibraryResource", "sLast") %>'
                },
                "fnInfoCallback": null
            };
            var dataString = '{"begDate":"' + $("#fltDate1").datepicker({ dateFormat: $("#dateFormat").val() }).val() + '","endDate":"' + $("#fltDate2").datepicker({ dateFormat: $("#dateFormat").val() }).val() + '","Hotel":"' + $("#fltHotel").val() + '","depCity":' + $("#fltDepCity").val() + ',"arrCity":' + $("#fltArrCity").val() + ',"flgClass":"' + $("#fltFlightCls").val() + '"}';
            $.ajax({
                type: "POST",
                data: dataString,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'StopSaleV2.aspx/getFlightSeatWithOptResult',
                success: function(msg) {
                    $("#StopSaleListGrid").html('');
                    $("#StopSaleListGrid").html(msg.d);
                    stopSaleListPagination();
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function btnFilter_onclick() {
            var value = $('input:radio[name=rbSelectListType]:checked').val();
            if (value == '0') {
                standartStopSale();
            }
            else if (value == '1') {
                optionalStopSale();
            }
        }

        function btnUnFilter_onclick() {
            window.location = "StopSaleV2.aspx";
        }

        function showAlert(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function setOptions() {
            $.ajax({ async: false,
                type: "POST",
                url: "StopSaleV2.aspx/getDateRegion",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $.datepicker.setDefaults($.datepicker.regional[msg.d != 'en' ? msg.d : '']);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });

            $("#fltDate1").datepicker({
                showOn: "button",
                buttonImage: "Images/Calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true                
            });
            
            $("#fltDate2").datepicker({
                showOn: "button",
                buttonImage: "Images/Calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true
            });
            
            var value = $('input:radio[name=rbSelectListType]:checked').val();
            var oldValue = $("#hfOptionType").val();
            if (oldValue != value) {
                if (value == '0') {
                    $("#fltDepCity").html('');
                    $("#fltArrCity").html('');
                    $("#fltFlightCls").html('');
                    $("#fltHotel").html('');
                    $("#depCityDiv").hide();
                    $("#arrCityDiv").hide();
                    $("#flgClassDiv").hide();
                    $("#StopSaleGrid").html('');
                    $("#StopSaleListGrid").html('');
                    stopSaleStandartDefaults();
                }
                else {
                    $("#depCityDiv").show();
                    $("#arrCityDiv").show();
                    $("#flgClassDiv").show();
                    $("#fltHotel").html('');
                    $("#StopSaleGrid").html('');
                    $("#StopSaleListGrid").html('');
                    stopSaleFlightSeatWithOpt();
                }
            }
            $("#hfOptionType").val(value);
        }

        $(document).ready(function() {
            setOptions();
        });        
              
    </script>

</head>
<body>
    <form id="StopSaleForm" runat="server">
    <div class="Page">
        <input id="dateFormat" type="hidden" value='' />
        <tv1:Header ID="tvHeader" runat="server" />
        <tv1:MainMenu ID="tvMenu" runat="server" />
        <div class="divFilter">
            <div class="ssInputBlockDiv">
                <div class="labelDiv">
                    <%= GetGlobalResourceObject("StopSale", "lblFirstDate")%>
                    :
                </div>
                <div class="ssInputDiv">
                    <input id="fltDate1" type="text" />
                </div>
            </div>
            <div class="ssInputBlockDiv">
                <div class="labelDiv">
                    <%= GetGlobalResourceObject("StopSale", "lblLastDate")%>
                    :
                </div>
                <div class="ssInputDiv">
                    <input id="fltDate2" type="text" />
                </div>
            </div>
            <div id="depCityDiv" class="ssInputBlockDiv">
                <div class="labelDiv">
                    <%= GetGlobalResourceObject("ResMonitor", "lblDepartureCity")%>
                    :
                </div>
                <div class="ssInputDiv">
                    <select id="fltDepCity" onchange="ChangeDepCity();">
                    </select>
                </div>
            </div>
            <div id="arrCityDiv" class="ssInputBlockDiv">
                <div class="labelDiv">
                    <%= GetGlobalResourceObject("ResMonitor", "lblArrivalCity")%>
                    :
                </div>
                <div class="ssInputDiv">
                    <select id="fltArrCity" onchange="ChangeArrCity();">
                    </select>
                </div>
            </div>
            <div id="flgClassDiv" class="ssInputBlockDiv">
                <div class="labelDiv">
                    <%= GetGlobalResourceObject("BookTicket", "flightInfoClass")%>
                    :
                </div>
                <div class="ssInputDiv">
                    <select id="fltFlightCls">
                    </select>
                </div>
            </div>
            <div class="ssInputBlockDiv">
                <div class="labelDiv">
                    <%= GetGlobalResourceObject("StopSale", "lblHotel")%>
                    :
                </div>
                <div class="ssInputDiv">
                    <select id="fltHotel">
                    </select>
                </div>
            </div>
            <div id="divssInputBlock" class="ssInputBlockDiv">
                <div>
                    <input id="rbSelectListType0" name="rbSelectListType" type="radio" value="0" checked="checked"
                        onclick="setOptions();" />
                    <label for="rbSelectListType0">
                        <%= GetGlobalResourceObject("StopSale", "cbStandartStopSaleList")%></label>
                    <input id="rbSelectListType1" name="rbSelectListType" type="radio" value="1" onclick="setOptions();" />
                    <label for="rbSelectListType1">
                        <%= GetGlobalResourceObject("StopSale", "cbStopSaleListWithFlightOptimization")%></label>
                    <input id="hfOptionType" type="hidden" value="1" />
                    <input id="hfPagingListCount" type="hidden" value="10" />
                </div>
            </div>
            <br />
            <div id="divssInputBlockButton" class="ssInputBlockDiv">
                <table id="tablessInputBlockButton" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="tdbtnFilter">
                            <input visible="true" type="button" id="btnFilter" value='<%= GetGlobalResourceObject("StopSale", "btnFilter")%>'
                                onclick="return btnFilter_onclick()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                        </td>
                        <td class="tdbtnUnFilter">
                            <input type="button" visible="true" id="btnUnFilter" value='<%= GetGlobalResourceObject("StopSale", "btnClearFilter")%>'
                                onclick="return btnUnFilter_onclick()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="Content">
            <div class="dataTables_scroll">
                <div id="StopSaleGrid" class="dataTables_scrollBody">
                </div>
            </div>
            <div id="StopSaleListGrid">
            </div>
        </div>
        <div class="Footer">
            <tv1:Footer ID="tvfooter" runat="server" />
        </div>
    </div>
    </form>
</body>
</html>
