﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CustomerDetail.aspx.cs" Inherits="CustomerDetail" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "CustomerDetail")%></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="Scripts/jquery.simplemodal.js" type="text/javascript"></script>
    <script src="Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="Scripts/checkDate.js" type="text/javascript"></script>
    <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/confirm.css" rel="stylesheet" type="text/css" />
    <link href="CSS/ResCustDetail.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">

        var InvalidBirthDate = '<%= GetGlobalResourceObject("LibraryResource","InvalidBirthDate") %>';

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function logout() {
            self.parent.logout();
        }

        var _dateMask = '';
        var _dateFormat = '';

        function SetAge(Id, cinDate, _formatDate) {
            try {
                var birthDate = dateValue(_formatDate, $("#iBirthDay").val());
                if (birthDate == null) {
                    $("#iBirthDay").val('');
                    $("#iAge").val('');
                    return;
                }
                var checkIN = dateValue(_formatDate, cinDate);
                if (checkIN == null) return;
                var _minBirthDate = new Date(1, 1, 1910);
                var _birthDate = birthDate;
                var _cinDate = checkIN;
                if (_birthDate < _minBirthDate) {
                    $("#iAge").val('');
                    birthDate = '';
                    showMsg(InvalidBirthDate);
                    return;
                }
                var age = Age(_birthDate, _cinDate);  //parseInt((_cinDate - _birthDate) / (1000 * 24 * 60 * 60 * 365.25))
                $("#iAge").val(age);
            }
            catch (err) {
                $("#iAge").val('');
            }
        }

        function exit(source) {
            var hfCustCount = parseInt($("#hfCustCount").val());
            var custCount = parseInt(hfCustCount);

            var i = '';
            var cust = new Object();
            cust.SeqNo = parseInt($("#iSeqNo" + i).val());
            cust.Title = $("#iTitle" + i).length > 0 ? parseInt($("#iTitle" + i).val()) : null;
            cust.Surname = $("#iSurname" + i).length > 0 ? $("#iSurname" + i).val() : '';
            cust.SurnameL = $("#iSurnameL" + i).length > 0 ? $("#iSurnameL" + i).val() : '';
            cust.Name = $("#iName" + i).length > 0 ? $("#iName" + i).val() : '';
            cust.NameL = $("#iNameL" + i).length > 0 ? $("#iNameL" + i).val() : '';
            cust.BirtDay = $("#iBirthDay" + i).length > 0 ? $("#iBirthDay" + i).val() : '';
            cust.Age = $("#iAge" + i).length > 0 ? $("#iAge" + i).val() : '';
            cust.IDNo = $("#iIDNo" + i).length > 0 ? $("#iIDNo" + i).val() : '';
            cust.PassSerie = $("#iPassSerie" + i).length > 0 ? $("#iPassSerie" + i).val() : '';
            cust.PassNo = $("#iPassNo" + i).length > 0 ? $("#iPassNo" + i).val() : '';
            cust.PassIssueDate = $("#iPassIssueDate" + i).length > 0 ? $("#iPassIssueDate" + i).val() : '';
            cust.PassExpDate = $("#iPassExpDate" + i).length > 0 ? $("#iPassExpDate" + i).val() : '';
            cust.PassGiven = $("#iPassGiven" + i).length > 0 ? $("#iPassGiven" + i).val() : '';
            cust.Phone = $("#iPhone" + i).length > 0 ? $("#iPhone" + i).val() : '';
            cust.Nation = $("#iNation" + i).length > 0 ? parseInt($("#iNation" + i).val()) : null;
            cust.Nationality = $("#iNationality" + i).length > 0 ? $("#iNationality" + i).val() : '';
            cust.Passport = $("#iHasPassport" + i).length > 0 ? $("#iHasPassport" + i)[0].checked : true;
            cust.Leader = $('#iLeader').val().toLowerCase() == 'true';

            //return $.json.encode(custList);
            window.close;
            self.parent.returnCustDetailEdit(cust, source);
        }

        function getPage(CustNo) {
            $.ajax({
                type: "POST",
                url: "CustomerDetail.aspx/getFormData",
                data: '{"_CustNo":' + CustNo + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divDetail").html('');
                    $("#divDetail").html(msg.d);
                    var _dateMask = $("#dateMask").val();
                    $(".formatDate").mask(_dateMask);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function pageLoad() {
            $.query = $.query.load(location.href);
            var CustNo = $.query.get('CustNo');
            $("#hfCustCount").val(CustNo);
            getPage(CustNo)
        }

    </script>

    <style type="text/css">
        </style>
</head>
<body onload="pageLoad();">
    <form id="ResCustDetailForm" runat="server">
        <input id="hfCustCount" type="hidden" />
        <div class="divCustDetail">
            <div id="divDetail">
            </div>
            <br />
            <div class="divBtn">
                <div>
                    <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource","btnSave").ToString() %>'
                        style="width: 150px;" onclick="exit('save');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />&nbsp;&nbsp;
                <input id="btnCancel" type="button" value='<%= GetGlobalResourceObject("LibraryResource","btnCancel").ToString() %>'
                    style="width: 150px;" onclick="exit('');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
