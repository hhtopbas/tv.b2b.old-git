﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Data.SqlTypes;
using System.Data;
using System.Web.Services;
using System.Text;
using TvTools;
using System.IO;

public partial class ResMonitor : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url);

        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string datePatern = new TvBo.Common().getDateFormat(UserData.Ci);

        hfCustomRegID.Value = UserData.CustomRegID;

        ppcBegDate1.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcBegDate1.Format = datePatern.Replace('/', ' ');
        ppcBegDate2.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcBegDate2.Format = datePatern.Replace('/', ' ');
        ppcEndDate1.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcEndDate1.Format = datePatern.Replace('/', ' ');
        ppcEndDate2.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcEndDate2.Format = datePatern.Replace('/', ' ');
        ppcResDate1.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcResDate1.Format = datePatern.Replace('/', ' ');
        ppcResDate2.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcResDate2.Format = datePatern.Replace('/', ' ');
    }

    public static resMonitorFilterRecord createFilter()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        resMonitorFilterRecord filterDef = new resMonitorFilterRecord();
        if (HttpContext.Current.Session["FilterDef"] != null)
            filterDef = (resMonitorFilterRecord)HttpContext.Current.Session["FilterDef"];
        object _endDate = new TvBo.Common().getFormConfigValue("ResMonitor", "FilterDate");        
        string datePatern = new TvBo.Common().getDateFormat(UserData.Ci);
        datePatern = datePatern.Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], '-');
        if (_endDate != null && Conversion.getBoolOrNull(_endDate).Value)
            filterDef.EndDate1 = DateTime.Today.Date.ToUniversalTime();
        filterDef.AgencyUser = UserData.UserID;
        filterDef.DateFormat = datePatern;
        filterDef.ShowAllRes = UserData.ShowAllRes;
        return filterDef;
    }

    [WebMethod(EnableSession = true)]
    public static string CreateFilterData(string clear)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        bool? _clear = Conversion.getBoolOrNull(clear);
        if ((_clear.HasValue ? _clear.Value : true))
            HttpContext.Current.Session["FilterDef"] = null;

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        List<resMonitorDefaultRecord> defaultData = new ReservationMonitor().getResMonDefaultData(UserData, ref errorMsg);
        resMonitorFilterData filterData = new resMonitorFilterData();
        
        filterData.Filter = createFilter();
        filterData.HolPackData = new ReservationMonitor().getHolPacks(useLocalName, defaultData);         
        filterData.AgencyOfficeData = new ReservationMonitor().getAgencyOffices(useLocalName, defaultData);
        filterData.AgencyUserData = new ReservationMonitor().getAgencyUser(useLocalName, defaultData);
        filterData.DepCityData = new ReservationMonitor().getDepCity(useLocalName, defaultData);
        filterData.ArrCityData = new ReservationMonitor().getArrCity(useLocalName, defaultData);
        filterData.CurrencyData = new ReservationMonitor().getCurrency(useLocalName, UserData.Market, defaultData);

        if (filterData.AgencyUserData.Count > 0)
        {
            filterData.ShowAllRes = UserData.ShowAllRes;
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            {
                filterData.ShowAllRes = true;
                filterData.Filter.AgencyUser = string.Empty;
            }
            else filterData.Filter.AgencyUser = UserData.UserID;

            if (!filterData.ShowAllRes && filterData.AgencyUserData.Where(w => w.Code != UserData.UserID).Count() > 0)
                foreach (listString row in filterData.AgencyUserData.Where(w => w.Code != UserData.UserID).Select(s => s).ToList<listString>())
                    filterData.AgencyUserData.Remove(row);
        }
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Orex))
            filterData.ShowDraft = true;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Azur) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Kusadasi_Ro) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Orex))
            filterData.ShowOptionControl = true;

        return Newtonsoft.Json.JsonConvert.SerializeObject(filterData);
    }

    public static string createResultGridDetail(User UserData, resMonitorRecord row, int cnt)
    {
        StringBuilder sb = new StringBuilder();

        bool? _showAgencyComView = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyComView"));
        bool showAgencyComView = !_showAgencyComView.HasValue || (UserData.ShowAllRes || _showAgencyComView.Value);
        bool showAgencyCom = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyCom")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyCom")).Value : true;
        bool showAgencyEB = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyEB")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyEB")).Value : true;
        bool showPassengerEB = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showPassengerEB")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showPassengerEB")).Value : true;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        if (!Users.checkCurrentUserOperator(TvBo.Common.crID_Anex))
        {
            #region for Anex
            if (!_showAgencyComView.HasValue || (UserData.ShowAllRes || _showAgencyComView.Value))
            {
                showAgencyCom = true;
                showAgencyEB = true;
                showPassengerEB = true;
            }
            #endregion
        }
        else
        {
            if (_showAgencyComView.HasValue && _showAgencyComView.Value)
            {
                showAgencyCom = true;
                showAgencyEB = true;
                showPassengerEB = true;
            }
        }

        sb.AppendFormat("<div id=\"rowOtherInfo{0}\" class=\"rowDetail\">", cnt);
        sb.Append("<div style=\"float: left; width: 650px;\">");
        sb.Append("<table width=\"100%\"><tr>");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblCatalogPrice"));
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0}</span></td>", row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") : "&nbsp;");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            (showAgencyCom || !Users.checkCurrentUserOperator(TvBo.Common.crID_Azur)) ? HttpContext.GetGlobalResourceObject("ResMonitor", "lblDueToPay") : "");
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0}</span></td>", (showAgencyCom || !Users.checkCurrentUserOperator(TvBo.Common.crID_Azur)) && row.DueAgentPayment.HasValue ? row.DueAgentPayment.Value.ToString("#,###.00") : "&nbsp;");
        sb.Append("</tr><tr>");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblDiscount"));
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0}</span></td>", row.Discount.HasValue ? row.Discount.Value.ToString("#,###.00") : "&nbsp;");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            (showAgencyCom || !Users.checkCurrentUserOperator(TvBo.Common.crID_Azur)) ? HttpContext.GetGlobalResourceObject("ResMonitor", "lblActuallyPaid") : "");
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0}</span></td>", (showAgencyCom || !Users.checkCurrentUserOperator(TvBo.Common.crID_Azur)) && row.AgencyPayment.HasValue ? row.AgencyPayment.Value.ToString("#,###.00") : "&nbsp;");
        sb.Append("</tr><tr>");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            showAgencyEB ? HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB") : "");
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0}</span></td>", showAgencyEB && row.EBAgency.HasValue ? row.EBAgency.Value.ToString("#,###.00") : "&nbsp;");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            (showAgencyCom || !Users.checkCurrentUserOperator(TvBo.Common.crID_Azur)) ? HttpContext.GetGlobalResourceObject("ResMonitor", "lblBalance") : "");
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\"><b>{0}</b></span></td>", (showAgencyCom || !Users.checkCurrentUserOperator(TvBo.Common.crID_Azur)) && row.Balance.HasValue ? row.Balance.Value.ToString("#,###.00") : "&nbsp;");
        sb.Append("</tr><tr>");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            showPassengerEB ? HttpContext.GetGlobalResourceObject("ResMonitor", "lblPassangerEB") : "");
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0}</span></td>", showPassengerEB && row.EBPas.HasValue ? row.EBPas.Value.ToString("#,###.00") : "&nbsp;");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            (showAgencyCom || !Users.checkCurrentUserOperator(TvBo.Common.crID_Azur)) ? HttpContext.GetGlobalResourceObject("ResMonitor", "lblPaymentDueDates") : "");
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\" onclick=\"showPaymentPlan('{1}', '{2}')\" style=\"cursor: pointer; text-decoration: underline;\">{0}</span></td>",
                (showAgencyCom || !Users.checkCurrentUserOperator(TvBo.Common.crID_Azur)) ? HttpContext.GetGlobalResourceObject("ResMonitor", "lblView") : "",
                row.ResNo,
                UserData.Ci.LCID);
        sb.Append("</tr><tr>");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
                showAgencyCom ? HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgentCommision") : "");
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0} {1}</span></td>",
                showAgencyCom && row.AgencyComPer.HasValue ? "(%" + Math.Abs(row.AgencyComPer.Value).ToString("#.00") + ")" : "&nbsp;",
                showAgencyCom && row.AgencyCom.HasValue ? row.AgencyCom.Value.ToString("#,###.00") : "&nbsp;");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            (showAgencyCom || !Users.checkCurrentUserOperator(TvBo.Common.crID_Azur)) ? HttpContext.GetGlobalResourceObject("ResMonitor", "lblInvoiceToAgency") : "");
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0}</span></td>", (showAgencyCom || !Users.checkCurrentUserOperator(TvBo.Common.crID_Azur)) ? row.InvoiceNo : "");
        sb.Append("</tr><tr>");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            showAgencyCom ? HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyDiscountFromCommision") : "");
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\">{0} {1}</span></td>",
            showAgencyCom && row.AgencyDisPasPer.HasValue ? "(%" + Math.Abs(row.AgencyDisPasPer.Value).ToString("#.00") + ")" : "&nbsp;",
            showAgencyCom && row.AgencyDisPasVal.HasValue ? row.AgencyDisPasVal.Value.ToString("#.00") : "&nbsp;");
        sb.AppendFormat("<td class=\"priceDetailCaption\">{0}</td>",
            (showAgencyCom || !Users.checkCurrentUserOperator(TvBo.Common.crID_Azur)) ? HttpContext.GetGlobalResourceObject("ResMonitor", "lblCommisionInvoiceNumber") : "");
        sb.AppendFormat("<td class=\"priceDetailValue\"><span class=\"right\" onclick=\"showCommissionInvoice('{1}', '{2}');\" style=\"cursor: pointer; text-decoration: underline;\">{0}</span></td>",
            (showAgencyCom || !Users.checkCurrentUserOperator(TvBo.Common.crID_Azur)) ? HttpContext.GetGlobalResourceObject("ResMonitor", "lblShow") : "",
            row.ResNo,
            UserData.Ci.LCID);
        sb.Append("</tr></table>");

        sb.Append("</div>");
        sb.Append("<div style=\"float:left; width: 280px;\">");
        sb.Append("<table width=\"100%\">");
        sb.AppendFormat("<tr><td class=\"leftTD\"><strong>{0}</strong></td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblReservationDate"),
            row.ResDate.HasValue ? row.ResDate.Value.ToShortDateString() : "&nbsp;");
        sb.AppendFormat("<tr><td class=\"leftTD\"><strong>{0}</strong></td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyUser"),
            useLocalName ? row.AgencyUserL : row.AgencyUser);
        sb.AppendFormat("<tr><td class=\"leftTD\"><strong>{0}</strong></td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblRecordChangeDate"),
            row.ChgDate);
        sb.AppendFormat("<tr><td class=\"leftTD\"><strong>{0}</strong></td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblReadByOperator"),
            row.ReadByOpr);
        sb.AppendFormat("<tr><td class=\"leftTD\"><strong>{0}</strong></td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblReadByOperatorUser"),
            useLocalName ? row.ReadByOprUserL : row.ReadByOprUser);
        sb.AppendFormat("<tr><td class=\"leftTD\"><strong>{0}</strong></td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblReadByOperatorDate"),
            row.ReadByOprDate.HasValue ? row.ReadByOprDate.Value.ToShortDateString() : "&nbsp;");
        sb.Append("</table>");
        sb.Append("</div>");
        sb.Append("</div>");

        return sb.ToString();
    }

    public static object getObject(TvBo.resMonitorRecord row, string fieldName)
    {
        System.Reflection.PropertyInfo[] oProps = null;
        oProps = row.GetType().GetProperties();
        List<System.Reflection.PropertyInfo> _pi = (from q in oProps
                                                    where q.Name == fieldName
                                                    select q).ToList<System.Reflection.PropertyInfo>();
        object value = null;
        if (_pi != null && _pi.Count() > 0)
        {
            System.Reflection.PropertyInfo pi = _pi[0];
            value = pi.GetValue(row, null) == null ? DBNull.Value : pi.GetValue(row, null);
        }
        return value;
    }

    public static string createResultGridDetailTemplate(User UserData, resMonitorRecord row, int cnt, string tmpl)
    {
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        int lastPosition = 0;
        if (string.IsNullOrEmpty(tmpl))
            return createResultGridDetail(UserData, row, cnt);
        string retVal = string.Empty;
        string tmpTemplate = tmpl;
        bool exit = true;
        while (exit)
        {
            string valueTemp = string.Empty;
            int first = tmpTemplate.IndexOf("{[");
            if (first > -1)
            {
                lastPosition = first;
                tmpTemplate = tmpTemplate.Remove(0, first + 2);
                int last = tmpTemplate.IndexOf("]}");
                valueTemp = tmpTemplate.Substring(0, last);
                tmpTemplate = tmpTemplate.Remove(0, last + 2);
                string[] local = valueTemp.Trim().Split(',');
                if (local.Length > 1)
                {
                    string classKey = local[0].Trim('\"');
                    string resourceKey = local[1].Trim('\"');
                    string localStr = HttpContext.GetGlobalResourceObject(classKey, resourceKey).ToString();
                    tmpl = tmpl.Replace("{[" + valueTemp + "]}", localStr);
                }
            }
            else exit = false;
        }
        tmpTemplate = tmpl;
        exit = true;
        while (exit)
        {
            string valueTemp = string.Empty;
            int first = tmpTemplate.IndexOf("{");
            if (first > -1)
            {
                lastPosition = first;
                tmpTemplate = tmpTemplate.Remove(0, first + 1);
                int last = tmpTemplate.IndexOf("}");
                valueTemp = tmpTemplate.Substring(0, last);
                valueTemp = valueTemp.Trim('\"');
                tmpTemplate = tmpTemplate.Remove(0, last + 1);

                string valueStr = string.Empty;
                if (valueTemp.IndexOf('.') > -1)
                {
                    if (Equals(valueTemp, "UserData.Ci.LCID"))
                        valueStr = UserData.Ci.LCID.ToString();
                }
                else
                {
                    if (valueTemp.IndexOf('-') > -1)
                    {
                        string[] valuelist = valueTemp.Split('-');
                        List<TvTools.objectList> objectValues = new List<objectList>();
                        Type _type = typeof(System.String);
                        for (int i = 0; i < valuelist.Length; i++)
                        {
                            object value = getObject(row, valuelist[i]);
                            objectValues.Add(new TvTools.objectList
                            {
                                TypeName = value.GetType().Name,
                                Value = value
                            });
                        }
                        object obj = mathLib.returnFormulaMinus(objectValues, ref _type);
                        valueStr = Conversion.getObjectToString(obj, _type);
                    }
                    else
                        if (valueTemp.IndexOf('+') > -1)
                        {
                            string[] valuelist = valueTemp.Split('+');
                            List<TvTools.objectList> objectValues = new List<objectList>();
                            Type _type = typeof(System.String);
                            for (int i = 0; i < valuelist.Length; i++)
                            {
                                object value = getObject(row, valuelist[i]);
                                objectValues.Add(new TvTools.objectList
                                {
                                    TypeName = value.GetType().Name,
                                    Value = value
                                });
                            }
                            object obj = mathLib.returnFormulaPlus(objectValues, ref _type);
                            valueStr = Conversion.getObjectToString(obj, _type);
                        }
                        else
                        {
                            object value = getObject(row, valueTemp);
                            valueStr = Conversion.getObjectToString(value, value.GetType()); //value.ToString();
                        }
                    tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", valueStr);
                }
                tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", valueStr);
            }
            else exit = false;
        }

        tmpl = string.Format("<div id=\"rowOtherInfo{0}\" class=\"rowDetail\">", cnt) +
                "<div style=\"width: 100%;\">" + tmpl;
        tmpl += "</div></div>";
        return tmpl;
    }

    public static string getDetailTemplate(User UserData)
    {
        string sb = string.Empty;
        string filePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + new UICommon().getWebID() + "\\ResMonTemplate.tmpl";
        if (System.IO.File.Exists(filePath))
        {
            System.IO.StreamReader Tex = new System.IO.StreamReader(filePath);
            sb = Tex.ReadToEnd();
            Tex.Close();
        }
        return sb;
    }

    public static string createResultGrid(List<resMonitorRecord> data, int PageNo, int PageDataCount)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (PageDataCount == 0) PageDataCount = 10;
        int cnt = 0;
        int beg = (PageNo - 1) * PageDataCount;
        int end = (PageNo * PageDataCount);
        string detailTemplate = getDetailTemplate(UserData);
        StringBuilder sb = new StringBuilder();
        #region gridPager
        int? recordCount = data.Count;
        if (end >= recordCount) end = recordCount.Value;
        decimal? _pageCount = Convert.ToDecimal(recordCount) / PageDataCount;

        int pageCount = (int)_pageCount != _pageCount ? (int)_pageCount + 1 : (int)_pageCount;
        string pageNrList = "";
        for (int i = 0; i < pageCount; i++)
        {
            if (PageNo == (i + 1)) pageNrList += string.Format("<option value=\"{0}\" selected=\"selected\">{1}</option>", i + 1, i + 1);
            else pageNrList += string.Format("<option value=\"{0}\">{1}</option>", i + 1, i + 1);
        }
        sb.Append("<div class=\"reservationPager ui-state-hover\">");
        sb.AppendFormat("<div style= \"width: 200px; float:left\"><span>{0} :</span><select id=\"pageList\" onchange=\"pageChange();\">{1}</select>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblPage"),
            pageNrList);
        sb.AppendFormat("<input id=\"PageNo\" type=\"hidden\" value=\"{0}\" /></div>", PageNo);

        sb.Append("<div style=\"width:200px; float:left;\">");
        string pageListStr = string.Empty;

        pageListStr += PageDataCount == 10 ? "<OPTION value=\"10\" selected=\"selected\" >10</OPTION>" : "<OPTION value=\"10\">10</OPTION>";
        pageListStr += PageDataCount == 25 ? "<OPTION value=\"25\" selected=\"selected\" >25</OPTION>" : "<OPTION value=\"25\">25</OPTION>";
        pageListStr += PageDataCount == 50 ? "<OPTION value=\"50\" selected=\"selected\" >50</OPTION>" : "<OPTION value=\"50\">50</OPTION>";
        pageListStr += PageDataCount == 100 ? "<OPTION value=\"100\" selected=\"selected\" >100</OPTION>" : "<OPTION value=\"100\">100</OPTION>";

        sb.AppendFormat("{0} <SELECT id=\"pageDataList\" onchange=\"pageDataCountChange();\">{1}</SELECT> {2}",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblShow"),
            pageListStr,
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblentries"));
        sb.Append("</div>");
        sb.AppendFormat("<div style=\"width:200px; float:right; \"><span style=\"bottom: 4px;\">{0} : </span><img alt=\"\" title=\"\" src=\"Images/html_icon.gif\" height=\"22px\" onclick=\"exportHtml();\" /></div>",
                HttpContext.GetGlobalResourceObject("ResMonitor", "lblExportTo"));
        //&nbsp;&nbsp;<img alt=\"\" title=\"\" src=\"Images/txt_icon.gif\" height=\"22px\" onclick=\"exportText();\" />
        //sb.AppendFormat("<div style=\"width:200px; float:left;\">Showing {0} to {1} of {2} entries</div>", beg + 1, end + 1, recordCount);
        sb.Append("</div>");
        #endregion

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        #region gridHeader
        sb.Append("<div class=\"reservationHeaderRow ui-widget-header ui-priority-secondary\">");
        sb.AppendFormat("<div class=\"resNo\">{0}{1}</div>",
            HttpContext.GetGlobalResourceObject("ResMonitor", "lblReservationnumber"),
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Azur) ? "<br />" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblOptionTime") : "");
        sb.AppendFormat("<div class=\"info1\">{0}<br />&nbsp;</div>", "#");
        sb.AppendFormat("<div class=\"leaderName\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblLeaderName"));
        sb.AppendFormat("<div class=\"hotelName\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblHotel"));
        sb.AppendFormat("<div class=\"adult\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblAdult"));
        sb.AppendFormat("<div class=\"child\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblChild"));
        sb.AppendFormat("<div class=\"beginDate\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblBeginDate"));
        sb.AppendFormat("<div class=\"days\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblNight"));
        sb.AppendFormat("<div class=\"hotstatus\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblHotStatus"));
        sb.AppendFormat("<div class=\"status\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblStatus"));
        sb.AppendFormat("<div class=\"sPrice\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblSalePrice"));
        sb.AppendFormat("<div class=\"saleCur\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblCurr"));
        sb.AppendFormat("<div class=\"depCity\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblDepartureCity"));
        sb.AppendFormat("<div class=\"arrCity\">{0}</div>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblArrivalCity"));
        sb.Append("</div>");
        #endregion

        for (int j = beg; j < end; j++)
        {
            resMonitorRecord row = data[j];
            bool unRead = string.Equals(row.ReadByOpr, "&nbsp;");
            cnt++;
            #region gridDataRow
            if (row.OptDate.HasValue)
                sb.Append("<div class=\"reservationRow optionRow\">");
            else if (Convert.ToInt32(cnt / 2.0) == (cnt / 2.0))
                sb.Append("<div class=\"reservationRow Odd\">");
            else sb.Append("<div class=\"reservationRow Even\">");
            sb.Append("<div class=\"resNo bold\">");
            sb.AppendFormat("<img id=\"openDetail{0}\" alt=\"{1}\" src=\"Images/infoOpen.gif\" onClick=\"openRow('{0}');\">&nbsp;<span class=\"span\" onclick=\"showResView('{2}');\">{3}</span>",
                cnt,
                HttpContext.GetGlobalResourceObject("ResMonitor", "lblOpenDetail"),
                row.ResNo,
                row.ResNo);
            sb.AppendFormat("<input type=\"hidden\" id=\"ocRowId{0}\" value=\"0\" />", cnt);
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Azur) ||
                string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) ||
                string.Equals(UserData.CustomRegID, TvBo.Common.crID_Kusadasi_Ro) ||
                string.Equals(UserData.CustomRegID, TvBo.Common.crID_Safiran) ||
                string.Equals(UserData.CustomRegID, TvBo.Common.crID_Qualitum) ||
                string.Equals(UserData.CustomRegID, TvBo.Common.crID_Orex))
            {
                if (row.OptDate.HasValue)
                    sb.AppendFormat("<div style=\"width:100%;font-weight:normal; font-size:7pt;text-align:center;\">{0}</div>",
                        row.OptDate.Value.ToShortDateString() + " " + row.OptDate.Value.ToString("HH:mm"));
            }
            sb.Append("</div>");
            string infoStr = string.Empty;
            if (row.NewComment)
                infoStr += string.Format("<img alt=\"{0}\" src=\"Images/mail_16.gif\" onclick=\"showResComment('{1}','{2}',{3})\" />",
                        HttpContext.GetGlobalResourceObject("ResMonitor", "lblNewComment"),
                        row.ResNo,
                        UserData.UserID,
                        UserData.Ci.LCID);
            if (!string.IsNullOrEmpty(row.ResNote))
            {
                if (infoStr.Length > 0) infoStr += "&nbsp;";
                infoStr += string.Format("<img alt=\"{0}\" src=\"Images/edit_16.gif\" onclick=\"showResNote({1})\" />",
                    HttpContext.GetGlobalResourceObject("ResMonitor", "lblShowReservationNote"),
                    UICommon.EncodeJsString(row.ResNote.Replace('\"', '!')));
            }
            sb.AppendFormat("<div id=\"img_{1}\" class=\"info1\">{0}</div>",
                    string.IsNullOrEmpty(infoStr) ? "&nbsp;" : infoStr, row.ResNo);
            sb.AppendFormat("<div class=\"leaderName {1}\">{0}</div>",
                    !string.IsNullOrEmpty(row.LeaderName) ? row.LeaderName : "&nbsp;",
                    unRead ? "bold" : "");
            sb.AppendFormat("<div class=\"hotelName {1}\">{0}</div>",
                !string.IsNullOrEmpty(row.HotelName) ? (useLocalName ? row.HotelNameL : row.HotelName) : "&nbsp;",
                    unRead ? "bold" : "");
            sb.AppendFormat("<div class=\"adult {1}\">{0}</div>",
                    row.Adult,
                    unRead ? "bold" : "");
            sb.AppendFormat("<div class=\"child {1}\">{0}</div>",
                    row.Child,
                    unRead ? "bold" : "");
            sb.AppendFormat("<div class=\"beginDate {1}\">{0}</div>",
                    row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "&nbsp;",
                    unRead ? "bold" : "");
            sb.AppendFormat("<div class=\"days {1}\">{0}</div>",
                    row.Days,
                    unRead ? "bold" : "");
            string hotstatusStr = string.Format("<img src=\"Images/Status/ConfStatus{0}.gif\" width=\"16\" height=\"16\" alt=\"{1}\">",
                                row.HotConfStat.ToString(),
                                HttpContext.GetGlobalResourceObject("ResMonitor", "lblHotStatus").ToString() + ", " +
                                    (row.HotConfStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + row.HotConfStat.ToString()).ToString() : "")
                                );
            if (!row.HotConfStat.HasValue)
                hotstatusStr = "&nbsp;";

            sb.AppendFormat("<div class=\"hotstatus\">{0}</div>",
                    hotstatusStr);
            string statusStr = string.Format("<img src=\"Images/Status/ResStatus{0}.gif\" width=\"16\" height=\"16\" alt=\"{1}\"><img src=\"Images/Status/ConfStatus{2}.gif\" width=\"16\" height=\"16\" alt=\"{3}\"><img src=\"Images/Status/PayStatus{4}.gif\" width=\"16\" height=\"16\" alt=\"{5}\">",
                    row.ResStat.ToString(),
                    HttpContext.GetGlobalResourceObject("ResMonitor", "lblResStatus").ToString() + ", " +
                        (row.ResStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + row.ResStat.ToString()).ToString() : ""),
                    row.ConfStat.ToString(),
                    HttpContext.GetGlobalResourceObject("ResMonitor", "lblConfStatus").ToString() + ", " +
                        (row.ConfStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + row.ConfStat.ToString()).ToString() : ""),
                    row.PaymentStat.ToString(),
                    HttpContext.GetGlobalResourceObject("ResMonitor", "lblPaymentStatus").ToString() + ", " +
                        (!string.IsNullOrEmpty(row.PaymentStat) ? HttpContext.GetGlobalResourceObject("LibraryResource", "PayStatus" + row.PaymentStat).ToString() : ""));
            sb.AppendFormat("<div class=\"status\">{0}</div>",
                    statusStr);
            sb.AppendFormat("<div class=\"sPrice\"><b>{0}</b></div>",
                    row.SPrice.HasValue ? row.SPrice.Value.ToString("#,###.00") : "&nbsp;");
            sb.AppendFormat("<div class=\"saleCur {1}\">{0}</div>",
                    !string.IsNullOrEmpty(row.SaleCur) ? row.SaleCur : "&nbsp;",
                    unRead ? "bold" : "");
            sb.AppendFormat("<div class=\"depCity {1}\">{0}</div>",
                !string.IsNullOrEmpty(row.DepCityName) ? (useLocalName ? row.DepCityNameL : row.DepCityName) : "&nbsp;",
                    unRead ? "bold" : "");
            sb.AppendFormat("<div class=\"arrCity {1}\">{0}</div>",
                !string.IsNullOrEmpty(row.ArrCityName) ? (useLocalName ? row.ArrCityNameL : row.ArrCityName) : "&nbsp;",
                    unRead ? "bold" : "");

            #region gridDataRow Detail
            sb.Append("<div style=\"clear:both; border:none; font-weight: normal;\">");

            #region gridDataRow Detail

            sb.Append(createResultGridDetailTemplate(UserData, row, cnt, detailTemplate));

            #endregion

            sb.Append("</div>");
            #endregion gridDataRow Detail

            sb.Append("</div>");
            #endregion gridDataRow
        }

        #region Total Row
        sb.Append("<div class=\"reservationRow\">");
        sb.AppendFormat("<div class=\"resNo\"><b>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblTotal").ToString() + ": {0}</b></div>", data.Count);
        sb.Append("<div class=\"info1\">&nbsp;</div>");
        sb.Append("<div class=\"leaderName\">&nbsp;</div>");
        sb.Append("<div class=\"hotelName\">&nbsp;</div>");
        sb.AppendFormat("<div class=\"adult\"><b>{0}</b></div>", data.Sum(s => s.Adult).ToString());
        sb.AppendFormat("<div class=\"child\"><b>{0}</b></div>", data.Sum(s => s.Child).ToString());
        sb.Append("<div class=\"beginDate\">&nbsp;</div>");
        sb.Append("<div class=\"days\">&nbsp;</div>");
        sb.Append("<div class=\"status\">&nbsp;</div>");
        //var TotalPrice = from q in data
        //                 group q by new { q.SaleCur } into k
        //                 select new { Currency = k.Key.SaleCur, SPrice = data.Sum(s => s.SPrice.HasValue ? s.SalePrice.Value : 0) };
        var TotalPrice = from q in data
                         group q by new { q.SaleCur } into k
                         select new { Currency = k.Key.SaleCur, SPrice = data.Where(w => w.SaleCur == k.Key.SaleCur).Sum(s => s.SPrice.HasValue ? s.SPrice.Value : 0) };
        string sumPrice = string.Empty;
        string sumPriceCurr = string.Empty;
        foreach (var r in TotalPrice)
        {
            if (sumPrice.Length > 0) sumPrice += "<br />";
            if (sumPriceCurr.Length > 0) sumPriceCurr += "<br />";
            sumPrice += string.Format("{0}", r.SPrice.ToString("#,###.00"));
            sumPriceCurr += string.Format("{0}", r.Currency);
        }
        sb.AppendFormat("<div class=\"sPrice\"><b>{0}</b></div>", sumPrice);
        sb.AppendFormat("<div class=\"saleCur\">{0}</div>", sumPriceCurr);
        sb.Append("<div class=\"depCity\">&nbsp;</div>");
        sb.Append("<div class=\"arrCity\">&nbsp;</div>");
        sb.Append("</div>");
        #endregion

        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string showResult(string PageNr, string PageCount, string First)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        List<resMonitorRecord> dataResMain = new List<resMonitorRecord>();
        int? pageNo = Conversion.getInt32OrNull(PageNr);
        int? pageCount = Conversion.getInt32OrNull(PageCount);
        bool? _firstSearch = Conversion.getBoolOrNull(First.ToLower());
        if (_firstSearch.HasValue && _firstSearch.Value)
        {
            if (HttpContext.Current.Session["ResMonData"] != null)
            {
                dataResMain = (List<resMonitorRecord>)HttpContext.Current.Session["ResMonData"];
                return createResultGrid(dataResMain, pageNo.HasValue ? pageNo.Value : 1, pageCount.HasValue ? pageCount.Value : 10);
            }
        }
        string errorMsg = string.Empty;
        resMonitorFilterRecord filter = (resMonitorFilterRecord)HttpContext.Current.Session["FilterDef"];

        string AgencyUser = UserData.UserID;
        if (UserData.ShowAllRes)
            AgencyUser = filter.AgencyUser;
        else AgencyUser = UserData.UserID;

        string OptionControl = string.Empty;
        //if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr))
        OptionControl = (filter.ShowOptionRes.HasValue && filter.ShowOptionRes.Value) ? "1" : "0";
        //else OptionControl = "0";

        try
        {
            List<resMonitorRecord> _dataResMain = new ReservationMonitor().getResMonitorData(UserData, AgencyUser, filter.AgencyOffice,
                        filter.ResNo1, filter.ResNo2, Conversion.getInt32OrNull(filter.DepCity), Conversion.getInt32OrNull(filter.ArrCity),
                        filter.BegDate1, filter.BegDate2, filter.EndDate1, filter.EndDate2, filter.ResDate1, filter.ResDate2,
                        filter.HolPack, filter.LeaderNameS, filter.LeaderNameF, filter.ResStatus, filter.ConfStatus, filter.PayStatus,
                        OptionControl, (filter.ShowDraft.HasValue && filter.ShowDraft.Value), filter.Currency, ref errorMsg);
            dataResMain = (from q in _dataResMain
                           orderby q.NewComment descending, q.ResDate descending, q.RefNo descending
                           select q).ToList<resMonitorRecord>();
            if (_dataResMain.Count > 0)
            {
                if (HttpContext.Current.Session["ResMonData"] != null) HttpContext.Current.Session["ResMonData"] = null;
                HttpContext.Current.Session["ResMonData"] = dataResMain;
            }
            else
            {
                if (HttpContext.Current.Session["ResMonData"] != null) HttpContext.Current.Session["ResMonData"] = null;
            }
        }
        catch
        {
            if (HttpContext.Current.Session["ResMonData"] != null) HttpContext.Current.Session["ResMonData"] = null;
            dataResMain = null;
        }

        if (dataResMain.Count > 0)
            return createResultGrid(dataResMain, pageNo.HasValue ? pageNo.Value : 1, pageCount.HasValue ? pageCount.Value : 10);
        else return HttpContext.GetGlobalResourceObject("LibraryResource", "sEmptyTable").ToString();
    }

    public static resMonitorFilterRecord convertFilterRecord(resMonitorFilterJSonRecord _data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        resMonitorFilterRecord retVal = new resMonitorFilterRecord();
        retVal.AgencyOffice = _data.AgencyOffice;
        retVal.AgencyUser = _data.AgencyUser;
        retVal.ArrCity = _data.ArrCity;
        retVal.BegDate1 = Conversion.getDateTimeOrNull(_data.BegDate1);
        retVal.BegDate2 = Conversion.getDateTimeOrNull(_data.BegDate2);
        retVal.ConfStatus = _data.ConfStatus;
        retVal.Currency = _data.Currency;
        retVal.DateFormat = _data.DateFormat;
        retVal.DepCity = _data.DepCity;
        retVal.EndDate1 = Conversion.getDateTimeOrNull(_data.EndDate1);
        retVal.EndDate2 = Conversion.getDateTimeOrNull(_data.EndDate2);
        retVal.HolPack = _data.HolPack;
        retVal.LeaderNameF = _data.LeaderNameF;
        retVal.LeaderNameS = _data.LeaderNameS;
        retVal.PageNo = Conversion.getInt32OrNull(_data.PageNo);
        retVal.PayDate1 = Conversion.getDateTimeOrNull(_data.PayDate1);
        retVal.PayDate2 = Conversion.getDateTimeOrNull(_data.PayDate2);
        retVal.PayStatus = _data.PayStatus;
        retVal.ResDate1 = Conversion.getDateTimeOrNull(_data.ResDate1);
        retVal.ResDate2 = Conversion.getDateTimeOrNull(_data.ResDate2);
        retVal.ResNo1 = _data.ResNo1;
        retVal.ResNo2 = _data.ResNo2;
        retVal.ResStatus = _data.ResStatus;
        retVal.ShowDraft = Equals(_data.ShowDraft, "1") ? true : false;
        retVal.ShowOptionRes = Equals(_data.ShowOptionRes, "0") ? false : true;
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string SaveFilter(string data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["FilterDef"] == null)
        {
            resMonitorFilterRecord filterDef = createFilter();
            HttpContext.Current.Session["FilterDef"] = filterDef;
        }

        resMonitorFilterRecord dt = (resMonitorFilterRecord)HttpContext.Current.Session["FilterDef"];
        string _dataJson = data.Replace('<', '{').Replace('>', '}').Replace('|', '"');
        resMonitorFilterJSonRecord _data = Newtonsoft.Json.JsonConvert.DeserializeObject<resMonitorFilterJSonRecord>(_dataJson);
        dt = convertFilterRecord(_data);
        HttpContext.Current.Session["FilterDef"] = dt;
        return "OK";
    }

    [WebMethod(EnableSession = true)]
    public static string setReadCommentData(string ResNo, string UserID, int? RecID, int? lcID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string retVal = string.Empty;
        retVal = new TvBo.ReservationMonitor().setReadComment(UserID, ResNo, RecID.HasValue ? RecID.Value : -1, lcID.HasValue ? lcID.Value : -1);
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string getCommentData(string ResNo, string UserID, int? lcID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        return new TvBo.ReservationMonitor().getCommentDataString(ResNo, UserID, lcID.HasValue ? lcID.Value : -1);
    }

    [WebMethod(EnableSession = true)]
    public static string setCommentData(string ResNo, string Comment, string UserID, int? lcID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string retVal = string.Empty;
        retVal = new TvBo.ReservationMonitor().setComment(UserData, ResNo, Comment, lcID.HasValue ? lcID.Value : -1);
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string getCommisionInvoices(string ResNo, int? lcID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();
        List<CommisionInvoiceNumberRecord> list = new ReservationMonitor().getCommisionInvoiceNumber(ResNo, ref errorMsg);
        if (list.Count > 0)
        {
            sb.Append("<br />");
            sb.Append("<table style=\"font-family: Tahoma; font-size: small\">");
            sb.Append(" <tr>");
            sb.Append("     <td align=\"center\" style=\"font-weight:bold\">" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblInvSerie").ToString() + "</td>");
            sb.Append("     <td align=\"center\" style=\"font-weight:bold\">" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblInvNo").ToString() + "</td>");
            sb.Append(" </tr>");
            foreach (CommisionInvoiceNumberRecord row in list)
            {
                sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>",
                    row.InvSerial,
                    row.InvNo);
            }
            sb.Append("</table>");
        }
        return list.Count > 0 ? sb.ToString() : HttpContext.GetGlobalResourceObject("ResMonitor", "msgNoCommisionInvoice").ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getTotalBonus()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        UserBonusTotalRecord agencyBonus = new Bonus().getBonus(DateTime.Today, UserData.SaleCur, "A", UserData.AgencyID, ref errorMsg);
        UserBonusTotalRecord userBonus = new Bonus().getBonus(DateTime.Today, UserData.SaleCur, "U", UserData.PIN, ref errorMsg);
        StringBuilder sb = new StringBuilder();

        if (agencyBonus == null && userBonus == null) return "";
        if (!UserData.Bonus.BonusUserSeeOwnW && !UserData.Bonus.BonusAgencySeeOwnW) return "";

        sb.Append("<table>");
        sb.Append(" <tr>");
        sb.AppendFormat(" <td>{0}</td>", "&nbsp;");
        if (UserData.Bonus.BonusAgencySeeOwnW)
            sb.AppendFormat(" <td>{0} (<span class=\"link\" onclick=\"showBonusList('agency');\">{1}</span>)</td>", HttpContext.GetGlobalResourceObject("LibraryResource", "bonuslblAgency"),
                                                  HttpContext.GetGlobalResourceObject("Bonus", "BLBonusList"));
        sb.AppendFormat(" <td>{0} (<span class=\"link\" onclick=\"showBonusList('user');\">{1}</span>)</td>", HttpContext.GetGlobalResourceObject("LibraryResource", "bonuslblUser"),
                                              HttpContext.GetGlobalResourceObject("Bonus", "BLBonusList"));
        sb.Append(" </tr>");

        sb.Append(" <tr>");
        sb.AppendFormat(" <td class=\"header\">{0}</td>", HttpContext.GetGlobalResourceObject("LibraryResource", "bonuslblTotalbonus"));
        if (UserData.Bonus.BonusAgencySeeOwnW && UserData.Bonus.BonusUserSeeAgencyW)
            sb.AppendFormat(" <td class=\"agency\">{0}</td>", agencyBonus.TotBonus.HasValue ? agencyBonus.TotBonus.Value.ToString() : "&nbsp;");
        if (UserData.Bonus.BonusUserSeeOwnW)
            sb.AppendFormat(" <td class=\"user\">{0}</td>", userBonus.TotBonus.HasValue ? userBonus.TotBonus.Value.ToString() : "&nbsp;");
        sb.Append(" </tr>");
        sb.Append(" <tr>");
        sb.AppendFormat(" <td class=\"header\">{0}</td>", HttpContext.GetGlobalResourceObject("LibraryResource", "bonuslblTotalUsedBonus"));
        if (UserData.Bonus.BonusUserSeeOwnW && UserData.Bonus.BonusUserSeeAgencyW)
            sb.AppendFormat(" <td class=\"agency\">{0}</td>", agencyBonus.UsedBonus.HasValue ? agencyBonus.UsedBonus.Value.ToString() : "&nbsp;");
        if (UserData.Bonus.BonusUserSeeOwnW)
            sb.AppendFormat(" <td class=\"user\">{0}</td>", userBonus.UsedBonus.HasValue ? userBonus.UsedBonus.Value.ToString() : "&nbsp;");
        sb.Append(" </tr>");
        sb.Append(" <tr>");
        sb.AppendFormat(" <td class=\"header\">{0}</td>", HttpContext.GetGlobalResourceObject("LibraryResource", "bonuslblTotalUseableBonus"));
        if (UserData.Bonus.BonusAgencySeeOwnW && UserData.Bonus.BonusUserSeeAgencyW)
            sb.AppendFormat(" <td class=\"agency\">{0}</td>", agencyBonus.UseableBonus.HasValue ? agencyBonus.UseableBonus.Value.ToString() : "&nbsp;");
        if (UserData.Bonus.BonusUserSeeOwnW)
            sb.AppendFormat(" <td class=\"user\">{0}</td>", userBonus.UseableBonus.HasValue ? userBonus.UseableBonus.Value.ToString() : "&nbsp;");
        sb.Append(" </tr>");
        sb.Append("</table>");
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getBonusList(string listType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        string _html = new Bonus().getBonusHtml(UserData, Equals(listType, "user") ? "U" : "A", Equals(listType, "user") ? UserData.PIN : UserData.AgencyID);
        return _html;
    }

    [WebMethod(EnableSession = true)]
    public static string exportExcel()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["ResMonData"] == null)
        {
            return string.Empty;
        }
        List<resMonitorRecord> dataResMain = (List<resMonitorRecord>)HttpContext.Current.Session["ResMonData"];
        StringBuilder sb = new StringBuilder();
        string strRow = string.Empty;
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblReservationnumber") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblLeaderName") + ";";
        strRow += "Package name" + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblHotel") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblAdult") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblChild") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblBeginDate") + ";";
        //strRow += (row.EndDate.HasValue ? row.EndDate.Value.ToShortDateString() : "") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblNight") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblStatus") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblConfStatus") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblCurr") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblCatalogPrice") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblDiscount") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB") + " %" + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblPassangerEB") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblSalePrice") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgentCommision") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgentCommision") + " %" + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyDiscountFromCommision") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyDiscountFromCommision") + " %" + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblDueToPay") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblActuallyPaid") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblBalance") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblPaymentStatus") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblInvoiceToAgency") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblRecordChangeDate") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyUser") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblDepartureCity") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblArrivalCity") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblReservationDate") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblReadByOperatorUser") + ";";
        strRow += HttpContext.GetGlobalResourceObject("ResMonitor", "lblReadByOperatorDate");
        sb.Append(strRow + "\n");
        foreach (resMonitorRecord row in dataResMain)
        {
            strRow = string.Empty;
            strRow += row.ResNo + ";";
            //strRow += row.ResNote.Replace(";", " ") + ";";
            strRow += row.LeaderName.Replace(";", " ") + ";";
            strRow += row.HolPackName.Replace(";", " ") + ";";
            strRow += row.HotelName.Replace(";", " ") + ";";
            strRow += row.Adult.ToString() + ";";
            strRow += row.Child.ToString() + ";";
            strRow += (row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "") + ";";
            //strRow += (row.EndDate.HasValue ? row.EndDate.Value.ToShortDateString() : "") + ";";
            strRow += row.Days.ToString() + ";";
            strRow += (row.ResStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + row.ResStat.ToString()).ToString() : "") + ";";
            strRow += (row.ConfStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + row.ConfStat.ToString()).ToString() : "") + ";";
            strRow += row.SaleCur + ";";
            strRow += (row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.Discount.HasValue ? row.Discount.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.EBAgency.HasValue ? row.EBAgency.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.EBAgencyPer.HasValue ? row.EBAgencyPer.Value.ToString("###.00") : "") + ";";
            strRow += (row.EBPas.HasValue ? row.EBPas.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.SPrice.HasValue ? row.SPrice.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.AgencyCom.HasValue ? row.AgencyCom.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.AgencyComPer.HasValue ? row.AgencyComPer.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.AgencyDisPasVal.HasValue ? row.AgencyDisPasVal.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.AgencyDisPasPer.HasValue ? row.AgencyDisPasPer.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.DueAgentPayment.HasValue ? row.DueAgentPayment.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.AgencyPayment.HasValue ? row.AgencyPayment.Value.ToString("#,###.00") : "") + ";";
            strRow += (row.Balance.HasValue ? row.Balance.Value.ToString("#,###.00") : "") + ";";
            strRow += (!string.IsNullOrEmpty(row.PaymentStat) ? HttpContext.GetGlobalResourceObject("LibraryResource", "PayStatus" + row.PaymentStat).ToString() : "") + ";";
            strRow += row.InvoiceNo + ";";
            strRow += (row.ChgDate.HasValue ? row.ChgDate.Value.ToShortDateString() : "") + ";";
            strRow += row.AgencyUser + ";";
            strRow += row.DepCityName + ";";
            strRow += row.ArrCityName + ";";
            strRow += (row.ResDate.HasValue ? row.ResDate.Value.ToShortDateString() : "") + ";";
            strRow += row.ReadByOprUser + ";";
            strRow += row.ReadByOprDate.HasValue ? row.ReadByOprDate.Value.ToShortDateString() : "";
            sb.Append(strRow + "\n");
        }
        string filePath = AppDomain.CurrentDomain.BaseDirectory + "Cache\\" + UserData.AgencyID + "_" + UserData.UserID + ".csv";
        try
        {
            if (File.Exists(filePath))
                File.Delete(filePath);
            using (StreamWriter outfile = new StreamWriter(filePath))
            {
                outfile.Write(sb.ToString());
            }
            return WebRoot.BasePageRoot + "Cache/" + UserData.AgencyID + "_" + UserData.UserID + ".csv";
        }
        catch (Exception Ex)
        {
            errorMsg = Ex.Message;
            return "";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string exportHtml()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResMonData"] == null)
        {
            return string.Empty;
        }
        List<resMonitorRecord> dataResMain = (List<resMonitorRecord>)HttpContext.Current.Session["ResMonData"];
        StringBuilder sb = new StringBuilder();
        sb.Append("<table width=\"100%\" cellpadding=\"5\" cellspacing=\"5\">");
        sb.Append("<tr>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblReservationnumber") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblLeaderName") + "</th>");
        sb.Append("<th>" + "Package name" + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblHotel") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAdult") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblChild") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblBeginDate") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblNight") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblStatus") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblConfStatus") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblCurr") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblCatalogPrice") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblDiscount") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB") + " %" + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblPassangerEB") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblSalePrice") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgentCommision") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgentCommision") + " %" + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyDiscountFromCommision") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyDiscountFromCommision") + " %" + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblDueToPay") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblActuallyPaid") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblBalance") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblPaymentStatus") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblInvoiceToAgency") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblRecordChangeDate") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyUser") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblDepartureCity") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblArrivalCity") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblReservationDate") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblReadByOperatorUser") + "</th>");
        sb.Append("<th>" + HttpContext.GetGlobalResourceObject("ResMonitor", "lblReadByOperatorDate") + "</th>");
        sb.Append("</tr>");
        foreach (resMonitorRecord row in dataResMain)
        {
            sb.Append("<tr>");
            sb.Append("<td>" + row.ResNo + "</td>");
            sb.Append("<td>" + row.LeaderName.Replace(";", " ") + "</td>");
            sb.Append("<td>" + row.HolPackName.Replace(";", " ") + "</td>");
            sb.Append("<td>" + row.HotelName.Replace(";", " ") + "</td>");
            sb.Append("<td class=\"alignCenter\">" + row.Adult.ToString() + "</td>");
            sb.Append("<td class=\"alignCenter\">" + row.Child.ToString() + "</td>");
            sb.Append("<td>" + (row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "") + "</td>");
            sb.Append("<td class=\"alignCenter\">" + row.Days.ToString() + "</td>");
            sb.Append("<td class=\"alignCenter\">" + (row.ResStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + row.ResStat.ToString()).ToString() : "") + "</td>");
            sb.Append("<td class=\"alignCenter\">" + (row.ConfStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + row.ConfStat.ToString()).ToString() : "") + "</td>");
            sb.Append("<td>" + row.SaleCur + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.SalePrice.HasValue && row.SalePrice.Value > 0 ? row.SalePrice.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.Discount.HasValue && row.Discount.Value != 0 ? row.Discount.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.EBAgency.HasValue && row.EBAgency.Value != 0 ? row.EBAgency.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.EBAgencyPer.HasValue && row.EBAgencyPer.Value != 0 ? row.EBAgencyPer.Value.ToString("###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.EBPas.HasValue && row.EBPas.Value != 0 ? row.EBPas.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.SPrice.HasValue && row.SalePrice.Value != 0 ? row.SPrice.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.AgencyCom.HasValue && row.AgencyCom.Value != 0 ? row.AgencyCom.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.AgencyComPer.HasValue && row.AgencyComPer.Value != 0 ? row.AgencyComPer.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.AgencyDisPasVal.HasValue && row.AgencyDisPasVal.Value != 0 ? row.AgencyDisPasVal.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.AgencyDisPasPer.HasValue && row.AgencyDisPasPer.Value != 0 ? row.AgencyDisPasPer.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.DueAgentPayment.HasValue && row.DueAgentPayment.Value != 0 ? row.DueAgentPayment.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.AgencyPayment.HasValue && row.AgencyPayment.Value != 0 ? row.AgencyPayment.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignRight\">" + (row.Balance.HasValue && row.Balance.Value != 0 ? row.Balance.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("<td class=\"alignCenter\">" + (!string.IsNullOrEmpty(row.PaymentStat) ? HttpContext.GetGlobalResourceObject("LibraryResource", "PayStatus" + row.PaymentStat).ToString() : "") + "</td>");
            sb.Append("<td>" + row.InvoiceNo + "</td>");
            sb.Append("<td>" + (row.ChgDate.HasValue ? row.ChgDate.Value.ToShortDateString() : "") + "</td>");
            sb.Append("<td>" + row.AgencyUser + "</td>");
            sb.Append("<td>" + row.DepCityName + "</td>");
            sb.Append("<td>" + row.ArrCityName + "</td>");
            sb.Append("<td>" + (row.ResDate.HasValue ? row.ResDate.Value.ToShortDateString() : "") + "</td>");
            sb.Append("<td>" + row.ReadByOprUser + "</td>");
            sb.Append("<td>" + (row.ReadByOprDate.HasValue ? row.ReadByOprDate.Value.ToShortDateString() : "") + "</td>");
            sb.Append("</tr>");
        }
        sb.Append("</table>");
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string reservationIsBusy(string ResNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        bool? _resIsBusy = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "reservationIsBusyControl"));
        if (!_resIsBusy.HasValue || (_resIsBusy.HasValue && _resIsBusy.Value))
        {
            bool resIsBusy = new Reservation().reservationIsBusy(ResNo);
            if (resIsBusy)
                return HttpContext.GetGlobalResourceObject("LibraryResource", "msgReservationIsBusy").ToString();
            else return string.Empty;
        }
        else return string.Empty;
    }
}


