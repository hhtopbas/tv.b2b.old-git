﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CustomerVisaInfo.aspx.cs"
    Inherits="CustomerVisaInfo" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "CustomerVisaInfo")%></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>

    <script src="Scripts/checkDate.js" type="text/javascript"></script>

    <script src="Scripts/linq.js" type="text/javascript"></script>

    <script src="Scripts/jquery.linq.js" type="text/javascript"></script>

    <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/confirm.css" rel="stylesheet" type="text/css" />
    <link href="CSS/ResCustVisa.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .ui-datepicker { font-family: Garamond; font-size: 11px; margin-left: 10px; }
    </style>

    <script language="javascript" type="text/javascript">
        var datePickerLang = 'en';

        if (typeof window.event != 'undefined')
            document.onkeydown = function() {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function(e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function logout() {
            self.parent.logout();
        }

        function showAlert(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                            return true;
                        }
                    }
                });
            });
        }

        function exit(source) {
            obj = new Object();
            if (source == 'save') {
                obj.CustNo = $("#hfCustNo").val();
                obj.Title = $('#edtTitle').val();
                obj.Surname = $('#edtSurname').val();
                obj.Name = $('#edtName').val();
                obj.Birtday = $("#edtBirtday").datepicker("getDate") != null ? "\/Date(" + ($("#edtBirtday").datepicker("getDate").getTime() - $("#edtBirtday").datepicker("getDate").getTimezoneOffset()) + ")\/" : null;
                obj.IDNo = $('#editIDNo').val();
                obj.PassSerie = $('#edtPassSerie').val();
                obj.PassNo = $('#edtPassNo').val();
                obj.PassIssueDate = $("#edtPassIssueDate").datepicker("getDate") != null ? "\/Date(" + ($("#edtPassIssueDate").datepicker("getDate").getTime() - $("#edtPassIssueDate").datepicker("getDate").getTimezoneOffset()) + ")\/" : null;
                obj.PassExpDate = $("#edtPassExpDate").datepicker("getDate") != null ? "\/Date(" + ($("#edtPassExpDate").datepicker("getDate").getTime() - $("#edtPassExpDate").datepicker("getDate").getTimezoneOffset()) + ")\/" : null;
                obj.PassGiven = $('#edtPassGiven').val();
                obj.BirthSurname = $('#edtBirthSurname').val();
                obj.PlaceOfBirth = $('#edtPlaceOfBirth').val();
                obj.CountryOfBirth = $('#edtCountryOfBirth').val();
                obj.RemarkForChild = $('#edtRemarkForChild').val();
                obj.GivenBy = $('#edtGivenBy').val();
                obj.Job = $('#edtJob').val();
                obj.Employer = $('#edtEmployer').val();
                obj.EmpAddress = $('#edtEmpAddress').val();
                obj.EmpTel = $('#edtEmpTel').val();
                obj.InvitePerson = $('#edtInvitePerson').val();
                obj.Sex = $('#edtSex').val();
                obj.Marital = $('#edtMarital').val();
                obj.MaritalRemark = $('#edtMaritalRemark').val();
                obj.VisaCount = $('#edtVisaCount').val();
                obj.ValidFrom = convertJsonDateToDate($("#edtValidFrom").datepicker("getDate"));
                obj.ValidTill = convertJsonDateToDate($("#edtValidTill").datepicker("getDate"));
                obj.ValidFrom2 = convertJsonDateToDate($("#edtValidFrom1").datepicker("getDate"));
                obj.ValidTill2 = convertJsonDateToDate($("#edtValidTill1").datepicker("getDate"));
                obj.ValidFrom3 = convertJsonDateToDate($("#edtValidFrom2").datepicker("getDate"));
                obj.ValidTill3 = convertJsonDateToDate($("#edtValidTill2").datepicker("getDate"));
                obj.ValidFrom4 = convertJsonDateToDate($("#edtValidFrom3").datepicker("getDate"));
                obj.ValidTill4 = convertJsonDateToDate($("#edtValidTill3").datepicker("getDate"));
                obj.Address = $('#edtAddress').val();
                obj.Tel = $('#edtTel').val();
                obj.Fax = $('#edtFax').val();
                obj.VisaNo = $('#edtVisaNo').val();
                obj.DepCity = $('#edtDepCity').val();
                obj.ArrCity = $('#edtArrCity').val();
                obj.AgencyName = $('#edtAgencyName').val();
                obj.AgencyAddress = $('#edtAgencyAddress').val();
                obj.AgencyTel = $('#edtAgencyTel').val();
                obj.AgencyFax = $('#edtAgencyFax').val();
            }
            self.parent.returnCustVisaEdit($("#hfCustNo").val(), obj, source);
        }

        function setCss(element, css) {
            if (css != null && css != '') {
                var cssTag = element;
                var cssList = css.split(';');
                for (i = 0; i < cssList.length; i++) {
                    var _css = cssList[i].split(':');
                    if (_css.length > 1)
                        cssTag.css(_css[0], _css[1]);
                }
            }
        }

        function getCustomerInfo(custNo) {
            $.ajax({
                async: false,
                type: "POST",
                url: "CustomerVisaInfo.aspx/createResCustDiv",
                data: '{"custNo":' + custNo + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg != null && msg.hasOwnProperty('d') && (msg.d != null)) {
                        var data = msg.d;
                        $.each(data, function(i) {
                            switch (this.TagName) {
                                case 'combo':
                                    var combo = this;
                                    $("#" + combo.IdName).html('');
                                    $("#" + combo.IdName).append("<option value='' >" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                                    $.each($.json.decode(this.Data), function(i1) {
                                        if (this.Code == combo.Value)
                                            $("#" + combo.IdName).append("<option value='" + this.Code + "' selected='selected' >" + this.Name + "</option>");
                                        else $("#" + combo.IdName).append("<option value='" + this.Code + "' >" + this.Name + "</option>");
                                    });
                                    break;
                                case 'span':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data);
                                    setCss($("#" + this.IdName), this.Css);
                                    break;
                                case 'date':
                                    var dateInput = $("#" + this.IdName);
                                    if (this.Data != null && this.Data != '') {
                                        var date = this.Data;
                                        //var _date = eval(this.Data.replace(/"\\\/(Date\(-?\d+\))\\\/"/g, 'new $1'));
                                        date = date.replace('/Date(', '');
                                        date = date.replace(')/', '');
                                        date = date.replace(/\\/g, '').replace('\/', '').replace(')', '').replace(/\"/g, '')
                                        var _date = new Date(parseInt(date));
                                        dateInput.datepicker('setDate', _date);
                                        setCss($("#" + this.IdName), this.Css);
                                    } else dateInput.datepicker('setDate', null)
                                    if (this.Disabled == true)
                                        $("#" + this.IdName).datepicker().datepicker('disable')
                                    //$("#" + this.IdName).attr('disabled', 'disabled');
                                    break;
                                case 'input':
                                    $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                                    if (this.Disabled == true)
                                        $("#" + this.IdName).attr('disabled', 'disabled');
                                    setCss($("#" + this.IdName), this.Css);
                                    break;
                                case 'css':
                                    setCss($("#" + this.IdName), this.Css);
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    $("#" + this.IdName).attr("src", this.Data);
                                    setCss($("#" + this.IdName), this.Css);
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                                    setCss($("#" + this.IdName), this.Css);
                                    if (this.Disabled == true)
                                        $("#" + this.IdName).attr('disabled', 'disabled');
                                    break;
                            }
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "CustomerVisaInfo.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    datePickerLang = msg.d;
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }


        $(document).ready(
            function() {
                $.query = $.query.load(location.href);
                var CustNo = $.query.get('CustNo');
                getFormData();
                $("#hfCustNo").val(CustNo);
                $.datepicker.setDefaults($.datepicker.regional[datePickerLang]);
                $("#edtBirtday").datepicker({ showOn: "button", buttonImage: "Images/Calendar.gif", buttonImageOnly: true, changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#edtPassIssueDate").datepicker({ showOn: "button", buttonImage: "Images/Calendar.gif", buttonImageOnly: true, changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#edtPassExpDate").datepicker({ showOn: "button", buttonImage: "Images/Calendar.gif", buttonImageOnly: true, changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#edtValidFrom").datepicker({ showOn: "button", buttonImage: "Images/Calendar.gif", buttonImageOnly: true, changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#edtValidFrom1").datepicker({ showOn: "button", buttonImage: "Images/Calendar.gif", buttonImageOnly: true, changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#edtValidFrom2").datepicker({ showOn: "button", buttonImage: "Images/Calendar.gif", buttonImageOnly: true, changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#edtValidFrom3").datepicker({ showOn: "button", buttonImage: "Images/Calendar.gif", buttonImageOnly: true, changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#edtValidTill").datepicker({ showOn: "button", buttonImage: "Images/Calendar.gif", buttonImageOnly: true, changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#edtValidTill1").datepicker({ showOn: "button", buttonImage: "Images/Calendar.gif", buttonImageOnly: true, changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#edtValidTill2").datepicker({ showOn: "button", buttonImage: "Images/Calendar.gif", buttonImageOnly: true, changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#edtValidTill3").datepicker({ showOn: "button", buttonImage: "Images/Calendar.gif", buttonImageOnly: true, changeMonth: true, changeYear: true, showButtonPanel: true });
                getCustomerInfo(CustNo);
            }
        );
        
    </script>

    <style type="text/css">
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <div class="divVisaInfo">
        <div class="divCustomersVisa">
            <div id="divResCustVisa">
                <input id="hfCustNo" value="" type="hidden" />
                <div class="divResNo">
                    <span id="lblResNo">ResNo</span></div>
                <div class="divCustInfo">
                    <span id="txtCustInfo" class="divCustInfo">CustInfo</span></div>
                <div class="divTitleNameSurname">
                    <div class="divTitle">
                        <span id="edtTitleCap" class="Caption">Title</span><br />
                        <input id="edtTitle" class="titleInput" disabled="disabled" value="" />
                        <input id="edtTitleCode" value="" type="hidden" />
                    </div>
                    <div class="divSurname">
                        <span id="edtSurnameCap" class="Caption">Surname</span><br />
                        <input id="edtSurname" class="surnameInput" onkeypress="return isNonUniCodeChar(event);"
                            maxlength="30" value="" />
                    </div>
                    <div class="divName">
                        <span id="edtNameCap" class="Caption">Name</span><br />
                        <input id="edtName" class="nameInput" onkeypress="return isNonUniCodeChar(event);"
                            maxlength="30" value="" />
                    </div>
                </div>
                <div class="divBirthDayIDNoPassSeriePassNo">
                    <div class="divBirthday">
                        <span id="edtBirtdayCap" class="Caption">Birthday</span><br />
                        <input id="edtBirtday" class="birthdayInput" value="" />
                    </div>
                    <div class="divIdNo">
                        <span id="editIDNoCap" class="Caption">ID No</span><br />
                        <input id="edtIDNo" class="idNoInput" maxlength="20" />
                    </div>
                    <div class="divPassSerie">
                        <span id="editPassSerieCap" class="Caption">Pass Serie-No</span><br />
                        <input id="edtPassSerie" class="passSerieInput" maxlength="5" value="" />-
                        <input id="edtPassNo" class="passNoInput" maxlength="10" value="" /></div>
                </div>
                <div class="divPassIssueDateExpDateGiven">
                    <div class="divPassIssueDate">
                        <span id="edtPassIssueDateCap" class="Caption">Pass Issue Date</span><br />
                        <input id="edtPassIssueDate" class="passIssueDateInput" /></div>
                    <div class="divPassExpDate">
                        <span id="edtPassExpDateCap" class="Caption">Pass Exp Date</span><br />
                        <input id="edtPassExpDate" class="passExpDateInput" /></div>
                    <div class="divPassGiven">
                        <span id="edtPassGivenCap" class="Caption">Pass Given</span><br />
                        <input id="edtPassGiven" class="passGivenInput" maxlength="30" /></div>
                </div>
                <br />
            </div>
        </div>
        <div id="divVisa">
            <div id="divVisaInput">
                <div>
                    <span id="edtBirthSurnameCap" class="Caption">Birth Surname</span><br />
                    <input id="edtBirthSurname" maxlength="30" /></div>
                <div>
                    <span id="edtPlaceOfBirthCap" class="Caption">Place of birth</span><br />
                    <input id="edtPlaceOfBirth" maxlength="30" /></div>
                <div>
                    <span id="edtCountryOfBirthCap" class="Caption">Country of birth</span><br />
                    <input id="edtCountryOfBirth" maxlength="30" /></div>
                <div>
                    <span id="edtRemarkForChildCap" class="Caption">Remark for child</span><br />
                    <textarea id="edtRemarkForChild" cols="63" rows="3"></textarea></div>
                <div>
                    <span id="edtGivenByCap" class="Caption">Given by</span><br />
                    <input id="edtGivenBy" maxlength="30" /></div>
                <div>
                    <span id="edtJobCap" class="Caption">Job</span><br />
                    <input id="edtJob" maxlength="30" /></div>
                <div>
                    <span id="edtEmployerCap" class="Caption">Employer</span><br />
                    <input id="edtEmployer" maxlength="30" /></div>
                <div>
                    <span id="edtEmpAddressCap" class="Caption">Employer Address</span>
                    <br />
                    <textarea id="edtEmpAddress" cols="63" rows="2"></textarea></div>
                <div>
                    <span id="edtEmpTelCap" class="Caption">Employer Tel</span>
                    <br />
                    <input id="edtEmpTel" maxlength="20" style="width: 150px;" /></div>
                <div>
                    <span id="edtInvitePersonCap" class="Caption">Invite person</span><br />
                    <input id="edtInvitePerson" maxlength="30" /></div>
                <div>
                    <span id="edtSexCap" class="Caption">Sex</span>
                    <br />
                    <select id="edtSex">
                    </select></div>
                <div style="clear: both;">
                    <div style="width: 45%; float: left;">
                        <span id="edtMaritalCap" class="Caption">Maritial</span>
                        <br />
                        <select id="edtMarital">
                        </select>
                    </div>
                    <div style="width: 50%; float: left;">
                        <span id="edtMaritalRemarkCap" class="Caption">Marital Remark</span>
                        <br />
                        <input id="edtMaritalRemark" maxlength="30" /></div>
                </div>
                <div style="clear: both;">
                    <span id="edtVisaCountCap" class="Caption">Visa Count</span>
                    <br />
                    <select id="edtVisaCount">
                    </select>
                </div>
                <div style="clear: both;">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 15px;">
                                <strong>1. </strong>
                            </td>
                            <td style="width: 150px;">
                                <span id="edtValidFromCap" class="Caption">Valid From</span>
                                <br />
                                <input id="edtValidFrom" style="width: 110px;" />
                            </td>
                            <td style="width: 150px;">
                                <span id="edtValidTillCap" class="Caption">Valid Till</span>
                                <br />
                                <input id="edtValidTill" style="width: 110px;" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15px;">
                                <strong>2. </strong>
                            </td>
                            <td style="width: 150px;">
                                <span id="edtValidFrom1Cap" class="Caption">Valid From</span>
                                <br />
                                <input id="edtValidFrom1" style="width: 110px;" />
                            </td>
                            <td style="width: 150px;">
                                <span id="edtValidTill1Cap" class="Caption">Valid Till</span>
                                <br />
                                <input id="edtValidTill1" style="width: 110px;" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15px;">
                                <strong>3. </strong>
                            </td>
                            <td style="width: 150px;">
                                <span id="edtValidFrom2Cap" class="Caption">Valid From</span>
                                <br />
                                <input id="edtValidFrom2" style="width: 110px;" />
                            </td>
                            <td style="width: 150px;">
                                <span id="edtValidTill2Cap" class="Caption">Valid Till</span>
                                <br />
                                <input id="edtValidTill2" style="width: 110px;" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15px;">
                                <strong>4. </strong>
                            </td>
                            <td style="width: 150px;">
                                <span id="edtValidFrom3Cap" class="Caption">Valid From</span>
                                <br />
                                <input id="edtValidFrom3" style="width: 110px;" />
                            </td>
                            <td style="width: 150px;">
                                <span id="edtValidTill3Cap" class="Caption">Valid Till</span>
                                <br />
                                <input id="edtValidTill3" style="width: 110px;" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                    <span id="edtAddressCap" class="Caption">Address</span>
                    <br />
                    <textarea id="edtAddress" cols="63" rows="4"></textarea></div>
                <div>
                    <div style="width: 40%; float: left;">
                        <span id="edtTelCap" class="Caption">Tel</span>
                        <br />
                        <input id="edtTel" maxlength="20" /></div>
                    <div style="width: 40%; float: left;">
                        <span id="edtFaxCap" class="Caption">Fax</span>
                        <br />
                        <input id="edtFax" maxlength="20" /></div>
                </div>
                <div style="clear: both;">
                    <span id="edtVisaNoCap" class="Caption">Visa No</span>
                    <br />
                    <input id="edtVisaNo" maxlength="10" /></div>
                <div>
                    <span id="edtDepCityCap" class="Caption">Departure</span>
                    <br />
                    <input id="edtDepCity" maxlength="20" /></div>
                <div>
                    <span id="edtArrCityCap" class="Caption">Arrival</span>
                    <br />
                    <input id="edtArrCity" maxlength="20" /></div>
                <div>
                    <span id="edtAgencyNameCap" class="Caption">Agency Name</span>
                    <br />
                    <input id="edtAgencyName" maxlength="50" /></div>
                <div>
                    <span id="edtAgencyAddressCap" class="Caption">Agency Address</span>
                    <br />
                    <textarea id="edtAgencyAddress" cols="63" rows="2"></textarea></div>
                <div>
                    <div style="width: 40%; float: left;">
                        <span id="edtAgencyTelCap" class="Caption">Agency Tel.</span>
                        <br />
                        <input id="edtAgencyTel" maxlength="20" style="width: 150px;" /></div>
                    <div style="width: 40%; float: left;">
                        <span id="edtAgencyFaxCap" class="Caption">Agency Fax</span>
                        <br />
                        <input id="edtAgencyFax" maxlength="20" style="width: 150px;" /></div>
                </div>
            </div>
        </div>
        <div id="divButtons" style="width: 550px; text-align: center;">
            <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>'
                onclick="exit('save');" style="width: 110px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
            &nbsp;&nbsp;
            <input id="btnCancel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>'
                onclick="exit('cancel');" style="width: 110px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
        </div>
    </div>
    <input id="CustNo" type="hidden" />
    </div>
    <div id="dialog-message" title='<%= GetGlobalResourceObject("ResView", "lblMessage") %>'
        style="display: none;">
        <span id="messages">Message</span>
    </div>
    </form>
</body>
</html>
