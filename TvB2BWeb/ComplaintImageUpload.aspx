﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ComplaintImageUpload.aspx.cs" Inherits="ComplaintImageUpload" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title></title>
  <script src="Scripts/jquery.min.js" type="text/javascript"></script>
  
  <script type="text/javascript">        

    function onCloseBtn() {
      this.close;
      self.parent.uploadPictureClose($("#FileName").val());
    }

    $(document).ready(function () {
    });

  </script>
</head>
<body>
  <form id="fileUploadForm" runat="server">
    <div>
      <asp:FileUpload ID="fileUpld" runat="server" />      
      <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="Upload" />
      <asp:Button ID="btnClose" runat="server" Text="Close" OnClientClick="onCloseBtn()" />
      <asp:HiddenField ID="FileName" runat="server" />
    </div>
  </form>
</body>
</html>
