using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace SANDAO
{

    public class DataBaseParameter
    {

        private SqlParameter param;

        public DataBaseParameter()
        {
            
        }

        public DataBaseParameter(string parameterName, object value)
        {
            this.ParameterName = parameterName;
            this.Value = value;
        }

        public DataBaseParameter(string parameterName, object value, ParameterDirection direction)
        {
            this.ParameterName = parameterName;
            this.Value = value;
            this.Direction = direction;
        }

        public string ParameterName;
        public object Value;
        public ParameterDirection Direction;

        public SqlParameter ToSqlParameter()
        {
            param = new SqlParameter();
            param.ParameterName = this.ParameterName;
            param.Value = this.Value;
            param.Direction = this.Direction;

            return param;
        }

    }
}
