using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Reflection;
using System.Diagnostics;

namespace SANDAO
{
    public class BusinessBase
    {
        #region Fields

        protected string TableName = String.Empty;
        protected string PrimaryKey = String.Empty;
        protected object PrimaryValue = null;
        protected string ConnectionString = String.Empty;
        protected string UserLanguage = "EN";
        protected string LanguageFile = string.Empty;
        protected string UserId = string.Empty;
        protected DataRow Row = null;
        protected Dictionary<string, object> Values = new Dictionary<string, object>();
        protected List<BusinessBase> ReggedObjects = new List<BusinessBase>();

        internal bool HasParent = false;
        internal DataBase CurrentDataBase = null;

        protected string LastErrorMessage = "";
        
        #endregion

        #region Constructors

        public BusinessBase()
        {

        }

        #endregion

        #region Public Funcs

        public void BeginTransaction()
        {
            if (this.CurrentDataBase == null)
                this.CurrentDataBase = new DataBase();
            this.CurrentDataBase.BeginTransaction(IsolationLevel.ReadUncommitted);
            
        }

        public void CommitTransaction()
        {
            if (this.CurrentDataBase != null)
            {
                this.CurrentDataBase.CommitTransaction();
            }
        }

        public void RollBackTransaction()
        {
            if (this.CurrentDataBase != null)
            {
                this.CurrentDataBase.RollBackTransaction();
            }
        }

        public void RegisterObject(BusinessBase childObject)
        {
            childObject.HasParent = true;
            childObject.CurrentDataBase = this.CurrentDataBase;
            this.ReggedObjects.Add(childObject);
        }

        public void Load()
        {
            if (this.PrimaryValue == null)
                throw new Exception("Primary Value is null, cannot load from Table. Internal Library Error, BusinessBase:PrimaryValue");

            string commandText = "Select * from " + this.TableName + " Where " + this.PrimaryKey + " = @id";
            DataBaseParameter param = new DataBaseParameter("@id", this.PrimaryValue);
            if (!this.HasParent && this.CurrentDataBase == null)
            {
                this.CurrentDataBase = new DataBase(this.ConnectionString);
                this.CurrentDataBase.OpenConnection();
            }
            DataSet ds = this.CurrentDataBase.ExecuteDataset(commandText, param);
            this.Row = ds.Tables[0].Rows[0];
            ds.Dispose();
            if (!this.HasParent)
            {
                this.CurrentDataBase.CloseConnection();
                this.CurrentDataBase.Dispose();
            }
            LastErrorMessage = "";
        }

        public void Load(string otherFieldName, object otherValue)
        {
            string commandText = "Select * from " + this.TableName + " Where " + otherFieldName + " = @id";
            DataBaseParameter param = new DataBaseParameter("@id", otherValue);
            if (!this.HasParent && this.CurrentDataBase == null)
            {
                this.CurrentDataBase = new DataBase(this.ConnectionString);
                this.CurrentDataBase.OpenConnection();
            }
            DataSet ds = this.CurrentDataBase.ExecuteDataset(commandText, param);
            this.Row = ds.Tables[0].Rows[0];
            ds.Dispose();
            if (!this.HasParent)
            {
                this.CurrentDataBase.CloseConnection();
                this.CurrentDataBase.Dispose();
            }
            LastErrorMessage = "";
        }

        public void Load(string sql)
        {
            if (!this.HasParent && this.CurrentDataBase == null)
            {
                this.CurrentDataBase = new DataBase(this.ConnectionString);
                this.CurrentDataBase.OpenConnection();
            }
            DataSet ds = this.CurrentDataBase.ExecuteDataset(sql);
            this.Row = ds.Tables[0].Rows[0];
            ds.Dispose();
            if (!this.HasParent)
            {
                this.CurrentDataBase.CloseConnection();
                this.CurrentDataBase.Dispose();
            }
            LastErrorMessage = "";
        }

        public void Load(DataRow dr)
        {
            // PrimaryValue dolmali.....
            this.Row = dr;
            this.PrimaryValue = dr[this.PrimaryKey];
            LastErrorMessage = "";
        }

  
        public DataTable GetTable(List<string> fields, string filter)
        {
            DataSet ds = null;
            if (this.TableName != "")
            {
                DataBase db = new DataBase(this.ConnectionString);
                db.OpenConnection();
                string sql = "Select "; //+ keyField + ", " + valueField + " from " + this.TableName;
                foreach (string s in fields)
                {
                    sql += s + ",";
                }
                sql = sql.Substring(0, sql.Length - 1) + " from " + this.TableName;

                if (filter != "")
                    sql += " Where " + filter;

                ds = db.ExecuteDataset(sql);
                db.Dispose();
            }

            return ds.Tables[0];
        }

        #endregion
    }
}
