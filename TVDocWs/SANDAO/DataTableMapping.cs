using System;
using System.Collections.Generic;
using System.Text;

namespace SANDAO
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class DataTableMapping : Attribute
    {
        private string dataTableName;

        public DataTableMapping(string dataTableName)
        {
            this.dataTableName = dataTableName;
        }

        internal string GetDataTableName()
        {
            return dataTableName;
            
        }
    }
}
