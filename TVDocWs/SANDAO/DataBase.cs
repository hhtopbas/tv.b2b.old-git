using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace SANDAO
{
    public class DataBase : IDisposable
    {
        private SqlConnection connection = null;
        private SqlConnectionStringBuilder connectionStringBuilder = null;
        private string connectionString = String.Empty;
        private bool disposed = false;
        private SqlTransaction Transaction = null;
        public SqlException LastException = null;

        #region Constructors

        /// <summary>
        /// Tries to read connection string info from settings file [DBSettings.xml]
        /// </summary>
        public DataBase()
        {
            
        }

        /// <summary>
        /// Creates a DataBase object with connection string given in parameters
        /// </summary>
        /// <param name="connectionString">Database connection string</param>
        public DataBase(string connectionStr)
        {
            this.connectionString = connectionStr;
            //this.OpenConnection();
        }

        /// <summary>
        /// Builds the connection string with given parameters
        /// </summary>
        /// <param name="dataSource">Database Servers Name or IP</param>
        /// <param name="databaseName">Initial Catalogue, Default Database</param>
        /// <param name="userName">Sql Server Login [Database User]</param>
        /// <param name="password">Sql Server Password Of The User</param>
        public DataBase(string dataSource, string databaseName, string userName, string password)
        {
            connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.DataSource = dataSource;
            connectionStringBuilder.InitialCatalog = databaseName;
            connectionStringBuilder.UserID = userName;
            connectionStringBuilder.Password = password;
            connectionString = connectionStringBuilder.ConnectionString;
        }

        #endregion

        #region Distructors

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.CloseConnection();
                }
            }
            disposed = true;
        }

        ~DataBase()
        {
            Dispose(false);
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// Opens a new connection to the server. If SqlConnection object is null, it constructs a new one
        /// </summary>
        public bool OpenConnection()
        {
            connection = null;
            if (connection == null)
                connection = new SqlConnection(this.connectionString);

            if (connection.State != ConnectionState.Open)
            {
                try
                {
                    
                    connection.Open();
                    return true;
                }
                catch (SqlException ex)
                {
                    //exception gelecek.
                    LastException = ex;
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Closes & disposes current connection
        /// </summary>
        public void CloseConnection()
        {
            if (connection.State != ConnectionState.Closed)
                connection.Close();
            connection.Dispose();
        }

        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            Transaction = connection.BeginTransaction(isolationLevel);
        }

        public void CommitTransaction()
        {
            if (Transaction != null)
                Transaction.Commit();
        }

        public void RollBackTransaction()
        {
            if (Transaction != null)
            {
                Transaction.Rollback();
                Transaction.Dispose();
            }
        }

        #region DataSet Functions

        /// <summary>
        /// Executes the query and returns a full dataset
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <returns>DataSet</returns>
        public DataSet ExecuteDataset(string commandText)
        {
            DataSet ds = new DataSet();
            SqlCommand command = new SqlCommand(commandText, this.connection);
            //command.CommandTimeout = 500;

            if (Transaction != null)
                command.Transaction = this.Transaction;
            if (this.Parameters.Count > 0)
            {
                foreach (DataBaseParameter param in Parameters.Values)
                {
                    command.Parameters.Add(param.ToSqlParameter());
                }
            }
            
            SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
            dataAdapter.Fill(ds);
            dataAdapter.Dispose();
            command.Dispose();            
            return ds;
        }
        public DataSet ExecuteDataset1(string commandText)
        {
            DataSet ds = new DataSet();
            SqlConnection cnn = new SqlConnection(this.connectionString);
            cnn.Open();            
            SqlCommand command = new SqlCommand(commandText, cnn);
            command.CommandTimeout = 500;
            if (this.Parameters.Count > 0)
            {
                foreach (DataBaseParameter param in Parameters.Values)
                {
                    command.Parameters.Add(param.ToSqlParameter());
                }
            }

            SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
            dataAdapter.Fill(ds);
            dataAdapter.Dispose();
            command.Dispose();
            cnn.Close();
            cnn.Dispose();
            return ds;
        }

        /// <summary>
        /// Executes the query and returns a full dataset
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <returns>DataSet</returns>
        public DataSet ExecuteDataset(string commandText, CommandType commandType)
        {
            DataSet ds = new DataSet();            
            SqlCommand command = new SqlCommand(commandText, this.connection);

            if (Transaction != null)
                command.Transaction = this.Transaction;

            if (this.Parameters.Count > 0)
            {
                foreach (DataBaseParameter parameter in this.Parameters.Values)
                {
                    command.Parameters.Add(parameter.ToSqlParameter());
                }
            }
            command.CommandType = commandType;
            SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
            dataAdapter.Fill(ds);
            dataAdapter.Dispose();
            command.Dispose();            
            return ds;
        }

        /// <summary>
        /// Executes the query and returns a full dataset
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <param name="parameters">Command Parameters</param>
        /// <returns>DataSet</returns>
        public DataSet ExecuteDataset(string commandText, params DataBaseParameter[] parameters)
        {
            DataSet ds = new DataSet();
            //SqlConnection cnn = new SqlConnection(this.connectionString);
            //cnn.Open();
            SqlCommand command = new SqlCommand(commandText, this.connection);

            if (Transaction != null)
                command.Transaction = this.Transaction;
            foreach (DataBaseParameter param in parameters)
            {
                command.Parameters.Add(new SqlParameter(param.ParameterName, param.Value));
            }
            SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
            dataAdapter.Fill(ds);
            dataAdapter.Dispose();
            command.Dispose();
            //cnn.Close();
            //cnn.Dispose();
            return ds;
        }

        /// <summary>
        /// Executes the query and returns a full dataset
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <param name="parameters">Command Parameters</param>
        /// <returns>DataSet</returns>
        public DataSet ExecuteDataset(string commandText, CommandType commandType, params DataBaseParameter[] parameters)
        {
            
            DataSet ds = new DataSet();            
            SqlCommand command = new SqlCommand(commandText, this.connection);
            command.CommandType = commandType;

            if (Transaction != null)
                command.Transaction = this.Transaction;

            foreach (DataBaseParameter param in parameters)
            {
                command.Parameters.Add(new SqlParameter(param.ParameterName, param.Value));
            }
            SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
            dataAdapter.Fill(ds);
            dataAdapter.Dispose();
            command.Dispose();
            return ds;
        }

     


        #endregion

        #region DataTable Functions

        /// <summary>
        /// Executes the query and returns a full dataset
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <returns>DataSet</returns>
        public DataTable ExecuteDataTable(string commandText)
        {
            return this.ExecuteDataset(commandText).Tables[0];
        }
        public DataTable ExecuteSPDataTable(string commandText, params DataBaseParameter[] parameters)
        {
            DataSet ds = new DataSet();
            //SqlConnection cnn = new SqlConnection(this.connectionString);
            //cnn.Open();
            SqlCommand command = new SqlCommand(commandText, this.connection);
            command.CommandType = CommandType.StoredProcedure;

            if (Transaction != null)
                command.Transaction = this.Transaction;
            foreach (DataBaseParameter param in parameters)
            {
                command.Parameters.Add(new SqlParameter(param.ParameterName, param.Value));
            }
            SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
            dataAdapter.Fill(ds);
            dataAdapter.Dispose();
            command.Dispose();
            //cnn.Close();
            //cnn.Dispose();
            return ds.Tables[0];
        }

        /// <summary>
        /// Executes the query and returns a full dataset
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <returns>DataSet</returns>
        public DataTable ExecuteDataTable(string commandText, CommandType commandType)
        {
            return this.ExecuteDataset(commandText, commandType).Tables[0];
        }

        /// <summary>
        /// Executes the query and returns a full dataset
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <param name="parameters">Command Parameters</param>
        /// <returns>DataSet</returns>
        public DataTable ExecuteDataTable(string commandText, params DataBaseParameter[] parameters)
        {
            return this.ExecuteDataset(commandText, parameters).Tables[0];
        }

        /// <summary>
        /// Executes the query and returns a full dataset
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <param name="parameters">Command Parameters</param>
        /// <returns>DataSet</returns>
        public DataTable ExecuteDataTable(string commandText, CommandType commandType, params DataBaseParameter[] parameters)
        {
            return this.ExecuteDataset(commandText, commandType, parameters).Tables[0];
        }

        #endregion

        #region DataReader Functions

        /// <summary>
        /// Executes the query and returns an open SqlDataReader
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <returns>SqlDataReader</returns>
        
        public SqlDataReader ExecuteReader(string commandText)
        {
            SqlCommand command = new SqlCommand(commandText, this.connection);
            if (this._Parameters.Count > 0)
            {
                foreach (DataBaseParameter parameter in this._Parameters.Values)
                {
                    command.Parameters.Add(parameter.ToSqlParameter());
                }
            }
            SqlDataReader reader = command.ExecuteReader();
            command.Dispose();
            return reader;
        }

        /// <summary>
        /// Executes the query and returns an open SqlDataReader
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <param name="commandType">CommandType</param>
        /// <returns>SqlDataReader</returns>
        public SqlDataReader ExecuteReader(string commandText, CommandType commandType)
        {
            SqlCommand command = new SqlCommand(commandText, this.connection);
            command.CommandType = commandType;
            if (this._Parameters.Count > 0)
            {
                foreach (DataBaseParameter parameter in this._Parameters.Values)
                {
                    command.Parameters.Add(parameter.ToSqlParameter());
                }
            }
            SqlDataReader reader = command.ExecuteReader();
            command.Dispose();
            return reader;
        }

        /// <summary>
        /// Executes the query and returns an open SqlDataReader
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <param name="parameters">Command Parameters</param>
        /// <returns></returns>
        public SqlDataReader ExecuteReader(string commandText, params DataBaseParameter[] parameters)
        {
            SqlCommand command = new SqlCommand(commandText, this.connection);
            foreach (DataBaseParameter param in parameters)
            {
                command.Parameters.Add(new SqlParameter(param.ParameterName, param.Value));
            }
            SqlDataReader reader = command.ExecuteReader();
            command.Dispose();
            return reader;
        }

        /// <summary>
        /// Executes the query and returns an open SqlDataReader
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <param name="commandType">Command Type</param>
        /// <param name="parameters">Command Parameters</param>
        /// <returns></returns>
        public SqlDataReader ExecuteReader(string commandText, CommandType commandType, params DataBaseParameter[] parameters)
        {
            SqlCommand command = new SqlCommand(commandText, this.connection);
            command.CommandType = commandType;
            foreach (DataBaseParameter param in parameters)
            {
                command.Parameters.Add(new SqlParameter(param.ParameterName, param.Value));
            }
            SqlDataReader reader = command.ExecuteReader();
            command.Dispose();
            return reader;
        }

        #endregion

        #region NonQuery Functions

        /// <summary>
        /// Executes the query and returns the count of effected rows by the query (eg. update clause)
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <returns>Number Of Rows Effected</returns>
        public int ExecuteNoneQuery(string commandText)
        {
            SqlCommand command = new SqlCommand(commandText, this.connection);

            if (Transaction != null)
                command.Transaction = this.Transaction;
            if (this._Parameters.Count > 0)
            {
                foreach (DataBaseParameter parameter in this._Parameters.Values)
                {
                    command.Parameters.Add(parameter.ToSqlParameter());
                }
            }
            int rowsEffected = command.ExecuteNonQuery();
            command.Dispose();
            return rowsEffected;
        }

        /// <summary>
        /// Executes the query and returns the count of effected rows by the query (eg. update clause)
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <param name="commandType">Command Type</param>
        /// <returns>Number Of Rows Effected</returns>
        public int ExecuteNoneQuery(string commandText, CommandType commandType)
        {
            SqlCommand command = new SqlCommand(commandText, this.connection);
            command.CommandType = commandType;

            if (Transaction != null)
                command.Transaction = this.Transaction;
            if (this._Parameters.Count > 0)
            {
                foreach (DataBaseParameter parameter in this._Parameters.Values)
                {
                    command.Parameters.Add(parameter.ToSqlParameter());
                }
            }
            int rowsEffected = command.ExecuteNonQuery();
            command.Dispose();
            return rowsEffected;
        }

        /// <summary>
        /// Executes the query and returns the count of effected rows by the query (eg. update clause)
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <param name="parameters">Command Parameters</param>
        /// <returns>Number Of Rows Effected</returns>
        public int ExecuteNoneQuery(string commandText, params DataBaseParameter[] parameters)
        {
            SqlCommand command = new SqlCommand(commandText, this.connection);
            foreach (DataBaseParameter param in parameters)
            {
                command.Parameters.Add(new SqlParameter(param.ParameterName, param.Value));
            }

            if (Transaction != null)
                command.Transaction = this.Transaction;

            int rowsEffected = command.ExecuteNonQuery();
            command.Dispose();
            return rowsEffected;
        }

        /// <summary>
        /// Executes the query and returns the count of effected rows by the query (eg. update clause)
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <param name="commandType">Command Type</param>
        /// <param name="parameters">Command Parameters</param>
        /// <returns>Number Of Rows Effected</returns>
        public int ExecuteNoneQuery(string commandText, CommandType commandType, params DataBaseParameter[] parameters)
        {
            SqlCommand command = new SqlCommand(commandText, this.connection);
            command.CommandType = commandType;
            foreach (DataBaseParameter param in parameters)
            {
                command.Parameters.Add(new SqlParameter(param.ParameterName, param.Value));
            }

            if (Transaction != null)
                command.Transaction = this.Transaction;

            int rowsEffected = command.ExecuteNonQuery();
            command.Dispose();
            return rowsEffected;
        }

        #endregion

        #region ExecuteScalar Functions

        /// <summary>
        /// Executes the query and returs the first column of the first row of the result set. Additional rows and columns are ignored. (eg. SUM, COUNT)
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <returns>The first column of the first row in the result set</returns>
        public object ExecuteScalar(string commandText)
        {
            SqlCommand command = new SqlCommand(commandText, this.connection);

            if (Transaction != null)
                command.Transaction = this.Transaction;
            if (this._Parameters.Count > 0)
            {
                foreach (DataBaseParameter parameter in this._Parameters.Values)
                {
                    command.Parameters.Add(parameter.ToSqlParameter());
                }
            }
            object firstCol = command.ExecuteScalar();
            command.Dispose();
            return firstCol;
        }

        /// <summary>
        /// Executes the query and returs the first column of the first row of the result set. Additional rows and columns are ignored. (eg. SUM, COUNT)
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <param name="commandType">Command Type</param>
        /// <returns>The first columns of the first row in the result set</returns>
        public object ExecuteScalar(string commandText, CommandType commandType)
        {
            SqlCommand command = new SqlCommand(commandText, this.connection);
            command.CommandType = commandType;

            if (Transaction != null)
                command.Transaction = this.Transaction;
            if (this._Parameters.Count > 0)
            {
                foreach (DataBaseParameter parameter in this._Parameters.Values)
                {
                    command.Parameters.Add(parameter.ToSqlParameter());
                }
            }
            object firstCol = command.ExecuteScalar();
            command.Dispose();
            return firstCol;
        }

        /// <summary>
        /// Executes the query and returs the first column of the first row of the result set. Additional rows and columns are ignored. (eg. SUM, COUNT)
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <param name="parameters">Command Parameters</param>
        /// <returns>The first columns of the first row in the result set</returns>
        public object ExecuteScalar(string commandText, params DataBaseParameter[] parameters)
        {
            SqlCommand command = new SqlCommand(commandText, this.connection);
            foreach (DataBaseParameter param in parameters)
            {
                command.Parameters.Add(new SqlParameter(param.ParameterName, param.Value));
            }

            if (Transaction != null)
                command.Transaction = this.Transaction;

            object firstCol = command.ExecuteScalar();
            command.Dispose();
            return firstCol;
        }

        /// <summary>
        /// Executes the query and returs the first column of the first row of the result set. Additional rows and columns are ignored. (eg. SUM, COUNT)
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <param name="commandType">Command Type</param>
        /// <param name="parameters">Command Parameters</param>
        /// <returns>The first columns of the first row in the result set</returns>
        public object ExecuteScalar(string commandText, CommandType commandType, params DataBaseParameter[] parameters)
        {
            SqlCommand command = new SqlCommand(commandText, this.connection);
            command.CommandType = commandType;
            foreach (DataBaseParameter param in parameters)
            {
                command.Parameters.Add(new SqlParameter(param.ParameterName, param.Value));
            }

            if (Transaction != null)
                command.Transaction = this.Transaction;

            object firstCol = command.ExecuteScalar();
            command.Dispose();
            return firstCol;
        }

        #endregion

        #region ExecuteAndGetIdentity Functions

        /// <summary>
        /// Executes the query and returns the last inserted identity value
        /// </summary>
        /// <param name="commandText">Sql String</param>
        /// <returns>Last Inserted Identity Value in This Scope</returns>
        public object ExecuteAndGetIdentity(string commandText)
        {
            commandText += " SELECT SCOPE_IDENTITY() ";
            SqlCommand command = new SqlCommand(commandText, this.connection);

            if (Transaction != null)
                command.Transaction = this.Transaction;
            if (this._Parameters.Count > 0)
            {
                foreach (DataBaseParameter parameter in this._Parameters.Values)
                {
                    command.Parameters.Add(parameter.ToSqlParameter());
                }
            }
            object identityValue = command.ExecuteScalar();
            command.Dispose();
            return identityValue;
        }

        public object ExecuteAndGetIdentity(string commandText, params DataBaseParameter[] parameters)
        {
            commandText += " SELECT SCOPE_IDENTITY() ";
            SqlCommand command = new SqlCommand(commandText, this.connection);
            foreach (DataBaseParameter param in parameters)
            {
                command.Parameters.Add(new SqlParameter(param.ParameterName, param.Value));
            }

            if (Transaction != null)
                command.Transaction = this.Transaction;

            object identityValue = command.ExecuteScalar();
            command.Dispose();
            return identityValue;
        }

        #endregion

        #region Other Util Functions

        public object InsertRecord(string TableName, Dictionary<string, DataBaseParameter> parameters)
        {
            object retVal = null;
            SqlCommand command = new SqlCommand();
            string commandText = "";
            string valueClause = "";
            commandText = "Insert Into " + TableName + "(";

            foreach (string key in parameters.Keys)
            {
                commandText += key + ",";
                command.Parameters.Add(new SqlParameter(parameters[key].ParameterName, parameters[key].Value));
                valueClause += parameters[key].ParameterName + ",";
            }
            commandText = commandText.Substring(0, commandText.Length - 1) + ")";
            valueClause = valueClause.Substring(0, valueClause.Length - 1);
            commandText = commandText + " Values(" + valueClause + ")";

            command.CommandText = commandText + " SELECT SCOPE_IDENTITY() ";
            command.Connection = this.connection;
            if (Transaction != null)
                command.Transaction = this.Transaction;

            retVal = command.ExecuteScalar().ToString();

            command.Dispose();
            return retVal;
        }

        public bool UpdateRecord(string TableName, Dictionary<string, DataBaseParameter> parameters, string primaryKeyField, int privaryKeyValue)
        {
            object retVal = null;
            SqlCommand command = new SqlCommand();
            string commandText = "";
            commandText = "Update " + TableName + " Set ";

            foreach (string key in parameters.Keys)
            {
                commandText += key + " = " + parameters[key].ParameterName + ",";

                command.Parameters.Add(new SqlParameter(parameters[key].ParameterName, parameters[key].Value));

            }
            commandText = commandText.Substring(0, commandText.Length - 1);
            commandText += " Where " + primaryKeyField + " = " + privaryKeyValue.ToString();
            this.OpenConnection();
            command.CommandText = commandText;
            command.Connection = this.connection;

            if (Transaction != null)
                command.Transaction = this.Transaction;

            retVal = command.ExecuteNonQuery();
            command.Dispose();
            if (retVal != null)
                return true;
            else
                return false;
        }

        #endregion


        #region Stored Procedure Functions



        #endregion


        #endregion

        #region Public Properties

        private Dictionary<string, DataBaseParameter> _Parameters = new Dictionary<string, DataBaseParameter>();
        public Dictionary<string, DataBaseParameter> Parameters
        {
            get { return _Parameters; }
        }

        private string _QueryString = String.Empty;
        public string QueryString { get { return _QueryString; } set { _QueryString = value; } }


        #endregion

    }
}
