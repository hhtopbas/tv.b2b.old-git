using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Web;

namespace SANDAO
{
    public static class xDBSettings
    {
        public static Dictionary<string, string> GetConnectionStrings()
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load("DBSettings.xml");
            XmlElement xRoot = xDoc.DocumentElement;
            XmlNodeList dbList = xDoc.GetElementsByTagName("ConnectionString");
            Dictionary<string, string> lines = new Dictionary<string, string>();
            foreach (XmlNode node in dbList)
            {
                lines.Add(node.Attributes["Name"].Value, node.Attributes["Value"].Value);
            }
            return lines;
        }

        public static string GetConnectionString(string databaseName)
        {
            XmlDocument xDoc = new XmlDocument();
            
            xDoc.Load("DBSettings.xml");
            XmlElement xRoot = xDoc.DocumentElement;
            XmlNodeList list = xDoc.GetElementsByTagName("ConnectionString");
            string conStr = "";
            foreach (XmlNode node in list)
            {
                if (node.Attributes["Name"].Value == databaseName)
                    conStr = node.Attributes["Value"].Value;
            }

            return conStr; 
        }
    }
}
