using System;
using System.Collections.Generic;
using System.Text;

namespace SANDAO
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class DataColumnMapping : Attribute
    {
        private string dataColumnName;
        private bool primaryKey;

        public DataColumnMapping(string dataColumnName)
        {
            this.dataColumnName = dataColumnName;
        }

        public bool PrimaryKey
        {
            get { return primaryKey; }
            set { primaryKey = value; }
        }

        public string GetDataColumnName()
        {
            return dataColumnName;
        }
    }
}
