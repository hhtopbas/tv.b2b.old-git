﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Collections.Generic;
using SANDAO;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;

namespace TVDocWs
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSDocument : System.Web.Services.WebService
    {

        protected List<string> getConnString(string dbName)
        {
            DataSet ds = new DataSet();
            List<string> list = null;
            ds.ReadXml(Server.MapPath("~/Data/database.xml"));            
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if (row["name"].ToString() == dbName)
                {
                    list = new List<string>() { row["connstr"].ToString(), row["TvPath"].ToString() };                    
                    break;
                }
            }
            return list;
        }

        [WebMethod]
        public TOutParams GetDocument(string ResNo, int DocType, string Agency, string AgencyUser,
                                      string Supplier, string Service, int? ServiceID, int? CustNo, int? FormID, string Params)
        {
            TOutParams tsOutParams = new TOutParams();

            List<string> tvDocList =getConnString("TVDoc");
            string TvConString = tvDocList[0];
            DataBase dbTv = new DataBase(TvConString);

            try
            {
                dbTv.OpenConnection();

                string SqlStr0 = "Select RefNo From ResMain (NOLOCK) Where ResNo=@ResNo";
                DataTable dt0 = dbTv.ExecuteDataTable(SqlStr0, new DataBaseParameter("@ResNo", ResNo));
                if (dt0.Rows.Count == 0)
                {
                    tsOutParams.ErrMessage = "Reservation is not found!";
                    return tsOutParams;
                }

                int sleepCount = 0;
                while (1 == 1)
                {
                    Process[] ps = Process.GetProcessesByName("TourVISIO.exe");
                    if (ps.Length < 10 || sleepCount > 10)
                        break;
                    else
                    {
                        System.Threading.Thread.Sleep(1000);
                        sleepCount++;
                    }
                }

                string SqlStr = " Insert Into DocReportTmp (ResNo,DocType,Status,CrtDate,CrtUser,CrtAgency) " +
                                " Values(@ResNo,@DocType,0,GetDate(),@AgencyUser,@Agency) " +
                                " Select SCOPE_IDENTITY() as NewID ";

                DataTable dt = dbTv.ExecuteDataTable(SqlStr,
                                                new DataBaseParameter("@ResNo", ResNo),
                                                new DataBaseParameter("@DocType", DocType),
                                                new DataBaseParameter("@AgencyUser", AgencyUser),
                                                new DataBaseParameter("@Agency", Agency)
                                      );

                string NewIDStr = dt.Rows[0]["NewID"].ToString();
                int Status = 0;
                byte[] RptImageOut = null;

                //C:\TourVisioDoc\TourVISIO.exe /DE /ID:451451 /RN:RU072982 /DT:5

                string CmdStr = "/DE /ID:" + NewIDStr + " /RN:" + ResNo + " /DT:" + DocType;

                //Additional parameters
                if (Supplier != "")
                    CmdStr = CmdStr + " /SP:" + (char)34 + Supplier + (char)34;
                if (Service != "")
                    CmdStr = CmdStr + " /SV:" + (char)34 + Service + (char)34;

                if (ServiceID.HasValue)
                //if (ServiceID>0)
                    CmdStr = CmdStr + " /SI:" + ServiceID;

                if (CustNo.HasValue)
                //if (CustNo>0)
                    CmdStr = CmdStr + " /CN:" + CustNo;

                if (FormID.HasValue)
                //if (FormID>0)
                    CmdStr = CmdStr + " /DI:" + FormID;

                if (Params != "")
                    CmdStr = CmdStr + " " + Params;

                string SqlStr4 = "Update DocReportTmp Set ParamStr=@CmdStr Where RecID=@RecID";
                dbTv.ExecuteNoneQuery(SqlStr4, new DataBaseParameter("@CmdStr", CmdStr),
                                               new DataBaseParameter("@RecID", NewIDStr)
                                     );

                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo(tvDocList[1]);
                psi.RedirectStandardOutput = true;
                psi.Arguments = CmdStr; //"/DE /ID:" + NewIDStr + " /RN:" + ResNo + " /DT:" + DocType;
                psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                psi.UseShellExecute = false;
                System.Diagnostics.Process listFiles;
                listFiles = System.Diagnostics.Process.Start(psi);
                System.IO.StreamReader myOutput = listFiles.StandardOutput;
                // buraya süre yazmazsak bitene kadar bekler süre yazarsak o kadar süre biter sonra hasexited ile exenin sonlanıp sonlanmadığını görebiliriz
                listFiles.WaitForExit(95000);//TourVisio.exe : 90 sn.
                
                if (listFiles.HasExited)
                {
                    string SqlStr2 = "Select Status,RptImage From DocReportTmp (NOLOCK) Where RecID=@RecID";
                    DataTable dt2 = dbTv.ExecuteDataTable(SqlStr2, new DataBaseParameter("@RecID", NewIDStr));
                    Status = Convert.ToInt32(dt2.Rows[0]["Status"].ToString());
                    object imageObj = dt2.Rows[0]["RptImage"];
                    if (dt2.Rows[0]["RptImage"].ToString() != "")
                        RptImageOut = ((byte[])imageObj);

                    if (Status == 1)
                    {
                        string SqlStr3 = "Update DocReportTmp Set RptImage=Null Where RecID=@RecID";
                        dbTv.ExecuteNoneQuery(SqlStr3, new DataBaseParameter("@RecID", NewIDStr));
                    }
                }
                else
                {
                    Status = 9;
                    listFiles.Kill();
                }

                tsOutParams.newRecID = NewIDStr;
                tsOutParams.Status = Status;
                tsOutParams.RptImage = RptImageOut;

                return tsOutParams;
            }
            catch (Exception ExcStr)
            {
                tsOutParams.ErrMessage = ExcStr.Message;
                return tsOutParams;
            }
            finally
            {
                dbTv.CloseConnection();
            }
        }

    }
    [Serializable]
    public class TOutParams
    {
        public string newRecID;
        public int    Status;
        public byte[] RptImage;
        public string ErrMessage;
    }
}
